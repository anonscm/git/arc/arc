/*
 * ar-interp-altarica-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_INTERP_ALTARICA_P_H
# define AR_INTERP_ALTARICA_P_H

# include <ccl/ccl-assert.h>
# include "ar-interp-altarica.h"

CCL_DECLARE_EXCEPTION(invalid_node_type_error,internal_error);

#define INVALID_NODE_TYPE() ccl_throw_no_msg(invalid_node_type_error)
#define INTERP_EXCEPTION() ccl_throw_no_msg(altarica_interpretation_exception)

			/* --------------- */

#define INTERP_EXCEPTION_MSG(_args_) \
do { \
  ar_error _args_ ; \
  INTERP_EXCEPTION(); \
} while(0)


			/* --------------- */

# define IS_LABELLED_BY(_t,_l) \
  ( (_t) != NULL && (_t)->node_type == AR_TREE_ ## _l)

			/* --------------- */

extern void
ar_error (altarica_tree *loc, const char *fmt, ...);

extern void
ar_warning (altarica_tree *loc, const char *fmt, ...);

extern void
ar_interp_extern_decl (altarica_tree *t, ar_node *node)
  CCL_THROW(altarica_interpretation_exception);
			     
#endif /* ! AR_INTERP_ALTARICA_P_H */
