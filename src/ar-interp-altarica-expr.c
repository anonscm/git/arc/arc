/*
 * ar-interp-altarica-expr.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-attributes.h"
#include "ar-model.h"
#include "ar-bang-ids.h"
#include "ar-interp-altarica-p.h"
#include "mec5/mec5.h"

static void
s_check_expected_type (altarica_tree *t, ar_type *actual_type, 
		       ar_type *expected_type)
  CCL_THROW(altarica_interp_exception)
{
  if (expected_type == NULL ||
      ar_type_have_same_base_type(actual_type, expected_type))
    return;

  ar_error (t, "bad expression type.\n");
  ar_error (t, "getting type '");
  ar_type_log (CCL_LOG_ERROR, actual_type);
  ccl_error("'.\n");
  ar_error (t, "instead of '");
  ar_type_log (CCL_LOG_ERROR, expected_type);
  ccl_error("'.\n");
  INTERP_EXCEPTION();
}

			/* --------------- */

static void
s_check_result_type (altarica_tree *t, ar_expr *result, ar_type *expected_type)
  CCL_THROW(altarica_interp_exception)
{
  ar_type *result_type = ar_expr_get_type (result);  

  ccl_try (altarica_interpretation_exception)
    {
      s_check_expected_type (t, result_type, expected_type);
    }
  ccl_catch
    {
      ar_type_del_reference (result_type);
      ar_expr_del_reference (result);
      ccl_rethrow ();
    }
  ccl_end_try;
  ar_type_del_reference (result_type);
}

/*
ar_expr *
ar_interp_typed_expression(altarica_tree *t, ar_type *type, ar_context *ctx,
			   ar_member_access_interp *mai)
  CCL_THROW(altarica_interp_exception)
{
  ar_expr *result = ar_interp_expression(t,ctx,mai);
  if( type != NULL )
    {
      ar_type *btype = ar_type_get_base_type(type);

      if( ! ar_expr_has_base_type(result,btype) )
	{
	  ar_type *rtype = ar_expr_get_type(result);
	  ccl_error("%s:%d: error: bad expression type.\n",
		    t->filename,t->line);
	  ccl_error("%s:%d: error: getting type '",
		    t->filename,t->line);
	  ar_type_log(CCL_LOG_ERROR,rtype);
	  ccl_error("'.\n%s:%d: error: instead of '",
		    t->filename,t->line);
	  ar_type_log(CCL_LOG_ERROR,type);
	  ccl_error("'.\n");
	  ar_type_del_reference(rtype);
	  ar_expr_del_reference(result);
	  ar_type_del_reference(btype);
	  INTERP_EXCEPTION();
	}
      ar_type_del_reference(btype);
    }


  return result;
}
*/
			/* --------------- */


ar_expr *
ar_interp_constant_expression (altarica_tree *t, ar_type *type, ar_context *ctx,
			      ar_member_access_interp *mai)
  CCL_THROW (altarica_interp_exception)
{
  ar_expr *expr = ar_interp_expression (t, type, ctx, mai);

  if (! ar_expr_is_constant (expr, ctx))
    {
      ar_error (t, "not constant expression '");
      ar_expr_log (CCL_LOG_ERROR, expr);
      ar_error (NULL, "'.\n");
      ar_expr_del_reference (expr);
      INTERP_EXCEPTION ();
    }

  return expr;
}

			/* --------------- */

static ar_constant *
s_interp_typed_constant (altarica_tree *t, ar_type *type, ar_context *ctx,
			ar_member_access_interp *mai)
{
  ar_expr *expr = ar_interp_constant_expression (t, type, ctx, mai);
  ar_constant *result = NULL;

  if (! ar_expr_is_computable (expr))
    {
      ar_error (t, "constant expression '");
      ar_expr_log (CCL_LOG_ERROR, expr);
      ar_error (NULL, "' can't be evaluated to a constant value.\n");
      ar_expr_del_reference (expr);
      INTERP_EXCEPTION();
    } 
    
  result = ar_expr_eval (expr, ctx);
  ar_expr_del_reference (expr);

  return result;
}

			/* --------------- */

static ar_constant *
s_interp_integer_constant (altarica_tree *t, ar_context *ctx,
			   ar_member_access_interp *mai)
{
  return s_interp_typed_constant (t, AR_INTEGERS, ctx, mai);
}

			/* --------------- */

static ar_expr *
s_interp_integer_expr (altarica_tree *t, ar_context *ctx,
		       ar_member_access_interp *mai)
{
  return ar_interp_expression (t, AR_INTEGERS, ctx, mai);
}

			/* --------------- */

static ar_expr *
s_interp_ite (altarica_tree *t, ar_type *type, ar_context *ctx, 
	      ar_member_access_interp *mai)
{
  ar_expr *e_if = NULL;
  ar_expr *e_then = NULL;
  ar_expr *e_else = NULL;
  ar_type *then_type = NULL;
  ar_expr *result = NULL;

  ccl_pre (t->node_type == AR_TREE_ITE);

  e_if = ar_interp_boolean_expression (t->child, ctx, mai);

  ccl_try (altarica_interpretation_exception)
    {
      e_then = ar_interp_expression (t->child->next, type, ctx,mai);
      then_type = ar_expr_get_type (e_then);
      e_else = ar_interp_expression (t->child->next->next, then_type, ctx,
				     mai);
      result = ar_expr_crt_ternary (AR_EXPR_ITE, e_if, e_then, e_else);
    }
  ccl_no_catch;
	
  ar_expr_del_reference (e_if);
  ccl_zdelete (ar_expr_del_reference, e_then);
  ccl_zdelete (ar_expr_del_reference, e_else);
  ccl_zdelete (ar_type_del_reference, then_type);

  if (result == NULL)
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_case (altarica_tree *t, ar_type *type, ar_context *ctx, 
	       ar_member_access_interp *mai)
{
  ar_expr **conds;
  ar_expr *result = NULL;
  int i;
  int nb_args;
  ar_expr *dflt = NULL;

  ccl_pre (IS_LABELLED_BY (t, CASE));

  nb_args = ccl_parse_tree_count_siblings (t->child) - 1;

  ccl_assert (nb_args > 0);

  conds = ccl_new_array (ar_expr *, 2 * nb_args);
  
  if (type != NULL)
    type = ar_type_add_reference (type);

  ccl_try (altarica_interpretation_exception)
    {
      altarica_tree *choice = t->child;

      for (i = 0; i < nb_args; i++, choice = choice->next)
	{
	  ccl_assert (choice->node_type == AR_TREE_CASE_CHOICE);

	  conds[2 * i] = ar_interp_boolean_expression (choice->child, ctx, mai);
	  conds[2 * i + 1] = ar_interp_expression (choice->child->next, type, 
						   ctx, mai);
	  if (type == NULL) 
	    type = ar_expr_get_type (conds[2 * i + 1]);
	}

      ccl_assert (choice != NULL);
      ccl_assert (choice->node_type == AR_TREE_CASE_DEFAULT);

      dflt = ar_interp_expression (choice->child, type, ctx, mai);

      result = ar_expr_crt_case (conds,nb_args,dflt);
    }
  ccl_no_catch;

  for (i = 0; i < nb_args; i++)
    {
      ccl_zdelete (ar_expr_del_reference, conds[2 * i]);
      ccl_zdelete (ar_expr_del_reference, conds[2 * i + 1]);
    }
  ccl_delete (conds);
  ccl_zdelete (ar_type_del_reference, type);
  ccl_zdelete (ar_expr_del_reference, dflt);

  if (result == NULL)
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static ar_expr *
s_crt_min_max (ar_expr_op op, ar_expr **args, int nb_args)
{
#if 0
  int i;
  ar_expr *result = ar_expr_add_reference (args[0]);

  for (i = 1; i < nb_args; i++)
    {
      ar_expr *cond = ar_expr_crt_binary (op, result, args[i]);
      ar_expr *ite =  ar_expr_crt_ternary (AR_EXPR_ITE, cond, result, args[i]);
      ar_expr_del_reference (result);
      ar_expr_del_reference (cond);
      result = ite;
    }
#elif 0
  int i;
  ar_expr *result = ar_expr_add_reference (args[nb_args - 1]);

  ccl_assert (nb_args > 1);

  for (i = nb_args - 2; i >= 0; i--)
    {
      int j;
      ar_expr *ite;
      ar_expr *cond = NULL;

      for (j = 0; j < nb_args; j++)
	{
	  ar_expr *tmp; 
	  if (j == i)
	    continue;
	  tmp = ar_expr_crt_binary (op, args[i], args[j]);
	  if (cond == NULL)
	    cond = tmp;
	  else
	    {
	      ar_expr *aux = ar_expr_crt_binary (AR_EXPR_AND, cond, tmp);
	      ar_expr_del_reference (tmp);
	      ar_expr_del_reference (cond);
	      cond = aux;
	    }
	}
      ite =  ar_expr_crt_ternary (AR_EXPR_ITE, cond, args[i], result);
      ar_expr_del_reference (result);
      ar_expr_del_reference (cond);
      result = ite;
    }
#else
  ar_expr *result;

  if (op == AR_EXPR_LT) 
    result = ar_expr_crt_min (nb_args, args);
  else
    result = ar_expr_crt_max (nb_args, args);
#endif

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_min_max (altarica_tree *t, ar_context *ctx, 
		  ar_member_access_interp *mai)
{
  int i;
  ar_expr *result = NULL;
  ar_expr **args;
  int nb_args;

  ccl_pre (IS_LABELLED_BY (t, MIN) || IS_LABELLED_BY (t, MAX));

  nb_args = ccl_parse_tree_count_siblings (t->child);
  args = ccl_new_array (ar_expr *, nb_args);

  ccl_try (altarica_interpretation_exception)
    {
      ar_expr_op op;
      
      if (IS_LABELLED_BY (t, MIN)) 
	op = AR_EXPR_LT;
      else 
	op = AR_EXPR_GT;

      for (i = 0, t = t->child; i < nb_args; i++, t = t->next)
	args[i] = s_interp_integer_expr (t, ctx, mai);

      result = s_crt_min_max (op, args, nb_args);
    }
  ccl_no_catch;

  for (i = 0; i < nb_args; i++)
    {
      if (args[i] != NULL)
	ar_expr_del_reference (args[i]);
      else 
	break;
    }
  ccl_delete (args);

  if (result == NULL)
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static ccl_list *
s_interp_quantified_variable_list (altarica_tree *t, ar_context *ctx,
				   ar_context **newctx,
				   ar_member_access_interp *mai)
{
  int error = 0;
  ccl_list *qvars = NULL;
  ccl_list *slots = ccl_list_create ();
  ar_type *domain = NULL;
  ccl_list *id_list = NULL;
  ccl_list *ids = ccl_list_create ();

  ccl_pre (IS_LABELLED_BY (t, QUANTIFIED_VARIABLE_LIST));

  ccl_try (altarica_interpretation_exception)
    {
      ccl_pair *p;

      for (t = t->child; t; t = t->next)
	{
	  if (t->child->next == NULL)
	    {
	      ccl_error ("type inference is no supported.\n");
	      INTERP_EXCEPTION ();	      
	    }
	  domain = ar_interp_domain (t->child->next,ctx,mai);
	  id_list = ar_interp_id_list (t->child, 1);

	  for(p = FIRST (id_list); p; p = CDR (p))
	    {
	      ar_identifier *id = (ar_identifier *) CAR (p);

	      if (ccl_list_has (ids, id)
		  /* || ar_context_has_slot (ctx, id)*/ )
		{
		  ccl_error ("%s:%d: error: quantified variable '",
			     t->filename, t->line);
		  ar_identifier_log (CCL_LOG_ERROR, id);
		  ccl_error ("' is duplicated or clashes with an existing "
			     "symbol.\n");
		  INTERP_EXCEPTION ();
		}	      
	      
	      ar_context_slot_expand (-1, id, domain, 0, NULL, NULL, slots);
	    }

	  ccl_list_append (ids, id_list);
	  ccl_list_delete (id_list);
	  ar_type_del_reference (domain);

	  domain = NULL;
	  id_list = NULL;
	}

      *newctx = ar_context_create_from_slots (ctx, slots);
      qvars = ccl_list_create ();
      for (p = FIRST (ids); p; p = CDR (p))
	{
	  ar_expr *var;
	  ar_identifier *varname = (ar_identifier *) CAR (p);
	  ar_context_slot *sl = ar_context_get_slot (*newctx, varname);

	  ccl_assert (sl != NULL);

	  var = ar_expr_crt_variable (sl);
	  ccl_list_add (qvars, var);
	  ar_context_slot_del_reference (sl);
	}
    }
  ccl_catch
    {
      error = 1;
    }
  ccl_end_try;

  ccl_zdelete (ar_type_del_reference, domain);
  if (id_list != NULL)
    ccl_list_clear_and_delete (id_list, (ccl_delete_proc *) 
			       ar_identifier_del_reference);
  ccl_list_clear_and_delete (ids, (ccl_delete_proc *) 
			     ar_identifier_del_reference);
  ccl_list_clear_and_delete (slots, (ccl_delete_proc *) 
			     ar_context_slot_del_reference);
			    
  if (error)
    INTERP_EXCEPTION ();

  return qvars;
}

			/* --------------- */

static ar_expr *
s_interp_quantified_formula(altarica_tree *t, ar_context *ctx,
			    ar_member_access_interp *mai)
{
  ar_expr_op op;
  ccl_list *qvars;
  ar_context *newctx = NULL;
  ar_expr *F = NULL;
  ar_expr *R = NULL;

  if( t->node_type == AR_TREE_EXIST ) op = AR_EXPR_EXIST;
  else op = AR_EXPR_FORALL;

  qvars = s_interp_quantified_variable_list(t->child,ctx,&newctx,mai);  

  ccl_try(altarica_interpretation_exception)
    {
      F = ar_interp_boolean_expression(t->child->next,newctx,mai);
      R = ar_expr_crt_quantifier(op,newctx,qvars,F);
    }
  ccl_no_catch;

  ccl_list_clear_and_delete(qvars,(ccl_delete_proc *)ar_expr_del_reference);
  ccl_zdelete(ar_expr_del_reference,F);
  ccl_zdelete(ar_context_del_reference,newctx);

  if( R == NULL )
    ccl_rethrow();

  return R;
}

			/* --------------- */

ar_identifier *
s_bang_id_to_id (altarica_tree *t)
{
  char *tmp = ccl_string_format_new ("%s!%s", t->child->value.id_value,
				     t->child->next->value.id_value);
  ar_identifier *result = ar_identifier_create (tmp);
  ccl_string_delete (tmp);
  
  return result;
}

			/* --------------- */

ar_identifier *
ar_interp_id_or_bang_id (altarica_tree *t, ar_context *ctx)
{
  ar_identifier *result = NULL;

  if (IS_LABELLED_BY (t, BANG_ID))
    {
      ar_identifier *nodename;

      result = s_bang_id_to_id (t);
      
      if (ar_model_has_signature (result))
	goto end;

      nodename = ar_interp_symbol_as_identifier (t->child);
      if (ar_model_has_node (nodename))
	{
	  ar_bang_t bang;
	  ccl_ustring bangid = t->child->next->value.id_value;

	  if (ar_bang_str_to_bang_t (bangid, &bang))
	    {
	      ar_node *n = ar_model_get_node (nodename);
	      
	      ccl_try (exception)
	        {
		  ar_mec5_build_bang_relation (n, bang);
		}
	      ccl_no_catch;
	      ar_node_del_reference (n);
	    }
	}
      ar_identifier_del_reference (nodename);
    }
  else
    {
      result = ar_interp_symbol_as_identifier(t);
    }

 end:
  return result;
}

			/* --------------- */

static ar_expr *
s_interp_function_call(altarica_tree *t, ar_type *type, ar_context *ctx,
		       ar_member_access_interp *mai)
{
  int error = 0;
  ar_expr **args;
  ar_expr *result = NULL;
  int i;
  altarica_tree *op = t->child->next;  
  int arity = ccl_parse_tree_count_siblings (op);
  ar_identifier *fname = ar_interp_id_or_bang_id (t->child, ctx);
  ar_signature *sig = ar_context_get_signature (ctx, fname);
  
  if (sig == NULL)
    {
      ar_error (t, "undefined function '");
      ar_identifier_log (CCL_LOG_ERROR, fname);
      ccl_error ("'.\n");      
      ar_identifier_del_reference (fname);

      INTERP_EXCEPTION ();
    }

  if(ar_signature_get_arity (sig) != arity)
    {
      const char *msg = "too few arguments to function '";

      if (ar_signature_get_arity (sig) < arity)
	msg = "too many arguments to function '";
      ar_error (t, msg);
      ar_identifier_log (CCL_LOG_ERROR, fname);
      ccl_error ("'.\n");      

      ar_signature_del_reference (sig);
      ar_identifier_del_reference (fname);
      INTERP_EXCEPTION ();
    }

  args = ccl_new_array (ar_expr *, arity + 1);

  for (i = 0; i < arity; i++, op = op->next)
    {
      ar_type *arg_type = ar_signature_get_ith_arg_domain (sig, i);

      ccl_try (altarica_interpretation_exception)
	{
	  args[i] = ar_interp_expression (op, arg_type, ctx, mai);
	}
      ccl_catch
	{
	  error = 1;
	}
      ccl_end_try;

      ar_type_del_reference (arg_type);
    }

  if (! error)
    result = ar_expr_crt_function_call (sig, args);
  ar_signature_del_reference (sig);
  ar_identifier_del_reference (fname);

  for (i = 0; i < arity; i++)
    ccl_zdelete (ar_expr_del_reference, args[i]);
  ccl_delete (args);

  if (error)
    {
      ccl_assert (result == NULL);
      INTERP_EXCEPTION ();
    }

  return result;
}

			/* --------------- */

static ar_expr *
s_crt_imply(ar_expr *a, ar_expr *b)
{
  ar_expr *nota = ar_expr_crt_unary(AR_EXPR_NOT,a);
  ar_expr *result = ar_expr_crt_binary(AR_EXPR_OR,nota,b);
  ar_expr_del_reference(nota);

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_expr_symbol (altarica_tree *t, ar_type *expected_type, ar_context *ctx)
{
  int enum_value;
  ar_context_slot *slot;
  ar_identifier *id = ar_interp_symbol_as_identifier(t);
  ar_signature *sig;
  ar_expr *result = NULL;

  if( (slot = ar_context_get_slot(ctx,id)) != NULL )
    {
      if( ar_context_slot_get_flags(slot) & AR_SLOT_FLAG_PARAMETER )
	{
	  struct ar_expr_st *val = ar_context_slot_get_value(slot);
	  if( val != NULL )
	    result = val;
	  else
	    result = ar_expr_crt_parameter(slot);
	}
      else
	result = ar_expr_crt_variable(slot);
      ar_context_slot_del_reference(slot);
    }
  else if( (sig = ar_context_get_signature (ctx, id)) != NULL )
    {
      if( ar_signature_get_arity(sig) == 0 )
	result = ar_expr_crt_function_call(sig,NULL);
      else
	{
	  ar_identifier_del_reference(id);
	  ar_signature_del_reference(sig);
	  INTERP_EXCEPTION_MSG((t, "bad use of the function symbol '%s'.\n",
				t->value.id_value));
	}
      ar_signature_del_reference(sig);
    }
  else if (expected_type != NULL && 
	   ar_type_get_kind (expected_type) == AR_TYPE_ENUMERATION)
    {
      int value = ar_type_enum_get_value_index (expected_type, id);
      if (value >= 0)
	{
	  ar_constant *cst = ar_constant_crt_symbol (value, expected_type);
	  result = ar_expr_crt_constant (cst);
	  ar_constant_del_reference(cst);      
	}
      else
	{
	  ar_error (t, "enum value '");
	  ar_identifier_log (CCL_LOG_ERROR, id);
	  ccl_error ("' does not belong to domain '");
	  ar_type_log (CCL_LOG_ERROR, expected_type);
	  ccl_error ("'.\n");
	  ar_identifier_del_reference(id);
	  INTERP_EXCEPTION ();
	}
    }
  else if ((enum_value = ar_type_symbol_set_get_value(id)) >= 0)
    {
      ar_constant *cst;
      ar_type *t = ar_type_crt_symbol_set ();

      ar_type_symbol_set_add_value (t, id);
      cst = ar_constant_crt_symbol (enum_value, t);
      result = ar_expr_crt_constant (cst);
      ar_constant_del_reference(cst);      
      ar_type_del_reference (t);
    }

  ar_identifier_del_reference(id);
  
  if (result == NULL)
    {
      INTERP_EXCEPTION_MSG((t, "undefined symbol '%s'.\n", t->value.id_value));
    }

  s_check_result_type (t, result, expected_type);

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_member_access(altarica_tree *t, ar_type *type, ar_context *ctx);

static ar_expr *
s_interp_struct_member(altarica_tree *t, ar_type *expected_type, 
		       ar_context *ctx)
{
  ar_expr *result;
  ar_type *ftype;
  ar_identifier *field;
  ar_expr *econt = s_interp_member_access (t->child, NULL, ctx);

  if( ! (ar_expr_is_struct(econt) || ar_expr_is_bang(econt)) )
    {
      ccl_error("%s:%d: error: ",t->filename,t->line);
      ar_expr_log(CCL_LOG_ERROR,econt);
      ccl_error(" is neither a structure nor a node.\n",t->filename,t->line);
      ar_expr_del_reference(econt);
      INTERP_EXCEPTION();
    }

  if (t->child->next == NULL)
    field = ar_identifier_create ("");
  else
    field = ar_interp_symbol_as_identifier(t->child->next);
  ftype = ar_expr_get_type(econt);

  if (!ar_type_struct_has_field (ftype, field))
    {
      ar_identifier_del_reference (field);
      ar_expr_del_reference (econt);
      ar_type_del_reference (ftype);
      INTERP_EXCEPTION_MSG ((t, "structure has no member called '%s'.\n", 
			     t->child->next->value.id_value));
    }
  
  result = ar_expr_crt_struct_member (econt, field);
  
  ar_expr_del_reference (econt);
  ar_type_del_reference (ftype);
  ar_identifier_del_reference (field);

  s_check_result_type (t, result, expected_type);
  
  return result;
}

			/* --------------- */

static ar_expr *
s_interp_array_element(altarica_tree *t, ar_type *expected_type, 
		       ar_context *ctx, ar_member_access_interp *mai)
{
  ar_expr *pos = NULL;
  ar_expr *result = NULL;
  ar_expr *array = s_interp_member_access (t->child, NULL, ctx);

  if (! ar_expr_is_array (array))
    {
      ar_error (t, "'");
      ar_expr_log (CCL_LOG_ERROR, array);
      ccl_error ("' is not an array.\n");
      ar_expr_del_reference (array);
      INTERP_EXCEPTION ();
    }

  ccl_try (altarica_interpretation_exception)
    {
      ar_expr *epos = s_interp_integer_expr (t->child->next,ctx,mai);

      if (ar_expr_is_computable (epos))
	{
	  ar_constant *cpos = ar_expr_eval(epos, ctx);
	  int ipos = ar_constant_get_integer_value(cpos);
	  ar_type *atype = ar_expr_get_type(array);
	  
	  ar_constant_del_reference(cpos);
	  
	  if( ipos < 0 || ipos >= ar_type_get_width(atype) )
	    {	 
	      ccl_error("\n%s:%d: error: array index (%d) out of bounds for '",
			t->filename,t->line,ipos);
	      ar_expr_log(CCL_LOG_ERROR,array);
	      ccl_error("'.\n");
	      
	      ar_type_del_reference(atype);
	      ar_expr_del_reference(epos);
	      INTERP_EXCEPTION();
	    }
	  ar_type_del_reference(atype);
	}
      else
	{
	  ar_error(t, "array index (");
	  ar_expr_log (CCL_LOG_ERROR, epos);
	  ccl_error (") is not computable.\n");
	  ar_expr_del_reference (epos);
	  INTERP_EXCEPTION ();
	}
      result = ar_expr_crt_binary (AR_EXPR_ARRAY_MEMBER, array, epos);

      ar_expr_del_reference(epos);
    }
  ccl_no_catch;

  ccl_zdelete(ar_expr_del_reference,pos);
  ccl_zdelete(ar_expr_del_reference,array);

  if( result == NULL )
    ccl_rethrow();

  s_check_result_type (t, result, expected_type);

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_member_access(altarica_tree *t, ar_type *type, ar_context *ctx)
{
  ar_expr *result;
  ar_expr *tmp = NULL;

  switch (t->node_type) 
    {
    case AR_TREE_IDENTIFIER : 
      tmp = s_interp_expr_symbol (t, type, ctx); 
      break;
    case AR_TREE_STRUCT_MEMBER : 
      tmp = s_interp_struct_member (t, type, ctx); 
      break;
    case AR_TREE_ARRAY_MEMBER : 
      tmp = s_interp_array_element (t, type, ctx, s_interp_member_access); 
      break;
    default : 
      INVALID_NODE_TYPE (); 
      break;
    }

  if (ar_expr_is_constant (tmp, ctx))
    result = ar_expr_add_reference (tmp);
  else if (1 || t->node_type != AR_TREE_ARRAY_MEMBER)
    {
      ar_context_slot *sl = ar_expr_eval_slot (tmp, ctx);
      result = ar_expr_crt_variable (sl);      
      ar_context_slot_del_reference (sl);
    }
  else
    result = ar_expr_add_reference (tmp);
  ar_expr_del_reference (tmp);

  return result;
}

			/* --------------- */

ar_expr *
ar_interp_member_access (altarica_tree *t, ar_type *type, ar_context *ctx)
  CCL_THROW (altarica_interp_exception)
{
  return s_interp_member_access (t, type, ctx);
}

			/* --------------- */

ar_expr *
ar_interp_acheck_member_access (altarica_tree *t, ar_type *type, 
				ar_context *ctx)
  CCL_THROW (altarica_interpretation_exception)
{
  int enum_value;
  ar_expr *result = NULL;
  ar_identifier *id = 
    ar_interp_member_access_as_identifier (t, ctx, 
					   ar_interp_acheck_member_access);
  ar_context_slot *sl = ar_context_get_slot (ctx, id);


  if (sl != NULL)
    {
      int flags = ar_context_slot_get_flags (sl);
      
      if (! (flags & (AR_SLOT_FLAG_PARAMETER | AR_SLOT_FLAG_STATE_VAR |
		      AR_SLOT_FLAG_FLOW_VAR)))
	{
	  ar_error (t, "invalid identifier type '");
	  ar_identifier_log (CCL_LOG_ERROR, id);
	  ccl_error ("'.\n");
	}
      else if ((flags & AR_SLOT_FLAG_PARAMETER))
	{
	  result = ar_expr_crt_parameter (sl);
	}
      else
	{
	  result = ar_expr_crt_variable (sl);
	}
      ar_context_slot_del_reference (sl);
    }
  else if ((enum_value = ar_type_symbol_set_get_value (id)) >= 0)
    {
      ar_constant *cst;
      ar_type *t = ar_type_crt_symbol_set ();

      ar_type_symbol_set_add_value (t, id);
      cst = ar_constant_crt_symbol (enum_value, t);
      result = ar_expr_crt_constant (cst);
      ar_constant_del_reference (cst);      
      ar_type_del_reference (t);
    }
  else
    {
      ar_error (t, "undefined symbol '");
      ar_identifier_log (CCL_LOG_ERROR, id);
      ccl_error ("'.\n");
    }
  ar_identifier_del_reference (id);

  if (result == NULL)
    INTERP_EXCEPTION ();
  else
    s_check_result_type (t, result, type);

  return result;
}

			/* --------------- */

ar_identifier *
ar_interp_member_access_as_identifier (altarica_tree *t, ar_context *ctx, 
				       ar_member_access_interp *mai)
  CCL_THROW (altarica_interp_exception)
{
  ar_identifier *result = NULL;

  switch (t->node_type) 
    {
    case AR_TREE_IDENTIFIER : 
      result = ar_interp_symbol_as_identifier (t);
      break;

    case AR_TREE_STRUCT_MEMBER : 
      {
	ar_identifier *st = 
	  ar_interp_member_access_as_identifier (t->child, ctx, mai);
	ar_identifier *field = ar_interp_symbol_as_identifier (t->child->next);
	result = ar_identifier_add_prefix (field, st);
	ar_identifier_del_reference (field);
	ar_identifier_del_reference (st);
      }
      break;
      
    case AR_TREE_ARRAY_MEMBER : 
      {
	int ipos = ar_interp_integer_value (t->child->next, ctx, mai);
	ar_identifier *st = 
	  ar_interp_member_access_as_identifier (t->child, ctx, mai);
	char *tmp = ccl_string_format_new ("[%d]", ipos);
	result = ar_identifier_rename_with_suffix (st, tmp);
	ccl_string_delete (tmp);
	ar_identifier_del_reference (st);
      }
      break;
      
    default : 
      INVALID_NODE_TYPE (); 
      break;
    }

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_struct_expr (altarica_tree *t, ar_type *expected_type, ar_context *ctx,
		      ar_member_access_interp *mai)
{
  int i;
  int width = ccl_parse_tree_count_siblings (t->child);
  ar_type *type = NULL;
  ar_identifier **fnames = ccl_new_array (ar_identifier *, width);
  ar_expr **fvals = ccl_new_array (ar_expr *, width);
  ar_expr *result = NULL;

  ccl_try (altarica_interpretation_exception)
    {
      int j;
      
      for (t = t->child, i = 0; t != NULL; t = t->next, i++)
	{
	  ccl_assert (t->node_type == AR_TREE_FIELD);
	  
	  fnames[i] = ar_interp_symbol_as_identifier (t->child);
	  fvals[i] = ar_interp_expression (t->child->next, NULL, ctx, mai);

	  for(j = 0; j < i; j++)
	    if (fnames[i] == fnames[j])
	      INTERP_EXCEPTION_MSG ((t, "structure field '%s' "
				     "assigned twice.\n",
				     t->child->value.id_value));
	}

      result = ar_expr_crt_structure (fnames, fvals, width);
      type = ar_expr_get_type (result);
      s_check_expected_type (t, type, expected_type);
    }
  ccl_catch
    {
      ccl_zdelete (ar_expr_del_reference, result);
      result = NULL;
    }
  ccl_end_try;

  
  ccl_zdelete (ar_type_del_reference, type);
  for(i = 0; i < width; i++)
    {
      ccl_zdelete(ar_identifier_del_reference,fnames[i]);
      ccl_zdelete(ar_expr_del_reference,fvals[i]);
    }
  ccl_delete(fnames);
  ccl_delete(fvals);

  if( result == NULL )
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static ar_expr *
s_interp_array_expr (altarica_tree *t, ar_type *type, ar_context *ctx,
		     ar_member_access_interp *mai)
{
  int i;
  int asize = ccl_parse_tree_count_siblings(t->child);
  ar_expr **elements;
  ar_type *base_type = NULL;
  ar_expr *result = NULL;

  if (type != NULL)
    {
      if (ar_type_array_get_size (type) != asize)
	INTERP_EXCEPTION_MSG ((t, "bad array size %d while %d was expected.\n",
			       asize, ar_type_array_get_size (type)));
      base_type = ar_type_array_get_base_type (type);
    }
  elements = ccl_new_array (ar_expr *, asize);
  
  ccl_try(altarica_interpretation_exception)
    {
      t = t->child;
      elements[0] = ar_interp_expression(t, base_type, ctx,mai);
      if (base_type == NULL)
	base_type = ar_expr_get_type(elements[0]);

      for(i = 1, t = t->next; t != NULL; t = t->next, i++)
	elements[i] = ar_interp_expression (t, base_type, ctx, mai);

      result = ar_expr_crt_array(elements,asize);
    }
  ccl_no_catch;

  for(i = 0; i < asize; i++)
    ccl_zdelete(ar_expr_del_reference,elements[i]);
  ccl_delete(elements);
  ccl_zdelete(ar_type_del_reference,base_type);

  if( result == NULL )
    ccl_rethrow();

  return result;
}

			/* --------------- */

static void
s_warn_non_intersecting_symbol_sets (altarica_tree *t, ar_type *type1, 
				     ar_type *type2)
{
  int inter_is_empty = 1;
  ccl_int_iterator *i;

  if (ar_type_get_kind (type1) == AR_TYPE_SYMBOLS || 
      ar_type_get_kind (type2) == AR_TYPE_SYMBOLS)
    return;

  i = ar_type_symbol_set_get_values (type1);
  while (ccl_iterator_has_more_elements (i) && inter_is_empty)
    {
      int v = ccl_iterator_next_element (i);
      inter_is_empty = ! ar_type_symbol_set_has_value (type2, v);
    }
  ccl_iterator_delete (i);

  if (inter_is_empty)
    {
      i = ar_type_symbol_set_get_values (type2);

      while (ccl_iterator_has_more_elements (i) && inter_is_empty)
	{
	  int v = ccl_iterator_next_element (i);
	  inter_is_empty = ! ar_type_symbol_set_has_value (type1, v);
	}
      ccl_iterator_delete (i);
    }
  
  if (inter_is_empty)
    {
      ar_warning (t, "comparison of domains that have an empty "
		  "intersection.\n");
      ar_warning (t, "domains are ");
      ar_type_log (CCL_LOG_WARNING, type1);
      ccl_warning ("\n");
      ar_warning (t, "and ");
      ar_type_log (CCL_LOG_WARNING, type2);
      ccl_warning ("\n");
    }
}

			/* --------------- */

ar_expr *
ar_interp_expression (altarica_tree *t, ar_type *expected_type, ar_context *ctx,
		      ar_member_access_interp *mai)
  CCL_THROW (altarica_interp_exception) 
{
  ar_constant *cst;
  ar_expr *arg1 = NULL;
  ar_expr *arg2 = NULL;
  ar_expr *result = NULL;
  ar_expr_op op = AR_EXPR_ITE;
  ar_type *type = NULL;

  ccl_try (altarica_interpretation_exception)
    {
      switch (t->node_type) 
	{
	case AR_TREE_ITE : 
	  result = s_interp_ite (t, expected_type, ctx, mai); 
	  break;

	case AR_TREE_CASE : 
	  result = s_interp_case (t, expected_type, ctx,mai); 
	  break;

	case AR_TREE_OR: case AR_TREE_AND: case AR_TREE_IMPLY: 
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  ccl_exception_local_variable (ar_expr_del_reference, &arg2);
	  arg2 = ar_interp_boolean_expression (t->child->next, ctx, mai);

	case AR_TREE_NOT :
	  arg1 = ar_interp_boolean_expression (t->child, ctx, mai);
	
	  if (t->node_type == AR_TREE_IMPLY) 
	    result = s_crt_imply (arg1, arg2); 
	  else if (t->node_type == AR_TREE_AND) 
	    result = ar_expr_crt_binary (AR_EXPR_AND, arg1, arg2);
	  else if (t->node_type == AR_TREE_OR) 
	    result = ar_expr_crt_binary (AR_EXPR_OR, arg1, arg2);
	  else 
	    {
	      ccl_exception_local_variable (ar_expr_del_reference, &arg1);
	      s_check_expected_type (t, AR_BOOLEANS, expected_type);
	      result = ar_expr_crt_unary (AR_EXPR_NOT, arg1);
	    }
	  break;

	case AR_TREE_EQ : case AR_TREE_NEQ :
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  arg1 = ar_interp_expression (t->child, NULL, ctx, mai);
	  ccl_exception_local_variable (ar_expr_del_reference, &arg1);
	  type = ar_expr_get_type (arg1);
	  ccl_exception_local_variable (ar_type_del_reference, &type);
	  arg2 = ar_interp_expression (t->child->next, type, ctx, mai);	
	  if (t->node_type == AR_TREE_EQ) 
	    op = AR_EXPR_EQ;
	  else
	    op = AR_EXPR_NEQ;
	  if (ar_type_get_kind (type) == AR_TYPE_SYMBOLS || 
	      ar_type_get_kind (type) == AR_TYPE_SYMBOL_SET)
	    {
	      ar_type *type2 = ar_expr_get_type (arg2);
	      s_warn_non_intersecting_symbol_sets (t, type, type2);
	      ar_type_del_reference (type2);
	    }
	  result = ar_expr_crt_binary (op, arg1, arg2);
	  break;

	case AR_TREE_LT : 
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  op = AR_EXPR_LT; 
	  goto eval;
	case AR_TREE_LEQ : 
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  op = AR_EXPR_LEQ; 
	  goto eval;
	case AR_TREE_GT : 
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  op = AR_EXPR_GT; 
	  goto eval;
	case AR_TREE_GEQ :
	  s_check_expected_type (t, AR_BOOLEANS, expected_type); 
	  op = AR_EXPR_GEQ; 
	  goto eval;
	case AR_TREE_ADD : 
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  op = AR_EXPR_ADD; 
	  goto eval;
	case AR_TREE_SUB : 
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  op = AR_EXPR_SUB; 
	  goto eval;
	case AR_TREE_MUL : 
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  op = AR_EXPR_MUL; 
	  goto eval;
	case AR_TREE_DIV : 
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  op = AR_EXPR_DIV; 
	  goto eval;
	case AR_TREE_MOD : 
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  op = AR_EXPR_MOD;
	eval :
	  arg2 = s_interp_integer_expr (t->child->next, ctx, mai);
	  ccl_exception_local_variable (ar_expr_del_reference, &arg2);
	case AR_TREE_NEG :
	  arg1 = s_interp_integer_expr (t->child, ctx, mai);
	  ccl_exception_local_variable (ar_expr_del_reference, &arg1);

	  if (t->node_type == AR_TREE_NEG) 
	    {
	      s_check_expected_type (t, AR_INTEGERS, expected_type);
	      result = ar_expr_crt_unary (AR_EXPR_NEG, arg1);
	    }
	  else
	    {
	      ccl_assert (op != AR_EXPR_ITE);
	      result = ar_expr_crt_binary (op, arg1, arg2);
	    }
	  break;

	case AR_TREE_PARENTHEZED_EXPR :
	  result = ar_interp_expression (t->child, expected_type, ctx, mai);
	  break;

	case AR_TREE_MIN : case AR_TREE_MAX :
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  result = s_interp_min_max (t, ctx, mai);
	  break;

	case AR_TREE_TRUE : case AR_TREE_FALSE : 
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  if( t->node_type == AR_TREE_TRUE )
	    result = ar_expr_crt_true();
	  else
	    result = ar_expr_crt_false();
	  break;

	case AR_TREE_INTEGER : 
	  s_check_expected_type (t, AR_INTEGERS, expected_type);
	  cst = ar_constant_crt_integer(t->value.int_value);
	  result = ar_expr_crt_constant(cst);
	  ar_constant_del_reference(cst);
	  break;

	case AR_TREE_EXIST: case AR_TREE_FORALL: 
	  s_check_expected_type (t, AR_BOOLEANS, expected_type);
	  result = s_interp_quantified_formula (t, ctx, mai);
	  break;
	
	case AR_TREE_FUNCTION_CALL: 
	case AR_TREE_BANG_ID: 
	  result = s_interp_function_call (t, expected_type, ctx, mai);
	  break;
	

	case AR_TREE_IDENTIFIER : 
	case AR_TREE_STRUCT_MEMBER :
	case AR_TREE_ARRAY_MEMBER :
	  result = mai (t, expected_type, ctx);
	  break;

	case AR_TREE_STRUCT_EXPR :
	  if (expected_type != NULL && 
	      ar_type_get_kind (expected_type) != AR_TYPE_STRUCTURE)
	    {
	      ar_error (t, "expecting type '");
	      ar_type_log (CCL_LOG_ERROR, expected_type);
	      ccl_error ("' and getting a structure.\n");
	      INTERP_EXCEPTION ();
	    }
	  result = s_interp_struct_expr (t, expected_type, ctx,mai);
	  break;

	case AR_TREE_ARRAY_EXPR :
	  if (expected_type != NULL && 
	      ar_type_get_kind (expected_type) != AR_TYPE_ARRAY)
	    {
	      ar_error (t, "expecting type '");
	      ar_type_log (CCL_LOG_ERROR, expected_type);
	      ccl_error ("' and getting an array.\n");
	      INTERP_EXCEPTION ();
	    }
	  result = s_interp_array_expr (t, expected_type, ctx,mai);
	  break;
	
	default: 
	  INVALID_NODE_TYPE (); 
	  break;
	}      
    }
  ccl_catch_rethrow;

  ccl_zdelete (ar_expr_del_reference, arg1);
  ccl_zdelete (ar_expr_del_reference, arg2);
  ccl_zdelete (ar_type_del_reference, type);

  ccl_assert (result != NULL);

  return result;
}

			/* --------------- */

int
ar_interp_integer_value(altarica_tree *t, ar_context *ctx,
			ar_member_access_interp *mai)
  CCL_THROW(altarica_interp_exception)
{
  ar_constant *cst = s_interp_integer_constant(t,ctx,mai);
  int result = ar_constant_get_integer_value(cst);

  ar_constant_del_reference(cst);

  return result;
}

			/* --------------- */

ar_expr *
ar_interp_boolean_expression(altarica_tree *t, ar_context *ctx,
			     ar_member_access_interp *mai)
  CCL_THROW(altarica_interp_exception)
{
  return ar_interp_expression (t, AR_BOOLEANS, ctx, mai);
}
