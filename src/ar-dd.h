/*
 * ar-dd.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_DD_H
# define AR_DD_H

# include <stdio.h>
# include <ccl/ccl-common.h>
# include <ccl/ccl-array.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-serializer.h>

typedef struct ar_decision_diagram_st *ar_dd;
typedef CCL_ARRAY(ar_dd) ar_dd_array;
typedef struct ar_decision_diagram_manager_st *ar_ddm;
typedef struct ar_decision_diagram_st **ar_ddtable;
typedef union  ar_decision_diagram_word_st *ar_ddword;

typedef unsigned char ar_ddcode;
enum { EXHAUSTIVE = 0, COMPACTED };

/*
 *
 * THE DD STRUCTURE AND ASSOCIATED MACROS
 *
 */
struct ar_decision_diagram_st {
  int32_t        refcount; /* 2 bytes */
  short         index;    /* 2 bytes */
  short         arity;    /* 2 bytes */
  ar_ddcode   encoding; /* 1 bytes */
  unsigned char flag;     /* 1 bytes */
  ar_ddtable  sons;     /* 4 bytes */
  ar_dd       next;     /* 4 bytes */
};                /* total = 4 words */

union ar_decision_diagram_word_st {
  ar_dd                               decision_diagram;
  struct { short first; short second; } shorts;
  ar_ddtable                          table;
  long                                  size;
  };

# define AR_DD_MAX_ARITY (0x7FFF)

/* manipulation of addresses */

# define AR_DD_POLARITY(F)   (CCL_PTRHASBIT(F))
# define AR_DD_ADDR(F)       (CCL_BITPTR2PTR(ar_dd,F))
# define AR_DD_PADDR(F,P)    ((P)?CCL_BITPTR(ar_dd,F):CCL_BITPTR2PTR(ar_dd,F))

/* access to fields */
# define AR_DD_INDEX(F)    (AR_DD_ADDR(F)->index)
# define AR_DD_ARITY(F)    (AR_DD_ADDR(F)->arity)
# define AR_DD_REFCOUNT(F) (AR_DD_ADDR(F)->refcount)
# define AR_DD_CODE(F)     (AR_DD_ADDR(F)->encoding)
# define AR_DD_FLAG(F)     (AR_DD_ADDR(F)->flag)
# define AR_DD_SONS(F)     (AR_DD_ADDR(F)->sons)
# define AR_DD_NEXT(F)     (AR_DD_ADDR(F)->next)

/* some boolean operations */
# define AR_DD_NOT(F) AR_DD_PADDR(AR_DD_ADDR(F),(1-AR_DD_POLARITY(F)))

/* table of sons manipulation */

# define AR_DD_TABLE_SIZE(arity,mode) (mode==EXHAUSTIVE ? (arity) : 2*(arity))

# define AR_DD_TADDR(table) CCL_BITPTR2PTR (ar_ddtable, table)
# define AR_DD_NTHWORD(table,n) \
  ((ar_ddword) (AR_DD_TADDR(table)+(n)))

# define AR_DD_PNTHSON(F,n) \
  (AR_DD_CODE (F) == EXHAUSTIVE ? AR_DD_EPNTHSON (F, n) : AR_DD_CPNTHSON (F, n))
# define AR_DD_NTHSON(F,n) \
  (AR_DD_CODE (F) == EXHAUSTIVE ? AR_DD_ENTHSON (F, n) : AR_DD_CNTHSON (F, n))

# define AR_DD_ENTHDD(table,n)   (*(AR_DD_TADDR(table)+(n)))
# define AR_DD_ENTHSON(F,n)      (AR_DD_ENTHDD(AR_DD_SONS(F),n))
# define AR_DD_EPNTHSON(F,n)     (AR_DD_POLARITY(F) \
                                    ? AR_DD_NOT(AR_DD_ENTHSON(F,n)) \
                                    : AR_DD_ENTHSON(F,n))

# define AR_DD_MIN_OFFSET_NTHDD(table,n) \
  (AR_DD_NTHWORD(table,(2*(n)))->shorts.first)
# define AR_DD_MAX_OFFSET_NTHDD(table,n) \
  (AR_DD_NTHWORD(table,(2*(n)))->shorts.second)

# define AR_DD_CNTHDD(table,n)   (*(AR_DD_TADDR(table)+2*(n)+1))
# define AR_DD_MIN_OFFSET_NTHSON(F,n) \
 (AR_DD_MIN_OFFSET_NTHDD(AR_DD_SONS(F),n))
# define AR_DD_MAX_OFFSET_NTHSON(F,n) \
 (AR_DD_MAX_OFFSET_NTHDD(AR_DD_SONS(F),n))
# define AR_DD_CNTHSON(F,n)      (AR_DD_CNTHDD(AR_DD_SONS(F),n))
# define AR_DD_CPNTHSON(F,n)     (AR_DD_POLARITY(F) \
                                    ? AR_DD_NOT(AR_DD_CNTHSON(F,n)) \
                                    : AR_DD_CNTHSON(F,n))


/* other operations */
# define ALTA_MAX_REFERENCES 0x7FFFFFFF

# define AR_DD_DUP(F) \
  (ccl_pre (AR_DD_REFCOUNT(F) >= 0), (( AR_DD_REFCOUNT(F)<ALTA_MAX_REFERENCES ? AR_DD_REFCOUNT(F)++, F : F )))

# define AR_DD_DUP_NOT(F) \
(ccl_pre (AR_DD_REFCOUNT(F) >= 0), ( AR_DD_REFCOUNT(F)<ALTA_MAX_REFERENCES ? \
				     AR_DD_REFCOUNT(F)++, AR_DD_NOT(F) : AR_DD_NOT(F) ))

# define AR_DD_FREE(F)   \
  do { ccl_pre (AR_DD_REFCOUNT(F) > 0); if(AR_DD_REFCOUNT(F)<ALTA_MAX_REFERENCES) AR_DD_REFCOUNT(F)--; } while (0)

# define AR_DD_DELETE_ARRAY(_a_) ccl_array_clean_and_delete(_a_, AR_DD_FREE)

extern ar_ddm DDM;

extern int
ar_dd_init (void);

extern void
ar_dd_terminate (void);

/*
 *
 * OPERATIONS ON THE DECISION DIAGRAM MANAGER
 *
 */
 
extern ar_ddm
ar_dd_create_manager(void);

extern void
ar_dd_delete_manager(ar_ddm ddm);

extern int
ar_dd_reset_manager(ar_ddm ddm);

extern int
ar_dd_enlarge_hashtables(ar_ddm ddm);

extern int
ar_dd_reduce_hashtables(ar_ddm ddm);

extern void
ar_dd_garbage_collection(ar_ddm ddm, int show_info);

extern int
ar_dd_get_garbage_collection_status (ar_ddm ddm);

extern void
ar_dd_set_garbage_collection_status (ar_ddm ddm, int on);

extern ar_dd
ar_dd_one(ar_ddm ddm);

extern ar_dd
ar_dd_dup_one(ar_ddm ddm);

extern ar_dd
ar_dd_zero(ar_ddm ddm);

extern ar_dd
ar_dd_dup_zero(ar_ddm ddm);

extern int
ar_dd_is_one (ar_ddm ddm, ar_dd dd);

extern int
ar_dd_is_zero (ar_ddm ddm, ar_dd dd);

/*
 *
 * OPERATIONS ON THE GLOBAL NODE HASTABLE
 *
 */
extern int
ar_dd_resize_hashtable(ar_ddm ddm, long new_size);

extern ar_dd 
ar_dd_find_or_add (ar_ddm ddm, int index, int arity, ar_ddcode encoding,
		   ar_ddtable sons);

/*
 *
 * OPERATIONS ON THE OPERATION CACHE
 *
 */

extern void
ar_dd_remove_foreign_memoization_records(ar_ddm ddm);

extern int
ar_dd_resize_cache(ar_ddm ddm, long new_size);

extern void
ar_dd_memoize_operation(ar_ddm ddm, void *F, void* G, void* H, void* R);

extern void
ar_dd_memoize_double_operation(ar_ddm ddm, void *F, void* G, void* H, 
			       double R);

extern void *
ar_dd_entry_for_operation(ar_ddm ddm, void *F, void* G, void* H);

/*
 *
 * BOOLEAN OPERATORS ON DDs
 *
 */

extern ar_dd 
ar_dd_dup_dd (ar_dd F);

extern void
ar_dd_free_dd (ar_dd F);

extern ar_dd 
ar_dd_compute_ite(ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H);

extern ar_dd
ar_dd_compute_and(ar_ddm ddm, ar_dd F,ar_dd G);

extern ar_dd
ar_dd_compute_and_3 (ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H);

extern ar_dd 
ar_dd_compute_or(ar_ddm ddm, ar_dd F,ar_dd G);

extern ar_dd 
ar_dd_compute_xor(ar_ddm ddm, ar_dd F,ar_dd G);

extern ar_dd 
ar_dd_compute_iff(ar_ddm ddm, ar_dd F,ar_dd G);

extern ar_dd 
ar_dd_compute_imply(ar_ddm ddm, ar_dd F,ar_dd G);

extern ar_dd 
ar_dd_compute_diff (ar_ddm ddm, ar_dd F, ar_dd G);

extern ar_dd
ar_dd_project_on_variable (ar_ddm ddm, ar_dd F, intptr_t v);

extern ar_dd
ar_dd_project_variable (ar_ddm ddm, ar_dd F, intptr_t v);

extern ar_dd
ar_dd_project_variable2 (ar_ddm ddm, ar_dd F, ar_dd replace);

extern ar_dd
ar_dd_project_variable3 (ar_ddm ddm, ar_dd F, ar_dd st);

extern void
ar_dd_assign_bin_op (ar_ddm ddm, ar_dd (*op)(ar_ddm ddm, ar_dd F, ar_dd G),
		     ar_dd *pF, ar_dd G);

/*
 *
 * MISCELLANEOUS OPERATIONS ON DDs
 *
 */

extern void
ar_dd_initialize_decision_diagram_flag(ar_dd F);

extern long
ar_dd_get_number_of_nodes (ar_dd F);

extern long
ar_dd_get_size_in_bytes (ar_dd F);

extern ar_ddtable
ar_dd_allocate_dd_table(ar_ddm ddm, int arity, ar_ddcode encoding);

extern void
ar_dd_free_dd_table (ar_ddm ddm, int arity, ar_ddcode encoding,
		     ar_ddtable T);

extern ar_dd
ar_dd_var_in_range (ar_ddm ddm, int index, int arity, ar_ddcode encoding, 
		    int min, int max);


/*
 *
 * DISPLAYING INFORMATIONS ABOUT DDs
 *
 */

extern long
ar_dd_relative_address(ar_ddm ddm, ar_dd F);

extern void
ar_dd_display_decision_diagram_node(ar_ddm ddm, ar_dd F, FILE *out);

extern void
ar_dd_display_decision_diagram_nodes(ar_ddm ddm, ar_dd F, FILE *output);

extern void
ar_dd_display_decision_diagram_nodes_as_dot (ar_ddm ddm, ar_dd F,
					     FILE *output);

extern void
ar_dd_display_decision_diagram_nodes_as_dot2 (ar_ddm ddm, ar_dd F, 
					      ccl_log_type output,
					      int only_assignments);

extern void
ar_dd_display_decision_diagram_pages(ar_ddm ddm, 
				     FILE* output, long from, long to);

extern void
ar_dd_display_statistics_about_decision_diagrams(ar_ddm ddm, ccl_log_type log);


extern ar_dd
ar_dd_shift_variables (ar_ddm ddm, ar_dd X, int offset);

extern void
ar_dd_write_node (ar_ddm ddm, ar_dd dd, FILE *out, 
		  ccl_serializer_status *p_err);

extern void
ar_dd_read_node (ar_ddm ddm, ar_dd *p_dd, FILE *in, 
		 ccl_serializer_status *p_err);

			/* --------------- */

extern ar_dd
ar_dd_create_projection_list (ar_ddm ddm);

extern void
ar_dd_projection_list_add (ar_ddm ddm, ar_dd *list, int index, int newindex);

extern void
ar_dd_projection_list_merge (ar_ddm ddm, ar_dd *list, ar_dd other);

extern int
ar_dd_projection_list_get_new_index (ar_dd l);

extern ar_dd
ar_dd_projection_list_get_tail (ar_dd l);

extern ar_dd
ar_dd_project_on_variables (ar_ddm ddm, ar_dd F, ar_dd varlist);

extern ar_dd
ar_dd_project_variables (ar_ddm ddm, ar_dd F, ar_dd varlist);

extern ar_dd
ar_dd_get_variables (ar_ddm ddm, ar_dd F);

			/* --------------- */

extern ar_dd
ar_dd_create_card_list (ar_ddm ddm);

extern void 
ar_dd_card_list_add (ar_ddm ddm, ar_dd *list, int index, 
		     ar_ddcode encoding, int card);

extern ar_ddcode
ar_dd_card_list_get_encoding (ar_dd l);

extern int
ar_dd_card_list_get_card (ar_dd l);

extern ar_dd
ar_dd_card_list_get_tail (ar_dd l);

extern double
ar_dd_cardinality (ar_ddm ddm, ar_dd F, ar_dd cardlist);

extern ar_dd
ar_dd_pick (ar_ddm ddm, ar_dd F, ar_dd cardlist);

			/* --------------- */

extern int
ar_dd_is_included_in (ar_ddm ddm, ar_dd X, ar_dd Y);

extern int
ar_dd_intersect (ar_ddm ddm, ar_dd X, ar_dd Y);

extern void
ar_dd_display_node_map (ar_ddm ddm, FILE *out);

extern void
ar_dd_display_cache_map (ar_ddm ddm, FILE *out);

extern uintptr_t
ar_dd_get_number_of_paths (ar_ddm ddm, ar_dd dd);

extern ar_dd
ar_dd_pick_one_path (ar_ddm ddm, ar_dd F);

extern ar_dd 
ar_dd_pick_min_path (ar_ddm ddm, ar_dd F, int *D);

extern ar_dd 
ar_dd_pick_prime (ar_ddm ddm, ar_dd *pF);

extern ar_dd 
ar_dd_pick_cut (ar_ddm ddm, ar_dd *pF);

#endif /* ! AR_DD_H */
