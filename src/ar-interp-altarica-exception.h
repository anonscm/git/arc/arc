/*
 * ar-interp-altarica-exception.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_INTERP_ALTARICA_EXCEPTION_H
# define AR_INTERP_ALTARICA_EXCEPTION_H

# include <ccl/ccl-exception.h>

CCL_DECLARE_EXCEPTION(altarica_interpretation_exception,exception);

#endif /* ! AR_INTERP_ALTARICA_EXCEPTION_H */
