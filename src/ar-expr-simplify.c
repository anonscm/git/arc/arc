/*
 * ar-expr-simplify.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-dd.h"
#include "ar-attributes.h"
#include "ar-expr-p.h"

static ar_expr *
s_dd_to_expr(ar_ddm ddm, ar_dd dd, ccl_list *slots);

static ar_dd
s_expr_to_dd(ar_ddm ddm, ar_expr *e, ccl_list *slots, ccl_hash *cache);

static ar_dd
s_quantify_dd(ar_ddm ddm, ar_expr_op op, int var, ar_dd F);

static ar_dd
s_enumerate_assignments(ar_ddm ddm, ar_expr *e);

static ccl_list *
s_collect_slots(ar_expr *e);

static void
s_delete_dd (void *dd);


			/* --------------- */

int
ar_expr_is_simplifiable (ar_expr *e)
{
  if( e->op == AR_EXPR_VAR || e->op == AR_EXPR_PARAM )
    return (ar_type_get_cardinality (e->type) >= 0);
  else if( e->op == AR_EXPR_CST )
    return 1;
  else
    {
      switch (e->op) 
	{
	case AR_EXPR_NOT :
	case AR_EXPR_OR: case AR_EXPR_AND: 
	case AR_EXPR_EQ: case AR_EXPR_NEQ:
	case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_LT: case AR_EXPR_GT:
	case AR_EXPR_ITE :
	case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL: case AR_EXPR_DIV: 
	case AR_EXPR_MOD: case AR_EXPR_NEG: 
	case AR_EXPR_MIN: case AR_EXPR_MAX:
	  {
	    int i;

	    for(i = 0; i < e->arity; i++)
	      {
		if (! ar_expr_is_simplifiable (e->args[i]))
		  return 0;
	      }
	  }
	break;

	case AR_EXPR_EXIST: case AR_EXPR_FORALL:
	  if (! ar_expr_is_simplifiable (e->args[0]))
	    return 0;
	  else
	    {
	      ccl_pair *p;

	      for(p = FIRST(e->u.qvars); p; p = CDR(p))
		{
		  ar_expr *var = (ar_expr *)CAR(p);
		  if (! ar_expr_is_simplifiable (var))
		    return 0;
		}
	    }
	  break;

	case AR_EXPR_CALL: case AR_EXPR_ARRAY: 
	case AR_EXPR_STRUCT: case AR_EXPR_STRUCT_MEMBER: 
	case AR_EXPR_ARRAY_MEMBER:
	  return 0;
	default:
	  ccl_throw_no_msg (internal_error);
      }
    }

  return 1;
}

static ar_expr *
s_simplify (ar_expr *e, ccl_hash *ecache)
{
  ar_expr *result = NULL;

  ccl_pre( e != NULL );

  if (ccl_hash_find (ecache, e))
    {
      result = ccl_hash_get (ecache);
      result = ar_expr_add_reference (result);
      return result;
    }

  switch( e->op ) {
  case AR_EXPR_NOT: case AR_EXPR_NEG:
  case AR_EXPR_ITE: 
  case AR_EXPR_OR: case AR_EXPR_AND:
  case AR_EXPR_EQ: case AR_EXPR_NEQ:
  case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ: case AR_EXPR_GEQ:
  case AR_EXPR_ADD: case AR_EXPR_SUB: 
  case AR_EXPR_MUL: case AR_EXPR_DIV: case AR_EXPR_MOD:
  case AR_EXPR_MIN: case AR_EXPR_MAX: 
    {
      int i, arity, changed = 0;
      ar_expr_op op = ar_expr_get_op (e);
      ar_expr **args = ar_expr_get_args (e, &arity);
      ar_expr **ops = ccl_new_array (ar_expr *, arity);

      for (i = 0; i < arity; i++) 
	{
	  ops[i] = s_simplify (args[i], ecache);
	  changed = changed || (ops[i] != args[i]);
	}

      if (changed)
	{
	  if (arity == 1) 
	    result = ar_expr_crt_unary (op, ops[0]);
	  else if (arity == 2) 
	    result = ar_expr_crt_binary (op, ops[0], ops[1]);
	  else if (arity == 3) 
	    result = ar_expr_crt_ternary (op, ops[0], ops[1], ops[2]);
	  else if (op == AR_EXPR_MIN)
	    result = ar_expr_crt_min (arity, ops);
	  else if (op == AR_EXPR_MAX)
	    result = ar_expr_crt_max (arity, ops);
	  else if (op == AR_EXPR_AND)
	    result = ar_expr_crt_and (arity, ops);
	  else 
	    {
	      ccl_assert (op == AR_EXPR_OR);
	      result = ar_expr_crt_or (arity, ops);
	    }
	}
      else
	{
	  result = ar_expr_add_reference (e);
	}
      for (i = 0; i < arity; i++) 
	ar_expr_del_reference (ops[i]);      
      ccl_delete (ops);
    }
    break;

  case AR_EXPR_EXIST: case AR_EXPR_FORALL: 
    {
      ccl_pair *p;
      ar_ddm ddm = DDM;
      ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, s_delete_dd);
      ccl_list *slots = s_collect_slots(e);      
      ar_dd dd;
      int i;
      for(i = 0, p = FIRST(slots); p; p = CDR(p), i++)
	{
	  ar_context_slot_set_index (CAR(p), i);
	  if (ccl_debug_is_on)
	    {
	      ar_identifier *id = ar_context_slot_get_name (CAR (p));
	      ar_identifier_log (CCL_LOG_DEBUG, id);
	      ccl_debug ("-> %d\n", i);
	      ar_identifier_del_reference (id);
	    }
	}
      dd = s_expr_to_dd(ddm,e,slots, cache); 
      result = s_dd_to_expr(ddm,dd,slots);
      for(p = FIRST(slots); p; p = CDR(p))
	ar_context_slot_set_index(CAR(p),-1);
      AR_DD_FREE(dd);
      ccl_list_delete(slots);
      ccl_hash_delete (cache);
    }
    break;

  case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
    ccl_warning("warning: unexpected call to ar_expr_simplify on member "
		"access.\n");
  case AR_EXPR_CST: case AR_EXPR_VAR: case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:
  case AR_EXPR_PARAM:
    result = ar_expr_add_reference(e);
    break;

  case AR_EXPR_CALL:
    {
      int i;
      ar_expr **args = ccl_new_array(ar_expr *,e->arity);

      for(i = 0; i < e->arity; i++)
	args[i] = s_simplify(e->args[i], ecache);
      result = ar_expr_crt_function_call(e->u.sig,args);
      for(i = 0; i < e->arity; i++)
	ar_expr_del_reference(args[i]);
      ccl_delete(args);      
    }
    break;
  }

  ccl_post( result != NULL );

  ccl_hash_find (ecache, e);
  ccl_hash_insert (ecache, ar_expr_add_reference (result));

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_simplify_with_cache (ar_expr *e, ccl_hash *cache)
{
  return s_simplify (e, cache);
}

ar_expr *
ar_expr_simplify (ar_expr *e)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				     ar_expr_del_reference);
  
  ar_expr *result = s_simplify (e, cache);
  ccl_hash_delete (cache);

  return result;
}

			/* --------------- */

static ar_expr *
s_type_get_value (ar_type *type, int i)
{
  ar_expr *result = NULL;
  ar_type_kind kind = ar_type_get_kind (type);

  switch (kind) 
    {
    case AR_TYPE_BOOLEANS:
      if (i == 0) 
	result = ar_expr_crt_false ();
      else 
	result = ar_expr_crt_true ();
      break;
      
    case AR_TYPE_RANGE:
      result = ar_expr_crt_integer (i + ar_type_range_get_min (type));
      break;

    case AR_TYPE_ENUMERATION:      
    case AR_TYPE_SYMBOL_SET:
      {
	ar_constant *cst = NULL;

	if (kind == AR_TYPE_ENUMERATION)
	  {
	    ccl_assert (0 <= i && i < ar_type_get_cardinality (type));
	    cst = ar_constant_crt_symbol (i, type);
	  }
	else
	  {
	    int n = 0;
	    ccl_int_iterator *ii = ar_type_symbol_set_get_values (type);
	    
	    while (cst == NULL)
	      {
		int v = ccl_iterator_next_element (ii);
		if (n == i)
		  cst = ar_constant_crt_symbol (v, type);
		else 
		  n++;
	      }
	    ccl_assert (cst != NULL);
	    ccl_iterator_delete (ii);	
	  }
	result = ar_expr_crt_constant (cst);
	ar_constant_del_reference (cst);
      }
      break;
      
    case AR_TYPE_INTEGERS: case AR_TYPE_SYMBOLS: case AR_TYPE_ABSTRACT:
    case AR_TYPE_ARRAY: case AR_TYPE_STRUCTURE: case AR_TYPE_BANG:
      ccl_throw_no_msg (internal_error);
  }

  return result;
}

			/* --------------- */

static ar_expr *
s_dd_to_expr(ar_ddm ddm, ar_dd Fdd, ccl_list *slots)
{
  ar_expr *result = NULL;
  ar_dd ONE = ar_dd_one(ddm);
  ar_dd ZERO = ar_dd_zero(ddm);

  if( Fdd == ONE ) result = ar_expr_crt_true();
  else if( Fdd == ZERO ) result = ar_expr_crt_false();
  else
    {
      int index = AR_DD_INDEX(Fdd);
      int arity = AR_DD_ARITY(Fdd);
      ar_context_slot *slot = ccl_list_get_at(slots,index);
      ar_expr *var;
      ar_type *dom = ar_context_slot_get_type(slot);

      if( (ar_context_slot_get_flags(slot) & AR_SLOT_FLAG_PARAMETER) )
	var = ar_expr_crt_parameter(slot);
      else
	var = ar_expr_crt_variable(slot);

      ccl_assert( ar_type_is_scalar(dom) );

      if( dom == AR_BOOLEANS )
	{
	  if (AR_DD_EPNTHSON (Fdd, 0) == ZERO)
	    { /* AND */
	      ar_expr *F1 = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,1),slots);
	      ccl_assert( arity == 2 );
	      result = ar_expr_crt_binary (AR_EXPR_AND, var, F1);
	      ar_expr_del_reference(F1);
	    }
	  else if (AR_DD_EPNTHSON (Fdd, 1) == ONE)
	    {
	      /* OR */
	      ar_expr *F0 = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,0),slots);
	      ccl_assert( arity == 2 );
	      result = ar_expr_crt_binary (AR_EXPR_OR, var, F0);
	      ar_expr_del_reference(F0);
	    }
	  else if (AR_DD_EPNTHSON (Fdd, 0) == ONE)
	    {
	      /* imply */
	      ar_expr *F1 = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,1),slots);
	      ccl_assert( arity == 2 );
	      ar_expr *tmp = ar_expr_crt_unary (AR_EXPR_NOT, var);
	      result = ar_expr_crt_binary (AR_EXPR_OR, tmp, F1);
	      ar_expr_del_reference (tmp);
	      ar_expr_del_reference(F1);
	    }
	  else if (AR_DD_EPNTHSON (Fdd, 1) == AR_DD_NOT (AR_DD_EPNTHSON (Fdd, 0)))
	    {
	      ar_expr *F1 = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,1),slots);
	      ccl_assert( arity == 2 );
	      result = ar_expr_crt_binary (AR_EXPR_EQ, var, F1);
	      ar_expr_del_reference(F1);
	    }
	  else
	    {
	      ar_expr *F0 = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,0),slots);
	      ar_expr *F1 = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,1),slots);
	      ccl_assert( arity == 2 );
	      result = ar_expr_crt_ternary(AR_EXPR_ITE,var,F1,F0);
	      ar_expr_del_reference(F0);
	      ar_expr_del_reference(F1);
	    }
	}
      else if( AR_DD_CODE(Fdd) == COMPACTED )
	{
	  int i;

	  if (ar_type_have_same_base_type(dom, AR_INTEGERS))
	    {
	      for(i = 0; i < arity; i++) 
		{
		  ar_expr *tmp[3]; 
		  int min = AR_DD_MIN_OFFSET_NTHSON (Fdd, i);
		  int max = AR_DD_MAX_OFFSET_NTHSON (Fdd, i);

		  if (min == max)
		    {
		      tmp[1] = s_type_get_value (dom, min);
		      tmp[0] = ar_expr_crt_binary (AR_EXPR_EQ, var, tmp[1]);
		      ar_expr_del_reference (tmp[1]);
		    }
		  else
		    {
		      tmp[0] = s_type_get_value (dom, min);
		      tmp[1] = ar_expr_crt_binary (AR_EXPR_LEQ, tmp[0], var);
		      ar_expr_del_reference (tmp[0]);

		      tmp[0] = s_type_get_value (dom, max);
		      tmp[2] = ar_expr_crt_binary (AR_EXPR_LEQ, var, tmp[0]);
		      ar_expr_del_reference (tmp[0]);

		      tmp[0] = ar_expr_crt_binary (AR_EXPR_AND, tmp[1], tmp[2]);
		      ar_expr_del_reference (tmp[1]);
		      ar_expr_del_reference (tmp[2]);
		    }

		  tmp[1] = s_dd_to_expr (ddm, AR_DD_CPNTHSON (Fdd, i), slots);
		  tmp[2] = ar_expr_crt_binary (AR_EXPR_AND, tmp[0], tmp[1]);
		  ar_expr_del_reference (tmp[0]);
		  ar_expr_del_reference (tmp[1]);

		  if (i == 0) 
		    result = tmp[2];
		  else
		    {
		      tmp[0] = ar_expr_crt_binary (AR_EXPR_OR, result, tmp[2]);
		      ar_expr_del_reference (result);
		      ar_expr_del_reference (tmp[2]);
		      result = tmp[0];
		    }
		}
	    }
	  else
	    {
	      for(i = 0; i < arity; i++) 
		{
		  int j;
		  ar_expr *eq;
		  ar_expr *aux2;
		  int min = AR_DD_MIN_OFFSET_NTHSON(Fdd,i);
		  int max = AR_DD_MAX_OFFSET_NTHSON(Fdd,i);
		  ar_expr *aux = s_type_get_value(dom,min);
		  ar_expr *tmp = ar_expr_crt_binary(AR_EXPR_EQ,var,aux);
		  ar_expr_del_reference(aux);

		  for(j = min+1; j <= max; j++) 
		    {		  
		      aux = s_type_get_value(dom,j);
		      eq = ar_expr_crt_binary(AR_EXPR_EQ,var,aux);
		      ar_expr_del_reference(aux);
		      aux = ar_expr_crt_binary(AR_EXPR_OR,tmp,eq);
		      ar_expr_del_reference(tmp);
		      ar_expr_del_reference(eq);
		      tmp = aux;
		    }

		  aux2 = s_dd_to_expr(ddm,AR_DD_CPNTHSON(Fdd,i),slots);
		  aux = ar_expr_crt_binary(AR_EXPR_AND,tmp,aux2);
		  ar_expr_del_reference(aux2);
		  ar_expr_del_reference(tmp);
		  tmp = aux;

		  if( i == 0 ) result = tmp;
		  else
		    {
		      aux = ar_expr_crt_binary(AR_EXPR_OR,result,tmp);
		      ar_expr_del_reference(result);
		      ar_expr_del_reference(tmp);
		      result = aux;
		    }
		}
	    }
	}
      else
	{
	  int i;
	  for(i = 0; i < arity; i++) 
	    {
	      ar_expr *aux2;
	      ar_expr *aux = s_type_get_value(dom,i);
	      ar_expr *tmp = ar_expr_crt_binary(AR_EXPR_EQ,var,aux);
	      ar_expr_del_reference(aux);
	      aux = s_dd_to_expr(ddm,AR_DD_EPNTHSON(Fdd,i),slots);
	      aux2 = ar_expr_crt_binary(AR_EXPR_AND,tmp,aux);
	      ar_expr_del_reference(tmp);
	      ar_expr_del_reference(aux);
	      tmp = aux2;

	      if( i == 0 ) result = tmp;
	      else
		{
		  aux = ar_expr_crt_binary(AR_EXPR_OR,result,tmp);
		  ar_expr_del_reference(tmp);
		  ar_expr_del_reference(result);
		  result = aux;
		}
	    }
	}

      ar_expr_del_reference(var);
      ar_type_del_reference(dom);
    }

  ccl_post( result != NULL );


  return result;
}

			/* --------------- */

static ar_dd
s_expr_to_dd(ar_ddm ddm, ar_expr *e, ccl_list *slots, ccl_hash *cache)
{
  ar_dd result = NULL;
  ar_dd op1;
  ar_dd op2;
  ar_dd op3;

  ccl_assert( ar_expr_has_base_type(e,AR_BOOLEANS) );

  if (ccl_hash_find (cache, e))
    {
      result = ccl_hash_get (cache);
      result = AR_DD_DUP (result);
      return result;
    }

  if( e->op == AR_EXPR_VAR )
    {
      int arity = 2;
      ar_ddtable sons = ar_dd_allocate_dd_table(ddm,arity,EXHAUSTIVE);
      int index = ar_context_slot_get_index(e->u.slot);

      ccl_assert( index >= 0 );

      AR_DD_ENTHDD(sons,0) = AR_DD_DUP(ar_dd_zero(ddm));
      AR_DD_ENTHDD(sons,1) = AR_DD_DUP(ar_dd_one(ddm));
      result = ar_dd_find_or_add(ddm,index,arity,EXHAUSTIVE,sons);
      ar_dd_free_dd_table(ddm,arity,EXHAUSTIVE,sons);
    }
  else if( e->op == AR_EXPR_CST )
    {
      if( ar_constant_get_boolean_value(e->u.cst) )
	result = ar_dd_one(ddm);
      else
	result = ar_dd_zero(ddm);
      result = AR_DD_DUP(result);
    }
  else 
    {
      op1 = op2 = op3 = NULL;
            
      switch( e->op ) {
      case AR_EXPR_NOT :
	op1 = s_expr_to_dd(ddm,e->args[0],slots,cache);
	result = AR_DD_DUP_NOT(op1);
	break;

      case AR_EXPR_OR: case AR_EXPR_AND: 
	result = s_expr_to_dd(ddm,e->args[0],slots,cache);
	{
	  int i;

	  for(i = 1; i < e->arity; i++)
	    {
	      op1 = s_expr_to_dd(ddm,e->args[i],slots,cache);
	      if( e->op == AR_EXPR_OR )
		op2 = ar_dd_compute_or(ddm,result,op1);
	      else
		op2 = ar_dd_compute_and(ddm,result,op1);
	      AR_DD_FREE(op1);
	      AR_DD_FREE(result);
	      result = op2;
	      op1 = op2 = NULL;
	    }	  
	}
	break;
	
      case AR_EXPR_EQ: case AR_EXPR_NEQ:
	if( ar_expr_has_base_type(e->args[0],AR_BOOLEANS) )
	  {
	    op1 = s_expr_to_dd(ddm,e->args[0],slots,cache);
	    op2 = s_expr_to_dd(ddm,e->args[1],slots,cache);
	    if( e->op == AR_EXPR_EQ ) 
	      result = ar_dd_compute_iff(ddm,op1,op2);
	    else
	      result = ar_dd_compute_xor(ddm,op1,op2);
	  }
	else 
	  {
	    result = s_enumerate_assignments(ddm,e);
	  }
	break;

      case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_LT: case AR_EXPR_GT:
	result = s_enumerate_assignments(ddm,e);
	break;


      case AR_EXPR_ITE :
	if( ar_expr_has_base_type(e->args[0],AR_BOOLEANS) )
	  {
	    op1 = s_expr_to_dd(ddm,e->args[0],slots,cache);
	    op2 = s_expr_to_dd(ddm,e->args[1],slots,cache);
	    op3 = s_expr_to_dd(ddm,e->args[2],slots,cache);
	    result = ar_dd_compute_ite(ddm,op1,op2,op3);
	  }
	else 
	  {
	    result = s_enumerate_assignments(ddm,e);
	  }
	break;

      case AR_EXPR_EXIST: case AR_EXPR_FORALL:
	result = s_expr_to_dd(ddm,e->args[0],slots,cache);
	{
	  ccl_pair *p;

	  for(p = FIRST(e->u.qvars); p; p = CDR(p))
	    {
	      ar_expr *var = (ar_expr *)CAR(p);
	      ccl_list *slots = 
		ar_context_expand_composite_slot (e->qctx, var->u.slot, NULL);

	      while (! ccl_list_is_empty (slots))
		{
		  ar_context_slot *sl = ccl_list_take_first (slots);
		  int index = ar_context_slot_get_index (sl);

		  if( index >= 0 )
		    {
		      ar_dd tmp = s_quantify_dd (ddm, e->op, index, result);
		      AR_DD_FREE (result);
		      result = tmp;
		    }
		  ar_context_slot_del_reference (sl);
		}
	      ccl_list_delete (slots);
	    }
	}
	break;
	
      case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL: case AR_EXPR_DIV: 
      case AR_EXPR_MOD: case AR_EXPR_NEG: case AR_EXPR_CST: case AR_EXPR_VAR: 
      case AR_EXPR_CALL: case AR_EXPR_PARAM: case AR_EXPR_ARRAY: 
      case AR_EXPR_STRUCT: case AR_EXPR_STRUCT_MEMBER: 
      case AR_EXPR_ARRAY_MEMBER:
      case AR_EXPR_MIN: case AR_EXPR_MAX:
	ccl_throw_no_msg(internal_error);
      }

      ccl_zdelete(AR_DD_FREE,op1);
      ccl_zdelete(AR_DD_FREE,op2);
      ccl_zdelete(AR_DD_FREE,op3);
    }

  ccl_hash_find (cache, e);
  ccl_hash_insert (cache, AR_DD_DUP (result));

  ccl_post( result != NULL );

  return result;
}

			/* --------------- */

static ar_dd
s_quantify_dd(ar_ddm ddm, ar_expr_op q, int var, ar_dd F)
{
  ar_dd R = NULL;
  if (q == AR_EXPR_FORALL)
    {
      R  = ar_dd_project_variable (ddm, AR_DD_NOT (F), var);
      R = AR_DD_NOT (R);
    }
  else
    {
      R  = ar_dd_project_variable (ddm, F, var);
    }
  return R;
}

			/* --------------- */

static ar_dd
s_enumerate_assignments_rec(ar_ddm ddm, ar_expr *F, ccl_pair *p)
{
  ar_dd R = NULL;
  ar_ddtable sons;
  int i, arity, index;
  ar_context_slot *var;
  ar_ddcode encoding;
  ar_type *domain;

  if( p == NULL ) 
    {
      ar_constant *cst = ar_expr_eval(F, NULL);
      if( ar_constant_get_boolean_value(cst) ) R = ar_dd_one(ddm);
      else R = ar_dd_zero(ddm);
      R = AR_DD_DUP(R);
      ar_constant_del_reference(cst);
    }
  else
    {
      var = (ar_context_slot *)CAR(p);
      domain = ar_context_slot_get_type(var);
      arity = ar_type_get_cardinality(domain);
      index = ar_context_slot_get_index(var);
      encoding = ((arity > 2)?COMPACTED:EXHAUSTIVE);
      sons = ar_dd_allocate_dd_table(ddm,arity,encoding);

      if( encoding == COMPACTED ) 
	{
	  for(i = 0; i < arity; i++)
	    {
	      ar_expr *V = s_type_get_value(domain,i);
	      ar_context_slot_set_value(var,V);
	      ar_expr_del_reference(V);

	      AR_DD_MIN_OFFSET_NTHDD(sons,i) = i;
	      AR_DD_MAX_OFFSET_NTHDD(sons,i) = i;
	      AR_DD_CNTHDD(sons,i) = s_enumerate_assignments_rec(ddm,F,CDR(p));
	    }
	}
      else
	{
	  for(i = 0; i < arity; i++)
	    {
	      ar_expr *V = s_type_get_value(domain,i);
	      ar_context_slot_set_value(var,V);
	      ar_expr_del_reference(V);
	      AR_DD_ENTHDD(sons,i) = s_enumerate_assignments_rec(ddm,F,CDR(p));
	    }
	}
      R = ar_dd_find_or_add(ddm,index,arity,encoding,sons);
      ar_dd_free_dd_table(ddm,arity,encoding,sons);

      ar_type_del_reference(domain);
    }

  ccl_post( R != NULL );

  return R;
}

			/* --------------- */


static int
s_cmp_slots(const void *s1, const void *s2)
{
  const ar_context_slot *sl1 = s1;
  const ar_context_slot *sl2 = s2;

  return ar_context_slot_get_index(sl1)-ar_context_slot_get_index(sl2);
}

			/* --------------- */

static ar_dd
s_enumerate_assignments (ar_ddm ddm, ar_expr *e)
{
  ccl_pair *p;
  ar_dd result;
  ccl_list *slots = s_collect_slots (e);

  ccl_list_sort (slots, s_cmp_slots);
  result = s_enumerate_assignments_rec(ddm,e,FIRST(slots));
  for(p = FIRST(slots); p; p = CDR(p))
    ar_context_slot_set_value((ar_context_slot *)CAR(p),NULL);
  ccl_list_delete (slots);

  return result;
}

			/* --------------- */

static void
s_collect_slots_rec(ar_expr *e, ccl_list *slots, ccl_hash *cache)
{
  if (ccl_hash_find (cache, e))
    return;
  ccl_hash_insert (cache, e);

  switch( e->op ) {
  case AR_EXPR_VAR: case AR_EXPR_PARAM:
    if (ccl_hash_find (cache, e->u.slot))
      return;
    if( e->op == AR_EXPR_VAR || ! ar_context_slot_has_value(e->u.slot) )
      {
	ccl_hash_insert (cache, e->u.slot);
	ccl_list_add (slots, e->u.slot);
      }
    break;

  case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ: 
  case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ: 
  case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL: 
  case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
  case AR_EXPR_EXIST: case AR_EXPR_FORALL: 
  case AR_EXPR_MIN: case AR_EXPR_MAX: 
    {
      int i;
      for(i = 0; i < e->arity; i++)
	s_collect_slots_rec(e->args[i],slots, cache);
    }
    break;

  case AR_EXPR_CST: 
    break;
  case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER: case AR_EXPR_CALL:
  case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:
    ccl_throw_no_msg(internal_error);
    break;
  }
}

			/* --------------- */

static ccl_list *
s_collect_slots(ar_expr *e)
{
  ccl_list *result = ccl_list_create();
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
  s_collect_slots_rec (e, result, cache);
  ccl_hash_delete (cache);

  return result;
}

static void
s_delete_dd (void *dd)
{
  AR_DD_FREE (dd);
}

