/*
 * arc-debug.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "deps/fdset-comp.h"
#include "ar-rel-semantics.h"
#include "ar-flat-semantics.h"
#include "ar-ca-to-sas.h"
#include "sas/sas.h"
#include <ccl/ccl-memory.h>

# define ARC_DEBUG_PREFS(type_, id_, string_) 
# include "arc-debug.def"
# undef ARC_DEBUG_PREFS

#include "arc-debug.h"

struct debug_flag
{
  struct debug_flag *next;
  const char *id;
  void *addr;
  void (*update)(const char *id, void *addr, ccl_config_table *conf);
};

static struct debug_flag *DEBUG_FLAGS = NULL;

const char *arc_debug_preferences_prefix = ARC_DEBUG_PREFS_PREFIX;
const char *arc_debug_default_dot_filename = NULL;
const char *arc_debug_default_dot_attributes = NULL;
int arc_debug_max_log_level = -1;

int
arc_debug_init (ccl_config_table *conf)
{
  DEBUG_FLAGS = NULL;
  
# define ARC_DEBUG_PREFS(type_, id_, string_) \
  arc_debug_register_ ## type_ ## _flag (string_, &id_);
# include "arc-debug.def"
# undef ARC_DEBUG_PREFS

  arc_debug_update_flags (conf);
  
  return 1;
}

void
arc_debug_terminate (void)
{
  struct debug_flag *next;

  while (DEBUG_FLAGS)
    {
      next = DEBUG_FLAGS->next;
      ccl_delete (DEBUG_FLAGS);
      DEBUG_FLAGS = next;
    }
}

void
arc_debug_update_integer (const char *id, void *addr, ccl_config_table *conf)
{
  *((int *) addr) = ccl_config_table_get_integer (conf, id);
}

void
arc_debug_update_string (const char *id, void *addr, ccl_config_table *conf)
{
  *((const char **) addr) = ccl_config_table_get (conf, id);
}

void
arc_debug_update_boolean (const char *id, void *addr, ccl_config_table *conf)
{
  *((int *) addr) = ccl_config_table_get_boolean (conf, id);
}

void
arc_debug_register_flag (const char *id, void *addr,
			 void (*update)(const char *id, void *pflag,
					ccl_config_table *conf))
{
  struct debug_flag *newflag = ccl_new (struct debug_flag);

  newflag->next = DEBUG_FLAGS;
  newflag->id = id;
  newflag->addr = addr;
  newflag->update = update;
  DEBUG_FLAGS = newflag;
}

void
arc_debug_update_flags (ccl_config_table *conf)
{
  struct debug_flag *df;

  for (df = DEBUG_FLAGS; df; df = df->next)
    {
      df->update (df->id, df->addr, conf);
      if (df->addr == &arc_debug_max_log_level)
	ccl_debug_max_level (arc_debug_max_log_level);
    }
}
