/*
 * ar-expr2dd.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_EXPR2DD_H
# define AR_EXPR2DD_H

#include "ar-dd.h"

extern ccl_hash *
ar_expr2dd_create_info_cache (void);


extern ar_dd 
ar_boolean_expr_to_dd (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		       ccl_hash *info_cache, ar_expr *e);

#endif /* ! AR_EXPR2DD_H */
