/*
 * diagnosis.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef DIAGNOSIS_H
# define DIAGNOSIS_H

# include <ccl/ccl-set.h>
# include "ar-constraint-automaton.h"

extern void
diagnosis_generator (ar_ca *ca, ar_ca_expr *target, ccl_set *failures,
		     ccl_set *disabled, const char *prefix, ccl_log_type log);

#endif /* ! DIAGNOSIS_H */
