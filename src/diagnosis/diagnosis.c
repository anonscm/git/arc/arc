/*
 * diagnosis.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "arc-debug.h"
#include "ar-ca-expr2dd.h"
#include <deps/depgraph.h>
#include <deps/fdset-comp.h>
#include "ar-constraint-automaton.h"
#include "ar-ca-rewriters.h"
#include "ar-solver.h"
#include "ar-rel-semantics.h"
#include "observer.h"
#include "diagnosis.h"

#define DBG_PREPROCESS_GRAPH 0
#define DBG_SCC_GRAPH 0
#define DBG_SCC_GRAPH_NAME arc_debug_default_dot_filename
#define DBG_BUNDLES 1
#define DBG_PREPROCESS_RESULT 0

#define PREFIX_FOR_ROOT_GATES "root_"

#if 0
# define ARALIA_COMMANDS			\
  "/*\n"					\
  "compute BDD root; \n"			\
  "display products BDD root; \n"		\
  "compute ZPC root; \n"			\
  "display products ZPC root; \n"		\
  " */\n"					
#else
# define ARALIA_COMMANDS NULL
#endif

typedef struct bundle_st bundle;

struct bundle_st
{
  intptr_t idx;
  ccl_vertex *scc;
  ccl_hash *elements;
  ccl_list *variables;
  ccl_list *transitions;
  ccl_list *assertions;
  ccl_list *functions;
  ar_dd trel;
  ar_dd reachables;       /* reachable configuration visible from parent 
			     bundles */
  ar_dd ext_reachables;   /* reachable configurations with observer variables
			     and dependencies */
  ccl_list *inputs;

  ar_dd pl_outputs;
  ar_dd plist1;
  ar_dd plist2;
  ar_dd wlist;
  ar_dd state_variables_list; /* state variables of the bundle with observer's
				 variables excluded i.e. wlist without event
				 variables. */
  ccl_set *to_remove;
  bundle *lsa;
  int single_var;
};

typedef struct compiler_st
{
  int eqid;
  char *top_prefix;
  char *assignment_prefix;
  char *equation_prefix;
  ar_ca_exprman *eman;
  ar_ca *ca;
  ar_relsem *rs;
  ar_ddm ddm;
  const varorder *order;

  ccl_set *statevars;
  ccl_set *eventvars;
  ccl_set *invvars;
  ccl_hash *e2vl;
  ccl_hash *v2comp;
  
  depgraph *DG;
  int nb_scc;
  ccl_graph *sccG;
  bundle **B;
  ccl_hash *v2bundle; /* mix graph objects and scc vertices !!!! */
  ccl_set *qvars;

  ccl_set *acache;
  ccl_hash *ecache;
  ccl_hash *domaincache;
} compiler;

static compiler *
s_crt_compiler (const char *prefix, ar_ca *srcca,  ccl_set *failures,
		ccl_set *disabled);

static void
s_delete_compiler (compiler *C);

static bundle *
s_new_bundle (ccl_vertex *scc);

static void
s_delete_bundle (bundle *b);

static void
s_build_bundles (compiler *C);

static void
s_log_bundle (ccl_log_type log, compiler *C, bundle *b);

static void
s_propagate_constants (ar_ca **p_ca, ar_ca_expr **p_target);

static void
s_dot_scc_vertex (ccl_log_type log, ccl_vertex *v, void *cbdata);

static void
s_log_scc_graph (ccl_log_type log, compiler *C);

static void
s_formula_for_var_assignment (ccl_log_type log, compiler *C,
			      ar_ca_expr *v, ar_ca_expr *val,
			      ccl_list *todo);

static char * 
s_equations_for_boolean_expression (ccl_log_type log, compiler *C,
				    ar_ca_expr *E, ccl_list *todo);

static void
s_generate_formula (ccl_log_type log, const char *prefix, compiler *C, 
		    ar_ca_expr *target);

static void
s_generate_root_formula (ccl_log_type log, compiler *C, int nb_subtargets);

static void
s_formula_for_subautomaton (ccl_log_type log, ar_ca_expr *var, ar_ca_expr *val,
			    bundle *b, compiler *C, ccl_list *todo);

static char * 
s_dd_to_formula (ccl_log_type log, const char *prefix, compiler *C, ar_dd dd, 
		 ccl_list *todo);

static void
s_compute_lsa (compiler *C);

static void
s_compute_reachables_for_bundle (compiler *C, bundle *b);

static void
s_log_dd (ccl_log_type log, compiler *C, ar_dd F);

static ccl_list *
s_preprocess_target (ar_ca *srcca,  ar_ca_expr *target);

static void
s_diagnose_for_target (const char *prefix, compiler *C, ar_ca_expr *target,
		       ccl_log_type log);

static ar_ca_expr *
s_domain_get_value (ar_ca_exprman *eman, const ar_ca_domain *dom, int idx);

static void
s_generate_domain_of_variable (ccl_log_type log, compiler *C, ar_ca_expr *v,
			       ccl_list *gates, ccl_list *vars);

void
diagnosis_generator (ar_ca *srcca,  ar_ca_expr *target, ccl_set *failures, 
		     ccl_set *disabled, const char *prefix, ccl_log_type log)
{
  int tindex;
  ccl_list *subtargets;
  compiler *C;
  ar_ca_expr *ntarget = ar_ca_expr_add_reference (target);
  ar_ca *nca = ar_ca_add_reference (srcca);

  s_propagate_constants (&nca, &ntarget);
  ar_ca_reduce_assertions (nca);
  ar_ca_trim_to_reachables_from_formula (nca, ntarget);

  subtargets = s_preprocess_target (nca,  ntarget);
  C = s_crt_compiler (prefix, nca,  failures, disabled);
  for (tindex = 0;  ! ccl_list_is_empty (subtargets); tindex++)
    {
      ar_ca_expr *t = ccl_list_take_first (subtargets);      
      char *pref = ccl_string_format_new ("%s%s%d", C->top_prefix,
					  PREFIX_FOR_ROOT_GATES, 
					  tindex);
      ccl_log (log, "/* generating equations %s for sub-target: ", pref);
      ar_ca_expr_log (log, t);
      ccl_log (log, " */\n");
      s_diagnose_for_target (pref, C, t, log);
      ar_ca_expr_del_reference (t);
      ccl_string_delete (pref);
    }
  ccl_list_delete (subtargets);
  ar_ca_expr_del_reference (ntarget);

  s_generate_root_formula (log, C, tindex);

  if (DBG_SCC_GRAPH)
    {
      ccl_graph *G = depgraph_get_graph (C->DG);
      s_log_scc_graph (CCL_LOG_DEBUG, C);
      ccl_log (CCL_LOG_DEBUG, "#scc= %d/%d\n", C->nb_scc,
	       ccl_graph_get_number_of_vertices (G));
    } 
  s_delete_compiler (C);
  ar_ca_del_reference (nca);
}

static void
s_diagnose_for_target (const char *prefix, compiler *C,  ar_ca_expr *target,
		       ccl_log_type log)
{  
  CCL_DEBUG_START_TIMED_BLOCK (("compute %s formula", prefix));
  s_generate_formula (log, prefix, C, target);
  CCL_DEBUG_END_TIMED_BLOCK ();
}

static bundle *
s_new_bundle (ccl_vertex *scc)
{
  bundle *b = ccl_new (bundle);

  b->idx = ((intptr_t) ccl_vertex_get_data (scc)) - 1;
  b->scc = scc;
  b->elements = ccl_hash_create (NULL, NULL, NULL, NULL);
  b->variables = ccl_list_create ();
  b->transitions = ccl_list_create ();
  b->assertions = ccl_list_create ();
  b->functions = ccl_list_create ();
  b->trel = NULL;
  b->reachables = NULL;
  b->ext_reachables = NULL;
  b->inputs = ccl_list_create ();
  b->pl_outputs = NULL;
  b->plist1 = NULL;
  b->plist2 = NULL;
  b->wlist = NULL;
  b->state_variables_list = NULL;
  b->to_remove = ccl_set_create ();
  b->lsa = NULL;
  
  return b;
}

static void
s_delete_bundle (bundle *b)
{
  ccl_zdelete (AR_DD_FREE, b->trel);
  ccl_zdelete (AR_DD_FREE, b->reachables);
  ccl_zdelete (AR_DD_FREE, b->ext_reachables);
  ccl_zdelete (AR_DD_FREE, b->pl_outputs);
  ccl_zdelete (AR_DD_FREE, b->plist1);
  ccl_zdelete (AR_DD_FREE, b->plist2);
  ccl_zdelete (AR_DD_FREE, b->wlist);
  ccl_zdelete (AR_DD_FREE, b->state_variables_list);
  
  ccl_hash_delete (b->elements);
  ccl_list_delete (b->transitions);
  ccl_list_clear_and_delete (b->assertions, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);
  ccl_list_delete (b->variables);
  ccl_list_delete (b->functions);
  ccl_list_delete (b->inputs);
  ccl_set_delete (b->to_remove);
  ccl_delete (b);
}

static void
s_build_bundle (compiler *C, bundle *b)
{
  ccl_pair *p;
  ccl_pointer_iterator *pe = ccl_hash_get_keys (b->elements);
  ccl_list *variables = ccl_list_create ();
  ccl_list *outputs = ccl_list_create ();
  ccl_list *wvariables = ccl_list_create ();

  b->single_var = 1;
  
  while (ccl_iterator_has_more_elements (pe))
    {
      void *obj = ccl_iterator_next_element (pe);
      depgraph_object_kind k = depgraph_get_kind (C->DG, obj);
      
      if (k == DG_TRANSITION)
	{
	  b->single_var = 0;
	  ccl_list_add (b->transitions, obj);
	}
      else if (k == DG_CONSTRAINT)
	{
	  b->single_var = 0;
	  ccl_list_add (b->assertions, ar_ca_expr_add_reference (obj));
	  ar_ca_expr_get_variables (obj, variables);
	}
      else if (k == DG_INIT_CONSTRAINT)
	{
	  b->single_var = 0;
	  ccl_assert ("do not handle init constraint" &&
		      k != DG_INIT_CONSTRAINT);
	}
      else if (k == DG_FUNC)
	{
	  const ccl_list *fin = funcdep_get_inputs (obj);
	  b->single_var = 0;
	  for (p = FIRST(fin); p; p = CDR (p))
	    if (! ccl_list_has (variables, CAR (p)))
	      ccl_list_add (variables, CAR (p));

	  ccl_list_add (b->functions, obj);
	  ccl_list_add (b->variables, funcdep_get_output (obj));
	  //	  ccl_list_add (outputs, funcdep_get_output (obj));
	}
      else if (k == DG_VARIABLE)
	{
	  ccl_list_add (b->variables, obj);
	}
      else if (k == DG_EVENT || k == DG_INIT)
	{
	  b->single_var = 0;
	  ((void)0);
	  /* ignore it */
	}
    }
  ccl_iterator_delete (pe);

  for (p = FIRST (b->transitions); p; p = CDR (p))
    {
      ar_ca_trans_get_variables (CAR (p), variables, AR_CA_R_VARS);
      ar_ca_trans_get_variables (CAR (p), wvariables, AR_CA_W_VARS);
    }
  
  for (p = FIRST (variables); p; p = CDR (p))
    {
      ar_ca_expr *var = CAR (p);
      bundle *bv = ccl_hash_get_with_key (C->v2bundle, var);
      
      if (bv != b && ccl_vertex_has_successor (b->scc, bv->scc) &&
	  ! ccl_list_has (b->inputs, var))
	ccl_list_add (b->inputs, var);
      else if (! ccl_set_has (C->statevars, var))
	ccl_list_add (outputs, var);
    }
  
  b->pl_outputs = ar_dd_create_projection_list (C->ddm);
  while (! ccl_list_is_empty (outputs))
    {
      ar_ca_expr *v = ccl_list_take_first (outputs);
      int vindex = varorder_get_dd_index (C->order, v);
      ccl_assert (vindex >= 0);
      
      ar_dd_projection_list_add (C->ddm, &b->pl_outputs, vindex, vindex);
    }
  ccl_list_delete (outputs);
  
  b->plist1 = ar_dd_create_projection_list (C->ddm);
  b->plist2 = ar_dd_create_projection_list (C->ddm);  
  for (p = FIRST (b->inputs); p; p = CDR (p))
    {
      int vindex = varorder_get_dd_index (C->order, CAR (p));
      ccl_assert (vindex >= 0);
      
      ar_dd_projection_list_add (C->ddm, &b->plist1, vindex, vindex);
      ar_dd_projection_list_add (C->ddm, &b->plist2, vindex, vindex);
    }

  b->state_variables_list = ar_dd_create_projection_list (C->ddm);
  b->wlist = ar_dd_create_projection_list (C->ddm);
  for (p = FIRST (wvariables); p; p = CDR (p))
    {
      int vindex = varorder_get_dd_index (C->order, CAR (p));
      ccl_assert (vindex >= 0);
      
      if (! ccl_set_has (C->eventvars, CAR (p)))
	{
	  ar_dd_projection_list_add (C->ddm, &b->state_variables_list, vindex,
				     vindex);
	}
      else
	{
	  int pvindex = ar_relsem_get_primed_dd_index (C->rs, vindex);
	  ar_dd_projection_list_add (C->ddm, &b->plist1, pvindex, pvindex);
	  ar_dd_projection_list_add (C->ddm, &b->plist2, pvindex, vindex);
	}
	
      ar_dd_projection_list_add (C->ddm, &b->wlist, vindex, vindex);      
    }
  ccl_list_delete (wvariables);
  ccl_list_delete (variables);
}

static void
s_build_bundles (compiler *C)
{
  int i;
  ccl_hash_entry_iterator *ei = ccl_hash_get_entries (C->v2comp);

  C->v2bundle = ccl_hash_create (NULL, NULL, NULL, NULL);
  C->B = ccl_new_array (bundle *, C->nb_scc);
  while (ccl_iterator_has_more_elements (ei))
    {
      ccl_hash_entry e = ccl_iterator_next_element (ei);
      void *obj = ccl_vertex_get_data (e.key);
      ccl_vertex *scc = e.object;
      intptr_t idx = ((intptr_t) ccl_vertex_get_data (scc)) - 1;
      bundle *b;
      
      ccl_assert (0 <= idx && idx < C->nb_scc);
      
      if (C->B[idx] == NULL)
	C->B[idx] = s_new_bundle (scc);
      b = C->B[idx];
      
      ccl_assert (idx == b->idx);

      ccl_hash_find (b->elements, obj);
      ccl_hash_insert (b->elements, obj);

      ccl_assert (!ccl_hash_find (C->v2bundle, obj));
      
      ccl_hash_find (C->v2bundle, obj);
      ccl_hash_insert (C->v2bundle, b);
      if (!ccl_hash_find (C->v2bundle, scc))
	ccl_hash_insert (C->v2bundle, b);
      else
	ccl_assert (ccl_hash_get (C->v2bundle) == b);
    }
  ccl_iterator_delete (ei);

  for (i = 0; i < C->nb_scc; i++)
    s_build_bundle (C, C->B[i]);

  s_compute_lsa (C);

  for (i = 0; i < C->nb_scc; i++)
    s_compute_reachables_for_bundle (C, C->B[i]);
}

static void
s_log_bundle (ccl_log_type log, compiler *C, bundle *b)
{
  ccl_pair *p;
  
  ccl_log (log, "bundle %d\n", b->idx);
  
  for (p = FIRST (b->inputs); p; p = CDR (p))
    {
      ccl_log (log, "input ");
      ar_ca_expr_log (log, CAR (p));
      ccl_log (log, "\n");
    }

  for (p = FIRST (b->variables); p; p = CDR (p))
    {
      ccl_log (log, "variables ");
      ar_ca_expr_log (log, CAR (p));
      ccl_log (log, "\n");
    }

  for (p = FIRST (b->assertions); p; p = CDR (p))
    {
      ccl_log (log, "assert ");
      ar_ca_expr_log (log, CAR (p));
      ccl_log (log, "\n");
    }

  for (p = FIRST (b->functions); p; p = CDR (p))
    {
      funcdep *fd = CAR (p);
      ccl_log (log, "function ");
      funcdep_log (log, fd);
      ccl_log (log, "\n");
    }

  for (p = FIRST (b->transitions); p; p = CDR (p))
    {
      ccl_log (log, "trans ");
      ar_ca_trans_log (log, CAR (p));
      ccl_log (log, "\n");
    }
  {
    ccl_pointer_iterator *i = ccl_set_get_elements (b->to_remove);
    ccl_log (log, "TO_REMOVE=%d\n", ccl_set_get_size (b->to_remove));
    ccl_log (log, "LSA=%d\n", b->lsa ? b->lsa->idx : -1);

    while (ccl_iterator_has_more_elements (i))
      {
	ar_ca_expr *var = ccl_iterator_next_element (i);
	ccl_log (log, " ");
	ar_ca_expr_log (log, var);
      }
    ccl_iterator_delete (i);
    ccl_log (log, "\n");
  }

  if(0){
    ar_dd vars = ar_dd_get_variables (C->ddm, b->ext_reachables);
    ccl_log (log, "Reach=");
    s_log_dd (log, C, b->ext_reachables);
    ccl_log (log, "\b");
    AR_DD_FREE (vars);
  }
}

static void
s_propagate_constants (ar_ca **p_ca, ar_ca_expr **p_target)
{
  const fdset *fds = ar_ca_get_fdset (*p_ca);
  fdset *cfds = fdset_get_constants_and_variables (fds);
  ar_ca *rca = ar_ca_remove_functional_dependencies (*p_ca, cfds);
  ar_ca_expr *rtarget = ar_ca_rewrite_expr_with_fd (cfds, *p_target, 1);
  ar_ca_del_reference (*p_ca);
  *p_ca = rca;
  ar_ca_expr_del_reference (*p_target);
  *p_target = rtarget;
  fdset_del_reference (cfds);
}

static void
s_dot_scc_vertex (ccl_log_type log, ccl_vertex *v, void *data)
{
  ccl_pair *p;
  bundle *b;
  compiler *C = data;
  void *v2bundle = C->v2bundle;
  ccl_pre (ccl_hash_find (v2bundle, v));
  ccl_hash_find (v2bundle, v);
  b = ccl_hash_get (v2bundle);
  ccl_log (log, "label=\"SCC %d", b->idx);
  if (b->lsa != NULL)
    ccl_log (log, " [lsa=%d]", b->lsa->idx);
  ccl_log (log, "\\n");
  if (! ccl_list_is_empty (b->inputs))
    {
      ccl_log (log, "in");
      for (p = FIRST (b->inputs); p; p = CDR (p))
	{
	  ccl_log (log, " ");
	  ar_ca_expr_log (log, CAR (p));
	}
      ccl_log (log, "\\n");
    }

  if (! ccl_list_is_empty (b->variables))
    {
      ccl_log (log, "var");
      for (p = FIRST (b->variables); p; p = CDR (p))
	{
	  if (ccl_set_has (C->eventvars, CAR (p)))
	    continue;
	  ccl_log (log, " ");
	  ar_ca_expr_log (log, CAR (p));
	}
      ccl_log (log, "\\n");
    }
  
  for (p = FIRST (b->assertions); p; p = CDR (p))
    {
      ccl_log (log, "assert ");
      ar_ca_expr_log (log, CAR (p));
      ccl_log (log, "\\n");
    }
  for (p = FIRST (b->functions); p; p = CDR (p))
    {
      ccl_log (log, "function ");
      funcdep_log (log, CAR (p));
      ccl_log (log, "\\n");
    }
  for (p = FIRST (b->transitions); p; p = CDR (p))
    {
      ar_ca_trans_log (log, CAR (p));
      ccl_log (log, "\\n");
    }
  
  ccl_log (log, "\"");
}

static void
s_dot_graph_attributes (ccl_log_type log, ccl_graph *g, void *dummy)
{
  if (arc_debug_default_dot_attributes)
    ccl_log (log, "%s\n", arc_debug_default_dot_attributes);
}

static void
s_log_scc_graph (ccl_log_type log, compiler *C)
{
  int i;
  int nb_scc = ccl_graph_get_number_of_vertices (C->sccG);
  ccl_hash *scc2bundle = ccl_hash_create (NULL, NULL, NULL, NULL);
  FILE *out;
  
#if 0  
  ccl_vertex_iterator *vi = ccl_graph_get_leaves (C->sccG);
  ccl_list *leaves = ccl_list_create_from_iterator ((ccl_pointer_iterator *)vi);
  ccl_iterator_delete (vi);
  while (! ccl_list_is_empty (leaves))
    ccl_graph_remove_vertex (C->sccG, ccl_list_take_first (leaves));
  ccl_list_delete (leaves);
#endif
  for (i = 0; i < nb_scc; i++)
    {
      ccl_assert(! ccl_hash_find (scc2bundle, C->B[i]->scc));
      ccl_hash_find (scc2bundle, C->B[i]->scc);
      ccl_hash_insert (scc2bundle, C->B[i]);
    }

  if (DBG_SCC_GRAPH_NAME != NULL)
    {
      out = fopen (DBG_SCC_GRAPH_NAME, "w");
      if (out == NULL)
	ccl_debug ("can't open file '%s'\n", DBG_SCC_GRAPH_NAME);
      else
	ccl_log_redirect_to_FILE (log, out);
    }
  else
    out = NULL;
  ccl_graph_log_as_dot (log, C->sccG, s_dot_graph_attributes, NULL,
			s_dot_scc_vertex, C, NULL, NULL);
  if (out != NULL)
    {
      ccl_log_pop_redirection (log);
      fflush (out);
      fclose (out);
    }
  ccl_hash_delete (scc2bundle);
}

			/* --------------- */

static void
s_formula_for_var_assignment (ccl_log_type log, compiler *C, ar_ca_expr *v,
			      ar_ca_expr *val, ccl_list *todo)
{
  bundle *b;
  ar_ca_expr *eq = ar_ca_expr_crt_eq (v, val);

  if (ccl_set_has (C->acache, eq))
    {
      ar_ca_expr_del_reference (eq);
      return;
    }
  ccl_set_add (C->acache, eq);
  
  ccl_pre (ccl_hash_find (C->v2bundle, v));
  
  if (ccl_set_has (C->eventvars, v))
    {
      ccl_log (log, "/* event variable ");
      ar_ca_expr_log (log, v);
      ccl_log (log, "*/\n");
      ccl_log (log, "`%s", C->assignment_prefix);
      ar_ca_expr_log (log, eq);
      ccl_log (log, "' := ");

      if (ar_ca_expr_is_false (val))
	ccl_log (log, "-");
      ccl_log (log, "`");
      {
	char *sid = observer_get_event_name_from_var (v);
	ccl_log (log, sid);	
	ccl_string_delete (sid);
      }
      ccl_log (log, "';\n");	  
    }
  else
    {  
      b = ccl_hash_get_with_key (C->v2bundle, v);
      CCL_DEBUG_START_TIMED_BLOCK(("BUNDLE %d", b->idx));

      if (ccl_hash_get_size (b->elements) == 1) 
	{ /* v is the output of a FD */
	  ccl_vertex_iterator *pi = ccl_vertex_get_successors (b->scc);
	  if (ccl_iterator_has_more_elements (pi))
	    {
	      ccl_vertex *succ = ccl_iterator_next_element (pi);
	      bundle *bsucc = ccl_hash_get_with_key (C->v2bundle, succ);
	      funcdep *fd = CAR (FIRST (bsucc->functions));
	      ar_ca_expr *eq2 = ar_ca_expr_crt_eq (funcdep_get_function (fd),
						   val);
	      char *geq2;
	  
	      ccl_assert (bsucc != NULL);
	      ccl_assert (ccl_list_is_empty (bsucc->transitions));
	      ccl_assert (ccl_list_is_empty (bsucc->assertions));
	      ccl_assert (ccl_list_get_size (bsucc->functions) == 1);

	      geq2 = s_equations_for_boolean_expression (log, C, eq2, todo);
	      ccl_log (log, "/* instanciate function ");
	      ar_ca_expr_log (log, v);
	      ccl_log (log, " = ");
	      ar_ca_expr_log (log, funcdep_get_function (fd));
	      ccl_log (log, "*/\n");

	      ccl_log (log, "`%s", C->assignment_prefix);
	      ar_ca_expr_log (log, eq);
	      if (*geq2 == '0' || *geq2 == '1')
		ccl_log (log, "' := %s;\n", geq2);
	      else
		ccl_log (log, "' := `%s';\n", geq2);
	      ar_ca_expr_del_reference (eq2);
	    }
	  else if (! ccl_set_has (C->qvars, v))
	    {
	      ccl_warning ("warning: free variable ");
	      ar_ca_expr_log (CCL_LOG_WARNING, v);
	      ccl_warning (" is not assigned.\n");

	      ccl_log (log, "/* WARNING: free variable ");
	      ar_ca_expr_log (log, v);
	      ccl_log (log, " is not assigned. */\n");
	      
	      ccl_set_add (C->qvars, v);
	    }
	  ccl_iterator_delete (pi);
	}
      else    
	{ /* v is a state variable assigned by some transitions */
	  ccl_assert (b->ext_reachables != NULL);
	  s_formula_for_subautomaton (log, v, val, b, C, todo);
	}
      CCL_DEBUG_END_TIMED_BLOCK();
    }
}

static char * 
s_equations_for_boolean_expression (ccl_log_type log, compiler *C,
				    ar_ca_expr *E, ccl_list *todo)
{
  char *g;
  ar_dd F = ar_relsem_compute_simple_set (C->rs, E);

  if (ccl_hash_find (C->ecache, F))
    g = ccl_hash_get (C->ecache);
  else
    g = s_dd_to_formula (log, C->equation_prefix, C, F, todo);
  AR_DD_FREE (F);
  
  return g;
}

			/* --------------- */

static void
s_generate_formula (ccl_log_type log, const char *prefix, compiler *C,
		    ar_ca_expr *target)
{
  ccl_list *todo = ccl_list_create ();
  char *g = s_equations_for_boolean_expression (log, C, target, todo);

  while (! ccl_list_is_empty (todo))
    {
      ar_ca_expr *var = ccl_list_take_first (todo);
      ar_ca_expr *val = ccl_list_take_first (todo);
      
      s_formula_for_var_assignment (log, C, var, val, todo);

      ar_ca_expr_del_reference (var);
      ar_ca_expr_del_reference (val);
    }

  if (*g == '0' || *g == '1')
    ccl_log (log, "`%s' := %s;\n", prefix, g);
  else
    ccl_log (log, "`%s' := `%s';\n", prefix, g);

  ccl_list_delete (todo);
}

static ar_ca_expr *
s_domain_get_value (ar_ca_exprman *eman, const ar_ca_domain *dom, int idx)
{
  int value = ar_ca_domain_get_ith_value (dom, idx);
  ar_ca_expr *result;

  if (ar_ca_domain_is_boolean (dom))
    result = ar_ca_expr_crt_boolean_constant (eman, value);
  else if (ar_ca_domain_is_enum (dom))
    result = ar_ca_expr_get_enum_constant_by_index (eman, value);
  else
    result = ar_ca_expr_crt_integer_constant (eman, value);

  return result;
}

static void
s_generate_domain_of_variable (ccl_log_type log, compiler *C, ar_ca_expr *v,
			       ccl_list *gates, ccl_list *vars)
{
  ccl_log_type redir = log == CCL_LOG_USR1 ? CCL_LOG_USR2 : CCL_LOG_USR1;
  char *tmp = NULL;
  char *g;
  ar_ca_exprman *eman = ar_ca_expr_get_expression_manager (v);
  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (v);
  int i;
  int card = ar_ca_domain_get_cardinality (dom);
  
  ccl_log_redirect_to_string (redir, &tmp);
  ccl_log (redir, "`%s", C->top_prefix);
  ar_ca_expr_log (redir, v);
  ccl_log (redir, "_dom'");
  g = tmp;
  tmp = NULL;
  ccl_list_add (gates, g);

  ccl_log (log, "%s := #(1,1,[", g);
  
  for (i = 0; i < card; i++)
    {
      ar_ca_expr *val = s_domain_get_value (eman, dom, i);
      ar_ca_expr *eq = ar_ca_expr_crt_eq (v, val);
      
      ccl_log (redir, "`%s", C->assignment_prefix);
      ar_ca_expr_log (redir, eq);
      ccl_log (redir, "'");
      ccl_list_add (vars, tmp);

      ccl_log (log, (i == 0) ? "%s" : ", %s", tmp);
      tmp = NULL;
      ar_ca_expr_del_reference (val);
      ar_ca_expr_del_reference (eq);
    }

  ccl_log (log, "]);\n");  
  ccl_log_pop_redirection (redir);
  ar_ca_exprman_del_reference (eman);
}

/*!
 * Compile a DD into a set of equations and return the identifier of the root 
 * gate. A cache is used and associates DDs and gate names. The list of 
 * variables assignments to be treated by FT generation algorithm is filled with
 * according to edges of the DD. The translation works as follows:
 * - constants are translated as-is.
 * - case (x, F_1, ..., F_N) is translated as:
 *   . for each i=1... N, (x,v_i) is added to the "todo" list for later 
 *     treatment
 *   . g_F_i is the root gate of the compilation of F_i
 *   . for each i=1,...,N : g_i := ((x = v_i) => g_F_i);
 *   . top level equation is g := (g_0 & g_1 & ...& g_n);
 */
static char * 
s_dd_to_formula (ccl_log_type log, const char *prefix, compiler *C, ar_dd dd, 
		 ccl_list *todo)
{
  char *gname;
  int i, arity;
  ar_ca_expr *var;
  const ar_ca_domain *dom;
  ccl_list *value_gates;
  ccl_pair *p;
  
  if (ar_dd_is_one (C->ddm, dd))
    return "1";
  if (ar_dd_is_zero (C->ddm, dd))
    return "0";

  if (ccl_hash_find (C->ecache, dd))
    return ccl_hash_get (C->ecache);

  dd = AR_DD_DUP (dd);
  gname = ccl_string_format_new ("%s%d", prefix, C->eqid++);
  ccl_hash_insert (C->ecache, gname);

  arity = AR_DD_ARITY (dd);
  var = varorder_get_variable_from_dd_index (C->order, AR_DD_INDEX (dd));
  dom = ar_ca_expr_variable_get_domain (var);     
  value_gates = ccl_list_create ();
  var = ar_ca_expr_add_reference (var);
    
  /*
   * First for each child of 'dd' we built couples (v_i, g_F_i) where v_i is
   * the ith value of the toplevel variable of 'dd'. 
   */
  for(i = 0; i < arity; i++) 
    {
      char *g = s_dd_to_formula (log, prefix, C, AR_DD_PNTHSON (dd, i), todo);
      if (AR_DD_CODE (dd) == COMPACTED)
	{
	  int j;
	  int min = AR_DD_MIN_OFFSET_NTHSON (dd, i);
	  int max = AR_DD_MAX_OFFSET_NTHSON (dd, i);
	  for (j = min; j <= max; j++)
	    {
	      ar_ca_expr *val = s_domain_get_value (C->eman, dom, j);
		  
	      ccl_list_add (value_gates, val);
	      ccl_list_add (value_gates, g);
	    }
	}	  
      else 
	{
	  ar_ca_expr *val = s_domain_get_value (C->eman, dom, i);
	      
	  ccl_list_add (value_gates, val);
	  ccl_list_add (value_gates, g);
	}
    }

  /*
   * Top-level equation.
   * g := (g_0 & ... & g_N) where N = arity of dom (v)
   */
  arity = ccl_list_get_size (value_gates) / 2;
  ccl_log (log, "`%s' := (`%s_x' & %s(`%s_0'", gname,	   
	   gname,
	   //	   AR_DD_POLARITY (dd) ? "-" : "",
	   "",
	   gname);
  for (i = 1; i < arity; i++)
    ccl_log (log, " & `%s_%d'", gname, i);
  ccl_log (log, ")); \n");

  if (ccl_hash_find (C->domaincache, var))
    {
      char *xg = ccl_hash_get (C->domaincache);
      ccl_log (log, "`%s_x' := `%s';\n", gname, xg);
    }
  else
    {
      char *xg = ccl_string_format_new ("%s_x", gname);
      ccl_hash_insert (C->domaincache, xg);
      
      ccl_log (log, "`%s' := #(1,1, [", xg);
      for (p = FIRST (value_gates); p; p = CDDR (p))
	{
	  ar_ca_expr *val = CAR (p);
	  ar_ca_expr *eq = ar_ca_expr_crt_eq (var, val);
	  if (p != FIRST (value_gates))
	    ccl_log (log, ", ");
	  ccl_log (log, "`%s", C->assignment_prefix);
	  ar_ca_expr_log (log, eq);
	  ccl_log (log, "'");
	  ar_ca_expr_del_reference (eq);
	}
      ccl_log (log, "]);\n");
    }
  
  /*
   * For each couple (v_i, g_F_i) we add an equation:
   *  g_i := ('A[x=v_i]') => (g_F_i);
   * where '[x=v_i]' is a new gate generated later on.
   */
  for (i = 0; ! ccl_list_is_empty (value_gates); i++)
    {
      ar_ca_expr *val = ccl_list_take_first (value_gates);
      ar_ca_expr *eq = ar_ca_expr_crt_eq (var, val);
      char *g = ccl_list_take_first (value_gates);

      ccl_list_add (todo, ar_ca_expr_add_reference (var));
      ccl_list_add (todo, ar_ca_expr_add_reference (val));
	  
      ccl_log (log, "`%s_%d' := (`%s", gname, i, C->assignment_prefix);
      ar_ca_expr_log (log, eq);
      ccl_log (log, "' => ");
      if (*g == '0' || *g == '1') 
	ccl_log (log, "%s", g);
      else
	ccl_log (log, "`%s'", g);
      ccl_log (log, "); \n", g);
      ar_ca_expr_del_reference (eq);
      ar_ca_expr_del_reference (val);
    }
  ccl_list_delete (value_gates);
  ar_ca_expr_del_reference (var);

  return gname;
}

static ar_dd
s_filter_relation_for_target (compiler *C, bundle *b, ar_dd target)
{
  ar_dd not_target = AR_DD_NOT (target);
  ar_dd r_not_target =
    ar_dd_compute_and (C->ddm, b->ext_reachables, not_target);
  ar_dd r_not_target_p = ar_relsem_make_it_prime (C->rs, r_not_target);
  
  ar_dd r_target =
    ar_dd_compute_and (C->ddm, b->ext_reachables, target);
  ar_dd r_target_p = ar_relsem_make_it_prime (C->rs, r_target);
  
  ar_dd trel =
    ar_dd_compute_and_3 (C->ddm, b->trel, b->ext_reachables, r_target_p);

  ar_dd sel = ar_dd_project_on_variables (C->ddm, trel, b->plist2);

  AR_DD_FREE (trel);
  AR_DD_FREE (r_target);
  AR_DD_FREE (r_not_target);
  AR_DD_FREE (r_not_target_p);
  AR_DD_FREE (r_target_p);

  return sel;
}

static void
s_formula_for_subautomaton (ccl_log_type log, ar_ca_expr *var, ar_ca_expr *val,
			    bundle *b, compiler *C, ccl_list *todo)
{
  ar_dd tmp0;
  ar_dd tmp1;
  ar_dd sel;
  char *seq = NULL;
  char *g = NULL;
  ar_ca_expr *eq;
  
  ccl_log (log, "/* bundle %d: formula for ", b->idx);
  ar_ca_expr_log (log, var);
  ccl_log (log, " = ");
  ar_ca_expr_log (log, val);
  ccl_log (log, " */\n");
  
  /* select part of reachables that intersect var=val */
  eq = ar_ca_expr_crt_eq (var, val);
  seq = ar_ca_expr_to_string (eq);

  tmp0 = ar_relsem_compute_simple_set (C->rs, eq);
  tmp1 = ar_dd_compute_and (C->ddm, b->ext_reachables, tmp0);
  ar_ca_expr_del_reference (eq);
  
  if (! ccl_set_has (C->statevars, var))
    {
      sel = ar_dd_project_variables (C->ddm, tmp1, b->pl_outputs);
      AR_DD_FREE (tmp1);
      AR_DD_FREE (tmp0);
    }
  else
    {
      ccl_pair *p;
      ar_dd sel2;
      ar_dd sel1 = s_filter_relation_for_target (C, b, tmp0);
      ar_dd init = ar_relsem_get_initial (C->rs);

      AR_DD_FREE (tmp0);
      tmp0 = ar_dd_project_on_variables (C->ddm, init, b->wlist);
		  
      AR_DD_FREE (init);
      init = ar_dd_compute_and (C->ddm, tmp0, tmp1);
      AR_DD_FREE (tmp0);
      AR_DD_FREE (tmp1);
      tmp1 = init;
      tmp0 = AR_DD_DUP (b->state_variables_list);

      for (p = FIRST (b->variables); p; p = CDR (p))
	{
	  if (! ccl_set_has (C->eventvars, CAR (p)))
	    {
	      int vindex = varorder_get_dd_index (C->order, CAR (p));
	      ccl_assert (vindex >= 0);
	      ar_dd_projection_list_add (C->ddm, &tmp0, vindex, vindex);
	    }
	}
      sel2 = ar_dd_project_variables (C->ddm, tmp1, tmp0);
      AR_DD_FREE (tmp1);
      AR_DD_FREE (tmp0);
      sel = ar_dd_compute_or (C->ddm, sel1, sel2);
      AR_DD_FREE (sel1);
      AR_DD_FREE (sel2);
    }

  g = s_dd_to_formula (log, C->equation_prefix, C, sel, todo);
  AR_DD_FREE (sel);
  
  if (*g == '0' || *g == '1')
    ccl_log (log, "`%s%s' := %s; \n", C->assignment_prefix, seq, g);
  else

    ccl_log (log, "`%s%s' := `%s'; \n", C->assignment_prefix, seq, g);
  
  ccl_string_delete (seq);
}

/*!
 * Compute Lowest Single Common Ancestors in the graph of SCCs.
 * Each bundle b is a SCC of the dependency graph. b->lsa points to its lowest 
 * single ancestor of b which means that variables of b are not relevant above 
 * b->lsa.
 */
static void
s_compute_lsa (compiler *C)
{
  ccl_hash *v2t = NULL;
  ccl_tree *lsa = ccl_graph_compute_lsa_tree (C->sccG, &v2t);
  ccl_vertex_iterator *i = ccl_graph_get_vertices (C->sccG);
  
  while (ccl_iterator_has_more_elements (i))
    {
      ccl_vertex *v = ccl_iterator_next_element (i);
      bundle *bv = ccl_hash_get_with_key (C->v2bundle, v);
      ccl_tree *vt = ccl_hash_get_with_key (v2t, v);

      ccl_assert (vt != NULL);     
      ccl_assert (vt->object == v);

      /* vt->parent->object is LSA(v) i.e. the lowest ancestor l of v where all
	 paths from the root pass through to reach v. */
      if (vt->parent->object != NULL)
	{
	  ccl_pair *p;

	  bv->lsa = ccl_hash_get_with_key (C->v2bundle, vt->parent->object);
	  /* Variable of v can be remove after the visit of its LSA. We only
	     kept event variables. Why ? FIXME */
	  for (p = FIRST (bv->variables); p; p = CDR (p))
	    //	    if (! ccl_set_has (C->eventvars, CAR (p)))
	    if ((! bv->lsa->single_var) ||
		! ccl_hash_find (bv->lsa->elements, CAR (p)))
	      ccl_set_add (bv->lsa->to_remove, CAR (p));
	}
    }
  ccl_iterator_delete (i);
  ccl_tree_del_reference (lsa);
  ccl_hash_delete (v2t);
}

/*!
 * Local computation of reachable configurations (RCs) of bundle b.
 * If b contains only assertions/functions then the DD of these relations are 
 * computed as usual.
 * Else reachable configurations are computed using assertions and transitions
 * of the bundle. Each configuration is conjoined with reachable configuration
 * of dependencies. 
 * 
 * The function computes two sets of RCs : 
 * - b->ext_reachables is the set of RCs with observation and dependent bundles
 *   variables.
 * - b->reachables is the projection of b->ext_reachables on variables actually
 *   useful for bundles that depends on b i.e. at least event variables have 
 *   been removed.
 */
static void
s_compute_reachables_for_bundle (compiler *C, bundle *b)
{
  ccl_pair *p;
  ar_dd A;
  ar_dd tmp0, tmp1;
  ar_dd Rdeps;
  ar_dd R;
  
  if (b->reachables != NULL)
    return;

  CCL_DEBUG_START_TIMED_BLOCK (("reachables for bundle %d", b->idx));

  CCL_DEBUG_START_TIMED_BLOCK (("assertions"));
  A = ar_dd_dup_one (C->ddm);
  for (p = FIRST (b->assertions); p; p = CDR (p))
    {
      ar_dd a = ar_relsem_compute_simple_set (C->rs, CAR (p));
      tmp0 = ar_dd_compute_and (C->ddm, A, a);
      AR_DD_FREE (a);
      AR_DD_FREE (A);
      A = tmp0;
    }

  for (p = FIRST (b->functions); p; p = CDR (p))
    {
      funcdep *fd = CAR (p);
      ar_ca_expr *eq = ar_ca_expr_crt_eq (funcdep_get_output (fd),
					  funcdep_get_function (fd));
      ar_dd a = ar_relsem_compute_simple_set (C->rs, eq);
      tmp0 = ar_dd_compute_and (C->ddm, A, a);
      AR_DD_FREE (a);
      AR_DD_FREE (A);
      A = tmp0;
      ar_ca_expr_del_reference (eq);
    }
  CCL_DEBUG_END_TIMED_BLOCK ();

  CCL_DEBUG_START_TIMED_BLOCK (("Rdeps"));
  {
    ccl_vertex_iterator *deps = ccl_vertex_get_successors (b->scc);
    Rdeps = ar_dd_dup_one (C->ddm);    
    while (ccl_iterator_has_more_elements (deps))
      {
	ccl_vertex *d = ccl_iterator_next_element (deps);
	bundle *bd = ccl_hash_get_with_key (C->v2bundle, d);

	if (bd->reachables == NULL)
	  s_compute_reachables_for_bundle (C, bd);
	tmp0 = ar_dd_compute_and (C->ddm, Rdeps, bd->reachables);
	AR_DD_FREE (Rdeps);
	Rdeps = tmp0;
      }
    ccl_iterator_delete (deps);
  }
  CCL_DEBUG_END_TIMED_BLOCK ();

  if (ccl_list_is_empty (b->transitions))
    R = ar_dd_compute_and (C->ddm, A, Rdeps);
  else
    {
      CCL_DEBUG_START_TIMED_BLOCK (("compute local post*"));
      int fixpoint = 0;      
      
      CCL_DEBUG_START_TIMED_BLOCK (("compute trel"));
      b->trel = ar_dd_dup_zero (C->ddm);     
      //ccl_list_add (b->transitions, ar_ca_get_epsilon_transition (C->ca));
      for (p = FIRST (b->transitions); p; p = CDR (p))
	{
	  tmp0 = ar_relsem_get_transition_relation (C->rs, 0, 0, CAR (p));
	  tmp1 = ar_dd_compute_or (C->ddm, tmp0, b->trel);
	  AR_DD_FREE (tmp0);
	  AR_DD_FREE (b->trel);
	  b->trel = tmp1;
	}
      CCL_DEBUG_END_TIMED_BLOCK ();

      CCL_DEBUG_START_TIMED_BLOCK (("compute local init"));
      /* compute initial configurations */
      tmp0 = ar_relsem_get_initial (C->rs);
      tmp1 = ar_dd_project_on_variables (C->ddm, tmp0, b->wlist);
      AR_DD_FREE (tmp0);
      R = ar_dd_compute_and_3 (C->ddm, tmp1, A, Rdeps);
      AR_DD_FREE (tmp1);
      CCL_DEBUG_END_TIMED_BLOCK ();

      CCL_DEBUG_START_TIMED_BLOCK (("compute fixpoint"));
      /* compute reachables */
      while (! fixpoint)
	{
	  tmp0 = ar_relsem_compute_post_by_T (C->rs, R, b->trel, 1, &A);
	  tmp1 = ar_dd_project_on_variables (C->ddm, tmp0, b->wlist);
	  AR_DD_FREE (tmp0);
	  tmp0 = ar_dd_compute_and (C->ddm, tmp1, Rdeps);
	  AR_DD_FREE (tmp1);
	  
	  tmp1 = ar_dd_compute_or (C->ddm, tmp0, R);
	  AR_DD_FREE (tmp0);
	  
	  if (tmp1 == R)
	    {
	      fixpoint = 1;
	      AR_DD_FREE (tmp1);
	    }
	  else
	    {
	      AR_DD_FREE (R);
	      R = tmp1;
	    }	  
	}
      
      CCL_DEBUG_END_TIMED_BLOCK ();
      
      CCL_DEBUG_END_TIMED_BLOCK ();
    }
  AR_DD_FREE (A);
  AR_DD_FREE (Rdeps);

  b->ext_reachables = R;

  CCL_DEBUG_START_TIMED_BLOCK (("remove useless variables"));
  {
    ccl_pointer_iterator *vi = ccl_set_get_elements (b->to_remove);

    tmp0 = ar_dd_create_projection_list (C->ddm);
    while (ccl_iterator_has_more_elements (vi))
      {
	ar_ca_expr *var = ccl_iterator_next_element (vi);
	int vindex = varorder_get_dd_index (C->order, var);
	ccl_assert (vindex >= 0);
	ar_dd_projection_list_add (C->ddm, &tmp0, vindex, vindex);	
      }
    ccl_iterator_delete (vi);

    for (p = FIRST (b->variables); p; p = CDR (p))
      {
	if (ccl_set_has (C->eventvars, CAR (p)))
	  {
	    int vindex = varorder_get_dd_index (C->order, CAR (p));
	    ccl_assert (vindex >= 0);
	    ar_dd_projection_list_add (C->ddm, &tmp0, vindex, vindex);
	  }
      }
    b->reachables = ar_dd_project_variables (C->ddm, b->ext_reachables, tmp0);
    AR_DD_FREE (tmp0);
  }
  CCL_DEBUG_END_TIMED_BLOCK ();  
  CCL_DEBUG_END_TIMED_BLOCK ();
}

/*!
 * Split the expression 'target' as operands of a conjunction. Rules applied 
 * are:
 * - target = F1 & F2 -> R <- R U split(F1) U split (F2)
 * - target = x and x = F1 & F2 -> R <- R U split (F1 & F2)
 * - target = x and x = y -> R <- R U split (y)
 * - target = not (F1 | F2) -> R <- R U split (not F1) U split (not F2) 
 * - target = not x and x = F1 | F2 -> R <- R U split (not F1) U split 
 *   (not F2)
 * - target = F(x) and x = G -> R <- R U split (F(G))
 * - else R = R U { target }
 */
static void
s_split_target (ar_ca_expr *target, ccl_list *result, const fdset *fds)
{
  int added = 0;
  
  if (ar_ca_expr_get_kind (target) == AR_CA_AND)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (target);
      s_split_target (args[0], result, fds);
      s_split_target (args[1], result, fds);
      added = 1;
    }
  else if (ar_ca_expr_get_kind (target) == AR_CA_VAR &&
	   fdset_has_dependency_for (fds, target))	   
    {
      funcdep *fd = fdset_get_funcdep (fds, target);      
      ar_ca_expr *F = funcdep_get_function (fd);

      if (ar_ca_expr_get_kind (F) == AR_CA_AND)
	{
	  added = 1;
	  s_split_target (F, result, fds);
	}
      else
	{
	  ccl_list *vars = ar_ca_expr_get_variables (F, NULL);
	  if (ccl_list_get_size (vars) == 1)
	    {
	      s_split_target (F, result, fds);
	      added = 1;
	    }
	  ccl_list_delete (vars);
	}
    }
  else if (ar_ca_expr_get_kind (target) == AR_CA_NOT)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (target);

      if (ar_ca_expr_get_kind (args[0]) == AR_CA_OR)
	{
	  int i;
	  args = ar_ca_expr_get_args (args[0]);
	  
	  added = 1;
	  for (i = 0; i < 2; i++)
	    {
	      ar_ca_expr *aux = ar_ca_expr_crt_not (args[i]);
	      s_split_target (aux, result, fds);
	      ar_ca_expr_del_reference (aux);
	    }
	}
      else if (ar_ca_expr_get_kind (args[0]) == AR_CA_VAR &&
	       fdset_has_dependency_for (fds, args[0]))	   
	{
	  funcdep *fd = fdset_get_funcdep (fds, args[0]);      
	  ar_ca_expr *F = funcdep_get_function (fd);
	  if (ar_ca_expr_get_kind (F) == AR_CA_OR)
	    {
	      int i;

	      args = ar_ca_expr_get_args (F);	  
	      added = 1;
	      for (i = 0; i < 2; i++)
		{
		  ar_ca_expr *aux = ar_ca_expr_crt_not (args[i]);
		  s_split_target (aux, result, fds);
		  ar_ca_expr_del_reference (aux);
		}
	    }
	}
    }
  else
    {
      ccl_list *vars = ar_ca_expr_get_variables (target, NULL);

      if (ccl_list_get_size (vars) == 1)
	{
	  ar_ca_expr *v = ccl_list_take_first (vars);
	  if (fdset_has_dependency_for (fds, v))
	    {
	      funcdep *fd = fdset_get_funcdep (fds, v);      
	      ar_ca_expr *F = funcdep_get_function (fd);
	      ar_ca_expr *new_target = ar_ca_expr_replace (target, v, F);
	      s_split_target (new_target, result, fds);
	      ar_ca_expr_del_reference (new_target);
	      added = 1;
	    }
	}
      ccl_list_delete (vars);
    }
  
  if (!added && !ccl_list_has (result, target))
    ccl_list_add (result, ar_ca_expr_add_reference (target));
}

/*!
 * Attach to each vertex its connected component (graph is considered as not
 * oriented here). 
 */
static ccl_hash * 
s_compute_cc (ccl_graph *G)
{  
  intptr_t C = 0;
  ccl_vertex_iterator *vi = ccl_graph_get_vertices (G);
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_list *todo = ccl_list_create ();
  
  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *v = ccl_iterator_next_element (vi);

      if (ccl_hash_find (result, v))
	continue;
      ccl_hash_insert (result, (void *) C);
      ccl_list_add (todo, v);

      while (! ccl_list_is_empty (todo))
	{
	  int i;
	  ccl_vertex *v = ccl_list_take_first (todo);
	  ccl_edge_iterator *ei = ccl_vertex_get_out_edges (v);
	  ccl_vertex *(*get_succ)(const ccl_edge *) = ccl_edge_get_tgt;
	  
	  for (i = 0; i < 2; i++)
	    {
	      while (ccl_iterator_has_more_elements (ei))
		{
		  ccl_edge *e = ccl_iterator_next_element (ei);
		  ccl_vertex *succ = get_succ (e);
		  if (! ccl_hash_find (result, succ))
		    {
		      ccl_hash_insert (result, (void *) C);
		      ccl_list_add (todo, succ);
		    }
		}
	      ccl_iterator_delete (ei);
	      if (i == 0)
		{
		  ei = ccl_vertex_get_in_edges (v);
		  get_succ = ccl_edge_get_src;
		}
	    }
	}
      C++;
    }
  ccl_list_delete (todo);
  ccl_iterator_delete (vi);
  
  return result;
}
	      
/*!
 * Split the target into independent parts. 
 * According to the dependency graph of srcca, the conjuction 'target' is 
 * split into sub-conjunctions that can be treated independently.
 * 
 * First the expression 'target' is syntactically split according to Boolean
 * operators; functional dependencies are also used for Boolean variables.
 * The set of expressions obtained this way is not exactly a set of 
 * sub-expressions of 'target' but the conjunction of all these expressions
 * is equivalent to it.
 *
 * Second a connected component index is associated to each sub-expression and
 * sub-expressions are gathered (and conjoined) according to cc indexes.
 */
static ccl_list * 
s_preprocess_target (ar_ca *srcca,  ar_ca_expr *target)
{
  ccl_pointer_iterator *pi;
  ccl_pair *p;
  ccl_hash *ccs;
  ar_ca_exprman *eman = ar_ca_get_expression_manager (srcca);
  depgraph *dg = depgraph_compute (srcca, 1);
  ccl_graph *G = depgraph_get_graph (dg);
  ccl_list *subtargets = ccl_list_create ();
  ccl_hash *subforms;
  const fdset *fds = ar_ca_get_fdset (srcca);
  ccl_list *result = ccl_list_create ();
  
  s_split_target (target, subtargets, fds);
  /* Some variables may have been replaced by their functional dependencies so
   * we have to restrict DG to variables of discovered subtargets.
   */
  {
    ccl_hash *vars = NULL;
    ccl_hash *objects = NULL;
    for (p = FIRST (subtargets); p; p = CDR (p))
      vars = ar_ca_expr_get_variables_in_table (CAR (p), vars);
    objects = depgraph_get_reachables_from_variables (dg, vars);
    ccl_hash_delete (vars);
    depgraph_induced_subgraph (dg, objects);
    ccl_hash_delete (objects);
  }
  if (DBG_PREPROCESS_GRAPH)
    depgraph_log (CCL_LOG_DEBUG, dg);
  
  /* add sub-targets to dependency graph */
  for (p = FIRST (subtargets); p; p = CDR (p))
    {
      ar_ca_expr *e = CAR (p);
      if (ccl_graph_get_vertex (G, e) == NULL)
	{
	  ccl_list *vars = ar_ca_expr_get_variables (e, NULL);
	  ccl_vertex *v = ccl_graph_find_or_add_vertex (G, e);
	  
	  while (!ccl_list_is_empty (vars))
	    {	      
	      ccl_vertex *w =
		ccl_graph_get_vertex (G, ccl_list_take_first (vars));
	      ccl_assert (w != NULL);
	      ccl_graph_add_edge (G, v, w, NULL);
	    }
	  ccl_list_delete (vars);
	}
    }
  
  ccs = s_compute_cc (G);
  subforms = ccl_hash_create (NULL, NULL, NULL, NULL);
  while (! ccl_list_is_empty (subtargets))
    {
      ar_ca_expr *F = ccl_list_take_first (subtargets);
      ccl_vertex *v = ccl_graph_get_vertex (G, F);
      void *comp = ccl_hash_get_with_key (ccs, v);
      ccl_assert (ccl_hash_has (ccs, v));

      if (! ccl_hash_find (subforms, comp))
	ccl_hash_insert (subforms, F);
      else
	{
	  ar_ca_expr *sf = ccl_hash_get (subforms);
 	  ar_ca_expr *t = ar_ca_expr_crt_and (sf, F);
	  ar_ca_expr_del_reference (sf);
	  ar_ca_expr_del_reference (F);
	  ccl_hash_insert (subforms, t);
	}
    }
  ccl_list_delete (subtargets);

  pi = ccl_hash_get_elements (subforms);
  while (ccl_iterator_has_more_elements (pi))
    {
      ar_ca_expr *sf = ccl_iterator_next_element (pi);
      ccl_list_add (result, sf);
      
      if (ccl_debug_is_on && DBG_PREPROCESS_RESULT)
	{	  
	  ccl_vertex *v = ccl_graph_get_vertex (G, sf);
	  void *comp = ccl_hash_get_with_key (ccs, v);
	  ccl_list *vars = ar_ca_expr_get_variables (sf, NULL);
	  
	  ccl_debug ("SUBTARGET %d=", ccl_list_get_size (result));
	  ar_ca_expr_log (CCL_LOG_DEBUG, sf);
	  ccl_debug (" is in comp %p\n", comp + 1);
	  
	  while (! ccl_list_is_empty (vars))
	    {	    
	      ar_ca_expr *var = ccl_list_take_first (vars);
	      ccl_vertex *v = ccl_graph_get_vertex (G, var);
	      void *comp = ccl_hash_get_with_key (ccs, v);
	      ccl_assert (ccl_hash_has (ccs, v));
	      ar_ca_expr_log (CCL_LOG_DEBUG, var);
	      ccl_debug(" is in comp %p\n", comp);
	    }
	  ccl_list_delete (vars);
	}
    }
  ccl_iterator_delete (pi);  
  ccl_hash_delete (subforms);
  ccl_hash_delete (ccs);
  depgraph_delete (dg);
  ar_ca_exprman_del_reference (eman);

  return result;
}

static void
s_log_dd (ccl_log_type log, compiler *C, ar_dd F)
{
  ccl_hash *cache = NULL;

  ar_ca_expr_build_dd_caches (NULL, &cache);
  ar_ca_expr *R = ar_ca_expr_compute_expr_from_dd (C->ddm, C->eman, C->order,
						   F, cache);
  ar_ca_expr_log (log, R);
  ar_ca_expr_del_reference (R);
  ar_ca_expr_clean_up_dd_caches (NULL, cache);
}

static compiler *
s_crt_compiler (const char *prefix, ar_ca *srcca, ccl_set *failures,
		ccl_set *disabled)
{
  ccl_pointer_iterator *pi;
  ccl_graph *G;
  compiler *C = ccl_new (compiler);
  
  CCL_DEBUG_START_TIMED_BLOCK (("compile model"));
  C->eqid = 0;
  C->top_prefix = ccl_string_dup (prefix);
  C->assignment_prefix = ccl_string_format_new ("%sA_", C->top_prefix);
  C->equation_prefix = ccl_string_format_new ("%sE_", C->top_prefix);
						
  C->ca = ar_ca_add_reference (srcca);
  C->eventvars = NULL;
  C->e2vl = NULL;
  ar_ca_add_event_observer (srcca, failures, disabled,
			    &C->eventvars, &C->e2vl, &C->invvars);
  C->eman = ar_ca_get_expression_manager (C->ca);
  C->DG = depgraph_compute (C->ca, 1);
  G = depgraph_get_graph (C->DG);
  C->sccG = ccl_graph_compute_scc_graph (G, &C->v2comp, NULL, NULL);
  C->nb_scc = ccl_graph_get_number_of_vertices (C->sccG);
  C->rs = ar_compute_relational_semantics (C->ca, NULL, NULL);
  C->ddm = ar_relsem_get_decision_diagrams_manager (C->rs);
  C->order = ar_relsem_get_order (C->rs);
  C->B = NULL;
  C->v2bundle = NULL;
  C->qvars = ccl_set_create ();
  C->acache =
    ccl_set_create_for_objects (NULL, NULL, (ccl_delete_proc *)
				ar_ca_expr_del_reference);
  C->ecache =
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *) ar_dd_free_dd,
		     (ccl_delete_proc *) ccl_string_delete);
  C->domaincache =
    ccl_hash_create (NULL, NULL, NULL, 
		     (ccl_delete_proc *) ccl_string_delete);
  C->statevars = ccl_set_create ();

  pi = ar_ca_get_state_variables_iterator (C->ca);
  while (ccl_iterator_has_more_elements (pi))
    {
      ar_ca_expr *v = ccl_iterator_next_element (pi);
      ccl_set_add (C->statevars, v);
    }
  ccl_iterator_delete (pi);

  s_build_bundles (C);

  if (DBG_BUNDLES)
    {
      int i;
      int nb_t = 0;
      for (i = 0; i < C->nb_scc; i++)
	{
	  bundle *b = C->B[i];
	  s_log_bundle (CCL_LOG_DEBUG, C, b);
	  if (ccl_list_get_size (b->transitions))
	    nb_t++;
	  ccl_debug ("\n");
	}
      ccl_debug ("nb trans pack %d\n", nb_t);
    }  
  CCL_DEBUG_END_TIMED_BLOCK ();
  
  return C;
}

static void
s_delete_compiler (compiler *C)
{
  int i;

  ccl_string_delete (C->top_prefix);
  ccl_string_delete (C->assignment_prefix);
  ccl_string_delete (C->equation_prefix);
  if (C->B != NULL)
    {
      for (i = 0; i< C->nb_scc; i++)
	s_delete_bundle (C->B[i]);
      ccl_delete (C->B);
    }
  ccl_zdelete (ccl_hash_delete, C->v2bundle);
  ccl_graph_del_reference (C->sccG);
  depgraph_delete (C->DG);

  ccl_set_delete (C->statevars);

  ccl_set_delete (C->acache);
  ccl_hash_delete (C->ecache);
  ccl_hash_delete (C->domaincache);
  ccl_set_delete (C->eventvars);
  ccl_set_delete (C->invvars);
  ccl_set_delete (C->qvars);
  ccl_hash_delete (C->e2vl);
  ccl_hash_delete (C->v2comp);
  ar_relsem_del_reference (C->rs);
  ar_ca_exprman_del_reference (C->eman);
  ar_ca_del_reference (C->ca);
  ccl_delete (C);
}

static void
s_generate_root_formula (ccl_log_type log, compiler *C, int nb_subtargets)
{
  int i;
  ccl_list *qgates = ccl_list_create ();
  ccl_list *qids = ccl_list_create ();

  if (! ccl_set_is_empty (C->invvars))
    {
      ccl_pointer_iterator *pi = ccl_set_get_elements (C->invvars);

      while (ccl_iterator_has_more_elements (pi))
	{
	  ar_ca_expr *v = ccl_iterator_next_element (pi);
	  char *sv = observer_get_event_name_from_var (v);
	  char *id = ccl_string_format_new ("`%s'", sv);
	  ccl_list_add (qids, id);
	  ccl_string_delete (sv);
	}
      ccl_iterator_delete (pi);
    }
  
  if (! ccl_set_is_empty (C->qvars))
    {
      ccl_pointer_iterator *pi = ccl_set_get_elements (C->qvars);

      while (ccl_iterator_has_more_elements (pi))
	s_generate_domain_of_variable (log, C, ccl_iterator_next_element (pi),
				       qgates, qids);
      ccl_iterator_delete (pi);
    }
  
  ccl_log (log, "%sroot := ", C->top_prefix);

  if (! ccl_list_is_empty (qids))
    {
      char *s = ccl_list_take_first (qids);
      ccl_log (log, "exists %s", s);
      ccl_string_delete (s);

      while (! ccl_list_is_empty (qids))
	{
	  s = ccl_list_take_first (qids);
	  ccl_log (log, ", %s", s);
	  ccl_string_delete (s);
	}
      ccl_log (log, " ");
    }
  ccl_list_delete (qids);
  
  ccl_log (log, "(`%s%s0'", C->top_prefix, PREFIX_FOR_ROOT_GATES);
  for (i = 1; i < nb_subtargets; i++)
    ccl_log (log, " & `%s%s%d'", C->top_prefix, PREFIX_FOR_ROOT_GATES, i);
  while (! ccl_list_is_empty (qgates))
    {
      char *s;
      ccl_log (log, "& %s", s = ccl_list_take_first (qgates));
      ccl_string_delete (s);
    }
  ccl_list_delete (qgates);
  ccl_log (log, ");\n");

  if (ARALIA_COMMANDS != NULL)
    ccl_log (log, ARALIA_COMMANDS);  
}
