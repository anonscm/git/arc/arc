/*
 * diagnosis-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef DIAGNOSIS_P_H
# define DIAGNOSIS_P_H

typedef struct diagnosis_st diagnosis;
typedef struct diagnosis_manager_st diagnosis_manager;

extern diagnosis *
diagnosis_create (diagnosis_manager *dm);

extern void
diagnosis_delete (diagnosis *diag);

extern int 
diagnosis_has (const diagnosis *diag, int trans_index);

extern diagnosis *
diagnosis_clone (const diagnosis *diag);

extern diagnosis *
diagnosis_prefix (const diagnosis *diag, int *p_ti);

extern void 
diagnosis_add (diagnosis *diag, int trans_index);

extern const ccl_list *
diagnosis_get_elements (const diagnosis *diag);

extern void
diagnosis_get_children (const diagnosis *diag, ccl_list *children);

extern int
diagnosis_get_size (const diagnosis *diag);

extern unsigned int 
diagnosis_hash (const diagnosis *diag);

extern int 
diagnosis_compare (const diagnosis *diag, const diagnosis *other);

extern void
diagnosis_log (ccl_log_type log, const diagnosis *diag);

extern diagnosis *
diagnosis_merge (const diagnosis *diag, const diagnosis *other);

extern diagnosis *
diagnosis_parent (const diagnosis *diag, int ti);

extern int 
diagnosis_is_included_in (const diagnosis *diag, const diagnosis *other);

#endif /* ! DIAGNOSIS_P_H */
