/*
 * ar-ca-varorder.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include <deps/fdset-comp.h>
#include <ccl/ccl-graph.h>
#include <ccl/ccl-log.h>
#include "ar-ca-expression.h"
#include "ar-constraint-automaton.h"
#include "ar-ca-p.h"
#include "ar-ca-expr2dd.h"

static void
s_create_equality_links (ar_ca *ca, varorder *order, ccl_hash *index);

static ccl_list *
s_order_variables (ar_ca *ca);

static void 
s_compute_order_from_fds (ar_ca *ca, ccl_list *order, ccl_hash *vars);

static void 
s_compute_order_from_assertions (ar_ca *ca, ccl_list *order, ccl_hash *vars);

static void 
s_compute_order_from_transitions (ar_ca *ca, ccl_list *order, ccl_hash *vars);

static void
s_add_wrt_prefixes (ccl_list *ordered_vars, varorder *order);

			/* --------------- */

int ar_ca_debug_ordering = 1;

void
ar_ca_order_variables (ar_ca *ca, varorder *order)
{
  ccl_pair *p;
  ccl_pointer_iterator *iv;
  ccl_list *ordered_vars;
  ccl_hash *index = ccl_hash_create (NULL, NULL, NULL, NULL);
  uintptr_t i;
  int dd_index;
  
  CA_DBG_F_START_TIMER (ordering, ("order variables"));  
  ordered_vars = s_order_variables (ca);
  s_add_wrt_prefixes (ordered_vars, order);
  dd_index = 0;
  for (i = 0, p = FIRST (ordered_vars); p; p = CDR (p), i++)
    {
      ccl_hash_find (index, CAR (p));
      ccl_hash_insert (index, (void *) i);
    }
  s_create_equality_links (ca, order, index);
  ccl_hash_delete (index);
  
  while (! ccl_list_is_empty (ordered_vars))
    {
      ar_ca_expr *var = ccl_list_take_first (ordered_vars);
      ar_ca_expr *rep = varorder_get_representative (order, var);

      if (var == rep)
	varorder_set_dd_index (order, var, dd_index++);
    }
  ccl_list_delete (ordered_vars);

  iv = varorder_get_variables (order);
  while (ccl_iterator_has_more_elements (iv))
    {
      ar_ca_expr *var = ccl_iterator_next_element (iv);
      int index = varorder_get_dd_index (order, var);

      if (index < 0)
	varorder_set_dd_index (order, var, dd_index++);
    }
  ccl_iterator_delete (iv);
  CA_DBG_F_END_TIMER (ordering);
}

			/* --------------- */

static void
s_add_variable_dependencies (ccl_graph *G, ar_ca_expr *e, ar_ca_expr *v)
{
  ccl_pair *p1;
  ccl_pair *p2;
  ccl_list *vars = ar_ca_expr_get_variables (e, NULL);
  ccl_vertex *vv;

  if (v != NULL)
    {
      vv = ccl_graph_add_vertex (G, v);
      for (p1 = FIRST (vars); p1; p1 = CDR (p1))
	{
	  ccl_vertex *v1 = ccl_graph_find_or_add_vertex (G, CAR (p1));

	  if (! ccl_vertex_has_successor (v1, vv))
	    ccl_graph_add_edge (G, v1, vv, NULL);
	}
    }
  else
    {
      for (p1 = FIRST (vars); p1; p1 = CDR (p1))
	{
	  ccl_vertex *v1 = ccl_graph_find_or_add_vertex (G, CAR (p1));

	  for(p2 = CDR (p1); p2; p2 = CDR (p2))
	    {
	      ccl_vertex *v2 = ccl_graph_find_or_add_vertex (G, CAR (p2));
	      
	      if (v1 == v2)		
		continue;
	      if (! ccl_vertex_has_successor (v1, v2))
		ccl_graph_add_edge (G, v1, v2, NULL);
	      if (! ccl_vertex_has_successor (v2, v1))
		ccl_graph_add_edge (G, v2, v1, NULL);
	    }
	}
    }
  ccl_list_delete (vars);
}

			/* --------------- */

static int
s_is_eq (ar_ca_expr *F)
{
  ar_ca_expr **args = ar_ca_expr_get_args (F);
  ar_ca_expression_kind k = ar_ca_expr_get_kind (F);

  if (k != AR_CA_EQ)
    return 0;
  return  (ar_ca_expr_get_kind (args[0]) == AR_CA_VAR &&
	   ar_ca_expr_get_kind (args[1]) == AR_CA_VAR); 
 }

			/* --------------- */

static ccl_list *
s_order_variables (ar_ca *ca)
{
#if 0
  ccl_list *result = ccl_list_create ();
  ccl_hash *visited = ccl_hash_create (NULL, NULL, NULL, NULL);

  s_compute_order_from_fds (ca, result, visited);
  s_compute_order_from_assertions (ca, result, visited);
  s_compute_order_from_transitions (ca, result, visited);
  
  ccl_hash_delete (visited);
  
  return result;
#else
  ccl_list *tmp = ccl_list_create ();
  ccl_list *result = ccl_list_create ();
  ccl_hash *visited = ccl_hash_create (NULL, NULL, NULL, NULL);
  fdset *fds = fdset_compute (ca, 1, 1);
  ccl_hash *prev = ccl_hash_create (NULL, NULL, NULL, NULL);
  const ccl_graph *dg = fdset_get_dependency_graph (fds);

  s_compute_order_from_assertions (ca, tmp, visited);
  s_compute_order_from_transitions (ca, tmp, visited);
  ccl_hash_delete (visited);  
 
  while (! ccl_list_is_empty (tmp))
    {
      ar_ca_expr *var = ccl_list_take_first (tmp);
      if (! fdset_has_dependency_for (fds, var))
	{
	  ccl_vertex *v = ccl_graph_get_vertex (dg, var);
	  ccl_edge_iterator *ei;
	  if (v == NULL)
	    continue;
	  ei = ccl_vertex_get_in_edges (v);

	  ccl_list_add (result, var);
	  ccl_debug ("add ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, var);
	  ccl_debug ("\n");
	  while (ccl_iterator_has_more_elements (ei))
	    {
	      ccl_edge *e = ccl_iterator_next_element (ei);
	      ccl_vertex *src = ccl_edge_get_src (e);
	      ccl_hash_find (prev, ccl_vertex_get_data (src));
	      ccl_hash_insert (prev, var);
	    }
	  ccl_iterator_delete (ei);
	}
    }
  ccl_list_delete (tmp);
  tmp = fdset_get_ordered_variables (fds, 0);
  while (! ccl_list_is_empty (tmp))
    {
      ar_ca_expr *v = ccl_list_take_first (tmp);
      ar_ca_expr *p = ccl_hash_get_with_key (prev, v);

      if (ccl_debug_is_on && p != NULL)
	{
	  ccl_debug ("insert ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, v);
	  ccl_debug (" after ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, p);
	  ccl_debug ("\n");	
	}
      ccl_list_insert_after (result, p, v);
    }
  ccl_list_delete (tmp);
  ccl_hash_delete (prev);
  fdset_del_reference (fds);
  
  return result;
#endif
}

			/* --------------- */

static void
s_collect_identical_variables (ar_ca_expr *F, varorder *order, ccl_hash *index)
{
  ar_ca_expr **args;
  ar_ca_expression_kind k = ar_ca_expr_get_kind (F);

  if (k != AR_CA_AND && k != AR_CA_EQ)
    return;

  args = ar_ca_expr_get_args (F);
  if (k == AR_CA_AND)
    {
      s_collect_identical_variables (args[0], order, index);
      s_collect_identical_variables (args[1], order, index);
    }
  else if (ar_ca_expr_get_kind (args[0]) == AR_CA_VAR &&
	   ar_ca_expr_get_kind (args[1]) == AR_CA_VAR && 
	   args[0] != args[1])
    {
      const ar_ca_domain *D1 = ar_ca_expr_variable_get_domain (args[0]);
      const ar_ca_domain *D2 = ar_ca_expr_variable_get_domain (args[1]);
      
      if (ar_ca_domain_equals (D1, D2))
	{
	  int i;
	  uintptr_t k1 = (uintptr_t) ccl_hash_get_with_key (index, args[0]);
	  uintptr_t k2 = (uintptr_t) ccl_hash_get_with_key (index, args[1]);
	  if (k1 < k2) i = 0;
	  else i = 1;
	  
	  varorder_merge (order, args[i], args[(i + 1) & 0x1]);
	}
    }
}

			/* --------------- */

static void
s_create_equality_links (ar_ca *ca, varorder *order, ccl_hash *index)
{
  const ccl_pair *p;
  const ccl_list *assertions = ar_ca_get_assertions (ca);

  for (p = FIRST (assertions); p; p = CDR (p))
    s_collect_identical_variables (CAR (p), order, index);
}


			/* --------------- */

static void
s_counter_scc_card (ccl_graph *G, ccl_vertex *v, ccl_vertex *sccv, void *data)
{
  intptr_t card = 1;

  if (ccl_hash_find (data, sccv))
    card += (intptr_t) ccl_hash_get (data);
  ccl_hash_insert (data, (void *) card);
  
  ccl_assert (! ccl_hash_find (data, v));  
  ccl_hash_find (data, v);
  ccl_hash_insert (data, sccv);
}

static void
s_scc_dfs_rec (ccl_vertex *v, ccl_set *visited, ccl_hash *cards)
{
  ccl_vertex_iterator *succ =ccl_vertex_get_successors (v);
  intptr_t lcard = (intptr_t) ccl_hash_get_with_key (cards, v);

  ccl_set_add (visited, v);
  ccl_assert (lcard > 0);
    
  while (ccl_iterator_has_more_elements (succ))
    {
      ccl_vertex *s = ccl_iterator_next_element (succ);
      if (! ccl_set_has (visited, s))
	s_scc_dfs_rec (s, visited, cards);
      lcard += (intptr_t) ccl_hash_get_with_key (cards, s);
    }
  ccl_iterator_delete (succ);
}

static ccl_hash * 
s_compute_weights (ccl_graph *G)
{
  intptr_t totalcard = 0;
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_hash *cards = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_graph *sccG =
    ccl_graph_compute_scc_graph (G, NULL, s_counter_scc_card, cards);
  ccl_set *visited = ccl_set_create ();
  ccl_vertex_iterator *vi = ccl_graph_get_roots (sccG);

  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *r = ccl_iterator_next_element (vi);
      ccl_assert (! ccl_set_has (visited, r));
      s_scc_dfs_rec (r, visited, cards);
      totalcard += (intptr_t) ccl_hash_get_with_key (cards, r);
    }

  ccl_iterator_delete (vi);
  ccl_set_delete (visited);
  ccl_graph_del_reference (sccG);
  
  ccl_assert (totalcard == ccl_graph_get_number_of_vertices (G));

  vi = ccl_graph_get_vertices (G);
  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *v = ccl_iterator_next_element (vi);
      ccl_vertex *c = ccl_hash_get_with_key (cards, v);
      intptr_t card = (intptr_t) ccl_hash_get_with_key (cards, c);

      ccl_assert (! ccl_hash_find (result, v));
      ccl_hash_find (result, v);
      ccl_hash_insert (result, (void *) card);
    }
  ccl_iterator_delete (vi);
  ccl_hash_delete (cards);

  return result;
}

static int
s_cmp_weights (const void *v1, const void *v2, const void *data)
{
  intptr_t w1, w2;

  w1 = (intptr_t) ccl_hash_get_with_key (data, v1);
  w2 = (intptr_t) ccl_hash_get_with_key (data, v2);

  return w2 - w1;
}

			/* --------------- */

static int
s_cmp_reverse (const void *p1, const void *p2)
{
  if (p1 < p2) return 1;
  if (p1 > p2) return -1;
  return 0;
}

static ccl_list * 
s_order_variables_by_weights (ar_ca *ca, const ccl_list *vars,
			      ccl_hash *weights, ccl_graph *G)
{
  const ccl_pair *p;
  ccl_list *result = ccl_list_create ();
  ccl_hash *cardmap = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_list *aux;
  ccl_pointer_iterator *pi;
  
  CCL_LIST_FOREACH (p, vars)
    {
      intptr_t w;
      ccl_list *vertices;
      ccl_vertex *v = ccl_graph_get_vertex (G, CAR (p));

      if (v == NULL)
	continue;

      ccl_hash_find (weights, v);
      w = (intptr_t) ccl_hash_get (weights);

      if (w == 1)
	continue;
      if (ccl_hash_find (cardmap, (void *) w))
	vertices = ccl_hash_get (cardmap);
      else
	{
	  vertices = ccl_list_create ();
	  ccl_hash_insert (cardmap, vertices);
	}
      ccl_list_add (vertices, v);
    }
  pi = ccl_hash_get_keys (cardmap);
  aux = ccl_list_create_from_iterator (pi);
  ccl_iterator_delete (pi);
  
  ccl_list_sort (aux, s_cmp_reverse);
  
  while (!ccl_list_is_empty (aux))
    {
      void *card = ccl_list_take_first (aux);
      ccl_list *vertices = ccl_hash_get_with_key (cardmap, card);
      ccl_assert (vertices != NULL);
      ccl_list_append (result, vertices);
      ccl_list_delete (vertices);
    }
  ccl_list_delete (aux);
  ccl_hash_delete (cardmap);
  
  return result;
}


static ccl_list * 
s_order_flow_variables_by_weights (ar_ca *ca, ccl_hash *weights, ccl_graph *G)
{
  return s_order_variables_by_weights (ca, ar_ca_get_flow_variables (ca),
				       weights, G);
}

			/* --------------- */
static ccl_list * 
s_order_state_variables_by_weights (ar_ca *ca, ccl_hash *weights, ccl_graph *G)
{
  return s_order_variables_by_weights (ca, ar_ca_get_state_variables (ca),
				       weights, G);
}

			/* --------------- */

static void
s_backward_bfs (ccl_vertex *root, ccl_graph *G, ccl_hash *visited, 
		ccl_list *order)
{
  ccl_set *inq = ccl_set_create ();
  ccl_list *todo = ccl_list_create ();

  ccl_list_add (todo, root);
  ccl_set_add (inq, root);
  
  while (! ccl_list_is_empty (todo))
    {
      ccl_edge_iterator *in;
      ccl_vertex *v = ccl_list_take_first (todo);
      ar_ca_expr *var = ccl_vertex_get_data (v);
      
      if (ccl_hash_find (visited, var))
	continue;
      ccl_hash_insert (visited, var);
      ccl_list_put_first (order, var);
      in = ccl_vertex_get_in_edges (v);

      while (ccl_iterator_has_more_elements (in))
	{
	  ccl_edge *e = ccl_iterator_next_element (in);
	  ccl_vertex *src = ccl_edge_get_src (e);
	  if (! ccl_set_has (inq, src))
	    {
	      ccl_list_add (todo, src);
	      ccl_set_add (inq, src);
	    }
	}
      ccl_iterator_delete (in);
    }
  ccl_list_delete (todo);
  ccl_set_delete (inq);
}

			/* --------------- */

static void
s_compute_order_from_assertion_graph (ar_ca *ca, ccl_graph *G, ccl_list *order,
				      ccl_hash *visited)
{
  ccl_hash *weights = s_compute_weights (G);
  ccl_list *flow_variables =
    s_order_flow_variables_by_weights (ca, weights, G);
  ccl_list *state_variables =
    s_order_state_variables_by_weights (ca, weights, G);
  ccl_hash_delete (weights);
  
  while (! ccl_list_is_empty (flow_variables))
    {
      ccl_vertex *v = ccl_list_take_first (flow_variables);
      s_backward_bfs (v, G, visited, order);
    }
  ccl_list_delete (flow_variables);

  while (! ccl_list_is_empty (state_variables))
    {
      ccl_vertex *v = ccl_list_take_first (state_variables);
      s_backward_bfs (v, G, visited, order);
    }
  ccl_list_delete (state_variables);
}

			/* --------------- */

static void 
s_compute_order_from_assertions (ar_ca *ca, ccl_list *order, ccl_hash *vars)
{
  CA_DBG_F_START_TIMER (ordering, ("ordering wrt assertions"));
  {
    const ccl_pair *p;
    /* V1 -> V2 means that V2 depends on V1 */
    ccl_graph *G = ccl_graph_create_default ();
    const ccl_list *constraints = ar_ca_get_assertions (ca);

    CA_DBG_F_START_TIMER (ordering, ("build dependency graph"));
    for (p = FIRST (constraints); p; p = CDR (p))
      {
	ar_ca_expr *c = CAR (p);
	if (! s_is_eq (c))
	  s_add_variable_dependencies (G, c, NULL);
      }
    CA_DBG_F_END_TIMER (ordering);

    CA_DBG_F_START_TIMER (ordering, ("order variables of assertions"));
    s_compute_order_from_assertion_graph (ca, G, order, vars);
    CA_DBG_F_END_TIMER (ordering);
    
    ccl_graph_del_reference (G);
  }
  CA_DBG_F_END_TIMER (ordering);
}

			/* --------------- */

static int
s_cmp_trans (const void *t1, const void *t2, void *data)
{
  intptr_t w1, w2;

  w1 = (intptr_t) ccl_hash_get_with_key (data, t1);
  w2 = (intptr_t) ccl_hash_get_with_key (data, t2);

  return w2 - w1;
}

			/* --------------- */

static ccl_list *
s_order_transition_by_weight (ar_ca *ca, ccl_hash *visited,
			      ccl_hash **p_t2varlist)
{
  int i;
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  ccl_hash *vars_per_trans = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
		     ccl_list_delete);
  ccl_hash *weights = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_list *translist = ccl_list_create ();

  for (i = 0; i < nb_trans ; i++)
    {
      int j;
      intptr_t w;
      ccl_pair *p;
      ar_ca_trans *t = trans[i];
      int nb_a = ar_ca_trans_get_number_of_assignments(t);
      ar_ca_expr **a = ar_ca_trans_get_assignments(t);
      ar_ca_expr *g = ar_ca_trans_get_guard (t);
      ccl_list *vars = NULL;

      vars = ar_ca_expr_get_variables (g, vars);
      for (j = 0; j < nb_a; j++)
	{
	  ar_ca_expr *val = a[2 * j + 1];
	  vars = ar_ca_expr_get_variables (val, vars);
	  vars = ar_ca_expr_get_variables (a[2 * j], vars);
	}

      ar_ca_expr_del_reference (g);
      for (w = 0, p = FIRST (vars); p; p = CDR (p))
	{
	  if (! ccl_hash_find (visited, CAR (p)))
	    w++;
	}

      if (0){
	ccl_list *gvars = ar_ca_expr_get_variables (g, NULL);
	for (j = 0; w > 0 && j < nb_a; j++)
	  {
	    if (! ccl_list_has (gvars, a[2 * j]))
	      w--;
	  }
	ccl_list_delete (gvars);
      }

      if (w == 0)
	ccl_list_delete (vars);
      else
	{
	  ccl_list_add (translist, t);
	  ccl_hash_find (weights, t);
	  ccl_hash_insert (weights, (void *) w);
	  ccl_hash_find (vars_per_trans, t);
	  ccl_hash_insert (vars_per_trans, vars);
	}

    }

  ccl_list_sort_with_data (translist, s_cmp_trans, weights);
  ccl_hash_delete (weights);

  if (p_t2varlist == NULL)
    ccl_hash_delete (vars_per_trans);
  else
    *p_t2varlist = vars_per_trans;

  return translist;
}

			/* --------------- */

static int
s_ignore (ar_ca_expr *var)
{
  ar_identifier *n = ar_ca_expr_variable_get_name (var);
  char *s = ar_identifier_to_string (n);
  int res = (strchr (s, '$') != NULL);
  ar_identifier_del_reference (n);
  ccl_string_delete (s);

  return res;
}

			/* --------------- */

static void 
s_compute_order_from_transitions (ar_ca *ca, ccl_list *order, ccl_hash *visited)
{
  ccl_hash *t2varlist;
  ccl_list *translist;
  ccl_hash *ignored;

  CA_DBG_F_START_TIMER (ordering, ("ordering wrt transitions"));
  
  t2varlist = NULL;
  translist = s_order_transition_by_weight (ca, visited, &t2varlist);
  ignored = ccl_hash_create (NULL, NULL, NULL, NULL);

  while (! ccl_list_is_empty (translist))
    {
      ccl_list *vars;
      ar_ca_trans *t = ccl_list_take_first (translist);
      ccl_list *stack = ccl_list_create ();
      ar_ca_expr *pred = NULL;

      ccl_hash_find (t2varlist, t);
      vars = ccl_hash_get (t2varlist);

      while (! ccl_list_is_empty (vars))
	{
	  ar_ca_expr *var = ccl_list_take_first (vars);

	  if (s_ignore (var))
	    {
	      if (! ccl_hash_find (ignored, var))
		ccl_hash_insert (ignored, t);
	      continue;
	    }

	  if (ccl_hash_find (visited, var))
	    {
	      if (ccl_list_is_empty (stack))
		pred = var;
	      else 
		{
		  while (! ccl_list_is_empty (stack))
		    {
		      ar_ca_expr *tmp = ccl_list_take_first (stack);
		      ccl_hash_find (visited, tmp);
		      ccl_hash_insert (visited, tmp);
		      ccl_list_insert_before (order, var, tmp);		      
		      var = tmp;
		    }
		}
	    }
	  else if (pred == NULL)
	    ccl_list_put_first (stack, var);
	  else
	    {
	      ccl_assert (ccl_list_is_empty (stack));
	      ccl_hash_find (visited, var);
	      ccl_hash_insert (visited, var);
	      ccl_list_insert_after (order, pred, var);
	    }
	}

      if (! ccl_list_is_empty (stack))
	{
	  ar_ca_expr *var = ccl_list_take_first (stack);

	  ccl_list_add (order, var);
	  ccl_hash_find (visited, var);
	  ccl_hash_insert (visited, var);
	  while (! ccl_list_is_empty (stack))
	    {
	      ar_ca_expr *tmp = ccl_list_take_first (stack);
	      ccl_hash_find (visited, tmp);
	      ccl_hash_insert (visited, tmp);
	      ccl_list_insert_before (order, var, tmp);
	      var = tmp;
	    }
	}
      ccl_list_delete (stack);
    }
  ccl_hash_delete (t2varlist);
  ccl_list_delete (translist);

  if (ccl_hash_get_size (ignored) > 0)
    {
      ccl_hash_entry_iterator *ki = ccl_hash_get_entries (ignored);
      while (ccl_iterator_has_more_elements (ki))
	{
	  int i;
	  ccl_hash_entry e = ccl_iterator_next_element (ki);
	  ar_ca_expr *var = e.key;
	  ar_ca_trans *t = e.object;
	  int nb_a = ar_ca_trans_get_number_of_assignments(t);
	  ar_ca_expr **a = ar_ca_trans_get_assignments(t);

	  for (i = 0; i < nb_a; i++)
	    {
	      if (a[2 * i] == var)
		break;
	    }

	  if (i > 0)
	    ccl_list_insert_after (order, a[2 * i - 2], var);
	}
      ccl_iterator_delete (ki);
    }
  ccl_hash_delete (ignored);
  CA_DBG_F_END_TIMER (ordering);
}

			/* --------------- */
#if 1
struct varorder_display_data
{
  ccl_hash *svars;
  ccl_log_type log;
};

static void
s_display_vertex (ccl_vertex *v, void *data)
{  
  struct varorder_display_data *vdd = data;
  ccl_log_type log = vdd->log;
  ccl_hash *svars = vdd->svars; 
  ar_ca_expr *var = ccl_vertex_get_data (v);
  ccl_edge_iterator *out = ccl_vertex_get_out_edges (v);

  ccl_log (log, "\"");
  ar_ca_expr_log (log, var);
  ccl_log (log, "\"");
  if (svars && ccl_hash_find (svars, var))
    ccl_log (log, "[style=filled,fillcolor=red];\n");
  
  while (ccl_iterator_has_more_elements (out))
    {
      ccl_edge *e = ccl_iterator_next_element (out);
      ccl_vertex *t = ccl_edge_get_tgt (e);
      ar_ca_expr *tar = ccl_vertex_get_data (t);
      
      ccl_log (log, "\"");
      ar_ca_expr_log (log, var);
      ccl_log (log, "\" -> \"");
      ar_ca_expr_log (log, tar);
      ccl_log (log, "\";\n");
    }
  ccl_iterator_delete (out);
}

static void
s_log_graph (ccl_log_type log, ccl_graph *G, ccl_hash *svars)
{
  struct varorder_display_data vdd = { svars, log };

  ccl_log (log, "digraph G%p { ranksep=3;\n", G);
  ccl_graph_map_vertices (G, s_display_vertex, &vdd);
  ccl_log (log, "}\n");
}
#endif

static void 
s_compute_order_from_fds (ar_ca *ca, ccl_list *order, ccl_hash *visited)
{
  const fdset *fds = ar_ca_get_fdset (ca);
  ccl_list *l = fdset_get_ordered_variables (fds, 1);
  while (! ccl_list_is_empty (l))
    {
      ar_ca_expr *v = ccl_list_take_first (l);
      ar_ca_expr_log (CCL_LOG_DEBUG, v);
      ccl_debug ("\n");
      
      if (! ccl_hash_find (visited, v))
	{
	  ccl_list_add (order, v);
	  ccl_hash_insert (visited, v);
	}
    }
  ccl_list_delete (l);
}

static void
s_add_wrt_prefixes (ccl_list *ordered_vars, varorder *order)
{
  ccl_pair *p;
  ccl_pointer_iterator *iv;
  ar_idtable *prefixes;
  ccl_set *already_ordered;
  
  if (varorder_get_size (order) == ccl_list_get_size (ordered_vars))
    return;
  
  already_ordered = ccl_set_create ();
  prefixes = ar_idtable_create (0, NULL, NULL);
  
  for (p = FIRST (ordered_vars); p; p = CDR (p))
    {
      ar_ca_expr *var = CAR (p);
      const ar_identifier *vname = ar_ca_expr_variable_get_const_name (var);
      ar_identifier *pref = ar_identifier_get_prefix (vname);

      ccl_set_add (already_ordered, var);
      if (! ar_idtable_has (prefixes, pref))
	ar_idtable_put (prefixes, pref, var);
      ar_identifier_del_reference (pref);      
    }

  iv = varorder_get_variables (order);
  while (ccl_iterator_has_more_elements (iv))
    {
      ar_ca_expr *var = ccl_iterator_next_element (iv);
      if (! ccl_set_has (already_ordered, var))
	{
	  const ar_identifier *vname = ar_ca_expr_variable_get_const_name (var);
	  ar_identifier *pref = ar_identifier_get_prefix (vname);

	  if (ar_idtable_has (prefixes, pref))
	    {
	      ar_ca_expr *prev = ar_idtable_get (prefixes, pref);

	      if (ccl_debug_is_on)
		{
		  ccl_debug ("insert ");
		  ar_ca_expr_log (CCL_LOG_DEBUG, var);
		  ccl_debug (" after ");
		  ar_ca_expr_log (CCL_LOG_DEBUG, prev);
		  ccl_debug ("\n");	
		}
	      ccl_list_insert_after (ordered_vars, prev, var);
	    }
	  else
	    {
	      ar_idtable_put (prefixes, pref, var);
	      ccl_list_add (ordered_vars, var);
	      ccl_set_add (already_ordered, var);
	    }
	  ar_identifier_del_reference (pref);
	}
    }
  ccl_iterator_delete (iv);
  ccl_set_delete (already_ordered);
  ar_idtable_del_reference (prefixes);
}
