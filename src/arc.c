/*
 * arc.c -- Main routine of ARC.
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "arc.h"
#include "commands/allcmds.h"
#include <ccl/ccl-exception.h>
#include <ccl/ccl-init.h>
#include <ccl/ccl-rusage.h>
#include <ccl/ccl-time.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-config-table.h>
#include "ar-identifier.h"
#include "ar-model.h"
#include "ar-semantics.h"
#include "ar-translators.h"
#include "ar-acheck.h"
#include "mec5/mec5.h"
#include "ar-help.h"
#include "arc-shell.h"
#include "arc-shell-preferences.h"
#include "arc-readline.h"
#include "ar-util.h"
#include "arc-debug.h"
#include "commands/stepper.h"
#include "commands/timer.h"
#include "ctlstar.h"

ccl_config_table *ARC_PREFERENCES = NULL;

static char *RC_FILE = NULL;
static FILE *LOG_FILE = NULL;

static int newline = 1;
static long start_time;

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (LOG_FILE != NULL)
    {
      if (newline)
	{
	  const char *log = "???";
	  switch (type) 
	    {
	    case CCL_LOG_DISPLAY: log = "info"; break;
	    case CCL_LOG_WARNING: log = "warning"; break;
	    case CCL_LOG_ERROR: log = "error"; break;
	    case CCL_LOG_DEBUG: log = "debug"; break;
	    case CCL_LOG_PANIC: log = "panic"; break;
	    default:
	      fprintf (stderr, "internal error\n");
	      abort ();
	    }
	  fprintf (LOG_FILE, "%s : ", log);
	}
      fprintf (LOG_FILE, "%s", msg);
      fflush (LOG_FILE);      
      newline = (msg != NULL && msg[strlen(msg) - 1] == '\n');
    }

  if (type == CCL_LOG_DISPLAY)
    {
      fprintf (stdout, "%s", msg);
      fflush (stdout);
      return;
    }

  if (type == CCL_LOG_WARNING)
    {
      if (ccl_config_table_get_boolean (ARC_PREFERENCES, ARC_SHELL_VERBOSE))
	{
	  fprintf (stdout, "%s", msg);
	  fflush (stdout);
	}
      return;
    }

  fprintf(stderr,"%s",msg);
}

			/* --------------- */



void
arc_terminate (void)
{
  long cputime, maxrss;
  int rusage_at_exit =
    ccl_config_table_get_boolean (ARC_PREFERENCES, ARC_SHELL_RUSAGE_AT_EXIT);

  timer_cmd_terminate ();
  stepper_cmd_terminate ();
  ctlstar_terminate ();
  ar_mec5_terminate ();
  ar_acheck_terminate ();
  ar_translators_terminate ();
  ar_semantics_terminate ();
  ar_node_terminate ();
  ar_model_terminate ();
  arc_shell_terminate ();
  ar_identifier_terminate ();
  ar_dd_terminate ();
  ar_help_terminate ();
  ar_util_terminate ();
  arc_debug_terminate ();
  ccl_string_delete (RC_FILE);
  if (LOG_FILE != NULL)
    {
      fflush (LOG_FILE);
      fclose (LOG_FILE);
    }
  
  if (rusage_at_exit)
    {
      ccl_getrusage (&cputime, &maxrss);
      ccl_display ("ru_time=%ld\nru_cputime=%ld\nru_maxrss=%ld\n",
		   ccl_time_get_time () - start_time,
		   cputime, maxrss);
    }
  ccl_config_table_del_reference (ARC_PREFERENCES);
  ccl_terminate ();
}

void
arc_init (void)
{  
  ccl_init ();
  start_time = ccl_time_get_time ();

  if( getenv(ENV_ARCRC) != NULL)
    RC_FILE = ar_tilde_substitution(getenv(ENV_ARCRC));
  else 
    RC_FILE = ar_tilde_substitution(ARCRC_FILE);

  ccl_log_add_listener(s_listener,NULL);
  arc_load_preferences();

  arc_debug_init (ARC_PREFERENCES);
  ar_help_init ();
  ar_util_init ();
  ar_dd_init ();
  ar_identifier_init();
  arc_shell_init ();
  ar_model_init();
  ar_node_init ();
  ar_semantics_init(ARC_PREFERENCES);
  ar_translators_init(ARC_PREFERENCES);
  ar_acheck_init (ARC_PREFERENCES);
  ar_mec5_init (ARC_PREFERENCES);
  ctlstar_init ();
  stepper_cmd_init (ARC_PREFERENCES);
  timer_cmd_init (ARC_PREFERENCES);
}

void
arc_save_preferences(void)
{
  FILE *output = fopen (RC_FILE,"w");

  if (output != NULL)
    {
      ccl_config_table_save(ARC_PREFERENCES,output);
      fflush(output);
      fclose(output);
    }
}

static int
s_execute_test_suite (void)
{
  const char *val = getenv ("ARC_TEST_SUITE");

  if (val != NULL)
    return (strcmp (val, "1") == 0 || strcmp (val, "true") == 0);
  return 0;
}

static void
s_set_initial_prefs (ccl_config_table *table)
{
  if (! s_execute_test_suite ())
    {
      char *datadir = getenv ("ARC_DATADIR");
      char *defprefs = ccl_string_format_new ("%s/" ARC_DEFAULT_PREFERENCES,
					  datadir);
      FILE *input = fopen (defprefs, "r");  
      if (input == NULL)
	{
	  input = fopen (ARC_DATADIR "/" ARC_DEFAULT_PREFERENCES, "r");
	  if (input == NULL)
	    fprintf (stderr, "can't load default preferences '%s/%s'.\n",
		     ARC_DATADIR, ARC_DEFAULT_PREFERENCES);
	}
      
      if (input != NULL)
	{
	  ccl_config_table_load (ARC_PREFERENCES, input);
	  fclose (input);
	}
      ccl_string_delete (defprefs);
    }
}

void
arc_load_preferences (void)
{
  FILE *input;

  if (ARC_PREFERENCES == NULL)
    ARC_PREFERENCES = ccl_config_table_create ();

  s_set_initial_prefs (ARC_PREFERENCES);
  input = fopen (RC_FILE, "r");
  if (input != NULL)
    {
      ccl_config_table_load (ARC_PREFERENCES, input);
      fclose (input);
    }
}

void
arc_set_logfile (FILE *file)
{
  if (LOG_FILE != NULL)
    {
      fflush (LOG_FILE);
      fclose (LOG_FILE);
    }
  LOG_FILE = file;
}
  
