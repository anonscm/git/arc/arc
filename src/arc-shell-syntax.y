/*
 * arc-shell-syntax.y -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

%{
#include <ccl/ccl-memory.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-string.h>
#include "parser/altarica-tree.h"
#include "arc-shell.h"

extern int arc_shell_parser_lex(void);
extern void arc_shell_parser_error(const char *error);

#define YYSTYPE ccl_parse_tree *

# define NODES (ARC_SHELL_CURRENT_CONTEXT->nodes)

#define NN(_type,_vtype,_child,_next) \
  (NODES=ccl_parse_tree_create(_type,# _type, _vtype,	\
       ARC_SHELL_CURRENT_CONTEXT->line,\
       ARC_SHELL_CURRENT_CONTEXT->filename,\
                 _child,_next,NODES))

#define NE(_type,_child,_next) NN(_type,CCL_PARSE_TREE_EMPTY,_child,_next)

/*
#define NID()  NN(ARC_SHELL_ID,CCL_PARSE_TREE_IDENT,NULL,NULL)
*/

#define NINT() NN(AR_TREE_INTEGER,CCL_PARSE_TREE_INT,NULL,NULL)
#define NFLT() NN(AR_TREE_FLOAT,CCL_PARSE_TREE_FLOAT,NULL,NULL)
#define NSTR() NN(AR_TREE_STRING,CCL_PARSE_TREE_STRING,NULL,NULL)

#define malloc  ccl_malloc
#define realloc ccl_realloc
#define calloc  ccl_calloc
#define free    ccl_free

# define CLR_NODES(_nodes) \
do { if( _nodes != NULL ) ccl_parse_tree_delete_container(_nodes); \
_nodes = NULL; } while(0)

# define CLEAR_NODES() CLR_NODES(NODES)
%}

/* %token TOK_IDENTIFIER */
%token TOK_STRING TOK_INTEGER TOK_FLOAT TOK_EOF TOK_ASSIGN TOK_SUPSUP
%token TOK_SEMICOLON TOK_SUP TOK_PIPE

%start start

%%

start : axiom TOK_EOF  { CLEAR_NODES(); arc_shell_exit(); YYACCEPT; }
      | TOK_EOF  { CLEAR_NODES(); arc_shell_exit(); YYACCEPT; }
;

axiom : axiom commands { NODES = NULL;   
                   if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
                      YYACCEPT; }
      | axiom '\n'     { arc_shell_pop_prompt_level();
                   if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
		     YYACCEPT; } 
      | commands { NODES = NULL;   
                   if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
                      YYACCEPT; }
      | '\n'     { arc_shell_pop_prompt_level();
                   if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
		     YYACCEPT; } 
;

commands: 
command_list newline
{ 
  if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
    arc_shell_pop_prompt_level(); 
  arc_shell_interp($1);
  CLEAR_NODES();
  if( ARC_SHELL_CURRENT_CONTEXT->terminated )
    YYACCEPT;
}
;

command_list : 
command_or_assignment 
{ $$ = $1; }
| command_or_assignment TOK_SEMICOLON newline_list command_list 
{ $$ = $1; $1->next = $4; }
| command_or_assignment TOK_SEMICOLON TOK_EOF
{ $$ = $1; $1->next = NULL; }
;

command_or_assignment :
  command_or_assignment_without_redirect TOK_SUP string
  { $$ = NE(AR_TREE_SHELL_REDIR_CREATE,$3,NULL); $3->next = $1; }
| command_or_assignment_without_redirect TOK_SUPSUP string
  { $$ = NE(AR_TREE_SHELL_REDIR_APPEND,$3,NULL); $3->next = $1; }
| command_or_assignment_without_redirect TOK_PIPE string
  { $$ = NE(AR_TREE_SHELL_REDIR_PIPE,$3,NULL); $3->next = $1; }
| command_or_assignment_without_redirect 
  { $$ = $1; }
;

command_or_assignment_without_redirect : 
  command 
  /*| assignment */

;

command :  
  command_name parameter_list
  { $$ = NE(AR_TREE_SHELL_CMD,$1,NULL); $1->next = $2;  }
;

command_name :
  string
;

;
/*
assignment :
  identifier TOK_ASSIGN command 
  { $$ = NE(AR_TREE_SHELL_ASSIGNMENT,$1,NULL); $1->next = $3;  }
;
*/

parameter_list :
  parameter parameter_list
{ $$ = $1; $1->next = $2; }
| /* empty */
{ $$ = NULL; }
;

parameter : 
  integer 
| string 
| real
;

integer : TOK_INTEGER
{ $$ = NINT(); $$->value = ARC_SHELL_CURRENT_CONTEXT->current_tok_value; }
;

real : TOK_FLOAT
{ $$ = NFLT(); $$->value = ARC_SHELL_CURRENT_CONTEXT->current_tok_value; }
;

string : TOK_STRING
{ $$ = NSTR(); $$->value = ARC_SHELL_CURRENT_CONTEXT->current_tok_value; }
;

newline_list : newline_list '\n' | /* empty */ ;
newline : '\n' | TOK_EOF;
%%

