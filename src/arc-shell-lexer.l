/*
 * arc-shell-lexer.l -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

%{
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-assert.h>
#include "ar-model.h"
#include "arc-readline.h"
#include "arc-shell.h"
#include "arc-shell-preferences.h"
#include "arc-shell-syntax.h"
#include "arc.h"

#define malloc  ccl_malloc
#define realloc ccl_realloc
#define calloc  ccl_calloc
#define free    ccl_free
%}

%option  always-interactive
%option  nounput

NUMBER	      [+-]?[0-9][0-9]*
QUOTED_STRING \"(\\.|[^\"])*\"
STRING        [^ :><\t\r\n;"()][^:<>() \t\r\n;"]*

%%

":="         { return TOK_ASSIGN; }
">>"         { return TOK_SUPSUP; }
">"          { return TOK_SUP; }
"|"          { return TOK_PIPE; }
";"          { return TOK_SEMICOLON; }
"#".*        ;
<<EOF>>      { return TOK_EOF; }

{NUMBER}     { 
  double value = atof (arc_shell_parser_text); 

  if (value > INT_MAX)
    {
       ARC_SHELL_CURRENT_CONTEXT->current_tok_value.flt_value = value;
       return TOK_FLOAT; 
    } 
  else
    {
       ARC_SHELL_CURRENT_CONTEXT->current_tok_value.int_value = 
           atoi(arc_shell_parser_text); 
       return TOK_INTEGER; 
    }
}

{STRING}     { 
  ARC_SHELL_CURRENT_CONTEXT->current_tok_value.string_value = 
    ccl_string_dup(arc_shell_parser_text); 
  return TOK_STRING; 
}

{QUOTED_STRING} { 
  int i, k;
  for(i = 1, k = 0; i < yyleng - 1; i++, k++)
    {
      if (yytext[i] == '\\')/* && yytext[i+1] == '"')*/
        i++;
      yytext[k] = yytext[i];
    }
  yytext[k] = '\0';
  ARC_SHELL_CURRENT_CONTEXT->current_tok_value.string_value = 
    ccl_string_dup(arc_shell_parser_text); 
  return TOK_STRING; 
}

<*>\n	     { ARC_SHELL_CURRENT_CONTEXT->line++; return '\n'; }
[ \t\r]	     ;
.            { return yytext[0]; }
%%

int
arc_shell_parse(void)
{
   return arc_shell_parser_parse();
}


			/* --------------- */

void
arc_shell_parser_error(const char *message) 
{
  arc_shell_error("%s (on token '%s')\n",message,arc_shell_parser_text);
}

			/* --------------- */

int
arc_shell_parser_wrap(void)
{
  if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
    {
      char *buf;
      const char *prompt;
      const char *promptpref;

      ccl_assert (ARC_SHELL_CURRENT_CONTEXT->prompt_level <= 1);

      if (ARC_SHELL_CURRENT_CONTEXT->prompt_level == 0)
         promptpref = ARC_SHELL_PROMPT_0;
      else
         promptpref = ARC_SHELL_PROMPT_1;
      prompt = ccl_config_table_get(ARC_PREFERENCES, promptpref);
      buf = 
	arc_readline_get_line (! ARC_SHELL_CURRENT_CONTEXT->cr_mode, prompt);

      ARC_SHELL_CURRENT_CONTEXT->prompt_level = 1;
    
      if( buf == NULL )
	return 1;

      yy_delete_buffer(ARC_SHELL_CURRENT_CONTEXT->state);
      ARC_SHELL_CURRENT_CONTEXT->state = yy_scan_string(buf);
      ccl_string_delete(buf);
    } 
  else 
    {
      return 1;
    }

  return 0;
}

			/* --------------- */

void
arc_shell_interactive_loop(int cr_mode)
{
  arc_shell_context c = ccl_new(struct arc_shell_context_st);

  c->next = ARC_SHELL_CURRENT_CONTEXT;
  c->filename = "<user input>";
  c->line = 1;

  c->nodes = NULL;
  c->stream = NULL;
  c->terminated = 0;
  c->is_interactive = 1;
  c->cr_mode = cr_mode;
  ARC_SHELL_CURRENT_CONTEXT = c;

  do
    {
      c->prompt_level = 0;
      c->state = yy_scan_string("");
      yy_switch_to_buffer(c->state);
      if (arc_shell_parse() && c->cr_mode)
         ccl_display ("EOCWE\n");
      yy_delete_buffer(c->state);
    }
  while( ! c->terminated );

  if( (ARC_SHELL_CURRENT_CONTEXT = c->next) != NULL )
    yy_switch_to_buffer(ARC_SHELL_CURRENT_CONTEXT->state);
  else 
    arc_shell_parser_lex_destroy ();

  ccl_delete(c);
}

			/* --------------- */

int
arc_shell_load_script_file(const char *filename)
{
  FILE         *stream;
  arc_shell_context c;
  int          result;

  if( (stream = fopen(filename,"r")) == NULL )
    {      
      arc_shell_error("can't open file '%s'\n",filename);
      result = 0;
    }
  else
    {
      c = ccl_new(struct arc_shell_context_st);
      c->next = ARC_SHELL_CURRENT_CONTEXT;
      c->filename = filename;
      c->line = 0;
      c->state = yy_create_buffer(stream,YY_BUF_SIZE);
      c->nodes = NULL;
      c->stream = stream;
      c->terminated = 0;
      c->is_interactive = 0;
      c->cr_mode = 0;
      ARC_SHELL_CURRENT_CONTEXT = c;

      yy_switch_to_buffer(c->state);
      result = ! arc_shell_parse();

      fclose(stream);
      yy_delete_buffer(c->state);
      if( (ARC_SHELL_CURRENT_CONTEXT = c->next) != NULL )
	yy_switch_to_buffer(ARC_SHELL_CURRENT_CONTEXT->state);
      else 
        arc_shell_parser_lex_destroy ();
      ccl_delete(c);
    }

  return result;
}

			/* --------------- */

int
arc_shell_interp_script_string (const char *script)
{
  arc_shell_context c;
  int result;

  c = ccl_new (struct arc_shell_context_st);
  c->next = ARC_SHELL_CURRENT_CONTEXT;
  c->filename = script;
  c->line = 0;
  c->state = yy_scan_string (script);
  c->nodes = NULL;
  c->stream = NULL;
  c->terminated = 0;
  c->is_interactive = 0;
  c->cr_mode = 0;
  ARC_SHELL_CURRENT_CONTEXT = c;
  
  yy_switch_to_buffer (c->state);
  result = ! arc_shell_parse ();
  
  yy_delete_buffer (c->state);
  if ((ARC_SHELL_CURRENT_CONTEXT = c->next) != NULL)
    yy_switch_to_buffer (ARC_SHELL_CURRENT_CONTEXT->state);
  else 
    arc_shell_parser_lex_destroy ();
  ccl_delete (c);

  return result;  
}

			/* --------------- */

void
arc_shell_pop_prompt_level(void)
{
  if( ARC_SHELL_CURRENT_CONTEXT->is_interactive )
    {
      if( --ARC_SHELL_CURRENT_CONTEXT->prompt_level == 0 ) 
	ARC_SHELL_CURRENT_CONTEXT->line = 1;

      ccl_post( ARC_SHELL_CURRENT_CONTEXT->prompt_level >= 0 );
    }
}

			/* --------------- */

void
arc_shell_exit(void)
{
  if( ARC_SHELL_CURRENT_CONTEXT != NULL )
    ARC_SHELL_CURRENT_CONTEXT->terminated = 1;
}


			/* --------------- */

void
arc_shell_error(const char *fmt, ...)
{
  va_list  pa;
  
  if( ARC_SHELL_CURRENT_CONTEXT != NULL )
    ccl_error("%s:%d:",ARC_SHELL_CURRENT_CONTEXT->filename,
	     ARC_SHELL_CURRENT_CONTEXT->line);

  va_start(pa,fmt);
  ccl_log_va(CCL_LOG_ERROR,fmt,pa);
  va_end(pa);
}
