/*
 * arc-shell-preferences.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_SHELL_PREFERENCES_H
# define ARC_SHELL_PREFERENCES_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif

# define ARC_SHELL_VERBOSE          "arc.shell.verbose"
# define ARC_SHELL_VERBOSE_DEFAULT  "true"

# define ARC_SHELL_PROMPT_0         "arc.shell.prompt.0"
# define ARC_SHELL_PROMPT_0_DEFAULT "arc>"

# define ARC_SHELL_PROMPT_1         "arc.shell.prompt.1"
# define ARC_SHELL_PROMPT_1_DEFAULT "."

# define ARC_SHELL_PROMPT_EVAL         "arc.shell.prompt.eval"
# define ARC_SHELL_PROMPT_EVAL_DEFAULT "eval>"

# define ARC_SHELL_HISTORY_FILE         "arc.shell.history_file"
# define ARC_SHELL_HISTORY_FILE_DEFAULT "~/.arc_history"

# define ARC_SHELL_CHECK_CARD_ABORT         "arc.shell.check-card-abort"
# define ARC_SHELL_CHECK_CARD_ABORT_DEFAULT "false"

# define ARC_SHELL_PREPROCESSOR "arc.shell.preprocessor"

# define ARC_SHELL_PREPROCESSOR_COMMAND_TMPL(s) \
  ARC_SHELL_PREPROCESSOR "." s ".command"

# define ARC_SHELL_PREPROCESSOR_ARGS_TMPL(s) \
  ARC_SHELL_PREPROCESSOR "." s ".args"

# define ARC_SHELL_PREPROCESSOR_COMMAND \
  ARC_SHELL_PREPROCESSOR_COMMAND_TMPL("%s")

# define ARC_SHELL_PREPROCESSOR_ARGS \
  ARC_SHELL_PREPROCESSOR_ARGS_TMPL("%s")

# define ARC_SHELL_RUSAGE_AT_EXIT  "arc.shell.rusage"
# define ARC_SHELL_RUSAGE_AT_EXIT_DEFAULT  "false"

#endif /* ! ARC_SHELL_PREFERENCES_H */
