/*
 * altarica-input-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ALTARICA_INPUT_P_H
# define ALTARICA_INPUT_P_H

# include <stdio.h>
# include "altarica-tree.h"
# include "altarica-input.h"

extern char *altarica_text;
extern ccl_parse_tree_value altarica_token_value;
extern int altarica_current_line;
extern const char *altarica_current_filename;
extern FILE *altarica_in;
extern altarica_tree *altarica_syntax_tree;
extern altarica_tree *altarica_syntax_tree_container;
extern int altarica_number_of_errors;

extern void
altarica_init_lexer (FILE *stream, const char *input_name, int modes, 
		     int (*is_cmd) (const char *id, void *data),
		     void *data);



extern void
altarica_init_lexer_with_string (const char *input, const char *input_name, 
				 int modes, 
				 int (*is_cmd) (const char *id, void *data),
				 void *data);


extern void
altarica_terminate_lexer (void);

extern int
altarica_lex (void);

extern int
altarica_parse (void);

extern void
altarica_error (const char *message);

extern void
altarica_format_error (altarica_tree *loc, int display_mode, 
		       const char *fmt, ...);

extern int
altarica_wrap (void);

extern int
altarica_input_get_mode (void);

extern void
altarica_input_set_mode__ (int mode, const char *smode, const char *file, 
			   int line);

# define altarica_input_set_mode(_m) \
  altarica_input_set_mode__(_m, # _m,__FILE__,__LINE__)

extern void
altarica_input_clear_mode (void);

#endif /* ! ALTARICA_INPUT_P_H */
