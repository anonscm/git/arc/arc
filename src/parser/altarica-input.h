/*
 * altarica-input.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ALTARICA_INPUT_H
# define ALTARICA_INPUT_H

# include <stdio.h>
# include "parser/altarica-tree.h"

# define AR_INPUT_NONE      (0x00)
# define AR_INPUT_ALTARICA  (0x01<<0)
# define AR_INPUT_MECV      (0x01<<1)
# define AR_INPUT_ACHECK    (0x01<<2)
# define AR_INPUT_AEXPR     (0x01<<3)
# define AR_INPUT_ARC       (0x01<<4)
# define AR_INPUT_CTLSTAR   (0x01<<5)

# define AR_INPUT_ANY (AR_INPUT_ALTARICA|AR_INPUT_MECV|AR_INPUT_ACHECK|\
		       AR_INPUT_AEXPR|AR_INPUT_ARC|AR_INPUT_CTLSTAR)

BEGIN_C_DECLS

extern altarica_tree *
altarica_read_file (const char *filename, int modes, 
		    int (*is_cmd) (const char *id, void *data),
		    void *data);

extern altarica_tree *
altarica_read_stream (FILE *stream, const char *input_name, int modes, 
		      int (*is_cmd) (const char *id, void *data),
		      void *data);

extern altarica_tree *
altarica_read_string (const char *input, const char *input_name, int modes, 
		      int (*is_cmd) (const char *id, void *data),
		      void *data);

END_C_DECLS

#endif /* ! ALTARICA_INPUT_H */
