/*
 * altarica-tree.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ALTARICA_TREE_H
# define ALTARICA_TREE_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-parse-tree.h>

BEGIN_C_DECLS

typedef ccl_parse_tree altarica_tree;

enum altarica_label {
  AR_TREE_ALTARICA,
  AR_TREE_MECV,
  AR_TREE_ACHECK,
  AR_TREE_AEXPR,
  AR_TREE_ARC,

  AR_TREE_CONSTANT = 0,

  AR_TREE_DOMAIN,
     AR_TREE_RANGE,
     AR_TREE_SYMBOL_SET,
     AR_TREE_BOOLEANS,
     AR_TREE_INTEGERS,
     AR_TREE_STRUCTURE,
        AR_TREE_STRUCTURE_FIELDS,
          AR_TREE_ID_LIST,
     AR_TREE_ARRAY_DOMAIN,

  AR_TREE_ITE,
  AR_TREE_CASE,
     AR_TREE_CASE_CHOICE,
     AR_TREE_CASE_DEFAULT,
  AR_TREE_OR,
  AR_TREE_AND,
  AR_TREE_EQ, AR_TREE_NEQ, AR_TREE_IMPLY,
  AR_TREE_LT, AR_TREE_GT, AR_TREE_LEQ, AR_TREE_GEQ,
  AR_TREE_ADD, AR_TREE_SUB,
  AR_TREE_MUL, AR_TREE_DIV, AR_TREE_MOD,
  AR_TREE_NEG, AR_TREE_NOT,
  AR_TREE_PARENTHEZED_EXPR,
  AR_TREE_STRUCT_MEMBER,
  AR_TREE_ARRAY_MEMBER,
  AR_TREE_MIN, AR_TREE_MAX,
  AR_TREE_EXIST, AR_TREE_FORALL,
     AR_TREE_QUANTIFIED_VARIABLE_LIST,
        AR_TREE_QUANTIFIED_VARIABLES,
  AR_TREE_FUNCTION_CALL,
  AR_TREE_STRUCT_EXPR,
     AR_TREE_FIELD,
  AR_TREE_ARRAY_EXPR,

  AR_TREE_IDENTIFIER_PATH,
     AR_TREE_ELEMENT_IN_ARRAY,

  AR_TREE_NODE,
     AR_TREE_ATTRIBUTES,
     AR_TREE_PARAMETERS_DECL,
        AR_TREE_PARAMETER_DECL,
     AR_TREE_PARAM_SET_DECL,
     AR_TREE_VARIABLES_DECL,
        AR_TREE_STATE,
        AR_TREE_FLOW,
        AR_TREE_VAR_DECL,
     AR_TREE_EVENTS_DECL,
        AR_TREE_EVENT_POSET,
           AR_TREE_EVENT_SET_LIST,
              AR_TREE_EVENT_DAG_LIST,
              AR_TREE_EVENT_LT,
              AR_TREE_EVENT_GT,

     AR_TREE_SUBNODES_DECL,
        AR_TREE_SUBNODES,
          AR_TREE_SUBNODE_ARRAY,
     AR_TREE_ASSERTIONS_DEF,
     AR_TREE_TRANSITIONS_DEF,
        AR_TREE_TRANSITION,
           AR_TREE_TRANSITION_TGT,
              AR_TREE_TRANS_LABEL_LIST,
              AR_TREE_ASSIGNMENT,
     AR_TREE_SYNCHRONIZATION_DEF,
        AR_TREE_SYNC_VECTOR,
           AR_TREE_BROADCAST_LIST,
              AR_TREE_EVENT_INSTANCE,
                 AR_TREE_SYNC_CONSTRAINT_EQ,
                 AR_TREE_SYNC_CONSTRAINT_LEQ,
                 AR_TREE_SYNC_CONSTRAINT_GEQ,
                 AR_TREE_SYNC_CONSTRAINT_LT,
                 AR_TREE_SYNC_CONSTRAINT_GT,
                 AR_TREE_SYNC_CONSTRAINT_NONE,
                 AR_TREE_SYNC_MIN,
                 AR_TREE_SYNC_MAX,
     AR_TREE_INIT_DECL,

     AR_TREE_EXTERN_DECL,
        AR_TREE_EXTERN_DIRECTIVE,
          AR_TREE_EXTERN_TERM_FUNCTION,
          AR_TREE_EXTERN_TERM_SET,
          AR_TREE_EXTERN_TERM_EXPR,
          AR_TREE_EXTERN_TERM_EVENT,
          AR_TREE_EXTERN_TERM_FLOW,
          AR_TREE_EXTERN_TERM_STATE,
          AR_TREE_EXTERN_TERM_LOCAL,
          AR_TREE_EXTERN_TERM_GLOBAL,
          AR_TREE_EXTERN_TERM_SUB,  

  AR_TREE_IDENTIFIER,
  AR_TREE_INTEGER,
  AR_TREE_FLOAT,
  AR_TREE_STRING,
  AR_TREE_TRUE, AR_TREE_FALSE,

  AR_TREE_SORT,
  AR_TREE_SIGNATURE,
  AR_TREE_CARTESIAN_PRODUCT,

  /* Mec V specific labels */
  AR_TREE_BANG_ID, AR_TREE_EQ_LFP, AR_TREE_EQ_GFP, AR_TREE_EQ_DEF,
  AR_TREE_EQ_PARAMETERS,
  AR_TREE_TYPED_ID, AR_TREE_LOCAL_EQUATION, AR_TREE_EQUATIONS_SYSTEM, 
  AR_TREE_MECV_COMMAND,

  /* Acheck specific labels */
  AR_TREE_WITH, AR_TREE_SYMBOLICALLY, AR_TREE_EXHAUSTIVELY,
  AR_TREE_CMD, AR_TREE_CRT_CMD, AR_TREE_APPEND_CMD, AR_TREE_WTS, AR_TREE_DOT,
  AR_TREE_GML, AR_TREE_TEST, AR_TREE_NRTEST, AR_TREE_SHOW, AR_TREE_QUOT, 
  AR_TREE_MODES, 
  AR_TREE_VALIDATE, AR_TREE_PROJECT, AR_TREE_REMOVE, 
  AR_TREE_RTGT, AR_TREE_TGT, AR_TREE_SRC, AR_TREE_RSRC, AR_TREE_REACH, 
  AR_TREE_COREACH, AR_TREE_UNAV, AR_TREE_EXPR, AR_TREE_LOOP, AR_TREE_TRACE, 
  AR_TREE_DOTTRACE, AR_TREE_LABEL, AR_TREE_EVENT_ATTRIBUTE, AR_TREE_DISPLAY, 
  AR_TREE_PROJ_S, AR_TREE_PROJ_F, AR_TREE_PICK, AR_TREE_CTL2MU, AR_TREE_ASSERT,
  AR_TREE_EVENTS, 
  
  AR_TREE_CTLSTAR_SPEC,
    AR_TREE_CTLSTAR_ATOM,
    AR_TREE_CTLSTAR_PARENTHESED,
    AR_TREE_CTLSTAR_NOT,
    AR_TREE_CTLSTAR_AND,
    AR_TREE_CTLSTAR_OR,
    AR_TREE_CTLSTAR_IMPLY,
    AR_TREE_CTLSTAR_XOR,
    AR_TREE_CTLSTAR_EQUIV,
    AR_TREE_CTLSTAR_AX, AR_TREE_CTLSTAR_AG, AR_TREE_CTLSTAR_AF,
    AR_TREE_CTLSTAR_EX, AR_TREE_CTLSTAR_EG, AR_TREE_CTLSTAR_EF,
    AR_TREE_CTLSTAR_A, AR_TREE_CTLSTAR_E, 
    AR_TREE_CTLSTAR_X, AR_TREE_CTLSTAR_G, AR_TREE_CTLSTAR_F,
    AR_TREE_CTLSTAR_U, AR_TREE_CTLSTAR_W,

  /* ARC commands */

  AR_TREE_SHELL_REDIR_CREATE, 
  AR_TREE_SHELL_REDIR_APPEND, 
  AR_TREE_SHELL_REDIR_PIPE, 
  AR_TREE_SHELL_CMD
};

typedef enum ar_identifier_context_st {
  AR_IC_IDENTIFIER,
  AR_IC_NODE_ATTR,
  AR_IC_STATE_VAR_ATTR,
  AR_IC_FLOW_VAR_ATTR,
  AR_IC_EVENT_ATTR,
  AR_IC_ACHECK_SET,
  AR_IC_ACHECK_FILENAME,
  AR_IC_ACHECK_PROJECT_TGT,
  AR_IC_BANG_TYPE,
  AR_IC_ARC_OUT_FILENAME,
  AR_IC_ARC_STRING,
  AR_IC_MEC5_COMMAND,
  AR_IC_EXTERN_DECL,
  AR_IC_EXTERN_FUNCTION
} ar_identifier_context;

typedef struct ar_identifier_translator_st ar_identifier_translator;
struct ar_identifier_translator_st 
{
  void (*translate) (ar_identifier_translator *it, ccl_log_type log,
		     ar_identifier_context ctx, const char *id);
};

			/* --------------- */

extern ar_identifier_translator *AR_NO_ID_TRANSLATION;

extern void
ar_tree_log (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it);

extern void
ar_tree_log_altarica (ccl_log_type log, const altarica_tree *t,		      
		      ar_identifier_translator *it);

extern void
ar_tree_log_mecv (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it);

extern void
ar_tree_log_acheck (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it);

extern void
ar_tree_log_acheck_expr (ccl_log_type log, const altarica_tree *t,
			 ar_identifier_translator *it);

extern void
ar_tree_log_ctlstar_expr (ccl_log_type log, const altarica_tree *t,
			  ar_identifier_translator *it);

extern void
ar_tree_log_arc (ccl_log_type log, const altarica_tree *t, 
		 ar_identifier_translator *it);

END_C_DECLS

#endif /* ! ALTARICA_TREE_H */
