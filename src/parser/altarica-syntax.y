/*
 * altarica-syntax.y -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

%{
#include <ccl/ccl-memory.h>
#include "parser/altarica-input-p.h"

#define YYSTYPE altarica_tree *

#define ZN ((altarica_tree *) NULL)

#define NN(_type,_vtype,_child,_next) _NN (_type, #_type, _vtype, _child, _next)

#define NE(_type,_child,_next) NN (_type, CCL_PARSE_TREE_EMPTY, _child, _next)

#define NID()  NN (AR_TREE_IDENTIFIER, CCL_PARSE_TREE_IDENT, ZN, ZN)
#define NINT() NN (AR_TREE_INTEGER, CCL_PARSE_TREE_INT, ZN, ZN)
#define NFLT() NN (AR_TREE_FLOAT, CCL_PARSE_TREE_FLOAT,NULL,NULL)
#define NSTR() NN (AR_TREE_STRING, CCL_PARSE_TREE_STRING, ZN, ZN)

# define SL(_x_) do { (_x_)->line = altarica_current_line; } while (0)

#define malloc  ccl_malloc
#define realloc ccl_realloc
#define calloc  ccl_calloc
#define free    ccl_free

altarica_tree *altarica_syntax_tree;
altarica_tree *altarica_syntax_tree_container;

# define NODES altarica_syntax_tree_container
# define REVERSE(_t) ccl_parse_tree_reverse_siblings (_t)

# define MAX_NB_SYNTAX_ERROR 100

# define ERRMSG_MISSING_AT(tok, where) \
  altarica_format_error (NULL, 0, "missing %s %s", tok, where)

 int altarica_number_of_errors;

static altarica_tree *
_NN (int type, const char *type_string, ccl_parse_tree_value_type vtype, 
     altarica_tree *child, altarica_tree *next)
{
  NODES = 
    ccl_parse_tree_create (type, type_string, vtype, 
			   (child == ZN 
			    ? altarica_current_line 
			    : child->line),
			   (child == ZN
			    ? altarica_current_filename
			    : child->filename),
			   child, next, NODES);
  return NODES;
}

%}

%token TOK_MECV_COMMAND
%token TOK_AEXPR_BEGIN
%token TOK_AEXPR_END
%token TOK_AND
%token TOK_ASSERT
%token TOK_ASSIGN
%token TOK_ATTRIBUTE
%token TOK_BANG
%token TOK_BEGIN
%token TOK_BOOL
%token TOK_CASE
%token TOK_COLON
%token TOK_COMMA
%token TOK_CONST
%token TOK_COREACH
%token TOK_DISPLAY
%token TOK_DO
%token TOK_DOMAIN
%token TOK_DONE
%token TOK_DOT 
%token TOK_EDON
%token TOK_ELSE
%token TOK_END
%token TOK_EQ
%token TOK_ERROR
%token TOK_EVENT
%token TOK_EVENTS
%token TOK_EXTERN
%token TOK_FALSE
%token TOK_FLOW 
%token TOK_GEQ
%token TOK_GFP
%token TOK_GML
%token TOK_IDENTIFIER
%token TOK_IF
%token TOK_IMPLY
%token TOK_INIT
%token TOK_INTEGER
%token TOK_LABEL
%token TOK_LANGLE
%token TOK_LBRACE 
%token TOK_LBRACKET
%token TOK_LEQ 
%token TOK_LFP
%token TOK_LOCAL
%token TOK_GLOBAL
%token TOK_LOOP
%token TOK_LPAR
%token TOK_MAX
%token TOK_MIN
%token TOK_MINUS
%token TOK_MOD
%token TOK_NEQ
%token TOK_NODE
%token TOK_NOT
%token TOK_NRTEST
%token TOK_OR
%token TOK_PARAM
%token TOK_PARAM_SET
%token TOK_PLUS
%token TOK_PROJECT
%token TOK_QUESTION
%token TOK_QUOT
%token TOK_MODES
%token TOK_RANGLE
%token TOK_RANGLE_RANGLE 
%token TOK_RBRACE
%token TOK_RBRACKET
%token TOK_REACH
%token TOK_REMOVE
%token TOK_RPAR
%token TOK_RSRC 
%token TOK_RTGT
%token TOK_SEMICOLON
%token TOK_SHOW
%token TOK_SIG
%token TOK_SLASH
%token TOK_SORT
%token TOK_SRC
%token TOK_STAR
%token TOK_STATE
%token TOK_STRUCT
%token TOK_STRING
%token TOK_SUB
%token TOK_SYMBOLICALLY
%token TOK_EXHAUSTIVELY
%token TOK_SYNC
%token TOK_TCURTS
%token TOK_TEST
%token TOK_TGT 
%token TOK_THEN
%token TOK_TILDE
%token TOK_TRACE
%token TOK_DOTTRACE
%token TOK_TRANS
%token TOK_TRUE
%token TOK_UINTEGER 
%token TOK_UNAV
%token TOK_WITH
%token TOK_WTS
%token TOK_PICK
%token TOK_PROJ_S
%token TOK_PROJ_F
%token TOK_SUPSUP
%token TOK_SUP
%token TOK_PIPE
%token TOK_FLOAT
%token TOK_NEWLINE
%token TOK_VALIDATE
%token TOK_CTL2MU
%token TOK_CTLSTARSPEC
%token TOK_X TOK_F TOK_G TOK_AX TOK_AF TOK_AG TOK_EX TOK_EF TOK_EG TOK_A TOK_E
%token TOK_U TOK_W TOK_XOR TOK_EQUIV
%token TOK_TERM

%start grammar_start_rule

%left TOK_IMPLY TOK_XOR TOK_EQUIV
%left TOK_OR 
%left TOK_AND 
%left TOK_U TOK_W
%right TOK_NOT TOK_TILDE
%right TOK_X TOK_F TOK_G TOK_AX TOK_AF TOK_AG TOK_EX TOK_EF TOK_EG TOK_A TOK_E

%%

grammar_start_rule : 
  description_list
  { altarica_syntax_tree = $1; }
| /* empty */
  { altarica_syntax_tree = ZN; }
;

description_list :
  desc description_list 
  { 
    if( $1->node_type == $2->node_type ) 
      {
	altarica_tree **pt;
	$1->child->next = $2->child;
	$2->child = $1->child;
	$1->child = $1->next = NULL;
	$$ = $2;

	for(pt = &NODES; *pt != $1 ; pt = &((*pt)->next_in_container))
	  /* dp nothing */;
	*pt = ($1)->next_in_container;
	ccl_parse_tree_delete_node($1);
      }
    else
      {
	$$ = $1; $1->next = $2;
      }
  }
| desc
  { $$ = $1; }
;

desc : 
  description
  { $$ = $1; altarica_input_clear_mode (); }
;

description :
  altarica_description 
  { $$ = NE(AR_TREE_ALTARICA,$1,ZN); }
| mecv_description 
  { $$ = NE(AR_TREE_MECV,$1,ZN); } 
| acheck_description
  { $$ = NE(AR_TREE_ACHECK,$1,ZN); } 
| aexpr_description
  { $$ = NE(AR_TREE_AEXPR,$1,ZN); } 
| arc_commands
  { $$ = NE(AR_TREE_ARC,$1,ZN); } 
| ctlstar_specification 
  { $$ = $1; } 
;

/*                                                      altarica_description */
altarica_description : 
  definition
;

definition : 
  constant_definition 
| domain_definition
| sort_declaration 
| signature_declaration
| node_definition
| toplevel_external_directives_declaration 
;

/*                                                      constant_declaration */
constant_definition :
  TOK_CONST cst_definition TOK_SEMICOLON 
  { $$ = $2; } 
| TOK_CONST cst_definition error
  { ERRMSG_MISSING_AT (";", "at the end of constant declaration"); YYERROR; }
;

cst_definition :
  constant_name TOK_EQ constant_expression 
  { $$ = NE(AR_TREE_CONSTANT,$1,ZN); $1->next = $3; }
| constant_name TOK_COLON domain TOK_EQ constant_expression 
{ $$ = NE(AR_TREE_CONSTANT,$1,ZN); $1->next = $3; $3->next = $5; 
  $$->value.int_value = 1; }
| constant_name TOK_COLON domain
  { $$ = NE(AR_TREE_CONSTANT,$1,ZN); $1->next = $3; 
    $$->value.int_value = 1; }
;

constant_name : 
  identifier
  ;

identifier :
  TOK_IDENTIFIER 
  { $$ = NID(); $$->value = altarica_token_value; SL($$); }
  ;

constant_expression : 
  expression
  ;

/*                                                         domain_definition */
domain_definition :
  TOK_DOMAIN domain_name TOK_EQ domain TOK_SEMICOLON
  { $$ = NE(AR_TREE_DOMAIN,$2,ZN); $2->next = $4; }
| TOK_DOMAIN domain_name error domain TOK_SEMICOLON
  { ERRMSG_MISSING_AT ("'='", "after the domain name"); YYERROR; }
  ;

domain_name :
  identifier
  ;

domain : 
  non_array_domain
| array_domain
;

non_array_domain : 
  range_domain
| symbol_set_domain
| predefined_domain
| domain_name
| structure_domain
;

/*                                                              range_domain */
range_domain :
  TOK_LBRACKET numerical_constant_expression TOK_COMMA 
  numerical_constant_expression TOK_RBRACKET
  { $$ = NE(AR_TREE_RANGE,$2,ZN); $2->next = $4; }
;

numerical_constant_expression :
  constant_expression
;

/*                                                         symbol_set_domain */
symbol_set_domain : 
  TOK_LBRACE symbol_list TOK_RBRACE
  { $$ = NE(AR_TREE_SYMBOL_SET,REVERSE($2),ZN); }
;

symbol_list :
  symbol_list TOK_COMMA symbol
  { $$ = $3; $3->next = $1; }
| symbol
;

symbol :
  identifier
| error
  { altarica_format_error (NULL, 0, "read '%s' while an enum is expected here",
			   altarica_text); YYABORT; }

;

/*                                                         predefined_domain */
predefined_domain : 
  TOK_BOOL
  { $$ = NE(AR_TREE_BOOLEANS,ZN,ZN); SL($$); }
| TOK_INTEGER
  { $$ = NE(AR_TREE_INTEGERS,ZN,ZN); SL($$); }
;

/*                                                          structure_domain */
structure_domain : 
  TOK_STRUCT structure_fields opt_semi_colon TOK_TCURTS
  { $$ = NE(AR_TREE_STRUCTURE,REVERSE($2),ZN); }
;

opt_semi_colon : TOK_SEMICOLON | /* empty */
;


structure_fields : 
  structure_fields TOK_SEMICOLON field_list
  { $$ = $3; $3->next = $1; }
| field_list
;

field_list : id_list TOK_COLON domain 
  { $$ = NE(AR_TREE_STRUCTURE_FIELDS,$1,ZN); $1->next = $3; }
;

id_list :
  list_of_identifiers
  { $$ = NE(AR_TREE_ID_LIST,REVERSE($1),ZN); }
;

list_of_identifiers : 
  list_of_identifiers TOK_COMMA identifier
  { $$ = $3; $3->next = $1; } 
| identifier
;

/*                                                              array_domain */
array_domain :
  array_domain TOK_LBRACKET numerical_constant_expression TOK_RBRACKET 
  { $$ = $1; $3->next = $1->child->next; $1->child->next = $3; }
|  non_array_domain TOK_LBRACKET numerical_constant_expression TOK_RBRACKET 
  { $$ = NE(AR_TREE_ARRAY_DOMAIN,$1,ZN); $1->next = $3; }
;

/*                                                                expression */
expression : 
 conditional_expr
;

conditional_expr :
  if_then_else_expr
| case_expr
| disjunctive_expr
;

if_then_else_expr :
  TOK_IF expression TOK_THEN expression TOK_ELSE expression
  { $$ = NE(AR_TREE_ITE,$2,ZN); $2->next = $4; $4->next = $6; }
| TOK_LPAR expression TOK_QUESTION expression TOK_COLON expression TOK_RPAR
  { $$ = NE(AR_TREE_ITE,$2,ZN); $2->next = $4; $4->next = $6; } 
;

case_expr :
  TOK_CASE TOK_LBRACE case_choice_list TOK_RBRACE
  { $$ = NE(AR_TREE_CASE,REVERSE($3),ZN); }
;

case_choice_list : 
  case_conditional_choice_list TOK_COMMA  case_default
  { $$ = $3; $3->next = $1; }
| case_default
;

case_conditional_choice_list :
  case_conditional_choice_list TOK_COMMA case_conditional_choice
  { $$ = $3; $3->next = $1; }
| case_conditional_choice
;

case_default :
  TOK_ELSE expression
  { $$ = NE(AR_TREE_CASE_DEFAULT,$2,ZN); }
  ;

case_conditional_choice : 
  boolean_expr TOK_COLON expression
  { $$ = NE(AR_TREE_CASE_CHOICE,$1,ZN); $1->next = $3; }
;

boolean_expr :
  expression
;

disjunctive_expr :
  disjunctive_expr TOK_OR conjunctive_expr
  { $$ = NE(AR_TREE_OR,$1,ZN); $1->next = $3; }
| conjunctive_expr
;

conjunctive_expr : 
  conjunctive_expr TOK_AND logical_comparison_expr
  { $$ = NE(AR_TREE_AND,$1,ZN); $1->next = $3; }
| logical_comparison_expr
;

logical_comparison_expr : 
  logical_comparison_expr TOK_EQ arithmetic_comparison_expr
  { $$ = NE(AR_TREE_EQ,$1,ZN); $1->next = $3; }
| logical_comparison_expr TOK_NEQ arithmetic_comparison_expr
  { $$ = NE(AR_TREE_NEQ,$1,ZN); $1->next = $3; }
| logical_comparison_expr TOK_IMPLY arithmetic_comparison_expr
  { $$ = NE(AR_TREE_IMPLY,$1,ZN); $1->next = $3; }
| arithmetic_comparison_expr
;

arithmetic_comparison_expr :
  arithmetic_comparison_expr TOK_LANGLE additive_expr
  { $$ = NE(AR_TREE_LT,$1,ZN); $1->next = $3; }
| arithmetic_comparison_expr TOK_RANGLE additive_expr
  { $$ = NE(AR_TREE_GT,$1,ZN); $1->next = $3; }
| arithmetic_comparison_expr TOK_LEQ additive_expr
  { $$ = NE(AR_TREE_LEQ,$1,ZN); $1->next = $3; }
| arithmetic_comparison_expr TOK_GEQ additive_expr
  { $$ = NE(AR_TREE_GEQ,$1,ZN); $1->next = $3; }
| additive_expr
;

additive_expr :
  additive_expr TOK_PLUS multiplicative_expr
  { $$ = NE(AR_TREE_ADD,$1,ZN); $1->next = $3; }
| additive_expr TOK_MINUS multiplicative_expr
  { $$ = NE(AR_TREE_SUB,$1,ZN); $1->next = $3; }
| multiplicative_expr
;

multiplicative_expr :
  multiplicative_expr TOK_STAR unary_expr
  { $$ = NE(AR_TREE_MUL,$1,ZN); $1->next = $3; }
| multiplicative_expr TOK_SLASH unary_expr
  { $$ = NE(AR_TREE_DIV,$1,ZN); $1->next = $3; }
| multiplicative_expr TOK_MOD unary_expr
  { $$ = NE(AR_TREE_MOD,$1,ZN); $1->next = $3; }
| unary_expr { $$ = $1; }
;

unary_expr : 
  TOK_MINUS unary_expr
  { $$ = NE(AR_TREE_NEG,$2,ZN); }
| TOK_TILDE unary_expr
  { $$ = NE(AR_TREE_NOT,$2,ZN); }
| TOK_NOT unary_expr
  { $$ = NE(AR_TREE_NOT,$2,ZN); }
| atomic_expr
;

atomic_expr :
  parenthezed_expr
| member_access
| min_max_expr
| unsigned_integer
| boolean_constant
| quantified_expr
| function_call_expr
| structure_expression
| array_expression
; 

parenthezed_expr :
  TOK_LPAR expression TOK_RPAR
  { $$ = NE(AR_TREE_PARENTHEZED_EXPR,$2,ZN); }
;

member_access :
   struct_member
 | array_member
 | identifier
;

struct_member :
   member_access TOK_DOT identifier
   { $$ = NE(AR_TREE_STRUCT_MEMBER,$1,ZN); $1->next = $3; }
 | member_access TOK_DOT 
   {
     if( altarica_input_get_mode() != AR_INPUT_MECV ) 
       { ERRMSG_MISSING_AT ("field name", "after dot");  YYERROR; } 
     else 
       { $$ = NE(AR_TREE_STRUCT_MEMBER,$1,ZN); }
   }
;
 
array_member :
  member_access TOK_LBRACKET numerical_constant_expression TOK_RBRACKET
  { $$ = NE(AR_TREE_ARRAY_MEMBER,$1,ZN); $1->next = $3; }
  ;

min_max_expr :
  TOK_MIN TOK_LPAR comma_separated_expr_list TOK_RPAR
  { $$ = NE(AR_TREE_MIN,REVERSE($3),ZN); }
| TOK_MAX TOK_LPAR comma_separated_expr_list TOK_RPAR
  { $$ = NE(AR_TREE_MAX,REVERSE($3),ZN); }
;

comma_separated_expr_list :
  comma_separated_expr_list TOK_COMMA expression
  { $$ = $3; $3->next = $1; }
| expression
;

boolean_constant :
  TOK_TRUE
  { $$ = NE(AR_TREE_TRUE,ZN,ZN); SL($$); }
| TOK_FALSE
  { $$ = NE(AR_TREE_FALSE,ZN,ZN); SL($$); }
 ;

unsigned_integer :
  TOK_UINTEGER
  { $$ = NINT(); $$->value = altarica_token_value; SL($$); }
;

quantified_expr :
  TOK_LANGLE quantified_variables_decl TOK_RANGLE unary_expr
  { $$ = NE(AR_TREE_EXIST,$2,ZN); $2->next = $4; }
| TOK_LBRACKET quantified_variables_decl TOK_RBRACKET unary_expr
  { $$ = NE(AR_TREE_FORALL,$2,ZN); $2->next = $4; }
;

quantified_variables_decl :
  quantified_variables_decl_list opt_semi_colon
  { $$ = NE(AR_TREE_QUANTIFIED_VARIABLE_LIST,REVERSE($1),ZN); }
;

quantified_variables_decl_list :
  quantified_variables_decl_list TOK_SEMICOLON quantified_variables
  { $$ = $3; $3->next = $1; }
| quantified_variables 
;

quantified_variables : 
  id_list TOK_COLON domain
  { $$ = NE(AR_TREE_QUANTIFIED_VARIABLES,$1,ZN); $1->next = $3; }
| id_list TOK_COLON bang_identifier
  { $$ = NE(AR_TREE_QUANTIFIED_VARIABLES,$1,ZN); $1->next = $3; }
| id_list 
  {
    if( altarica_input_get_mode() != AR_INPUT_MECV ) 
      { ERRMSG_MISSING_AT ("type", "after quantified variable names"); YYERROR; } 
    else 
      { $$ = NE(AR_TREE_QUANTIFIED_VARIABLES,$1,ZN); }
  }
;

function_call_expr :
  identifier TOK_LPAR function_parameters TOK_RPAR
  { $$ = NE(AR_TREE_FUNCTION_CALL,$1,ZN); $1->next = REVERSE($3); } 
| bang_identifier TOK_LPAR function_parameters TOK_RPAR
  { $$ = NE(AR_TREE_FUNCTION_CALL,$1,ZN); $1->next = REVERSE($3); } 
;

bang_identifier :
  identifier TOK_BANG identifier
  { 
    if( altarica_input_get_mode() != AR_INPUT_MECV ) 
      { altarica_format_error (NULL, 0, "bang identifier not allowed out of MEC files"); YYERROR; } 
    else 
      { $$ = NE(AR_TREE_BANG_ID,$1,ZN); $1->next = $3; }
  }
;

function_parameters :
  comma_separated_expr_list
| /* empty */
  { $$ = ZN; }
;

structure_expression : 
 TOK_LBRACE field_assignment_list TOK_RBRACE
 { $$ = NE(AR_TREE_STRUCT_EXPR,REVERSE($2),ZN); }
;

field_assignment_list : 
  field_assignment_list TOK_COMMA field_assignment
  { $$ = $3; $3->next = $1; }
| field_assignment  
;

field_assignment :
  TOK_DOT identifier TOK_EQ expression
  { $$ = NE(AR_TREE_FIELD,$2,ZN); $2->next = $4; }
;

array_expression :
  TOK_LBRACE array_expression_list TOK_RBRACE
  { $$ = NE(AR_TREE_ARRAY_EXPR,REVERSE($2),ZN); }
 ;

array_expression_list : 
  array_expression_list TOK_COMMA expression
  { $$ = $3; $3->next = $1; }
| expression
;

/*                                                           node_definition */
node_definition : 
  TOK_NODE node_name attributes node_field_list TOK_EDON
  { $$ = NE(AR_TREE_NODE,$2,ZN); $2->next = $3; $3->next = REVERSE($4); }
;

node_name :
  identifier
;

attributes :
  TOK_COLON attribute_list
  { $$ = NE(AR_TREE_ATTRIBUTES,REVERSE($2),ZN); }
| /* empty */
  { $$ = NE(AR_TREE_ATTRIBUTES,ZN,ZN); }
;

attribute_list :
  id_list
;

node_field_list :
  node_field_list node_field
  { if( $2 == NULL ) $$ = $1; else { $$ = $2; $2->next = $1; } }
| /* empty */
  { $$ = ZN; }
;

node_field : 
  parameters_declaration
| variables_declaration 
| events_declaration
| transitions_definition
| assertions_definition
| subnodes_declaration 
| vectors_definition
| init_declaration
| parameter_set_declaration
| external_directives_declaration 
;

/*                                                    parameters_declaration */
parameters_declaration :
  TOK_PARAM parameters_decl_list opt_semi_colon
  { $$ = NE(AR_TREE_PARAMETERS_DECL,REVERSE($2),ZN); }
;

parameters_decl_list :
  parameters_decl_list TOK_SEMICOLON parameter_decl
  { $$ = $3; $3->next = $1; }
| parameters_decl_list error parameter_decl
  { ERRMSG_MISSING_AT ("';'", "at the end of parameter declaration"); YYERROR; }
| parameter_decl 
;

parameter_decl : 
  parameter_name_list TOK_COLON domain 
  { $$ = NE(AR_TREE_PARAMETER_DECL,$1,ZN); $1->next = $3; }
;

parameter_name_list :
  id_list
;

/*                                                     variables_declaration */
variables_declaration :
  variable_class variable_decl_list opt_semi_colon
  { $$ = NE(AR_TREE_VARIABLES_DECL,$1,ZN); $1->next = REVERSE($2); }
;

variable_class : 
  TOK_FLOW
  { $$ = NE(AR_TREE_FLOW,ZN,ZN); SL($$); }
| TOK_STATE
  { $$ = NE(AR_TREE_STATE,ZN,ZN); SL($$); }
;

variable_decl_list :
  variable_decl_list TOK_SEMICOLON variable_decl
  { $$ = $3; $3->next = $1; }
| variable_decl_list error variable_decl
  { ERRMSG_MISSING_AT ("';'", "at the end of variable declaration"); YYERROR; }
| variable_decl 
;

variable_decl : 
  variable_name_list TOK_COLON domain attributes
  { $$ = NE(AR_TREE_VAR_DECL,$1,ZN); $1->next = $3; $3->next = $4; }
;

variable_name_list :
  id_list
;

/*                                                        events_declaration */
events_declaration : 
 TOK_EVENT events_decl_list opt_semi_colon
 { $$ = NE(AR_TREE_EVENTS_DECL,REVERSE($2),ZN); }
;

events_decl_list :
  events_decl_list TOK_SEMICOLON events_decl
  { $$ = $3; $3->next = $1; }
| events_decl_list error events_decl
  { ERRMSG_MISSING_AT ("';'", "at the end of event declaration"); YYERROR; }
| events_decl
;

events_decl : 
  event_dag_list attributes 
  { $$ = NE(AR_TREE_EVENT_POSET,$1,ZN); $1->next = $2; }
;

event_dag_list :
  event_dag_list_rec
  { $$ = NE(AR_TREE_EVENT_DAG_LIST,REVERSE($1),ZN); }
;

event_dag_list_rec :
  event_dag_list_rec TOK_COMMA event_dag
  { $$ = $3; $3->next = $1; }
| event_dag
;

event_dag : 
  event_dag TOK_LANGLE event_set
  { $$ = NE(AR_TREE_EVENT_LT,$1,ZN); $1->next = $3; }
| event_dag TOK_RANGLE event_set
  { $$ = NE(AR_TREE_EVENT_GT,$1,ZN); $1->next = $3; }
| event_set
;

event_set :
  TOK_LBRACE event_dag_list TOK_RBRACE
  { $$ = $2; } 
| event_name
;

event_name :
  ident
;

/*                                                      subnodes-declaration */
subnodes_declaration : 
  TOK_SUB subnodes_decl_list opt_semi_colon
  { $$ = NE(AR_TREE_SUBNODES_DECL,REVERSE($2),ZN); }
  ;

subnodes_decl_list : 
  subnodes_decl_list TOK_SEMICOLON subnodes_decl
  { $$ = $3; $3->next = $1; }
| subnodes_decl_list error subnodes_decl
  { ERRMSG_MISSING_AT ("';'", "at the end of subnode declaration"); YYERROR; }
| subnodes_decl
;

/*
subnodes_decl : 
  subnode_name_list TOK_COLON node_name
  { $$ = NE(AR_TREE_SUBNODES,$1,ZN); $1->next = $3; }
| subnode_name_list TOK_COLON node_name array_size_specifier 
  { $$ = NE(AR_TREE_SUBNODES,$1,ZN); $1->next = $3; $3->next = REVERSE($4); }
;
*/

subnodes_decl : 
  subnode_name_list TOK_COLON node_type
  { $$ = NE(AR_TREE_SUBNODES,$1,ZN); $1->next = $3; }
;

subnode_name_list :
  id_list
;

/*
array_size_specifier :
  array_size_specifier TOK_LBRACKET numerical_constant_expression TOK_RBRACKET 
  { $$ = $3; $3->next = $1; }
| TOK_LBRACKET numerical_constant_expression TOK_RBRACKET 
  { $$ = $2; }
;
*/

node_type : 
   node_type TOK_LBRACKET numerical_constant_expression TOK_RBRACKET 
   { $$ = NE(AR_TREE_SUBNODE_ARRAY,$1,ZN); $1->next = $3; }
 | node_name
   { $$ = $1; }
;

/*                                                     assertions_definition */
assertions_definition :
  TOK_ASSERT assertion_list opt_semi_colon
  { $$ = NE(AR_TREE_ASSERTIONS_DEF,REVERSE($2),ZN); }
;

assertion_list :
  assertion_list TOK_SEMICOLON assertion
  { $$ = $3; $3->next = $1; }
| assertion 
;

assertion : 
  boolean_expr
;

/*                                                    transitions_definition */
transitions_definition :
  TOK_TRANS transition_list opt_semi_colon
  { $$ = NE(AR_TREE_TRANSITIONS_DEF,REVERSE($2),ZN); }
;

transition_list : 
  transition_list TOK_SEMICOLON transition
  { $$ = $3; $3->next = $1; }
| transition
;

transition : 
  boolean_expr transition_target_list
  { $$ = NE(AR_TREE_TRANSITION,$1,ZN); $1->next = REVERSE($2); }
;

transition_target_list :
  transition_target_list transition_target 
  { $$ = $2; $2->next = $1; }
| transition_target
;

transition_target : 
  TOK_SRC trans_label_list  TOK_TGT opt_assignment_list
  { $$ = NE( AR_TREE_TRANSITION_TGT,$2,ZN); $2->next = $4; }
;

trans_label_list : 
  event_name_list
  { $$ = NE(AR_TREE_TRANS_LABEL_LIST,REVERSE($1),ZN); }
| /* empty */
  { altarica_tree *t = NID(); 
    t->value.id_value = ccl_string_make_unique ("$");
    SL(t); 
    $$ = NE (AR_TREE_TRANS_LABEL_LIST, t, ZN); }
;

event_name_list :
  event_name_list TOK_COMMA event_name
  { $$ = $3; $3->next = $1; }
| event_name
;

opt_assignment_list :
  assignment_list
  { $$ = REVERSE($1); }
| /* empty */
  { $$ = ZN; } 
;

assignment_list :
  assignment_list TOK_COMMA assignment
  { $$ = $3; $3->next = $1; }
| assignment
;

assignment : 
  member_access TOK_ASSIGN expression
  { $$ = NE(AR_TREE_ASSIGNMENT,$1,ZN); $1->next = $3; }
;

/*                                                        vectors_definition */
vectors_definition : 
  TOK_SYNC vector_list opt_semi_colon
  { $$ = NE(AR_TREE_SYNCHRONIZATION_DEF,REVERSE($2),ZN); }
;

vector_list : 
  vector_list TOK_SEMICOLON vector
  { $$ = $3; $3->next = $1; }
| vector 
;

vector : 
  TOK_LANGLE broadcast_list TOK_RANGLE constraint_on_vector broadcast_kind
  { 
     $2 = NE(AR_TREE_BROADCAST_LIST,REVERSE($2),ZN);
     $2->next = $4;
     $4->next = $5;
     $$ = NE(AR_TREE_SYNC_VECTOR,$2,ZN);  
  }
;

broadcast_list :
  broadcast_list TOK_COMMA broadcast
  { $$ = $3; $3->next = $1; }
| broadcast
;

broadcast :
  event_instance_identifier TOK_QUESTION
{ $$ = NE(AR_TREE_EVENT_INSTANCE,$1,ZN); $$->value_type = CCL_PARSE_TREE_INT;
  $$->value.int_value = 1; }
| TOK_QUESTION event_instance_identifier
  { $$ = NE(AR_TREE_EVENT_INSTANCE,$2,ZN); $$->value_type = CCL_PARSE_TREE_INT;
    $$->value.int_value = 1; }
| event_instance_identifier
  { $$ = NE(AR_TREE_EVENT_INSTANCE,$1,ZN); $$->value_type = CCL_PARSE_TREE_INT;
    $$->value.int_value = 0; }
;

event_instance_identifier : 
  identifier_path
;

identifier_path : 
  dot_separated_identifier_list
  { $$ = NE(AR_TREE_IDENTIFIER_PATH,REVERSE($1),ZN); }
;

dot_separated_identifier_list :
  dot_separated_identifier_list TOK_DOT ident
  { $$ = $3; $3->next = $1; }
| ident
;

ident : 
  identifier 
| identifier position_in_array
  { $$ = NE(AR_TREE_ELEMENT_IN_ARRAY,$1,ZN); $1->next = $2; }
;

position_in_array : 
   list_of_array_position
  { $$ = REVERSE($1); }
;

list_of_array_position :
  list_of_array_position 
  TOK_LBRACKET numerical_constant_expression TOK_RBRACKET
  { $$ = $3; $3->next = $1; }
| TOK_LBRACKET numerical_constant_expression TOK_RBRACKET
  { $$ = $2; }
;

constraint_on_vector : 
  TOK_LANGLE      numerical_constant_expression 
  { $$ = NE(AR_TREE_SYNC_CONSTRAINT_LT,$2,ZN); }
| TOK_LEQ numerical_constant_expression 
  { $$ = NE(AR_TREE_SYNC_CONSTRAINT_LEQ,$2,ZN);}
| TOK_RANGLE      numerical_constant_expression 
  { $$ = NE(AR_TREE_SYNC_CONSTRAINT_GT,$2,ZN); }
| TOK_GEQ numerical_constant_expression 
  { $$ = NE(AR_TREE_SYNC_CONSTRAINT_GEQ,$2,ZN); }
| TOK_EQ      numerical_constant_expression 
  { $$ = NE(AR_TREE_SYNC_CONSTRAINT_EQ,$2,ZN); }
| /* empty */
  { $$ = NE(AR_TREE_SYNC_CONSTRAINT_NONE,ZN,ZN); }
;

broadcast_kind :
  TOK_MIN
{ $$ = NE(AR_TREE_SYNC_MIN,ZN,ZN); }
| TOK_MAX
{ $$ = NE(AR_TREE_SYNC_MAX,ZN,ZN); }
| /* empty */
{ $$ = NE(AR_TREE_SYNC_MAX,ZN,ZN); }
;


/*                                                          init_declaration */
init_declaration : 
  TOK_INIT init_assignment_list opt_semi_colon
  { $$ = NE(AR_TREE_INIT_DECL,REVERSE($2),ZN); }
;

init_assignment_list : 
  init_assignment_list TOK_COMMA init_assignment
  { $$ = $3; $3->next = $1; }
|  init_assignment_list TOK_SEMICOLON init_assignment
  { $$ = $3; $3->next = $1; }
| init_assignment
;

init_assignment : 
  assignment 
| boolean_expr
;

/*                                                     param_set_declaration */
parameter_set_declaration :
  TOK_PARAM_SET param_assignment_list opt_semi_colon
  { $$ = NE(AR_TREE_PARAM_SET_DECL,REVERSE($2),ZN); }
;

param_assignment_list : 
  param_assignment_list TOK_COMMA param_assignment
  { $$ = $3; $3->next = $1; }
| param_assignment
;

param_assignment : 
  assignment;

/*                                           external_directives_declaration */
toplevel_external_directives_declaration : 
  TOK_EXTERN extern_decl TOK_SEMICOLON
  { $$ = NE (AR_TREE_EXTERN_DECL, $2, ZN); }

external_directives_declaration : 
  TOK_EXTERN extern_list opt_semi_colon
  { $$ = NE (AR_TREE_EXTERN_DECL, REVERSE ($2), ZN); }

extern_list :
  extern_list TOK_SEMICOLON extern_decl 
  { $$ = $3; $$->next = $1; }  
| extern_decl 
;

extern_decl :
  identifier extern_term TOK_EQ extern_term
  { $$ = NE(AR_TREE_EXTERN_DIRECTIVE, $1, ZN); $1->next = $2; $2->next = $4; }
| identifier extern_term
  { $$ = NE(AR_TREE_EXTERN_DIRECTIVE, $1, ZN); $1->next = $2; }
;

extern_term_list :
  extern_term_list TOK_COMMA extern_term
  { $$ = $3; $$->next = $1; }  
| extern_term
;

extern_term :
  boolean_constant
| signed_integer
| real
| string
| identifier_path
| identifier TOK_LPAR extern_term_list TOK_RPAR
  { $$ = NE (AR_TREE_EXTERN_TERM_FUNCTION, $1, ZN); $1->next = REVERSE ($3); }
| TOK_LBRACE extern_term_list TOK_RBRACE
  { $$ = NE (AR_TREE_EXTERN_TERM_SET, REVERSE ($2), ZN); }
| TOK_LANGLE TOK_FLOW identifier_path TOK_RANGLE
  { $$ = NE (AR_TREE_EXTERN_TERM_FLOW, $3, ZN); }
| TOK_LANGLE TOK_STATE identifier_path TOK_RANGLE
  { $$ = NE (AR_TREE_EXTERN_TERM_STATE, $3, ZN); }
| TOK_LANGLE TOK_EVENT identifier_path TOK_RANGLE
  { $$ = NE (AR_TREE_EXTERN_TERM_EVENT, $3, ZN); }
| TOK_LANGLE TOK_SUB identifier_path TOK_RANGLE
  { $$ = NE (AR_TREE_EXTERN_TERM_SUB, $3, ZN); }
| TOK_LANGLE TOK_LOCAL identifier_path TOK_RANGLE
  { $$ = NE (AR_TREE_EXTERN_TERM_LOCAL, $3, ZN); }
| TOK_LANGLE TOK_GLOBAL identifier_path TOK_RANGLE        
  { $$ = NE (AR_TREE_EXTERN_TERM_GLOBAL, $3, ZN); }
| TOK_LANGLE TOK_TERM TOK_LPAR expression TOK_RPAR TOK_RANGLE 
  { $$ = NE (AR_TREE_EXTERN_TERM_EXPR, $4, ZN); }
;  

/*                                                          sort_declaration */
sort_declaration :
  TOK_SORT id_list TOK_SEMICOLON
  { $$ = NE(AR_TREE_SORT,$2,ZN); }
;

/*                                                     signature_declaration */
signature_declaration :
  TOK_SIG signature  TOK_SEMICOLON
  { $$ = NE(AR_TREE_SIGNATURE,$2,ZN); }
;

signature : 
  operator_name TOK_COLON cartesian_product TOK_TGT base_type 
  { $$ = $1; $1->next = $3; $3->next = $5; }
;

operator_name :
  identifier 
;

cartesian_product :
  cartesian_product_list
  { $$ = NE(AR_TREE_CARTESIAN_PRODUCT,REVERSE($1),ZN); }
| /* empty */
  { $$ = NE(AR_TREE_CARTESIAN_PRODUCT,ZN,ZN); }
;

cartesian_product_list :
  cartesian_product_list TOK_STAR base_type
  { $$ = $3; $3->next = $1; }
| base_type
;

base_type :
  domain
;

/*                                                          mecv_description */
mecv_description :
  mecv_definition
| TOK_MECV_COMMAND
  { $$ = NN (AR_TREE_MECV_COMMAND, CCL_PARSE_TREE_STRING, ZN, ZN); 
    $$->value.string_value = altarica_token_value.string_value; SL ($$);}
;

mecv_definition :
  TOK_BEGIN equations_system TOK_END  
  { $$ = NE(AR_TREE_EQUATIONS_SYSTEM,$2,ZN); }
| mecv_equation TOK_SEMICOLON
  { $$ = NE(AR_TREE_EQUATIONS_SYSTEM,$1,ZN); }
;

equations_system :
  TOK_LOCAL mecv_equation TOK_SEMICOLON equations_system
  { $$ = NE(AR_TREE_LOCAL_EQUATION,$2,ZN); $$->next = $4; }
| mecv_equation TOK_SEMICOLON equations_system
  { $$ = $1; $1->next = $3; }
| /* empty */
  { $$ = ZN; }
  ;

mecv_equation :
identifier TOK_LPAR eq_parameters_list TOK_RPAR fixpoint_op boolean_expr 
{ $$ = $5; $6->next = $$->child; $$->child = $1; $1->next = $3; $3->next = $6; }
|
bang_identifier TOK_LPAR eq_parameters_list TOK_RPAR fixpoint_op boolean_expr 
{ $$ = $5; $6->next = $$->child; $$->child = $1; $1->next = $3; $3->next = $6; }
;

fixpoint_op :
  TOK_LFP
  { $$ = NE(AR_TREE_EQ_LFP,ZN,ZN); $$->value.int_value = 1; }
| TOK_PLUS unsigned_integer TOK_EQ 
  { $$ = NE(AR_TREE_EQ_LFP,$2,ZN); $$->value.int_value = $2->value.int_value; }
| TOK_GFP
  { $$ = NE(AR_TREE_EQ_GFP,ZN,ZN); $$->value.int_value = 1; }
| TOK_MINUS unsigned_integer TOK_EQ
  { $$ = NE(AR_TREE_EQ_GFP,$2,ZN); $$->value.int_value = $2->value.int_value; }
| TOK_ASSIGN
  { $$ = NE(AR_TREE_EQ_DEF,ZN,ZN); $$->value.int_value = -1; }
;

eq_parameters_list:
 eq_parameters 
 { $$ = NE(AR_TREE_EQ_PARAMETERS,$1,ZN); }
| /* empty */
 { $$ = NE(AR_TREE_EQ_PARAMETERS,ZN,ZN); }
 ;

eq_parameters:
   identifier
 | typed_identifier
 | identifier TOK_COMMA eq_parameters
   { $$ = $1; $$->next = $3; }
 | typed_identifier TOK_COMMA eq_parameters
   { $$ = $1; $$->next = $3; }
 ;

typed_identifier :
 identifier TOK_COLON domain 
 { $$ = NE(AR_TREE_TYPED_ID,$1,ZN); $1->next = $3; }
| identifier TOK_COLON bang_identifier
 { $$ = NE(AR_TREE_TYPED_ID,$1,ZN); $1->next = $3; }
 ;

/*                                                              ctl_formulas */
ctlstar_specification :
  TOK_CTLSTARSPEC ctlstar_formula 
 { $$ = NE (AR_TREE_CTLSTAR_SPEC, $2, ZN); }
;


ctlstar_formula :
  ctlstar_atom
  { $$ = NE (AR_TREE_CTLSTAR_ATOM, $1, ZN); }
| TOK_LPAR ctlstar_formula TOK_RPAR
  { $$ = NE (AR_TREE_CTLSTAR_PARENTHESED, $2, ZN); }
| TOK_NOT ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_NOT, $2, ZN); }
| TOK_TILDE ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_NOT, $2, ZN); }
| ctlstar_formula TOK_AND ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_AND, $1, ZN); $1->next = $3; }
| ctlstar_formula TOK_OR ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_OR, $1, ZN); $1->next = $3; }
| ctlstar_formula TOK_IMPLY ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_IMPLY, $1, ZN); $1->next = $3; }
| ctlstar_formula TOK_XOR ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_XOR, $1, ZN); $1->next = $3; }
| ctlstar_formula TOK_EQUIV ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_EQUIV, $1, ZN); $1->next = $3; }
| TOK_AX ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_AX, $2, ZN); }
| TOK_AG ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_AG, $2, ZN); }
| TOK_AF ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_AF, $2, ZN); }
| TOK_EX ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_EX, $2, ZN); }
| TOK_EG ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_EG, $2, ZN); }
| TOK_EF ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_EF, $2, ZN); }
| TOK_A TOK_LBRACKET ctlstar_formula TOK_RBRACKET
  { $$ = NE (AR_TREE_CTLSTAR_A, $3, ZN); }
| TOK_E TOK_LBRACKET ctlstar_formula TOK_RBRACKET
  { $$ = NE (AR_TREE_CTLSTAR_E, $3, ZN); }
| TOK_X ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_X, $2, ZN); }
| TOK_F ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_F, $2, ZN); }
| TOK_G ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_G, $2, ZN); }
| ctlstar_formula TOK_U ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_U, $1, ZN); $1->next = $3; }
| ctlstar_formula TOK_W ctlstar_formula
  { $$ = NE (AR_TREE_CTLSTAR_W, $1, ZN); $1->next = $3; }
;

ctlstar_atom :
  identifier
| atomic_state_expression
| boolean_constant 
;

/*                                                        acheck_description */
acheck_description : 
  with_block
;

with_block : 
  TOK_WITH id_list TOK_DO equation_or_command_list TOK_DONE
  { 
    altarica_tree *t = NE (AR_TREE_SYMBOLICALLY, ZN, $2); 
    $$ = NE (AR_TREE_WITH, t, ZN); 
    $2->next = $4; 
  }
| TOK_WITH id_list TOK_DO TOK_SYMBOLICALLY equation_or_command_list TOK_DONE
  { 
    altarica_tree *t = NE (AR_TREE_SYMBOLICALLY, ZN, $2); 
    $$ = NE (AR_TREE_WITH, t, ZN); 
    $2->next = $5; 
  }
| TOK_WITH id_list TOK_DO TOK_EXHAUSTIVELY equation_or_command_list TOK_DONE
  { 
    altarica_tree *t = NE (AR_TREE_EXHAUSTIVELY, ZN, $2); 
    $$ = NE (AR_TREE_WITH, t, ZN); 
    $2->next = $5; 
  }
;

equation_or_command_list : 
  equation_or_command TOK_SEMICOLON equation_or_command_list
  { $$ = $1; $$->next = $3; }
| equation_or_command TOK_SEMICOLON
  { $$ = $1; }
;

equation_or_command : 
  equation 
| redirected_command
;

equation :
  identifier TOK_ASSIGN ctlstar_specification
  { $$ = NE(AR_TREE_EQ_DEF,$1,ZN); $1->next = $3; }
| identifier TOK_ASSIGN st_formula 
  { $$ = NE(AR_TREE_EQ_DEF,$1,ZN); $1->next = $3; }
| identifier TOK_LFP st_formula 
  { $$ = NE(AR_TREE_EQ_LFP,$1,ZN); $1->next = $3; }
| identifier TOK_GFP st_formula 
  { $$ = NE(AR_TREE_EQ_GFP,$1,ZN); $1->next = $3; }
| identifier error
  { 
    altarica_format_error ($1, 0, "missing definition of set '%s'", 
			   $1->value.id_value);
    YYERROR; 
  }
;

redirected_command :
  command 
 { $$ = NE(AR_TREE_CMD,$1,ZN); }
| command TOK_RANGLE identifier
 { $$ = NE(AR_TREE_CRT_CMD,$1,ZN); $1->next = $3; }
| command TOK_RANGLE_RANGLE identifier
 { $$ = NE(AR_TREE_APPEND_CMD,$1,ZN); $1->next = $3; }
;

command :
  TOK_WTS TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_WTS,$3,ZN); $3->next = $5; }
| TOK_DOT TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_DOT,$3,ZN); $3->next = $5; }
| TOK_DOTTRACE TOK_LPAR st_formula TOK_COMMA st_formula TOK_COMMA 
  st_formula TOK_RPAR
  { $$ = NE(AR_TREE_DOTTRACE,$3,ZN); $3->next = $5; $5->next = $7; }
| TOK_GML TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_GML,$3,ZN); $3->next = $5; }

| TOK_TEST TOK_LPAR st_formula TOK_COMMA unsigned_integer TOK_RPAR
  {
    $$ = NE (AR_TREE_TEST, $3, ZN);
    $$->value.int_value = 0;
    $3->next = NE (AR_TREE_FALSE, ZN, $5);
  }
| TOK_TEST TOK_LPAR st_formula TOK_RPAR
  {
    $$ = NE (AR_TREE_TEST, $3, ZN);
    $$->value.int_value = 0;
    $3->next = NE (AR_TREE_FALSE, ZN, ZN);
  }
| TOK_TEST TOK_LPAR st_formula TOK_COMMA unsigned_integer TOK_COMMA
  boolean_constant TOK_RPAR
  {
    $$ = NE(AR_TREE_TEST,$3,ZN); $$->value.int_value = 0;
    $3->next = $7;
    $7->next = $5;
  }
| TOK_TEST TOK_LPAR st_formula TOK_COMMA boolean_constant TOK_RPAR
  {
    $$ = NE (AR_TREE_TEST, $3, ZN);
    $$->value.int_value = 0;
    $3->next = $5;
  }
| TOK_NRTEST TOK_LPAR st_formula TOK_COMMA unsigned_integer TOK_RPAR
  { $$ = NE(AR_TREE_TEST,$3,ZN); $$->value.int_value = 1;
    $3->next = NE (AR_TREE_FALSE, ZN, $5);
  }
| TOK_NRTEST TOK_LPAR identifier TOK_RPAR
  { $$ = NE(AR_TREE_NRTEST,$3,ZN); }
| TOK_SHOW TOK_LPAR id_list TOK_RPAR
  { $$ = NE(AR_TREE_SHOW,$3,ZN); }
| TOK_REMOVE TOK_LPAR id_list TOK_RPAR
  { $$ = NE(AR_TREE_REMOVE,$3,ZN); }
| TOK_DISPLAY TOK_LPAR id_list TOK_RPAR
  { $$ = NE(AR_TREE_DISPLAY,$3,ZN); }
| TOK_EVENTS TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_EVENTS,$3,ZN); SL ($$); }
| TOK_QUOT TOK_LPAR TOK_RPAR
  { $$ = NE(AR_TREE_QUOT,ZN,ZN); }
| TOK_MODES TOK_LPAR TOK_RPAR
  { $$ = NE(AR_TREE_MODES,ZN,ZN); }
| TOK_VALIDATE TOK_LPAR TOK_RPAR
  { $$ = NE(AR_TREE_VALIDATE,ZN,ZN); }  
| TOK_PROJECT TOK_LPAR st_formula TOK_COMMA 
  TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR 
  TOK_COMMA identifier 
  TOK_COMMA boolean_constant 
  opt_subnode_name
  TOK_RPAR
  { $$ = NE(AR_TREE_PROJECT,$3,ZN); $3->next = $6; $6->next = $8; 
    $8->next = $11; $11->next = $13; $13->next = $14; }
| TOK_PROJECT TOK_LPAR st_formula TOK_COMMA st_formula TOK_COMMA identifier 
  TOK_COMMA boolean_constant opt_subnode_name TOK_RPAR
  { $$ = NE(AR_TREE_PROJECT,$3,ZN); $3->next = $5; $5->next = $7; $7->next = $9;
    $9->next = $10;}
| TOK_CTL2MU TOK_LPAR ctlstar_formula TOK_RPAR
  { $$ = NE(AR_TREE_CTL2MU,$3,ZN);  }
;

opt_subnode_name :
  TOK_COMMA identifier_path
  { $$ = $2; }
| /* empty */
  { $$ = NULL; };
  ;

st_formula : 
  st_or_expression
;

st_or_expression :
  st_or_expression TOK_OR st_and_expression
  { $$ = NE(AR_TREE_OR,$1,ZN); $1->next = $3; }
| st_and_expression
  { $$ = $1; }
;

st_and_expression :
  st_and_expression TOK_AND st_sub_expression
  { $$ = NE(AR_TREE_AND,$1,ZN); $1->next = $3; }
| st_sub_expression
  { $$ = $1; }
;

st_sub_expression :
  st_sub_expression TOK_MINUS st_unary_expression
  { $$ = NE(AR_TREE_SUB,$1,ZN); $1->next = $3; }
| st_unary_expression
  { $$ = $1; }
;

st_unary_expression : 
  TOK_NOT st_unary_expression 
  { $$ = NE(AR_TREE_NOT,$2,ZN); }
| st_atomic_expression
  { $$ = $1; }
;

st_atomic_expression :
 TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_PARENTHEZED_EXPR,$2,ZN); }
| identifier
| atomic_state_expression
| atomic_transition_expression
| common_atomic_expression
;

atomic_state_expression :
  TOK_TGT TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_TGT,$3,ZN); SL($$); }
| TOK_SRC TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_SRC,$3,ZN); SL($$); }
| TOK_REACH TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_REACH,$3,ZN); $3->next = $5; }
| TOK_COREACH TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_COREACH,$3,ZN); $3->next = $5; }
| TOK_LBRACKET boolean_expr TOK_RBRACKET
  { $$ = NE(AR_TREE_EXPR,$2,ZN); SL($$); }
| TOK_UNAV TOK_LPAR st_formula  TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_UNAV,$3,ZN); $3->next = $5; }
| TOK_ASSERT TOK_LPAR st_formula TOK_RPAR
  { $$ = NE (AR_TREE_ASSERT ,$3,ZN); SL ($$); }
;

common_atomic_expression :
  TOK_PICK TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_PICK,$3,ZN); SL ($$); }
| TOK_PROJ_S TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_PROJ_S,$3,ZN); SL ($$); }
| TOK_PROJ_F TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_PROJ_F,$3,ZN); SL ($$); }
;

atomic_transition_expression :
  TOK_RTGT TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_RTGT,$3,ZN); SL($$); }
| TOK_RSRC TOK_LPAR st_formula TOK_RPAR
  { $$ = NE(AR_TREE_RSRC,$3,ZN); SL($$); }
| TOK_LOOP TOK_LPAR st_formula TOK_COMMA st_formula TOK_RPAR
  { $$ = NE(AR_TREE_LOOP,$3,ZN); $3->next = $5; } 
| TOK_TRACE TOK_LPAR st_formula TOK_COMMA st_formula TOK_COMMA 
  st_formula TOK_RPAR
  { $$ = NE(AR_TREE_TRACE,$3,ZN); $3->next = $5; $5->next = $7; }
| TOK_LABEL identifier_path
  { $$ = NE(AR_TREE_LABEL,$2,ZN);  }
| TOK_ATTRIBUTE identifier_path
  { $$ = NE(AR_TREE_EVENT_ATTRIBUTE,$2,ZN);  }
;

/*                                                       altarica expressions */
aexpr_description : 
  TOK_AEXPR_BEGIN boolean_expr TOK_AEXPR_END
  { $$ = $2; }
;

/*                                                               arc commands */

arc_commands : 
  arc_redirected_command TOK_NEWLINE
{ $$ = $1; }
;

arc_redirected_command :
  arc_command TOK_SUP string
  { $$ = NE(AR_TREE_SHELL_REDIR_CREATE,$3,NULL); $3->next = $1; }
| arc_command TOK_SUPSUP string
  { $$ = NE(AR_TREE_SHELL_REDIR_APPEND,$3,NULL); $3->next = $1; }
| arc_command TOK_PIPE string
  { $$ = NE(AR_TREE_SHELL_REDIR_PIPE,$3,NULL); $3->next = $1; }
| arc_command
  { $$ = $1; }
;

arc_command :  
  arc_command_name arc_parameter_list
  { $$ = NE(AR_TREE_SHELL_CMD,$1,NULL); $1->next = $2;  }
;

arc_command_name :
  string
;

arc_parameter_list :
  arc_parameter arc_parameter_list
{ $$ = $1; $1->next = $2; }
| /* empty */
{ $$ = NULL; }
;

signed_integer :
  TOK_UINTEGER
  { $$ = NINT(); $$->value = altarica_token_value; SL($$); }
| TOK_MINUS TOK_UINTEGER
  { $$ = NINT(); $$->value.int_value = -altarica_token_value.int_value;
    SL($$); }
| TOK_PLUS TOK_UINTEGER
  { $$ = NINT(); $$->value = altarica_token_value; SL($$); }
  ;

arc_parameter : 
  signed_integer   
| string
| real
;

real :
  TOK_FLOAT
  { $$ = NFLT(); $$->value = altarica_token_value; SL($$); }
| TOK_MINUS TOK_FLOAT
  { $$ = NFLT(); $$->value.flt_value = -altarica_token_value.flt_value; SL($$); }
| TOK_PLUS TOK_FLOAT
  { $$ = NFLT(); $$->value = altarica_token_value; SL($$); }
  ;

string : TOK_STRING
  { $$ = NSTR(); $$->value = altarica_token_value; SL($$); }
  ;
%%

