/*
 * altarica-input.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include "altarica-input-p.h"
#include "altarica-input.h"

altarica_tree *
altarica_read_file (const char *filename, int modes, 
		    int (*is_cmd) (const char *id, void *data),
		    void *data)
{
  altarica_tree *result = NULL;
  FILE *input_stream = fopen (filename, "r");

  if (input_stream != NULL)
    {
      result = altarica_read_stream (input_stream, filename, modes, is_cmd,
				     data);
      fclose (input_stream);
    }

  return result;
}

			/* --------------- */

altarica_tree *
altarica_read_stream (FILE *stream, const char *input_name, int modes, 
		      int (*is_cmd) (const char *id, void *data),
		      void *data)
{
  altarica_tree *result;

  altarica_number_of_errors = 0;
  result = altarica_syntax_tree = altarica_syntax_tree_container = NULL;
  altarica_init_lexer (stream, input_name, modes, is_cmd, data);
  if (altarica_parse () == 0)
    result = altarica_syntax_tree;
  else
    ccl_parse_tree_delete_container (altarica_syntax_tree_container);

  altarica_terminate_lexer ();

  return result;
}

			/* --------------- */

altarica_tree *
altarica_read_string (const char *input, const char *input_name, int modes, 
		      int (*is_cmd) (const char *id, void *data),
		      void *data)
{
  altarica_tree *result;

  altarica_number_of_errors = 0;
  result = altarica_syntax_tree = altarica_syntax_tree_container = NULL;
  altarica_init_lexer_with_string (input, input_name, modes, is_cmd, data);

  if (altarica_parse () == 0)
    result = altarica_syntax_tree;
  else
    ccl_parse_tree_delete_container (altarica_syntax_tree_container);
  
  altarica_terminate_lexer ();

  return result;
}

			/* --------------- */

extern char *altarica_text;
static const char *
s_parser_mode (void)
{
  const char *mode = NULL;

  switch (altarica_input_get_mode ()) 
    {
    case AR_INPUT_NONE: mode = "any text"; break;
    case AR_INPUT_ALTARICA: mode = "altarica"; break;    
    case AR_INPUT_MECV: mode = "mec5"; break;
    case AR_INPUT_ACHECK: mode = "acheck"; break;
    case AR_INPUT_AEXPR: mode = "altarica expression"; break;
    case AR_INPUT_ARC: mode = "arc"; break;
    case AR_INPUT_CTLSTAR: mode = "ctl*"; break;
    }
  ccl_post (mode != NULL);

  return mode;
}

			/* --------------- */

void
altarica_error (const char *message)
{
  const char *fmt;
  const char *token;

  if (altarica_text[0] == '\0')
    {
      token = "end of input";
      fmt = "%s on symbol %s";
    }
  else
    {
      token = altarica_text;
      fmt = "%s on symbol '%s'";
    }

  altarica_format_error (NULL, 1, fmt, message, token);
}

			/* --------------- */

void
altarica_format_error (altarica_tree *loc, int display_mode, const char *fmt, 
		       ...)
{
  const char *filename;
  int line;
  va_list pa;

  altarica_number_of_errors++; 

  if (loc != NULL)
    {
      filename = loc->filename;
      line = loc->line;
    }
  else
    {
      filename = altarica_current_filename;
      line = altarica_current_line;
    }
  ccl_error ("%s:%d: error: ", filename, line);
  va_start (pa, fmt);
  ccl_log_va (CCL_LOG_ERROR, fmt, pa);
  va_end (pa);

  if (display_mode)
    ccl_error (" (%s mode)",  s_parser_mode ());
  ccl_error (".\n");
}

			/* --------------- */

int
altarica_wrap (void)
{
  return 1;
}

			/* --------------- */

