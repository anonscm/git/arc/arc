/*
 * altarica-tree-acheck.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "altarica-tree-p.h"

			/* --------------- */

static void
s_log_equation (ccl_log_type log, const altarica_tree *top,
		ar_identifier_translator *it)
{
  const char *symbol;
  const altarica_tree *id = top->child;
  const altarica_tree *F = id->next;

  if (IS_LABELLED_BY (top, EQ_DEF))
    symbol = ":=";
  else if (IS_LABELLED_BY (top, EQ_LFP))
    symbol = "+=";
  else /* IS_LABELLED_BY (top, EQ_GFP)) */
    symbol = "-=";

  TRANSLATE (AR_IC_IDENTIFIER, id);
  ccl_log (log, " %s ", symbol);
  ar_tree_log_acheck_expr (log, F, it);    
}

			/* --------------- */

static void
s_log_command_name (ccl_log_type log, const altarica_tree *cmd)
{
  const char *cmdname = NULL;

  switch (cmd->node_type)
    {
    case AR_TREE_WTS: cmdname = "wts"; break;
    case AR_TREE_DOT: cmdname = "dot"; break;
    case AR_TREE_GML: cmdname = "gml"; break;
    case AR_TREE_TEST: cmdname = "test"; break;
    case AR_TREE_NRTEST: cmdname = "nrtest"; break;
    case AR_TREE_SHOW: cmdname = "show"; break;
    case AR_TREE_DISPLAY: cmdname = "display"; break;
    case AR_TREE_QUOT: cmdname = "quot"; break;
    case AR_TREE_MODES: cmdname = "modes"; break;
    case AR_TREE_PROJECT: cmdname = "project"; break;
    case AR_TREE_EVENTS: cmdname = "events"; break;
    case AR_TREE_CTL2MU: cmdname = "ctl2mu"; break;
    default: INVALID_NODE_TYPE (cmd);
    }

  ccl_log (log, "%s", cmdname);
}

			/* --------------- */

static void
s_log_command (ccl_log_type log, const altarica_tree *top, 
	       ar_identifier_translator *it)
{
  const altarica_tree *cmd = top->child;
  const altarica_tree *filename = cmd->next;

  s_log_command_name (log, cmd);
  ccl_log (log, " (");
  switch (cmd->node_type)
    {
    case AR_TREE_WTS:
    case AR_TREE_DOT:
    case AR_TREE_GML:
      ar_tree_log_acheck_expr (log, cmd->child, it);
      ccl_log (log, ", ");
      ar_tree_log_acheck_expr (log, cmd->child->next, it);
      break;

    case AR_TREE_TEST:
      ar_tree_log_acheck_expr (log, cmd->child, it);
      if (cmd->child->next->next != NULL)
	ccl_log (log, ", %d", cmd->child->next->next->value.int_value);
      if (cmd->child->next->value.int_value)
	ccl_log (log, ", true");
      break;

    case AR_TREE_EVENTS:
      ar_tree_log_acheck_expr (log, cmd->child, it);
      break;

    case AR_TREE_NRTEST:
      TRANSLATE (AR_IC_ACHECK_FILENAME, cmd->child);
      break;

    case AR_TREE_REMOVE:
    case AR_TREE_SHOW:
    case AR_TREE_DISPLAY:      
      {
	const altarica_tree *t;
	ccl_pre (IS_LABELLED_BY (cmd->child, ID_LIST));

	for (t = cmd->child->child; t; t = t->next)
	  {
	    if (IS_LABELLED_BY (cmd, SHOW) && 
		strcmp (t->value.id_value, "all") == 0)
	      ccl_log (log, "all");
	    else
	      ar_tree_log_acheck_expr (log, t, it);
	    if (t->next != NULL)
	      ccl_log (log, ", ");
	  }
      }
      break;

    case AR_TREE_CTL2MU:
      ar_tree_log_ctlstar_expr (log, top->child, it);
      break;

    case AR_TREE_QUOT: 
    case AR_TREE_MODES: 
      break;

    case AR_TREE_PROJECT:
      {
	altarica_tree *S = cmd->child;
	altarica_tree *T = S->next;
	altarica_tree *tgt = T->next;
	altarica_tree *simplify = tgt->next;
	altarica_tree *subnode = simplify->next;

	ar_tree_log_acheck_expr (log, S, it);
	ccl_log (log, ", ");
	ar_tree_log_acheck_expr (log, T, it);
	ccl_log (log, ", ");
	TRANSLATE (AR_IC_ACHECK_PROJECT_TGT, tgt);
	ccl_log (log, ", ");
	if (IS_LABELLED_BY (simplify, TRUE))
	  ccl_log (log, "true");
	else
	  ccl_log (log, "false");
	if (subnode != NULL)
	  {
	    ccl_log (log, ", ");
	    ar_tree_log_identifier_path (log, subnode, it);
	  }
      }
      break;

    default:
      INVALID_NODE_TYPE (cmd);
    }

  ccl_log (log, ")");
  if (! IS_LABELLED_BY (top, CMD))
    {
      const char *redir = IS_LABELLED_BY (top, CRT_CMD) ? ">" : ">>";
      ccl_log (log, " %s ", redir);
      TRANSLATE (AR_IC_ACHECK_FILENAME, filename);
    }
}

			/* --------------- */

static void
s_with (ccl_log_type log, const altarica_tree *top, 
	ar_identifier_translator *it)
{
  altarica_tree *t;
  const altarica_tree *node_names;
  altarica_tree *body;
  int symbolically = 1;

  ccl_pre (IS_LABELLED_BY (top, WITH));
  top = top->child;

  symbolically = !(IS_LABELLED_BY (top, EXHAUSTIVELY));
  if (symbolically)
    top = top->next;

  node_names = top;
  body = node_names->next;

  ccl_log (log, "with ");
  t = node_names->child;
  TRANSLATE (AR_IC_IDENTIFIER, t);
  for (t = t->next; t; t = t->next)
    {
      ccl_log (log, ", ");
      TRANSLATE (AR_IC_IDENTIFIER, t);
    }
  ccl_log (log, " do\n");
  if (symbolically)
    ccl_log (log, "  symbolically \n");
  for (t = body; t; t = t->next)
    {
      ccl_log (log, "  ");
      switch (t->node_type)
	{
	case AR_TREE_EQ_DEF:
	case AR_TREE_EQ_LFP:
	case AR_TREE_EQ_GFP:
	  s_log_equation (log, t, it);
	  break;

	case AR_TREE_CMD:
	case AR_TREE_CRT_CMD:
	case AR_TREE_APPEND_CMD:
	  s_log_command (log, t, it);
	  break;
	  
	default:
	  INVALID_NODE_TYPE (t);
	}
      ccl_log (log, "; \n");
    }

  ccl_log (log, "done\n");  
}

			/* --------------- */

void
ar_tree_log_acheck (ccl_log_type log, const altarica_tree *top,
		    ar_identifier_translator *it)
{
  const altarica_tree *t;

  for (t = top; t; t = t->next)
    {
      s_with (log, t, it);
      ccl_log (log, "\n");
    }
}

			/* --------------- */

static const char *
s_op_name (const altarica_tree *t)
{
  switch (t->node_type)
    {
    case AR_TREE_ASSERT:
      return "assert";
    case AR_TREE_OR:
      return "or";
    case AR_TREE_AND:
      return "and";
    case AR_TREE_SUB:
      return "-";
    case AR_TREE_NOT:
      return "not";
    case AR_TREE_RSRC:
      return "rsrc";
    case AR_TREE_RTGT:
      return "rtgt";
    case AR_TREE_SRC:
      return "src";
    case AR_TREE_TGT:
      return "tgt";
    case AR_TREE_LABEL:
      return "label";
    case AR_TREE_EVENT_ATTRIBUTE:
      return "attribute";
    case AR_TREE_REACH:
      return "reach";
    case AR_TREE_COREACH:
      return "coreach";
    case AR_TREE_LOOP:
      return "loop";
    case AR_TREE_UNAV:
      return "unav";
    case AR_TREE_TRACE:
      return "trace";
    case AR_TREE_PICK:
      return "pick";
    case AR_TREE_PROJ_S:
      return "proj_s";
    case AR_TREE_PROJ_F:
      return "proj_f";
    default:
      ccl_throw_no_msg (internal_error);
    }

  return NULL;
}

			/* --------------- */

void
ar_tree_log_acheck_expr (ccl_log_type log, const altarica_tree *t,
			 ar_identifier_translator *it)
{
  switch (t->node_type)
    {
    case AR_TREE_OR:
    case AR_TREE_AND:
    case AR_TREE_SUB:
      ar_tree_log_acheck_expr (log, t->child, it);
      ccl_log (log, " %s ", s_op_name (t));
      ar_tree_log_acheck_expr (log, t->child->next, it);
      break;

    case AR_TREE_PARENTHEZED_EXPR:
      ccl_log (log, "(");
      ar_tree_log_acheck_expr (log, t->child, it);
      ccl_log (log, ")");
      break;

    case AR_TREE_RSRC:
    case AR_TREE_RTGT:
    case AR_TREE_SRC:
    case AR_TREE_ASSERT:
    case AR_TREE_TGT:
    case AR_TREE_REACH:
    case AR_TREE_COREACH:
    case AR_TREE_UNAV:
    case AR_TREE_LOOP:
    case AR_TREE_TRACE:
    case AR_TREE_PICK:
    case AR_TREE_PROJ_S:
    case AR_TREE_PROJ_F:
      ccl_log (log, "%s (", s_op_name (t));
      for (t = t->child; t; t = t->next)
	{
	  ar_tree_log_acheck_expr (log, t, it);
	  if (t->next != NULL)
	    ccl_log (log, ",");
	}
      ccl_log (log, ")");
      break;

    case AR_TREE_NOT:
    case AR_TREE_LABEL:
    case AR_TREE_EVENT_ATTRIBUTE:
      ccl_log (log, "%s ", s_op_name (t));
      if (t->node_type == AR_TREE_NOT)
	ar_tree_log_acheck_expr (log, t->child, it);
      else
	ar_tree_log_identifier_path (log, t->child, it);
      break;

    case AR_TREE_EXPR:
      ccl_log (log, "[");
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, "]");
      break;

    case AR_TREE_IDENTIFIER:
      TRANSLATE (AR_IC_ACHECK_SET, t);
      break;

    default:
      INVALID_NODE_TYPE (t);
      break;
    }
}

			/* --------------- */
static const char *
s_ctl_op_name (const altarica_tree *t)
{
  switch (t->node_type)
    {
    case AR_TREE_CTLSTAR_AND: return "and"; 
    case AR_TREE_CTLSTAR_OR: return "or"; 
    case AR_TREE_CTLSTAR_IMPLY: return "=>"; 
    case AR_TREE_CTLSTAR_XOR: return "xor"; 
    case AR_TREE_CTLSTAR_EQUIV: return "<=>"; 
    case AR_TREE_CTLSTAR_U: return "U"; 
    case AR_TREE_CTLSTAR_NOT: return "not";
    case AR_TREE_CTLSTAR_AX: return "AX";
    case AR_TREE_CTLSTAR_AG: return "AG";
    case AR_TREE_CTLSTAR_AF: return "AF";
    case AR_TREE_CTLSTAR_EX: return "EX";
    case AR_TREE_CTLSTAR_EG: return "EG";
    case AR_TREE_CTLSTAR_EF: return "EF";
    case AR_TREE_CTLSTAR_X: return "X";
    case AR_TREE_CTLSTAR_F: return "F";
    case AR_TREE_CTLSTAR_G: return "G";
    case AR_TREE_CTLSTAR_A: return "A";
    case AR_TREE_CTLSTAR_E: return "E";
    case AR_TREE_CTLSTAR_W: return "W";
    default: INVALID_NODE_TYPE (t);
    }
  return  NULL;
}

			/* --------------- */

void
ar_tree_log_ctlstar_expr (ccl_log_type log, const altarica_tree *t,
			  ar_identifier_translator *it)
{
  switch (t->node_type)
    {
    case AR_TREE_CTLSTAR_ATOM:
      if (IS_LABELLED_BY (t->child, TRUE))
	ccl_log (log, "true");
      else if (IS_LABELLED_BY (t->child, FALSE))
	ccl_log (log, "false");
      else 
	ar_tree_log_acheck_expr (log, t->child, it);
      break;

    case AR_TREE_CTLSTAR_PARENTHESED:
      ccl_log (log, "(");
      ar_tree_log_ctlstar_expr (log, t->child, it);
      ccl_log (log, ")");
      break;


    case AR_TREE_CTLSTAR_AND:
    case AR_TREE_CTLSTAR_OR:
    case AR_TREE_CTLSTAR_IMPLY:
    case AR_TREE_CTLSTAR_XOR:
    case AR_TREE_CTLSTAR_EQUIV:
    case AR_TREE_CTLSTAR_U:
      ar_tree_log_ctlstar_expr (log, t->child, it);
      ccl_log (log, " %s ", s_ctl_op_name (t));
      ar_tree_log_ctlstar_expr (log, t->child->next, it);
      break;

    case AR_TREE_CTLSTAR_NOT: 
    case AR_TREE_CTLSTAR_AX:
    case AR_TREE_CTLSTAR_AG:
    case AR_TREE_CTLSTAR_AF:
    case AR_TREE_CTLSTAR_EX:
    case AR_TREE_CTLSTAR_EG:
    case AR_TREE_CTLSTAR_EF:
    case AR_TREE_CTLSTAR_X:
    case AR_TREE_CTLSTAR_F: 
    case AR_TREE_CTLSTAR_G: 
    case AR_TREE_CTLSTAR_W: 
      ccl_log (log, "%s ", s_ctl_op_name (t));
      ar_tree_log_ctlstar_expr (log, t->child, it);
      break;

    case AR_TREE_CTLSTAR_A: 
    case AR_TREE_CTLSTAR_E: 
      ccl_log (log, "%s [", s_ctl_op_name (t));
      ar_tree_log_ctlstar_expr (log, t->child, it);
      ccl_log (log, "]");
      break;

    default:
      INVALID_NODE_TYPE (t);
      break;
    }
}
