/*
 * altarica-tree-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ALTARICA_TREE_P_H
# define ALTARICA_TREE_P_H

# include "altarica-tree.h"

# define IS_LABELLED_BY(_t,_l) \
  ((_t) != NULL && (_t)->node_type == AR_TREE_ ## _l)

# define INVALID_NODE_TYPE(t)					\
  do {								\
    ccl_error ("node type is : %s\n", (t)->node_type_string);	\
    ccl_throw (internal_error, "invalid node type");		\
  } while (0)

extern void
ar_translate_identifier_tree (ar_identifier_translator *it, 
			      ar_identifier_context ctx, ccl_log_type log, 
			      const altarica_tree *t);

# define TRANSLATE(_ctx, _t) ar_translate_identifier_tree (it, _ctx, log, _t)

# define TRANSLATE_STR(_ctx, _st) \
  do { (it)->translate (it, log, _ctx, _st); } while (0)

extern void
ar_tree_log_identifier_path (ccl_log_type log, const altarica_tree *t,
			     ar_identifier_translator *it);

extern void
ar_tree_log_domain (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it);

extern void
ar_tree_log_expr (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it);

extern void
ar_tree_log_bang_identifier (ccl_log_type log, const altarica_tree *t,
			     ar_identifier_translator *it);

#endif /* ! ALTARICA_TREE_P_H */
