/*
 * altarica-tree-mecv.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "altarica-tree-p.h"

static void
s_equation_parameter (ccl_log_type log, const altarica_tree *t, 
		      ar_identifier_translator *it)
{
  if (IS_LABELLED_BY (t, IDENTIFIER))
    TRANSLATE (AR_IC_IDENTIFIER, t);
  else
    {
      const altarica_tree *id = t->child;
      const altarica_tree *domain = id->next;

      ccl_pre (IS_LABELLED_BY (t, TYPED_ID));
      
      TRANSLATE (AR_IC_IDENTIFIER, id);
      ccl_log (log, " : ");
      if (IS_LABELLED_BY (domain, BANG_ID))
	ar_tree_log_bang_identifier (log, domain, it);
      else
	ar_tree_log_domain (log, domain, it);
    }
}

			/* --------------- */

static void
s_equation  (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{
  const altarica_tree *id = t->child;
  const altarica_tree *params = id->next;
  const altarica_tree *F = params->next;

  if (IS_LABELLED_BY (id, IDENTIFIER))
    TRANSLATE (AR_IC_IDENTIFIER, id);
  else
    ar_tree_log_bang_identifier (log, id, it);
  ccl_log (log, " (");
  for (params = params->child; params; params = params->next)
    {
      s_equation_parameter (log, params, it);
      if (params->next != NULL)
	ccl_log (log, ", ");
    }
  ccl_log (log, ") ");
  if (IS_LABELLED_BY (t, EQ_DEF))
    ccl_log (log, ":=");
  else
    {
      if (IS_LABELLED_BY (t, EQ_LFP))
	ccl_log (log, "+");
      else
	ccl_log (log, "-");
      if (t->value.int_value != 1)
	ccl_log (log, "%d", t->value.int_value);
      ccl_log (log, "=");
    }
  ccl_log (log, " ");
  ar_tree_log_expr (log, F, it);
}

			/* --------------- */

void
ar_tree_log_mecv (ccl_log_type log, const altarica_tree *top, 
		  ar_identifier_translator *it)
{
  for (; top != NULL; top = top->next)
    {
      if (IS_LABELLED_BY (top, MECV_COMMAND))
	{
	  TRANSLATE_STR (AR_IC_MEC5_COMMAND, top->value.string_value);
	  ccl_log (log, "\n");
	}
      else
	{
	  int only_one;
	  const altarica_tree *t;
	  const altarica_tree *eqlist = top->child;

	  ccl_pre (IS_LABELLED_BY (top, EQUATIONS_SYSTEM));

	  only_one = (eqlist->next == NULL);
      
	  if (!only_one)
	    ccl_log (log, "begin\n");
	  for (t = eqlist; t; t = t->next)
	    {
	      const altarica_tree *eq = t;
	      if (!only_one)
		ccl_log (log, "  ");
	      if (IS_LABELLED_BY (t, LOCAL_EQUATION))
		{
		  ccl_log (log, "local ");
		  eq = t->child;
		}
	      s_equation (log, eq, it);
	      ccl_log (log, "; \n");
	    }
      
	  if (!only_one)
	    ccl_log (log, "end\n");
	}
    }
}

			/* --------------- */

void
ar_tree_log_bang_identifier (ccl_log_type log, const altarica_tree *t,
			     ar_identifier_translator *it)
{
  ccl_pre (IS_LABELLED_BY (t, BANG_ID));

  TRANSLATE (AR_IC_IDENTIFIER, t->child);
  ccl_log (log, "!");
  TRANSLATE (AR_IC_BANG_TYPE, t->child->next);
}
