/*
 * altarica-lexer.l -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

%{
#include <ctype.h>
#include <limits.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "parser/altarica-input-p.h"
#include "parser/altarica-syntax.h"

#define YY_NO_UNPUT
/* Some systems define fileno as a preprocessor macro. */
#ifndef fileno
extern int fileno (FILE *);
#endif

ccl_parse_tree_value altarica_token_value;
int altarica_current_line;
const char *altarica_current_filename = NULL;
static int c_comment_start_line = 0;
static int MODE = AR_INPUT_NONE;
static int allowed_modes = AR_INPUT_NONE;
static int allow_ctlstar = 0;

static int (*s_is_arc_cmd) (const char *id, void *data) = NULL;
static void *s_is_arc_cmd_data = NULL;

static void
s_change_current_filename (const char *f);

# define set_mode altarica_input_set_mode
%}

unsigned_integer  ([1-9][0-9]*)|0
float ([0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)

			/* --------------- */

simple_identifier [a-zA-Z_][a-zA-Z_0-9'^]*
quoted_identifier '[^']+'|\"[^"]*\"


ARC_NUMBER	  [+-]?[0-9][0-9]*
ARC_QUOTED_STRING \"[^"]*\"
ARC_STRING        [^ :><\t\r\n;"()][^:<>() \t\r\n;"]*
ARC_COMMAND_ID    {simple_identifier}(-{simple_identifier})+

			/* --------------- */

CONST      [cC][oO][nN][sS][tT]
DOMAIN     [dD][oO][mM][aA][iI][nN]
BOOL       [bB][oO][oO][lL]
INTEGER    [iI][nN][tT][eE][gG][eE][rR]
NODE       [nN][oO][dD][eE]
EDON       [eE][dD][oO][nN]
FLOW       [fF][lL][oO][wW]
STATE      [sS][tT][aA][tT][eE]
EVENT      [eE][vV][eE][nN][tT]
EVENTS     [eE][vV][eE][nN][tT][sS]
SUB        [sS][uU][bB]
TRANS      [tT][rR][aA][nN][sS]
ASSERT     [aA][sS][sS][eE][rR][tT]
SYNC       [sS][yY][nN][cC]
EXTERN     [eE][xX][tT][eE][rR][nN]
OR         [oO][rR]
AND        [aA][nN][dD]
IMPLY      [iI][mM][pP][lL][yY]
TRUE       [tT][rR][uU][eE]
FALSE      [fF][aA][lL][sS][eE]
NOT        [nN][oO][tT]
IF         [iI][fF]
THEN       [tT][hH][eE][nN]
ELSE       [eE][lL][sS][eE]
INIT       [iI][nN][iI][tT]
CASE       [cC][aA][sS][eE]
MIN        [mM][iI][nN]
MAX        [mM][aA][xX]
MOD        [mM][oO][dD]
STRUCT     [sS][tT][rR][uU][cC][tT]
TCURTS     [tT][cC][uU][rR][tT][sS]
SIG        [sS][iI][gG]
PARAM      [pP][aA][rR][aA][mM]
PARAM_SET  [pP][aA][rR][aA][mM]_[sS][eE][tT]
SORT       [sS][oO][rR][tT]

BEGIN      [bB][eE][gG][iI][nN]
END        [eE][nN][dD]
LOCAL      [lL][oO][cC][aA][lL]
GLOBAL     [gG][lL][oO][bB][aA][lL]		    
TERM       [tT][eE][rR][mM]

WITH       [wW][iI][tT][hH]
SYMBOLICALLY [Ss][Yy][Mm][Bb][Oo][Ll][Ii][Cc][Aa][Ll][Ll][Yy] 
EXHAUSTIVELY [Ee][Xx][Hh][Aa][Uu][Ss][Tt][Ii][Vv][Ee][Ll][Yy] 
DO         [dD][oO]
DONE       [dD][oO][nN][eE]
DOT        [dD][oO][tT]
DISPLAY    [dD][iI][sS][pP][lL][aA][yY]
GML        [gG][mM][lL]
SRC        [sS][rR][cC]
TGT        [tT][gG][tT]
WTS        [wW][tT][sS]
SHOW       [sS][hH][oO][wW]
RSRC       [rR][sS][rR][cC]
RTGT       [rR][tT][gG][tT]
LOOP       [lL][oO][oO][pP]
TRACE      [tT][rR][aA][cC][eE]
DOTTRACE   [dD][oO][tT]-[tT][rR][aA][cC][eE]
REACH      [rR][eE][aA][cC][hH]
TEST       [tT][eE][sS][tT]
NRTEST     [Nn][Rr][tT][eE][sS][tT]
COREACH    [cC][oO][rR][eE][aA][cC][hH]
LABEL      [lL][aA][bB][eE][lL]
ATTRIBUTE  [aA][tT][tT][rR][iI][bB][uU][tT][eE]
UNAV       [uU][nN][aA][vV]
PROJECT    [pP][rR][oO][jJ][eE][cC][tT]
PICK       [pP][iI][cC][kK]
PROJ_F     [pP][rR][oO][jJ]_[fF]
PROJ_S     [pP][rR][oO][jJ]_[sS]
QUOT       [qQ][uU][oO][tT]
MODES      [mM][oO][dD][eE][sS]
VALIDATE   [vV][aA][lL][iI][dD][aA][tT][eE]   
REMOVE     [rR][eE][mM][oO][vV][eE]
CTLSPEC    [cC][tT][lL][sS][pP][eE][cC]
CTL2MU     [cC][tT][lL][2][mM][uU]
XOR        [xX][oO][rR]

			/* --------------- */

cpp_comment       "//".*

			/* --------------- */

blank_character   [ \t\r]
newline           [\n]

%s altarica_lexer
%x c_comment
%s mecv_spec_lexer
%s acheck_spec_lexer
%s arc_command_lexer
%s ctlstar_spec_lexer
%option nounput
%option noinput

%%

^"#"[a-zA-Z]*[ ]+{unsigned_integer}([ ]*\".*\"[ ]*)?.*"\n" {
   char *s = yytext;
   while (*s && !isdigit(*s)) 
    s++;
   if (*s)
    {
      int line = 0;
      while (*s && isdigit(*s)) 
       {
         line *= 10;
         line += ((int)*s) - '0';
         s++;
       }
      altarica_current_line = line;

      while (*s && *s != '\"')
        s++;
      if (*s)
        {
          char *pos = strrchr (++s, '\"');
          if (pos)
            { 
               *pos = '\0';
               s_change_current_filename (s);
            }          
        }
    }
}

"/*" { 
  BEGIN(c_comment);
  c_comment_start_line = altarica_current_line; 
}

{cpp_comment} { ((void)0); }

<INITIAL>{CONST}|{DOMAIN}|{NODE}|{SORT}|{SIG}|{EXTERN} { 
  if( (allowed_modes & AR_INPUT_ALTARICA) ) {
    set_mode(AR_INPUT_ALTARICA); 
    BEGIN(altarica_lexer); 
    switch( yytext[0] ) {
    case 'c' : case 'C' : return TOK_CONST; 
    case 'd' : case 'D' : return TOK_DOMAIN; 
    case 'n' : case 'N' : return TOK_NODE; 
    case 'e' : case 'E' : return TOK_EXTERN; 
    case 's' : case 'S' : 
      if( yytext[1] == 'i' || yytext[1] == 'I' ) 
	return TOK_SIG; 
      else
	return TOK_SORT;
    default: abort();
    }
  } else if( (yytext[0] == 'c' || yytext[0] == 'C') && 
	     (allowed_modes & AR_INPUT_MECV) ) {
    set_mode(AR_INPUT_MECV); 
    BEGIN(mecv_spec_lexer); 
    return TOK_CONST;
  } else if( (yytext[0] == 'd' || yytext[0] == 'd') && 
	     (allowed_modes & AR_INPUT_MECV) ) {
    set_mode(AR_INPUT_MECV); 
    BEGIN(mecv_spec_lexer); 
    return TOK_DOMAIN;
  } else {
    altarica_token_value.id_value = ccl_string_make_unique(yytext);
    return TOK_IDENTIFIER;
  }
}

<INITIAL>{BEGIN} { 
  if( (allowed_modes & AR_INPUT_MECV) ) {
    set_mode(AR_INPUT_MECV); 
    BEGIN(mecv_spec_lexer); 
    return TOK_BEGIN; 
  } else {
    altarica_token_value.id_value = ccl_string_make_unique(yytext);
    return TOK_IDENTIFIER;
  }
}

<INITIAL>{WITH} { 
  if( (allowed_modes & AR_INPUT_ACHECK) ) {
    set_mode(AR_INPUT_ACHECK); 
    BEGIN(acheck_spec_lexer); 
    allow_ctlstar = 0;
    return TOK_WITH; 
  } else {
    altarica_token_value.id_value = ccl_string_make_unique(yytext);
    return TOK_IDENTIFIER;
  }
}

<INITIAL>{CTLSPEC} {
  if( (allowed_modes & AR_INPUT_CTLSTAR) ) {
    if ((MODE & AR_INPUT_CTLSTAR) == 0) {    
      set_mode(AR_INPUT_CTLSTAR); 
      allow_ctlstar = 1;
      BEGIN(ctlstar_spec_lexer); 
      return TOK_CTLSTARSPEC;
    } 
  } else {
    altarica_token_value.id_value = ccl_string_make_unique(yytext);
    return TOK_IDENTIFIER;
  } 
}


<INITIAL>{quoted_identifier} { 
  int i, len = strlen(yytext);
  for(i=1; i<len-1; i++) yytext[i-1] = yytext[i];
  yytext[i-1] = '\0';
  altarica_token_value.id_value = ccl_string_make_unique(yytext);
  if( (allowed_modes & AR_INPUT_MECV) ) {
    set_mode(AR_INPUT_MECV); 
    BEGIN(mecv_spec_lexer); 
  }

  return TOK_IDENTIFIER;
}

<INITIAL>{simple_identifier} { 
  if ((allowed_modes & AR_INPUT_ARC) != 0 &&
      s_is_arc_cmd != NULL &&
      s_is_arc_cmd (yytext, s_is_arc_cmd_data)) {
      altarica_token_value.string_value = ccl_string_dup (yytext);
    set_mode (AR_INPUT_ARC); 
    BEGIN (arc_command_lexer); 
    return TOK_STRING;
  }

  altarica_token_value.string_value = ccl_string_make_unique(yytext);
  if( (allowed_modes & AR_INPUT_MECV) ) {
    set_mode(AR_INPUT_MECV); 
    BEGIN(mecv_spec_lexer); 
  }

  return TOK_IDENTIFIER;
}

<INITIAL>{ARC_COMMAND_ID} { 
  altarica_token_value.string_value = ccl_string_dup(yytext);

  if ((allowed_modes & AR_INPUT_ARC) != 0 &&
      s_is_arc_cmd != NULL &&
      s_is_arc_cmd (yytext, s_is_arc_cmd_data)) {
    set_mode (AR_INPUT_ARC); 
    BEGIN (arc_command_lexer); 
  }

  return TOK_STRING;
}


<INITIAL>"@" {
  if( (allowed_modes & AR_INPUT_AEXPR) ) {
    if ((MODE & AR_INPUT_AEXPR) == 0) {    
      set_mode(AR_INPUT_AEXPR); 
      BEGIN(altarica_lexer); 
      return TOK_AEXPR_BEGIN;
    } 
  } 
  return '@';
}

<INITIAL>^:.*$ {
  altarica_token_value.string_value = ccl_string_dup (yytext);
  return TOK_MECV_COMMAND;
}

%{ 
  /* ALTARICA LEXER */ 
%}
<altarica_lexer>{CONST}          { return TOK_CONST; }
<altarica_lexer>{DOMAIN}         { return TOK_DOMAIN; }
<altarica_lexer>{BOOL}           { return TOK_BOOL; }
<altarica_lexer>{INTEGER}        { return TOK_INTEGER; }
<altarica_lexer>{NODE}           { return TOK_NODE; }
<altarica_lexer>{EDON}           { return TOK_EDON; }
<altarica_lexer>{FLOW}           { return TOK_FLOW; }
<altarica_lexer>{STATE}          { return TOK_STATE; }
<altarica_lexer>{EVENT}          { return TOK_EVENT; }
<altarica_lexer>{SUB}            { return TOK_SUB; }
<altarica_lexer>{TRANS}          { return TOK_TRANS; }
<altarica_lexer>{ASSERT}         { return TOK_ASSERT; }
<altarica_lexer>{SYNC}           { return TOK_SYNC; }
<altarica_lexer>{INIT}           { return TOK_INIT; }
<altarica_lexer>{STRUCT}         { return TOK_STRUCT; }
<altarica_lexer>{TCURTS}         { return TOK_TCURTS; }
<altarica_lexer>{SIG}            { return TOK_SIG; }
<altarica_lexer>{PARAM}          { return TOK_PARAM; }
<altarica_lexer>{PARAM_SET}      { return TOK_PARAM_SET; }
<altarica_lexer>{SORT}           { return TOK_SORT; }
<altarica_lexer>{LOCAL}          { return TOK_LOCAL; }
<altarica_lexer>{GLOBAL}         { return TOK_GLOBAL; }		    
<altarica_lexer>{TERM}           { return TOK_TERM; }
<altarica_lexer>{EXTERN}         { return TOK_EXTERN; }
<altarica_lexer>"?"              { return TOK_QUESTION; }
<altarica_lexer>"|-"             { return TOK_SRC; }
<altarica_lexer>"->"             { return TOK_TGT; }
<altarica_lexer>"{"              { return TOK_LBRACE; }
<altarica_lexer>"}"              { return TOK_RBRACE; }
<altarica_lexer>"@"              {
    if ((MODE & AR_INPUT_AEXPR) != 0) {    
      return TOK_AEXPR_END;
    } 
    return '@';
}

<altarica_lexer>{float}          {
  const char *s;
  int isint = 1;
  for (s = yytext; *s && isint; s++)
    isint = isdigit (*s);
  if (isint)
    {
      altarica_token_value.int_value = atoi (yytext);
      return TOK_UINTEGER;
    }
  altarica_token_value.flt_value = atof (yytext);
  return TOK_FLOAT;  
 }		    

%{
  /* GENERIC RULES */
%}
"!="             { return TOK_NEQ; }

%{
 /* MECV LEXER */ 
%}
<mecv_spec_lexer>{STRUCT}  { return TOK_STRUCT; }
<mecv_spec_lexer>{TCURTS}  { return TOK_TCURTS; }
<mecv_spec_lexer>{BOOL}	   { return TOK_BOOL; }
<mecv_spec_lexer>{INTEGER} { return TOK_INTEGER; }
<mecv_spec_lexer>{BEGIN}   { return TOK_BEGIN; }
<mecv_spec_lexer>{DOMAIN}  { return TOK_DOMAIN; }
<mecv_spec_lexer>{END}	   { return TOK_END; }
<mecv_spec_lexer>{LOCAL}   { return TOK_LOCAL; }
<mecv_spec_lexer>"+="      { return TOK_LFP; }
<mecv_spec_lexer>"-="      { return TOK_GFP; }
<mecv_spec_lexer>"{"            { return TOK_LBRACE; }
<mecv_spec_lexer>"}"            { return TOK_RBRACE; }
<mecv_spec_lexer>"!"            { return TOK_BANG; }

%{ 
 /* ACHECK LEXER */ 
%}

<acheck_spec_lexer>{ASSERT}     { return TOK_ASSERT; }
<acheck_spec_lexer>{VALIDATE}   { return TOK_VALIDATE; }
<acheck_spec_lexer>{EVENTS}     { return TOK_EVENTS; }
<acheck_spec_lexer>{WITH}     { return TOK_WITH; }
<acheck_spec_lexer>{SYMBOLICALLY}     { return TOK_SYMBOLICALLY; }
<acheck_spec_lexer>{EXHAUSTIVELY}     { return TOK_EXHAUSTIVELY; }
<acheck_spec_lexer>{DO}       { return TOK_DO; }
<acheck_spec_lexer>{DONE}     { return TOK_DONE; }
<acheck_spec_lexer>{DOT}      { return TOK_DOT; }
<acheck_spec_lexer>{GML}      { return TOK_GML; }
<acheck_spec_lexer>{SRC}      { return TOK_SRC; }
<acheck_spec_lexer>{TGT}      { return TOK_TGT; }
<acheck_spec_lexer>{WTS}      { return TOK_WTS; }
<acheck_spec_lexer>{SHOW}     { return TOK_SHOW; }
<acheck_spec_lexer>{RSRC}     { return TOK_RSRC; }
<acheck_spec_lexer>{RTGT}     { return TOK_RTGT; }
<acheck_spec_lexer>{LOOP}     { return TOK_LOOP; }
<acheck_spec_lexer>{TRACE}    { return TOK_TRACE; }
<acheck_spec_lexer>{DOTTRACE}    { return TOK_DOTTRACE; }
<acheck_spec_lexer>{REACH}    { return TOK_REACH; }
<acheck_spec_lexer>{TEST}     { return TOK_TEST; }
<acheck_spec_lexer>{NRTEST}   { return TOK_NRTEST; }
<acheck_spec_lexer>{COREACH}  { return TOK_COREACH; }
<acheck_spec_lexer>{LABEL}    { return TOK_LABEL; }
<acheck_spec_lexer>{ATTRIBUTE}    { return TOK_ATTRIBUTE; }
<acheck_spec_lexer>{UNAV}     { return TOK_UNAV; }
<acheck_spec_lexer>{PROJECT}  { return TOK_PROJECT; }
<acheck_spec_lexer>{PICK}     { return TOK_PICK; }
<acheck_spec_lexer>{PROJ_S}   { return TOK_PROJ_S; }
<acheck_spec_lexer>{PROJ_F}   { return TOK_PROJ_F; }
<acheck_spec_lexer>{QUOT}     { return TOK_QUOT; }
<acheck_spec_lexer>{MODES}     { return TOK_MODES; }
<acheck_spec_lexer>{DISPLAY}    { return TOK_DISPLAY; }
<acheck_spec_lexer>{REMOVE}    { return TOK_REMOVE; }
<acheck_spec_lexer>">>"       { return TOK_RANGLE_RANGLE; }
<acheck_spec_lexer>"+="       { return TOK_LFP; }
<acheck_spec_lexer>"-="       { return TOK_GFP; }
<acheck_spec_lexer>{CTLSPEC}  { BEGIN(ctlstar_spec_lexer); 
                                allow_ctlstar = 1;
                                return TOK_CTLSTARSPEC; }
<acheck_spec_lexer>{CTL2MU}  { BEGIN(ctlstar_spec_lexer); 
                               allow_ctlstar = 1;
                               return TOK_CTL2MU; }

<ctlstar_spec_lexer>"<=>" { return TOK_EQUIV; }
<ctlstar_spec_lexer>{XOR} { return TOK_XOR; }
<ctlstar_spec_lexer>"X" { return TOK_X; }
<ctlstar_spec_lexer>"F" { return TOK_F; }
<ctlstar_spec_lexer>"G" { return TOK_G; }
<ctlstar_spec_lexer>"AX" { return TOK_AX; }
<ctlstar_spec_lexer>"AF" { return TOK_AF; }
<ctlstar_spec_lexer>"AG" { return TOK_AG; }
<ctlstar_spec_lexer>"EX" { return TOK_EX; }
<ctlstar_spec_lexer>"EF" { return TOK_EF; }
<ctlstar_spec_lexer>"EG" { return TOK_EG; }
<ctlstar_spec_lexer>"A" { return TOK_A; }
<ctlstar_spec_lexer>"E" { return TOK_E; }
<ctlstar_spec_lexer>"U" { return TOK_U; }
<ctlstar_spec_lexer>"W" { return TOK_W; }

<ctlstar_spec_lexer>{SRC}      { return TOK_SRC; }
<ctlstar_spec_lexer>{TGT}      { return TOK_TGT; }
<ctlstar_spec_lexer>{RSRC}     { return TOK_RSRC; }
<ctlstar_spec_lexer>{RTGT}     { return TOK_RTGT; }
<ctlstar_spec_lexer>{LOOP}     { return TOK_LOOP; }
<ctlstar_spec_lexer>{TRACE}    { return TOK_TRACE; }
<ctlstar_spec_lexer>{REACH}    { return TOK_REACH; }
<ctlstar_spec_lexer>{COREACH}  { return TOK_COREACH; }
<ctlstar_spec_lexer>{LABEL}     { return TOK_LABEL; }
<ctlstar_spec_lexer>{ATTRIBUTE}    { return TOK_ATTRIBUTE; }
<ctlstar_spec_lexer>{UNAV}      { return TOK_UNAV; }
<ctlstar_spec_lexer>";"         { BEGIN(acheck_spec_lexer); 
                                  allow_ctlstar = 0;
                                  return TOK_SEMICOLON; ; }

%{
 /* ARC COMMANDS */
%}
<arc_command_lexer>":="         { return TOK_ASSIGN; }
<arc_command_lexer>">>"         { return TOK_SUPSUP; }
<arc_command_lexer>">"          { return TOK_SUP; }
<arc_command_lexer>"|"          { return TOK_PIPE; }
<arc_command_lexer>"("          { return TOK_LPAR; }
<arc_command_lexer>")"          { return TOK_RPAR; }
<arc_command_lexer>";"          { return TOK_SEMICOLON; }

<arc_command_lexer>{ARC_NUMBER}     { 
  double value = atof (yytext); 

  if (value > INT_MAX)
    {
       altarica_token_value.flt_value = value;
       return TOK_FLOAT; 
    } 
  else
    {
       altarica_token_value.int_value = atoi (yytext); 
       return TOK_UINTEGER; 
    }
}

<arc_command_lexer>{ARC_STRING}     { 
  altarica_token_value.string_value = ccl_string_dup (yytext); 
  return TOK_STRING; 
}

<arc_command_lexer>{ARC_QUOTED_STRING} { 
  int i; 
  for(i = 0; i<yyleng-2; i++)
    yytext[i] = yytext[i+1];
  yytext[i] = '\0';
  altarica_token_value.string_value = ccl_string_dup (yytext); 
  return TOK_STRING; 
}

<arc_command_lexer>{newline} { 
 altarica_current_line++; 
 return TOK_NEWLINE;
}

<arc_command_lexer><<EOF>> { 
 return TOK_NEWLINE;
}

%{ 
 /* GENERIC RULES */ 
%}

{OR}             { return TOK_OR; }
{AND}            { return TOK_AND; }
{IMPLY}          { return TOK_IMPLY; }
{TRUE}           { return TOK_TRUE; }
{FALSE}          { return TOK_FALSE; }
{NOT}            { return TOK_NOT; }
{IF}             { return TOK_IF; }
{THEN}           { return TOK_THEN; }
{ELSE}           { return TOK_ELSE; }
{CASE}           { return TOK_CASE; }
{MIN}            { return TOK_MIN; }
{MAX}            { return TOK_MAX; }
{MOD}            { return TOK_MOD; }

"<="             { return TOK_LEQ; }
">="             { return TOK_GEQ; }
"|"              { return TOK_OR; }
"&"              { return TOK_AND; }
"=>"             { return TOK_IMPLY; }
":="             { return TOK_ASSIGN; }
";"              { return TOK_SEMICOLON; }
"="              { return TOK_EQ; }
":"              { return TOK_COLON; }
","              { return TOK_COMMA; }
"("              { return TOK_LPAR; }
")"              { return TOK_RPAR; }
"["              { return TOK_LBRACKET; }
"]"              { return TOK_RBRACKET; }
"."              { return TOK_DOT; }
"<"              { return TOK_LANGLE; }
">"              { return TOK_RANGLE; }
"+"              { return TOK_PLUS; }
"-"              { return TOK_MINUS; }
"*"              { return TOK_STAR; }
"/"              { return TOK_SLASH; }
"~"              { return TOK_TILDE; }

{unsigned_integer}  { 
  altarica_token_value.int_value = atoi(yytext);
  return TOK_UINTEGER; 
}

{simple_identifier} { 
  altarica_token_value.id_value = ccl_string_make_unique(yytext);
  return TOK_IDENTIFIER;
}

{quoted_identifier} {
  int i, len = strlen(yytext);
  for(i=1; i<len-1; i++) yytext[i-1] = yytext[i];
  yytext[i-1] = '\0';
  altarica_token_value.id_value = ccl_string_make_unique(yytext);
  return TOK_IDENTIFIER;
}

{blank_character} { ((void)0); }

<c_comment>[^*\n]*      { /* empty */ }
<c_comment>"*"+[^*/\n]* { /* empty */ }
<c_comment>\n           { altarica_current_line++; }
<c_comment>"*"+"/"      {  
                          if( MODE == AR_INPUT_ALTARICA )
                            {
			      BEGIN(altarica_lexer);
                            }
                          else if( MODE == AR_INPUT_MECV )
                            {
			      BEGIN(mecv_spec_lexer);
                            }
                          else if( MODE == AR_INPUT_ACHECK )
                            {
			      if (allow_ctlstar)
    			         BEGIN(ctlstar_spec_lexer);
			      else
    			         BEGIN(acheck_spec_lexer);
                            }
			  else
                            {
			      BEGIN(INITIAL);
                            }
                        }

<c_comment><<EOF>>      { return TOK_ERROR; }

{newline} { altarica_current_line++; }

[^/]               { return (int)yytext[0]; }

%%

void
altarica_init_lexer(FILE *stream, const char *input_name, int modes, 
		    int (*is_cmd) (const char *id, void *data),
		    void *data)
{
  altarica_in = stream;
  altarica_current_line = 1;
  s_change_current_filename (input_name);
  altarica_input_clear_mode();
  allowed_modes = modes;
  yyrestart(stream);
  s_is_arc_cmd = is_cmd;
  s_is_arc_cmd_data = data;
}

			/* --------------- */

void
altarica_init_lexer_with_string (const char *input, const char *input_name, 
				 int modes, 
				 int (*is_cmd) (const char *id, void *data),
				 void *data)
{
  void *buffer = yy_scan_string(input);

  altarica_in = NULL;
  altarica_current_line = 1;
  s_change_current_filename (input_name);
  altarica_input_clear_mode();
  allowed_modes = modes;
  yy_switch_to_buffer(buffer);
  s_is_arc_cmd = is_cmd;
  s_is_arc_cmd_data = data;
}

			/* --------------- */

void
altarica_terminate_lexer(void)
{
  altarica_lex_destroy ();
  /*if (YY_CURRENT_BUFFER != NULL)
     yy_delete_buffer(YY_CURRENT_BUFFER); */

  altarica_in = NULL;
  altarica_current_line = -1;
  s_change_current_filename (NULL);
  s_is_arc_cmd = NULL;
  s_is_arc_cmd_data = NULL;
}

			/* --------------- */

int
altarica_input_get_mode(void)
{
  return MODE;
}

			/* --------------- */

void
altarica_input_clear_mode(void)
{
  set_mode(AR_INPUT_NONE);
  BEGIN(INITIAL);
}

			/* --------------- */
static void
s_change_current_filename (const char *f)
{
  if (f != NULL)
    altarica_current_filename = ccl_string_make_unique (f);
  else 
    altarica_current_filename = NULL;
}



void
altarica_input_set_mode__ (int mode, const char *smode, const char *file, 
			       int line)
{
  ccl_assert( (allowed_modes & mode) == mode );
  if(0)
    ccl_display("%s:%d:set mode = %s\n",file,line,smode);
  MODE = mode;
}
