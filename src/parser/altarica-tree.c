/*
 * altarica-tree.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-identifier.h"
#include "altarica-tree-p.h"

			/* --------------- */

void
ar_tree_log (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{  
  for (; t; t = t->next)
    {
      const altarica_tree *body = t->child;
      switch (t->node_type)
	{
	case AR_TREE_ALTARICA: ar_tree_log_altarica (log, body, it); break;
	case AR_TREE_MECV: ar_tree_log_mecv (log, body, it); break;
	case AR_TREE_ACHECK: ar_tree_log_acheck (log, body, it); break;
	case AR_TREE_AEXPR: ar_tree_log_acheck_expr (log, body, it); break;
	case AR_TREE_ARC: ar_tree_log_arc (log, body, it); break;
	default: INVALID_NODE_TYPE (t); break;
	}
      ccl_log (log, "\n");
    }
}

			/* --------------- */

void
s_id_list (ccl_log_type log, const altarica_tree *t, 
	   ar_identifier_translator *it, ar_identifier_context ctx)
{
  ccl_pre (IS_LABELLED_BY (t, ID_LIST));

  t = t->child;
  TRANSLATE (ctx, t);
  for (t = t->next; t != NULL; t = t->next)
    {
      ccl_log (log, ", ");
      TRANSLATE (ctx, t);
    }
}

			/* --------------- */

static void
s_structure (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{
  ccl_log (log, "struct ");
  for (t = t->child; t != NULL; t = t->next)
    {
      altarica_tree *id_list = t->child;
      altarica_tree *domain = id_list->next;

      s_id_list (log, id_list, it, AR_IC_IDENTIFIER);
      ccl_log (log, " : ");
      ar_tree_log_domain (log, domain, it);
      if (t->next != NULL)
	ccl_log (log, "; ");
    }
  ccl_log (log, " tcurts");
}

			/* --------------- */

static void
s_display_array_dims (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it)
{
  if (t->next != NULL)
    s_display_array_dims (log, t->next, it);
  ccl_log (log, "[");
  ar_tree_log_expr (log, t, it);
  ccl_log (log, "]");
}

			/* --------------- */

void
ar_tree_log_domain (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it)
{
  switch (t->node_type)
    {
    case AR_TREE_IDENTIFIER:
      TRANSLATE (AR_IC_IDENTIFIER, t);
      break;

    case AR_TREE_RANGE:
      ccl_log (log, "[");
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, ",");
      ar_tree_log_expr (log, t->child->next, it);
      ccl_log (log, "]");
      break;

    case AR_TREE_SYMBOL_SET:
      ccl_log (log, "{");
      for (t = t->child; t != NULL; t = t->next)
	{
	  TRANSLATE (AR_IC_IDENTIFIER, t);
	  if (t->next != NULL)
	    ccl_log (log, ", ");
	}
      ccl_log (log, "}");
      break;

    case AR_TREE_BOOLEANS:
      ccl_log (log, "bool");
      break;

    case AR_TREE_INTEGERS:
      ccl_log (log, "integer");
      break;

    case AR_TREE_STRUCTURE:
      s_structure (log, t, it);
      break;

    case AR_TREE_ARRAY_DOMAIN:
      ar_tree_log_domain (log, t->child, it);
      s_display_array_dims (log, t->child->next, it);
      break;

    default:
      INVALID_NODE_TYPE (t);
      break;
    };
}

			/* --------------- */

static void
s_constant_decl (ccl_log_type log, const altarica_tree *t, 
		 ar_identifier_translator *it)
{
  altarica_tree *ident = t->child;
  altarica_tree *domain = NULL;
  altarica_tree *value = NULL;

  if (t->value.int_value)
    {
      domain = ident->next;
      value = domain->next;
    }
  else
    {
      value = ident->next;
    }

  ccl_log (log, "const ");
  TRANSLATE (AR_IC_IDENTIFIER, ident);

  if (domain != NULL)
    {
      ccl_log (log, " : ");
      ar_tree_log_domain (log, domain, it);
    }

  if (value != NULL)
    {
      ccl_log (log, " = ");
      ar_tree_log_expr (log, value, it);
    }
  ccl_log (log, ";\n");
}

			/* --------------- */


static void
s_domain_decl (ccl_log_type log, const altarica_tree *t, 
	       ar_identifier_translator *it)
{
  altarica_tree *ident = t->child;
  altarica_tree *dom = ident->next;

  ccl_pre (IS_LABELLED_BY (t, DOMAIN));
  ccl_log (log, "domain ");
  TRANSLATE (AR_IC_IDENTIFIER, ident);
  ccl_log (log, " = ");
  ar_tree_log_domain (log, dom, it);
  ccl_log (log, ";\n");
}

			/* --------------- */

static void
s_attributes (ccl_log_type log, const altarica_tree *t, 
	      ar_identifier_translator *it, ar_identifier_context ctx)
{
  if (t->child == NULL)
    return;

  ccl_log (log, " : ");
  s_id_list (log, t->child, it, ctx);
}

			/* --------------- */


static void
s_node_parameters (ccl_log_type log, const altarica_tree *t, 
		   ar_identifier_translator *it)
{
  altarica_tree *param = t->child;

  ccl_log (log, "  param\n");

  while (param != NULL)
    {
      altarica_tree *id_list = param->child;
      altarica_tree *domain = id_list->next;

      ccl_log (log, "   ");
      s_id_list (log, id_list, it, AR_IC_IDENTIFIER);
      ccl_log (log, " : ");
      ar_tree_log_domain (log, domain, it);
      if ((param = param->next) != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_node_variables (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it)
{
  ar_identifier_context ctx;
  altarica_tree *var_class = t->child;
  altarica_tree *variables = var_class->next;

  if (IS_LABELLED_BY (var_class, FLOW))
    {
      ccl_log (log, "  flow\n");
      ctx = AR_IC_FLOW_VAR_ATTR;
    }
  else
    {
      ccl_log (log, "  state\n");
      ctx = AR_IC_STATE_VAR_ATTR;
    }

  while (variables != NULL)
    {

      altarica_tree *id_list = variables->child;
      altarica_tree *domain = id_list->next;
      altarica_tree *attr = domain->next;

      ccl_log (log, "   ");
      s_id_list (log, id_list, it, AR_IC_IDENTIFIER);
      ccl_log (log, " : ");
      ar_tree_log_domain (log, domain, it);
      s_attributes (log, attr, it, ctx);
      if ((variables = variables->next) != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void 
s_event_dag_list (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it);

static void
s_event_dag (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{
  if (IS_LABELLED_BY (t, EVENT_DAG_LIST))
    {
      ccl_log (log, "{");
      s_event_dag_list (log, t, it);
      ccl_log (log, "}");
    }
  else if (IS_LABELLED_BY (t, IDENTIFIER))
    {
      TRANSLATE (AR_IC_IDENTIFIER, t);
    }
  else if (IS_LABELLED_BY (t, ELEMENT_IN_ARRAY))
    {
      altarica_tree *id = t->child;
      altarica_tree *pos = id->next;

      TRANSLATE (AR_IC_IDENTIFIER, id);
      while (pos != NULL)
	{
	  ccl_log (log, "[");
	  ar_tree_log_expr (log, pos, it);
	  ccl_log (log, "]");
	  pos = pos->next;
	}
    }
  else if (IS_LABELLED_BY (t, EVENT_LT))
    {
      s_event_dag (log, t->child, it);
      ccl_log (log, " < ");
      s_event_dag (log, t->child->next, it);
    }
  else
    {
      ccl_assert (IS_LABELLED_BY (t, EVENT_GT));

      s_event_dag (log, t->child, it);
      ccl_log (log, " > ");
      s_event_dag (log, t->child->next, it);
    }
}

			/* --------------- */

static void
s_event_dag_list (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it)
{
  altarica_tree *dag;

  ccl_pre (IS_LABELLED_BY (t, EVENT_DAG_LIST));

  for (dag = t->child; dag != NULL; dag = dag->next)
    {
      s_event_dag (log, dag, it);
      if (dag->next != NULL)
	ccl_log (log, ", ");
    }
}

			/* --------------- */


static void
s_node_events (ccl_log_type log, const altarica_tree *t, 
	       ar_identifier_translator *it)
{
  ccl_log (log, "  event\n");

  for (t = t->child; t != NULL; t = t->next)
    {
      altarica_tree *dag_list;
      altarica_tree *attr;

      ccl_assert (IS_LABELLED_BY (t, EVENT_POSET));

      dag_list = t->child;
      attr = dag_list->next;

      ccl_log (log, "   ");
      s_event_dag_list (log, dag_list, it);
      s_attributes (log, attr, it, AR_IC_EVENT_ATTR);
      if (t->next != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_subnode_type (ccl_log_type log, const altarica_tree *t, 
		ar_identifier_translator *it)
{
  if (IS_LABELLED_BY (t,SUBNODE_ARRAY))
    {
      s_subnode_type (log, t->child, it);
      ccl_log (log, "[");
      ar_tree_log_expr (log, t->child->next, it);
      ccl_log (log, "]");
    }
  else
    {
      ccl_assert (IS_LABELLED_BY (t, IDENTIFIER));
      TRANSLATE (AR_IC_IDENTIFIER, t);
    }
}

			/* --------------- */

static void
s_node_subnodes (ccl_log_type log, const altarica_tree *t, 
		 ar_identifier_translator *it)
{
  ccl_log (log, "  sub\n");
  for (t = t->child; t != NULL; t = t->next)
    {
      altarica_tree *id_list = t->child;
      altarica_tree *node_type = id_list->next;

      ccl_assert (IS_LABELLED_BY (t, SUBNODES));

      ccl_log (log, "   ");
      s_id_list (log, id_list, it, AR_IC_IDENTIFIER);
      ccl_log (log, " : ");
      s_subnode_type (log, node_type, it);
      if (t->next != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_node_assertions (ccl_log_type log, const altarica_tree *t, 
		   ar_identifier_translator *it)
{
  ccl_log (log, "  assert\n");
  for (t = t->child; t != NULL; t = t->next)
    {
      ccl_log (log, "   ");
      ar_tree_log_expr (log, t, it);
      if (t->next != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_struct_expr (ccl_log_type log, const altarica_tree *t, 
	       ar_identifier_translator *it)
{
  ccl_log (log, "{ ");
  for (t = t->child; t; t = t->next)
    {
      altarica_tree *field_name = t->child;
      altarica_tree *field_value = field_name->next;

      ccl_log (log, ".");
      TRANSLATE (AR_IC_IDENTIFIER, field_name);
      ccl_log (log, " = ");
      ar_tree_log_expr (log, field_value, it);

      if (t->next != NULL)
	ccl_log (log, ", ");
    }
  ccl_log (log, " }");
}

			/* --------------- */

static void
s_array_expr (ccl_log_type log, const altarica_tree *t, 
	      ar_identifier_translator *it)
{
  ccl_log (log, "{ ");
  for (t = t->child; t; t = t->next)
    {
      ar_tree_log_expr (log, t, it);
      if (t->next != NULL)
	ccl_log (log, ", ");
    }
  ccl_log (log, " }");
}

			/* --------------- */

static void
s_member_access (ccl_log_type log, const altarica_tree *t, 
		 ar_identifier_translator *it)
{
  switch (t->node_type)
    {
    case AR_TREE_IDENTIFIER:
      TRANSLATE (AR_IC_IDENTIFIER, t);
      break;

    case AR_TREE_STRUCT_MEMBER:
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, ".");
      if (t->child->next != NULL)
	TRANSLATE (AR_IC_IDENTIFIER, t->child->next);
      break;

    case AR_TREE_ARRAY_MEMBER:
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, "[");
      ar_tree_log_expr (log, t->child->next, it);
      ccl_log (log, "]");
      break;

    default:
      INVALID_NODE_TYPE (t);
      break;
    }
}

			/* --------------- */

static void
s_trans_label_list (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it)
{
  ccl_pre (IS_LABELLED_BY (t, TRANS_LABEL_LIST));

  for (t = t->child; t; t = t->next)
    {
      if (IS_LABELLED_BY (t, IDENTIFIER))
	TRANSLATE (AR_IC_IDENTIFIER, t);
      else
	{
	  altarica_tree *id;
	  altarica_tree *pos;

	  ccl_assert (IS_LABELLED_BY (t, ELEMENT_IN_ARRAY));

	  id = t->child;
	  TRANSLATE (AR_IC_IDENTIFIER, id);
	  pos = id->next;

	  while (pos != NULL)
	    {
	      ccl_log (log, "[");
	      ar_tree_log_expr (log, pos, it);
	      ccl_log (log, "]");
	      pos = pos->next;
	    }
	}

      if (t->next != NULL)
	ccl_log (log, ", ");
    }
}

			/* --------------- */

static void
s_transition (ccl_log_type log, const altarica_tree *t, 
	      ar_identifier_translator *it)
{
  altarica_tree *guard = t->child;
  altarica_tree *tgt = guard->next;

  ar_tree_log_expr (log, t->child, it);
  ccl_log (log, "\n");
  while (tgt != NULL)
    {
      altarica_tree *label_list = tgt->child;
      altarica_tree *assign = label_list->next;

      ccl_log (log, "    |- ");
      s_trans_label_list (log, label_list, it);
      ccl_log (log, " -> ");

      while (assign != NULL)
	{
	  s_member_access (log, assign->child, it);
	  ccl_log (log, " := ");
	  ar_tree_log_expr (log, assign->child->next, it);

	  if ((assign = assign->next) != NULL)
	    ccl_log (log, ", ");
	}

      if ((tgt = tgt->next) != NULL)
	ccl_log (log, "\n");
    }
}

			/* --------------- */

static void
s_node_transitions (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it)
{
  ccl_log (log, "  trans\n");
  for (t = t->child; t != NULL; t = t->next)
    {
      ccl_log (log, "   ");
      s_transition (log, t, it);
      if (t->next != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_sync_vector (ccl_log_type log, const altarica_tree *t, 
	       ar_identifier_translator *it)
{
  altarica_tree *vec = t->child;
  altarica_tree *events = vec->child;
  altarica_tree *constraint = vec->next;
  altarica_tree *kind = constraint->next;

  ccl_log (log, "<");
  for (; events != NULL; events = events->next)
    {
      ar_tree_log_identifier_path (log, events->child, it);
      if (events->value.int_value)
	ccl_log (log, "?");
      if (events->next != NULL)
	ccl_log (log, ", ");
    }
  ccl_log (log, ">");

  switch (constraint->node_type)
    {
    case AR_TREE_SYNC_CONSTRAINT_LT: ccl_log (log, " < "); break;
    case AR_TREE_SYNC_CONSTRAINT_LEQ: ccl_log (log, " <= "); break;
    case AR_TREE_SYNC_CONSTRAINT_GT: ccl_log (log, " > "); break;
    case AR_TREE_SYNC_CONSTRAINT_GEQ: ccl_log (log, " >= "); break;
    case AR_TREE_SYNC_CONSTRAINT_EQ: ccl_log (log, " = "); break;
    case AR_TREE_SYNC_CONSTRAINT_NONE: break;
    default: INVALID_NODE_TYPE (t); break;
    }

  if (constraint->child != NULL)
    ar_tree_log_expr (log, constraint->child, it);

  if (kind != NULL && IS_LABELLED_BY (kind, MIN))
    ccl_log (log, " min");
}

			/* --------------- */

static void
s_node_synchronization (ccl_log_type log, const altarica_tree *t, 
			ar_identifier_translator *it)
{
  ccl_log (log, "  sync\n");

  for (t = t->child; t != NULL; t = t->next)
    {
      ccl_log (log, "   ");
      s_sync_vector (log, t, it);
      if (t->next != NULL)
	ccl_log (log, ";\n");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_quantifier_expr (ccl_log_type log, const altarica_tree * t,
		   ar_identifier_translator *it)
{
  int exist = IS_LABELLED_BY (t, EXIST);
  const altarica_tree *vars = t->child->child;
  const altarica_tree *qF = t->child->next;

  ccl_log (log, exist ? "<" : "[");

  while (vars != NULL)
    {
      const altarica_tree *id_list = vars->child;
      const altarica_tree *domain = id_list->next;

      ccl_assert (IS_LABELLED_BY (vars, QUANTIFIED_VARIABLES));

      s_id_list (log, id_list, it, AR_IC_IDENTIFIER);
      ccl_log (log, " : ");
      if (IS_LABELLED_BY (domain, BANG_ID))
	ar_tree_log_bang_identifier (log, domain, it);
      else
	ar_tree_log_domain (log, domain, it);

      if (vars->next != NULL)
	ccl_log (log, "; ");

      vars = vars->next;
    }
  ccl_log (log, exist ? ">" : "]");
  ar_tree_log_expr (log, qF, it);
}

			/* --------------- */

void
ar_tree_log_expr (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it)
{
  const char *aux;

  switch (t->node_type)
    {
    case AR_TREE_ITE:
      ccl_log (log, "if ");
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, " then ");
      ar_tree_log_expr (log, t->child->next, it);
      ccl_log (log, " else ");
      ar_tree_log_expr (log, t->child->next->next, it);
      break;

    case AR_TREE_CASE:
      ccl_log (log, "case {\n");
      for (t = t->child; t->node_type != AR_TREE_CASE_DEFAULT; t = t->next)
	{
	  ccl_log (log, "  ");
	  ar_tree_log_expr (log, t->child, it);
	  ccl_log (log, " : ");
	  ar_tree_log_expr (log, t->child->next, it);
	  ccl_log (log, ",\n");
	}
      ccl_log (log, "  else ");
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, "\n}\n");
      break;

    case AR_TREE_OR:
    case AR_TREE_AND:
    case AR_TREE_EQ:
    case AR_TREE_NEQ:
    case AR_TREE_IMPLY:
    case AR_TREE_LT:
    case AR_TREE_GT:
    case AR_TREE_LEQ:
    case AR_TREE_GEQ:
    case AR_TREE_ADD:
    case AR_TREE_SUB:
    case AR_TREE_MUL:
    case AR_TREE_DIV:
    case AR_TREE_MOD:
      switch (t->node_type)
	{
	case AR_TREE_OR:
	  aux = "|";
	  break;
	case AR_TREE_AND:
	  aux = "&";
	  break;
	case AR_TREE_EQ:
	  aux = "=";
	  break;
	case AR_TREE_NEQ:
	  aux = "!=";
	  break;
	case AR_TREE_IMPLY:
	  aux = "=>";
	  break;
	case AR_TREE_LT:
	  aux = "<";
	  break;
	case AR_TREE_GT:
	  aux = ">";
	  break;
	case AR_TREE_LEQ:
	  aux = "<=";
	  break;
	case AR_TREE_GEQ:
	  aux = ">=";
	  break;
	case AR_TREE_ADD:
	  aux = "+";
	  break;
	case AR_TREE_SUB:
	  aux = "-";
	  break;
	case AR_TREE_MUL:
	  aux = "*";
	  break;
	case AR_TREE_DIV:
	  aux = "/";
	  break;
	case AR_TREE_MOD:
	  aux = "mod";
	  break;
	}
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, " %s ", aux);
      ar_tree_log_expr (log, t->child->next, it);
      break;

    case AR_TREE_NEG:
    case AR_TREE_NOT:
      if (t->node_type == AR_TREE_NEG)
	aux = "- ";
      else
	aux = "~";

      ccl_log (log, "%s", aux);
      ar_tree_log_expr (log, t->child, it);
      break;

    case AR_TREE_PARENTHEZED_EXPR:
      ccl_log (log, "(");
      ar_tree_log_expr (log, t->child, it);
      ccl_log (log, ")");
      break;

    case AR_TREE_IDENTIFIER:
    case AR_TREE_STRUCT_MEMBER:
    case AR_TREE_ARRAY_MEMBER:
      s_member_access (log, t, it);
      break;

    case AR_TREE_INTEGER:
      ccl_log (log, "%d", t->value.int_value);
      break;

    case AR_TREE_TRUE:
      ccl_log (log, "true");
      break;

    case AR_TREE_FALSE:
      ccl_log (log, "false");
      break;

    case AR_TREE_FUNCTION_CALL:
    case AR_TREE_MIN:
    case AR_TREE_MAX:
      if (t->node_type == AR_TREE_MIN)
	{
	  ccl_log (log, "min");
	  t = t->child;
	}
      else if (t->node_type == AR_TREE_MAX)
	{
	  ccl_log (log, "max");
	  t = t->child;
	}
      else 
	{
	  if (IS_LABELLED_BY (t->child, BANG_ID))
	    ar_tree_log_bang_identifier (log, t->child, it);
	  else
	    TRANSLATE (AR_IC_IDENTIFIER, t->child);
	  t = t->child->next;
	}

      ccl_log (log, " (");
      for (; t; t = t->next)
	{
	  ar_tree_log_expr (log, t, it);
	  if (t->next != NULL)
	    ccl_log (log, ", ");
	}
      ccl_log (log, ")");
      break;

    case AR_TREE_EXIST:
    case AR_TREE_FORALL:
      s_quantifier_expr (log, t, it);
      break;

    case AR_TREE_STRUCT_EXPR:
      s_struct_expr (log, t, it);
      break;

    case AR_TREE_ARRAY_EXPR:
      s_array_expr (log, t, it);
      break;

    default:
      INVALID_NODE_TYPE (t);
      break;
    }
}

			/* --------------- */

void
ar_tree_log_identifier_path (ccl_log_type log, const altarica_tree *t, 
			     ar_identifier_translator *it)
{
  for (t = t->child; t != NULL; t = t->next)
    {
      if (IS_LABELLED_BY (t, IDENTIFIER))
	TRANSLATE (AR_IC_IDENTIFIER, t);
      else
	{
	  altarica_tree *id = t->child;
	  altarica_tree *pos = id->next;

	  ccl_assert (IS_LABELLED_BY (t, ELEMENT_IN_ARRAY));

	  TRANSLATE (AR_IC_IDENTIFIER, id);
	  for (; pos != NULL; pos = pos->next)
	    {
	      ccl_display ("[");
	      ar_tree_log_expr (log, pos, it);
	      ccl_display ("]");
	    }
	}
      if (t->next != NULL)
	ccl_display (".");
    }
}

			/* --------------- */


static void
s_node_init (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{
  altarica_tree *assign = t->child;

  ccl_log (log, "  init\n");
  while (assign != NULL)
    {
      ccl_log (log, "   ");
      if (IS_LABELLED_BY (assign, ASSIGNMENT))
	{
	  s_member_access (log, assign->child, it);
	  ccl_log (log, " := ");
	  ar_tree_log_expr (log, assign->child->next, it);
	}
      else
	{
	  ar_tree_log_expr (log, assign, it);
	}
      if ((assign = assign->next) != NULL)
	ccl_log (log, ",\n");
    }

  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_node_param_set (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it)
{
  altarica_tree *assign = t->child;

  ccl_log (log, "  param_set\n");
  while (assign != NULL)
    {
      ccl_log (log, "   ");
      s_member_access (log, assign->child, it);
      ccl_log (log, " := ");
      ar_tree_log_expr (log, assign->child->next, it);

      if ((assign = assign->next) != NULL)
	ccl_log (log, ",\n");
    }

  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_extern_term (ccl_log_type log, const altarica_tree *t, 
	       ar_identifier_translator *it)
{
  const char *kind = NULL;
  switch (t->node_type)
    {
    case AR_TREE_TRUE:
      ccl_log (log, "true");
      break;
    case AR_TREE_FALSE:
      ccl_log (log, "false");
      break;
    case AR_TREE_INTEGER:
      ccl_log (log, "%d", t->value.int_value);
      break;
    case AR_TREE_FLOAT:
      ccl_log (log, "%e", t->value.flt_value);
      break;
    case AR_TREE_STRING:
      ccl_log (log, "\"%s\"", t->value.string_value);
      break; 
    case AR_TREE_IDENTIFIER:
      TRANSLATE (AR_IC_IDENTIFIER, t);
      break; 
    case AR_TREE_IDENTIFIER_PATH:
      ar_tree_log_identifier_path (log, t, it);
      break; 
    case AR_TREE_EXTERN_TERM_FUNCTION:
      TRANSLATE (AR_IC_EXTERN_FUNCTION, t->child);
      ccl_log (log, "(");
      {
	const altarica_tree *ta = t->child->next;
	s_extern_term (log, ta, it);
	for (ta = ta->next; ta; ta = ta->next)
	  {
	    ccl_log (log, ", ");
	    s_extern_term (log, ta, it);
	  }
      }
      ccl_log (log, ")");
      break;
    case AR_TREE_EXTERN_TERM_SET:
      ccl_log (log, "{");
      {
	const altarica_tree *ta = t->child;
	s_extern_term (log, ta, it);
	for (ta = ta->next; ta; ta = ta->next)
	  {
	    ccl_log (log, ", ");
	    s_extern_term (log, ta, it);
	  }
      }
      ccl_log (log, "}");
      break;
    case AR_TREE_EXTERN_TERM_FLOW: kind = "flow"; goto output;
    case AR_TREE_EXTERN_TERM_STATE: kind = "state"; goto output;
    case AR_TREE_EXTERN_TERM_EVENT: kind = "event"; goto output;
    case AR_TREE_EXTERN_TERM_SUB: kind = "sub"; goto output;
    case AR_TREE_EXTERN_TERM_LOCAL: kind = "local"; goto output;
    case AR_TREE_EXTERN_TERM_GLOBAL:
      kind = "global"; 
    output:
      ccl_assert (kind != NULL);
      ccl_log (log, "<%s ", kind);
      ar_tree_log_identifier_path (log, t->child, it);
      ccl_log (log, ">");
      break;
    case AR_TREE_EXTERN_TERM_EXPR:
      ccl_log (log, "<term (");
      ar_tree_log_expr (log, t->child, it); 
      ccl_log (log, ")>");
      break;
    default:
      ccl_unreachable ();
      break;
    };
}

static void
s_extern_directive (ccl_log_type log, const altarica_tree *t, 
		    ar_identifier_translator *it, int toplevel)
{
  const char *prefix = toplevel ? "" : "  ";
  altarica_tree *e = t->child;
  
  ccl_pre (IS_LABELLED_BY (t, EXTERN_DECL));  
  ccl_log (log, "%sextern\n", prefix);
  for (e = t->child; e; e = e->next)
    {
      ccl_pre (IS_LABELLED_BY (e, EXTERN_DIRECTIVE));
      ccl_log (log, "%s  ", prefix);
      TRANSLATE (AR_IC_EXTERN_DECL, e->child);
      ccl_log (log, " ");
      s_extern_term (log, e->child->next, it);
      if (e->child->next->next != NULL)
	{
	  ccl_log (log, " = ");
	  s_extern_term (log, e->child->next->next, it);
	}
      ccl_log (log, "; \n");
    }
}

			/* --------------- */

static void
s_node_decl (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{
  altarica_tree *ident = t->child;
  altarica_tree *attr = ident->next;
  altarica_tree *fields = attr->next;

  ccl_log (log, "node ");
  TRANSLATE (AR_IC_IDENTIFIER, ident);
  s_attributes (log, attr, it, AR_IC_NODE_ATTR);
  ccl_log (log, "\n");

  for (; fields != NULL; fields = fields->next)
    {
    switch (fields->node_type)
      {
      case AR_TREE_PARAMETERS_DECL:
	s_node_parameters (log, fields, it);
	break;
      case AR_TREE_VARIABLES_DECL:
	s_node_variables (log, fields, it);
	break;
      case AR_TREE_EVENTS_DECL:
	s_node_events (log, fields, it);
	break;
      case AR_TREE_SUBNODES_DECL:
	s_node_subnodes (log, fields, it);
	break;
      case AR_TREE_ASSERTIONS_DEF:
	s_node_assertions (log, fields, it);
	break;
      case AR_TREE_TRANSITIONS_DEF:
	s_node_transitions (log, fields, it);
	break;
      case AR_TREE_SYNCHRONIZATION_DEF: 
	s_node_synchronization (log, fields, it);
	break;
      case AR_TREE_INIT_DECL:
	s_node_init (log, fields, it);
	break;
      case AR_TREE_PARAM_SET_DECL:
	s_node_param_set (log, fields, it);
	break;
      case AR_TREE_EXTERN_DECL:
	s_extern_directive (log, fields, it, 1);
	break;
      default:
	INVALID_NODE_TYPE (fields);
	break;
      }
    }
  ccl_log (log, "edon\n");
}

			/* --------------- */

static void
s_signature_decl (ccl_log_type log, const altarica_tree *t, 
		  ar_identifier_translator *it)
{
  altarica_tree *op_name = t->child;
  altarica_tree *dom = op_name->next;
  altarica_tree *img = dom->next;

  ccl_log (log, "sig ");
  TRANSLATE (AR_IC_IDENTIFIER, op_name);
  ccl_log (log, " : ");
  dom = dom->child;

  while (dom != NULL)
    {
      ar_tree_log_domain (log, dom, it);
      if (dom->next != NULL)
	ccl_log (log, " * ");
      dom = dom->next;
    }
  ccl_log (log, " -> ");
  ar_tree_log_domain (log, img, it);
  ccl_log (log, ";\n");
}

			/* --------------- */

static void
s_sort_decl (ccl_log_type log, const altarica_tree *t, 
	     ar_identifier_translator *it)
{
  altarica_tree *sort = t->child;

  ccl_pre (sort->node_type == AR_TREE_ID_LIST);
  sort = sort->child;

  ccl_log (log, "sort ");

  while (sort)
    {
      TRANSLATE (AR_IC_IDENTIFIER, sort);
      if (sort->next != NULL)
	ccl_log (log, ", ");
      sort = sort->next;
    }

  ccl_log (log, ";\n");
}

			/* --------------- */

void
ar_tree_log_altarica (ccl_log_type log, const altarica_tree *t, 
		      ar_identifier_translator *it)
{
  while (t != NULL)
    {
      switch (t->node_type)
	{
	case AR_TREE_CONSTANT: s_constant_decl (log, t, it); break;
	case AR_TREE_DOMAIN: s_domain_decl (log, t, it); break;
	case AR_TREE_NODE: s_node_decl (log, t, it); break;
	case AR_TREE_SORT: s_sort_decl (log, t, it); break;
	case AR_TREE_SIGNATURE: s_signature_decl (log, t, it); break;
	case AR_TREE_EXTERN_DECL: s_extern_directive (log, t, it, 1); break;
	default: INVALID_NODE_TYPE (t); break;
	}

      if ((t = t->next) != NULL)
	ccl_log (log, "\n");
    }
}


			/* --------------- */

static void
s_translate_as_is (ar_identifier_translator *it, ccl_log_type log,
		   ar_identifier_context ctx, const char *id)
{
  const char *fmt;
  if (ar_need_quotes (id))
    fmt = "'%s'";
  else
    fmt = "%s";
  ccl_log (log, fmt, id);
}

			/* --------------- */

static ar_identifier_translator TRANSLATE_AS_IS = {
  s_translate_as_is
};

ar_identifier_translator *AR_NO_ID_TRANSLATION = &TRANSLATE_AS_IS;

			/* --------------- */

void
ar_translate_identifier_tree (ar_identifier_translator *it, 
			      ar_identifier_context ctx, ccl_log_type log, 
			      const altarica_tree *t)
{
  ccl_pre (IS_LABELLED_BY (t, IDENTIFIER));
  ccl_pre (it != NULL);

  it->translate (it, log, ctx, t->value.id_value);
}

			/* --------------- */

