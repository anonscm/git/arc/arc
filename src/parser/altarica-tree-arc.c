/*
 * altarica-tree-arc.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "altarica-tree-p.h"

static void
s_command (ccl_log_type log, const altarica_tree *t, 
	   ar_identifier_translator *it)
{
  const altarica_tree *cmdname = t->child;
  const altarica_tree *p = cmdname->next;

  ccl_pre (IS_LABELLED_BY (t, SHELL_CMD));
	   
  ccl_log (log, "%s", cmdname->value.string_value);
  for (; p; p = p->next)
    {
      ccl_log (log, " ");
      if (p->node_type == AR_TREE_FLOAT)
	ccl_log (log, "%g", p->value.flt_value);
	else if (p->node_type == AR_TREE_INTEGER)
	  ccl_log (log, "%d", p->value.int_value);
	else 
	  {	    
	    char *bang;
	    char *str = p->value.string_value;

	    ccl_assert (IS_LABELLED_BY (p, STRING));

	    bang = strchr (str, '!');

	    if (bang != NULL)
	      {
		*bang = '\0';
		TRANSLATE_STR (AR_IC_IDENTIFIER, str);
		ccl_log (log, "!");
		TRANSLATE_STR (AR_IC_BANG_TYPE, bang + 1);
		*bang = '!';
	      }
	    else
	      {
		TRANSLATE_STR (AR_IC_ARC_STRING, str);
	      }
	  }
    }
}

			/* --------------- */

void
ar_tree_log_arc (ccl_log_type log, const altarica_tree *t, 
		 ar_identifier_translator *it)
{
  for (; t; t = t->next)
    {
      const altarica_tree *cmd;
      const altarica_tree *redir;

      if (IS_LABELLED_BY (t, SHELL_CMD))
	{
	  cmd = t;
	  redir = NULL;
	}
      else
	{
	  redir = t->child;
	  cmd = redir->next;
	}

      s_command (log, cmd, it);

      if (redir != NULL)
	{
	  ccl_log (log, " ");
	  if (IS_LABELLED_BY (t, SHELL_REDIR_PIPE))
	    ccl_log (log, "| \"%s\"", t->value.string_value);
	  else 
	    {
	      if (IS_LABELLED_BY (t, SHELL_REDIR_CREATE))
		ccl_log (log, "> \"");
	      else
		ccl_log (log, ">> \"");
	      TRANSLATE_STR (AR_IC_ARC_OUT_FILENAME, redir->value.string_value);
	      ccl_log (log, "\"");
	    }
	}
      ccl_log (log, " \n");
    }
}
