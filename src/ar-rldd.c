/*
 * ar-rldd.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-memory.h>
#include <ccl/ccl-list.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-assert.h>
#include "ar-rldd.h"

typedef struct ar_word_diagram_cache_record_st
{
  void *op;
  ar_rldd *n1;
  ar_rldd *n2;
  ar_rldd *r;
} ar_rldd_record;

			/* --------------- */

struct ar_word_diagram_st
{
  uint32_t references;
  uint32_t flag;
  void *label;
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *next;
  uint16_t min;
  uint16_t max;
};

			/* --------------- */

typedef struct ar_word_diagram_page_st ar_rldd_page;
struct ar_word_diagram_page_st
{
  ar_rldd *nodes;
  ar_rldd_page *next;
};

			/* --------------- */

typedef struct ar_word_diagram_pager_st ar_rldd_pager;
struct ar_word_diagram_pager_st
{
  ar_rldd_page *first;
  ar_rldd_page **last;
  ar_rldd *free_nodes;
  int page_size;
  int nb_pages;
  int nb_free_nodes;
};

			/* --------------- */

struct ar_word_diagram_manager_st
{
  ar_rldd **rldd_table;
  int rldd_actual_size;
  int rldd_min_size;
  int rldd_fill_degree;
  int rldd_nb_nodes;
  ar_rldd_record *op_table;
  int op_table_size;
  int nodes_to_release;
  ar_rldd *nil;
  ar_rldd *epsilon;
  ar_rldd_pager *pager;
  ar_rldd_log_label *label_log;
  ccl_duplicate_func *label_dup;
  ccl_delete_proc *label_delete;
  ccl_compare_func *label_compare;
};

			/* --------------- */

#define m_abs_hash(h) \
(((h)<0)?-(h):(h))

#define m_hash_operation(_op,_n1,_n2) \
 m_abs_hash(1741l*((uintptr_t)_op)+2777l*((uintptr_t)_n1)+727l*((uintptr_t)_n2))

#define m_hash_node(_l,_n1,_n2)					\
  m_abs_hash(1741l*((uintptr_t)_l)+2777l*((uintptr_t)_n1)+727l*((uintptr_t)_n2))

#define m_label_delete(_man,_l) \
do { if((_man)->label_delete!= NULL) (_man)->label_delete(_l); } while(0)

#define m_label_dup(_man,_l) \
((_man)->label_dup==NULL?(_l):(_man)->label_dup(_l))

#define m_label_compare(_man,_l1,_l2) \
((_man)->label_compare==NULL?((intptr_t)(_l1))-((intptr_t)(_l2)):(_man)->label_compare(_l1,_l2))

			/* --------------- */

static void
s_pager_create_page (ar_rldd_pager *pager)
{
  int i;
  ar_rldd *node;
  ar_rldd_page *new_page = ccl_new (ar_rldd_page);

  new_page->nodes = ccl_new_array (ar_rldd, pager->page_size);
  new_page->next = NULL;

  *(pager->last) = new_page;
  pager->last = &(new_page->next);

  for (i = 0, node = new_page->nodes; i < pager->page_size - 1; i++, node++)
    node->next = node + 1;
  pager->free_nodes = new_page->nodes;
  pager->nb_free_nodes += pager->page_size;
  pager->nb_pages++;
}

			/* --------------- */

static ar_rldd_pager *
s_pager_create (int page_size)
{
  ar_rldd_pager *result = ccl_new (ar_rldd_pager);

  result->first = NULL;
  result->last = &(result->first);
  result->free_nodes = NULL;
  result->page_size = page_size;
  result->nb_pages = 0;
  s_pager_create_page (result);

  return result;
}

			/* --------------- */

static void
s_update_reference_counter (ar_rldd *node)
{
  if (node->left && node->left->flag == 0 && --node->left->references == 0)
    s_update_reference_counter (node->left);
  if (node->right && node->right->flag == 0 && --node->right->references == 0)
    s_update_reference_counter (node->right);
  node->flag = 1;
}

#if 0
static void
s_collect_nodes (ar_rlddm *manager)
{
  int i;
  ar_rldd *node;
  ar_rldd **pnode;
  ar_rldd_page *page;
  ar_rldd_record *rec;
  ar_rldd_pager *pager = manager->pager;

  for (page = pager->first; page != NULL; page = page->next)
    for (i = 0, node = page->nodes; i < pager->page_size; i++, node++)
      node->flag = 0;

  for (page = pager->first; page != NULL; page = page->next)
    {
      for (i = 0, node = page->nodes; i < pager->page_size; i++, node++)
	{
	  if (node->references == 0 && node->flag == 0)
	    s_update_reference_counter (node);
	}
    }

  for (i = 0; i < manager->rldd_actual_size; i++)
    {
      for (pnode = manager->rldd_table + i; *pnode;)
	{
	  if ((*pnode)->references == 0)
	    {
	      node = *pnode;
	      *pnode = node->next;
	      node->next = pager->free_nodes;
	      pager->free_nodes = node;
	      m_label_delete (manager, node->label);
	      pager->nb_free_nodes++;
	      manager->rldd_nb_nodes--;
	    }
	  else
	    {
	      pnode = &((*pnode)->next);
	    }
	}
    }

  for (i = 0, rec = manager->op_table; i < manager->op_table_size; i++, rec++)
    {
      if (rec->n1 == NULL || rec->n1->references == 0 ||
	  rec->n2 == NULL || rec->n2->references == 0 ||
	  rec->r == NULL || rec->r->references == 0)
	{
	  rec->op = NULL;
	  rec->n1 = NULL;
	  rec->n2 = NULL;
	  rec->r = NULL;
	}
    }

  manager->nodes_to_release = 0;
}
#else
static void
s_collect_nodes (ar_rlddm *manager)
{
  int i;
  ar_rldd *node;
  ar_rldd **pnode;
  ar_rldd_page *page;
  ar_rldd_record *rec;
  ar_rldd_pager *pager = manager->pager;
  int nb_collected = 0;
  int empty_cache_entries = 0;

  for (i = 0, rec = manager->op_table; i < manager->op_table_size; i++, rec++)
    {
      if (rec->n1 == NULL || rec->n1->references == 0 ||
	  rec->n2 == NULL || rec->n2->references == 0 ||
	  rec->r == NULL || rec->r->references == 0)
	{
	  empty_cache_entries++;
	}
    }

  ccl_debug ("--> #nodes=%d #free=%d #pages=%d #to_release=%d "
	     "#c-entries=%d %%\n",
	     manager->rldd_nb_nodes, manager->pager->nb_free_nodes,
	     manager->pager->nb_pages, manager->nodes_to_release,
	     (empty_cache_entries * 100) / manager->op_table_size);

  empty_cache_entries = 0;

  for (page = pager->first; page != NULL; page = page->next)
    {
      for (i = 0, node = page->nodes; i < pager->page_size; i++, node++)
	node->flag = 0;
    }

  for (page = pager->first; page != NULL; page = page->next)
    {
      for (i = 0, node = page->nodes; i < pager->page_size; i++, node++)
	{
	  if (node->references == 0 && node->flag == 0)
	    s_update_reference_counter (node);
	}
    }

  for (i = 0; i < manager->rldd_actual_size; i++)
    {
      for (pnode = manager->rldd_table + i; *pnode;)
	{
	  if ((*pnode)->references == 0)
	    {
	      node = *pnode;
	      *pnode = node->next;
	      node->next = pager->free_nodes;
	      pager->free_nodes = node;
	      m_label_delete (manager, node->label);
	      pager->nb_free_nodes++;
	      manager->rldd_nb_nodes--;
	      nb_collected++;
	    }
	  else
	    {
	      pnode = &((*pnode)->next);
	    }
	}
    }


  for (i = 0, rec = manager->op_table; i < manager->op_table_size; i++, rec++)
    {
      if (rec->n1 == NULL || rec->n1->references == 0 ||
	  rec->n2 == NULL || rec->n2->references == 0 ||
	  rec->r == NULL || rec->r->references == 0)
	{
	  empty_cache_entries++;
	  rec->op = NULL;
	  rec->n1 = NULL;
	  rec->n2 = NULL;
	  rec->r = NULL;
	}
    }
  manager->nodes_to_release = 0;

  ccl_debug ("<-- #nodes=%d #free=%d #pages=%d #to_release=%d "
	     "#collected=%d #c-entries=%d %%\n\n",
	     manager->rldd_nb_nodes, manager->pager->nb_free_nodes,
	     manager->pager->nb_pages, manager->nodes_to_release,
	     nb_collected,
	     (empty_cache_entries * 100) / manager->op_table_size);

}
#endif

static ar_rldd *
s_allocate_node (ar_rlddm *manager, void *label, ar_rldd *left, ar_rldd *right)
{
  int t;
  ar_rldd *r = manager->pager->free_nodes;

  for (t = 0; r == NULL; t++, r = manager->pager->free_nodes)
    {
      if (t == 0 && manager->nodes_to_release > 1000 * manager->pager->page_size)
	s_collect_nodes (manager);
      else
	s_pager_create_page (manager->pager);
    }

  manager->pager->free_nodes = r->next;
  manager->pager->nb_free_nodes--;

  r->references = 1;
  r->label = label;
  r->left = left;
  r->right = right;
  r->next = NULL;

  if (left == NULL && right == NULL)
    {
      r->min = 0;
      r->max = 0;
    }
  else
    {
      r->min = left->min + 1 < right->min ? left->min + 1 : right->min;
      r->max = left->max + 1 < right->max ? left->max + 1 : right->max;
    }

  return r;
}

			/* --------------- */

ar_rlddm *
ar_rldd_create_manager (int node_table_min_size,
		       int node_table_fill_degree,
		       int op_table_size,
		       int page_size,
		       ccl_duplicate_func *label_dup,
		       ccl_delete_proc *label_delete,
		       ccl_compare_func *label_compare)
{
  ar_rlddm *result = ccl_new (ar_rlddm);

  result->rldd_table = ccl_new_array (ar_rldd *, node_table_min_size);
  result->rldd_actual_size = node_table_min_size;
  result->rldd_min_size = node_table_min_size;
  result->rldd_fill_degree = node_table_fill_degree;
  result->rldd_nb_nodes = 0;
  result->op_table = ccl_new_array (ar_rldd_record, op_table_size);
  result->op_table_size = op_table_size;
  result->nodes_to_release = 0;
  result->pager = s_pager_create (page_size);
  result->nil = s_allocate_node (result, NULL, NULL, NULL);
  result->epsilon = s_allocate_node (result, NULL, NULL, NULL);
  result->label_dup = label_dup;
  result->label_delete = label_delete;
  result->label_compare = label_compare;

  return result;
}

			/* --------------- */

static void
s_pager_destroy_page (ar_rldd_page *page)
{
  ccl_delete (page->nodes);
  if (page->next != NULL)
    s_pager_destroy_page (page->next);
  ccl_delete (page);
}

			/* --------------- */

static void
s_pager_destroy (ar_rldd_pager *pager)
{
  s_pager_destroy_page (pager->first);
  ccl_delete (pager);
}

			/* --------------- */

void
ar_rldd_destroy_manager (ar_rlddm *manager)
{
  int i;
  ar_rldd *node;

  for (i = 0; i < manager->rldd_actual_size; i++)
    {
      for (node = manager->rldd_table[i]; node; node = node->next)
	m_label_delete (manager, node->label);
    }

  ccl_delete (manager->rldd_table);
  ccl_delete (manager->op_table);
  s_pager_destroy (manager->pager);
  ccl_delete (manager);
}

			/* --------------- */

ar_rldd *
ar_rldd_epsilon (ar_rlddm *manager)
{
  return ar_rldd_dup (manager, manager->epsilon);
}

			/* --------------- */

ar_rldd *
ar_rldd_empty (ar_rlddm *manager)
{
  return ar_rldd_dup (manager, manager->nil);
}

			/* --------------- */

ar_rldd *
ar_rldd_letter (ar_rlddm *manager, void *label)
{
  return ar_rldd_concat_letter (manager, label, manager->epsilon);
}

			/* --------------- */

int
ar_rldd_is_nil (ar_rlddm *man, ar_rldd *node)
{
  return (node == man->nil);
}

			/* --------------- */

int
ar_rldd_is_epsilon (ar_rlddm *man, ar_rldd *node)
{
  return (node == man->epsilon);
}

			/* --------------- */

ar_rldd *
ar_rldd_dup (ar_rlddm *manager, ar_rldd *node)
{
  ccl_pre (node != NULL);

  if (node->references == 0)
    manager->nodes_to_release--;
  node->references++;

  return node;
}

			/* --------------- */

void
ar_rldd_delete (ar_rlddm *man, ar_rldd *node)
{
  ccl_pre (node->references > 0);

  node->references--;

  if (node->references == 0)
    {
      man->nodes_to_release++;

      ccl_pre (man->nodes_to_release <= man->rldd_nb_nodes);
    }
}

			/* --------------- */

static ar_rldd *
s_find_or_create_node (ar_rlddm *man, void *label, ar_rldd *left,
		       ar_rldd *right)
{
  ar_rldd *node;
  uint32_t index = m_hash_node (label, left, right) % man->rldd_actual_size;
  ar_rldd **nptr = man->rldd_table + index;

  for (; *nptr; nptr = &((*nptr)->next))
    {
      if (m_label_compare (man, (*nptr)->label, label) == 0 &&
	  (*nptr)->left == left && (*nptr)->right == right)
	{
	  return ar_rldd_dup (man, *nptr);
	}
    }

  label = m_label_dup (man, label);
  left = ar_rldd_dup (man, left);
  right = ar_rldd_dup (man, right);
  node = s_allocate_node (man, label, left, right);
  man->rldd_nb_nodes++;

  for (nptr = man->rldd_table + index; *nptr; nptr = &((*nptr)->next))
    {
      ccl_assert (!(m_label_compare (man, (*nptr)->label, label) == 0 &&
		    (*nptr)->left == left && (*nptr)->right == right));
    }
  *nptr = node;

  return *nptr;
}

			/* --------------- */

ar_rldd *
ar_rldd_union (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2)
{
  void *label;
  ar_rldd *tmpn;
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *r = NULL;

  if (n1 == n2)
    r = ar_rldd_dup (man, n1);
  else if (n1 == man->nil)
    r = ar_rldd_dup (man, n2);
  else if (n2 == man->nil)
    r = ar_rldd_dup (man, n1);
  else
    {
      r = ar_rldd_find_operation (man, &ar_rldd_union, n1, n2);

      if (r != NULL)
	r = ar_rldd_dup (man, r);
      else
	{
	  if (n1 == man->epsilon || n2 == man->epsilon)
	    {
	      if (n1 == man->epsilon)
		{
		  tmpn = n2;
		  n2 = n1;
		  n1 = tmpn;
		}
	      right = ar_rldd_union (man, n1->right, n2);
	      r = s_find_or_create_node (man, n1->label, n1->left, right);
	      ar_rldd_delete (man, right);
	    }
	  else
	    {
	      int c = m_label_compare (man, n1->label, n2->label);
	      if (c == 0)
		{
		  label = n1->label;
		  left = ar_rldd_union (man, n1->left, n2->left);
		  right = ar_rldd_union (man, n1->right, n2->right);
		}
	      else if (c < 0)
		{
		  label = n1->label;
		  left = ar_rldd_dup (man, n1->left);
		  right = ar_rldd_union (man, n1->right, n2);
		}
	      else
		{
		  label = n2->label;
		  left = ar_rldd_dup (man, n2->left);
		  right = ar_rldd_union (man, n1, n2->right);
		}
	      r = s_find_or_create_node (man, label, left, right);
	      ar_rldd_delete (man, left);
	      ar_rldd_delete (man, right);
	    }
	  r = ar_rldd_add_operation (man, &ar_rldd_union, n1, n2, r);
	  r = ar_rldd_add_operation (man, &ar_rldd_union, n2, n1, r);
	}
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_intersection (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2)
{
  int c;
  ar_rldd *tmpn;
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *r = ar_rldd_find_operation (man, &ar_rldd_intersection, n1, n2);

  if (r != NULL)
    r = ar_rldd_dup (man, r);
  else
    {
      if (n1 == n2)
	r = ar_rldd_dup (man, n1);
      else if (n1 == man->nil || n2 == man->nil)
	r = ar_rldd_dup (man, man->nil);
      else if (n1 == man->epsilon || n2 == man->epsilon)
	{
	  if (n1 == man->epsilon)
	    {
	      tmpn = n2;
	      n2 = n1;
	      n1 = tmpn;
	    }
	  r = ar_rldd_intersection (man, n1->right, n2);
	}
      else
	{
	  c = m_label_compare (man, n1->label, n2->label);
	  if (c == 0)
	    {
	      left = ar_rldd_intersection (man, n1->left, n2->left);
	      right = ar_rldd_intersection (man, n1->right, n2->right);
	      if (left == man->nil)
		r = ar_rldd_dup (man, right);
	      else
		{
		  ar_rldd *a = ar_rldd_concat_letter (man, n1->label, left);
		  r = ar_rldd_union (man, a, right);
		  ar_rldd_delete (man, a);
		}
	      ar_rldd_delete (man, left);
	      ar_rldd_delete (man, right);
	    }
	  else if (c < 0)
	    {
	      r = ar_rldd_intersection (man, n1->right, n2);
	    }
	  else
	    {
	      r = ar_rldd_intersection (man, n1, n2->right);
	    }

	  ar_rldd_add_operation (man, &ar_rldd_intersection, n1, n2, r);
	}
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_concatenation (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2)
{
  ar_rldd *a;
  ar_rldd *b;
  ar_rldd *c;
  ar_rldd *r = NULL;

  if (n1 == man->nil || n2 == man->nil)
    r = ar_rldd_dup (man, man->nil);
  else if (n1 == man->epsilon)
    r = ar_rldd_dup (man, n2);
  else if (n2 == man->epsilon)
    r = ar_rldd_dup (man, n1);
  else
    {
      r = ar_rldd_find_operation (man, &ar_rldd_concatenation, n1, n2);
      if (r != NULL)
	r = ar_rldd_dup (man, r);
      else
	{
	  a = ar_rldd_concatenation (man, n1->left, n2);
	  b = ar_rldd_concat_letter (man, n1->label, a);
	  c = ar_rldd_concatenation (man, n1->right, n2);
	  r = ar_rldd_union (man, b, c);
	  ar_rldd_delete (man, a);
	  ar_rldd_delete (man, b);
	  ar_rldd_delete (man, c);
	  r = ar_rldd_add_operation (man, &ar_rldd_concatenation, n1, n2, r);
	}
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_concat_letter (ar_rlddm *man, void *label, ar_rldd *n)
{
  ar_rldd *r;

  if (n == man->nil)
    r = ar_rldd_dup (man, n);
  else
    r = s_find_or_create_node (man, label, n, man->nil);

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_extract (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2)
{
  ar_rldd *a;
  ar_rldd *b;
  ar_rldd *c;
  ar_rldd *d;
  ar_rldd *r = NULL;

  if (n1 == n2)
    r = ar_rldd_dup (man, man->nil);
  else if (n1 == man->nil)
    r = ar_rldd_dup (man, man->nil);
  else if (n2 == man->nil)
    r = ar_rldd_dup (man, n1);
  else if (n2 == man->epsilon)
    r = ar_rldd_dup (man, man->nil);
  else
    {
      r = ar_rldd_find_operation (man, &ar_rldd_extract, n1, n2);
      if (r != NULL)
	{
	  r = ar_rldd_dup (man, r);
	}
      else
	{
	  if (n1 == man->epsilon)
	    r = ar_rldd_extract (man, n1, n2->right);
	  else
	    {
	      int cmp = m_label_compare (man, n1->label, n2->label);
	      if (cmp == 0)
		{
		  a = ar_rldd_extract (man, n1->left, n2->left);
		  b = ar_rldd_extract (man, n1->left, n2->right);
		  d = ar_rldd_intersection (man, a, b);
		  ar_rldd_delete (man, a);
		  ar_rldd_delete (man, b);
		  b = ar_rldd_concat_letter (man, n1->label, d);
		  ar_rldd_delete (man, d);
		}
	      else if (cmp < 0)
		{
		  d = ar_rldd_extract (man, n1->left, n2);
		  b = ar_rldd_concat_letter (man, n1->label, d);
		  ar_rldd_delete (man, d);
		}
	      else
		{
		  d = ar_rldd_concat_letter (man, n2->label, n2->left);
		  c = ar_rldd_extract (man, n1->left, d);
		  ar_rldd_delete (man, d);
		  d = ar_rldd_concat_letter (man, n1->label, c);
		  b = ar_rldd_extract (man, d, n2->right);
		  ar_rldd_delete (man, c);
		  ar_rldd_delete (man, d);
		}

	      a = ar_rldd_extract (man, n1->right, n2);
	      r = ar_rldd_union (man, a, b);
	      ar_rldd_delete (man, a);
	      ar_rldd_delete (man, b);
	    }
	  r = ar_rldd_add_operation (man, &ar_rldd_extract, n1, n2, r);
	}
    }
  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_min (ar_rlddm *man, ar_rldd *n)
{
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *a;
  ar_rldd *b;
  ar_rldd *r = NULL;

  if (n == man->nil || n == man->epsilon)
    r = ar_rldd_dup (man, n);
  else
    {
      r = ar_rldd_find_operation (man, &ar_rldd_min, n, n);
      if (r != NULL)
	{
	  r = ar_rldd_dup (man, r);
	}
      else if (0)
	{
	  left = ar_rldd_min (man, n->left);
	  a = ar_rldd_extract (man, left, n->right);
	  ar_rldd_delete (man, left);
	  left = ar_rldd_concat_letter (man, n->label, a);
	  ar_rldd_delete (man, a);

	  a = ar_rldd_min (man, n->right);
	  b = ar_rldd_concat_letter (man, n->label, n->left);
	  right = ar_rldd_extract (man, a, b);
	  ar_rldd_delete (man, a);
	  ar_rldd_delete (man, b);

	  r = ar_rldd_union (man, left, right);
	  ar_rldd_delete (man, left);
	  ar_rldd_delete (man, right);

	  r = ar_rldd_add_operation (man, &ar_rldd_min, n, n, r);
	}
      else
	{
	  a = ar_rldd_min (man, n->left);
	  left = ar_rldd_extract (man, a, n->right);
	  ar_rldd_delete (man, a);

	  a = ar_rldd_min (man, n->right);
	  b = ar_rldd_concat_letter (man, n->label, n->left);
	  right = ar_rldd_extract (man, a, b);
	  ar_rldd_delete (man, a);
	  ar_rldd_delete (man, b);

	  if (left == man->nil)
	    r = ar_rldd_dup (man, right);
	  else
	    r = s_find_or_create_node (man, n->label, left, right);
	  ar_rldd_delete (man, left);
	  ar_rldd_delete (man, right);
	  r = ar_rldd_add_operation (man, &ar_rldd_min, n, n, r);
	}
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_power (ar_rlddm *man, ar_rldd *E, int N)
{
  ar_rldd *R;

  if (N <= 0)
    R = ar_rldd_empty (man);
  else if (N == 1)
    R = ar_rldd_dup (man, E);
  else
    {
      ar_rldd *tmp = ar_rldd_power (man, E, N >> 1);

      R = ar_rldd_concatenation (man, tmp, tmp);

      ar_rldd_delete (man, tmp);
      if ((N & 1) != 0)
	{
	  tmp = ar_rldd_concatenation (man, E, R);
	  ar_rldd_delete (man, R);
	  R = tmp;
	}
    }

  return R;
}

			/* --------------- */

ar_rldd *
ar_rldd_find_operation (ar_rlddm *man, void *op, ar_rldd *n1, ar_rldd *n2)
{
  long index = m_hash_operation (op, n1, n2) % man->op_table_size;
  ar_rldd_record *rec = man->op_table + index;

  if (rec->op != op)
    return NULL;
  if (rec->n1 == NULL || rec->n2 == NULL || rec->r == NULL)
    return NULL;
  if (rec->n1 == n1 && rec->n2 == n2)
    return rec->r;

  return NULL;
}

			/* --------------- */

ar_rldd *
ar_rldd_add_operation (ar_rlddm *man, void *op, ar_rldd *n1, ar_rldd *n2, ar_rldd *r)
{
  long index = m_hash_operation (op, n1, n2) % man->op_table_size;
  ar_rldd_record *rec = man->op_table + index;

  rec->op = op;
  rec->n1 = n1;
  rec->n2 = n2;
  rec->r = r;

  return r;
}

			/* --------------- */

typedef struct log_stack {
  void *label;
  struct log_stack *tail;
} log_stack;

static void
s_display_stack (ccl_log_type log, ar_rldd_log_label *log_label, void *data, 
		 log_stack *s)
{
  if (s == NULL)
    return;

  if (s->tail != NULL)
    {
      s_display_stack (log, log_label, data, s->tail);
      ccl_log (log, ", ");
    }

  if (log_label)
    log_label (log, s->label, data);
  else
    ccl_log (log, "%p", s->label);  
}

			/* --------------- */

static void
s_log_sequences_rec (ccl_log_type log, ar_rlddm *man, ar_rldd *n, 
		     ar_rldd_log_label *log_label, void *data, log_stack *s)
{
  if (n == man->nil)
    return;

  if (n == man->epsilon)
    {
      ccl_log (log, "(");
      s_display_stack (log, log_label, data, s);
      ccl_log (log, ")\n");
    }
  else
    {
      log_stack ls;

      ls.label = n->label;
      ls.tail = s;
      s_log_sequences_rec (log, man, n->left, log_label, data, &ls);
      s_log_sequences_rec (log, man, n->right, log_label, data, s);
    }
}

			/* --------------- */

void
ar_rldd_display_node_sequences (ar_rlddm *man, ar_rldd *n, ccl_log_type log,
			       ar_rldd_log_label *log_label, void *data)
{
  s_log_sequences_rec (log, man, n, log_label, data, NULL);
}

			/* --------------- */

void
ar_rldd_display_node_table (ar_rlddm *man, ccl_log_type log)
{
  ccl_log (log, "********* node table ***********\n");
  ccl_log (log, "size                    = %6d\n", man->rldd_actual_size);
  ccl_log (log, "minimal size            = %6d\n", man->rldd_min_size);
  ccl_log (log, "fill degree             = %6d\n", man->rldd_fill_degree);
  ccl_log (log, "page size               = %6d\n", man->pager->page_size);
  ccl_log (log, "number of pages         = %6d (=%6d nodes)\n",
	   man->pager->nb_pages,
	   man->pager->nb_pages * man->pager->page_size);
  ccl_log (log, "number of nodes         = %6d\n", man->rldd_nb_nodes + 2);
  ccl_log (log, "current free nodes      = %6d\n",
	   man->pager->nb_free_nodes);
  ccl_log (log, "total number of nodes   = %6d\n",
	   man->pager->nb_free_nodes + man->rldd_nb_nodes + 2);
  ccl_log (log, "********************************\n\n");
}

			/* --------------- */

void *
ar_rldd_get_label (ar_rldd *n)
{
  return n->label;
}

			/* --------------- */

ar_rldd *
ar_rldd_get_left (ar_rldd *n)
{
  return n->left;
}

			/* --------------- */

ar_rldd *
ar_rldd_get_right (ar_rldd *n)
{
  return n->right;
}

			/* --------------- */

typedef struct node_list_st node_list;
struct node_list_st
{
  ar_rldd *n;
  node_list *next;
};

			/* --------------- */

static int
s_node_list_has (node_list *l, ar_rldd *n)
{
  for (; l != NULL; l = l->next)
    if (l->n == n)
      return 1;
  return 0;
}

			/* --------------- */

static void
s_node_list_delete (node_list * l)
{
  node_list *next;
  for (; l != NULL; l = next)
    {
      next = l->next;
      ccl_delete (l);
    }
}

			/* --------------- */

static int
s_node_list_index_of (node_list * l, ar_rldd *n)
{
  int result = 0;

  for (; l != NULL; l = l->next, result++)
    if (l->n == n)
      break;
  if (l == NULL)
    result = -1;

  return result;
}

			/* --------------- */

static int
s_node_list_size (node_list *l)
{
  int result = 0;

  for (; l != NULL; l = l->next, result++)
    /* empty */ ;

  return result;
}

			/* --------------- */

static void
s_rldd_nodes (ar_rldd *n, node_list **l)
{
  node_list *aux;

  if (s_node_list_has (*l, n))
    return;

  aux = ccl_new (node_list);
  aux->n = n;
  aux->next = *l;
  *l = aux;


  if (n->left != NULL)
    s_rldd_nodes (n->left, l);

  if (n->right != NULL)
    s_rldd_nodes (n->right, l);
}

			/* --------------- */

int
ar_rldd_size (ar_rldd *n)
{
  int result = 0;
  node_list *l = NULL;

  s_rldd_nodes (n, &l);
  result = s_node_list_size (l);
  s_node_list_delete (l);

  return result;
}

			/* --------------- */

void
ar_rldd_display_node_structure (ar_rlddm *man, ar_rldd *n, ccl_log_type log,
			       ar_rldd_log_label *log_label, void *data)
{
  node_list *l = NULL;
  node_list *p;

  s_rldd_nodes (n, &l);

  for (p = l; p; p = p->next)
    {
      ar_rldd *n = p->n;
      if (ar_rldd_is_nil (man, n))
	ccl_log (log, "[%d] : {}", s_node_list_index_of (l, n));
      else if (ar_rldd_is_epsilon (man, n))
	ccl_log (log, "[%d] : $", s_node_list_index_of (l, n));
      else
	{
	  ccl_log (log, "[%d] : ", s_node_list_index_of (l, n));
	  if (log_label != NULL)
	    log_label (log, n->label, data);
	  else
	    ccl_log (log, "%p", n->label);
	  ccl_log (log, ".[%d] + [%d]", s_node_list_index_of (l, n->left),
		   s_node_list_index_of (l, n->right));
	}
      ccl_log (log, "\n");
    }
  s_node_list_delete (l);
}

			/* --------------- */

void
ar_rldd_map_nodes (ar_rlddm *man, ar_rldd *n, ar_rldd_map_node_proc *map, 
		  void *data)
{
  node_list *l = NULL;
  node_list *p;

  s_rldd_nodes (n, &l);

  for (p = l; p; p = p->next)
    {
      ar_rldd *n = p->n;
      map (man, n, data);
    }
  s_node_list_delete (l);
}

			/* --------------- */

ar_rldd *
ar_rldd_div (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2)
{
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *r = ar_rldd_find_operation (man, &ar_rldd_div, n1, n2);

  if (r != NULL)
    r = ar_rldd_dup (man, r);
  else
    {
      if (n1 == man->nil || n2 == man->nil)
	r = ar_rldd_dup (man, man->nil);
      else if (n2 == man->epsilon)
	r = ar_rldd_dup (man, n1);
      else if (n1 == man->epsilon)
	r = ar_rldd_div (man, n1, n2->right);
      else
	{
	  int c = m_label_compare (man, n1->label, n2->label);
	  if (c == 0)
	    {
	      left = ar_rldd_div (man, n1->left, n2->left);
	      right = ar_rldd_div (man, n1->right, n2->right);
	      r = ar_rldd_union (man, left, right);
	      ar_rldd_delete (man, left);
	      ar_rldd_delete (man, right);
	    }
	  else if (c < 0)
	    {
	      r = ar_rldd_div (man, n1->right, n2);
	    }
	  else
	    {
	      r = ar_rldd_div (man, n1, n2->right);
	    }
	}

      r = ar_rldd_add_operation (man, &ar_rldd_div, n1, n2, r);
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_difference (ar_rlddm * man, ar_rldd * n1, ar_rldd * n2)
{
  int c;
  ar_rldd *tmpn;
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *r = ar_rldd_find_operation (man, &ar_rldd_intersection, n1, n2);

  if (r != NULL)
    r = ar_rldd_dup (man, r);
  else
    {
      if (n1 == n2)
	r = ar_rldd_dup (man, n1);
      else if (n1 == man->nil || n2 == man->nil)
	r = ar_rldd_dup (man, man->nil);
      else if (n1 == man->epsilon || n2 == man->epsilon)
	{
	  if (n1 == man->epsilon)
	    {
	      tmpn = n2;
	      n2 = n1;
	      n1 = tmpn;
	    }
	  r = ar_rldd_intersection (man, n1->right, n2);
	}
      else
	{
	  c = m_label_compare (man, n1->label, n2->label);
	  if (c == 0)
	    {
	      left = ar_rldd_intersection (man, n1->left, n2->left);
	      right = ar_rldd_intersection (man, n1->right, n2->right);
	      if (left == man->nil)
		r = ar_rldd_dup (man, right);
	      else
		r = s_find_or_create_node (man, n1->label, left, right);
	      ar_rldd_delete (man, left);
	      ar_rldd_delete (man, right);
	    }
	  else if (c < 0)
	    r = ar_rldd_intersection (man, n1->right, n2);
	  else
	    r = ar_rldd_intersection (man, n1, n2->right);

	  ar_rldd_add_operation (man, &ar_rldd_intersection, n1, n2, r);
	}
    }

  return r;
}

			/* --------------- */

int
ar_rldd_get_height (ar_rldd * n)
{
  int l, r;

  if (n->left == NULL && n->right == NULL)
    return 0;

  l = ar_rldd_get_height (n->left);
  r = ar_rldd_get_height (n->right);

  return ((l < r) ? r : l) + 1;
}

			/* --------------- */

static ar_rldd *
s_project (ar_rlddm * man, ar_rldd *n, void *label)
{
  ar_rldd *left;
  ar_rldd *right;
  ar_rldd *r = NULL;

  if (n == man->nil || n == man->epsilon)
    r = ar_rldd_dup (man, n);
  else
    {
      int c = m_label_compare (man, n->label, label);
      left = s_project (man, n->left, label);
      right = s_project (man, n->right, label);

      if (c != 0)
	{
	  ar_rldd *tmp = ar_rldd_concat_letter (man, n->label, left);
	  ar_rldd_delete (man, left);
	  left = tmp;
	}
      r = ar_rldd_union (man, left, right);
      ar_rldd_delete (man, right);
      ar_rldd_delete (man, left);
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_project (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2)
{
  ar_rldd *n;
  ar_rldd *tmp;
  ar_rldd *aux;
  ar_rldd *r = ar_rldd_find_operation (man, &ar_rldd_project, n1, n2);

  if (r != NULL)
    r = ar_rldd_dup (man, r);
  else if (n1 == man->nil || n1 == man->epsilon)
    r = ar_rldd_dup (man, n1);
  else if (n2 == man->nil || n2 == man->epsilon)
    r = ar_rldd_dup (man, n1);
  else
    {
      r = ar_rldd_empty (man);
      for (n = n2; n != man->nil && n != man->epsilon; n = n->right)
	{
	  tmp = s_project (man, n1, n->label);
	  aux = ar_rldd_union (man, r, tmp);
	  r = ar_rldd_dup (man, aux);
	  ar_rldd_delete (man, aux);
	  ar_rldd_delete (man, tmp);
	}
      ar_rldd_add_operation (man, &ar_rldd_project, n1, n2, r);
    }

  return r;
}

			/* --------------- */

ar_rldd *
ar_rldd_reverse (ar_rlddm *man, ar_rldd *n)
{
  ar_rldd *a;
  ar_rldd *b;
  ar_rldd *c;
  ar_rldd *d;
  ar_rldd *r = ar_rldd_find_operation (man, &ar_rldd_reverse, n, n);

  if (r != NULL)
    {
      r = ar_rldd_dup (man, r);
    }
  else
    {
      if (n == man->nil || n == man->epsilon)
	r = ar_rldd_dup (man, n);
      else
	{
	  a = ar_rldd_reverse (man, n->left);
	  b = ar_rldd_reverse (man, n->right);
	  c = ar_rldd_letter (man, n->label);
	  d = ar_rldd_concatenation (man, a, c);
	  r = ar_rldd_union (man, b, d);
	  ar_rldd_delete (man, a);
	  ar_rldd_delete (man, b);
	  ar_rldd_delete (man, c);
	  ar_rldd_delete (man, d);
	}

      r = ar_rldd_add_operation (man, &ar_rldd_reverse, n, n, r);
    }

  return r;
}

			/* --------------- */


int
ar_rldd_number_of_sequences (ar_rlddm * man, ar_rldd * n)
{
  int result = 0;

  if (ar_rldd_is_nil (man, n))
    result = 0;
  else if (ar_rldd_is_epsilon (man, n))
    result = 1;
  else
    result = (ar_rldd_number_of_sequences (man, n->left)
	      + ar_rldd_number_of_sequences (man, n->right));

  return result;
}
