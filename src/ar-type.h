/*
 * ar-type.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_TYPE_H
# define AR_TYPE_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-serializer.h>
# include "ar-identifier.h"

struct ar_constant_st;
struct ar_node_st;

typedef union ar_type_union ar_type;
typedef enum {
  /* base types */
  AR_TYPE_BOOLEANS,
  AR_TYPE_INTEGERS,
  AR_TYPE_SYMBOLS,
  AR_TYPE_ABSTRACT,

  /* restrictions */
  AR_TYPE_RANGE,
  AR_TYPE_ENUMERATION,
  AR_TYPE_SYMBOL_SET,

  /* structures */
  AR_TYPE_ARRAY,
  AR_TYPE_STRUCTURE,
  AR_TYPE_BANG
} ar_type_kind;

typedef enum {
  AR_TYPE_BANG_CONFIGURATIONS = 0,
  AR_TYPE_BANG_EVENTS,
  AR_TYPE_BANG_VARIABLES,
  AR_TYPE_LAST_AND_UNUSED_BANG_TYPE
} ar_type_bang_kind;

			/* --------------- */

extern ar_type *AR_BOOLEANS;
extern ar_type *AR_INTEGERS;
extern ar_type *AR_SYMBOLS;

			/* --------------- */

extern void
ar_type_init(void);
extern void
ar_type_terminate(void);

			/* --------------- */

extern int
ar_type_get_width (const ar_type *type);
extern int
ar_type_get_size (const ar_type *type);
extern int
ar_type_get_cardinality (const ar_type *type);

			/* --------------- */

extern ar_type *
ar_type_crt_abstract(ar_identifier *name);
extern ar_identifier *
ar_type_abstract_get_name(ar_type *type);

			/* --------------- */

extern ar_type *
ar_type_crt_range(int min, int max);
extern int
ar_type_range_get_min(ar_type *type);
extern int
ar_type_range_get_max(ar_type *type);
extern void
ar_type_range_get_bounds(ar_type *type, int *pmin, int *pmax);

			/* --------------- */

extern ar_type *
ar_type_crt_symbol_set(void);
extern int
ar_type_symbol_set_add_value(ar_type *type, ar_identifier *id);
extern int
ar_type_symbol_set_has_value_name(ar_type *type, ar_identifier *id);
extern int
ar_type_symbol_set_has_value(ar_type *type, int id);
extern int
ar_type_symbol_set_get_value(ar_identifier *id);
extern int
ar_type_symbol_set_get_value_name_index (ar_type *type, ar_identifier *id);
extern int
ar_type_symbol_set_get_value_index (ar_type *type, int val);
extern ar_identifier *
ar_type_symbol_set_get_value_name(uint32_t id);
extern ccl_int_iterator *
ar_type_symbol_set_get_values(ar_type *type);

			/* --------------- */

extern ar_type *
ar_type_crt_enum (void);
extern int
ar_type_enum_add_value (ar_type *type, ar_identifier *id);
extern int
ar_type_enum_has_value (const ar_type *type, ar_identifier *id);
extern int
ar_type_enum_get_value_index (const ar_type *type, ar_identifier *id);
extern ar_identifier *
ar_type_enum_get_value_at_index (const ar_type *type, int id);

			/* --------------- */

extern ar_type *
ar_type_crt_array (ar_type *base_type, uint32_t size);
extern ar_type *
ar_type_array_get_base_type (const ar_type *type);
extern uint32_t 
ar_type_array_get_size (const ar_type *type);

			/* --------------- */

extern ar_type *
ar_type_crt_structure(void);
extern void 
ar_type_struct_add_field(ar_type *type, ar_identifier *fname, ar_type *ftype);
extern ar_type *
ar_type_struct_get_field(ar_type *type, ar_identifier *fname);
extern int
ar_type_struct_has_field(ar_type *type, ar_identifier *fname);
extern ar_identifier_iterator *
ar_type_struct_get_fields(ar_type *type);
extern int 
ar_type_struct_get_field_index(ar_type *type, ar_identifier *fname);
extern void
ar_type_struct_map_fields (ar_type *type,
			   void (*map)(ar_identifier *id, ar_type *type,
				       void *mapdata),
			   void *mapdata);
			   
			/* --------------- */

extern ar_type *
ar_type_crt_bang (ar_type_bang_kind kind, struct ar_node_st *node);

#define ar_type_crt_configurations(node) \
  ar_type_crt_bang (AR_TYPE_BANG_CONFIGURATIONS, node)

#define ar_type_crt_events(node) \
  ar_type_crt_bang (AR_TYPE_BANG_EVENTS, node)

#define ar_type_crt_variables(node) \
  ar_type_crt_bang (AR_TYPE_BANG_VARIABLES, node)

extern struct ar_node_st *
ar_type_bang_get_node (ar_type *type);

extern ar_type_bang_kind
ar_type_bang_get_kind (ar_type *type);

			/* --------------- */

extern int
ar_type_is_scalar(ar_type *type) ;
extern ar_type_kind
ar_type_get_kind(ar_type *type);
extern ar_type *
ar_type_add_reference(ar_type *type);
extern void
ar_type_del_reference(ar_type *type);
extern void
ar_type_log(ccl_log_type log, ar_type *type);
extern char *
ar_type_to_string(ar_type *type);

			/* --------------- */

extern ar_type *
ar_type_get_base_type(ar_type *t);
extern int
ar_type_equals(ar_type *t1, ar_type *t2);
extern int
ar_type_have_same_base_type(ar_type *t1, ar_type *t2);

			/* --------------- */

extern ccl_list *
ar_type_expand (ar_type *t, ccl_list *result);

extern ccl_list *
ar_type_expand_with_prefix (ar_identifier *prefix, ar_type *t, 
			    ccl_list *result);

			/* --------------- */

extern struct ar_constant_st *
ar_type_get_ith_element (ar_type *t, int i);

			/* --------------- */

extern void
ar_type_write (ar_type *t, FILE *out, ccl_serializer_status *p_err);

extern void
ar_type_read (ar_type **p_t, FILE *in, ccl_serializer_status *p_err);

#endif /* ! AR_TYPE_H */
