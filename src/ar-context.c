/*
 * ar-context.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-node.h"
#include "ar-expr.h"
#include "ar-context.h"
#include "ar-context.h"

			/* --------------- */

typedef enum table_index_enum {
  SLOTS=0,
  TYPES,
  SIGNATURES,
  LAW_PARAMS,
  NB_TABLES
} table_index;

struct ar_context_st {
  int refcount;
  ar_context *parent;  
  ar_idtable *tables[NB_TABLES];
};

struct ar_context_slot_st {
  int refcount;
  int index;
  ar_identifier *name;
  ar_type *type;
  uint32_t flags;
  ccl_list *attr;
  struct ar_expr_st *value;
};

			/* --------------- */

static void
s_add_object(ar_context *ctx, table_index idx, ar_identifier *id, void *obj);
static int
s_has_object(ar_context *ctx, table_index idx, ar_identifier *id);
static void *
s_get_object(ar_context *ctx, table_index idx, const ar_identifier *id);
static void 
s_remove_object(ar_context *ctx, table_index idx, ar_identifier *id);
static ccl_list *
s_get_ids(ar_context *ctx, table_index idx);
static ccl_list *
s_get_slots(ar_context *ctx, uint32_t mask);
static ccl_list *
s_get_local_slots(ar_context *ctx, uint32_t mask);

			/* --------------- */

ar_context *
ar_context_create_top(void)
{
  return ar_context_create(NULL);
}

			/* --------------- */

ar_context *
ar_context_create(ar_context *parent)
{
  ar_context *result = ccl_new(ar_context);

  result->refcount = 1;
  result->parent = parent?ar_context_add_reference(parent):NULL;
  result->tables[SLOTS] = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_context_slot_add_reference,
		      (ccl_delete_proc *)ar_context_slot_del_reference);
  result->tables[TYPES] = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_type_add_reference,
		      (ccl_delete_proc *)ar_type_del_reference);
  result->tables[SIGNATURES] = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_signature_add_reference,
		      (ccl_delete_proc *)ar_signature_del_reference);
  result->tables[LAW_PARAMS] = 
    ar_idtable_create(1, (ccl_duplicate_func *)sas_law_param_add_reference,
		      (ccl_delete_proc *)sas_law_param_del_reference);

  return result;
}

			/* --------------- */

ar_context *
ar_context_create_from_slots(ar_context *parent, ccl_list *slots)
{
  ccl_pair *p;
  ar_context *result = ar_context_create(parent);

  for(p = FIRST(slots); p; p = CDR(p))
    {
      ar_context_slot *s = (ar_context_slot *)CAR(p);
      ar_identifier *id = ar_context_slot_get_name(s);

      ccl_assert( id != NULL );

      ar_context_add_slot(result,id,s);
      ar_identifier_del_reference(id);
    }

  return (ar_context *)result;
}

			/* --------------- */

ar_context *
ar_context_add_reference(ar_context *ctx)
{
  ccl_pre( ctx != NULL );

  ctx->refcount++;

  return ctx;
}

			/* --------------- */

void
ar_context_del_reference(ar_context *ctx)
{  
  ccl_pre( ctx != NULL ); ccl_pre( ctx->refcount > 0 );

  if( --ctx->refcount == 0 )
    {
      int i;

      if( ctx->parent != NULL )
	ar_context_del_reference(ctx->parent);

      for(i = 0; i < NB_TABLES; i++)
	ar_idtable_del_reference(ctx->tables[i]);
      ccl_delete(ctx);
    }
}

			/* --------------- */

ar_context *
ar_context_get_parent(ar_context *ctx)
{
  ccl_pre( ctx != NULL );

  return ctx->parent?ar_context_add_reference(ctx->parent):NULL;
}

			/* --------------- */

void
ar_context_add_type(ar_context *ctx, ar_identifier *id, ar_type *type)
{
  s_add_object(ctx,TYPES,id,type);
}

			/* --------------- */

int
ar_context_has_type(ar_context *ctx, ar_identifier *id)
{
  return s_has_object(ctx,TYPES,id);
}

			/* --------------- */

ar_type *
ar_context_get_type(ar_context *ctx, ar_identifier *id)
{
  return (ar_type *)s_get_object(ctx,TYPES,id);
}

			/* --------------- */

void
ar_context_remove_type(ar_context *ctx, ar_identifier *id)
{
  s_remove_object(ctx,TYPES,id);
}

			/* --------------- */

ccl_list *
ar_context_get_types(ar_context *ctx)
{
  return s_get_ids(ctx,TYPES);
}

			/* --------------- */

void
ar_context_add_signature(ar_context *ctx, ar_identifier *id, 
			 ar_signature *signature)
{
  s_add_object(ctx,SIGNATURES,id,signature);
}

			/* --------------- */

int
ar_context_has_signature(ar_context *ctx, ar_identifier *id)
{
  return s_has_object(ctx,SIGNATURES,id);
}

			/* --------------- */

ar_signature *
ar_context_get_signature(ar_context *ctx, ar_identifier *id)
{
  return (ar_signature *)s_get_object(ctx,SIGNATURES,id);
}

			/* --------------- */

void
ar_context_remove_signature(ar_context *ctx, ar_identifier *id)
{
  s_remove_object(ctx,SIGNATURES,id);
}

			/* --------------- */

ccl_list *
ar_context_get_signatures(ar_context *ctx)
{
  return s_get_ids(ctx,SIGNATURES);
}

			/* --------------- */

void
ar_context_add_law_parameter(ar_context *ctx, ar_identifier *id, 
			 sas_law_param *law_param)
{
  s_add_object(ctx,LAW_PARAMS,id,law_param);
}

			/* --------------- */

int
ar_context_has_law_parameter(ar_context *ctx, ar_identifier *id)
{
  return s_has_object(ctx,LAW_PARAMS,id);
}

			/* --------------- */

sas_law_param *
ar_context_get_law_parameter(ar_context *ctx, ar_identifier *id)
{
  return (sas_law_param *)s_get_object(ctx,LAW_PARAMS,id);
}

			/* --------------- */

void
ar_context_remove_law_parameter(ar_context *ctx, ar_identifier *id)
{
  s_remove_object(ctx,LAW_PARAMS,id);
}

			/* --------------- */

ccl_list *
ar_context_get_law_parameters(ar_context *ctx)
{
  return s_get_ids(ctx,LAW_PARAMS);
}

			/* --------------- */

void
ar_context_add_slot(ar_context *ctx, ar_identifier *id, ar_context_slot *slot)
{
  s_add_object(ctx,SLOTS,id,slot);
}

			/* --------------- */

int
ar_context_has_slot(ar_context *ctx, ar_identifier *id)
{
  return s_has_object(ctx,SLOTS,id);
}

			/* --------------- */

ar_context_slot *
ar_context_get_slot(ar_context *ctx, const ar_identifier *id)
{
  return (ar_context_slot *)s_get_object(ctx,SLOTS,id);
}

			/* --------------- */

void
ar_context_remove_slot(ar_context *ctx, ar_identifier *id)
{
  s_remove_object(ctx,SLOTS,id);
}

			/* --------------- */

ccl_list *
ar_context_get_slots(ar_context *ctx, uint32_t flags)
{
  return s_get_slots(ctx, flags);
}

			/* --------------- */

ccl_list *
ar_context_get_local_slots (ar_context *ctx, uint32_t mask)
{
  return s_get_local_slots (ctx, mask);
}

			/* --------------- */

void
ar_context_log_slots(ar_context *ctx, uint32_t mask,
		     ccl_list *(*flags_to_strings)(uint32_t flags))
{
  ccl_pair *p;
  ccl_list *slot_ids = ar_context_get_slots(ctx,mask);

  for(p = FIRST(slot_ids) ; p; p = CDR(p))
    {
      ar_identifier *slot_id = (ar_identifier *)CAR(p);
      ar_context_slot *slot = ar_context_get_slot(ctx,slot_id);
      
      ar_identifier_log(CCL_LOG_DISPLAY,slot_id);

      ccl_display("(depth=%d): ",ar_identifier_get_path_length(slot_id));
      ar_identifier_del_reference(slot_id);

      if( slot->index >= 0 )
	ccl_display("index = %d ",slot->index);
      ccl_display("type = ");
      ar_type_log(CCL_LOG_DISPLAY,slot->type);      
      ccl_display(" flags = ");
      if (flags_to_strings)
	{
	  ccl_pair *pf;
	  ccl_list *flags = flags_to_strings(slot->flags);
	  for(pf = FIRST(flags); pf; pf = CDR(pf))
	    {
	      if( CDR(pf) ) ccl_display("%s|",CAR(pf));
	      else ccl_display("%s",CAR(pf));
	    }
	  ccl_display(" ");
	  ccl_list_delete(flags);
	}

      if( slot->attr != NULL && ! ccl_list_is_empty(slot->attr) )
	{
	  ccl_pair *pa;

	  ccl_display("attr = { ");	  
	  for(pa = FIRST(slot->attr); pa; pa = CDR(pa))
	    {
	      ar_identifier_log(CCL_LOG_DISPLAY,CAR(pa));
	      if( CDR(pa) != NULL )
		ccl_display(", ");
	    }
	  ccl_display(" } ");
	}
      
      if( slot->value != NULL )
	{
	  ccl_display("value = ");
	  ar_expr_log(CCL_LOG_DISPLAY,slot->value);
	}

      
      ccl_display("\n");

      ar_context_slot_del_reference(slot);
    }
  ccl_list_delete(slot_ids);
}

			/* --------------- */

static void
s_expand_composite_slot (ar_context *ctx, ar_identifier *slot_name, 
			 ar_type *type, ccl_list *result)
{
  if (ar_type_is_scalar(type))
    {
      ar_context_slot *sl = ar_context_get_slot (ctx, slot_name);
      ccl_assert (sl != NULL);
      if (ccl_list_has (result, sl))
	ar_context_slot_del_reference (sl);
      else
	ccl_list_add (result, sl);
    }
  else 
    {
      ar_type_kind k = ar_type_get_kind (type);

      switch (k)
	{
	case AR_TYPE_ARRAY:
	  {
	    int i;
	    char *tmp = NULL;
	    size_t tmp_size = 0;
	    int w = ar_type_get_width (type);
	    ar_type *stype = ar_type_array_get_base_type (type);

	    for (i = 0; i < w; i++)
	      {
		ar_identifier *aid;
		
		ccl_string_format (&tmp, &tmp_size, "[%d]", i);
		aid = ar_identifier_rename_with_suffix (slot_name, tmp);
		
		s_expand_composite_slot (ctx, aid, stype, result);
		ar_identifier_del_reference (aid);
	      }

	    ccl_string_delete (tmp);
	    ar_type_del_reference (stype);
	  }
	  break;

	case AR_TYPE_STRUCTURE:
	case AR_TYPE_BANG:
	  {
	    ar_identifier_iterator *ii = ar_type_struct_get_fields (type);
	    while (ccl_iterator_has_more_elements (ii))
	      {
		ar_identifier *fname = ccl_iterator_next_element (ii);
		ar_identifier *slname = ar_identifier_add_prefix (fname, 
								  slot_name);
		ar_type *sltype = ar_type_struct_get_field (type, fname);
		
		s_expand_composite_slot (ctx, slname, sltype, result);
		
		ar_type_del_reference (sltype);
		ar_identifier_del_reference (slname);
		ar_identifier_del_reference (fname);
	      }
	    ccl_iterator_delete (ii);
	  }
	  break;

	default:
	  ccl_unreachable ();
	  break;
	}
    }
}

			/* --------------- */

ccl_list *
ar_context_expand_composite_slot (ar_context *ctx, ar_context_slot *slot,
				  ccl_list *result)
{
  ar_type *type = ar_context_slot_get_type (slot);
  ar_identifier *slot_name = ar_context_slot_get_name (slot);

  if (result == NULL)
    result = ccl_list_create ();
  s_expand_composite_slot (ctx, slot_name, type, result);

  ar_type_del_reference(type);
  ar_identifier_del_reference(slot_name);

  return result;
}

			/* --------------- */

ar_context_slot *
ar_context_slot_create(void)
{
  ar_context_slot *result = ccl_new(ar_context_slot);

  result->refcount = 1;
  result->index = -1;
  result->name = NULL;
  result->type = NULL;
  result->flags = 0;
  result->attr = NULL;
  result->value = NULL;

  return result;
}

			/* --------------- */

ar_context_slot *
ar_context_slot_create_filled(int index,
			      ar_identifier *name,
			      ar_type *type,
			      uint32_t flags,
			      ccl_list *attr,
			      struct ar_expr_st *value)
{
  ar_context_slot *result = ar_context_slot_create();

  ar_context_slot_set_index(result,index);
  ar_context_slot_set_name(result,name);
  ar_context_slot_set_type(result,type);
  ar_context_slot_set_flags(result,flags);
  ar_context_slot_set_attributes(result,attr);
  ar_context_slot_set_value(result,value);

  return result;
}

			/* --------------- */

ar_context_slot *
ar_context_slot_add_reference(ar_context_slot *slot)
{
  ccl_pre( slot != NULL );

  slot->refcount++;
  
  return slot;
}

			/* --------------- */

void
ar_context_slot_del_reference(ar_context_slot *slot)
{
  ccl_pre( slot != NULL ); ccl_pre( slot->refcount > 0 );

  if( --slot->refcount == 0 )
    {
      ccl_zdelete(ar_identifier_del_reference,slot->name);
      ccl_zdelete(ar_type_del_reference,slot->type);
      if( slot->attr != NULL )
	ccl_list_clear_and_delete(slot->attr,(ccl_delete_proc *) 
				  ar_identifier_del_reference);  
      ccl_zdelete(ar_expr_del_reference,slot->value);  
      ccl_delete(slot);
    }
}

			/* --------------- */

int
ar_context_slot_get_index(const ar_context_slot *slot)
{
  ccl_pre( slot != NULL ); 

  return slot->index;
}

			/* --------------- */

void
ar_context_slot_set_index(ar_context_slot *slot, int index)
{
  ccl_pre( slot != NULL ); 

  slot->index = index;
}

			/* --------------- */

ar_identifier *
ar_context_slot_get_name(const ar_context_slot *slot)
{
  ccl_pre( slot != NULL );

  if( slot->name != NULL )
    return ar_identifier_add_reference(slot->name);
  return NULL;
}

			/* --------------- */

void
ar_context_slot_set_name(ar_context_slot *slot, ar_identifier *id)
{
  ccl_pre( slot != NULL );

  if( slot->name != NULL )
    {
      ar_identifier_del_reference(slot->name);
      slot->name = NULL;
    }
  if( id != NULL )
    slot->name = ar_identifier_add_reference(id);
}

			/* --------------- */

ar_type *
ar_context_slot_get_type(const ar_context_slot *slot)
{
  ar_type *result = NULL;

  ccl_pre( slot != NULL );

  if( slot->type != NULL )
    result = ar_type_add_reference(slot->type);

  return result;
}


			/* --------------- */

void
ar_context_slot_set_type(ar_context_slot *slot, ar_type *type)
{
  ccl_pre( slot != NULL );

  if( slot->type != NULL )
    {
      ar_type_del_reference(slot->type);
      slot->type = NULL;
    }

  if( type != NULL )
    slot->type = ar_type_add_reference(type);
}

			/* --------------- */

uint32_t
ar_context_slot_get_flags(const ar_context_slot *slot)
{
  ccl_pre( slot != NULL );

  return slot->flags;
}

			/* --------------- */

void
ar_context_slot_set_flags(ar_context_slot *slot, uint32_t flags)
{
  ccl_pre( slot != NULL );

  slot->flags = flags;
}

			/* --------------- */

ccl_list *
ar_context_slot_get_attributes(const ar_context_slot *slot)
{
  ccl_list *result = NULL;

  ccl_pre( slot != NULL );

  if( slot->attr != NULL )
    result = ccl_list_deep_dup(slot->attr,(ccl_duplicate_func *)
			       ar_identifier_add_reference);
  return result;
}

			/* --------------- */

void
ar_context_slot_set_attributes(ar_context_slot *slot, ccl_list *attr)
{
  ccl_pre( slot != NULL );

  if( slot->attr != NULL )
    {
      ccl_list_clear_and_delete(slot->attr,
				(ccl_delete_proc *) 
				ar_identifier_del_reference);
      slot->attr = NULL;
    }
  if( attr != NULL )
    slot->attr = 
      ccl_list_deep_dup(attr,
			(ccl_duplicate_func *) ar_identifier_add_reference);
}

			/* --------------- */

struct ar_expr_st *
ar_context_slot_get_value(const ar_context_slot *slot)
{
  struct ar_expr_st *result = NULL;

  ccl_pre( slot != NULL );

  if( slot->value != NULL )
    result = ar_expr_add_reference(slot->value);

  return result;
}

			/* --------------- */

int
ar_context_slot_has_value(ar_context_slot *slot)
{
  ccl_pre( slot != NULL );

  return ( slot->value != NULL );
}

			/* --------------- */

void
ar_context_slot_set_value(ar_context_slot *slot, struct ar_expr_st *value)
{
  ccl_pre( slot != NULL );

  if( slot->value != NULL )
    {
      ar_expr_del_reference(slot->value);
      slot->value = NULL;
    }
  if( value != NULL )
    slot->value = ar_expr_add_reference(value);
}


			/* --------------- */

ar_context_slot *
ar_context_slot_rename(ar_context_slot *slot, ar_identifier *newname)
{
  ccl_pre( slot != NULL ); ccl_pre( newname != NULL );

  return ar_context_slot_create_filled(slot->index,newname,slot->type,
				       slot->flags,slot->attr,slot->value);
}

			/* --------------- */

ccl_list *
ar_context_slot_expand (int index, ar_identifier *name, ar_type *type,
			uint32_t flags, ccl_list *attr, 
			struct ar_expr_st *value, ccl_list *result)
{
  ar_context_slot *slot = 
    ar_context_slot_create_filled (-1, name, type, flags, attr, value);
  result = ar_context_slot_expand_slot (index, slot, result);
  ar_context_slot_del_reference (slot);

  return result;
}

			/* --------------- */

ccl_list *
ar_context_slot_expand_slot (int index, ar_context_slot *slot, ccl_list *result)
{
  if (result == NULL)
    result = ccl_list_create ();

  ccl_list_add (result, ar_context_slot_add_reference (slot));

  if (ar_type_is_scalar (slot->type))
    return result;

  if (ar_type_get_kind (slot->type) == AR_TYPE_ARRAY)
    {
      int i, width = ar_type_get_width (slot->type);
      ar_type *base_type = ar_type_array_get_base_type (slot->type);
      char *pos_str = NULL;
      size_t pos_str_size = 0;
      ar_expr *subvalue = NULL;

      for (i = 0; i < width; i++)
	{
	  ar_identifier *slot_id;
	  
	  ccl_string_format (&pos_str, &pos_str_size, "[%d]", i);
	  slot_id = ar_identifier_rename_with_suffix (slot->name, pos_str);

	  if (slot->value != NULL)
	    {
	      ar_constant *pos = ar_constant_crt_integer (i);
	      ar_expr *epos = ar_expr_crt_constant (pos);
	      subvalue = 
		ar_expr_crt_binary (AR_EXPR_ARRAY_MEMBER, slot->value, epos);
	      ar_constant_del_reference (pos);
	      ar_expr_del_reference (epos);
	    }

	  ar_context_slot_expand (index, slot_id, base_type, slot->flags, 
				  slot->attr, subvalue, result);
	  ar_identifier_del_reference (slot_id);
	  ccl_zdelete (ar_expr_del_reference, subvalue);
	}
      ccl_string_delete (pos_str);
      ar_type_del_reference (base_type);
    }
  else if (ar_type_get_kind (slot->type) == AR_TYPE_STRUCTURE)
    {
      ar_expr *subvalue = NULL;
      ar_identifier_iterator *fields;

      fields = ar_type_struct_get_fields (slot->type);

      while (ccl_iterator_has_more_elements (fields))
	{
	  ar_identifier *field_name = ccl_iterator_next_element (fields);
	  ar_identifier *field_id = 
	    ar_identifier_add_prefix (field_name, slot->name);
	  ar_type *field_type = 
	    ar_type_struct_get_field (slot->type, field_name);
	  
	  if (slot->value != NULL)
	    subvalue = ar_expr_crt_struct_member (slot->value, field_name);

	  ar_context_slot_expand (index, field_id, field_type, slot->flags, 
				  slot->attr, subvalue, result);

	  ar_type_del_reference (field_type);
	  ar_identifier_del_reference (field_id);
	  ar_identifier_del_reference (field_name);
	  ccl_zdelete (ar_expr_del_reference, subvalue);
	}
      ccl_iterator_delete (fields);
    }
  else if (ar_type_get_kind (slot->type) == AR_TYPE_BANG)
    {
      ar_identifier_iterator *fields;
      ar_node *model = ar_type_bang_get_node (slot->type);
      ar_context *ctx = ar_node_get_context (model);

      ccl_assert (slot->value == NULL);

      fields = ar_type_struct_get_fields (slot->type);
      while (ccl_iterator_has_more_elements (fields))
	{
	  ar_identifier *field_name = ccl_iterator_next_element (fields);
	  ar_identifier *field_id = 
	    ar_identifier_add_prefix (field_name, slot->name);
	  ar_type *field_type = ar_type_struct_get_field (slot->type, 
							  field_name);
	  ar_context_slot *slot = ar_context_get_slot (ctx, field_name);
	  uint32_t flags = ar_context_slot_get_flags (slot);
	  ccl_list *attr = ar_context_slot_get_attributes (slot);
	  ar_expr *value = ar_context_slot_get_value (slot);

	  ccl_assert (slot != NULL);
	  
	  ar_context_slot_expand (index, field_id, field_type, flags, attr, 
				  value, result); 
	  ar_type_del_reference (field_type);
	  ar_identifier_del_reference (field_id);
	  ar_identifier_del_reference (field_name); 
	  if (attr != NULL)
	    ccl_list_clear_and_delete (attr, (ccl_delete_proc *)
				       ar_identifier_del_reference);
	  ccl_zdelete (ar_expr_del_reference, value);
	  ar_context_slot_del_reference (slot);
	}
      ccl_iterator_delete (fields);
      ar_node_del_reference (model);
      ar_context_del_reference (ctx);
    }

  return result;
}

			/* --------------- */

static void
s_add_object(ar_context *ctx, table_index idx, ar_identifier *id, void *obj)
{
  ar_idtable_put(ctx->tables[idx],id,obj);
}

			/* --------------- */

static int
s_has_object(ar_context *ctx, table_index idx, ar_identifier *id)
{
  int result = ar_idtable_has(ctx->tables[idx],id);

  if( ! result && ctx->parent != NULL )
    result = s_has_object(ctx->parent,idx,id);

  return result;
}

			/* --------------- */

static void *
s_get_object(ar_context *ctx, table_index idx, const ar_identifier *id)
{
  void *result = ar_idtable_get(ctx->tables[idx],id);

  while (result == NULL && ctx->parent != NULL)
    {
      ctx = ctx->parent;
      result = ar_idtable_get(ctx->tables[idx],id);
    }

  return result;
}

			/* --------------- */

static void 
s_remove_object(ar_context *ctx, table_index idx, ar_identifier *id)
{
  ar_idtable_remove(ctx->tables[idx],id);
}

			/* --------------- */

typedef struct linked_iterator_st {
  ar_identifier_iterator super;
  ar_identifier_iterator *current;
  table_index idx;
  ar_context *ctx;
} linked_iterator;

			/* --------------- */

static int 
s_linked_iterator_has_more_elements(const ar_identifier_iterator *i)
{
  linked_iterator *lit = (linked_iterator *)i;

  ccl_pre( i != NULL );

  return lit->current != NULL && ccl_iterator_has_more_elements(lit->current);
}

			/* --------------- */

static ar_identifier *
s_linked_iterator_next_element(ar_identifier_iterator *i)
{
  ar_identifier *result;
  linked_iterator *lit = (linked_iterator *)i;

  ccl_pre( i != NULL );

  result = ccl_iterator_next_element(lit->current);

  if( ! ccl_iterator_has_more_elements(lit->current) )
    {
      ccl_iterator_delete(lit->current); 

      if( lit->ctx->parent != NULL )
	{	  
	  ar_context *pctx = ar_context_add_reference(lit->ctx->parent);
	  ar_context_del_reference(lit->ctx);
	  lit->ctx = pctx;
	  lit->current = ar_idtable_get_keys(pctx->tables[lit->idx]);
	}
      else
	{
	  ar_context_del_reference(lit->ctx);
	  lit->ctx = NULL;
	  lit->current = NULL;
	}
    }

  return result;
}

			/* --------------- */

static void
s_linked_iterator_delete_iterator(ar_identifier_iterator *i)
{
  linked_iterator *lit = (linked_iterator *)i;

  if( lit->current != NULL )
    ccl_iterator_delete(lit->current);
  if( lit->ctx != NULL )
    ar_context_del_reference(lit->ctx);
  ccl_delete(lit);
}

			/* --------------- */

static ccl_list *
s_get_ids(ar_context *ctx, table_index idx)
{
  ccl_hash *s = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_list *result = ccl_list_create();
  linked_iterator *li = ccl_new(linked_iterator);
  ar_identifier_iterator *i = (ar_identifier_iterator *)li;

  li->super.has_more_elements = s_linked_iterator_has_more_elements;
  li->super.next_element = s_linked_iterator_next_element;
  li->super.delete_iterator = s_linked_iterator_delete_iterator;
  li->current = ar_idtable_get_keys(ctx->tables[idx]);
  li->ctx = ar_context_add_reference(ctx);
  li->idx = idx;

  while( ccl_iterator_has_more_elements(i) )
    {
      ar_identifier *id = ccl_iterator_next_element(i);

      if (! ccl_hash_find (s, id))
	{
	  ccl_hash_insert (s, id);
	  ccl_list_add(result,ar_identifier_add_reference(id));
	}
      ar_identifier_del_reference(id);
    }
  ccl_iterator_delete(i);
  ccl_hash_delete (s);

  return result;
}

			/* --------------- */

static ccl_list *
s_get_slots(ar_context *ctx, uint32_t mask)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create();
  ccl_list *tmp = s_get_ids (ctx,SLOTS);

  ccl_list_sort (tmp, (ccl_compare_func *) ar_identifier_lex_compare);

  for(p = FIRST(tmp); p; p = CDR(p))
    {
      ar_identifier *id = CAR(p);
      ar_context_slot *sl = s_get_object(ctx,SLOTS,id);
      ccl_assert( sl != NULL );
      if( (sl->flags & mask) != 0 )
	ccl_list_add(result,id);
      else
	ar_identifier_del_reference(id);
      ar_context_slot_del_reference(sl);
    }
  ccl_list_delete(tmp);

  return result;
}

			/* --------------- */

static ccl_list *
s_get_local_slots(ar_context *ctx, uint32_t mask)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create();
  ccl_list *tmp = s_get_ids (ctx, SLOTS);

  ccl_list_sort (tmp, (ccl_compare_func *) ar_identifier_lex_compare);

  for(p = FIRST(tmp); p; p = CDR(p))
    {
      ar_identifier *id = CAR(p);
      ar_context_slot *sl = s_get_object(ctx,SLOTS,id);
      ccl_assert( sl != NULL );
      if( (sl->flags & mask) != 0 && 
	  ar_idtable_has (ctx->tables[SLOTS], id))
	ccl_list_add(result,id);
      else
	ar_identifier_del_reference(id);
      ar_context_slot_del_reference(sl);
    }
  ccl_list_delete(tmp);

  return result;
}

			/* --------------- */

