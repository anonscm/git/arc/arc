/*
 * ar-model.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-hash.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-assert.h>
#include "ar-attributes.h"
#include "ar-event.h"
#include "ar-model.h"


static ar_idtable *TABLE_OF_NODES;

			/* --------------- */

ar_context *AR_MODEL_CONTEXT = NULL;

			/* --------------- */

void
ar_model_init(void)
{
  TABLE_OF_NODES = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_node_add_reference,
		      (ccl_delete_proc *)ar_node_del_reference);

  AR_MODEL_CONTEXT = ar_context_create_top();

  ar_type_init();
  ar_constant_init();
  ar_expr_init();
  ar_attributes_init();
  ar_event_init();
}

			/* --------------- */

void
ar_model_terminate(void)
{
  ar_idtable_del_reference(TABLE_OF_NODES);
  ar_event_terminate();
  ar_attributes_terminate();
  ar_expr_terminate();
  ar_constant_terminate();
  ar_type_terminate();
  ar_context_del_reference(AR_MODEL_CONTEXT);
}

			/* --------------- */

void
ar_model_add_parameter(ar_identifier *id, ar_type *type, ar_expr *value)
{
  ccl_pair *p;
  ccl_list *slots = ar_context_slot_expand(-1,id,type,
					   AR_SLOT_FLAG_PARAMETER|
					   AR_SLOT_FLAG_DFLT_PARAMETER_VIS,
					   NULL,
					   value, NULL);
  for(p = FIRST(slots); p; p = CDR(p))
    {
      ar_context_slot *sl = (ar_context_slot *)CAR(p);
      ar_identifier *name = ar_context_slot_get_name(CAR(p));
      ar_context_add_slot(AR_MODEL_CONTEXT,name,sl);
      ar_identifier_del_reference(name);
      ar_context_slot_del_reference(sl);
    }
  ccl_list_delete(slots);
}

			/* --------------- */

int
ar_model_has_parameter(ar_identifier *id)
{
  ar_context_slot *sl = ar_model_get_parameter(id);
  int result = (sl != NULL);
  if( result )
    ar_context_slot_del_reference(sl);

  return result;
}

			/* --------------- */

ar_context_slot *
ar_model_get_parameter(ar_identifier *id)
{
  ar_context_slot *sl = ar_context_get_slot(AR_MODEL_CONTEXT,id);
  
  if( sl != NULL )
    {
      if( (ar_context_slot_get_flags(sl) & AR_SLOT_FLAG_PARAMETER) != 0 )
	return sl;
      ar_context_slot_del_reference(sl);
    }
  return NULL;
}

			/* --------------- */

ar_expr *
ar_model_get_parameter_value(ar_identifier *id)
{
  ar_context_slot *sl = ar_model_get_parameter(id);
  ar_expr *result = ar_context_slot_get_value(sl);

  ar_context_slot_del_reference(sl);

  return result;
}

			/* --------------- */

void
ar_model_set_parameter_value(ar_identifier *id, ar_expr *value)
{
  ar_context_slot *sl = ar_model_get_parameter(id);
  
  ccl_assert( sl != NULL );

  ar_context_slot_set_value(sl,value);
}

			/* --------------- */

ccl_list *
ar_model_get_parameters(void)
{
  return ar_context_get_slots(AR_MODEL_CONTEXT,AR_SLOT_FLAG_PARAMETER);
}

			/* --------------- */

void
ar_model_remove_parameter(ar_identifier *id)
{
  ar_context_remove_slot(AR_MODEL_CONTEXT,id);
}


void
ar_model_set_type(ar_identifier *id, ar_type *type)
{
  ar_context_add_type(AR_MODEL_CONTEXT,id,type);
}

			/* --------------- */

ar_type *
ar_model_get_type(ar_identifier *id)
{
  return ar_context_get_type(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

int
ar_model_has_type(ar_identifier *id)
{
  return ar_context_has_type(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

ccl_list *
ar_model_get_types(void)
{
  return ar_context_get_types(AR_MODEL_CONTEXT);
}

			/* --------------- */

void
ar_model_remove_type(ar_identifier *id)
{
  ar_context_remove_type(AR_MODEL_CONTEXT,id);
}

			/* --------------- */
void
ar_model_set_node(ar_identifier *id, ar_node *node)
{
  ar_idtable_put(TABLE_OF_NODES,id,node);
}

			/* --------------- */

ar_node *
ar_model_get_node(ar_identifier *id)
{
  return (ar_node *)ar_idtable_get(TABLE_OF_NODES,id);
}

			/* --------------- */

int
ar_model_has_node(ar_identifier *id)
{
  return ar_idtable_has(TABLE_OF_NODES,id);
}

			/* --------------- */

ccl_list *
ar_model_get_nodes(void)
{
  return ar_idtable_get_list_of_keys(TABLE_OF_NODES);
}

			/* --------------- */

void
ar_model_remove_node(ar_identifier *id)
{
  ar_idtable_remove(TABLE_OF_NODES,id);
}

			/* --------------- */

void
ar_model_set_signature(ar_identifier *id, ar_signature *signature)
{
  ar_context_add_signature(AR_MODEL_CONTEXT,id,signature);
}

			/* --------------- */

ar_signature *
ar_model_get_signature(ar_identifier *id)
{
  return ar_context_get_signature(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

int
ar_model_has_signature(ar_identifier *id)
{
  return ar_context_has_signature(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

ccl_list *
ar_model_get_signatures(void)
{
  return ar_context_get_signatures(AR_MODEL_CONTEXT);
}

			/* --------------- */

void
ar_model_remove_signature(ar_identifier *id)
{
  ar_context_remove_signature(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

void
ar_model_set_law_parameter(ar_identifier *id, sas_law_param *law_parameter)
{
  ar_context_add_law_parameter(AR_MODEL_CONTEXT,id,law_parameter);
}

			/* --------------- */

sas_law_param *
ar_model_get_law_parameter(ar_identifier *id)
{
  return ar_context_get_law_parameter(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

int
ar_model_has_law_parameter(ar_identifier *id)
{
  return ar_context_has_law_parameter(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

ccl_list *
ar_model_get_law_parameters(void)
{
  return ar_context_get_law_parameters(AR_MODEL_CONTEXT);
}

			/* --------------- */

void
ar_model_remove_law_parameter(ar_identifier *id)
{
  ar_context_remove_law_parameter(AR_MODEL_CONTEXT,id);
}

			/* --------------- */

