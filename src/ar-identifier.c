/*
 * ar-identifier.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include "ar-identifier.h"

struct ar_identifier_st {
  int refcount;
  ccl_list *path;
  uint32_t hval;
};

struct ar_idtable_st {
  int refcount;
  ccl_list *ordered_keys;
  ccl_hash *table;
  ccl_delete_proc *del;
  ccl_duplicate_func *dup;
};

			/* --------------- */

static unsigned int
s_list_hash (const void *l);

static int
s_list_cmp (const void *l1, const void *l2);

static void
s_identifier_destroy (void *id);

static ar_identifier *
s_find_or_add_identifier(ccl_list *path);

static int
s_cmpustr (const char *s1, const char *s2)
{
  /* return ((intptr_t) s1) - ((intptr_t) s2); */
  return ccl_string_cmp (s1, s2);
}

			/* --------------- */

static ccl_hash *IDENTIFIERS = NULL;

			/* --------------- */

ar_identifier *AR_EMPTY_ID = NULL;

			/* --------------- */

void
ar_identifier_init(void)
{
  ccl_list *e;

  IDENTIFIERS = ccl_hash_create (s_list_hash, s_list_cmp, NULL, 
				 s_identifier_destroy);
  e = ccl_list_create();
  AR_EMPTY_ID = s_find_or_add_identifier (e);
}

			/* --------------- */

void
ar_identifier_terminate(void)
{
  ar_identifier_del_reference(AR_EMPTY_ID);  
  ccl_hash_delete (IDENTIFIERS);
}

			/* --------------- */

ar_identifier *
ar_identifier_create(const char *name)
{
  ar_symbol s = ccl_string_make_unique(name);
  ar_identifier *result = ar_identifier_create_simple(s);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_create_from_path (const ccl_list *path)
{
  ccl_pre( path != NULL ); 

  return s_find_or_add_identifier (ccl_list_dup (path));
}

			/* --------------- */

ar_identifier *
ar_identifier_create_simple(ar_symbol name)
{
  ar_identifier *result;
  ccl_list *path = ccl_list_create();

  ccl_pre( name != NULL );

  ccl_list_add(path,name);
  result = s_find_or_add_identifier (path);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_create_numbered (const char *pref, int n)
{
  char *id = ccl_string_format_new ("%s%d", pref, n);
  ar_identifier *result = ar_identifier_create (id);
  ccl_string_delete (id);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_create_numbered_auto (const char *pref, int *p_n)
{
  return ar_identifier_create_numbered (pref, (*p_n)++);
}

			/* --------------- */

ar_identifier *
ar_identifier_add_reference(ar_identifier *id)
{
  ccl_pre( id != NULL );

  id->refcount++;
    
  return id;
}

			/* --------------- */

void
ar_identifier_del_reference(ar_identifier *id)
{
  ccl_pre (id != NULL); 
  ccl_pre (id->refcount > 0);

  id->refcount--;
  if (id->refcount == 0)
    { 
      ccl_assert (ccl_hash_find (IDENTIFIERS, id->path));
      ccl_hash_find (IDENTIFIERS, id->path);
      ccl_hash_remove (IDENTIFIERS);
    }
}

			/* --------------- */

ar_identifier *
ar_identifier_add_prefix(const ar_identifier *id, const ar_identifier *prefix)
{
  ar_identifier *result;
  ccl_list *path;

  ccl_pre( id != NULL ); ccl_pre( prefix != NULL );

  path = ccl_list_dup(prefix->path);
  ccl_list_append(path,id->path);
  result = s_find_or_add_identifier (path);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_rename_with_prefix(const ar_identifier *id, const char *prefix)
{
  ar_identifier *result;
  ccl_list *path;
  ccl_pair *p;
  ar_symbol n;
  char *tmp;

  ccl_pre( id != NULL ); ccl_pre( prefix != NULL );
  
  path = ccl_list_dup(id->path);
  for(p = FIRST(path); CDR(p) != NULL; p = CDR(p))
    /* empty */;

  tmp = ccl_string_format_new("%s%s",prefix,(char *)CAR(p));
  n = ccl_string_make_unique(tmp);
  ccl_string_delete (tmp);

  CAR(p) = n;

  result = s_find_or_add_identifier (path);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_rename_with_suffix(const ar_identifier *id, const char *suffix)
{
  ar_identifier *result;
  ccl_list *path;
  ccl_pair *p;
  ar_symbol n;
  char *tmp;

  ccl_pre( id != NULL ); ccl_pre( suffix != NULL );
  
  path = ccl_list_dup(id->path);
  for(p = FIRST(path); CDR(p) != NULL; p = CDR(p))
    /* empty */;

  tmp = ccl_string_format_new("%s%s",(char *)CAR(p),suffix);
  n = ccl_string_make_unique(tmp);
  ccl_string_delete(tmp);

  CAR(p) = n;

  result = s_find_or_add_identifier (path);

  return result;
}

			/* --------------- */

int
ar_identifier_is_simple(const ar_identifier *id)
{
  ccl_pre( id != NULL );

  return ccl_list_get_size(id->path) == 1;
}

			/* --------------- */

ar_identifier *
ar_identifier_get_name(const ar_identifier *id)
{
  ar_symbol name;
  ccl_list *tmp;
  ar_identifier *result;

  ccl_pre( id != NULL ); ccl_pre( ar_identifier_get_path_length(id)> 0 );

  name = (ar_symbol)ccl_list_get_at(id->path,ccl_list_get_size(id->path)-1);
  tmp  = ccl_list_create();
  ccl_list_add(tmp,name);
  result = s_find_or_add_identifier (tmp);

  return result;
}

			/* --------------- */

int
ar_identifier_has_name (const ar_identifier *id, const ar_identifier *name)
{
  ccl_pair *p;

  ccl_pre (id != NULL);
  ccl_pre (ar_identifier_get_path_length (name) == 1);

  p = FIRST (id->path);
  if (p == NULL)
    return 0;

  while (CDR(p))
    p = CDR (p);

  return s_cmpustr (CAR (p), CAR (FIRST (name->path))) == 0;
}

			/* --------------- */

ar_identifier *
ar_identifier_get_prefix(const ar_identifier *id)
{
  ar_identifier *result;
  ccl_list *path = ccl_list_create();
  ccl_pair *p;

  ccl_pre( id != NULL ); ccl_pre( ar_identifier_get_path_length(id) >= 1 );

  for(p = FIRST(id->path); CDR(p) != NULL; p = CDR(p))
    ccl_list_add(path,CAR(p));
  result = s_find_or_add_identifier (path);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_get_prefix_of_length (const ar_identifier *id, int length)
{
  ccl_pair *p;
  ccl_list *path = ccl_list_create ();
  ar_identifier *result;

  ccl_pre (id != NULL);
  ccl_pre (0 <= length);

  p = FIRST (id->path);
  while (length-- && p)
    {
      ccl_list_add (path, CAR (p));
      p = CDR (p);
    }
  result = s_find_or_add_identifier (path);

  return result;
}

ar_identifier *
ar_identifier_get_name_and_prefix(const ar_identifier *id, 
				  ar_identifier **ppref)
{
  ar_identifier *result;
  ccl_list *path = ccl_list_create();
  ccl_pair *p;

  ccl_pre( id != NULL ); ccl_pre( ar_identifier_get_path_length(id) >= 1 );

  for(p = FIRST(id->path); CDR(p) != NULL; p = CDR(p))
    ccl_list_add(path,CAR(p));
  *ppref = s_find_or_add_identifier (path);
  result = ar_identifier_create_simple(CAR(p));

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_get_first(const ar_identifier *id, ar_identifier **remain)
{
  ar_identifier *result;

  ccl_pre( id != NULL ); ccl_pre( ar_identifier_get_path_length(id)> 0 );

  if( remain == NULL ) 
    result = ar_identifier_create_simple(CAR(FIRST(id->path)));
  else
    {
      ccl_list *tmp  = ccl_list_dup(id->path);
      ar_symbol name = (ar_symbol)ccl_list_take_first(tmp);
      if( remain != NULL )
	*remain = s_find_or_add_identifier (tmp);

      tmp  = ccl_list_create();
      ccl_list_add(tmp,name);
      result = s_find_or_add_identifier (tmp);
    }

  return result;
}

			/* --------------- */

ccl_list *
ar_identifier_get_path(ar_identifier *id)
{
  ccl_pre( id != NULL );

  return id->path;
}

			/* --------------- */

int
ar_identifier_get_path_length(const ar_identifier *id)
{
  ccl_pre( id != NULL );

  return ccl_list_get_size(id->path);
}

			/* --------------- */

int 
ar_identifier_is_container_of(const ar_identifier *id, 
			      const ar_identifier *other)
{
  ccl_pair *idp;
  ccl_pair *op;

  ccl_pre( id != NULL ); ccl_pre( other != NULL );

  if( ccl_list_get_size(id->path) > ccl_list_get_size(other->path) )
    return 0;
  
  idp = FIRST(id->path);
  op  = FIRST(other->path);
  while (idp != NULL && s_cmpustr (CAR(idp),CAR(op)) == 0)
    {
      idp = CDR(idp);
      op = CDR(op);
    }

  return idp == NULL;
}

			/* --------------- */

int 
ar_identifier_is_prefix_of(const ar_identifier *id, const ar_identifier *other)
{
  ccl_pair *idp;
  ccl_pair *op;

  ccl_pre( id != NULL ); ccl_pre( other != NULL );

  if( ccl_list_get_size(id->path) != ccl_list_get_size(other->path)-1 )
    return 0;

  idp = FIRST(id->path);
  op  = FIRST(other->path);
  for(; idp != NULL; idp = CDR(idp), op = CDR(op))
    {
      ccl_assert( op != NULL );

      if (s_cmpustr ((ar_symbol)CAR(idp),(ar_symbol)CAR(op)) != 0)
	return 0;
    }

  return 1;
}

			/* --------------- */

int
ar_identifier_compare(const ar_identifier *id1, const ar_identifier *id2)
{
  int difflen;

  ccl_pre( id1 != NULL ); ccl_pre( id2 != NULL );

  difflen = ar_identifier_get_path_length(id1)
    -       ar_identifier_get_path_length(id2);

  if( difflen == 0 )
    return ccl_list_compare(id1->path,id2->path,
			    (ccl_compare_func *) s_cmpustr);
  return difflen;
}

			/* --------------- */

int
ar_identifier_lex_compare(const ar_identifier *id1, const ar_identifier *id2)
{
  ccl_pre( id1 != NULL ); ccl_pre( id2 != NULL );

  return ccl_list_compare (id1->path, id2->path,
			   (ccl_compare_func *) s_cmpustr);
}

			/* --------------- */

uint32_t
ar_identifier_hash(const ar_identifier *id)
{
  ccl_pre( id != NULL );

  return id->hval;
}

			/* --------------- */

int
ar_need_quotes (const char *s)
{
  if( !(('a' <= *s && *s <= 'z') || ('A' <= *s && *s <= 'Z') || *s == '_') )
    return 1;

  while( *++s )
    {
      if( !( ('a' <= *s && *s <= 'z') || ('A' <= *s && *s <= 'Z') || 
	     ('0' <= *s && *s <= '9') || (*s == '_') ) )
	return 1;
    }	

  return 0;
}

			/* --------------- */

void
ar_identifier_log (ccl_log_type log, const ar_identifier *id)
{
  const char *fmt = "%s.";
  ccl_pair *p;

  ccl_pre (id != NULL);

  for(p = FIRST (id->path); p; p = CDR (p))
    {
      ccl_ustring c = (ccl_ustring) CAR (p);

      if (CDR(p) == NULL)
	fmt = "%s";
      ccl_log (log, fmt, c);
    }
}

			/* --------------- */

void
ar_identifier_log_quote(ccl_log_type log, const ar_identifier *id)
{
  ar_identifier_log_quote_gen (log, id, ".", "''");
}

			/* --------------- */

void
ar_identifier_log_global_quote(ccl_log_type log, const ar_identifier *id)
{
  ar_identifier_log_global_quote_gen (log, id, ".", "''");
}

			/* --------------- */

void
ar_identifier_log_quote_gen (ccl_log_type log, const ar_identifier *id,
			     const char *separator, 
			     const char *quotes)
{
  ccl_pair *p;

  ccl_pre (id != NULL);
  ccl_pre (quotes != NULL);

  for (p = FIRST (id->path); p; p = CDR (p))
    {
      ccl_ustring c = (ccl_ustring) CAR (p);

      if (ar_need_quotes (c)) 
	ccl_log (log, "%c%s%c", quotes[0], c, quotes[1]);
      else
	ccl_log (log, "%s", c);

      if (CDR(p) != NULL)
	ccl_log (log, separator);
    }
}

			/* --------------- */

static char *
s_identifier_to_string (const ar_identifier *id, const char *separator)
{
  ccl_pair *p;
  const char *fmt = "%s%s";
  char *result = ccl_string_dup ("");

  ccl_pre (id != NULL);
  ccl_pre (separator != NULL);

  for (p = FIRST (id->path); p; p = CDR (p))
    {
      ccl_ustring c = (ccl_ustring) CAR (p);

      if (CDR(p) == NULL)
	fmt = "%s";
      ccl_string_format_append (&result, fmt, c, separator);
    }

  return result;
}

			/* --------------- */

void
ar_identifier_log_global_quote_gen (ccl_log_type log, const ar_identifier *id,
				    const char *separator, 
				    const char *quotes)
{
  char *s;

  ccl_pre (id != NULL);
  ccl_pre (quotes != NULL);

  s = s_identifier_to_string (id, separator);
  if (ar_need_quotes (s))
    ccl_log (log, "%c%s%c", quotes[0], s, quotes[1]);
  else
    ccl_log (log, "%s", s);
  ccl_string_delete (s);
}

			/* --------------- */

char *
ar_identifier_to_string(const ar_identifier *id)
{
  return s_identifier_to_string (id, ".");
}

			/* --------------- */

ar_identifier *
ar_identifier_collapse(const ar_identifier *id)
{
  char *tmp = ar_identifier_to_string(id);
  ccl_ustring sym = ccl_string_make_unique(tmp);
  ar_identifier *result = ar_identifier_create_simple(sym);
  ccl_string_delete(tmp);

  return result;
}

			/* --------------- */

ar_identifier *
ar_identifier_collapse_suffix(const ar_identifier *id)
{
  ar_identifier *suffix, *first, *csuffix, *result;

  ccl_assert( ar_identifier_get_path_length(id) > 1 );

  first = ar_identifier_get_first(id,&suffix); 
  csuffix = ar_identifier_collapse(suffix);
  result = ar_identifier_add_prefix(csuffix,first);
  ar_identifier_del_reference(first);
  ar_identifier_del_reference(suffix);
  ar_identifier_del_reference(csuffix);

  return result;
}

			/* --------------- */

void
ar_identifier_write (ar_identifier *id, FILE *out, 
		     ccl_serializer_status *p_err)
{
  ccl_pair *p;

  if (*p_err != CCL_SERIALIZER_OK)
    return;
  ccl_serializer_write_uint32 (ccl_list_get_size (id->path), out, p_err);
  for (p = FIRST (id->path); p && *p_err == CCL_SERIALIZER_OK; p = CDR (p))
    ccl_serializer_write_string (CAR (p), out, p_err);
}

			/* --------------- */

void
ar_identifier_read (ar_identifier **p_id, FILE *in, 
		    ccl_serializer_status *p_err)
{
  uint32_t len;
  ccl_list *path;

  ccl_serializer_read_uint32 (&len, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    return;

  path = ccl_list_create ();
  while (*p_err == CCL_SERIALIZER_OK && len--)
    {
      char *us;
      char *s = NULL;

      ccl_serializer_read_string (&s, in, p_err);
      if (*p_err == CCL_SERIALIZER_OK)
	{
	  us = ccl_string_make_unique (s);
	  ccl_list_add (path, us);
	}
      ccl_zdelete (ccl_string_delete, s);
    }
  if (*p_err == CCL_SERIALIZER_OK)
    *p_id = s_find_or_add_identifier(path);
  else
    ccl_list_delete (path);
}

			/* --------------- */

static ar_identifier *
s_find_or_add_identifier (ccl_list *path)
{
  ar_identifier *i;

  if (ccl_hash_find (IDENTIFIERS, path))
    {
      i = ccl_hash_get (IDENTIFIERS);
      ccl_list_delete (path);
    }
  else
    {
      i = ccl_new (struct ar_identifier_st);
      i->refcount = 1;
      i->path = path;
      i->hval = ccl_list_hash (path);
      ccl_hash_find (IDENTIFIERS, i->path);
      ccl_hash_insert (IDENTIFIERS, i);
    }

  return ar_identifier_add_reference (i);
}

			/* --------------- */

ar_idtable *
ar_idtable_create (int ordered, ccl_duplicate_func dup, ccl_delete_proc del)
{
  ar_idtable *result = ccl_new(ar_idtable);

  result->refcount = 1;
  if (ordered)
    result->ordered_keys = ccl_list_create ();
  else
    result->ordered_keys = NULL;
  result->table = 
    ccl_hash_create(NULL,NULL,(ccl_delete_proc *)ar_identifier_del_reference,
		    del);
  result->dup = dup;
  result->del = del;

  return result;
}

			/* --------------- */

ar_idtable *
ar_idtable_clone(ar_idtable *idt)
{
  ar_idtable *result =
    ar_idtable_create (idt->ordered_keys != NULL, idt->dup, idt->del);
  
  if (idt->ordered_keys != NULL)
    {
      ccl_pair *p;
      for (p = FIRST (idt->ordered_keys); p; p = CDR (p))
	{
	  void *object = ccl_hash_get_with_key (idt->table, CAR (p));
	  ar_idtable_put (result, CAR (p), object);
	}
    }
  else
    {
      ccl_hash_entry_iterator *it = ccl_hash_get_entries (idt->table);
      
      while (ccl_iterator_has_more_elements (it))
	{
	  ccl_hash_entry e = ccl_iterator_next_element(it);
	  ar_idtable_put (result, e.key, e.object);
	}
      ccl_iterator_delete(it);
    }
  
  return result;
}

			/* --------------- */

ar_idtable *
ar_idtable_add_reference(ar_idtable *idt)
{
  ccl_pre( idt != NULL );

  idt->refcount++;

  return idt;
}

			/* --------------- */

void
ar_idtable_del_reference(ar_idtable *idt)
{
  ccl_pre( idt != NULL ); ccl_pre( idt->refcount > 0 );

  if( --idt->refcount == 0 )
    {
      ccl_zdelete (ccl_list_delete, idt->ordered_keys);
      ccl_hash_delete(idt->table);
      ccl_delete(idt);
    }
}

			/* --------------- */

void
ar_idtable_put (ar_idtable *idt, ar_identifier *id, void *obj)
{
  ccl_pre( idt != NULL );
  ccl_pre( id != NULL );

  if (! ccl_hash_find (idt->table, id))
    {
      ar_identifier_add_reference (id);
      if (idt->ordered_keys != NULL)
	ccl_list_add (idt->ordered_keys, id);
    }
  ccl_hash_insert (idt->table, idt->dup ? idt->dup (obj) : obj);
}

			/* --------------- */

void
ar_idtable_remove(ar_idtable *idt, ar_identifier *id)
{
  ccl_pre (idt != NULL); ccl_pre (id != NULL);

  if (ccl_hash_find (idt->table, id))
    {
      ccl_hash_remove (idt->table);
      if (idt->ordered_keys != NULL)
	ccl_list_remove (idt->ordered_keys, id);	    
    }
}

			/* --------------- */

int
ar_idtable_has(const ar_idtable *idt, const ar_identifier *id)
{
  ccl_pre( idt != NULL ); ccl_pre( id != NULL );

  return ccl_hash_has (idt->table,id);
}

			/* --------------- */

int
ar_idtable_has_value(const ar_idtable *idt, ccl_equals_func equ, void *val)
{
  int result = 0;
  ccl_hash_entry_iterator *it = ccl_hash_get_entries(idt->table);

  if( equ == NULL )
    {
      while( ccl_iterator_has_more_elements(it) && ! result )
	{
	  ccl_hash_entry e = ccl_iterator_next_element(it);
	  
	  result = e.object == val;
	}
    }
  else
    {
      while( ccl_iterator_has_more_elements(it) && ! result )
	{
	  ccl_hash_entry e = ccl_iterator_next_element(it);
	  
	  result = equ(e.object,val);
	}
    }
  ccl_iterator_delete(it);

  return result;
}

			/* --------------- */

void *
ar_idtable_get (const ar_idtable *idt, const ar_identifier *id)
{
  void *result = NULL;

  ccl_pre (idt != NULL); 
  ccl_pre (id != NULL);

  result = ccl_hash_get_with_key (idt->table,id);
  if (result != NULL && idt->dup != NULL)
    result = idt->dup (result);


  return result;
}

			/* --------------- */

typedef struct id_iterator_st {
  ar_identifier_iterator super;  
  ccl_pointer_iterator *iterator;  
} id_iterator;

			/* --------------- */

static int
s_id_iterator_has_more_elements(const ar_identifier_iterator *i)
{
  return ccl_iterator_has_more_elements(((id_iterator*)i)->iterator);
}

			/* --------------- */
static ar_identifier *
s_id_iterator_next_element(ar_identifier_iterator *i)
{
  ar_identifier *result = (ar_identifier *)
    ccl_iterator_next_element(((id_iterator*)i)->iterator);
  

  return ar_identifier_add_reference(result);
}

			/* --------------- */

static void
s_id_iterator_delete_iterator(ar_identifier_iterator *i)
{
  ccl_iterator_delete(((id_iterator*)i)->iterator);
  ccl_delete(i);
}

			/* --------------- */

ar_identifier_iterator *
ar_idtable_get_keys(const ar_idtable *idt)
{
  ar_identifier_iterator *result;
  
  if (idt->ordered_keys != NULL)
    {
      result = (ar_identifier_iterator *)
	ccl_list_get_iterator (idt->ordered_keys, (ccl_duplicate_func *)
			       ar_identifier_add_reference, NULL);
    }
  else
    {
      id_iterator *i = ccl_new(id_iterator);
      
      i->super.has_more_elements = s_id_iterator_has_more_elements;
      i->super.next_element = s_id_iterator_next_element;
      i->super.delete_iterator = s_id_iterator_delete_iterator;
      i->iterator = ccl_hash_get_keys(idt->table);
      result = (ar_identifier_iterator *) i;
    }
  
  return result;
}

			/* --------------- */

ccl_list *
ar_idtable_get_list_of_keys(const ar_idtable *idt)
{
  ccl_list *result = ccl_list_create();
  ar_identifier_iterator *it = ar_idtable_get_keys(idt);

  while( ccl_iterator_has_more_elements(it) )
    ccl_list_add(result,ccl_iterator_next_element(it));
  ccl_iterator_delete(it);

  return result;
}

			/* --------------- */

int
ar_idtable_get_size(const ar_idtable *idt)
{
  ccl_pre( idt != NULL );

  return ccl_hash_get_size(idt->table);
}


			/* --------------- */

void
ar_idtable_log(ccl_log_type log, ar_idtable *idt, 
	       void (*logproc)(ccl_log_type, void *obj, void *data),
	       void *data)
{
  ccl_hash_entry_iterator *it = ccl_hash_get_entries(idt->table);

  while( ccl_iterator_has_more_elements(it) )
    {
      ccl_hash_entry e = ccl_iterator_next_element(it);

      ar_identifier_log(log,(ar_identifier *)e.key);      
      ccl_log(log,"  -> ");
      if( logproc != NULL ) 
	logproc(log,e.object,data);
      else
	ccl_log(log,"%p",e.object);
      ccl_log(log,"\n");
    }
  ccl_iterator_delete(it);
}

			/* --------------- */

static unsigned int
s_list_hash (const void *l)
{
  return ccl_list_hash (l);
}

			/* --------------- */

static int
s_list_cmp (const void *l1, const void *l2)
{
  return ccl_list_compare (l1, l2, NULL);
}

			/* --------------- */

static void
s_identifier_destroy (void *p)
{
  ar_identifier *id = p;

  ccl_list_delete (id->path);
  ccl_delete (id);
}
