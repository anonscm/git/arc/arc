/*
 * ar-event.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-attributes.h"
#include "ar-event.h"

struct ar_event_st {
  int refcount;
  ar_identifier *name;
  ar_event **events;
  int nb_events;
  int flags;
  ccl_list *attr;
  int hvalue;
};

			/* --------------- */

ar_identifier *AR_EPSILON_ID;
ar_event *AR_EPSILON = NULL;

			/* --------------- */

static unsigned int 
s_event_hash (const ar_event *e);


void
ar_event_init(void)
{
  AR_EPSILON_ID = ar_identifier_create("$");
  AR_EPSILON = ar_event_create(AR_EPSILON_ID);
  ar_event_set_flags(AR_EPSILON,AR_SLOT_FLAG_PARENT);
}

			/* --------------- */

void
ar_event_terminate(void)
{
  ar_identifier_del_reference(AR_EPSILON_ID);
  ar_event_del_reference(AR_EPSILON);
}

			/* --------------- */

ar_event *
ar_event_create_vector (ar_identifier *name, int vectsize, ar_event **vect)
{
  int i;
  ar_event *result = ccl_new(ar_event);

  result->refcount = 1;
  result->name = ar_identifier_add_reference (name);
  result->events = vectsize ? ccl_new_array (ar_event *, vectsize) : NULL;
  result->nb_events = vectsize;
  result->attr = ccl_list_create ();
  for(i = 0; i < vectsize; i++)
    {
      result->events[i] = ar_event_add_reference(vect[i]);
      
      if (result->events[i]->attr)
	{
	  ccl_pair *p;

	  for (p = FIRST (result->events[i]->attr); p; p = CDR (p))
	    {
	      if (! ccl_list_has (result->attr, CAR (p)))
		ccl_list_add (result->attr, 
			      ar_identifier_add_reference (CAR (p)));
	    }
	}
    }
  result->flags = 0;
  result->hvalue = s_event_hash (result);
  
  return result;
}

			/* --------------- */

ar_event *
ar_event_add_reference(ar_event *e)
{
  ccl_pre( e != NULL );

  e->refcount++;

  return e;
}

			/* --------------- */

void
ar_event_del_reference(ar_event *e)
{
  ccl_pre( e != NULL ); ccl_pre( e->refcount > 0 );

  if( --e->refcount == 0 ) 
    {
      ar_identifier_del_reference (e->name);
      if( e->events != NULL )
	{
	  int i;
	  for(i = 0; i < e->nb_events; i++)
	    ar_event_del_reference(e->events[i]);
	  ccl_delete(e->events);
	}

      if( e->attr != NULL )
	ccl_list_clear_and_delete(e->attr,(ccl_delete_proc *)
				  ar_identifier_del_reference);
      ccl_delete(e);
    }
}

			/* --------------- */

static int 
s_log_not_quoted_rec (ccl_log_type log, ar_event *e, int is_first,
		      void (*idlog)(ccl_log_type log, const ar_identifier *id))
{
  int i, res = 0;
  
  if (! ar_identifier_has_name (e->name, AR_EPSILON_ID))
    {
      if (! is_first)
	ccl_log (log, ", ");
      else
	is_first = 0;
      idlog (log, e->name);
      res = 1;
    }
  
  for(i = 0; i < e->nb_events; i++)
    {
      if (s_log_not_quoted_rec (log, e->events[i], is_first, idlog))
	{
	  res = 1;
	  is_first = 0;
	}
    }

  return res;
}

static void
s_log_not_quoted (ccl_log_type log, ar_event *e,
		  void (*idlog)(ccl_log_type log, const ar_identifier *id))
{
  ccl_log(log,"<");
  s_log_not_quoted_rec (log, e, 1, idlog);
  ccl_log (log, ">");
}

			/* --------------- */

void
ar_event_log (ccl_log_type log, ar_event *e)
{
  ccl_pre (e != NULL);

  ar_event_log_gen (log, e, ar_identifier_log);
}

void
ar_event_log_gen (ccl_log_type log, ar_event *e,
		  void (*idlog)(ccl_log_type log, const ar_identifier *id))
{
  ccl_pre (e != NULL);

  ccl_log (log, "'");
  if (e->events == NULL)
    idlog (log, e->name);
  else
    s_log_not_quoted (log, e, idlog);
  ccl_log (log, "'");
}

			/* --------------- */

ar_identifier *
ar_event_get_name(const ar_event *e)
{
  ccl_pre (e != NULL);
  ccl_pre (e->name != NULL);
  
  return ar_identifier_add_reference(e->name);
}

int
ar_event_has_epsilon_name (const ar_event *e)
{
  ccl_pre (e != NULL);
  ccl_pre (e->name != NULL);
  
  return ar_identifier_has_name (e->name, AR_EPSILON_ID);
}


int
ar_event_get_flags(ar_event *e)
{
  ccl_pre( e != NULL );

  return e->flags;
}

			/* --------------- */

void
ar_event_set_flags(ar_event *e, int flags)
{
  ccl_pre( e != NULL );

  e->flags = flags;
}

			/* --------------- */

const ccl_list *
ar_event_get_attr(ar_event *e)
{
  ccl_pre( e != NULL );

  return e->attr;
}

			/* --------------- */

int 
ar_event_has_attr (ar_event *e, ar_identifier *attribute)
{
  ccl_pre (e != NULL);

  return e->attr != NULL && ccl_list_has (e->attr, attribute);
}

			/* --------------- */

void
ar_event_set_attr(ar_event *e, const ccl_list *attr)
{
  ccl_pre( e != NULL );
 
  if( e->attr != NULL )
    {
      ccl_list_clear_and_delete(e->attr,(ccl_delete_proc *)
				ar_identifier_del_reference);
      e->attr = NULL;
    }

  if( attr != NULL )
    e->attr = ccl_list_deep_dup(attr,(ccl_duplicate_func *)
				ar_identifier_add_reference);
}

			/* --------------- */

int
ar_event_equals (const ar_event *e1, const ar_event *e2)
{
  int i;
  
  if (e1 == e2) return 1;
  if (e1->hvalue != e2->hvalue) return 0;  
  if( e1->name != e2->name ) return 0;
  if( e1->nb_events != e2->nb_events ) return 0;

  for(i = 0; i < e1->nb_events; i++)
    {
      if (! ar_event_equals (e1->events[i], e2->events[i]))
	return 0;
    }
  return 1;
}

			/* --------------- */

int
ar_event_compare (const ar_event *e1, const ar_event *e2)
{
  int i, j, result;

  if (e1 == e2) 
    result = 0;
  else if (e1->hvalue != e2->hvalue)
    result = (e1->hvalue - e2->hvalue);
  else if (e1->name != e2->name)
    result = (intptr_t) e1->name - (intptr_t) e2->name;
  else
    {
      result = 0;

      for (i = 0, j = 0; (i < e1->nb_events && j < e2->nb_events
			  && result == 0);
	   i++, j++)
	result = ar_event_compare (e1->events[i], e2->events[i]);

      if (result == 0)
	result = e1->nb_events - e2->nb_events;
    }

  return result;
}

			/* --------------- */

unsigned int 
ar_event_hash (const ar_event *e)
{
  ccl_pre (e != NULL);
  
  return e->hvalue;
}

			/* --------------- */

ar_event *
ar_event_add_prefix (ar_event *ev, ar_identifier *pref, ccl_hash *cache)
{
  ar_event *result;

  if (cache && ccl_hash_find (cache, ev))
    result = ccl_hash_get (cache);
  else
    {
      result = ccl_new(ar_event);
      result->refcount = 1;
      result->name = ar_identifier_add_prefix(ev->name,pref);
      result->nb_events = ev->nb_events;
      result->flags = ev->flags;  
      if( ev->attr != NULL )
	result->attr = ccl_list_deep_dup(ev->attr,(ccl_duplicate_func *)
					 ar_identifier_add_reference);
      else
	result->attr = NULL;
      
      if( ev->events != NULL )
	{
	  int i;
	  
	  result->events = ccl_new_array(ar_event *, ev->nb_events);
	  for(i = 0; i < ev->nb_events; i++)
	    result->events[i] = ar_event_add_prefix(ev->events[i],pref, cache);
	}
      result->hvalue = s_event_hash (result);
      
      if (cache)
	{
	  ccl_hash_find (cache, ev);
	  ccl_hash_insert (cache, result);
	}
    }

  if (cache)
    result = ar_event_add_reference (result);
  
  return result;
}

			/* --------------- */

int
ar_event_get_width (const ar_event *ev)
{
  ccl_pre (ev != NULL);

  return ev->nb_events;
}

			/* --------------- */

ar_event **
ar_event_get_components(const ar_event *ev)
{
  ccl_pre( ev != NULL );

  return ev->events;
}

			/* --------------- */

int
ar_event_is_epsilon_vector (const ar_event *e)
{
  int i;
  int result;

  if (e == AR_EPSILON)
    result = 1;
  else
    {
      ar_identifier *id = ar_identifier_get_name (e->name);
      result = (id == AR_EPSILON_ID);
      ar_identifier_del_reference (id);
    }

  if (result)
    {
      for (i = 0; result && i < e->nb_events; i++)
	result = ar_event_is_epsilon_vector (e->events[i]);
    }
  
  return result;
}

static unsigned int 
s_event_hash (const ar_event *e)
{
  int i;
  uintptr_t result = (uintptr_t) e->name;

  if (e->events != NULL)
    {
      for (i = 0; i < e->nb_events; i++)
	{
	  result *= 19;
	  result += 111 * ar_event_hash (e->events[i]);
	}
    }

  return result;
}
