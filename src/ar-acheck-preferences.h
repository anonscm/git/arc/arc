/*
 * ar-acheck-preferences.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_ACHECK_PREFERENCES_H
# define AR_ACHECK_PREFERENCES_H

enum ar_acheck_pref {
# define AR_ACHECK_PREF(_enumid, _strid) ACHECK_PREF_ ## _enumid,
# include "ar-acheck-preferences.def"
# undef AR_ACHECK_PREF
  LAST_AND_UNUSED_AR_ACHECK_PREF
};

extern const char *AR_ACHECK_PREF_NAMES[];

# define AR_ACHECK_PREF(_enumid, _strid) \
  extern const char *ACHECK_PREF_ ## _enumid ## _STR;
# include "ar-acheck-preferences.def"
# undef AR_ACHECK_PREF

#endif /* ! AR_ACHECK_PREFERENCES_H */
