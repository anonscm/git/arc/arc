/*
 * ar-ca-expr2dd.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_EXPR2DD_H
# define AR_CA_EXPR2DD_H

# include "varorder.h"
# include "ar-dd.h"
# include "ar-ca-expression.h"

extern ar_dd 
ar_ca_expr_compute_dd (ar_ca_expr *expr, ar_ddm ddm, const varorder *order, 
		       ccl_hash *cache);

extern ar_ca_expr *
ar_ca_expr_compute_expr_from_dd (ar_ddm ddm, ar_ca_exprman *eman, 
				 const varorder *order, ar_dd dd,
				 ccl_hash *cache);

extern ar_ca_expr *
ar_ca_expr_simplify_with_dd (ar_ddm ddm, ar_ca_exprman *eman, 
			     const varorder *order, ar_ca_expr *expr,
			     ccl_hash *e2dd, ccl_hash *dd2e);

extern ar_ca_expr *
ar_ca_expr_simplify (ar_ca_expr *expr);

extern ar_ca_expr *
ar_ca_expr_rewrite_as_primes (ar_ca_expr *expr);

extern ccl_list * 
ar_ca_expr_get_primes_as_expr (ar_ca_expr *expr);

extern void
ar_ca_expr_build_dd_caches (ccl_hash **p_e2dd, ccl_hash **p_dd2e);

extern void
ar_ca_expr_clean_up_dd_caches (ccl_hash *e2dd, ccl_hash *dd2e);
 
#endif /* ! AR_CA_EXPR2DD_H */
