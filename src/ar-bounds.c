/*
 * ar-bounds.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-bounds.h"

# define MAX(_a,_b) (((_a)<(_b))?(_b):(_a))
# define MIN(_a,_b) (((_a)<(_b))?(_a):(_b))

# define SET_TRUE(_b) ar_bounds_set(_b,1,1)
# define SET_FALSE(_b) ar_bounds_set(_b,0,0)
# define SET_BOOL(_b) ar_bounds_set(_b,0,1)

			/* --------------- */

void
ar_bounds_init(ar_bounds *b, int min, int max)
{
  ccl_pre( b != NULL );

  b->min = min;
  b->max = max;
}

			/* --------------- */

void
ar_bounds_init_singleton(ar_bounds *b, int value)
{
  ar_bounds_init(b,value,value);
}

			/* --------------- */

void
ar_bounds_log(ccl_log_type log, const ar_bounds *b)
{
  ccl_pre( b != NULL );

  ccl_log(log,"[%d,%d]",b->min,b->max);
}

			/* --------------- */

void
ar_bounds_save(ar_bounds *b, ar_backtracking_stack *bs)
{
  ccl_pre( b != NULL ); 

  ar_backtracking_stack_save(bs,&b->min);
  ar_backtracking_stack_save(bs,&b->max);
}

			/* --------------- */

int
ar_bounds_contains(const ar_bounds *b, int value)
{
  ccl_pre( b != NULL );

  return (b->min <= value && value <= b->max);
}

			/* --------------- */

int
ar_bounds_include(const ar_bounds *b, const ar_bounds *other)
{
  ccl_pre( other != NULL );

  return ar_bounds_contains(b,other->min) && ar_bounds_contains(b,other->max);
}

			/* --------------- */

void
ar_bounds_set(ar_bounds *b, int min, int max)
{
  ar_bounds_init(b,min,max);
}

			/* --------------- */

int 
ar_bounds_is_singleton(const ar_bounds *b)
{
  ccl_pre( b != NULL );

  return (b->min == b->max);
}

			/* --------------- */

void
ar_bounds_copy(ar_bounds *dst, const ar_bounds *src)
{
  ccl_pre( dst != NULL );   ccl_pre( src != NULL );

  dst->min = src->min;
  dst->max = src->max;
}

			/* --------------- */

int
ar_bounds_are_equal (const ar_bounds *b1, const ar_bounds *b2)
{
  ccl_pre (b1 != NULL); 
  ccl_pre (b2 != NULL);

  return (b1->min == b2->min && b1->max == b2->max);
}

			/* --------------- */

void
ar_bounds_or(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  ccl_pre( b1 != NULL ); ccl_pre( b2 != NULL );

  if( b1->min || b2->min ) SET_TRUE(r);
  else if( !(b1->max || b2->max) ) SET_FALSE(r);
  else SET_BOOL(r);
}

			/* --------------- */

void
ar_bounds_and(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  ccl_pre( b1 != NULL ); ccl_pre( b2 != NULL );

  if( b1->min && b2->min ) SET_TRUE(r);
  else if( !(b1->max && b2->max) ) SET_FALSE(r);
  else SET_BOOL(r);
}

			/* --------------- */

void
ar_bounds_not(ar_bounds *r, ar_bounds *b)
{
  ccl_pre( b != NULL );

  if( b->min == b->max ) ar_bounds_init_singleton(r,!b->min);
  else SET_BOOL(r);
}

			/* --------------- */

void
ar_bounds_neg(ar_bounds *r, ar_bounds *b)
{
  ccl_pre( b != NULL );

  ar_bounds_set(r,-b->max,-b->min);
}

			/* --------------- */

void
ar_bounds_eq(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  if( ar_bounds_is_singleton(b1) && ar_bounds_is_singleton(b2) )
    {
      if( b1->min == b2->min ) SET_TRUE(r);
      else SET_FALSE(r);
    }
  else
    {
      SET_BOOL(r);
    }
}

			/* --------------- */

void
ar_bounds_lt(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  if( b1->max < b2->min ) SET_TRUE(r);
  else if( b1->min >= b2->max ) SET_FALSE(r);
  else SET_BOOL(r);
}

			/* --------------- */

void
ar_bounds_add(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  ccl_pre( r != NULL ); ccl_pre( b1 != NULL ); ccl_pre( b2 != NULL );

  r->min = b1->min+b2->min;
  r->max = b1->max+b2->max;
}

			/* --------------- */

void
ar_bounds_mul(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  int s1, s2, s3, s4;

  ccl_pre( r != NULL ); ccl_pre( b1 != NULL ); ccl_pre( b2 != NULL );

  s1 = b1->min*b2->min;
  s2 = b1->min*b2->max;
  s3 = b1->max*b2->min;
  s4 = b1->max*b2->max;

  if ((s1<=s2)&&(s1<=s3)&&(s1<=s4)) r->min = s1;
  else if ((s2<=s3)&&(s2<=s4))      r->min = s2;
  else if (s3<=s4)                  r->min = s3;
  else                              r->min = s4;

  if ((s1>=s2)&&(s1>=s3)&&(s1>=s4)) r->max = s1;
  else if ((s2>=s3)&&(s2>=s4))      r->max = s2;
  else if (s3>=s4)                  r->max = s3;
  else                              r->max = s4;
}

			/* --------------- */

void
ar_bounds_div(ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  int s1, s2;

  ccl_pre( r != NULL ); ccl_pre( b1 != NULL ); 
  ccl_pre( ar_bounds_is_singleton(b2) );
  ccl_pre( b2->min != 0 );

  s1 = b1->min/b2->min;
  s2 = b1->max/b2->max;
  r->min = MIN(s1,s2);
  r->max = MAX(s1,s2);
}

			/* --------------- */

void
ar_bounds_mod (ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  int s1, s2;

  ccl_pre (r != NULL); 
  ccl_pre (b1 != NULL);
  ccl_pre (ar_bounds_is_singleton (b2));
  ccl_pre (b2->min != 0);

  ar_bounds_init (r, b2->min, 0);

  for (s1 = b1->min; s1 <= b1->max; s1++)
    {
      s2 = s1 % b2->min;
      r->min = MIN (r->min, s2);
      r->max = MAX (r->max, s2);
    }
}

			/* --------------- */

void
ar_bounds_min (ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  ccl_pre (r != NULL); 
  ccl_pre (b1 != NULL);
  ccl_pre (b2 != NULL);

  r->min = MIN (b1->min, b2->min);
  r->max = MIN (b1->max, b2->max);
}

			/* --------------- */

void
ar_bounds_max (ar_bounds *r, ar_bounds *b1, ar_bounds *b2)
{
  ccl_pre (r != NULL); 
  ccl_pre (b1 != NULL);
  ccl_pre (b2 != NULL);

  r->min = MAX (b1->min, b2->min);
  r->max = MAX (b1->max, b2->max);
}

			/* --------------- */

void
ar_bounds_ite(ar_bounds *r, ar_bounds *i, ar_bounds *t, ar_bounds *e)
{
  int s1, s2;

  ccl_pre( r != NULL ); ccl_pre( i != NULL ); 

  s1 = i->min;
  s2 = i->max;

  if( s1 == s2 )
    {
      if( s1 ) ar_bounds_copy(r,t);
      else     ar_bounds_copy(r,e);
    }
  else
    {
      r->min = MIN(t->min,e->min);
      r->max = MAX(t->max,e->max);
    }
}

