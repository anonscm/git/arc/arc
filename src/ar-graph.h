/*
 * ar-graph.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_GRAPH_H
# define AR_GRAPH_H

# include <ccl/ccl-protos.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-list.h>

typedef struct ar_graph_st ar_graph;
typedef struct ar_graph_vertex_st ar_graph_vertex;
typedef struct ar_graph_edge_st ar_graph_edge;

typedef struct ar_graph_label_methods_st {
  ccl_log_proc *log;
  ccl_compare_func *cmp;
  ccl_hash_func *hash;
  ccl_delete_proc *del;
} ar_graph_label_methods;

			/* --------------- */

extern ar_graph *
ar_graph_create(void *graph_label, 
		const ar_graph_label_methods *graph_methods,
		const ar_graph_label_methods *vertex_methods,
		const ar_graph_label_methods *edge_methods);

extern ar_graph *
ar_graph_add_reference(ar_graph *g);

extern void
ar_graph_del_reference(ar_graph *g);

extern void *
ar_graph_get_label(ar_graph *g);

extern int
ar_graph_get_number_of_vertices(ar_graph *g);

extern int
ar_graph_get_number_of_edges(ar_graph *g);

extern void
ar_graph_log(ccl_log_type log, ar_graph *g);

extern ccl_list *
ar_graph_get_vertices(ar_graph *g);

extern ccl_list *
ar_graph_get_edges(ar_graph *g);

			/* --------------- */

extern ar_graph_vertex *
ar_graph_add_vertex(ar_graph *g, void *label);

extern ar_graph_vertex *
ar_graph_get_vertex(ar_graph *g, void *label);

extern void *
ar_graph_vertex_get_label(ar_graph_vertex *v);

extern ccl_list *
ar_graph_vertex_get_out_edges(ar_graph_vertex *v);

extern ccl_list *
ar_graph_vertex_get_in_edges(ar_graph_vertex *v);

			/* --------------- */

extern ar_graph_edge *
ar_graph_add_edge(ar_graph *g, ar_graph_vertex *v1, void *label, 
		  ar_graph_vertex *v2);

extern ar_graph_vertex *
ar_graph_edge_get_src(ar_graph_edge *e);

extern void *
ar_graph_edge_get_label(ar_graph_edge *e);

extern ar_graph_vertex *
ar_graph_edge_get_tgt(ar_graph_edge *e);

			/* --------------- */

extern int
ar_graph_get_topological_order(ar_graph *g, ccl_list *order);

extern int
ar_graph_get_rooted_topological_order(ar_graph *g, ar_graph_vertex *v,
				      ccl_list *order);

			/* --------------- */

extern ccl_list *
ar_graph_get_articulation_points (ar_graph *g);

#endif /* ! AR_GRAPH_H */
