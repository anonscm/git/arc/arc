/*
 * ar-help.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_HELP_H
# define AR_HELP_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-list.h>

typedef struct ar_help_st ar_help;
struct ar_help_st {
  const char *manpage;
  const char *shortdesc;
  const char *description;  
};

extern int
ar_help_init (void);

extern void
ar_help_terminate (void);

extern const ar_help *
ar_help_manpage (const char *mp);

extern ccl_list *
ar_help_apropos (const char *kw);

extern void
ar_help_log_manpage (ccl_log_type log, const char *mp);

#endif /* ! AR_HELP_H */
