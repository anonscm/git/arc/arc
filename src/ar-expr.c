/*
 * ar-expr.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-stack.h>
#include "ar-node.h"
#include "ar-expr-p.h"
#include "mec5/mec5.h"
#include "ar-bang-ids.h"

struct op_property {
  int priority;
  char *name;
};

			/* --------------- */

static struct op_property OP_PROPS[] = {
  { 0, "ite" }, /* AR_EXPR_ITE */
  { 1, "or" }, /* AR_EXPR_OR */
  { 2, "and" }, /* AR_EXPR_AND */
  { 3, "=" }, /* AR_EXPR_EQ */ 
  { 3, "!=" }, /* AR_EXPR_NEQ */
  { 4, "<" }, /* AR_EXPR_LT */
  { 4, ">" }, /* AR_EXPR_GT */ 
  { 4, "<=" }, /* AR_EXPR_LEQ */
  { 4, ">=" }, /* AR_EXPR_GEQ */
  { 5, "+" }, /* AR_EXPR_ADD */
  { 5, "-" }, /* AR_EXPR_SUB */ 
  { 6, "*" }, /* AR_EXPR_MUL */ 
  { 6, "/" }, /* AR_EXPR_DIV */
  { 6, "mod"  }, /* AR_EXPR_MOD */
  { 7, "not" }, /* AR_EXPR_NOT */
  { 7, "-" }, /* AR_EXPR_NEG */
  { 8, "<>" },  /* AR_EXPR_EXIST */ 
  { 8, "[]" },  /* AR_EXPR_FORALL */
  { 8, NULL }, /* AR_EXPR_CST */
  { 8, NULL }, /* AR_EXPR_VAR */
  { 8, NULL }, /* AR_EXPR_STRUCT_MEMBER */
  { 8, NULL }, /* AR_EXPR_ARRAY_MEMBER */
  { 8, NULL }, /* AR_EXPR_CALL */
  { 8, NULL }, /* AR_EXPR_PARAM */
  { 8, NULL }, /* AR_EXPR_ARRAY */
  { 8, NULL }  /* AR_EXPR_STRUCT */
};

			/* --------------- */

static ar_expr *
s_new_expr(ar_expr_op op, int arity);

static void
s_delete_expr(ar_expr *expr);

			/* --------------- */

static ar_expr *BOOLEANS[2] = { NULL, NULL };
# define BOOL_TRUE (BOOLEANS[1])
# define BOOL_FALSE (BOOLEANS[0])

			/* --------------- */

void
ar_expr_init(void)
{  
  ar_constant *t = ar_constant_crt_boolean(1);
  ar_constant *f = ar_constant_crt_boolean(0);
  BOOL_TRUE = ar_expr_crt_constant(t);
  BOOL_FALSE = ar_expr_crt_constant(f);
  ar_constant_del_reference(t);
  ar_constant_del_reference(f);
}
			/* --------------- */

void
ar_expr_terminate(void)
{
  ar_expr_del_reference(BOOL_TRUE);
  ar_expr_del_reference(BOOL_FALSE);
}

			/* --------------- */

ar_expr *
ar_expr_crt_true(void)
{
  return ar_expr_add_reference(BOOL_TRUE);
}

			/* --------------- */

ar_expr *
ar_expr_crt_false(void)
{
  return ar_expr_add_reference(BOOL_FALSE);
}

			/* --------------- */

ar_expr *
ar_expr_crt_integer(int i)
{
  ar_constant *cst = ar_constant_crt_integer(i);
  ar_expr *result = ar_expr_crt_constant(cst);

  ar_constant_del_reference(cst);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_constant(ar_constant *cst)
{
  ar_expr *result;

  ccl_pre( cst != NULL );
  
  if (ar_constant_has_type (cst, AR_BOOLEANS))
    {
      if (ar_constant_get_boolean_value (cst) && BOOL_TRUE != NULL)
	return ar_expr_crt_true ();
      if (ar_constant_get_boolean_value (cst) && BOOL_FALSE != NULL)
	return ar_expr_crt_false ();
    }

  result = s_new_expr (AR_EXPR_CST, 0);
      
  result->u.cst = ar_constant_add_reference(cst);
  result->type = ar_constant_get_type(cst);
  
  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_parameter(ar_context_slot *p)
{
  ar_expr *result = s_new_expr(AR_EXPR_PARAM,0);

  ccl_pre( p != NULL ); 

  result->u.slot = ar_context_slot_add_reference(p);
  result->type = ar_context_slot_get_type(p);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_variable(ar_context_slot *slot)
{
  ar_expr *result = s_new_expr(AR_EXPR_VAR,0);

  result->u.slot = ar_context_slot_add_reference(slot);
  result->type = ar_context_slot_get_type(slot);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_struct_member(ar_expr *s, ar_identifier *id)
{
  ar_type *aux;
  ar_expr *result = s_new_expr(AR_EXPR_STRUCT_MEMBER,1);

  result->u.field = ar_identifier_add_reference(id);
  result->args[0] = ar_expr_add_reference(s);

  aux = ar_expr_get_type(s);
  result->type = ar_type_struct_get_field(aux,id);
  ar_type_del_reference(aux);

  return result;
}

			/* --------------- */

static ar_expr *
s_crt_nary(ar_expr_op op, int arity, ar_expr **args)
{
  int i;
  ar_type *aux;
  ar_expr *result = s_new_expr(op,arity);

  ccl_pre( args != NULL );

  switch( op ) {
  case AR_EXPR_ITE :
    result->type = ar_expr_get_type(args[1]);
    break;
  case AR_EXPR_AND : case AR_EXPR_OR :    
  case AR_EXPR_EQ : case AR_EXPR_NEQ : 
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
  case AR_EXPR_NOT : case AR_EXPR_EXIST : case AR_EXPR_FORALL :  
    result->type = ar_type_add_reference(AR_BOOLEANS);
    break;
  case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
  case AR_EXPR_MOD : case AR_EXPR_NEG : case AR_EXPR_MIN : case AR_EXPR_MAX :
    result->type = ar_type_add_reference(AR_INTEGERS);
    break;

  case AR_EXPR_ARRAY_MEMBER :
    /* ccl_assert( ar_expr_is_constant(args[1]) );*/

    aux = ar_expr_get_type(args[0]);
    result->type = ar_type_array_get_base_type(aux);
    ar_type_del_reference(aux);
    break;

  case AR_EXPR_CST : case AR_EXPR_VAR : case AR_EXPR_STRUCT_MEMBER :
  case AR_EXPR_CALL : case AR_EXPR_PARAM : case AR_EXPR_ARRAY : 
  case AR_EXPR_STRUCT :
    ccl_throw_no_msg(internal_error);
    break;
  }

  for(i = 0; i < arity; i++)
    result->args[i] = ar_expr_add_reference(args[i]);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_unary(ar_expr_op op, ar_expr *e0)
{
  ar_expr *args[1];

  ccl_pre( e0 != NULL );

  args[0] = e0;

  if( op == AR_EXPR_NOT )
    {
      if( e0 == BOOL_TRUE ) 
	return ar_expr_crt_false();
      if( e0 == BOOL_FALSE ) 
	return ar_expr_crt_true();
    }

  return s_crt_nary(op,1,args);
}

			/* --------------- */

ar_expr *
ar_expr_crt_binary(ar_expr_op op, ar_expr *e0, ar_expr *e1)
{
  ccl_pre( e0 != NULL ); ccl_pre( e1 != NULL );

  if( e0->op == AR_EXPR_CST || e1->op == AR_EXPR_CST )
    {
      if( op == AR_EXPR_AND  )
	{
	  if( e0 == BOOL_FALSE ) 
	    return ar_expr_crt_false();
	  if( e0 == BOOL_TRUE ) 
	    return ar_expr_add_reference(e1);
	  if( e1 == BOOL_FALSE ) 
	    return ar_expr_crt_false();
	  if( e1 == BOOL_TRUE ) 
	    return ar_expr_add_reference(e0);
	  if( e1 == e0 )
	    return ar_expr_add_reference(e0);
	}
      else if( op == AR_EXPR_OR  )
	{
	  if( e0 == BOOL_FALSE ) 
	    return ar_expr_add_reference(e1);
	  if( e0 == BOOL_TRUE ) 
	    return  ar_expr_crt_true();
	  if( e1 == BOOL_FALSE ) 
	    return ar_expr_add_reference(e0);
	  if( e1 == BOOL_TRUE ) 
	    return  ar_expr_crt_true();
	  if( e1 == e0 )
	    return ar_expr_add_reference(e0);
	}
      else if (op == AR_EXPR_EQ)
	{
	  if (BOOL_TRUE == e0)
	    return ar_expr_add_reference (e1);
	  else if (BOOL_FALSE == e0)
	    return ar_expr_crt_unary (AR_EXPR_NOT, e1);
	  else if (BOOL_TRUE == e1)
	    return ar_expr_add_reference (e0);
	  else if (BOOL_FALSE == e1)
	    return ar_expr_crt_unary (AR_EXPR_NOT, e0);
	}
    }

  if( (op == AR_EXPR_AND || op == AR_EXPR_OR) && 
      (e0->op == op || e1->op == op) )
    {
      int i, j, arity;
      ar_expr **args;
      ar_expr *result;

      if( e0->op == op ) arity = e0->arity;
      else arity = 1;

      if( e1->op == op ) arity += e1->arity;
      else arity++;

      ccl_assert( arity > 2 );

      i = 0;
      args = ccl_new_array(ar_expr *,arity);

      if( e0->op == op ) 
	{
	  for(j = 0; j < e0->arity; j++)
	    args[i++] = e0->args[j];
	}
      else
	args[i++] = e0;

      if( e1->op == op ) 
	{
	  for(j = 0; j < e1->arity; j++)
	    args[i++] = e1->args[j];
	}
      else
	args[i++] = e1;

      ccl_assert( arity == i );
      result = s_crt_nary(op,arity,args);
      ccl_delete(args);

      ccl_assert( result->arity >= 2 );

      return result;
    }
  else
    {
      ar_expr *args[2];
      args[0] = e0; args[1] = e1; 
      return s_crt_nary(op,2,args);
    }
}

			/* --------------- */

ar_expr *
ar_expr_crt_ternary(ar_expr_op op, ar_expr *e0, ar_expr *e1, 
		    ar_expr *e2)
{
  ar_expr *args[3];

  ccl_pre( e0 != NULL ); ccl_pre( e1 != NULL ); ccl_pre( e2 != NULL );

  args[0] = e0; args[1] = e1; args[2] = e2; 

  if(  op == AR_EXPR_ITE )
    {
      if( ar_expr_is_true(e0) ) 
	return ar_expr_add_reference(e1);
      else if( ar_expr_is_false(e0) ) 
	return ar_expr_add_reference(e2);
      else if( ar_expr_has_base_type(e1,AR_BOOLEANS) )
	{
	  if( ar_expr_is_true(e1) ) 
	    return ar_expr_crt_binary(AR_EXPR_OR,e0,e2);
	  else if( ar_expr_is_false(e1) ) 
	    {
	      ar_expr *aux = ar_expr_crt_unary(AR_EXPR_NOT,e0);
	      ar_expr *result = ar_expr_crt_binary(AR_EXPR_AND,aux,e2);
	      ar_expr_del_reference(aux);

	      return result;
	    }
	  else if( ar_expr_is_true(e2) ) 
	    {
	      ar_expr *aux = ar_expr_crt_unary(AR_EXPR_NOT,e0);
	      ar_expr *result = ar_expr_crt_binary(AR_EXPR_OR,aux,e1);
	      ar_expr_del_reference(aux);

	      return result;
	    }
	  else if( ar_expr_is_false(e2) ) 
	    return ar_expr_crt_binary(AR_EXPR_AND,e0,e1);
	}
    }

  return s_crt_nary(op,3,args);
}

			/* --------------- */

ar_expr *
ar_expr_crt_quantifier(ar_expr_op op, ar_context *ctx, ccl_list *qvars, 
		       ar_expr *F)
{
  ar_expr *result = s_crt_nary(op,1,&F);

  result->qctx = ar_context_add_reference(ctx);
  result->u.qvars = 
    ccl_list_deep_dup(qvars,(ccl_duplicate_func *)ar_expr_add_reference);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_function_call(ar_signature *sig, ar_expr **args)
{
  int i;
  int arity = ar_signature_get_arity(sig);
  ar_expr *result =  s_new_expr(AR_EXPR_CALL,arity);

  result->u.sig = ar_signature_add_reference(sig);

  for(i = 0; i < arity; i++)
    result->args[i] = ar_expr_add_reference(args[i]);
  result->type = ar_signature_get_return_domain(sig);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_array(ar_expr **a, int asize)
{
  int i;
  ar_type *stype = ar_expr_get_type(a[0]);
  ar_expr *result = s_new_expr(AR_EXPR_ARRAY,asize);

  result->type = ar_type_crt_array(stype,asize);
  ar_type_del_reference(stype);

  for(i = 0; i < asize; i++)
    result->args[i] = ar_expr_add_reference(a[i]);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_structure(ar_identifier **fnames, ar_expr **fvals, int width)
{
  int i;
  ar_expr *result = s_new_expr(AR_EXPR_STRUCT,width);

  result->type = ar_type_crt_structure();
  result->u.fnames = ccl_new_array(ar_identifier *,width);

  for(i = 0; i < width; i++)
    {
      ar_type *ftype = ar_expr_get_type(fvals[i]);
      ar_type_struct_add_field(result->type,fnames[i],ftype);
      ar_type_del_reference(ftype);
    }

  i = 0;
  for(i = 0; i < width; i++)
    {
      int index = ar_type_struct_get_field_index(result->type,fnames[i]);

      ccl_assert( 0 <= index && index < width );

      result->args[index] = ar_expr_add_reference(fvals[i]);
      result->u.fnames[index] = ar_identifier_add_reference(fnames[i]);
    }

  return result;
}

			/* --------------- */

static void
s_add_constraint (ar_expr **p_result, ar_expr *e, int val, ar_type *t)
{
  ar_constant *cval = ar_constant_crt_symbol (val, t);
  ar_expr *eval = ar_expr_crt_constant (cval);
  ar_expr *f = ar_expr_crt_binary (AR_EXPR_EQ, e, eval);

  ar_expr_del_reference (eval);
  ar_constant_del_reference (cval);

  if (*p_result == NULL)
    *p_result = ar_expr_add_reference (f);
  else
    {
      ar_expr *tmp = ar_expr_crt_binary (AR_EXPR_OR, f, *p_result);
      ar_expr_del_reference (*p_result);
      *p_result = tmp;
    }
  ar_expr_del_reference (f);
}

			/* --------------- */

ar_expr *
ar_expr_crt_in_domain (ar_expr *e, ar_type *t)
{
  ar_expr *result = NULL;
  ar_type_kind kind = ar_type_get_kind (t);

  switch (kind) 
    {
    case AR_TYPE_RANGE :
      {
	int min = ar_type_range_get_min (t);
	ar_constant *cmin = ar_constant_crt_integer (min);
	ar_expr *emin = ar_expr_crt_constant (cmin);
	int max = ar_type_range_get_max (t);
	ar_constant *cmax = ar_constant_crt_integer (max);
	ar_expr *emax = ar_expr_crt_constant (cmax);
	ar_expr *sup_min = ar_expr_crt_binary (AR_EXPR_LEQ, emin, e);
	ar_expr *inf_max = ar_expr_crt_binary (AR_EXPR_LEQ, e, emax);
	result = ar_expr_crt_binary (AR_EXPR_AND, sup_min, inf_max);
	ar_expr_del_reference (sup_min);
	ar_expr_del_reference (inf_max);
	ar_expr_del_reference (emax);
	ar_expr_del_reference (emin);
	ar_constant_del_reference (cmax);
	ar_constant_del_reference (cmin);
      }
      break;

    case AR_TYPE_SYMBOL_SET :
      {
	ccl_int_iterator *i = ar_type_symbol_set_get_values (t);
      
	while (ccl_iterator_has_more_elements (i))
	  {
	    int ival = ccl_iterator_next_element (i);
	    s_add_constraint (&result, e, ival, t);
	  }
	ccl_iterator_delete (i);
      }
      break;

    case AR_TYPE_ENUMERATION :
      {
	int i, max = ar_type_get_cardinality (t);
	for (i = 0; i < max; i++)
	  s_add_constraint (&result, e, i, t);
      }
      break;

    case AR_TYPE_ARRAY :
      {
	int i, width = ar_type_get_width(t);
	ar_type *base_type = ar_type_array_get_base_type(t);
	ar_expr *aux1, *aux2;

	ccl_assert( width >= 1 );

	aux1 = ar_expr_crt_integer(0);
	aux2 = ar_expr_crt_binary(AR_EXPR_ARRAY_MEMBER,e,aux1);
	result = ar_expr_crt_in_domain(aux2,base_type);
	ar_expr_del_reference(aux1);
	ar_expr_del_reference(aux2);

	for(i = 1; i < width; i++)
	  {
	    aux1 = ar_expr_crt_integer(i);
	    aux2 = ar_expr_crt_binary(AR_EXPR_ARRAY_MEMBER,e,aux1);
	    ar_expr_del_reference(aux1);
	    aux1 = ar_expr_crt_in_domain(aux2,base_type);
	    ar_expr_del_reference(aux2);
	    aux2 = ar_expr_crt_binary(AR_EXPR_AND,result,aux1);
	    ar_expr_del_reference(aux1);
	    ar_expr_del_reference(result);

	    result = aux2;
	  }
	ar_type_del_reference(base_type);
      }
      break;

    case AR_TYPE_BANG:
      {
	ar_node *node = ar_type_bang_get_node (t);
	switch (ar_type_bang_get_kind (t)) 
	  {
	  case AR_TYPE_BANG_EVENTS:
	  case AR_TYPE_BANG_VARIABLES:
	    result = ar_expr_crt_true ();
	    break;
	  case AR_TYPE_BANG_CONFIGURATIONS:
	    {
	      ar_identifier *conf = 
		ar_bang_id_for_node (node, AR_BANG_CONFIGURATIONS);
	      ar_signature *sig = ar_context_get_signature (MEC5_CONTEXT, conf);
	      result = ar_expr_crt_function_call(sig, &e);
	      ar_signature_del_reference (sig);
	      ar_identifier_del_reference (conf);
	    }
	    break;
	  default:
	    ccl_unreachable ();
	  }
	ar_node_del_reference (node);
      }
      break;

    case AR_TYPE_STRUCTURE :
      {
	ar_identifier_iterator *i = ar_type_struct_get_fields(t);

	while( ccl_iterator_has_more_elements(i) )
	  {
	    ar_identifier *fname = ccl_iterator_next_element(i);
	    ar_type *ftype = ar_type_struct_get_field(t,fname);
	    ar_expr *sm = ar_expr_crt_struct_member(e,fname);
	    ar_expr *f = ar_expr_crt_in_domain(sm,ftype);

	    if( result == NULL )
	      result = ar_expr_add_reference(f);
	    else
	      {
		ar_expr *tmp = ar_expr_crt_binary(AR_EXPR_AND,result,f);
		ar_expr_del_reference(result);
		result = tmp;
	      }

	    ar_expr_del_reference(f);
	    ar_expr_del_reference(sm);
	    ar_type_del_reference(ftype);
	    ar_identifier_del_reference(fname);
	  }
	ccl_iterator_delete(i);
      }
      break;

    case AR_TYPE_BOOLEANS : case AR_TYPE_INTEGERS : case AR_TYPE_SYMBOLS :
    case AR_TYPE_ABSTRACT :
      result = ar_expr_crt_true();
      break;

    default:
      ccl_unreachable ();
      break;
    }

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_and(int arity, ar_expr **ops)
{
  int i, k;
  ar_expr *result;
  ar_expr **args = ccl_new_array(ar_expr *, arity);

  for(k = 0, i = 0; i < arity; i++)
    {
      if( ops[i] == BOOL_FALSE )
	{
	  ccl_delete(args);
	  return ar_expr_crt_false();
	}
      else if( ops[i] != BOOL_TRUE )
	{
	  args[k++] = ops[i];
	}
    }

  if( k == 0 )
    result = ar_expr_crt_true();
  else if( k == 1 )
    result = ar_expr_add_reference(args[0]);
  else
    result = s_crt_nary(AR_EXPR_AND,k,args);
  ccl_delete(args);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_or (int arity, ar_expr **ops)
{
  int i, k;
  ar_expr *result;
  ar_expr **args = ccl_new_array (ar_expr *, arity);

  for (k = 0, i = 0; i < arity; i++)
    {
      if (ops[i] == BOOL_TRUE)
	{
	  ccl_delete (args);
	  return ar_expr_crt_false ();
	}
      else if (ops[i] != BOOL_FALSE)
	{
	  args[k++] = ops[i];
	}
    }

  if (k == 0)
    result = ar_expr_crt_false ();
  else if (k == 1)
    result = ar_expr_add_reference (args[0]);
  else
    result = s_crt_nary (AR_EXPR_OR, k, args);
  ccl_delete (args);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_max (int arity, ar_expr **ops)
{
  return s_crt_nary (AR_EXPR_MAX, arity, ops);
}

			/* --------------- */

ar_expr *
ar_expr_crt_min (int arity, ar_expr **ops)
{
  return s_crt_nary (AR_EXPR_MIN, arity, ops);
}

			/* --------------- */

ar_expr *
ar_expr_add_reference(ar_expr *e)
{
  ccl_pre( e != NULL );
  
  e->refcount++;

  return e;
}

			/* --------------- */

void
ar_expr_del_reference(ar_expr *e)
{
  ccl_pre( e != NULL );
  
  if( --e->refcount == 0 )
    s_delete_expr(e);
}

			/* --------------- */

ar_expr_op
ar_expr_get_op (const ar_expr *e)
{
  ccl_pre( e != NULL );

  return e->op;
}

			/* --------------- */

ar_expr **
ar_expr_get_args(ar_expr *e, int *psize)
{
  ccl_pre( e != NULL );

  if( psize )
    *psize = e->arity;

  return e->args;
}

			/* --------------- */

int
ar_expr_get_arity(ar_expr *e)
{
  ccl_pre( e != NULL );

  return e->arity;
}

			/* --------------- */

ar_context_slot *
ar_expr_get_slot(ar_expr *e)
{
  ccl_pre( e != NULL );
  ccl_pre( e->op == AR_EXPR_VAR || e->op == AR_EXPR_PARAM );

  return ar_context_slot_add_reference(e->u.slot);
}

			/* --------------- */

ar_identifier *
ar_expr_get_name (ar_expr *e)
{
  ccl_pre (e != NULL);
  ccl_pre (e->op == AR_EXPR_VAR || e->op == AR_EXPR_PARAM);

  return ar_context_slot_get_name (e->u.slot);
}

			/* --------------- */

ccl_list *
ar_expr_get_attributes (ar_expr *e)
{
  ccl_pre (e != NULL);
  ccl_pre (e->op == AR_EXPR_VAR || e->op == AR_EXPR_PARAM);

  return ar_context_slot_get_attributes (e->u.slot);
}

			/* --------------- */

ccl_list *
ar_expr_get_quantified_variables(ar_expr *e)
{
  ccl_pre( e != NULL );
  ccl_pre( e->op == AR_EXPR_EXIST || e->op == AR_EXPR_FORALL );

  return e->u.qvars;
}

			/* --------------- */

ccl_list *
ar_expr_get_quantified_variables_rec (ar_expr *e, ccl_list *result)
{
  ccl_pair *p;
  int i;

  if (result == NULL)
    result = ccl_list_create ();

   switch (e->op) 
    {
    case AR_EXPR_CST : case AR_EXPR_VAR : case AR_EXPR_PARAM :
    case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER :
      break;

    case AR_EXPR_EXIST: case AR_EXPR_FORALL:
      for (p = FIRST (e->u.qvars); p; p = CDR (p))
	{
	  ccl_list_add (result, ar_context_add_reference (e->qctx));
	  ccl_list_add (result, ar_expr_add_reference (CAR (p)));
	}

    case AR_EXPR_ITE : case AR_EXPR_NOT : case AR_EXPR_NEG : case AR_EXPR_AND : 
    case AR_EXPR_OR : case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : 
    case AR_EXPR_DIV : case AR_EXPR_MOD : case AR_EXPR_LT : case AR_EXPR_GT : 
    case AR_EXPR_LEQ : case AR_EXPR_GEQ : case AR_EXPR_EQ : case AR_EXPR_NEQ : 
    case AR_EXPR_CALL : case AR_EXPR_ARRAY : case AR_EXPR_STRUCT : 
    case AR_EXPR_MIN : case AR_EXPR_MAX : 
      for (i = 0; i < e->arity; i++)
	ar_expr_get_quantified_variables_rec (e->args[i], result);
      break;

    default:
      ccl_unreachable ();
      break;
    } 

   return result;
}

			/* --------------- */

ar_context *
ar_expr_get_quantifier_context (ar_expr *e)
{
  ccl_pre (e != NULL);
  ccl_pre (e->op == AR_EXPR_EXIST || e->op == AR_EXPR_FORALL);

  return ar_context_add_reference (e->qctx);
}

			/* --------------- */

void
ar_expr_log (ccl_log_type log, const ar_expr *e)
{
  ar_expr_log_gen (log, e, ar_identifier_log_quote);
}

			/* --------------- */

void
ar_expr_log_gen (ccl_log_type log, const ar_expr *e,
		 void (*idlog) (ccl_log_type log, const ar_identifier *id))
{
  switch (e->op) 
    {
    case AR_EXPR_CST :
      ar_constant_log (log, e->u.cst);
      break;
    case AR_EXPR_VAR : 
    case AR_EXPR_PARAM :
      {
	ar_identifier *id = ar_context_slot_get_name (e->u.slot);
	idlog (log, id);
	ar_identifier_del_reference (id);
      }
      break;
    case AR_EXPR_ITE :
      ccl_log (log, "if ");
      ar_expr_log_gen (log, e->args[0], idlog);
      ccl_log (log, " then ");
      ar_expr_log_gen (log, e->args[1], idlog);
      ccl_log (log, " else ");
      ar_expr_log_gen (log, e->args[2], idlog);
      break;
    case AR_EXPR_STRUCT_MEMBER :
      ar_expr_log_gen (log, e->args[0], idlog);
      ccl_log (log, ".");
      idlog (log, e->u.field);
      break;
    case AR_EXPR_ARRAY_MEMBER :
      ar_expr_log_gen (log, e->args[0], idlog);
      ccl_log (log, "[");
      ar_expr_log_gen (log, e->args[1], idlog);
      ccl_log (log, "]");
      break;

    case AR_EXPR_NOT : 
    case AR_EXPR_NEG :
      if ('a' <= OP_PROPS[e->op].name[0] && OP_PROPS[e->op].name[0] <= 'z' )
	ccl_log (log, "%s ", OP_PROPS[e->op].name);
      else
	ccl_log (log, "%s", OP_PROPS[e->op].name);

      if (OP_PROPS[e->op].priority > OP_PROPS[e->args[0]->op].priority)
	ccl_log (log, "(");
      ar_expr_log_gen (log, e->args[0], idlog);
      if (OP_PROPS[e->op].priority > OP_PROPS[e->args[0]->op].priority)
	ccl_log (log, ")");
      break;

    case AR_EXPR_AND : case AR_EXPR_OR : 
    case AR_EXPR_ADD : case AR_EXPR_SUB : 
    case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD : 
    case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
    case AR_EXPR_EQ : case AR_EXPR_NEQ :
      {
	int i;

      if (OP_PROPS[e->op].priority >= OP_PROPS[e->args[0]->op].priority)
	ccl_log (log, "(");
      ar_expr_log_gen (log, e->args[0], idlog);
      if (OP_PROPS[e->op].priority >= OP_PROPS[e->args[0]->op].priority)
	ccl_log (log, ")");

      for (i = 1; i < e->arity; i++)
	{
	  if ('a' <= OP_PROPS[e->op].name[0] && OP_PROPS[e->op].name[0] <= 'z')
	    ccl_log (log, " %s ", OP_PROPS[e->op].name);
	  else
	    ccl_log (log, "%s", OP_PROPS[e->op].name);

	  if (OP_PROPS[e->op].priority >= OP_PROPS[e->args[i]->op].priority)
	    ccl_log (log, "(");
	  ar_expr_log_gen (log, e->args[i], idlog);
	  if (OP_PROPS[e->op].priority >= OP_PROPS[e->args[i]->op].priority)
	    ccl_log (log, ")");
	}
      }
      break;

    case AR_EXPR_EXIST : case AR_EXPR_FORALL :
      {
	ccl_pair *p;
	
	ccl_log (log, "%c", OP_PROPS[e->op].name[0]);
	for(p = FIRST (e->u.qvars); p; p = CDR (p))
	  {
	    ar_expr *var = (ar_expr *) CAR (p);
	    
	    ar_expr_log_gen (log, var, idlog);
	    ccl_log (log, " : ");
	    ar_type_log (log, var->type);
	    
	    if (CDR (p) != NULL)
	      ccl_log (log, "; ");
	  }
	ccl_log (log, "%c", OP_PROPS[e->op].name[1]);
	
	ccl_log (log, "(");
	ar_expr_log_gen (log, e->args[0], idlog);
	ccl_log (log, ")");
      }  
      break;

    case AR_EXPR_MIN:
      ccl_log (log, "min");
      goto dispargs;
    case AR_EXPR_MAX:
      ccl_log (log, "max");
      goto dispargs;

    case AR_EXPR_CALL:      
      {
	ar_identifier *fname = ar_signature_get_name (e->u.sig);
	idlog (log, fname);
	ar_identifier_del_reference (fname);
      }

    dispargs:
      ccl_log (log, "(");
      if (e->arity > 0)
	{
	  int i;
	  
	  for (i = 0; i < e->arity; i++)
	    {
	      ar_expr_log_gen (log, e->args[i], idlog);
	      if (i < e->arity - 1)
		ccl_log (log, ",");
	    }
	}
      ccl_log (log, ")");
      break;

    case AR_EXPR_ARRAY : 
    case AR_EXPR_STRUCT :
      {
	int i;
	
	ccl_log (log, "{");
	for(i = 0; i < e->arity; i++)
	  {
	    if (e->op == AR_EXPR_STRUCT)
	      {
		ccl_log (log, ".");
		idlog (log, e->u.fnames[i]);
		ccl_log (log, " = ");
	      }
	    ar_expr_log_gen (log, e->args[i], idlog);
	    if (i < e->arity - 1)
	      ccl_log (log, ", ");
	  }
	ccl_log (log, "}");
      }
      break;

    default:
      ccl_unreachable ();
      break;
    }
}

void
ar_expr_log_expr_list (ccl_log_type log, const ccl_list *l)
{
  const ccl_pair *p;

  for(p = FIRST (l); p; p = CDR (p))
    {
      ar_expr_log (log, CAR (p));
      ccl_log (log, ", ");
    }
  ccl_log (log, "\n");
}

			/* --------------- */

ar_signature *
ar_expr_call_get_signature (const ar_expr *e)
{
  ccl_pre (e != NULL); 
  ccl_pre (e->op == AR_EXPR_CALL);

  return ar_signature_add_reference (e->u.sig);
}

			/* --------------- */

ccl_list *
ar_expr_call_get_subtypes (ar_expr *e)
{
  ccl_pre (e != NULL); 
  ccl_pre (e->op == AR_EXPR_CALL);

  if (e->subtypes == NULL)
    {
      int i;

      e->subtypes = ccl_list_create ();
      for (i = 0; i < e->arity; i++)
	{
	  ar_type *argtype = ar_signature_get_ith_arg_domain (e->u.sig, i);
	  ar_type_expand (argtype, e->subtypes);
	  ar_type_del_reference (argtype);
	}
    }

  return e->subtypes;
}

			/* --------------- */

ccl_list *
ar_expr_call_get_subargs (ar_expr *e, ar_context *ctx)
{
  ccl_pre (e != NULL); 
  ccl_pre (e->op == AR_EXPR_CALL);

  if (e->subargs == NULL)
    {
      int i;

      e->subargs = ccl_list_create ();
      for (i = 0; i < e->arity; i++)
	ar_expr_expand_composite_types(e->args[i], ctx, e->subargs);
    }

  return e->subargs;
}

			/* --------------- */

int
ar_expr_is_true(ar_expr *e)
{
  ccl_pre( e != NULL && e->type == AR_BOOLEANS );

  return e == BOOL_TRUE;
}

			/* --------------- */

int
ar_expr_is_false(ar_expr *e)
{
  ccl_pre( e != NULL && e->type == AR_BOOLEANS );

  return e == BOOL_FALSE;
}

			/* --------------- */

ar_type_kind
ar_expr_get_type_kind(ar_expr *e)
{
  ccl_pre( e != NULL ); ccl_pre( e->type != NULL ); 

  return ar_type_get_kind(e->type);
}

			/* --------------- */

ar_type *
ar_expr_get_type(ar_expr *e)
{
  ar_type *result;

  ccl_pre( e != NULL );

  if( (result = e->type) != NULL )
    result = ar_type_add_reference(e->type);
  
  return result;
}
			/* --------------- */

int
ar_expr_has_base_type(ar_expr *e, ar_type *t)
{
  return ar_type_have_same_base_type(e->type,t);
}

			/* --------------- */

int
ar_expr_is_constant(ar_expr *e, ar_context *ctx)
{
  int result = 1;

  ccl_pre( e != NULL ); 

  switch( e->op ) {
  case AR_EXPR_NOT : case AR_EXPR_NEG :
  case AR_EXPR_ITE : case AR_EXPR_OR : case AR_EXPR_AND :
  case AR_EXPR_EQ : case AR_EXPR_NEQ :
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
  case AR_EXPR_ADD : case AR_EXPR_SUB : 
  case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD :
  case AR_EXPR_CALL:  case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER :
  case AR_EXPR_MIN : case AR_EXPR_MAX :
    {
      int i;

      for(i = 0; i < e->arity && result; i++)
	result = ar_expr_is_constant(e->args[i],ctx);
    }
    break;

  case AR_EXPR_VAR :
    result = 0;
    break;

  case AR_EXPR_CST : case AR_EXPR_PARAM : 
    result = 1;
    break;

  case AR_EXPR_EXIST : case AR_EXPR_FORALL :
    {
      ccl_list *vars = ar_expr_get_variables(e,ctx,NULL);
      result = (ccl_list_get_size(vars) == 0);
      ccl_list_delete(vars);
    }
    break;
  }

  return result;
}

			/* --------------- */

static int
s_eval_boolean(ar_expr *e, ar_context *ctx);

static int
s_eval_integer(ar_expr *e, ar_context *ctx)
{
  int result = 0;

  ccl_pre( ar_expr_has_base_type(e,AR_INTEGERS) );

  switch( e->op ) {
  case AR_EXPR_ITE :
    if( s_eval_boolean(e->args[0], ctx) ) 
      result = s_eval_integer(e->args[1], ctx);
    else 
      result = s_eval_integer(e->args[2], ctx);
    break;

  case AR_EXPR_ADD : 
    result = s_eval_integer(e->args[0], ctx)+s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_SUB : 
    result = s_eval_integer(e->args[0], ctx)-s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_MUL : 
    result = s_eval_integer(e->args[0], ctx)*s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_DIV : 
    result = s_eval_integer(e->args[0], ctx)/s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_MOD : 
    result = s_eval_integer(e->args[0], ctx)%s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_NEG : 
    result = -s_eval_integer(e->args[0], ctx);
    break;

  case AR_EXPR_CST : 
    result = ar_constant_get_integer_value(e->u.cst);
    break;

  case AR_EXPR_PARAM :  case AR_EXPR_VAR : 
    {
      ar_constant *cst;
      ar_expr *value = ar_context_slot_get_value(e->u.slot);

      ccl_assert( value != NULL && ar_expr_is_computable(value) );

      cst = ar_expr_eval(value, ctx);
      result = ar_constant_get_integer_value(cst);
      ar_constant_del_reference(cst);
      ar_expr_del_reference(value);
    }
    break;

  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : 
    {
      ar_constant *value = ar_expr_eval(e, ctx);
      result = ar_constant_get_integer_value(value);
      ar_constant_del_reference(value);
    }
    break;
  case AR_EXPR_MIN: 
    {
      int i;
      result = s_eval_integer(e->args[0], ctx);
      for (i = 1; i < e->arity; i++)
	{
	  int v = s_eval_integer(e->args[i], ctx);
	  if (v < result)
	    result = v;
	}
    }
    break;
  case AR_EXPR_MAX:
    {
      int i;
      result = s_eval_integer(e->args[0], ctx);
      for (i = 1; i < e->arity; i++)
	{
	  int v = s_eval_integer(e->args[i], ctx);
	  if (v > result)
	    result = v;
	}
    }
    break;
  case AR_EXPR_OR : case AR_EXPR_AND : case AR_EXPR_NOT : case AR_EXPR_EQ : 
  case AR_EXPR_NEQ : case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
  case AR_EXPR_GEQ : case AR_EXPR_EXIST : case AR_EXPR_FORALL : 
  case AR_EXPR_CALL : case AR_EXPR_ARRAY : 
  case AR_EXPR_STRUCT :
    ccl_throw_no_msg(internal_error);
    break;
  }

  return result;
}

			/* --------------- */


static int
s_eval_boolean(ar_expr *e, ar_context *ctx)
{
  int result = 0;

  ccl_pre( ar_expr_has_base_type(e,AR_BOOLEANS) );

  switch( e->op ) {
  case AR_EXPR_ITE :
    if( s_eval_boolean(e->args[0], ctx) ) 
      result = s_eval_boolean(e->args[1], ctx);
    else 
      result = s_eval_boolean(e->args[2], ctx);
    break;
  case AR_EXPR_OR : 
    {
      int i;
      for(i = 0; i < e->arity && ! result; i++)
	result = s_eval_boolean(e->args[i], ctx);
    }
    break;

  case AR_EXPR_AND :
    {
      int i;
      for(result = 1, i = 0; i < e->arity && result; i++)
	result = s_eval_boolean(e->args[i], ctx);
    }
    break;
  case AR_EXPR_NOT :
    result = ! s_eval_boolean(e->args[0], ctx);
    break;
  case AR_EXPR_EQ : 
    {
      ar_constant *c1 = ar_expr_eval(e->args[0], ctx);
      ar_constant *c2 = ar_expr_eval(e->args[1], ctx);
      result = ar_constant_are_equal(c1,c2);
      ar_constant_del_reference(c1);
      ar_constant_del_reference(c2);
    }
    break;
  case AR_EXPR_NEQ :
    {
      ar_constant *c1 = ar_expr_eval(e->args[0], ctx);
      ar_constant *c2 = ar_expr_eval(e->args[1], ctx);
      result = ! ar_constant_are_equal(c1,c2);
      ar_constant_del_reference(c1);
      ar_constant_del_reference(c2);
    }
    break;

  case AR_EXPR_CST :
    result = ar_constant_get_boolean_value(e->u.cst);
    break;

  case AR_EXPR_PARAM : 
    {
      ar_constant *cst;
      ar_expr *value = ar_context_slot_get_value(e->u.slot);

      ccl_assert( value != NULL && ar_expr_is_computable(value) );

      cst = ar_expr_eval(value, ctx);
      result = ar_constant_get_boolean_value(cst);
      ar_constant_del_reference(cst);
      ar_expr_del_reference(value);
    }
    break;

  case AR_EXPR_VAR :
  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : 
    {
      ar_constant *value = ar_expr_eval(e, ctx);
      result = ar_constant_get_boolean_value(value);
      ar_constant_del_reference(value);
    }
    break;

  case AR_EXPR_LT : 
    result = s_eval_integer(e->args[0], ctx) < s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_GT : 
    result = s_eval_integer(e->args[0], ctx) > s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_LEQ : 
    result = s_eval_integer(e->args[0], ctx) <= s_eval_integer(e->args[1], ctx);
    break;
  case AR_EXPR_GEQ :
    result = s_eval_integer(e->args[0], ctx) >= s_eval_integer(e->args[1], ctx);
    break;

  case AR_EXPR_EXIST : case AR_EXPR_FORALL :    
    ccl_throw(runtime_exception,"quantifiers are not supported in constants.");
    break;

  case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
  case AR_EXPR_MOD : case AR_EXPR_NEG : 
  case AR_EXPR_CALL : case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
  case AR_EXPR_MIN : case AR_EXPR_MAX :
    ccl_unreachable ();
    break;
  }

  return result;
}

			/* --------------- */

ar_constant *
ar_expr_eval(ar_expr *e, ar_context *ctx)
{
  ar_constant *result = NULL;

  /*
  ccl_pre( ar_expr_is_constant(e) );
  ccl_pre( ar_expr_is_computable(e) );
  */
  switch( e->op ) {
    int val;

  case AR_EXPR_ITE :
    if( s_eval_boolean(e->args[0], ctx) )
      result = ar_expr_eval(e->args[1], ctx);
    else
      result = ar_expr_eval(e->args[2], ctx);
    break;
  case AR_EXPR_OR : case AR_EXPR_AND : case AR_EXPR_EQ : case AR_EXPR_NEQ :
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
  case AR_EXPR_NOT : case AR_EXPR_EXIST : case AR_EXPR_FORALL :
    val = s_eval_boolean(e, ctx);
    result = ar_constant_crt_boolean(val);
    break;

  case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
  case AR_EXPR_MOD :case AR_EXPR_NEG : case AR_EXPR_MIN : case AR_EXPR_MAX :
    val = s_eval_integer(e, ctx);
    result = ar_constant_crt_integer(val);
    break;

  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : 
    if (ar_expr_is_constant (e, ctx) && ar_expr_is_computable (e))
      {
	ar_constant *cst = ar_expr_eval (e->args[0], ctx);
	
	if( e->op == AR_EXPR_STRUCT_MEMBER )
	  result = ar_constant_get_field(cst,e->u.field);
	else
	  result = ar_constant_get_at(cst,s_eval_integer(e->args[1], ctx));
	ar_constant_del_reference(cst);
      }
    else
      {
	ar_context_slot *sl = ar_expr_eval_slot (e, ctx);
	ar_expr *value = ar_context_slot_get_value(sl);
	ccl_assert( value != NULL && ar_expr_is_computable(value) );
	result = ar_expr_eval(value, ctx);
	ar_expr_del_reference(value);
	ar_context_slot_del_reference (sl);
      }
    break;

  case AR_EXPR_CST :
    result = ar_constant_add_reference(e->u.cst);
    break;

  case AR_EXPR_PARAM :  case AR_EXPR_VAR :
    {
      ar_expr *value = ar_context_slot_get_value(e->u.slot);
      ccl_assert( value != NULL && ar_expr_is_computable(value) );
      result = ar_expr_eval(value, ctx);
      ar_expr_del_reference(value);
    }
    break;
    
  case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
    {
      int i;
      ar_constant **a = ccl_new_array(ar_constant *,e->arity);

      for(i = 0; i < e->arity; i++)
	a[i] = ar_expr_eval(e->args[i], ctx);
      if( e->op == AR_EXPR_ARRAY )
	result = ar_constant_crt_array(a,e->arity,e->type);
      else
	result = ar_constant_crt_structure(e->u.fnames,a,e->arity,e->type);
      for(i = 0; i < e->arity; i++)
	ar_constant_del_reference(a[i]);
      ccl_delete(a);
    }

    break;

  case AR_EXPR_CALL :
    ccl_throw_no_msg(internal_error);
    break;
  }

  return result;
}

			/* --------------- */

static ar_identifier *
s_slot_id_rec(const ar_expr *m, ar_context *ctx)
{
  ar_identifier *aux;
  ar_identifier *result = NULL;

  switch( m->op ) {
  case AR_EXPR_VAR : case AR_EXPR_PARAM :
    result = ar_context_slot_get_name(m->u.slot);
    break;

  case AR_EXPR_STRUCT_MEMBER :
    aux = s_slot_id_rec(m->args[0], ctx);
    result = ar_identifier_add_prefix(m->u.field,aux);
    ar_identifier_del_reference(aux);
    break;

  case AR_EXPR_ARRAY_MEMBER : 
    {
      ar_constant *e_pos = ar_expr_eval(m->args[1], ctx);
      int i_pos = ar_constant_get_integer_value(e_pos);
      char *s_pos = ccl_string_format_new("[%d]",i_pos);
      ar_constant_del_reference(e_pos);

      aux = s_slot_id_rec(m->args[0], ctx);

      result = ar_identifier_rename_with_suffix(aux,s_pos);
      ar_identifier_del_reference(aux);
      ccl_string_delete(s_pos);
    }
    break;

  default : ccl_unreachable ();
  }

  ccl_post( result != NULL );

  return result;
}

			/* --------------- */

ar_context_slot *
ar_expr_eval_slot (ar_expr *m, ar_context *ctx)
{ 
  ar_identifier *id = s_slot_id_rec (m, ctx);
  ar_context_slot *slot = ar_context_get_slot (ctx, id);
  ar_identifier_del_reference (id);

  return slot;
}

			/* --------------- */

static void
s_collect_variables(ar_expr *e, ar_context *ctx, ccl_list *result)
{
  switch( e->op ) {
  case AR_EXPR_ITE :
  case AR_EXPR_OR : case AR_EXPR_AND : case AR_EXPR_EQ : case AR_EXPR_NEQ :
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
  case AR_EXPR_GEQ : case AR_EXPR_ADD : case AR_EXPR_SUB :
  case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD :
  case AR_EXPR_NOT : case AR_EXPR_NEG :
  case AR_EXPR_CALL : case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
  case AR_EXPR_MIN : case AR_EXPR_MAX :
    {
      int i;
      for(i = 0; i < e->arity; i++)
	s_collect_variables(e->args[i],ctx,result);
    }
    break;

  case AR_EXPR_EXIST : case AR_EXPR_FORALL :
    {
      ccl_pair *p;
      s_collect_variables(e->args[0],ctx,result);
      for(p = FIRST(e->u.qvars); p; p = CDR(p))
	{
	  ar_expr *var = (ar_expr *)CAR(p);
	  if( ccl_list_has(result,var->u.slot) )
	    {
	      ccl_list_remove(result,var->u.slot);
	      ar_context_slot_del_reference(var->u.slot);
	    }
	}
    }
    break;

  case AR_EXPR_VAR :
    if( ! ccl_list_has(result,e->u.slot) )
      ccl_list_add(result,ar_context_slot_add_reference(e->u.slot));
    break;

  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : 
    if( ! ar_expr_is_constant(e,ctx) )
      {
	ar_context_slot *s = ar_expr_eval_slot(e,ctx);
	if( ccl_list_has(result,s) )
	  ar_context_slot_del_reference(s);
	else
	  ccl_list_add(result,s);
      }
    break;

  case AR_EXPR_CST : case AR_EXPR_PARAM :
    break;
  }
}

			/* --------------- */

ccl_list *
ar_expr_get_variables(ar_expr *e, ar_context *ctx, ccl_list *result)
{
  if( result == NULL )
    result = ccl_list_create();
  s_collect_variables(e,ctx,result);

  return result;
}

			/* --------------- */

static void
s_collect_slots (ar_expr *e, ar_context *ctx, ccl_list *result)
{
  switch( e->op ) {
  case AR_EXPR_EXIST : case AR_EXPR_FORALL:
  case AR_EXPR_ITE :
  case AR_EXPR_OR : case AR_EXPR_AND : case AR_EXPR_EQ : case AR_EXPR_NEQ :
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
  case AR_EXPR_GEQ : case AR_EXPR_ADD : case AR_EXPR_SUB :
  case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD :
  case AR_EXPR_NOT : case AR_EXPR_NEG :
  case AR_EXPR_CALL : case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
  case AR_EXPR_MIN : case AR_EXPR_MAX :
    {
      int i;

      if (e->op == AR_EXPR_EXIST || e->op == AR_EXPR_FORALL)
	ctx = e->qctx;

      for(i = 0; i < e->arity; i++)
	s_collect_slots (e->args[i], ctx, result);
    }
    break;

  case AR_EXPR_VAR :
    ar_context_expand_composite_slot (ctx, e->u.slot, result);
    break;

  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : 
    if( ! ar_expr_is_constant(e,ctx) )
      {
	ar_context_slot *s = ar_expr_eval_slot (e, ctx);
	ar_context_expand_composite_slot (ctx, s, result);
	ar_context_slot_del_reference(s);
      }
    break;

  case AR_EXPR_CST : case AR_EXPR_PARAM :
    break;  
  }
}

			/* --------------- */

ccl_list *
ar_expr_get_slots (ar_expr *e, ar_context *ctx, ccl_list *result)
{
  if( result == NULL )
    result = ccl_list_create();
  s_collect_slots (e, ctx, result);

  return result;
}

			/* --------------- */

int
s_expr_is_computable (ar_expr *e)
{
  int result = 1;

  switch( e->op ) {
  case AR_EXPR_CALL : 
    result = 0; 
    break;

  case AR_EXPR_PARAM : 
    {
      ar_expr *value = ar_context_slot_get_value(e->u.slot);
      result = value != NULL && ar_expr_is_computable(value);
      ccl_zdelete(ar_expr_del_reference,value);
    }
    break;

  case AR_EXPR_VAR : 
    result = 0;
    break;

  case AR_EXPR_CST : 
    break;

  case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
  case AR_EXPR_ITE :
  case AR_EXPR_STRUCT_MEMBER :
  case AR_EXPR_NOT : case AR_EXPR_NEG :
  case AR_EXPR_OR : case AR_EXPR_AND :
  case AR_EXPR_EQ : case AR_EXPR_NEQ :
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
  case AR_EXPR_ADD : case AR_EXPR_SUB : 
  case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD :
  case AR_EXPR_ARRAY_MEMBER :
  case AR_EXPR_EXIST : case AR_EXPR_FORALL : case AR_EXPR_MIN :
  case AR_EXPR_MAX :
    {
      int i;

      for(i = 0; i < e->arity && result; i++)
	result = ar_expr_is_computable(e->args[i]);
    }
    break;
  }

  return result;
}

int
ar_expr_is_computable(ar_expr *e)
{
  int result = 1;

  /*ccl_pre( ar_expr_is_constant(e) );*/

  switch( e->op ) {
  case AR_EXPR_CALL : 
    result = 0; 
    break;

  case AR_EXPR_PARAM : 
    {
      ar_expr *value = ar_context_slot_get_value(e->u.slot);
      result = value != NULL && ar_expr_is_computable(value);
      ccl_zdelete(ar_expr_del_reference,value);
    }
    break;

  case AR_EXPR_VAR : 
    result = 0;
    break;

  case AR_EXPR_CST : 
    break;

  case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
  case AR_EXPR_ITE :
  case AR_EXPR_STRUCT_MEMBER :
  case AR_EXPR_NOT : case AR_EXPR_NEG :
  case AR_EXPR_OR : case AR_EXPR_AND :
  case AR_EXPR_EQ : case AR_EXPR_NEQ :
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
  case AR_EXPR_ADD : case AR_EXPR_SUB : 
  case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD :
  case AR_EXPR_ARRAY_MEMBER :
  case AR_EXPR_EXIST : case AR_EXPR_FORALL : case AR_EXPR_MIN :
  case AR_EXPR_MAX :
    {
      int i;

      for(i = 0; i < e->arity && result; i++)
	result = ar_expr_is_computable(e->args[i]);
    }
    break;
  }

  return result;
}

			/* --------------- */

char *
ar_expr_to_string(ar_expr *e)
{
  char *aux;
  char *fmt;
  char **ops = NULL;
  char *result = NULL;

  if( e->arity )
    {
      int i;
      ops = ccl_new_array(char *,e->arity);
      for(i = 0; i < e->arity; i++)
	ops[i] = ar_expr_to_string(e->args[i]);
    }

  switch( e->op ) {
  case AR_EXPR_CST :
    result = ar_constant_to_string(e->u.cst);
    break;

  case AR_EXPR_VAR : case AR_EXPR_PARAM :
    {
      ar_identifier *id = ar_context_slot_get_name(e->u.slot);
      result = ar_identifier_to_string(id);
      ar_identifier_del_reference(id);
    }
    break;

  case AR_EXPR_ITE :
    result = 
      ccl_string_format_new("if %s then %s else %s",ops[0],ops[1],ops[2]);
    break;

  case AR_EXPR_STRUCT_MEMBER :
    aux = ar_identifier_to_string(e->u.field);
    result = ccl_string_format_new("%s.%s",ops[0],aux);
    ccl_delete(aux);
    break;

  case AR_EXPR_ARRAY_MEMBER :
    result = ccl_string_format_new("%s[%s]",ops[0],ops[1]);
    break;

  case AR_EXPR_NOT : case AR_EXPR_NEG :
    if( 'a' <= OP_PROPS[e->op].name[0] && OP_PROPS[e->op].name[0] <= 'z' )
      aux = " ";
    else
      aux = "";

    if( OP_PROPS[e->op].priority > OP_PROPS[e->args[0]->op].priority )
      fmt = "%s%s(%s)";
    else
      fmt = "%s%s%s";
    ccl_string_format_append(&result,fmt,OP_PROPS[e->op].name,aux,ops[0]);
    break;

  case AR_EXPR_AND : case AR_EXPR_OR : case AR_EXPR_ADD : case AR_EXPR_SUB : 
  case AR_EXPR_MUL : case AR_EXPR_DIV : case AR_EXPR_MOD : 
  case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : case AR_EXPR_GEQ :
  case AR_EXPR_EQ : case AR_EXPR_NEQ :
    {
      int i;
      
      if( OP_PROPS[e->op].priority >= OP_PROPS[e->args[0]->op].priority )
	fmt = "(%s)";
      else
	fmt = "%s";

      result = ccl_string_format_new(fmt,ops[0]);
      for(i = 0; i < e->arity; i++)
	{
	  if('a' <= OP_PROPS[e->op].name[0] && OP_PROPS[e->op].name[0] <= 'z')
	    aux = " ";
	  else
	    aux = "";
      
	  if( OP_PROPS[e->op].priority >= OP_PROPS[e->args[i]->op].priority )
	    fmt = "%s%s%s(%s)";
	  else
	    fmt = "%s%s%s%s";
	  ccl_string_format_append(&result,fmt,aux,OP_PROPS[e->op].name,aux,
				   ops[i]);
	}
    }
    break;

  case AR_EXPR_EXIST : case AR_EXPR_FORALL :
    {
      ccl_pair *p;

      fmt = "%s : %s, ";
      result = ccl_string_format_new("%c",OP_PROPS[e->op].name[0]);
      for(p = FIRST(e->u.qvars); p; p = CDR(p))
	{
	  ar_expr *var = (ar_expr *)CAR(p);
	  char *vstr = ar_expr_to_string(var);
	  char *tstr = ar_type_to_string(var->type);

	  if( CDR(p) == NULL )
	    fmt = "%s : %s";
	  ccl_string_format_append(&result,fmt,vstr,tstr);
	  ccl_string_delete(vstr);
	  ccl_string_delete(tstr);
	}
      ccl_string_format_append(&result,"%c(%s)",OP_PROPS[e->op].name[1],
			       ops[0]);
    }
    break;

  case AR_EXPR_MIN: result = ccl_string_dup ("min"); goto args;
  case AR_EXPR_MAX: result = ccl_string_dup ("max"); goto args;
  case AR_EXPR_CALL:
    {
      ar_identifier *fname = ar_signature_get_name(e->u.sig);
      result = ar_identifier_to_string(fname);
      ar_identifier_del_reference(fname);
    }

    args:
      if( e->arity > 0 )
	{
	  int i;

	  fmt = "%s,";
	  ccl_string_format_append(&result,"(");
	  for(i = 0; i < e->arity; i++)
	    {
	      if( i == e->arity-1 )
		fmt = "%s";
	      ccl_string_format_append(&result,"%s",ops[i]);
	    }
	  ccl_string_format_append(&result,")");
	}
    break;

  case AR_EXPR_ARRAY : case AR_EXPR_STRUCT :
    {
      int i;
      
      fmt = "%s, ";
      result = ccl_string_dup("{ ");
      for(i = 0; i < e->arity; i++)
	{
	  if( e->op == AR_EXPR_STRUCT )
	    {
	      aux = ar_identifier_to_string(e->u.fnames[i]);
	      ccl_string_format_append(&result,".%s = ",aux);
	      ccl_string_delete(aux);
	    }
	  if( i == e->arity-1 )
	    fmt = "%s";
	  ccl_string_format_append(&result,fmt,ops[i]);
	}
      ccl_string_format_append(&result," }");
    }
    break;
  }

  if( ops != NULL )
    {
      int i;

      for(i = 0; i < e->arity; i++)
	ccl_zdelete (ccl_string_delete, ops[i]);
      ccl_delete(ops);
    }
  
  return result;
}

			/* --------------- */

ar_expr *
ar_expr_replace(ar_expr *e, ar_context_slot *to_replace, ar_expr *newvalue)
{
  int args_changed = 0;
  ar_expr **ops = NULL;
  ar_expr *result = NULL;

  ccl_pre( e != NULL ); ccl_pre( to_replace != NULL ); 
  ccl_pre( newvalue != NULL );


  if( e->arity )
    {
      int i;
      ops = ccl_new_array(ar_expr *,e->arity);
      for(i = 0; i < e->arity; i++)
	{
	  ops[i] = ar_expr_replace(e->args[i],to_replace,newvalue);
	  if (ops[i] != e->args[i])
	    args_changed = 1;
	}
    }

  if (e->op ==AR_EXPR_VAR && e->u.slot == to_replace )
    result = ar_expr_add_reference (newvalue);
  else if (! args_changed)
    result = ar_expr_add_reference(e);
  else 
    {
      switch( e->op ) {
      case AR_EXPR_VAR : case AR_EXPR_CST :  case AR_EXPR_PARAM :
	ccl_throw_no_msg(internal_error);
	break;


      case AR_EXPR_STRUCT_MEMBER :
	result = ar_expr_crt_struct_member(ops[0],e->u.field);
	break;

      case AR_EXPR_ITE :
      case AR_EXPR_NOT : case AR_EXPR_NEG :
      case AR_EXPR_ARRAY_MEMBER : case AR_EXPR_AND : case AR_EXPR_OR : 
      case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
      case AR_EXPR_MOD : case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
      case AR_EXPR_GEQ : case AR_EXPR_EQ : case AR_EXPR_NEQ :
      case AR_EXPR_MAX : case AR_EXPR_MIN :
	result = s_crt_nary(e->op,e->arity,ops);
	break;

      case AR_EXPR_EXIST : case AR_EXPR_FORALL :
	result = ar_expr_crt_quantifier(e->op,e->qctx,e->u.qvars,ops[0]);
	break;
      
      case AR_EXPR_CALL:
	result = ar_expr_crt_function_call(e->u.sig,ops);
	break;
      
      case AR_EXPR_ARRAY : 
	result = ar_expr_crt_array(ops,e->arity);
	break;
      
      case AR_EXPR_STRUCT :
	result = ar_expr_crt_structure(e->u.fnames,ops,e->arity);
	break;
      }
    }
  if( ops != NULL )
    {
      int i;

      for(i = 0; i < e->arity; i++)
	ar_expr_del_reference(ops[i]);
      ccl_delete(ops);
    }
  
  return result;
}

			/* --------------- */

ar_expr *
ar_expr_replace_with_tables (ar_expr *e, ar_context *ctx, ccl_hash **subst, 
			     int nb_tables, ccl_hash *cache)
{
  ar_expr **ops = NULL;
  ar_expr *result = NULL;

  ccl_pre (e != NULL); 
  ccl_pre (subst != NULL); 

  if (cache && ccl_hash_find (cache, e))
    {
      result = ccl_hash_get (cache);
      result = ar_expr_add_reference (result);

      return result;
    }
  if (e->arity)
    {
      int i;
      ops = ccl_new_array (ar_expr *, e->arity);
      if (!(e->op == AR_EXPR_EXIST || e->op == AR_EXPR_FORALL))
	{
	  for (i = 0; i < e->arity; i++)
	    ops[i] = ar_expr_replace_with_tables (e->args[i], ctx, subst,
						  nb_tables, cache);
	}
    }

  switch (e->op) 
    {
    case AR_EXPR_VAR : case AR_EXPR_PARAM :
      {
	int i;
	result = NULL;
	
	for (i = 0; result == NULL && i < nb_tables; i++)
	  {
	    if (!ccl_hash_find (subst[i], e->u.slot))
	      continue;
	    result = ccl_hash_get (subst[i]);
	    result = ar_expr_add_reference (result);
	  }
	
	if (result == NULL)
	  result = ar_expr_add_reference (e);
      }
      break;

    case AR_EXPR_CST : 
      result = ar_expr_add_reference (e);
      break;

    case AR_EXPR_STRUCT_MEMBER :
      result = ar_expr_crt_struct_member (ops[0], e->u.field);
      break;

    case AR_EXPR_ITE :
    case AR_EXPR_NOT : case AR_EXPR_NEG :
    case AR_EXPR_ARRAY_MEMBER : case AR_EXPR_AND : case AR_EXPR_OR : 
    case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
    case AR_EXPR_MOD : case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
    case AR_EXPR_GEQ : case AR_EXPR_EQ : case AR_EXPR_NEQ :
    case AR_EXPR_MIN : case AR_EXPR_MAX : 
      result = s_crt_nary (e->op, e->arity, ops);
      break;

    case AR_EXPR_EXIST : case AR_EXPR_FORALL :
      {
	ccl_pair *p;
	ar_context *qctx;
	ccl_list *l_slots = ar_context_get_local_slots (e->qctx, 0xFFFFFFFF);

	for (p = FIRST (l_slots); p; p = CDR (p))
	  {
	    ar_identifier *id = (ar_identifier *) CAR (p);
	    CAR (p) = ar_context_get_slot (e->qctx, id);
	    ar_identifier_del_reference (id);
	  }
	qctx = ar_context_create_from_slots (ctx, l_slots);
	ops[0] = ar_expr_replace_with_tables (e->args[0], qctx, subst, 
					      nb_tables, cache);
	result = ar_expr_crt_quantifier (e->op, qctx, e->u.qvars, ops[0]);
	ar_context_del_reference (qctx);
	ccl_list_clear_and_delete (l_slots, (ccl_delete_proc *)
				   ar_context_slot_del_reference);
      }
      break;

    case AR_EXPR_CALL:
      result = ar_expr_crt_function_call (e->u.sig, ops);
      break;
      
    case AR_EXPR_ARRAY : 
      result = ar_expr_crt_array(ops,e->arity);
      break;
      
    case AR_EXPR_STRUCT :
      result = ar_expr_crt_structure (e->u.fnames, ops, e->arity);
      break;
    }

  if (ops != NULL)
    {
      int i;
      
      for(i = 0; i < e->arity; i++)
	ar_expr_del_reference(ops[i]);
      ccl_delete(ops);
    }
  
  if (cache)
    {
      result = ar_expr_add_reference (result);
      ccl_hash_find (cache, e);
      ccl_hash_insert (cache, result);
    }

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_replace_with_context(ar_expr *e, ar_context *ctx)
{
  ar_expr **ops = NULL;
  ar_expr *result = NULL;

  ccl_pre( e != NULL ); ccl_pre( ctx != NULL ); 

  if( e->arity )
    {
      int i;
      ops = ccl_new_array(ar_expr *,e->arity);
      for(i = 0; i < e->arity; i++)
	ops[i] = ar_expr_replace_with_context(e->args[i],ctx);
    }

  switch( e->op ) {
  case AR_EXPR_VAR : case AR_EXPR_PARAM :
    {
      ar_identifier *name = ar_context_slot_get_name(e->u.slot);
      ar_context_slot *sl = ar_context_get_slot(ctx,name);
      if( sl == NULL )
	result = ar_expr_add_reference(e);	
      else 
	{
	  if( e->op == AR_EXPR_VAR )
	    result = ar_expr_crt_variable(sl);
	  else
	    result = ar_expr_crt_parameter(sl);
	  ar_context_slot_del_reference(sl);
	}
      ar_identifier_del_reference(name);
    }
    break;

  case AR_EXPR_CST : 
    result = ar_expr_add_reference(e);
    break;

  case AR_EXPR_STRUCT_MEMBER :
    result = ar_expr_crt_struct_member(ops[0],e->u.field);
    break;

  case AR_EXPR_ITE :
  case AR_EXPR_NOT : case AR_EXPR_NEG :
  case AR_EXPR_ARRAY_MEMBER : case AR_EXPR_AND : case AR_EXPR_OR : 
  case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
  case AR_EXPR_MOD : case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
  case AR_EXPR_GEQ : case AR_EXPR_EQ : case AR_EXPR_NEQ :
    case AR_EXPR_MIN : case AR_EXPR_MAX : 
    result = s_crt_nary(e->op,e->arity,ops);
    break;

  case AR_EXPR_EXIST : case AR_EXPR_FORALL :
    result = ar_expr_crt_quantifier(e->op,e->qctx,e->u.qvars,ops[0]);
    break;

  case AR_EXPR_CALL:
    result = ar_expr_crt_function_call(e->u.sig,ops);
    break;

  case AR_EXPR_ARRAY : 
    result = ar_expr_crt_array(ops,e->arity);
    break;

  case AR_EXPR_STRUCT :
    result = ar_expr_crt_structure(e->u.fnames,ops,e->arity);
    break;
  }

  if( ops != NULL )
    {
      int i;

      for(i = 0; i < e->arity; i++)
	ar_expr_del_reference(ops[i]);
      ccl_delete(ops);
    }
  
  return result;
}

			/* --------------- */
static int
s_is_var (ar_expr *e)
{
  if (e->op == AR_EXPR_VAR) 
    return 1;
  else if (e->op != AR_EXPR_CST && e->op != AR_EXPR_PARAM)
    return s_is_var (e->args[0]);
  return 0;
}

			/* --------------- */

static ar_expr *
s_remove_composite_slots (ar_expr *e, ar_context *ctx, ccl_hash *cache);

static ccl_list *
s_expand_composite_types(ar_expr *e, ar_context *ctx, ccl_list *result,
			 ccl_hash *cache)
{
  if (result == NULL)
    result = ccl_list_create();

  switch (e->op) 
    {
    case AR_EXPR_ITE:
      {      
	ar_expr *cond = s_remove_composite_slots (e->args[0], ctx, cache);
	ccl_list *arg1 = s_expand_composite_types (e->args[1], ctx, NULL, 
						   cache);
	ccl_list *arg2 = s_expand_composite_types (e->args[2], ctx, NULL, 
						   cache);
	ar_expr **ites = ccl_new_array (ar_expr *, ccl_list_get_size (arg1));
	int nb_ites = 0;
      
	ccl_assert (ccl_list_get_size (arg1) == ccl_list_get_size (arg1));
	ccl_assert (ccl_list_get_size (arg1) > 0);
	
	while (!ccl_list_is_empty (arg1))
	  {
	    ar_expr *a1 = (ar_expr *) ccl_list_take_first (arg1);
	    ar_expr *a2 = (ar_expr *) ccl_list_take_first (arg2);

	    ites[nb_ites++] = ar_expr_crt_ternary (AR_EXPR_ITE, cond, a1, a2);
	    ar_expr_del_reference (a1);
	    ar_expr_del_reference (a2);
	  }
	ar_expr_del_reference (cond);
	ccl_list_delete(arg1);
	ccl_list_delete (arg2);

	if (ar_expr_has_base_type (e, AR_BOOLEANS))
	  ccl_list_add (result, ar_expr_crt_and (nb_ites, ites));
	else
	  {
	    int i;
	    for (i = 0; i < nb_ites; i++)
	      ccl_list_add (result, ar_expr_add_reference (ites[i]));
	  }

	while (nb_ites--)
	  ar_expr_del_reference (ites[nb_ites]);
	ccl_delete (ites);
      }
      break;
      
    case AR_EXPR_NOT: case AR_EXPR_NEG:
      {
	ar_expr *tmp = s_remove_composite_slots (e->args[0], ctx, cache);
	ccl_list_add (result, ar_expr_crt_unary (e->op, tmp));
	ar_expr_del_reference (tmp);      
      }
      break;

    case AR_EXPR_MIN : case AR_EXPR_MAX : 
    case AR_EXPR_OR: case AR_EXPR_AND: 
      {
	int i;
	ar_expr **args = ccl_new_array (ar_expr *, e->arity);
	
	for (i = 0; i < e->arity; i++)
	  args[i] = s_remove_composite_slots (e->args[i], ctx, cache);

	ccl_list_add (result, s_crt_nary (e->op, e->arity, args));
	while (i--)
	  ar_expr_del_reference (args[i]);
	ccl_delete (args);
      }
      break;

    case AR_EXPR_EQ: case AR_EXPR_NEQ: 
      {
	ccl_list *arg1 = s_expand_composite_types (e->args[0], ctx, NULL, 
						   cache);
	ccl_list *arg2 = s_expand_composite_types (e->args[1], ctx, NULL, 
						   cache);
	ar_expr **args = ccl_new_array (ar_expr *, ccl_list_get_size (arg1));
	int nb_args = 0;

	ccl_assert(ccl_list_get_size (arg1) == ccl_list_get_size (arg2));
	ccl_assert(ccl_list_get_size (arg1) > 0);

	while (!ccl_list_is_empty (arg1))
	  {
	    ar_expr *a1 = (ar_expr *) ccl_list_take_first (arg1);
	    ar_expr *a2 = (ar_expr *) ccl_list_take_first (arg2);
	    
	    args[nb_args++] = ar_expr_crt_binary (e->op, a1, a2);
	    ar_expr_del_reference (a1);
	    ar_expr_del_reference (a2);
	  }

	ccl_list_delete (arg1);
	ccl_list_delete (arg2);
	if (e->op == AR_EXPR_EQ)
	  ccl_list_add (result, ar_expr_crt_and (nb_args,args));
	else
	  ccl_list_add (result, ar_expr_crt_or (nb_args,args));
	while (nb_args--)
	  ar_expr_del_reference (args[nb_args]);
	ccl_delete (args);
      }
      break;

    case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ: case AR_EXPR_GEQ:
    case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL: case AR_EXPR_DIV: 
    case AR_EXPR_MOD: 
      ccl_assert (e->arity == 2);
      {
	ar_expr *a1 = s_remove_composite_slots (e->args[0], ctx, cache);
	ar_expr *a2 = s_remove_composite_slots (e->args[1], ctx, cache);
	ccl_list_add (result, ar_expr_crt_binary (e->op, a1, a2));
	ar_expr_del_reference (a1);
	ar_expr_del_reference (a2);
      }
      break;

    case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
#if 0
      {
	ccl_list *slots = ccl_list_create ();
	ar_context_slot *sl = ar_expr_eval_slot (e, ctx);
	if (ar_type_is_scalar (e->type))
	  ccl_list_add (slots, sl);
	else
	  ar_context_expand_composite_slot (ctx, sl, slots);
	
	if (s_is_var (e))
	  {
	    while (!ccl_list_is_empty (slots))
	      {
		sl = ccl_list_take_first (slots);
		ccl_list_add (result, ar_expr_crt_variable (sl));
		ar_context_slot_del_reference (sl);
	      }
	  }
	else
	  {
	    while (!ccl_list_is_empty (slots))
	      {
		sl = ccl_list_take_first (slots);
		ccl_list_add (result, ar_expr_crt_parameter (sl));
		ar_context_slot_del_reference (sl);
	      }
	  }
	ccl_delete (slots);
      }

#else
	if (ar_type_is_scalar (e->type))
	  {
	    if (ar_expr_is_constant (e, ctx) && ar_expr_is_computable (e))
	      {
		ar_constant *c = ar_expr_eval (e, ctx);
		ccl_list_add (result, ar_expr_crt_constant (c));
		ar_constant_del_reference (c);
	      }
	    else
	      {
		ccl_list *slots = ccl_list_create ();
		ar_context_slot *sl = ar_expr_eval_slot (e, ctx);
		ccl_assert (sl != NULL);

		ccl_list_add (slots, sl);
	    
		if (s_is_var (e))
		  {
		    while (!ccl_list_is_empty (slots))
		      {
			sl = ccl_list_take_first (slots);
			ccl_list_add (result, ar_expr_crt_variable (sl));
			ar_context_slot_del_reference (sl);
		      }
		  }
		else
		  {
		    while (!ccl_list_is_empty (slots))
		      {
			sl = ccl_list_take_first (slots);
			ccl_list_add (result, ar_expr_crt_parameter (sl));
			ar_context_slot_del_reference (sl);
		      }
		  }
		ccl_list_delete (slots);
	      }
	  }
	else
	  {
	    if (ar_type_get_kind (e->type) == AR_TYPE_STRUCTURE)
	      {
		ar_identifier_iterator *i = ar_type_struct_get_fields(e->type);
		while (ccl_iterator_has_more_elements (i))
		  {
		    ar_identifier *fname = ccl_iterator_next_element (i);
		    ar_expr *tmp = ar_expr_crt_struct_member(e, fname);
		    result = s_expand_composite_types(tmp, ctx, result, cache);
		    ar_expr_del_reference (tmp);
		    ar_identifier_del_reference (fname);
		  }
		ccl_iterator_delete (i);
	      }
	    else
	      {
		int i, sz;
		ccl_assert (ar_type_get_kind (e->type) == AR_TYPE_ARRAY);
		sz = ar_type_get_width (e->type);

		for (i = 0; i < sz; i++)
		  {
		    ar_expr *index = ar_expr_crt_integer (i);
		    ar_expr *tmp = ar_expr_crt_binary (AR_EXPR_ARRAY_MEMBER, e,
						       index);
		    result = s_expand_composite_types (tmp, ctx, result, cache);
		    ar_expr_del_reference (tmp);
		    ar_expr_del_reference (index);
		  }
	      }
	  }
#endif	    
      break;
    
    case AR_EXPR_CALL:
#if 0
      {
	int i;
	ar_expr **args = ccl_new_array (ar_expr *,e->arity);
	ar_expr *res;

	for (i = 0; i < e->arity; i++)
	  args[i] = s_remove_composite_slots (e->args[i], ctx);
	res = ar_expr_crt_function_call (e->u.sig,args);
	for(i = 0; i < e->arity; i++)
	  ar_expr_del_reference (args[i]);
	ccl_delete (args);
	ccl_list_add (result, res);
      }
#else
      ccl_list_add (result, ar_expr_add_reference (e));
#endif
      break;
      
    case AR_EXPR_EXIST: case AR_EXPR_FORALL: 
#if 0
      {
	ccl_pair *p;
	ar_expr *qf = s_remove_composite_slots (e->args[0], e->qctx);
	ar_context *pqctx = ar_context_get_parent (e->qctx);
	ar_context *qctx = ar_context_create (pqctx);
	ccl_list *qvars = ccl_list_deep_dupcreate ();
      
	ar_context_del_reference (pqctx);
	for (p = FIRST (e->u.qvars); p; p = CDR (p))
	  {
	    ar_expr *var = (ar_expr *) CAR (p);
	    ar_context_expand_composite_slot (e->qctx, var->u.slot, qvars);
	  }

	for (p = FIRST (qvars); p; p = CDR (p))
	  {
	    ar_context_slot *sl = (ar_context_slot *) CAR (p);
	    ar_identifier *id = ar_context_slot_get_name (sl);
	  
	    ar_context_add_slot (qctx, id, sl);
	    CAR (p) = ar_expr_crt_variable (sl);
	    ar_context_slot_del_reference (sl);
	    ar_identifier_del_reference (id);
	  }
	ccl_list_add (result, ar_expr_crt_quantifier (e->op, qctx, qvars, qf));
	ar_expr_del_reference (qf);
	ccl_list_clear_and_delete (qvars,
				   (ccl_delete_proc *) ar_expr_del_reference);
	ar_context_del_reference (qctx);
      }
#else
      {
	ar_expr *qf = s_remove_composite_slots (e->args[0], e->qctx, cache);
	ccl_list *qvars = 
	  ccl_list_deep_dup (e->u.qvars, (ccl_duplicate_func *)
			     ar_expr_add_reference);
	ccl_list_add (result, 
		      ar_expr_crt_quantifier (e->op, e->qctx, qvars, qf));

	ar_expr_del_reference (qf);
	ccl_list_clear_and_delete (qvars,
				   (ccl_delete_proc *) ar_expr_del_reference);
      }
#endif
      break;

    case AR_EXPR_CST: 
      if (ar_type_is_scalar (e->type))
	ccl_list_add (result, ar_expr_add_reference (e));
      else if (ar_constant_get_kind (e->u.cst) == AR_CST_ARRAY)
	{
	  int i;
	  int w = ar_constant_get_width (e->u.cst);
	  
	  for (i = 0; i < w; i++)
	    {
	      ar_constant *c = ar_constant_get_at (e->u.cst,i);
	      ar_expr *ec = ar_expr_crt_constant (c);
	      ccl_list_add (result, ec);
	      
	      ar_constant_del_reference (c);
	    }
	}    
      else
	{
	  ar_identifier_iterator *ii = ar_type_struct_get_fields (e->type);
	  
	  ccl_assert (ar_constant_get_kind (e->u.cst) == AR_CST_STRUCTURE);
	  while (ccl_iterator_has_more_elements (ii))
	    {
	      ar_identifier *fname = ccl_iterator_next_element (ii);
	      ar_constant *c = ar_constant_get_field (e->u.cst,fname);
	      ar_expr *ec = ar_expr_crt_constant (c);
	      ccl_list_add (result, ec);
	      ar_constant_del_reference (c);
	      ar_identifier_del_reference (fname);
	    }
	  ccl_iterator_delete (ii);
	}
      break;

    case AR_EXPR_VAR: case AR_EXPR_PARAM:
      if (ar_type_is_scalar (e->type))
	{
	  ar_expr *tmp;
	  ar_identifier *sname = ar_context_slot_get_name (e->u.slot);
	  ar_context_slot *sl = ar_context_get_slot (ctx, sname);

	  if (e->op == AR_EXPR_VAR)
	    tmp = ar_expr_crt_variable (sl);
	  else
	    tmp = ar_expr_crt_parameter (sl);
	  ccl_list_add (result, tmp);
	  ar_context_slot_del_reference (sl);
	  ar_identifier_del_reference (sname);
	}
      else 
	{
	  ccl_list *slots = 
	    ar_context_expand_composite_slot (ctx, e->u.slot, NULL);

	  while (!ccl_list_is_empty (slots))
	    {
	      ar_context_slot *sl = (ar_context_slot *)
		ccl_list_take_first (slots);
	      if (e->op == AR_EXPR_VAR)
		ccl_list_add (result, ar_expr_crt_variable (sl));
	      else
		ccl_list_add (result, ar_expr_crt_parameter (sl));
	      ar_context_slot_del_reference (sl);
	    }
	  ccl_list_delete (slots);
	}
      break;

    case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:
      {
	int i;

	for (i = 0; i < e->arity; i++)
	  {
	    ccl_list *l = 
	      s_expand_composite_types (e->args[i], ctx, NULL, cache);
	    ccl_list_append (result, l);
	    ccl_list_delete (l);
	  }
      }
      break;
    };

  return result;
}

			/* --------------- */

ccl_list *
ar_expr_expand_composite_types(ar_expr *e, ar_context *ctx, ccl_list *result)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				     ar_expr_del_reference);
  result = s_expand_composite_types (e, ctx, result, cache);
  ccl_hash_delete (cache);

  return result;
}  

			/* --------------- */

static ar_expr *
s_remove_composite_slots (ar_expr *e, ar_context *ctx, ccl_hash *cache)
{
  ar_expr *result;

  if (ccl_hash_find (cache, e))
    {
      result = ccl_hash_get (cache);
      result = ar_expr_add_reference (result);
    }
  else
    {
      ccl_list *sub = s_expand_composite_types (e, ctx, NULL, cache);

      result = CAR (FIRST (sub));
      ccl_hash_find (cache, e);
      ccl_hash_insert (cache, ar_expr_add_reference (result));
      ccl_assert (ccl_list_get_size (sub) == 1);
      ccl_list_delete (sub);
    }
  if (e->op == AR_EXPR_ARRAY || e->op == AR_EXPR_STRUCT)
    ccl_throw_no_msg (internal_error);

  return result;
}

ar_expr *
ar_expr_remove_composite_slots (ar_expr *e, ar_context *ctx)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				     ar_expr_del_reference);
  ar_expr *result = s_remove_composite_slots (e, ctx, cache);
  ccl_hash_delete (cache);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_remove_composite_slots_with_cache (ar_expr *e, ar_context *ctx,
					   ccl_hash *cache)
{
  return s_remove_composite_slots (e, ctx, cache);
}

ccl_list *
ar_expr_get_signatures (const ar_expr *e, ccl_list *result)
{
  if( result == NULL )
    result = ccl_list_create();
  
  switch( e->op ) {
  case AR_EXPR_ITE: case AR_EXPR_NOT: case AR_EXPR_NEG: case AR_EXPR_OR: 
  case AR_EXPR_AND: case AR_EXPR_EQ:  case AR_EXPR_NEQ: case AR_EXPR_LT: 
  case AR_EXPR_GT:  case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_ADD: 
  case AR_EXPR_SUB: case AR_EXPR_MUL: case AR_EXPR_DIV: case AR_EXPR_MOD: 
  case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
  case AR_EXPR_ARRAY: case AR_EXPR_STRUCT: case AR_EXPR_EXIST: 
  case AR_EXPR_FORALL: case AR_EXPR_CALL:
  case AR_EXPR_MIN: case AR_EXPR_MAX:

    {
      int i;
      for(i = 0; i < e->arity; i++)
	(void)ar_expr_get_signatures(e->args[i],result);
      if( e->op == AR_EXPR_CALL && ! ccl_list_has(result,e->u.sig) )
	ccl_list_add(result,ar_signature_add_reference(e->u.sig));
    }
    break;
  
  case AR_EXPR_CST: case AR_EXPR_VAR:     
    break;
  case AR_EXPR_PARAM:
    {
      ar_expr *value = ar_context_slot_get_value(e->u.slot);
      if( value != NULL )
	{
	  (void)ar_expr_get_signatures(value,result);
	  ar_expr_del_reference(value);
	}
    }
    break;
  };

  return result;
}

			/* --------------- */

int
ar_expr_equals(const ar_expr *e1, const ar_expr *e2)
{
  int result = 1;

  if( e1 == e2 ) return 1; 
  if( e1->op != e2->op ) return 0;
  if( e1->arity != e2->arity ) return 0;
  if( e1->type == NULL || ! ar_type_equals(e1->type,e2->type) ) return 0;

  switch( e1->op ) {
  case AR_EXPR_CALL:
    result = ar_signature_equals(e1->u.sig,e2->u.sig);
  case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ:
  case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ:
  case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL:
  case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
  case AR_EXPR_ARRAY_MEMBER: case AR_EXPR_ARRAY:   case AR_EXPR_STRUCT:
  case AR_EXPR_MIN: case AR_EXPR_MAX:
    {
      int i;

      for(i = 0; result && i < e1->arity; i++)
	{
	  result = ar_expr_equals(e1->args[i],e2->args[i]);
	  if( e1->op == AR_EXPR_STRUCT && result )
	    result = e1->u.fnames[i] == e2->u.fnames[i];
	}
    }     
    break;

  case AR_EXPR_EXIST: case AR_EXPR_FORALL:
    result = ccl_list_equals(e1->u.qvars,e2->u.qvars) &&
      ar_expr_equals(e1->args[0],e2->args[0]);
    break;

  case AR_EXPR_STRUCT_MEMBER:
    result = ar_expr_equals(e1->args[0],e2->args[0]) && 
      e1->u.field == e2->u.field;
    break;

  case AR_EXPR_CST: 
    result = ar_constant_are_equal(e1->u.cst,e2->u.cst);
    break;

  case AR_EXPR_VAR: case AR_EXPR_PARAM:
    result = (e1->u.slot == e2->u.slot);
    break;


    ccl_unreachable ();
  };  

  return result;
}

			/* --------------- */

void
ar_expr_add_type_constraints_in_quantifiers (ar_expr *e)
{
  ccl_pair *p;
  int i;

  switch (e->op)
    {
    case AR_EXPR_PARAM: 
    case AR_EXPR_VAR: 
    case AR_EXPR_STRUCT_MEMBER: 
    case AR_EXPR_ARRAY_MEMBER:
    case AR_EXPR_CST: 
      return ;

    case AR_EXPR_EXIST: case AR_EXPR_FORALL:
      for (p = FIRST (e->u.qvars); p; p = CDR (p))
	{	 
	  ar_expr *tmp = CAR (p);	    
	  ar_expr *in_dom = ar_expr_crt_in_domain (tmp, tmp->type);
	  if (e->op == AR_EXPR_FORALL)
	    {
	      ar_expr *tmp2 = ar_expr_crt_unary (AR_EXPR_NOT, in_dom);
	      tmp = ar_expr_crt_binary (AR_EXPR_OR, e->args[0], tmp2);
	      ar_expr_del_reference (tmp2);
	    }
	  else
	    {
	      tmp = ar_expr_crt_binary (AR_EXPR_AND, e->args[0], in_dom);
	    }
	  ar_expr_del_reference (e->args[0]);
	  e->args[0] = tmp;
	  ar_expr_del_reference (in_dom);	    
	}

    case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ: 
    case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ:
    case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL:
    case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
    case AR_EXPR_ARRAY: case AR_EXPR_STRUCT: case AR_EXPR_CALL:
    case AR_EXPR_MIN: case AR_EXPR_MAX:
      for (i = 0; i < e->arity; i++)
	ar_expr_add_type_constraints_in_quantifiers (e->args[i]);
      break;

    default:
      ccl_unreachable ();
      break;
    }
}

			/* --------------- */

ccl_list *
ar_expr_get_array_indexes (ar_expr *e, ccl_list *result)
{
  if (result == NULL)
    result = ccl_list_create ();

  if (e->op == AR_EXPR_ARRAY_MEMBER)
    {
      ar_expr_get_array_indexes (e->args[0], result);
      if (! ar_expr_is_computable (e))
	{
	  ar_type *t = ar_expr_get_type (e->args[0]);
	  int nb_elements = ar_type_get_width (t);
	  ccl_list_add (result, (void *) (intptr_t) nb_elements);
	  ccl_list_add (result, ar_expr_add_reference (e->args[1]));
	  ar_type_del_reference (t);
	}
    }
  else if (e->op != AR_EXPR_CST && e->op != AR_EXPR_VAR && 
	   e->op != AR_EXPR_PARAM)
    {
      int i;
      for (i = 0; i < e->arity; i++)
	result = ar_expr_get_array_indexes (e->args[i], result);
    }

  return result;
}

			/* --------------- */


ar_expr *
ar_expr_subst (ar_expr *e, ar_context *ctx, ar_expr *ei, ar_expr *cst_i)
{
  ar_expr *result;
  ar_expr **ops;

  ccl_pre (e != NULL); 
  ccl_pre (ei != NULL);
  ccl_pre (cst_i != NULL);

  if (e == ei)
    return ar_expr_add_reference (cst_i);
  
  result = NULL;
  ops = NULL;
  if (e->arity)
    {
      int i;
      ops = ccl_new_array (ar_expr *, e->arity);
      for(i = 0; i < e->arity; i++)
	ops[i] = ar_expr_subst (e->args[i], ctx, ei, cst_i);
    }

  switch (e->op)
    {
    case AR_EXPR_CST: case AR_EXPR_PARAM:
    case AR_EXPR_VAR:
      result = ar_expr_add_reference (e);
      break;

    case AR_EXPR_STRUCT_MEMBER :            
      result = ar_expr_crt_struct_member (ops[0], e->u.field);
      break;

    case AR_EXPR_ITE :
    case AR_EXPR_NOT : case AR_EXPR_NEG :
    case AR_EXPR_ARRAY_MEMBER : case AR_EXPR_AND : case AR_EXPR_OR : 
    case AR_EXPR_ADD : case AR_EXPR_SUB : case AR_EXPR_MUL : case AR_EXPR_DIV : 
    case AR_EXPR_MOD : case AR_EXPR_LT : case AR_EXPR_GT : case AR_EXPR_LEQ : 
    case AR_EXPR_GEQ : case AR_EXPR_EQ : case AR_EXPR_NEQ :
    case AR_EXPR_MIN: case AR_EXPR_MAX:

      result = s_crt_nary (e->op, e->arity, ops);
      if (e->op == AR_EXPR_ARRAY_MEMBER && ar_expr_is_computable (result))
	{
	  ar_context_slot *sl = ar_expr_eval_slot (result, ctx);
	  ar_expr *tmp = ar_expr_crt_variable (sl);      
	  ar_context_slot_del_reference(sl);
	  ar_expr_del_reference (result);
	  result = tmp;
	}
      break;

    case AR_EXPR_EXIST : case AR_EXPR_FORALL :
      result = ar_expr_crt_quantifier (e->op, e->qctx, e->u.qvars, ops[0]);
      break;
      
    case AR_EXPR_CALL:
      result = ar_expr_crt_function_call (e->u.sig, ops);
      break;

    case AR_EXPR_ARRAY : 
      result = ar_expr_crt_array (ops, e->arity);
      break;

    case AR_EXPR_STRUCT :
      result = ar_expr_crt_structure (e->u.fnames, ops, e->arity);
      break;

    default:
      ccl_throw_no_msg (internal_error);
    }

  if (ops != NULL)
    {
      int i;

      for (i = 0; i < e->arity; i++)
	ar_expr_del_reference (ops[i]);
      ccl_delete (ops);
    }

  ccl_post (result != NULL);

  return result;
}

			/* --------------- */

ar_expr *
ar_expr_crt_case (ar_expr **condval, int nb_args, ar_expr *dft)
{
  int i;
  ar_expr *result = ar_expr_add_reference (dft);
      
  for(i = nb_args-1; i >= 0; i--)
    {
      ar_expr *ite = 
	ar_expr_crt_ternary (AR_EXPR_ITE, condval[2 * i], condval[2 * i + 1], 
			     result);
      ar_expr_del_reference (result);
      result = ite;
    }

  return result;
}

			/* --------------- */

static ar_expr *
s_new_expr(ar_expr_op op, int arity)
{
  ar_expr *result = ccl_new(struct ar_expr_st);

  result->refcount = 1;
  result->op = op;
  result->type = NULL;
  result->args = NULL;
  result->qctx = NULL;
  result->subtypes = NULL;
  result->subargs = NULL;
  if( (result->arity = arity) )
    result->args = ccl_new_array(ar_expr *,arity);

  return result;
}

			/* --------------- */

static void
s_delete_expr(ar_expr *e)
{
  int i;
  ccl_pre( e != NULL );

  if( e->args != NULL )
    {
      for(i = 0; i< e->arity; i++)
	ar_expr_del_reference(e->args[i]);
      ccl_delete(e->args);
    }

  ccl_zdelete(ar_type_del_reference,e->type);

  if( e->op == AR_EXPR_CST )
    ar_constant_del_reference(e->u.cst);
  else if( e->op == AR_EXPR_CALL )
    ar_signature_del_reference(e->u.sig);
  else if( e->op == AR_EXPR_STRUCT_MEMBER )
    ar_identifier_del_reference(e->u.field);
  else if( e->op == AR_EXPR_STRUCT )
    {
      for(i = 0; i < e->arity; i++)
	ar_identifier_del_reference(e->u.fnames[i]);
      ccl_delete(e->u.fnames);
    }
  else if( e->op == AR_EXPR_FORALL || e->op == AR_EXPR_EXIST )
    ccl_list_clear_and_delete(e->u.qvars,
			      (ccl_delete_proc *)ar_expr_del_reference);
  else if( e->op == AR_EXPR_VAR || e->op == AR_EXPR_PARAM )
    ar_context_slot_del_reference(e->u.slot);

  if (e->subtypes != NULL)
    ccl_list_clear_and_delete (e->subtypes, 
			       (ccl_delete_proc *) ar_type_del_reference);

  if (e->subargs != NULL)
    ccl_list_clear_and_delete (e->subargs, 
			       (ccl_delete_proc *) ar_expr_del_reference);
  ccl_zdelete(ar_context_del_reference,e->qctx);

  ccl_delete(e);
}

			/* --------------- */

