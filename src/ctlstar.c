/*
 * ctlstar.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-tree.h>
#include <ccl/ccl-graph.h>
#include <ccl/ccl-memory.h>
#include "ctlstar.h"

#define ENABLE_CHECKS 0
#define USE_MOVE_FOR_EX 1
#define INTERNAL_DUMMY_VARIABLES_PREFIX "X_"
#define INTERNAL_DUMMY_VARIABLES_PREFIX2 "Y_"
#define EXTERNAL_DUMMY_VARIABLES_PREFIX "Z_"
#define UNKNOWN_HVAL (~((unsigned int)0))

#define s_sort_operands_array(_arity, _operands) \
  qsort ((_operands), (_arity), sizeof ((_operands)[0]), s_compare_formulas)

#define s_sort_operands(_F) s_sort_operands_array((_F)->arity, (_F)->operands)

#define IS_EVENTUALITY(F)    \
  ((F)->kind == CTLSTAR_U ||   \
   ((F)->kind == CTLSTAR_X &&			\
    ((F)->operands[0]->kind == CTLSTAR_U 	\
     /* || (F)->operands[0]->kind == CTLSTAR_NOT_U */)))

#define IS_TRUE(F) ((F)->kind == CTLSTAR_TRUE)
#define IS_FALSE(F) ((F)->kind == CTLSTAR_NOT && IS_TRUE ((F)->operands[0]))
#define IS_F(F) (((F)->kind == CTLSTAR_U) && IS_TRUE ((F)->operands[0]))
#define IS_G(F) (((F)->kind == CTLSTAR_NOT_U) && IS_TRUE ((F)->operands[0]))
#define IS_GF(F)				\
  ((F)->kind == CTLSTAR_NOT_U && 		\
    IS_TRUE ((F)->operands[0]) &&		\
    (F)->operands[1]->kind == CTLSTAR_NOT_U &&	\
   IS_TRUE ((F)->operands[1]->operands[0]))
#define GF_ARG(F) ((F)->operands[1]->operands[1])


typedef struct ctlstar_manager_st ctlstar_manager;
struct ctlstar_manager_st 
{
  int refcount;
  int time;
  ccl_hash *dag;
};

			/* --------------- */

struct ctlstar_formula
{
  int refcount;
  unsigned int hval;
  int height;
  int alternation;
  ctlstar_formula_kind kind;
  ar_identifier *varname;
  int arity;
  int flags;
  int ctx;
  int scc;
  int uid;
  ctlstar_formula *cached;
  ctlstar_formula **operands;
};

			/* --------------- */

static void
s_check_formula (ctlstar_formula *F);

static void
s_check_manager (ctlstar_manager *man);

static ctlstar_formula *
s_crt_not_U (ctlstar_formula *F1, ctlstar_formula *F2);

static ctlstar_formula *
s_crt_GF (ctlstar_formula *X);

static ctlstar_formula *
s_allocate_formula (ctlstar_formula_kind kind, int arity);

static void
s_delete_formula (ctlstar_formula *F);

static void
s_compute_hashvalue (ctlstar_formula *F);

static ctlstar_formula *
s_find_or_add_formula (ctlstar_formula *F);

static ctlstar_manager *
s_manager_crt (void);

static void
s_manager_del_reference (ctlstar_manager *cm);

static unsigned int
s_ctlstar_hash (const void *key);

static int 
s_ctlstar_compare_addr (const void *k1, const void *k2);

static int 
s_ctlstar_compare_uid (const void *k1, const void *k2);

static ctlstar_formula *
s_ctlstar_to_fixpoint (ctlstar_formula *F, int *pcounter);

static void
s_make_operands_unique (int *p_arity, ctlstar_formula **operands);

static int
s_compare_formulas (const void *p1, const void *p2);

static ctlstar_formula *
s_crt_fixpoint_EX (ctlstar_formula *arg);

static ctlstar_formula *
s_crt_fixpoint (ctlstar_formula_kind kind, ar_identifier *qvarname, 
		ctlstar_formula *arg);

static int
s_is_subformula_of (ctlstar_formula *subF, ctlstar_formula *F);

static int
s_is_free_variable (ctlstar_formula *F, ar_identifier *V);

static ctlstar_formula *
s_duplicate_formula (const ctlstar_formula *F);

static int
s_are_opposite (const ctlstar_formula *F1, const ctlstar_formula *F2);

			/* --------------- */

static int external_dummy_variables_number = 0;
static ctlstar_manager *CTLSTAR_MANAGER = NULL;

			/* --------------- */

int 
ctlstar_init (void)
{
  if (CTLSTAR_MANAGER == NULL)
    CTLSTAR_MANAGER = s_manager_crt ();

  return 1;
}

			/* --------------- */

int 
ctlstar_terminate (void)
{
  ccl_zdelete (s_manager_del_reference, CTLSTAR_MANAGER);

  return 1;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_true (void)
{
  ctlstar_formula *F = s_allocate_formula (CTLSTAR_TRUE, 0);

  return s_find_or_add_formula (F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_variable (ar_identifier *X)
{
  ctlstar_formula *result;

  ccl_pre (X != NULL);

  result = s_allocate_formula (CTLSTAR_VAR, 0);
  result->varname = ar_identifier_add_reference (X);

  return s_find_or_add_formula (result);
}

			/* --------------- */

static ctlstar_formula *
s_crt_dummy_variable (ar_identifier **pvarname, const char *prefix, 
		      int *pcounter)
{
  ar_identifier *vid = ar_identifier_create_numbered_auto (prefix, pcounter);
  ctlstar_formula *result = ctlstar_formula_crt_variable (vid);

  if (pvarname != NULL)
    *pvarname = vid;
  else
    ar_identifier_del_reference (vid);

  return result;
}

			/* --------------- */
ctlstar_formula *
ctlstar_formula_crt_dummy_variable (ar_identifier **pvarname)
{
  return s_crt_dummy_variable (pvarname, EXTERNAL_DUMMY_VARIABLES_PREFIX, 
			       &external_dummy_variables_number);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_not (ctlstar_formula *F)
{
  ctlstar_formula *result;

  ccl_pre (F != NULL);

  if (F->kind == CTLSTAR_NOT) 
    {
      /* RWR: ~~F -> F */
      result = ctlstar_formula_add_reference (F->operands[0]);
    }
  else if (F->kind == CTLSTAR_NOT_U) 
    {
      /* RWR: ~(p ~U q) -> p U q */
      result = ctlstar_formula_crt_U (F->operands[0], F->operands[1]);
    }
  else if (F->kind == CTLSTAR_U)
    {
      /* RWR: ~(p U q) -> p ~U q */
      result = s_crt_not_U (F->operands[0], F->operands[1]);
    }
  else if (F->kind == CTLSTAR_X)
    {
      /* RWR: ~ X p  --> X ~ p */
      ctlstar_formula *tmp = ctlstar_formula_crt_not (F->operands[0]);
      result = ctlstar_formula_crt_X (tmp);
      ctlstar_formula_del_reference (tmp);
    }
  else if (F->kind == CTLSTAR_OR || F->kind == CTLSTAR_AND)
    {
      /* RWR: ~ (X | Y)  --> ~X & ~Y */
      /* RWR: ~ (X & Y)  --> ~X | ~Y */
      int i;     
      ctlstar_formula_kind dual =  
	F->kind == CTLSTAR_OR ? CTLSTAR_AND : CTLSTAR_OR;

      result = s_allocate_formula (dual, F->arity);
      for (i = 0; i < F->arity; i++)
	result->operands[i] = ctlstar_formula_crt_not (F->operands[i]);
      s_sort_operands (result);
      result = s_find_or_add_formula (result);
    }
  else 
    {
      result = s_allocate_formula (CTLSTAR_NOT, 1);
      result->operands[0] = ctlstar_formula_add_reference (F);
      result = s_find_or_add_formula (result);
    }

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_rename_variable (ctlstar_formula *F, ar_identifier *old, ar_identifier *new)
{
  ctlstar_formula *result = NULL;

  if (F->kind == CTLSTAR_VAR)
    {
      if (F->varname == old)
	result = ctlstar_formula_crt_variable (new);
      ccl_assert (F->varname != new);
    }
  else if (F->kind == FXP_MU || F->kind == FXP_NU)
    {
      if (F->varname == old)
	result = ctlstar_formula_add_reference (F);
    }

  if (result == NULL && F->arity > 0)
    {
      int i;

      ccl_assert (F->kind != CTLSTAR_NOT_U && F->kind != CTLSTAR_U &&
		  F->kind != CTLSTAR_X);

      result = s_duplicate_formula (F);
      
      for (i = 0; i < result->arity; i++)
	{
	  ctlstar_formula *tmp = 
	    s_rename_variable (result->operands[i], old, new);
	  ctlstar_formula_del_reference (result->operands[i]);
	  result->operands[i] = tmp;
	}
      s_sort_operands (result);
      result = s_find_or_add_formula (result);
    }

  if (result == NULL)
    result = ctlstar_formula_add_reference (F);
  if (ccl_debug_is_on)
    {
      ctlstar_formula_log (CCL_LOG_DEBUG, F, NULL, 0);
      ccl_debug ("[");
      ar_identifier_log (CCL_LOG_DEBUG, old);
      ccl_debug (" <- ");
      ar_identifier_log (CCL_LOG_DEBUG, new);
      ccl_debug ("] = ");
      ctlstar_formula_log (CCL_LOG_DEBUG, result, NULL, 0);
      ccl_debug ("\n");
    }
  return result;
}

			/* --------------- */


/* RWR: mu.x F(x) & mu.y G(y) --> mu.z (F(z) & G(z)) */
/* RWR: nu.x F(x) | nu.y G(y) --> nu.z (F(z) | G(z)) */
static void
s_merge_fixpoints (ctlstar_formula *(*crt)(ctlstar_formula *, 
					   ctlstar_formula *),
		   ctlstar_formula_kind fxp,
		   int *p_arity,
		   ctlstar_formula ***p_operands)
{
  int i, j;
  int arity = *p_arity;
  ctlstar_formula *tmp[3];
  ctlstar_formula **ops = *p_operands;
  ar_identifier *qvar;

  for (i = 0; i < arity && ops[i]->kind != fxp; i++)
    CCL_NOP ();
  ccl_assert (i <= arity - 2);
  qvar = ar_identifier_add_reference (ops[i]->varname);
  tmp[0] = ctlstar_formula_add_reference (ops[i]->operands[0]);
  ctlstar_formula_del_reference (ops[i]);
  ops[i] = tmp[0];

  for (j = i + 1; j < arity; j++)
    {
      if (ops[j]->kind == fxp)
	{
	  ccl_assert (!s_is_free_variable (ops[j], qvar));
	  tmp[0] = 
	    s_rename_variable (ops[j]->operands[0], ops[j]->varname, qvar);
	  tmp[1] = crt (ops[i], tmp[0]);
	  ctlstar_formula_del_reference (tmp[0]);
	  ctlstar_formula_del_reference (ops[i]);
	  ctlstar_formula_del_reference (ops[j]);
	  ops[i] = tmp[1];
	}
      else 
	{
	  tmp[0] = ops[j];
	  ops[j] = ops[i];
	  ops[i] = tmp[0];
	  i = j;
	}
    }
  tmp[0] = s_crt_fixpoint (fxp, qvar, ops[i]);
  ar_identifier_del_reference (qvar);
  ctlstar_formula_del_reference (ops[i]);
  ops[i] = tmp[0];
  ccl_assert (i <= arity - 1);
  *p_arity = i + 1;
  *p_operands = ccl_realloc (ops, sizeof (ctlstar_formula *) * (i + 1));
}

			/* --------------- */

static ctlstar_formula *
s_crt_boolean_connective (ctlstar_formula_kind kind, ctlstar_formula *F1, 
			  ctlstar_formula *F2)
{
  int k, i;
  int nb_mu = 0;
  int nb_nu = 0;
  int arity = 0;
  ctlstar_formula *result;
  
  ccl_pre (F1 != NULL);
  ccl_pre (F2 != NULL);

  if (F1 == F2)
    return ctlstar_formula_add_reference (F1);

  if (F1->kind == kind)
    arity = F1->arity;
  else 
    arity = 1;

  if (F2->kind == kind)
    arity += F2->arity;
  else 
    arity += 1;

  result = s_allocate_formula (kind, arity);  
  
  i = 0;
  for (k = 0; k < 2; k++)
    {
      if (F1->kind == kind)
	{
	  int j;

	  for (j = 0; j < F1->arity; j++, i++)
	    {
	      if (F1->operands[j]->kind == FXP_MU)
		nb_mu++;
	      else if (F1->operands[j]->kind == FXP_NU)
		nb_nu++;
	      result->operands[i] = 
		ctlstar_formula_add_reference (F1->operands[j]);
	    }
	}
      else
	{
	  result->operands[i++] = ctlstar_formula_add_reference (F1);
	  if (F1->kind == FXP_MU)
	    nb_mu++;
	  else if (F1->kind == FXP_NU)
	    nb_nu++;
	}
	
      F1 = F2;
    }
  ccl_post (i == arity);

  if (kind == CTLSTAR_AND && nb_mu > 1)
    s_merge_fixpoints (ctlstar_formula_crt_and, FXP_MU, &result->arity, 
		       &result->operands);
  else if (kind == CTLSTAR_OR && nb_nu > 1)
    s_merge_fixpoints (ctlstar_formula_crt_or, FXP_NU, &result->arity, 
		       &result->operands);
  if (result->arity == 1)
    {
      ctlstar_formula *tmp = 
	ctlstar_formula_add_reference (result->operands[0]);
      ctlstar_formula_del_reference (result);
      result = tmp;
    }
  else
    {
      s_sort_operands (result);
      s_make_operands_unique (&result->arity, result->operands);
      result = s_find_or_add_formula (result);
    }

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_and (ctlstar_formula *F1, ctlstar_formula *F2)
{
  ctlstar_formula *result;

  if (IS_TRUE (F1) || (F1 == F2))
    {
      /* RWR: 1 & F --> F */
      /* RWR: F & F --> F */
      result = ctlstar_formula_add_reference (F2);
    }
  else if (IS_TRUE (F2))
    {
      /* RWR: F & 1 --> F */
      result = ctlstar_formula_add_reference (F1);
    }
  else if (IS_FALSE (F1) || IS_FALSE (F2) || s_are_opposite (F1, F2))
    {
      /* RWR: F & 0 --> 0 */
      /* RWR: 0 & F --> 0 */
      /* RWR: ~F & F --> 0 */
      result = ctlstar_formula_crt_false ();
    }  
  else if (/* (F1->kind == CTLSTAR_X || */
	   F1->kind == FXP_EX 
	   && F1->kind == F2->kind)
    {
      /* RWR: X F1  & X F2 --> X (F1 & F2) */
      /* RWR: <> F1  & <> F2 --> <> (F1 & F2) */
      ctlstar_formula *tmp = 
	ctlstar_formula_crt_and (F1->operands[0], F2->operands[0]);
      if (F1->kind == FXP_EX)
	result = s_crt_fixpoint_EX (tmp);
      else
	result = ctlstar_formula_crt_X (tmp);
      ctlstar_formula_del_reference (tmp);
    }
  else
    {
      result = s_crt_boolean_connective (CTLSTAR_AND, F1, F2);
    }

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_or (ctlstar_formula *F1, ctlstar_formula *F2)
{
  ctlstar_formula *result;

  if (IS_FALSE (F1) || (F1 == F2))
    {
      /* RWR: F | F --> F */
      /* RWR: 0 | F --> F */
      result = ctlstar_formula_add_reference (F2);
    }
  else if (IS_FALSE (F2))
    {
      /* RWR: F | 0 --> F */
      result = ctlstar_formula_add_reference (F1);
    }
  else if (IS_TRUE (F1) || IS_TRUE (F2) || s_are_opposite (F1, F2))
    {
      /* RWR: 1 | F --> 1 */
      /* RWR: F | 1 --> 1 */
      /* RWR: ~F | F --> 1 */
      result = ctlstar_formula_crt_true ();
    }
  else if (IS_GF (F1) && IS_GF (F2))
    {
      /* RWR: GF x | GF y --> GF (x | y) */
      ctlstar_formula *tmp = ctlstar_formula_crt_or (GF_ARG(F1), GF_ARG (F2));
      result = s_crt_GF (tmp);
      ctlstar_formula_del_reference (tmp);
    }
  else if (IS_F (F1) && IS_F (F2))
    {
      /* RWR: F x | F y --> F (x | y) */
      ctlstar_formula *tmp = 
	ctlstar_formula_crt_or (F1->operands[1], F2->operands[1]);
      result = ctlstar_formula_crt_F (tmp);
      ctlstar_formula_del_reference (tmp);
    }
  else
    {
      result = s_crt_boolean_connective (CTLSTAR_OR, F1, F2);
    }

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_X (ctlstar_formula *F)
{
  ctlstar_formula *result= NULL;
  ctlstar_formula_kind kind;

  ccl_pre (F != NULL);
  kind = F->kind;

  if (IS_TRUE (F))
    {
      /* RWR: X 1 --> 1 */
      result = ctlstar_formula_add_reference (F);
    }
  else if (0)
    {
      if (IS_GF (F))
	{
	  /* RWR: X GF p --> GF p */
	  result = ctlstar_formula_add_reference (F);
	}
      else if (kind == CTLSTAR_AND || kind == CTLSTAR_OR)
	{
	  /* RWR: X (p & GF q) --> X p & GF q */
	  /* RWR: X (p | GF q) --> X p | GF q */
	  int i;      
	  int arity = F->operands[0]->arity;
	  ctlstar_formula **operands = F->operands[0]->operands;
	  ctlstar_formula *gf = NULL;
	  ctlstar_formula *notgf = NULL;

	  for (i = 0; i < arity; i++)
	    {
	      if (! IS_GF (operands[i]))
		continue;

	      if (gf == NULL)	    
		gf = ctlstar_formula_add_reference (operands[i]);
	      else 
		{
		  ctlstar_formula *tmp;
		  if (kind == CTLSTAR_AND)
		    tmp = ctlstar_formula_crt_and (gf, operands[i]);
		  else
		    tmp = ctlstar_formula_crt_or (gf, operands[i]);
		  ctlstar_formula_del_reference (gf);
		  gf = tmp;
		}
	    }

	  if (gf != NULL)
	    {
	      for (i = 0; i < arity; i++)
		{
		  if (IS_GF (operands[i]))
		    continue;

		  if (notgf == NULL)	    
		    notgf = ctlstar_formula_add_reference (operands[i]);
		  else
		    {
		      ctlstar_formula *tmp = 
			s_crt_boolean_connective (kind, notgf, operands[i]);
		      ctlstar_formula_del_reference (notgf);
		      notgf = tmp;
		    }
		}
	      if (notgf != NULL)
		{
		  ctlstar_formula *tmp = ctlstar_formula_crt_X (notgf);
		  result = s_crt_boolean_connective (kind, tmp, gf);
		  ctlstar_formula_del_reference (tmp);
		  ctlstar_formula_del_reference (notgf);
		  ctlstar_formula_del_reference (gf);
		}
	      else
		{
		  result = gf;
		}
	    }
	}
    }

  if (result == NULL)
    {
      result = s_allocate_formula (CTLSTAR_X, 1);
      result->operands[0] = ctlstar_formula_add_reference (F);
      result = s_find_or_add_formula (result);
    }

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_U (ctlstar_formula *F1, ctlstar_formula *F2)
{
  ctlstar_formula *result;

  ccl_pre (F1 != NULL);
  ccl_pre (F2 != NULL);

  if (IS_FALSE (F2))
    {
      /* RWR: p U 0 -> 0 */      
      result = ctlstar_formula_crt_false ();
    }
  else if (IS_TRUE (F2) || 
	   F1 == F2 ||
	   (IS_TRUE (F1) && IS_GF (F2)) || 
	   (F2->kind == CTLSTAR_U && F2->operands[0] == F1))
    {
      /* RWR: p U 1 -> 1 */      
      /* RWR: p U p -> p */      
      /* RWR: F G F x --> G F x */
      /* RWR: p U (p U q) --> (p U q) */
      result = ctlstar_formula_add_reference (F2);
    }
  else if (F1->kind == CTLSTAR_X && F2->kind == CTLSTAR_X)
    {
      /* RWR: (X p) U (X q) -> X (p U q) */
      ctlstar_formula *tmp = 
	ctlstar_formula_crt_U (F1->operands[0], F2->operands[0]);
      result = ctlstar_formula_crt_X (tmp);
      ctlstar_formula_del_reference (tmp);
    }
  else
    {
      result = s_allocate_formula (CTLSTAR_U, 2);
      result->operands[0] = ctlstar_formula_add_reference (F1);
      result->operands[1] = ctlstar_formula_add_reference (F2);
      result = s_find_or_add_formula (result);
    }

  return result;
}

			/* --------------- */

static int
s_are_opposite (const ctlstar_formula *F1, const ctlstar_formula *F2)
{
  if ((F1->kind == CTLSTAR_NOT && F1->operands[0] == F2) ||
      (F2->kind == CTLSTAR_NOT && F2->operands[0] == F1) ||
      (IS_G(F1) && IS_G(F2) && 
       s_are_opposite (F1->operands[1], F2->operands[1])) ||
      (((F1->kind == CTLSTAR_NOT_U && F2->kind == CTLSTAR_U) ||
	(F1->kind == CTLSTAR_U && F2->kind == CTLSTAR_NOT_U)) &&
       F1->operands[0] == F2->operands[0] && 
       F1->operands[1] == F2->operands[1]))
    return 1;
  return 0;
}

			/* --------------- */

static ctlstar_formula *
s_crt_E (int arity, ctlstar_formula * const *operands, int collect_X)
{
  int i, k;
  ctlstar_formula *X = NULL;
  ctlstar_formula *result = s_allocate_formula (CTLSTAR_E, arity);

  for (k = 0, i = 0; i < arity; i++)
    {
      int j, is_false;

      if (IS_TRUE (operands[i]))
	continue;

      is_false = IS_FALSE (operands[i]);
      for (j = i + 1; j < arity && !is_false; j++)
	is_false = s_are_opposite (operands[i], operands[j]);

      if (is_false)
	{
	  for (i = 0; i < k; i++)
	    ctlstar_formula_del_reference (result->operands[i]);
	  ccl_delete (result->operands);
	  ccl_delete (result);
	  ccl_zdelete (ctlstar_formula_del_reference, X);

	  return ctlstar_formula_crt_false ();
	}
      
      
      if (collect_X && operands[i]->kind == CTLSTAR_X)
	{
	  if (X == NULL)
	    X = ctlstar_formula_add_reference (operands[i]);
	  else
	    {
	      ctlstar_formula *tmp = ctlstar_formula_crt_and (X, operands[i]);
	      ctlstar_formula_del_reference (X);
	      X = tmp;
	    }
	}
      else
	{
	  result->operands[k++] = ctlstar_formula_add_reference (operands[i]);
	}
    }

  if (X != NULL)
    {
      ccl_assert (collect_X);
      result->operands[k++] = X;
    }

  s_sort_operands_array (k, result->operands);
  s_make_operands_unique (&k, result->operands);

  if (k != arity)
    {
      result->arity = k;
      if (k == 0)
	{
	  ctlstar_formula_del_reference (result);
	  result = ctlstar_formula_crt_true ();
	}
      else
	{
	  result->operands = 
	    ccl_realloc (result->operands, sizeof (ctlstar_formula *) * k);
	}
    }

  return s_find_or_add_formula (result);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_E (ctlstar_formula *F)
{
  ctlstar_formula *result;
  int arity;
  ctlstar_formula **operands;

  ccl_pre (F != NULL);

  if (F->kind == CTLSTAR_AND || F->kind == CTLSTAR_OR)
    {
      arity = F->arity;
      operands = F->operands;
    }
  else
    {
      arity = 1;
      operands = &F;
    }

  if (F->kind == CTLSTAR_OR)
    {
      int i;
      
      result = ctlstar_formula_crt_E (operands[0]);
      for (i = 1; i < arity; i++)
	{
	  ctlstar_formula *tmp0 = ctlstar_formula_crt_E (operands[i]);
	  ctlstar_formula *tmp1 = ctlstar_formula_crt_or (result, tmp0);
	  ctlstar_formula_del_reference (result);
	  ctlstar_formula_del_reference (tmp0);
	  result = tmp1;
	}
    }
  else 
    {
      result = s_crt_E (arity, operands, 1);
    }

  return result;
}

			/* --------------- */

void
ctlstar_formula_log (ccl_log_type log, ctlstar_formula *F,
		     void (*logid) (ccl_log_type log, ar_identifier *id, 
				    void *data),
		     void *data)
{
  int i;

  ccl_pre (F != NULL);

  if (IS_TRUE (F)) 
    ccl_log (log, "true");
  else if (IS_FALSE (F))
    ccl_log (log, "false");
  else switch (F->kind)
    {
    case CTLSTAR_VAR:
      if (logid == NULL)
	ar_identifier_log (log, F->varname);
      else 
	logid (log, F->varname, data);
      break;

    case CTLSTAR_NOT:
      ccl_log (log, "not (");
      ctlstar_formula_log (log, F->operands[0], logid, data);
      ccl_log (log, ")");
      break;

    case CTLSTAR_AND:
    case CTLSTAR_OR:

      ccl_log (log, "(");
      ctlstar_formula_log (log, F->operands[0], logid, data);
      ccl_log (log, ")");
      for (i = 1; i < F->arity; i++)
	{
	  if (F->kind == CTLSTAR_AND)
	    ccl_log (log, " and (");
	  else
	    ccl_log (log, " or (");
	  ctlstar_formula_log (log, F->operands[i], logid, data);
	  ccl_log (log, ")");
	}
      break;

    case FXP_EX:
    case CTLSTAR_X:
      {
	const char *op;
	ctlstar_formula *arg = F->operands[0];

	if (F->kind == FXP_EX)
	  {
	    if (F->operands[0]->kind == CTLSTAR_NOT)
	      {
		arg = F->operands[0]->operands[0];
		op = "[]";
	      }
	    else
	      {
		op = "<>";
	      }
	  }
	else
	  {
	    op = "X";
	  }
	ccl_log (log, "%s (", op);
	ctlstar_formula_log (log, arg, logid, data);
	ccl_log (log, ")");
      }
      break;

    case CTLSTAR_U:
      if (IS_TRUE (F->operands[0]))
	{
	  ccl_log (log, "F ");
	  ctlstar_formula_log (log, F->operands[1], logid, data);
	}
      else
	{
	  ccl_log (log, "(");
	  ctlstar_formula_log (log, F->operands[0], logid, data);
	  ccl_log (log, ") U (");
	  ctlstar_formula_log (log, F->operands[1], logid, data);
	  ccl_log (log, ")");
	}
      break;

    case CTLSTAR_NOT_U:
      if (IS_TRUE (F->operands[0]))
	{
	  ccl_log (log, "G ");
	  if (F->operands[1]->kind == CTLSTAR_NOT)
	    ctlstar_formula_log (log, F->operands[1]->operands[0], logid, data);
	  else
	    {
	      ccl_log (log, "not (");
	      ctlstar_formula_log (log, F->operands[1], logid, data);
	      ccl_log (log, ")");
	    }
	}
      else
	{
	  ccl_log (log, "not(");
	  ccl_log (log, "(");
	  ctlstar_formula_log (log, F->operands[0], logid, data);
	  ccl_log (log, ") U (");
	  ctlstar_formula_log (log, F->operands[1], logid, data);
	  ccl_log (log, ")");
	  ccl_log (log, ")");
	}
      break;

    case CTLSTAR_E:
      ccl_log (log, "E [");      
      ctlstar_formula_log (log, F->operands[0], logid, data);
      for (i = 1; i < F->arity; i++)
	{
	  ccl_log (log, ", ");
	  ctlstar_formula_log (log, F->operands[i], logid, data);
	}
      ccl_log (log, "]");
      break;

    case FXP_MU: case FXP_NU:
      if (F->kind == FXP_MU) 
	ccl_log (log, "mu(");
      else
	ccl_log (log, "nu(");
      if (logid == NULL)
	ar_identifier_log (log, F->varname);
      else 
	logid (log, F->varname, data);
      ccl_log (log, ").");
      ctlstar_formula_log (log, F->operands[0], logid, data);
      break;

    default:
      ccl_unreachable ();
    }
}

			/* --------------- */

static void
s_formula_log_as_dot_rec (ccl_log_type log, ctlstar_formula *F,
			  void (*logid) (ccl_log_type log, ar_identifier *id, 
					 void *data),
			  void *data)
{
  int i;

  ccl_pre (F != NULL);

  if (F->flags)
    return;
  F->flags = 1;

  ccl_log (log, "N%p[label=\"", F);

  if (IS_TRUE (F)) 
    ccl_log (log, "true");
  else if (IS_FALSE (F))
    ccl_log (log, "false");
  else if (F->kind == CTLSTAR_VAR)
    {
      if (logid == NULL)
	ar_identifier_log (log, F->varname);
      else
	logid (log, F->varname, data);
    }
  else
    {
      const char *op = NULL;
      switch (F->kind)
	{	  
	case CTLSTAR_NOT: op = "not"; break;
	case CTLSTAR_AND: op = "and"; break;
	case CTLSTAR_OR: op = "or"; break;
	case FXP_EX: op = "<>"; break; 
	case CTLSTAR_X: op = "X"; break;
	case CTLSTAR_U: op = "U"; break;
	case CTLSTAR_NOT_U: op = "not U"; break;
	case CTLSTAR_E: op = "E"; break;	  
	case FXP_MU: op = "mu."; break;
	case FXP_NU: op = "nu."; break;
	default:
	  ccl_unreachable ();
	}

      ccl_log (log, "%s", op);
      if (F->kind == FXP_MU || F->kind == FXP_NU)
	{
	  if (logid == NULL)
	    ar_identifier_log (log, F->varname);
	  else
	    logid (log, F->varname, data);
	}
    }

  ccl_log (log, "\"];\n");
  for (i = 0; i < F->arity; i++)
    ccl_log (log, "N%p -> N%p\n", F, F->operands[i]);
  for (i = 0; i < F->arity; i++)
    s_formula_log_as_dot_rec (log, F->operands[i], logid, data);
}

			/* --------------- */
#if 0
static void
s_clear_flags (ctlstar_formula *F)
{
  int i;

  F->flags = 0;
  for (i = 0; i < F->arity; i++)
    s_clear_flags (F->operands[i]);
}
#endif
			/* --------------- */

void
ctlstar_formula_log_as_dot (ccl_log_type log, ctlstar_formula *F,
			    void (*logid) (ccl_log_type log, ar_identifier *id, 
					   void *data),
			    void *data)

{
  ccl_log (log, "digraph G { \n");
  s_formula_log_as_dot_rec (log, F, logid, data);
  ccl_log (log, "}\n");
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_add_reference (ctlstar_formula *F)
{
  ccl_pre (F != NULL);

  F->refcount++;

  return F;
}

			/* --------------- */

void
ctlstar_formula_del_reference (ctlstar_formula *F)
{
  ccl_pre (F != NULL);
  ccl_pre (F->refcount > 0);

  F->refcount--;
  if (F->refcount == 0)
    {
      if (ccl_hash_find (CTLSTAR_MANAGER->dag, F) && 
	  ccl_hash_get (CTLSTAR_MANAGER->dag) == F)
	ccl_hash_remove (CTLSTAR_MANAGER->dag);
      else 
	s_delete_formula (F);
    }
}

			/* --------------- */

int
ctlstar_formula_is_state_formula (const ctlstar_formula *F)
{
  int result;

  ccl_pre (F != NULL);

  if (F->kind == CTLSTAR_TRUE || F->kind == CTLSTAR_VAR || 
      F->kind == CTLSTAR_E) 
    result = 1;
  else if (F->kind == CTLSTAR_NOT || F->kind == CTLSTAR_AND || 
	   F->kind == CTLSTAR_OR)
    result = ctlstar_formula_is_state_formula (F->operands[0]);
  else
    result = 0;

  return result;
}

			/* --------------- */

ctlstar_formula_kind 
ctlstar_formula_get_kind (const ctlstar_formula *F)
{
  ccl_pre (F != NULL);

  return F->kind;
}

			/* --------------- */

ctlstar_formula **
ctlstar_formula_get_operands (const ctlstar_formula *F)
{
  ccl_pre (F != NULL);

  return F->operands;
}

			/* --------------- */

int
ctlstar_formula_get_arity (const ctlstar_formula *F)
{
  ccl_pre (F != NULL);

  return F->arity;
}

			/* --------------- */

ar_identifier *
ctlstar_formula_get_name (const ctlstar_formula *F)
{
  ccl_pre (F != NULL);

  return ar_identifier_add_reference (F->varname);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_false (void)
{
  /* RWR: 0 -> ~ 1 */      
  ctlstar_formula *top = ctlstar_formula_crt_true ();
  ctlstar_formula *result = ctlstar_formula_crt_not (top);
  ctlstar_formula_del_reference (top);

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_imply (ctlstar_formula *F1, ctlstar_formula *F2)
{
  /* RWR: F1 => F2 -> ~ F1 | F2 */      
  ctlstar_formula *result;

  F1 = ctlstar_formula_crt_not (F1);
  result = ctlstar_formula_crt_or (F1, F2);
  ctlstar_formula_del_reference (F1);

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_equiv (ctlstar_formula *F1, ctlstar_formula *F2)
{
  /* RWR: F1 <=> F2 -> (F1 => F2) & (F2 => F1) */      
  ctlstar_formula *P1 = ctlstar_formula_crt_imply (F1, F2);
  ctlstar_formula *P2 = ctlstar_formula_crt_imply (F2, F1);
  ctlstar_formula *result = ctlstar_formula_crt_and (P1, P2);
  ctlstar_formula_del_reference (P1);
  ctlstar_formula_del_reference (P2);

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_xor (ctlstar_formula *F1, ctlstar_formula *F2)
{
  /* RWR: F1 xor F2 -> ~(F1 <=> F2) */      
  ctlstar_formula *P = ctlstar_formula_crt_equiv (F1, F2);
  ctlstar_formula *result = ctlstar_formula_crt_not (P);
  ctlstar_formula_del_reference (P);

  return result;
}

			/* --------------- */

#define QUANTIFY(_Q, _P, _F) \
  s_quantify_path (ctlstar_formula_crt_ ## _Q, ctlstar_formula_crt_ ## _P, _F)

static ctlstar_formula *
s_quantify_path (ctlstar_formula *(*quantifier) (ctlstar_formula *),
		 ctlstar_formula *(*path_constructor) (ctlstar_formula *),
		 ctlstar_formula *F)
{
  ctlstar_formula *tmp = path_constructor (F);
  ctlstar_formula *result = quantifier (tmp);
  ctlstar_formula_del_reference (tmp);

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_AX (ctlstar_formula *F)
{
  return QUANTIFY (A, X, F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_AF (ctlstar_formula *F)
{
  return QUANTIFY (A, F, F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_AG (ctlstar_formula *F)
{
  return QUANTIFY (A, G, F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_EX (ctlstar_formula *F)
{
  return QUANTIFY (E, X, F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_EF (ctlstar_formula *F)
{
  return QUANTIFY (E, F, F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_EG (ctlstar_formula *F)
{
  return QUANTIFY (E, G, F);
}

			/* --------------- */

static ctlstar_formula *
s_crt_dual (ctlstar_formula * (*operator)(ctlstar_formula *), 
	    ctlstar_formula *F)
{
  ctlstar_formula *tmp1 = ctlstar_formula_crt_not (F);
  ctlstar_formula *tmp2 = operator (tmp1);
  ctlstar_formula *result = ctlstar_formula_crt_not (tmp2);
  ctlstar_formula_del_reference (tmp1);  
  ctlstar_formula_del_reference (tmp2);  

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_A (ctlstar_formula *F)
{
  /* RWR: A p --> ~ E ~p */
  return s_crt_dual (ctlstar_formula_crt_E, F);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_F (ctlstar_formula *Y)
{
  ctlstar_formula *tmp;
  ctlstar_formula *result;

  /* RWR: F F Y --> F Y */
  /* RWR: F G F Y --> G F Y */
  if (IS_F (Y) || IS_GF (Y))
    return ctlstar_formula_add_reference (Y);

  if (Y->kind == CTLSTAR_X)
    {
      /* RWR: F X p --> X F p */
      tmp = ctlstar_formula_crt_F (Y->operands[0]);
      result = ctlstar_formula_crt_X (tmp);
    }
  else
    {
      /* RWR: F p --> 1 U p */
      tmp = ctlstar_formula_crt_true ();
      result = ctlstar_formula_crt_U (tmp, Y);
    }
  ctlstar_formula_del_reference (tmp);

  return result;
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_G (ctlstar_formula *Y)
{
  /* G G Y --> Y */
  if (IS_G (Y))
    return ctlstar_formula_add_reference (Y);

  /* RWR: G p --> ~ F ~ p */
  return s_crt_dual (ctlstar_formula_crt_F, Y);
}

			/* --------------- */

ctlstar_formula *
ctlstar_formula_crt_W (ctlstar_formula *F1, ctlstar_formula *F2)
{
  /* RWR: p W q --> p U q | G p  */
  ctlstar_formula *tmp1 = ctlstar_formula_crt_U (F1, F2);
  ctlstar_formula *tmp2 = ctlstar_formula_crt_G (F1);
  ctlstar_formula *result = ctlstar_formula_crt_or (tmp1, tmp2);
  ctlstar_formula_del_reference (tmp1);
  ctlstar_formula_del_reference (tmp2);

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_duplicate_formula (const ctlstar_formula *F)
{
  int i;
  ctlstar_formula *result = s_allocate_formula (F->kind, F->arity);

  for (i = 0; i < F->arity; i++)
    result->operands[i] = ctlstar_formula_add_reference (F->operands[i]);
  if (F->varname != NULL)
    result->varname = ar_identifier_add_reference (F->varname);

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_expand_formula (const ctlstar_formula *F, ccl_list *defs, int *pdummy)
{
  int i;
  ctlstar_formula *result = s_duplicate_formula (F);

  ccl_pre (!(F->kind == FXP_MU || F->kind == FXP_NU || F->kind == FXP_EX));

  for (i = 0; i < result->arity; i++)
    {
      ctlstar_formula *tmp = 
	s_expand_formula (result->operands[i], defs, pdummy);
      ctlstar_formula_del_reference (result->operands[i]);
      result->operands[i] = tmp;
    }
  
  if (result->kind != CTLSTAR_U && result->kind != CTLSTAR_NOT_U)
    s_sort_operands (result);
  result = s_find_or_add_formula (result);

  if (result->kind == CTLSTAR_E)
    {
      ar_identifier *varname;
      ctlstar_formula *tmp = 
	s_crt_dummy_variable (&varname, INTERNAL_DUMMY_VARIABLES_PREFIX, 
			      pdummy);
      ccl_list_add (defs, varname);
      ccl_list_add (defs, result);
      result = tmp;
    }
  
  ccl_post (result != NULL);
  
  return result;
}

			/* --------------- */

static ctlstar_formula *
s_crt_not_U (ctlstar_formula *F1, ctlstar_formula *F2)
{
  ctlstar_formula *result;

  ccl_pre (F1 != NULL);
  ccl_pre (F2 != NULL);

  if (IS_TRUE (F1) && IS_GF (F2))
    {
      /* RWR: G G F p --> G F p */
      result = ctlstar_formula_add_reference (F2);
    }
  else
    {
      result = s_allocate_formula (CTLSTAR_NOT_U, 2);
      result->operands[0] = ctlstar_formula_add_reference (F1);
      result->operands[1] = ctlstar_formula_add_reference (F2);
      result = s_find_or_add_formula (result);
    }

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_crt_GF (ctlstar_formula *X)
{
  ctlstar_formula *tmp = ctlstar_formula_crt_F (X);
  ctlstar_formula *result = ctlstar_formula_crt_G (tmp);
  ctlstar_formula_del_reference (tmp);

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_allocate_formula (ctlstar_formula_kind kind, int arity)
{
  ctlstar_formula *result = ccl_new (ctlstar_formula);

  result->refcount = 1;
  result->hval = UNKNOWN_HVAL;
  result->kind = kind;
  result->alternation = -1;
  result->varname = NULL;
  result->arity = arity;
  result->flags = 0;
  result->ctx = -1;
  result->scc = -1;
  result->uid = -1;
  result->cached = NULL;
  result->operands = ccl_new_array (ctlstar_formula *, arity);

  return result;
}

			/* --------------- */

static void
s_delete_formula (ctlstar_formula *F)
{
  int i;

  ccl_pre (F != NULL);
  ccl_pre (F->refcount == 0);

  ccl_zdelete (ar_identifier_del_reference, F->varname);
  for (i = 0; i < F->arity; i++)
    ctlstar_formula_del_reference (F->operands[i]);
  ccl_zdelete (ctlstar_formula_del_reference, F->cached);
  ccl_delete (F->operands);
  ccl_delete (F);
}

			/* --------------- */


static void
s_compute_hashvalue (ctlstar_formula *F)
{
  int i;
  unsigned int hval = (13 * ((unsigned int) F->kind) + 
		       19 * (uintptr_t) F->varname);

	  
  for (i = 0; i < F->arity; i++)
    hval = (hval >> 3) + 1711 * s_ctlstar_hash (F->operands[i]);
  
  F->hval = hval;
}

			/* --------------- */

static int
s_alternation_of (ctlstar_formula *F)
{
  int i;
  int result;
  
  switch (F->kind)
    {
    case CTLSTAR_TRUE:
      result = 0;
      break;

    case FXP_MU:
    case FXP_NU:      
    case CTLSTAR_VAR:
      result = F->alternation;
      break;

    case FXP_EX:      
    case CTLSTAR_NOT:
      result = s_alternation_of (F->operands[0]);
      break;

    case CTLSTAR_AND:
    case CTLSTAR_OR:
      result = s_alternation_of (F->operands[0]);
      for (i = 1; i < F->arity; i++)
	{
	  int d = s_alternation_of (F->operands[i]);
	  if (d > result)
	    result = d;
	}
      break;
      break;

    case CTLSTAR_E:
    case CTLSTAR_X:
    case CTLSTAR_NOT_U:
    case CTLSTAR_U:
    default:
      result = -1;
      break;
    }

  ccl_post (result >= 0);

  return result;
}
    

static ctlstar_formula *
s_find_or_add_formula (ctlstar_formula *F)
{
  ctlstar_formula *result;

  ccl_pre (F != NULL);

  s_check_manager (CTLSTAR_MANAGER);
  s_compute_hashvalue (F);
  if (ccl_hash_find (CTLSTAR_MANAGER->dag, F))
    {
      result = ccl_hash_get (CTLSTAR_MANAGER->dag);
      result = ctlstar_formula_add_reference (result);
      ctlstar_formula_del_reference (F);
    }
  else
    {
      int i;      
      result = F;
      ccl_hash_insert (CTLSTAR_MANAGER->dag, result);
      result->uid = CTLSTAR_MANAGER->time++;
      result->height = 0;
      result->alternation = 0;

      for (i = 0; i < result->arity; i++)
	{
	  if (result->operands[i]->height > result->height)
	    result->height = result->operands[i]->height;
	  //	  if (result->operands[i]->alternation > result->alternation)
	  // result->alternation = s_alternation_of (result->operands[i]);
	}
      if (result->kind == FXP_NU || result->kind == FXP_MU)
	{
	  ctlstar_formula *var = ctlstar_formula_crt_variable (result->varname);
	  result->alternation = 1 + s_alternation_of (result->operands[0]);
	  var->alternation = result->alternation;
	  ctlstar_formula_del_reference (var);
	}
      result->height++;
    }
  ccl_assert (ccl_hash_find (CTLSTAR_MANAGER->dag, result));

  s_check_manager (CTLSTAR_MANAGER);
  s_check_formula (result);

  return result;
}

			/* --------------- */

static ctlstar_manager *
s_manager_crt (void)
{
  ctlstar_manager *result = ccl_new (ctlstar_manager);

  result->refcount = 1;
  result->time = 0;
  result->dag = ccl_hash_create (s_ctlstar_hash, s_ctlstar_compare_addr, NULL,
				 (ccl_delete_proc *) s_delete_formula);

  return result;
}

			/* --------------- */

#if 0
static ctlstar_manager *
s_manager_add_reference (ctlstar_manager *cm)
{
  ccl_pre (cm != NULL);
  
  cm->refcount++;

  return cm;
}
#endif
			/* --------------- */

static void
s_manager_del_reference (ctlstar_manager *cm)
{
  ccl_pre (cm != NULL);

  cm->refcount--;

  if (cm->refcount == 0)
    {
      ccl_hash_delete (cm->dag);
      ccl_delete (cm);
    }
}

			/* --------------- */

static unsigned int
s_ctlstar_hash (const void *key)
{
  const ctlstar_formula *F = key;

  return F->hval;
}

			/* --------------- */

static int 
s_ctlstar_compare_addr (const void *k1, const void *k2)
{
  int result;
  const ctlstar_formula *F1 = k1;
  const ctlstar_formula *F2 = k2;

  if (F1 == F2)
    result = 0;
  else if (F1->kind != F2->kind)
    result = (int) F1->kind - (int) F2->kind;
  else if (F1->kind == CTLSTAR_VAR)
    result = (intptr_t) F1->varname - (intptr_t) F2->varname;
  else if (F1->arity != F2->arity)
    result = F1->arity - F2->arity;
  else
    {
      int i;

      result = 0;

      for (i = 0; i < F1->arity && result == 0; i++)
	result = (intptr_t) F1->operands[i] - (intptr_t) F2->operands[i];
    }

  return result;
}

			/* --------------- */

static int 
s_ctlstar_compare_uid (const void *k1, const void *k2)
{
  const ctlstar_formula *F1 = k1;
  const ctlstar_formula *F2 = k2;

  ccl_pre (F1->uid >= 0);
  ccl_pre (F2->uid >= 0);

  if (F1->kind != F2->kind)
    return (int) F1->kind - (int) F2->kind;

  return F1->uid - F2->uid;
}

			/* --------------- */

static int
s_is_simple_node (ctlstar_formula *Ephi)
{
  int i, arity = Ephi->arity;

  for (i = 0; i < arity; i++)
    {
      int j;
      ctlstar_formula *e = Ephi->operands[i];

      if (e->kind != CTLSTAR_U)
	continue;

      for (j = i + 1; j < arity; j++)
	{
	  ctlstar_formula *oe = Ephi->operands[j];

	  if (oe->kind == CTLSTAR_X && oe->operands[0] == e)
	    return 0;
	}
    }

  return 1;
}

			/* --------------- */

static void
s_insert_successors (ccl_vertex *v, ccl_list *Q, ccl_hash *visited)
{
  ccl_edge_iterator *ie = ccl_vertex_get_out_edges (v);

  while (ccl_iterator_has_more_elements (ie))
    {      
      ccl_edge *e = ccl_iterator_next_element (ie);
      ccl_vertex *tgt = ccl_edge_get_tgt (e);
      if (!ccl_hash_find (visited, tgt))
	{
	  ccl_list_add (Q, tgt);
	  ccl_hash_insert (visited, tgt);
	}
    }
  ccl_iterator_delete (ie);
}

			/* --------------- */

static int
s_is_reachable_from_childs (ctlstar_formula *F, ccl_vertex *t)
{    
  int result = 0;
  ccl_list *Q = ccl_list_create ();
  ccl_hash *visited = ccl_hash_create (NULL, NULL, NULL, NULL);

  s_insert_successors (t, Q, visited);
  while (!ccl_list_is_empty (Q) && !result)
    {
      ccl_vertex *v = ccl_list_take_first (Q);
      ctlstar_formula *vF = ccl_vertex_get_data (v);
      result = (vF == F);
      if (! result)
	s_insert_successors (v, Q, visited);
    }
  ccl_list_delete (Q);
  ccl_hash_delete (visited);

  return result;
}

			/* --------------- */

static void
s_create_identity_generation_edges (ccl_graph *gen, ctlstar_formula *F, 
				    ctlstar_formula *G)
{
  int i;

  if (F->kind != CTLSTAR_E)
    return;

  for (i = 0; i < F->arity; i++)
    {
      int j;
      
      for (j = 0; j  < G->arity; j++)
	if (G->operands[j] == F->operands[i])
	  ccl_graph_add_data_edge (gen, F->operands + i, G->operands + j, NULL);
    }
}

			/* --------------- */

static int
s_get_operand_index (ctlstar_formula *F, ctlstar_formula *op)
{
  int i;

  for (i = 0; i < F->arity; i++)
    if (F->operands[i] == op)
      return i;

  return -1;
}

			/* --------------- */

static ccl_vertex *
s_build_tableau (ctlstar_formula *F, ccl_graph *tableau, ccl_graph *gen, int D);

#define s_add_edge(g,s,t) s_add_edge_ (g,s,t,__FILE__,__LINE__)

static void
s_add_edge_ (ccl_graph *tableau, ccl_vertex *src, ccl_vertex *tgt, 
	    const char *filename, int line)
{  
  if (! ccl_vertex_has_successor (src, tgt))
    ccl_graph_add_edge (tableau, src, tgt, NULL);
}

			/* --------------- */

static void
s_bt_X_rule (ctlstar_formula *F, ccl_vertex *vF, ccl_graph *tableau, 
	     ccl_graph *gen, int D)
{
  int i;
  ctlstar_formula *succ;
  ctlstar_formula **operands;
  ccl_vertex *succ_v;

  operands = ccl_new_array (ctlstar_formula *, F->arity);
  for (i = 0; i < F->arity; i++)
    {
      ccl_pre (F->operands[i]->kind == CTLSTAR_X); 
      operands[i] = F->operands[i]->operands[0];
    }
  succ = s_crt_E (F->arity, operands, 0);
  if (succ->kind == CTLSTAR_E)
    {
      for (i = 0; i < F->arity; i++)
	{
	  int j;

	  for (j = 0; j < succ->arity; j++)		
	    if (succ->operands[j] == operands[i])
	      ccl_graph_add_data_edge (gen, F->operands + i, 
				       succ->operands + j, NULL);
	}
    }
  ccl_delete (operands);
  succ_v = s_build_tableau (succ, tableau, gen, D + 1);
  ctlstar_formula_del_reference (succ);
  s_add_edge (tableau, vF, succ_v);
}

			/* --------------- */

static void
s_bt_var_n_not_rule (ctlstar_formula *F, ccl_vertex *vF, ccl_graph *tableau, 
		     ccl_graph *gen, int D)
{
  ccl_vertex *succ_v = s_build_tableau (F->operands[0], tableau, gen, D + 1);

  s_add_edge (tableau, vF, succ_v);

  if (F->arity > 1)
    {
      ctlstar_formula *succ = s_crt_E (F->arity - 1, F->operands + 1, 0);

      s_create_identity_generation_edges (gen, F, succ);
      succ_v = s_build_tableau (succ, tableau, gen, D + 1);
      ctlstar_formula_del_reference (succ);
      s_add_edge (tableau, vF, succ_v);
    }
}

			/* --------------- */

#define s_add_regen_edges(F,vF,arity,operands,offset,tableau,gen,D) \
  s_add_regen_edges_(F,vF,arity,operands,offset,tableau,gen,D,__FILE__,__LINE__)

static void
s_add_regen_edges_ (ctlstar_formula *F, ccl_vertex *vF, int arity, 
		    ctlstar_formula **operands, int offset, ccl_graph *tableau, 
		    ccl_graph *gen, int D, const char *filename, int line)

{
  int i;
  ccl_vertex *succ_v;
  ctlstar_formula *succ = s_crt_E (arity, operands, 0);

  s_create_identity_generation_edges (gen, F, succ);
  
  for (i = offset; i < arity; i++)
    {
      int index = s_get_operand_index (succ, operands[i]);
      if (index >= 0)
	ccl_graph_add_data_edge (gen, F->operands, succ->operands + index, 
				 NULL);
    }  

  succ_v = s_build_tableau (succ, tableau, gen, D + 1);
  s_add_edge_ (tableau, vF, succ_v, filename, line);
  ctlstar_formula_del_reference (succ);
}

			/* --------------- */

static void
s_bt_U_rule (ctlstar_formula *F, ccl_vertex *vF, ccl_graph *tableau, 
	     ccl_graph *gen, int D)
{
  int offset = F->arity-1;
  ctlstar_formula *u = F->operands[0];
  ctlstar_formula **operands = ccl_new_array (ctlstar_formula *, F->arity + 1);
  ctlstar_formula **newops = operands + offset;

  ccl_memcpy (operands,  F->operands + 1, 
	      sizeof (ctlstar_formula *) * (F->arity - 1));

  /* E(Phi, p U q) -> E(Phi, q) */
  newops[0] = u->operands[1];
  s_add_regen_edges (F, vF, F->arity, operands, offset, tableau, gen, D);

  /* E(Phi, p U q) -> E(Phi, p, X(p U q)) */
  newops[0] = u->operands[0];
  newops[1] = ctlstar_formula_crt_X (u);
  s_add_regen_edges (F, vF, F->arity + 1, operands, offset, tableau, gen, D);
  ctlstar_formula_del_reference (newops[1]);
  ccl_delete (operands);
}

			/* --------------- */

static void
s_bt_not_U_rule (ctlstar_formula *F, ccl_vertex *vF, ccl_graph *tableau, 
		 ccl_graph *gen, int D)
{
  int offset = F->arity - 1;
  ctlstar_formula *not_u = F->operands[0];
  ctlstar_formula **operands = ccl_new_array (ctlstar_formula *, F->arity + 1);
  ctlstar_formula **newops = operands + offset;

  ccl_memcpy (operands,  F->operands + 1, 
	      sizeof (ctlstar_formula *) * (F->arity - 1));

  /* E(Phi, p ~U q) -> E(Phi, ~p, ~q) */
  newops[0] = ctlstar_formula_crt_not (not_u->operands[0]);
  newops[1] = ctlstar_formula_crt_not (not_u->operands[1]);
  s_add_regen_edges (F, vF, F->arity + 1, operands, offset, tableau, gen, D);
  ctlstar_formula_del_reference (newops[0]);
  ctlstar_formula_del_reference (newops[1]);

  /* E(Phi, p ~U q) -> E(Phi, ~q, X(p ~U q)) */
  newops[0] = ctlstar_formula_crt_not (not_u->operands[1]);
  newops[1] = ctlstar_formula_crt_X (not_u);

  s_add_regen_edges (F, vF, F->arity + 1, operands, offset, tableau, gen, D);
  ctlstar_formula_del_reference (newops[0]);
  ctlstar_formula_del_reference (newops[1]);

  ccl_delete (operands);
}

			/* --------------- */

static void
s_bt_and_rule (ctlstar_formula *F, ccl_vertex *vF, ccl_graph *tableau, 
	       ccl_graph *gen, int D)
{
  int offset = F->arity - 1;
  ctlstar_formula *phi = F->operands[0];
  ctlstar_formula **operands = 
    ccl_new_array (ctlstar_formula *, F->arity - 1 + phi->arity);
  ctlstar_formula **new_operands = operands + offset;

  ccl_memcpy (operands,  F->operands + 1, 
	      sizeof (ctlstar_formula *) * (F->arity - 1));

  ccl_memcpy (new_operands,  phi->operands, 
	      sizeof (ctlstar_formula *) * phi->arity);

  s_add_regen_edges (F, vF, F->arity - 1 + phi->arity, operands, offset, 
		     tableau, gen, D);
  ccl_delete (operands);
}

			/* --------------- */

static void
s_bt_or_rule (ctlstar_formula *F, ccl_vertex *vF, ccl_graph *tableau, 
	      ccl_graph *gen, int D)
{
  int i;
  int offset =  F->arity - 1;
  ctlstar_formula *phi = F->operands[0];
  ctlstar_formula **operands = ccl_new_array (ctlstar_formula *, F->arity);
  ctlstar_formula **newops = operands + offset;

  ccl_memcpy (operands,  F->operands + 1, 
	      sizeof (ctlstar_formula *) * (F->arity - 1));

  for (i = 0; i < phi->arity; i++)
    {
      newops[0] = phi->operands[i];
      s_add_regen_edges (F, vF, F->arity, operands, offset, tableau, gen, D);
    }
  ccl_delete (operands);
}

			/* --------------- */

static ccl_vertex *
s_build_tableau (ctlstar_formula *F, ccl_graph *tableau, ccl_graph *gen, int D)
{
  ccl_vertex *result = ccl_graph_get_vertex (tableau, F);

  if (ccl_debug_is_on)
    {
      ccl_debug ("%*s[%03d] %s ", D, "", D,
		 ((result != NULL || F->kind != CTLSTAR_E) 
		  ? "skip formula" 
		  : "build tableau for"));
      ctlstar_formula_log (CCL_LOG_DEBUG, F, NULL, 0);
      ccl_debug ("\n");
    }

  if (result != NULL)
    return result;

  F = ctlstar_formula_add_reference (F);
  result = ccl_graph_add_vertex (tableau, F);
  if (F->kind != CTLSTAR_E)
    return result;

  switch (F->operands[0]->kind)
    {     
    case CTLSTAR_X: s_bt_X_rule (F, result, tableau, gen, D); break;
    case CTLSTAR_VAR: 
    case CTLSTAR_NOT: s_bt_var_n_not_rule (F, result, tableau, gen, D); break;
    case CTLSTAR_U: s_bt_U_rule (F, result, tableau, gen, D); break;
    case CTLSTAR_NOT_U: s_bt_not_U_rule (F, result, tableau, gen, D); break;
    case CTLSTAR_AND: s_bt_and_rule (F, result, tableau, gen, D); break;
    case CTLSTAR_OR: s_bt_or_rule (F, result, tableau, gen, D); break;
    default: ccl_unreachable (); break;
    }

  return result;
}

			/* --------------- */
#if 0
static void
s_log_formula_vertex (ccl_log_type log, ccl_vertex *v, void *cbdata)
{
  void *data = ccl_vertex_get_data (v);
  ctlstar_formula *F = data;
  ccl_log (log, "label=\"");
  ctlstar_formula_log (log, F);
  if (F->scc >= 0)
    ccl_log (log, "\\n%d", F->scc);
  ccl_log (log, "\"");
}
			/* --------------- */

static void
s_display_tree_as_dot (ccl_log_type log, ccl_graph *t)
{
  ccl_graph_log_as_dot (log, t, s_log_formula_vertex, NULL, NULL, NULL);
}
#endif
			/* --------------- */

static int 
s_is_conjunctive_rule (ctlstar_formula *F)
{
  int i;

  for (i = 0; i < F->arity; i++)
    {
      if (F->operands[i]->kind != CTLSTAR_X)
	{
	  ctlstar_formula_kind not_X_kind = F->operands[i]->kind;

	  return (not_X_kind == CTLSTAR_VAR ||
		  not_X_kind == CTLSTAR_NOT || 
		  not_X_kind == CTLSTAR_TRUE);
	}
    }

  return 0;
}

			/* --------------- */

static ctlstar_formula *
s_apply_rule (ctlstar_formula *F, int nb_operands, ctlstar_formula **operands)
{
  int i;
  ctlstar_formula *result;
  int not_X_index = -1;
  ctlstar_formula *not_X = NULL;
  ctlstar_formula_kind not_X_kind;

  for (i = 0; i < F->arity && not_X_index == -1; i++)
    {
      if (F->operands[i]->kind != CTLSTAR_X)
	{
	  not_X_index = i;	
	  not_X_kind = F->operands[i]->kind;
	  not_X = F->operands[i];
	}
    }

  if (not_X != NULL)
    {
      switch (not_X_kind)
	{
	case CTLSTAR_AND:
	  ccl_assert (nb_operands == 1);      
	  result = ctlstar_formula_add_reference (operands[0]);
	  break;

	case CTLSTAR_OR: case CTLSTAR_U: case CTLSTAR_NOT_U:
	  result = ctlstar_formula_add_reference (operands[0]);
	  for (i = 1; i < nb_operands; i++)
	    {
	      ctlstar_formula *tmp = 
		ctlstar_formula_crt_or (result, operands[i]);
	      ctlstar_formula_del_reference (result);
	      result = tmp;
	    }
	  break;

	case CTLSTAR_VAR: case CTLSTAR_NOT: case CTLSTAR_TRUE:
	  result = ctlstar_formula_add_reference (operands[0]);
	  for (i = 1; i < nb_operands; i++)
	    {
	      ctlstar_formula *tmp = 
		ctlstar_formula_crt_and (result, operands[i]);
	      ctlstar_formula_del_reference (result);
	      result = tmp;
	    }
	  break;

	default:
	  ccl_unreachable ();
	  break;
	}
    }
  else
    {
      ccl_assert (nb_operands == 1);
      result = s_crt_fixpoint_EX (operands[0]);
    }
  return result;
}

			/* --------------- */

struct node_chain 
{
  struct node_chain *pred;
  ccl_vertex *node;
  int scc;
};

static ctlstar_formula *
s_translate_tableau (ccl_vertex *t, ccl_hash *tmpvars, ccl_graph *gen, 
		     struct node_chain  *pred, int *pcounter);

static ctlstar_formula *
s_apply_rule_with_context (ccl_vertex *t, ccl_hash *tmpvars, ccl_graph *gen, 
			   struct node_chain  *pred, int *pcounter)
{
  int i = 0;
  int stop = 0;
  ctlstar_formula *result; 
  int nb_operands = ccl_vertex_get_out_degree (t);
  ctlstar_formula **operands = ccl_new_array (ctlstar_formula *, nb_operands);
  ctlstar_formula *F = ccl_vertex_get_data (t);
  int conj = s_is_conjunctive_rule (F);
  ccl_edge_iterator *ie = ccl_vertex_get_out_edges (t);
  ccl_assert (nb_operands > 0);

  while (!stop && ccl_iterator_has_more_elements (ie))
    {
      ccl_edge *e = ccl_iterator_next_element (ie);
      ccl_vertex *c = ccl_edge_get_tgt (e);

      operands[i] = s_translate_tableau (c, tmpvars, gen, pred, pcounter);

      if (conj)
	{
	  int j;

	  stop = IS_FALSE (operands[i]);

	  for (j = 0; j < i && ! stop; j++)
	    stop = s_are_opposite (operands[i], operands[j]);
	}
      i++;
    }
  ccl_iterator_delete (ie);

  if (stop)
    result = ctlstar_formula_crt_false ();
  else
    result = s_apply_rule (F, nb_operands, operands);
  while (i--)
    ctlstar_formula_del_reference (operands[i]);

  ccl_delete (operands);

  return result;
}

			/* --------------- */

#define s_find_or_add_tmpvar(tmpvars,F,pcounter)		\
  s_find_or_add_tmpvar_(tmpvars,F,pcounter,__FILE__,__LINE__)

static ar_identifier *
s_find_or_add_tmpvar_ (ccl_hash *tmpvars, ctlstar_formula *F, int *pcounter,
		       const char *filename, int line)
{
  ar_identifier *result;
  ctlstar_formula *C[2];

  C[0] = F;
  C[1] = F->ctx >= 0 ? F->operands[F->ctx] : NULL;

  if (ccl_hash_find (tmpvars, C))
    {
      result = ccl_hash_get (tmpvars);
      if (ccl_debug_is_on)
	{
	  ctlstar_formula_log (CCL_LOG_DEBUG, C[0], NULL, 0);
	  if (C[1] != NULL)
	    {
	      ccl_debug (", ");
	      ctlstar_formula_log (CCL_LOG_DEBUG, C[1], NULL, 0);
	    }
	  ccl_debug (" --> ");
	  ar_identifier_log (CCL_LOG_DEBUG, result);
	  ccl_debug (" (found in cache)\n");
	}
    }
  else
    {
      const char *prefix = (C[1] != NULL 
			    ? INTERNAL_DUMMY_VARIABLES_PREFIX2
			    : INTERNAL_DUMMY_VARIABLES_PREFIX);
      ctlstar_formula **couple = ccl_new_array (ctlstar_formula *, 2);

      couple[0] = C[0];
      couple[1] = C[1];
      result = ar_identifier_create_numbered_auto (prefix, pcounter);
      if (ccl_debug_is_on)
	{
	  ctlstar_formula_log (CCL_LOG_DEBUG, C[0], NULL, 0);
	  if (C[1] != NULL)
	    {
	      ccl_debug (", ");
	      ctlstar_formula_log (CCL_LOG_DEBUG, C[1], NULL, 0);
	    }
	  ccl_debug (" --> ");
	  ar_identifier_log (CCL_LOG_DEBUG, result);
	  ccl_debug ("\n");
	}
      ccl_hash_find (tmpvars, couple);
      ccl_hash_insert (tmpvars, result);
    }
  result = ar_identifier_add_reference (result);

  return result;
}

			/* --------------- */

static int DDD = 0;

static int 
s_simple_regeneration_test (ctlstar_formula *F, ctlstar_formula *subF)
{
  int i;
  int result = 1;

  ccl_pre (IS_EVENTUALITY (subF));

  for (i = 0; i < F->arity && result; i++)
    {
      if (F->operands[i] != subF)
	result = ! s_is_subformula_of (subF, F->operands[i]);
    }

  return result;
}

			/* --------------- */

static int
s_is_regenerated (ccl_vertex *v, ccl_vertex *root, ctlstar_formula **ev, 
		  struct node_chain *pred)
{
  int result = 0;
  int q = 0;
  ccl_list *Q[2];
  ctlstar_formula *rootF = ccl_vertex_get_data (root);

  ccl_pre (ev == ccl_vertex_get_data (v));
  ccl_pre (rootF->operands <= ev && ev < rootF->operands + rootF->arity);

  if (s_simple_regeneration_test (rootF, *ev))
    {
      if (ccl_debug_is_on)
	{
	  ctlstar_formula_log (CCL_LOG_DEBUG, rootF, NULL, 0);
	  ccl_debug (" is simply regenerated.\n");
	}
      return 1;
    }
  
  ccl_assert (pred != NULL);

  DDD++;
  Q[0] = ccl_list_create ();
  ccl_list_add (Q[0], v);
  Q[1] = ccl_list_create ();

  while (!ccl_list_is_empty (Q[q]) && !result)
    {
      ctlstar_formula *P = ccl_vertex_get_data (pred->node);
      ccl_hash *s = ccl_hash_create (NULL, NULL, NULL, NULL);

      while (! ccl_list_is_empty (Q[q]) && !result)
	{
	  ccl_edge_iterator *ie;

	  v = ccl_list_take_first (Q[q]);
	  ie = ccl_vertex_get_in_edges (v);

	  while (ccl_iterator_has_more_elements (ie) && !result)
	    {
	      ccl_edge *e = ccl_iterator_next_element (ie);
	      ccl_vertex *p = ccl_edge_get_src (e);
	      ctlstar_formula **F = ccl_vertex_get_data (p);

	      if (P->operands <= F && F < P->operands + P->arity)
		{

		  result = (ev == F);
		  
		  if (!result && ! ccl_hash_find (s, p))
		    {
		      ccl_hash_insert (s, p);
		      ccl_list_add (Q[(q + 1) & 0x1], p);
		    }
		}
	    }
	  ccl_iterator_delete (ie);
	}
      ccl_hash_delete (s);

      if (pred->node == root)
	break;
      q = (q + 1) & 0x1;
      pred = pred->pred;
      ccl_assert (pred != NULL);
    }

  ccl_list_delete (Q[0]);
  ccl_list_delete (Q[1]);

  if (ccl_debug_is_on)
    {
      ctlstar_formula_log (CCL_LOG_DEBUG, rootF, NULL, 0);
      ccl_debug (" is %sregenerated.\n", result ? "" : "not ");
    }

  return result;
}

			/* --------------- */
#if 0
static void
s_log_segment (ccl_log_type log, struct node_chain  *pred, 
	       ccl_vertex *start)
{
  if (pred == NULL)
    {
      ccl_log (log, "NULL\n");
      return;
    }
  if (pred->node != start)
    s_log_segment (log, pred->pred, start);
  ctlstar_formula_log (log, ccl_vertex_get_data (pred->node));
  ccl_log (log, "\n");
}
#endif
			/* --------------- */

static ctlstar_formula *
s_translate_tableau (ccl_vertex *t, ccl_hash *tmpvars, ccl_graph *gen, 
		     struct node_chain  *pred, int *pcounter)
{
  ctlstar_formula *result; 
  ctlstar_formula *F = ccl_vertex_get_data (t);

  if (F->kind == CTLSTAR_VAR || F->kind == CTLSTAR_NOT || 
      F->kind == CTLSTAR_TRUE)
    result = ctlstar_formula_add_reference (F);
  else if (F->flags)
    { 
      if (F->ctx >= 0)
	{
	  ar_identifier *varname;
	  ctlstar_formula **pev = F->operands + F->ctx;
	  ccl_vertex *v = ccl_graph_get_vertex (gen, pev);

	  if (v != NULL && s_is_regenerated (v, t, pev, pred))
	    varname = s_find_or_add_tmpvar (tmpvars, F, pcounter);
	  else
	    {
	      int save = F->ctx;
	      F->ctx = -1;
	      varname = s_find_or_add_tmpvar (tmpvars, F, pcounter);
	      F->ctx = save;
	    }

	  result = ctlstar_formula_crt_variable (varname);
	  ar_identifier_del_reference (varname);
	}
      else
	{
	  ar_identifier *varname = s_find_or_add_tmpvar (tmpvars, F, pcounter);
	  result = ctlstar_formula_crt_variable (varname);
	  ar_identifier_del_reference (varname);
	}
    }
  else
    {
      struct node_chain nc;
      nc.pred = pred;
      nc.node = t;
      nc.scc = F->scc;
#define CACHE 1
      if (CACHE && pred != NULL && pred->scc != F->scc && F->cached != NULL)
	return ctlstar_formula_add_reference (F->cached);
      
      F->flags = 1;
      F->ctx = -1;

      if (! s_is_simple_node (F) && ! s_is_reachable_from_childs (F, t))
	result = s_apply_rule_with_context (t, tmpvars, gen, &nc, pcounter);
      else
	{
	  int i;
	  ctlstar_formula *subF = NULL;
	  ar_identifier *varname = s_find_or_add_tmpvar (tmpvars, F, pcounter);
	  
	  for (i = 0; i < F->arity; i++)
	    {
	      ar_identifier *tv;
	      ctlstar_formula *tmp[2];

	      if (! IS_EVENTUALITY(F->operands[i]))
		continue;

	      F->ctx = i;
	      
	      tmp[0] = 
		s_apply_rule_with_context (t, tmpvars, gen, &nc, pcounter);
	      tv = s_find_or_add_tmpvar (tmpvars, F, pcounter);
	      tmp[1] = s_crt_fixpoint (FXP_MU, tv, tmp[0]);
	      ctlstar_formula_del_reference (tmp[0]); 
	      ar_identifier_del_reference (tv);

	      if (subF == NULL)
		subF = tmp[1];
	      else
		{
		  tmp[0] = ctlstar_formula_crt_and (subF, tmp[1]);
		  ctlstar_formula_del_reference (tmp[1]);
		  ctlstar_formula_del_reference (subF);
		  subF = tmp[0];
		}
	      F->ctx = -1;
	    }

	  if (subF == NULL)
	    subF = s_apply_rule_with_context (t, tmpvars, gen, &nc, pcounter);

	  result = s_crt_fixpoint (FXP_NU, varname, subF);
	  ctlstar_formula_del_reference (subF);
	  ar_identifier_del_reference (varname);
	}
      F->flags = 0;
      ccl_post (F->ctx == -1);

      if (CACHE && pred != NULL && pred->scc != F->scc)
	{
	  ccl_assert (F->cached == NULL);
	  F->cached = ctlstar_formula_add_reference (result);
	}
    }
  s_check_formula (result);

  return result;
}

			/* --------------- */

static unsigned int
s_hash_formula_couple (const void *p)
{
  ctlstar_formula * const *C = p;

  return 1711 * ((uintptr_t) C[0]) + 19 * ((uintptr_t) C[1]);
}


			/* --------------- */


static int
s_compare_formula_couple (const void *p1, const void *p2)
{
  ctlstar_formula * const *C1 = p1;
  ctlstar_formula * const *C2 = p2;

  if (C1[0] != C2[0])
    return (uintptr_t) C1[0] - (uintptr_t) C2[0];
  else 
    return (uintptr_t) C1[1] - (uintptr_t) C2[1];
}

			/* --------------- */

static void
s_attach_scc (ccl_graph *G, void *data, int comp, void *cbdata)
{
  ctlstar_formula *F = data;
  F->scc = comp;
}

			/* --------------- */

CCL_DEFINE_DELETE_PROC(s_delete_formula_couple)

static ctlstar_formula *
s_ctlstar_to_fixpoint (ctlstar_formula *F, int *pcounter)
{
  ccl_vertex_data_methods tmethods = { NULL, NULL, (ccl_delete_proc *) 
				       ctlstar_formula_del_reference };
  ccl_vertex_data_methods vmethods = { NULL, NULL, NULL };
  ccl_edge_data_methods emethods = { NULL, NULL, NULL };
  ctlstar_formula *result = NULL;
  ccl_hash *tmpvars = 
    ccl_hash_create (s_hash_formula_couple, s_compare_formula_couple, 
		     s_delete_formula_couple,
		     (ccl_delete_proc *) ar_identifier_del_reference);
  ccl_graph *tableau = ccl_graph_create (&tmethods, &emethods);
  ccl_graph *gen = ccl_graph_create (&vmethods, &emethods);
  ccl_vertex *top = s_build_tableau (F, tableau, gen, 0);
  int nb_scc = ccl_graph_compute_scc (tableau, s_attach_scc, NULL);

  if (ccl_debug_is_on)
    ccl_debug ("tableau has %d scc and %d nodes.\n", nb_scc,
	       ccl_graph_get_number_of_vertices (tableau));
  result = s_translate_tableau (top, tmpvars, gen, NULL, pcounter);

  ccl_hash_delete (tmpvars);
  ccl_graph_del_reference (gen);
  ccl_graph_del_reference (tableau);

  return result;
}


			/* --------------- */

static void
s_make_operands_unique (int *p_arity, ctlstar_formula **operands)
{
  int i = 0;
  int arity = *p_arity;

  while (i < arity)
    {
      int j, nb_to_remove;

      for (j = i + 1; j < arity && operands[j] == operands[i]; j++)
	CCL_NOP ();

      nb_to_remove = j - i - 1;
      if (nb_to_remove == 0)
	i++;
      else
	{
	  operands[i]->refcount -= nb_to_remove;
	  ccl_assert (operands[i]->refcount > 0);
	  if (j < arity)
	    {
	      ctlstar_formula **dst = operands + i + 1;
	      ctlstar_formula **src = operands + j;

	      for (; j < arity; j++, dst++, src++)
		*dst = *src;
	    }
	  arity -= nb_to_remove;
	}
    }
  *p_arity = arity;
}

			/* --------------- */

static int
s_compare_formulas (const void *p1, const void *p2)
{
  const ctlstar_formula *F1 = *(ctlstar_formula * const *) p1;
  const ctlstar_formula *F2 = *(ctlstar_formula * const *) p2;

  return s_ctlstar_compare_uid (F1, F2);
}

			/* --------------- */

static void
s_check_formula (ctlstar_formula *F)
{
  if (ENABLE_CHECKS)
    {
      int i;

      ccl_assert (F->refcount > 0);
      for (i = 0; i < F->arity; i++)
	s_check_formula (F->operands[i]);
    }
}

			/* --------------- */

static void
s_check_manager (ctlstar_manager *man)
{
  if (ENABLE_CHECKS)
    {
      ccl_hash_entry_iterator *i = ccl_hash_get_entries (man->dag);

      while (ccl_iterator_has_more_elements (i))
	{
	  ccl_hash_entry e = ccl_iterator_next_element (i);
	  ccl_assert (e.key == e.object);
	  s_check_formula (e.key);
	}
      ccl_iterator_delete (i);
    }
}

			/* --------------- */

static ctlstar_formula *
s_crt_fixpoint_EX (ctlstar_formula *arg)
{
  ctlstar_formula *result = NULL;

  if (IS_TRUE (arg) || IS_FALSE (arg))
    result = ctlstar_formula_add_reference (arg);
  else
    {
      result = s_allocate_formula (FXP_EX, 1);
      result->operands[0] = ctlstar_formula_add_reference (arg);
      result = s_find_or_add_formula (result);
    }

  ccl_post (result != NULL);

  return result;
}

			/* --------------- */

static int
s_is_free_variable (ctlstar_formula *F, ar_identifier *V)
{
  int i, result = 0;

  switch (F->kind)
    {
    case CTLSTAR_TRUE: 
      result = 0; 
      break;

    case CTLSTAR_VAR: 
      result = F->varname == V; 
      break;

    case CTLSTAR_NOT: 
    case CTLSTAR_AND:
    case CTLSTAR_OR:
    case FXP_MU:
    case FXP_NU:      
    case FXP_EX:      
      result = 0;
      for (i = 0; i < F->arity && ! result; i++)
	result = s_is_free_variable (F->operands[i], V);
      if (result && (F->kind == FXP_MU || F->kind == FXP_NU))
	result = (F->varname != V);
      break;

    case CTLSTAR_X:
    case CTLSTAR_U:
    case CTLSTAR_NOT_U:
    case CTLSTAR_E:
    default:
      ccl_unreachable ();
      break;
    }

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_crt_fixpoint (ctlstar_formula_kind kind, ar_identifier *qvarname, 
		ctlstar_formula *arg)
{
  ctlstar_formula *result;

  if (! s_is_free_variable (arg, qvarname))
    {      
      result = ctlstar_formula_add_reference (arg);
    }
  else
    {      
      result = s_allocate_formula (kind, 1);
      result->varname = ar_identifier_add_reference (qvarname);
      result->operands[0] = ctlstar_formula_add_reference (arg);            
      result = s_find_or_add_formula (result);      
    }

  ccl_post (result->alternation >= 0);

  if (ccl_debug_is_on)
    {
      ccl_debug ("s_crt_fixpoint (%d, ", kind);
      ar_identifier_log (CCL_LOG_DEBUG, qvarname);
      ccl_debug (", ");
      ctlstar_formula_log (CCL_LOG_DEBUG, arg, NULL, 0);	  
      ccl_debug (")= ");
      ctlstar_formula_log (CCL_LOG_DEBUG, result, NULL, 0);	  
      //ccl_debug (" %p\n", result);
      ccl_debug ("\n");
    }
  
  return result;
}

			/* --------------- */

#define ZN ((altarica_tree *) NULL)

#define NN(_type,_vtype,_child,_next) \
  _NN (_type, #_type, _vtype, _child, _next, __FILE__, __LINE__)

#define NE(_type,_child,_next) NN (_type, CCL_PARSE_TREE_EMPTY, _child, _next)

#define NID()  NN (AR_TREE_IDENTIFIER, CCL_PARSE_TREE_IDENT, ZN, ZN)
#define NINT() NN (AR_TREE_INTEGER, CCL_PARSE_TREE_INT, ZN, ZN)
#define NFLT() NN (AR_TREE_FLOAT, CCL_PARSE_TREE_FLOAT,NULL,NULL)
#define NSTR() NN (AR_TREE_STRING, CCL_PARSE_TREE_STRING, ZN, ZN)
#define REVERSE(_t) ccl_parse_tree_reverse_siblings (_t)

static altarica_tree *
_NN (int type, const char *type_string, ccl_parse_tree_value_type vtype, 
     altarica_tree *child, altarica_tree *next, const char *filename, int line)
{
  return ccl_parse_tree_create (type, type_string, vtype, line, filename, 
				child, next, NULL);
}

			/* --------------- */

static ccl_ustring
s_generate_unique (const char *prefix, int *p_counter)
{
  char *s = ccl_string_format_new ("%s%d", prefix, *p_counter);
  ccl_ustring result = ccl_string_make_unique (s);
  ccl_string_delete (s);
  (*p_counter)++;

  return result;
}

			/* --------------- */

static altarica_tree *
s_crt_id (ccl_ustring id, altarica_tree *next)
{
  altarica_tree *result = NID ();
  result->value.id_value = id;
  result->next = next;

  return result;
}

			/* --------------- */

static altarica_tree *
s_crt_bang (ccl_ustring node, ccl_ustring b, altarica_tree *next)
{
  altarica_tree *bid = s_crt_id (b, ZN);
  altarica_tree *nodeid = s_crt_id (node, bid);
  altarica_tree *result = NE (AR_TREE_BANG_ID, nodeid, ZN);

  result->next = next;

  return result;
}

			/* --------------- */

static altarica_tree *
s_exists (ccl_ustring node, ccl_ustring v, ccl_ustring bang, altarica_tree *F)
{
  altarica_tree *dom = s_crt_bang (node, bang, ZN);
  altarica_tree *vid = s_crt_id (v, NULL);
  altarica_tree *result = NE (AR_TREE_ID_LIST, vid, ZN);

  result->next = dom;

  result = NE (AR_TREE_QUANTIFIED_VARIABLES, result, ZN);
  result = NE (AR_TREE_QUANTIFIED_VARIABLE_LIST, result, ZN);
  result->next = NE (AR_TREE_PARENTHEZED_EXPR, F, ZN);

  result = NE (AR_TREE_EXIST, result, ZN);

  return result;
}

			/* --------------- */

static altarica_tree *
s_crt_bang_call (ccl_ustring node, ccl_ustring bang, altarica_tree *args,
		 altarica_tree *next)
{
  altarica_tree *bangid;
  altarica_tree *result;

  if (node != NULL)
    bangid = s_crt_bang (node, bang, args);
  else
    bangid = s_crt_id (bang, args);
  
  result = NE (AR_TREE_FUNCTION_CALL, bangid, ZN);
  result->next = next;

  return result;
}

			/* --------------- */

static altarica_tree *
s_crt_ex (ccl_ustring node, ccl_ustring src, ccl_ustring tgt, altarica_tree *F,
	  int *p_counter)
{
  ccl_ustring sc = ccl_string_make_unique ("c");
#if USE_MOVE_FOR_EX
  ccl_ustring st = ccl_string_make_unique ("nsemove");
  altarica_tree *tgtid = s_crt_id (tgt, ZN);
  altarica_tree *srcid = s_crt_id (src, tgtid);
  altarica_tree *result;
  
  if (F->node_type == AR_TREE_OR || F->node_type == AR_TREE_EXIST)
    F = NE (AR_TREE_PARENTHEZED_EXPR, F, ZN);
  result = s_crt_bang_call (node, st, srcid, F);
  result = NE (AR_TREE_AND, result, ZN);

#else
  ccl_ustring ev = ccl_string_make_unique ("ev");
  ccl_ustring st = ccl_string_make_unique ("st");
  altarica_tree *tgtid = s_crt_id (tgt, ZN);
  ccl_ustring e = s_generate_unique ("e~", p_counter);
  altarica_tree *eid = s_crt_id (e, tgtid);
  altarica_tree *srcid = s_crt_id (src, eid);
  altarica_tree *result;

  if (F->node_type == AR_TREE_OR || F->node_type == AR_TREE_EXIST)
    F = NE (AR_TREE_PARENTHEZED_EXPR, F, ZN);
  result = s_crt_bang_call (node, st, srcid, F);
  result = NE (AR_TREE_AND, result, ZN);
  result = s_exists (node, tgt, sc, result);
  result = s_exists (node, e, ev, result);
#endif

  result = s_exists (node, tgt, sc, result);
  return result;
}

			/* --------------- */

static ccl_ustring
s_id_to_ustring (ar_identifier *id)
{
  char *tmp = ar_identifier_to_string (id);
  ccl_ustring result = ccl_string_make_unique (tmp);
  ccl_string_delete (tmp);

  return result;
}

			/* --------------- */

static void
s_crt_equation (int local, ar_identifier *relname, ccl_ustring varname, 
		ccl_ustring node, ccl_ustring bang, int label, int depth, 
		altarica_tree *qF, altarica_tree **pequations)
{
  altarica_tree *conf = s_crt_bang (node, bang, NULL);
  altarica_tree *v = s_crt_id (varname, conf);
  altarica_tree *param = NE (AR_TREE_TYPED_ID, v, ZN);
  altarica_tree *paramlist = NE (AR_TREE_EQ_PARAMETERS, param, ZN);
  ccl_ustring rname = s_id_to_ustring (relname);
  altarica_tree *R = s_crt_id (rname, paramlist);

  altarica_tree *result = NE (label, R, ZN);


  altarica_tree *arg = s_crt_id (varname, NULL);
  ccl_ustring any_s = ccl_string_make_unique ("any_s");
  altarica_tree *in_any_s = s_crt_bang_call (node, any_s, arg, qF);
  qF = NE (AR_TREE_AND, in_any_s, NULL);

  paramlist->next = qF;
  result->value.int_value = depth;
 
  if (local)
    {
      result = NE (AR_TREE_LOCAL_EQUATION, result, ZN);
    }

  result->next = *pequations;
  *pequations = result;
}

			/* --------------- */
#if 0
static int
s_has_equation_with_name (altarica_tree *eqlist, ccl_ustring name)
{
  for (; eqlist != NULL; eqlist = eqlist->next)
    {
      ccl_assert (eqlist->node_type == AR_TREE_LOCAL_EQUATION);
      ccl_assert (eqlist->child->child->node_type == AR_TREE_IDENTIFIER);
      if (eqlist->child->child->value.id_value == name)
	return 1;
    }

  return 0;
}
#endif
			/* --------------- */

static ar_identifier *
s_rename_with_counter (ar_identifier *id, ar_idtable *idcounters)
{
  char *s;
  uintptr_t c = 1 + (uintptr_t) ar_idtable_get (idcounters, id);

  ar_idtable_put (idcounters, id, (void *) c);

  s = ar_identifier_to_string (id);
  ccl_string_format_append (&s, "_%d", c);
  id = ar_identifier_create (s);
  ccl_string_delete (s);

  return id;
}

/*!
 * \brief Translate \mu-calculus formula into Mec 5 equations.
 * 
 * The algorithm assumes that the input formula F is the result of the Dam's 
 * translation i.e. CTL* eventualities have been replaced by \mu or \nu 
 * quantifiers.
 *
 * \param F the formula to translate
 * \param nodename if not NULL created relations are attached to this node 
 *                 with the ! notation
 * \param var the formal parameter of the relation been defined.
 * \param pequations the final list of equations. 
 * \param p_counter a pointer to a counter used for unique ID generator
 * \param equations_trees a cache for generated formulas
 * \param idcounters table that maintains counter to generate IDs for 
 *                   identifiers
 * \param iscst predicate used to check if an identifier is a constant relation 
 *        i.e. defined prior to the equation system
 * \param data for the the callback iscst
 * 
 * \return the AST of the Mec 5 formula.
 */
static altarica_tree *
s_formula_to_mec5 (ctlstar_formula *F, ccl_ustring nodename, 
		   ccl_ustring var, altarica_tree **pequations, int *p_counter, 
		   ccl_hash *equations_trees, ar_idtable *idcounters,
		   int (*iscst)(ar_identifier *id, void *data),
		   void *data)
{
  int i;
  altarica_tree *tmp[2];
  altarica_tree *result;

  switch (F->kind)
    {
    case CTLSTAR_TRUE:
      /* true ---> true */
      result = NE (AR_TREE_TRUE, ZN, ZN);
      break;

    case CTLSTAR_VAR:
      /* REL ---> nodename ! REL (var) */
      {
	ccl_ustring bangnode = iscst (F->varname, data) ? nodename : NULL;
	char *tmps = ar_identifier_to_string (F->varname);
	ccl_ustring cstname = ccl_string_make_unique (tmps);
	ccl_string_delete (tmps);

	tmp[0] = s_crt_id (var, ZN);

	result = s_crt_bang_call (bangnode, cstname, tmp[0], ZN);
      }
      break;

    case CTLSTAR_NOT:
      /* not G ----> not tr(G) */
      tmp[0] = 
	s_formula_to_mec5 (F->operands[0], nodename, var, pequations, 
			   p_counter, equations_trees, idcounters, iscst, data);
      if (tmp[0]->node_type != AR_TREE_TRUE && 
	  tmp[0]->node_type != AR_TREE_IDENTIFIER && 
	  tmp[0]->node_type != AR_TREE_FUNCTION_CALL)
	tmp[0] = NE(AR_TREE_PARENTHEZED_EXPR, tmp[0], ZN);
      result = NE (AR_TREE_NOT, tmp[0], ZN);
      break;

    case CTLSTAR_AND:
    case CTLSTAR_OR:
      /* G1 and G2 ----> tr(G1) and tr (G2) */
      /* G1 or G2 ----> tr(G1) or tr (G2) */
      result = s_formula_to_mec5 (F->operands[0], nodename, var, 
				  pequations, p_counter, equations_trees,
				  idcounters, iscst, data);
      if (result->node_type == AR_TREE_EXIST ||
	  (F->kind == CTLSTAR_AND && result->node_type == AR_TREE_OR))
	  result = NE(AR_TREE_PARENTHEZED_EXPR, result, ZN);

      for (i = 1; i < F->arity; i++)
	{
	  result->next = 
	    s_formula_to_mec5 (F->operands[i], nodename, var, pequations, 
			       p_counter, equations_trees, idcounters, iscst,
			       data);
	  if (result->next->node_type == AR_TREE_EXIST ||
	      (F->kind == CTLSTAR_AND && result->next->node_type == AR_TREE_OR))
	    result->next = NE(AR_TREE_PARENTHEZED_EXPR, result->next, ZN);

	  if (F->kind == CTLSTAR_AND)
	    result = NE (AR_TREE_AND, result, ZN);
	  else 
	    result = NE (AR_TREE_OR, result, ZN);
	}
      break;


    case FXP_MU:
    case FXP_NU:
      {
	/* mu X. G(X) ----> X~n(var) 
	 * where X~n(var) += tr (G(X)[X -> X~n])
	 */
	ccl_ustring R;
	altarica_tree *varid = s_crt_id (var, NULL);

	if (ccl_hash_find (equations_trees, F))
	  {
	    R = ccl_hash_get (equations_trees);
	    if (ccl_debug_is_on)
	      {
		ccl_debug ("find equation ");
		ctlstar_formula_log (CCL_LOG_DEBUG, F, NULL, 0);
		ccl_debug ("as %s\n", R);
	      }
	  }
	else
	  {
	    altarica_tree *f;
	    ar_identifier *newname;
	    ctlstar_formula *Faux;
	    int label = (F->kind == FXP_MU) ? AR_TREE_EQ_LFP : AR_TREE_EQ_GFP;
	    ccl_ustring sc = ccl_string_make_unique ("c");
	    ccl_ustring auxvar = s_generate_unique ("s~", p_counter);

	    if (ar_idtable_has (idcounters, F->varname))
	      {
		newname = s_rename_with_counter (F->varname, idcounters);
		Faux = s_rename_variable (F->operands[0], F->varname, newname);
	      }
	    else
	      {
		newname = ar_identifier_add_reference (F->varname);
		Faux = ctlstar_formula_add_reference (F->operands[0]);
		ar_idtable_put (idcounters, F->varname, (void *) (uintptr_t) 0);
	      }

	    R = s_id_to_ustring (newname);
	    F = ctlstar_formula_add_reference (F);
	    ccl_hash_insert (equations_trees, R);

	    f = s_formula_to_mec5 (Faux, nodename, auxvar, pequations, 
				   p_counter, equations_trees, idcounters,
				   iscst, data);

	    ctlstar_formula_del_reference (Faux);

	    ccl_debug ("new equation ");
	    ar_identifier_log (CCL_LOG_DEBUG, newname);
	    ccl_debug ("\n");
	    s_crt_equation (1, newname, auxvar, nodename, sc, label, 
			    s_alternation_of (F), f, pequations);
	    ar_identifier_del_reference (newname);
	  }

	result = s_crt_bang_call (NULL, R, varid, NULL);
      }
      break;

    case FXP_EX:
      /* EX G --> <t~n> (nodename !nsemove(var,t) & tr(G))*/
      {
	ccl_ustring t = s_generate_unique ("t~", p_counter);

	tmp[0] = s_formula_to_mec5 (F->operands[0], nodename, t,  
				    pequations, p_counter, equations_trees,
				    idcounters, iscst, data);
	result = s_crt_ex (nodename, var, t, tmp[0], p_counter);
      }
      break;

    case CTLSTAR_E:
    case CTLSTAR_X:
    case CTLSTAR_NOT_U:
    case CTLSTAR_U:
    default:
      result = NULL;
      break;
    }

  ccl_post (result != NULL);

  return result;
}

			/* --------------- */

static int
s_add_equation (int depth, ccl_ustring nodename,
		ar_identifier *varname,
		ctlstar_formula *F, altarica_tree **pequations,
		int *p_counter, ccl_hash *equations_trees,
		ar_idtable *idcounters, int (*iscst)(ar_identifier *id,
						     void *data),
		void *data)
{
  int result;
  altarica_tree *f;
  ccl_ustring var = s_generate_unique ("v~", p_counter);
  ccl_ustring sc = ccl_string_make_unique ("c");
  int local = depth < 0;

  if (F->kind == CTLSTAR_E)
    {
      ctlstar_formula *tmp = s_ctlstar_to_fixpoint (F, p_counter);

      if (ccl_debug_is_on)
	{
	  ctlstar_formula_log (CCL_LOG_DEBUG, F, NULL, 0);
	  ccl_debug (" -> ");
	  ctlstar_formula_log (CCL_LOG_DEBUG, tmp, NULL, 0);
	  ccl_debug ("\n");
	}
      F = tmp;
    }
  else 
    F = ctlstar_formula_add_reference (F);

  f = s_formula_to_mec5 (F, nodename, var, pequations,
			 p_counter,equations_trees, idcounters, iscst, data);
  if (local)
    depth = s_alternation_of(F);
  s_crt_equation (local, varname, var, nodename, sc, AR_TREE_EQ_LFP, 
		  depth + 1, f, pequations);
  result = depth + 1;  
  {
    ctlstar_formula *V = ctlstar_formula_crt_variable (varname);
    V->alternation = result;
    ctlstar_formula_del_reference (V);
  }

  ctlstar_formula_del_reference (F);

  return result;
}

altarica_tree *
ctlstar_compute_equation_system (ctlstar_formula *F, 
				 int (*is_constant_set)(ar_identifier *id, 
							void *data),
				 void *data,
				 ar_identifier *nodeid,
				 ar_identifier *Rname)
{
  int max_depth = 0;
  int local_variables = 0;
  altarica_tree *equations = NULL;
  ccl_list *defs = ccl_list_create ();
  ctlstar_formula *nF = s_expand_formula (F, defs, &local_variables);
  ccl_ustring nodename = s_id_to_ustring (nodeid);
  ccl_hash *equations_trees =
    ccl_hash_create (NULL, NULL,
		     (ccl_delete_proc *)ctlstar_formula_del_reference,
		     NULL);
  ar_idtable *idcounters = ar_idtable_create (0, NULL, NULL);

  Rname = ar_identifier_add_reference (Rname);
  ccl_list_add (defs, Rname);
  ccl_list_add (defs, nF);

  while (! ccl_list_is_empty (defs))
    {
      ar_identifier *varname = ccl_list_take_first (defs);
      ctlstar_formula *sF = ccl_list_take_first (defs);

      if (ccl_list_is_empty (defs))
	s_add_equation (max_depth, nodename, varname, sF, 
			&equations, &local_variables, equations_trees, 
			idcounters, is_constant_set, data);
      else
	{
	  int depth = 
	    s_add_equation (-1, nodename, varname, sF, &equations, 
			    &local_variables, equations_trees, 
			    idcounters, is_constant_set, data);
	  if (depth > max_depth)
	    max_depth = depth;	  
	}
      ctlstar_formula_del_reference (sF);
      ar_identifier_del_reference (varname);
    }

  ccl_list_delete (defs);
  ccl_hash_delete (equations_trees);
  ar_idtable_del_reference (idcounters);

  return  NE (AR_TREE_EQUATIONS_SYSTEM, REVERSE (equations), ZN);
}


			/* --------------- */

static int
s_is_subformula_of (ctlstar_formula *subF, ctlstar_formula *F)
{
  int result;

  if (subF->height > F->height)
    result = 0;
  else if (F == subF)
    result = 1;
  else if (subF->height == F->height && 
	   ((F->kind == CTLSTAR_U && subF->kind == CTLSTAR_NOT_U) ||
	    (F->kind == CTLSTAR_NOT_U && subF->kind == CTLSTAR_U)))
    {
      result = ((s_ctlstar_compare_addr (F->operands[0], 
					 subF->operands[0]) == 0) &&
		(s_ctlstar_compare_addr (F->operands[1], 
					 subF->operands[1]) == 0));
    }
  else 
    {
      int i;

      result = 0;
      
      for (i = 0; i < F->arity && !result; i++)
	result = s_is_subformula_of (subF, F->operands[i]);
    }

  return result;
}
