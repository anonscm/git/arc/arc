/*
 * ar-model.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_MODEL_H
# define AR_MODEL_H

# include <ccl/ccl-iterator.h>
# include "parser/altarica-tree.h"

# include "ar-identifier.h"
# include "ar-constant.h"
# include "ar-type.h"
# include "ar-expr.h"
# include "ar-node.h"

			/* --------------- */

extern ar_context *AR_MODEL_CONTEXT;

extern void
ar_model_init(void);
extern void
ar_model_terminate(void);

			/* --------------- */

extern void
ar_model_add_parameter(ar_identifier *id, ar_type *type, ar_expr *value);
extern int
ar_model_has_parameter(ar_identifier *id);
extern ar_context_slot *
ar_model_get_parameter(ar_identifier *id);
extern ar_expr *
ar_model_get_parameter_value(ar_identifier *id);
extern void
ar_model_set_parameter_value(ar_identifier *id, ar_expr *value);
extern ccl_list *
ar_model_get_parameters(void);
extern void
ar_model_remove_parameter(ar_identifier *id);

			/* --------------- */

extern void
ar_model_set_type(ar_identifier *id, ar_type *type);
extern ar_type *
ar_model_get_type(ar_identifier *id);
extern int
ar_model_has_type(ar_identifier *id);
extern ccl_list *
ar_model_get_types(void);
extern void
ar_model_remove_type(ar_identifier *id);

			/* --------------- */

extern void
ar_model_set_node(ar_identifier *id, ar_node *node);
extern ar_node *
ar_model_get_node(ar_identifier *id);
extern int
ar_model_has_node(ar_identifier *id);
extern ccl_list *
ar_model_get_nodes(void);
extern void
ar_model_remove_node(ar_identifier *id);

			/* --------------- */

extern void
ar_model_set_signature(ar_identifier *id, ar_signature *sig);
extern ar_signature *
ar_model_get_signature(ar_identifier *id);
extern int
ar_model_has_signature(ar_identifier *id);
extern ccl_list *
ar_model_get_signatures(void);
extern void
ar_model_remove_signature(ar_identifier *id);

			/* --------------- */

extern void
ar_model_set_law_parameter (ar_identifier *id, sas_law_param *param);
extern sas_law_param *
ar_model_get_law_parameter (ar_identifier *id);
extern int
ar_model_has_law_parameter (ar_identifier *id);
extern ccl_list *
ar_model_get_law_parameters (void);
extern void
ar_model_remove_law_parameter (ar_identifier *id);

#endif /* ! AR_MODEL_H */
