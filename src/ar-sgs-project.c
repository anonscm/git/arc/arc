/*
 * ar-sgs-project.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-acheck-builtins.h"
#include "ar-state-graph-semantics.h"
#include "ar-ca-semantics.h"

#define FIELD_TAB " "

			/* --------------- */

static void
s_display_header(ccl_log_type log, ar_node *N, ar_node *sub,
		 ar_identifier *pname);

static void
s_display_variables(ccl_log_type log, ar_node *sub);

static void
s_display_events(ccl_log_type log, ar_node *sub);

static void
s_display_subnodes(ccl_log_type log, ar_node *sub);

static void
s_display_assertions(ccl_log_type log, ar_node *sub);

static void
s_display_broadcasts(ccl_log_type log, ar_node *sub);

static void
s_display_init(ccl_log_type log, ar_node *sub);

static void
s_display_expr (ccl_log_type log, const ar_expr *e)
{
  ar_expr_log_gen (log, e, ar_identifier_log);
}

			/* --------------- */

static void
s_display_trans (ccl_log_type log, ar_expr *G, ar_trans *t)
{
  ar_event *e = ar_trans_get_event(t);
  const ar_trans_assignment *a = ar_trans_get_assignments(t);

  ccl_log(log,FIELD_TAB FIELD_TAB);
  s_display_expr (log, G);

  ccl_log(log," |- ");
  ar_event_log(log,e);
  ccl_log(log," -> ");
  ar_event_del_reference(e);

  if( a != NULL && a->next != NULL)
    ccl_log(log,"\n");

  for(; a; a = a->next)
    {
      ar_expr_log (log,a->lvalue);
      ccl_log(log," := ");
      s_display_expr(log,a->value);
      if( a->next != NULL )
	ccl_log(log,", ");
    }
  ccl_log(log,";\n");
}

			/* --------------- */

static ar_expr *
s_project_guard (ar_identifier *subid, ar_sgs *sgs, ar_sgs_set *T, ar_event *e,
		 ccl_list *pvars, ccl_list *vars, ar_ca_exprman *man, int exist)
{
  ar_sgs_set *aux;
  ar_identifier *label;
  ar_expr *result = NULL;
  ar_identifier *eid = ar_event_get_name (e);
  ar_sgs_set *e_labelled_trans;
  
  if (subid == NULL) 
    label = ar_identifier_add_reference (eid);
  else 
    label = ar_identifier_add_prefix (eid, subid);
  
  aux = ar_sgs_compute_label (sgs, label);
  ar_identifier_del_reference(label);
  if (exist)
    e_labelled_trans = ar_sgs_compute_intersection (sgs, aux, T);
  else
    {
      ar_identifier *id = ar_identifier_create (ACHECK_BUILTIN_ANY_T_STR);
      ar_sgs_set *any_t = ar_sgs_get_trans_set (sgs, id);
      ar_sgs_set *tmp = ar_sgs_compute_difference (sgs, any_t, T);
      e_labelled_trans = ar_sgs_compute_intersection (sgs, aux, tmp);
      ar_sgs_set_del_reference (any_t);
      ar_sgs_set_del_reference (tmp);
      ar_identifier_del_reference (id);
    }


  if (ar_sgs_set_is_empty (sgs, e_labelled_trans))
    result = exist ? ar_expr_crt_false () : ar_expr_crt_true ();
  else if (ccl_list_is_empty (vars))
    result = exist ? ar_expr_crt_true () : ar_expr_crt_false ();
  else
    {
      ar_sgs_set *src = ar_sgs_compute_src (sgs, e_labelled_trans);
      if (! exist)
	{
	  ar_identifier *id = ar_identifier_create (ACHECK_BUILTIN_ANY_S_STR);
	  ar_sgs_set *tmp1 = ar_sgs_get_state_set (sgs, id);
	  ar_sgs_set *tmp2 = ar_sgs_compute_src (sgs, aux);
	  ar_sgs_set *tmp3 = ar_sgs_compute_intersection (sgs, tmp1, tmp2);
	  ar_sgs_set_del_reference (tmp1);
	  ar_sgs_set_del_reference (tmp2);
	  tmp1 = ar_sgs_compute_difference (sgs, tmp3, src);
	  ar_sgs_set_del_reference (src);
	  ar_sgs_set_del_reference (tmp3);
	  ar_identifier_del_reference (id);
	  src = tmp1;
	}
      result = ar_sgs_set_states_to_formula (sgs, src, pvars, vars, 1);
      ar_sgs_set_del_reference (src);
    }
      
  ar_sgs_set_del_reference (e_labelled_trans);
  ar_identifier_del_reference (eid);
  ar_sgs_set_del_reference (aux);
  ccl_post (result != NULL);

  return result;
}

			/* --------------- */

static int
s_get_identifier_depth(ar_node *node, ar_identifier *id)
{
  int result = 1;

  id = ar_identifier_add_reference(id);
  node = ar_node_add_reference(node);

  while( ! ar_identifier_is_simple(id) ) 
    {
      ar_identifier *remain = NULL;
      ar_identifier *sub = ar_identifier_get_first(id,&remain);
      ar_node *subnode = ar_node_get_subnode_model(node,sub);
      ar_identifier_del_reference(id);
      ar_identifier_del_reference(sub);

      id = remain;
      if( subnode == NULL )
	break;
      else
	{
	  ar_node_del_reference(node);
	  node = subnode;
	}
      result++;
    }

  ar_identifier_del_reference(id);
  ar_node_del_reference(node);

  return result;
}

/**
 * Build two lists of variables:
 * - 'vars' : contains 'ar_expr' variables created from scalar slots of 'node' 
 *            (remember that each compound slot has been expanded).
 * - 'result' : constains 'ar_ca_expr' variables created from 'vars' but with
 *              subid prefix added to each identifier. This permits to retrieve
 *              actual variables from the CA.
 */
static ccl_list *
s_projected_variable_set (ar_identifier *subid, ar_node *node, ccl_list *vars,
			  ar_ca_exprman *man)
{  
  ccl_pair *p;
  ccl_list *result = ccl_list_create();
  ar_context *ctx = ar_node_get_context(node);
  ccl_list *slots = ar_context_get_slots(ctx,
					 AR_SLOT_FLAG_STATE_VAR|
					 AR_SLOT_FLAG_FLOW_VAR|
					 AR_SLOT_FLAG_PARAMETER);

  while( ! ccl_list_is_empty(slots) )
    {
      ar_identifier *slid = (ar_identifier *)ccl_list_take_first(slots);
      ar_context_slot *sl = ar_context_get_slot(ctx, slid);
      ar_type *type = ar_context_slot_get_type(sl);      
      ar_identifier_del_reference (slid);

      if( ar_type_is_scalar(type) && ! ar_context_slot_has_value(sl) )
	{
	  int addit;
	  ar_identifier *id = ar_context_slot_get_name(sl);
	  uint32_t flags = ar_context_slot_get_flags(sl);
	  int depth = s_get_identifier_depth(node,id);

	  if( depth == 1 )
	    addit = 1;
	  else if( depth == 2 ) 
	    addit = (flags&(AR_SLOT_FLAG_PARENT|AR_SLOT_FLAG_PUBLIC));
	  else
	    addit = flags&AR_SLOT_FLAG_PUBLIC;

	  if( addit )
	    {
	      ar_expr *v;

	      ccl_list_add(result,id);
	      if( (flags & AR_SLOT_FLAG_PARAMETER) )
		v = ar_expr_crt_parameter(sl);
	      else
		v = ar_expr_crt_variable(sl);
	      ccl_list_add(vars,v);
	    }
	  else
	    ar_identifier_del_reference(id);
	}
      
      ar_type_del_reference(type);
      ar_context_slot_del_reference(sl);
    }
  ccl_list_delete(slots);

  ar_context_del_reference(ctx);

  if( subid != NULL )
    {
      for(p = FIRST(result); p; p = CDR(p))
	{
	  ar_identifier *id = (ar_identifier *)CAR(p);
	  CAR(p) = ar_identifier_add_prefix(id,subid);
	  ar_identifier_del_reference(id);
	}
    }

  for(p = FIRST (result); p; p = CDR (p))
    {
      ar_identifier *id = (ar_identifier *) CAR(p);
      ar_ca_expr *v = ar_ca_expr_get_variable_by_name (man, id);

      ccl_assert (v != NULL);

      CAR (p) = v;
      ar_identifier_del_reference(id);
    }

  return result;
}

static ar_expr *
s_instantiate_expr (ar_expr *e, ar_identifier *prefix,
		    ar_context *from, ar_context *to)
{
  ar_expr *result;
  ccl_list *slots = ar_expr_get_slots (e, from, NULL);
  ccl_hash *sl2expr =
    ccl_hash_create(NULL,NULL,
		    (ccl_delete_proc *)ar_context_slot_del_reference,
		    (ccl_delete_proc *)ar_expr_del_reference);

  while (! ccl_list_is_empty (slots))
    {
      ar_context_slot *sl = ccl_list_take_first (slots);
      
      ar_context_slot *nsl;

      if (prefix == NULL)
	nsl = ar_context_slot_add_reference (sl);
      else
	{
	  ar_identifier *sln = ar_context_slot_get_name (sl);
	  ar_identifier *nid = ar_identifier_add_prefix (sln, prefix);
	  nsl = ar_context_get_slot (to, nid);
	  ar_identifier_del_reference (nid);
	  ar_identifier_del_reference (sln);
	}
      
      ccl_assert (nsl != NULL);
      
      ccl_hash_find (sl2expr, sl);
      ccl_hash_insert (sl2expr, ar_expr_crt_variable (nsl));

      ar_context_slot_del_reference (nsl);
    }
  ccl_list_delete (slots);
  
  result = ar_expr_replace_with_tables (e, from, &sl2expr, 1, NULL);
  ccl_hash_delete (sl2expr);
  
  return result;
}

static ar_sgs_set *
s_filter_assignments (ar_sgs *sgs, ar_trans *t, ar_identifier *subid,
		      ar_node *sub, ar_sgs_set *T, ar_ca_exprman *man,
		      ccl_list *vars, ccl_list *pvars)
{
  ccl_pair *p;
  ccl_pair *pp;
  ar_node *top;
  ar_context *ctx;
  ar_context *lctx;
  ccl_list *couples = ccl_list_create ();
  int nb_assignments = 0;
  ar_ca_expr **assignments = NULL;
  ar_sgs_set *filter;
  ar_sgs_set *result;
  const ar_trans_assignment *a = ar_trans_get_assignments (t);
  ccl_hash *assigned = ccl_hash_create (NULL, NULL, NULL, NULL);
    
  if (a != NULL)
    {
      top = ar_sgs_get_node (sgs);
      ctx = ar_node_get_context (top);
      lctx = ar_node_get_context (sub);      
      
      for (; a; a = a->next)
	{
	  ccl_list *lvalues =
	    ar_expr_expand_composite_types (a->lvalue, lctx, NULL);
	  ccl_pair *ps = FIRST (lvalues);
	  ccl_list *values = 
	    ar_expr_expand_composite_types (a->value, lctx, NULL);
	  ccl_pair *pa = FIRST(values);
      
	  ccl_assert (ccl_list_get_size (lvalues) == ccl_list_get_size (values));
	  for (; ps; ps = CDR (ps), pa = CDR (pa))
	    {
	      ar_expr *v = s_instantiate_expr (CAR (ps), subid, lctx, ctx);
	      ar_expr *val = s_instantiate_expr (CAR (pa), subid, lctx, ctx);
	      ar_ca_expr *cv = ar_model_expr_to_ca_expr (v, man, NULL);
	      ar_ca_expr *cval = ar_model_expr_to_ca_expr (val, man, NULL);

	      ccl_assert (! ccl_hash_find (assigned, cv));
	      ccl_hash_find (assigned, cv);
	      ccl_hash_insert (assigned, cv);
	      ccl_list_add (couples, cv);
	      ccl_list_add (couples, cval);

	      ar_expr_del_reference (v);
	      ar_expr_del_reference (val);
	    }
	  ccl_list_clear_and_delete (lvalues, (ccl_delete_proc *) 
				     ar_expr_del_reference);
	  ccl_list_clear_and_delete (values, (ccl_delete_proc *) 
				     ar_expr_del_reference);
	}
      ar_node_del_reference (top);
      ar_context_del_reference (lctx);
      ar_context_del_reference (ctx);
  
    }

  for (pp = FIRST (vars), p = FIRST (pvars); p; p = CDR (p), pp = CDR (pp))
    {      
      if (!ccl_hash_find (assigned, CAR (p)))
	{
	  ar_context_slot *sl = ar_expr_get_slot (CAR (pp));
	  ar_identifier *id = ar_context_slot_get_name (sl);
	  if ((ar_context_slot_get_flags (sl) & AR_SLOT_FLAG_STATE_VAR) &&
	      s_get_identifier_depth (sub, id) == 1)
	    {
	      ccl_list_add (couples, ar_ca_expr_add_reference (CAR (p)));
	      ccl_list_add (couples, ar_ca_expr_add_reference (CAR (p)));
	    }
	  ar_identifier_del_reference (id);
	  ar_context_slot_del_reference (sl);
	}
    }
  assignments = (ar_ca_expr **) ccl_list_to_array (couples, &nb_assignments);
  nb_assignments >>= 1;
  ccl_hash_delete (assigned);
  
  filter = ar_sgs_formula_compute_assignment (sgs, assignments, nb_assignments);
  result = ar_sgs_compute_intersection (sgs, filter, T);
  ar_sgs_set_del_reference (filter);
  ccl_list_clear_and_delete (couples, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);
  ccl_delete (assignments);
  
  return result;
}

static void
s_display_transitions (ccl_log_type log, ar_sgs *sgs, ar_identifier *subid, 
		       ar_node *sub, ar_sgs_set *T, int simplify,
		       ar_ca_exprman *man, int exist)
{
  ccl_pair *p;
  ccl_list *vars;
  ccl_list *pvars;
  ccl_list *tlist;
  ccl_list *transitions = ar_node_get_transitions (sub);
  
  if (ccl_list_is_empty (transitions) ||
      (ccl_list_get_size (transitions) == 1 &&
       ar_trans_has_event (CAR (FIRST (transitions)), AR_EPSILON)))
    return;

  if (1 || exist)
    T = ar_sgs_set_add_reference (T);
  else
    {
      ar_identifier *id = ar_identifier_create (ACHECK_BUILTIN_ANY_T_STR);
      ar_sgs_set *any_t = ar_sgs_get_trans_set (sgs, id);
      T = ar_sgs_compute_difference (sgs, any_t, T);
      ar_identifier_del_reference (id);
      ar_sgs_set_del_reference (any_t);
    }

  vars = ccl_list_create ();
  pvars = s_projected_variable_set (subid, sub, vars, man);

  tlist = ccl_list_create ();
  
  for(p = FIRST (transitions); p; p = CDR (p))
    {
      ar_event *e;
      ar_expr *G;
      ar_trans *t = (ar_trans *) CAR (p);
      
      if (ar_trans_has_event (t, AR_EPSILON))
	continue;

      G = ar_trans_get_guard (t);
      e = ar_trans_get_event (t);

      {
#if 1
	ar_sgs_set *Taux =
	  s_filter_assignments (sgs, t, subid, sub, T, man, vars, pvars);
#else
	ar_sgs_set *Taux = ar_sgs_set_add_reference (T);
#endif
	ar_expr *tmp =
	  s_project_guard (subid, sgs, Taux, e, pvars, vars, man, exist);
	ar_expr *newG = ar_expr_crt_binary (AR_EXPR_AND, tmp, G);
	ar_expr_del_reference (tmp);
	ar_expr_del_reference (G);

	if (simplify) 
	  {
	    G = ar_expr_simplify (newG);
	    ar_expr_del_reference (newG);
	  }
	else
	  {
	    G = newG;
	  }
	ar_sgs_set_del_reference (Taux);
      }

      if (! ar_expr_is_false (G))
	{
	  ccl_list_add (tlist, ar_expr_add_reference (G));
	  ccl_list_add (tlist, t);
	}

      ar_expr_del_reference (G);
      ar_event_del_reference (e);
    }

  if (ccl_list_is_empty (tlist))
    ccl_log (log, FIELD_TAB "/* no transition */\n");
  else
    {
      ccl_log (log, FIELD_TAB "trans\n");
      while (!ccl_list_is_empty (tlist))
	{
	  ar_expr *G = ccl_list_take_first (tlist);
	  ar_trans *t = ccl_list_take_first (tlist);
	  s_display_trans (log, G, t);
	  ar_expr_del_reference (G);
	}
    }
  ccl_list_delete (tlist);
  
  ar_sgs_set_del_reference (T);
  ccl_list_clear_and_delete (pvars, 
			     (ccl_delete_proc *) ar_ca_expr_del_reference);
  ccl_list_clear_and_delete (vars, (ccl_delete_proc *) ar_expr_del_reference);
}

			/* --------------- */

static void
s_filter_transitions (ar_sgs *sgs, ar_sgs_set **pT, ar_sgs_set *S)
{
  ar_sgs_set *tmp1 = ar_sgs_compute_rtgt (sgs, S);
  ar_sgs_set *tmp2 = ar_sgs_compute_rsrc (sgs, S);
  ar_sgs_set *tmp3 = ar_sgs_compute_intersection (sgs, tmp1, tmp2);
  ar_sgs_set_del_reference (tmp1);
  ar_sgs_set_del_reference (tmp2);

  tmp1 = ar_sgs_compute_intersection (sgs, tmp3, *pT);
  ar_sgs_set_del_reference (tmp3);
  *pT = tmp1;
}

void
ar_sgs_compute_project (ar_sgs *sgs, ccl_log_type log, ar_identifier *subid,
			ar_node *sub, ar_identifier *pname, ar_sgs_set *S, 
			ar_sgs_set *TE, ar_sgs_set *TA, int simplify)
{
  ar_node *N;
  ar_ca_exprman *man;

  s_filter_transitions (sgs, &TE, S);
  if (TA != NULL)
    {
      ar_sgs_set *tmp;

      s_filter_transitions (sgs, &TA, S);
      tmp = ar_sgs_compute_intersection (sgs, TE, TA);
      if (! ar_sgs_is_set_empty (sgs, tmp))
	ccl_warning ("warning: universal and existential transitions sets "
		     "have non-empty intersection.\n");    
      ar_sgs_set_del_reference (tmp);      
    }

  N = ar_sgs_get_node (sgs);

  ccl_pre (N != NULL);

  man = ar_sgs_get_expr_manager (sgs);

 
  if (sub == NULL)
    {
      ar_identifier *Nname = ar_node_get_name (N);
      sub = ar_model_get_node (Nname);
      ccl_assert (sub != NULL);
      ar_identifier_del_reference (Nname);
    }
  else
    {
      sub = ar_node_add_reference (sub);
    }

  s_display_header (log, N, sub, pname);
  s_display_variables (log, sub);
  s_display_events (log, sub);
  s_display_subnodes (log, sub);
  s_display_assertions (log, sub);
  s_display_broadcasts (log, sub);
  s_display_init (log, sub);
  
  ccl_log (log, "/* Existential transitions */\n");
  if (! ar_sgs_set_is_empty (sgs, TE))
    s_display_transitions (log, sgs, subid, sub, TE, simplify, man, 1);
  ar_sgs_set_del_reference (TE);

  ccl_log (log, "/* Universal transitions */\n");
  if (TA != NULL)
    {
      s_display_transitions (log, sgs, subid, sub, TA, simplify, man, 0);
      ar_sgs_set_del_reference (TA);
    }
  else
    ccl_log (log, "/* no transition */\n");

  ccl_log (log, "edon\n");
  ar_node_del_reference (N);
  ar_node_del_reference (sub);
  ar_ca_exprman_del_reference (man);
}

			/* --------------- */

static void
s_display_header(ccl_log_type log, ar_node *N, ar_node *sub,
		 ar_identifier *pname)
{
  ar_identifier *Nname = ar_node_get_name(N);

  ccl_log(log,"/*\n * This node is the result of the projection of the node '");
  ar_identifier_log(log,Nname);
  ccl_log(log,"'\n");
      
  if( sub == N )
    ccl_log(log," * on its own variables.\n");
  else
    {
      ar_identifier *subName = ar_node_get_name(sub);
      ccl_log(log," * on its subnode '");
      ar_identifier_log(log,subName);
      ccl_log(log,"'.\n");
      ar_identifier_del_reference(subName);
    }
  ccl_log(log," */\nnode ");
  ar_identifier_log(log,pname);
  ccl_log(log,"\n");
  ar_identifier_del_reference(Nname);
}

			/* --------------- */

static void
s_display_prop_list (ccl_log_type log, const ccl_list *l)
{
  if( l == NULL )
    return;

  if( ! ccl_list_is_empty(l) )
    {
      ccl_pair *p;

      ccl_log(log," : ");

      for(p = FIRST(l); p; p = CDR(p))
	{
	  ar_identifier *id = (ar_identifier *)CAR(p);
	  ar_identifier_log(log,id);
	  if( CDR(p) != NULL )
	    ccl_log(log,", ");
	}
    }
}

			/* --------------- */

static void
s_display_variables (ccl_log_type log, ar_node *sub)
{
  int i;  
  char *kind[2] = { "flow", "state" };
  ccl_list *vars[2];
  ar_context_slot_iterator *si = ar_node_get_slots_for_variables (sub);

  vars[0] = ccl_list_create ();
  vars[1] = ccl_list_create ();

  while (ccl_iterator_has_more_elements (si))
    {
      ar_context_slot *sl = ccl_iterator_next_element (si);
      if ((ar_context_slot_get_flags(sl) & AR_SLOT_FLAG_FLOW_VAR))
	ccl_list_add (vars[0], sl);
      else 
	ccl_list_add (vars[1], sl);
    }
  ccl_iterator_delete (si);

  if (ccl_list_is_empty (vars[0]) && ccl_list_get_size (vars[1]) == 1)
    {
      ar_context_slot *sl = (ar_context_slot *) CAR (FIRST (vars[1]));
      ar_identifier *id = ar_context_slot_get_name (sl);
      if (id == AR_IDLE_VAR)
	{
	  ar_context_slot_del_reference (sl);
	  ccl_list_clear (vars[1], NULL);
	}
      ar_identifier_del_reference (id);
    }

  for (i = 0; i < 2; i++)
    {
      if (! ccl_list_is_empty (vars[i]))
	{
	  ccl_log (log, FIELD_TAB "%s\n", kind[i]);
	  while (! ccl_list_is_empty (vars[i]))
	    {
	      ar_context_slot *sl = (ar_context_slot *)
		ccl_list_take_first (vars[i]);
	      ar_type *type = ar_context_slot_get_type (sl);
	      ar_identifier *id = ar_context_slot_get_name (sl);
	      ccl_list *attrs = ar_context_slot_get_attributes (sl);
	      ar_context_slot_del_reference (sl);

	      ccl_log (log, FIELD_TAB FIELD_TAB);
	      ar_identifier_log (log, id);
	      ar_identifier_del_reference (id);
	      ccl_log (log, " : ");
	      ar_type_log (log, type);
	      ar_type_del_reference (type);

	      if (attrs != NULL)
		{
		  s_display_prop_list (log, attrs);
		  ccl_list_clear_and_delete (attrs, (ccl_delete_proc *)
					     ar_identifier_del_reference);
		}
	      ccl_log (log, ";\n");
	    }
	}
      ccl_list_delete (vars[i]);
    }
}

			/* --------------- */

static void
s_display_events(ccl_log_type log, ar_node *sub)
{
  ar_event_iterator *ei;
  ar_event_poset *order;

  if( ar_node_get_number_of_events(sub) <= 1 )
    return;

  ei = ar_node_get_events(sub);
    
  ccl_log(log,FIELD_TAB "event\n");
  while( ccl_iterator_has_more_elements(ei) )
    {
      ar_event *ev = ccl_iterator_next_element(ei);
      ar_identifier *evid = ar_event_get_name(ev);
	
      if( evid != AR_EPSILON_ID )
	{
	  const ccl_list *attrs = ar_event_get_attr(ev);
	  ccl_log(log,FIELD_TAB FIELD_TAB);
	  ar_identifier_log_quote_gen (log, evid, ".", "''");
	  s_display_prop_list (log, attrs);
	  ccl_log(log,";\n");
	}
      ar_identifier_del_reference(evid);
      ar_event_del_reference(ev);
    }
  ccl_iterator_delete(ei);

  order = ar_node_get_event_poset(sub);
  if( ar_event_poset_is_empty(order) )
    {
      ar_event_poset_del_reference(order);
      return;
    }

  ei = ar_event_poset_get_events(order);
  while( ccl_iterator_has_more_elements(ei) )
    {
      ar_event *ev = ccl_iterator_next_element(ei);
      ar_identifier *evid = ar_event_get_name(ev);

      if( evid != AR_EPSILON_ID )
	{
	  ccl_list *gt = ar_event_poset_get_greater_events(order,ev);
	  
	  if( ! ccl_list_is_empty(gt) )
	    {
	      int need_braces = (ccl_list_get_size(gt) > 1);

	      ar_identifier_log_quote_gen (log, evid, ".", "''");
	      ccl_log (log, FIELD_TAB FIELD_TAB "%s", " < ");
	      if (need_braces)
		ccl_log (log, "{ ");
	      
	      while( ! ccl_list_is_empty(gt) )
		{
		  ar_event *other = (ar_event *)ccl_list_take_first(gt);
		  ar_identifier *other_id = ar_event_get_name(other);
		  ar_identifier_log_quote_gen (log, other_id, ".", "''");
		  if( ! ccl_list_is_empty(gt) )
		    ccl_log(log,", ");
		  ar_identifier_del_reference(other_id);
		}
	      if (need_braces)
		ccl_log (log, " }");
	      
	      ccl_log (log, ";\n");
	    }
	  ccl_list_delete(gt);
	}
      ar_event_del_reference(ev);
      ar_identifier_del_reference(evid);
    }
  ccl_iterator_delete(ei);
  ar_event_poset_del_reference(order);
}

			/* --------------- */
#if 0
static void
s_display_subnode_type (ccl_log_type log, ar_type *subtype)
{
  if (ar_type_get_kind (subtype) == AR_TYPE_BANG)
    {
      ar_node *subnodetype = ar_type_bang_get_node (subtype);
      ar_identifier *subnodetypename = ar_node_get_name (subnodetype);
      ar_identifier_log (log, subnodetypename);
      ar_identifier_del_reference (subnodetypename);
      ar_node_del_reference (subnodetype);
    }
  else
    {
      ar_type *t;

      ccl_assert (ar_type_get_kind (subtype) == AR_TYPE_ARRAY);
      t = ar_type_array_get_base_type (subtype);
      s_display_subnode_type (log, t);
      ccl_log (log, "[%d]", ar_type_get_width (subtype));
      ar_type_del_reference (t);
    }
}
#endif
			/* --------------- */

static void
s_display_subnodes (ccl_log_type log, ar_node *sub)
{
  ar_context_slot_iterator *si; 
    
  if (ar_node_is_leaf (sub))
    return;

  si = ar_node_get_slots_for_subnode_fields (sub);

  ccl_log (log, FIELD_TAB "sub\n");

  while (ccl_iterator_has_more_elements (si))
    {
      ar_context_slot *sl = ccl_iterator_next_element (si);
      ar_type *type = ar_context_slot_get_type (sl);
      ar_identifier *fname = ar_context_slot_get_name (sl);
	
      ccl_log (log, FIELD_TAB FIELD_TAB);
      ar_identifier_log_global_quote (log, fname);
      ccl_log (log, " : ");
      ar_node_display_subnode_type (log, type);
      ccl_log (log, ";\n");

      ar_type_del_reference (type);
      ar_identifier_del_reference (fname);
      ar_context_slot_del_reference (sl);
    }
  ccl_iterator_delete(si);
}

			/* --------------- */

static void
s_display_assertions(ccl_log_type log, ar_node *sub)
{
  ccl_pair *p;
  ccl_list *assertions = ar_node_get_assertions(sub);

  if( ccl_list_is_empty(assertions) )
    return;

  ccl_log(log,FIELD_TAB "assert\n");
  for(p = FIRST(assertions); p; p = CDR(p))
    {
      ccl_log(log,FIELD_TAB FIELD_TAB);
      s_display_expr(log,(ar_expr *)CAR(p));
      ccl_log(log,";\n");
    }
}

			/* --------------- */

static void
s_display_broadcasts(ccl_log_type log, ar_node *sub)
{
  ccl_pair *p;
  ccl_list *bv = ar_node_get_broadcasts (sub);

  if (ccl_list_is_empty (bv))
    return;

  ccl_log (log, FIELD_TAB "sync\n");
  for (p = FIRST (bv); p; p = CDR (p))
    {
      ar_broadcast *bv = (ar_broadcast *) CAR (p);

      if (ar_broadcast_get_size (bv) == 0) 
	continue;

      ccl_log (log, FIELD_TAB FIELD_TAB);
      ar_broadcast_log (log, bv);
      ccl_log (log, ";\n");
    }
}

			/* --------------- */

static void
s_display_init(ccl_log_type log, ar_node *sub)
{
  ccl_list *init = ar_node_get_initialized_slots (sub);
  ccl_list *init_c = ar_node_get_initial_constraints (sub);

  if (ccl_list_is_empty (init) && ccl_list_is_empty (init_c))
    return;

  ccl_log (log, FIELD_TAB "init\n");
  
  if (! ccl_list_is_empty (init))
    {
      ccl_pair *p;

      for (p = FIRST (init); p; p = CDR (p))
	{
	  ar_context_slot *sl = (ar_context_slot *) CAR (p);
	  ar_identifier *name = ar_context_slot_get_name (sl);
	  const ar_expr *value = ar_node_get_initial_value_cst (sub, sl);
	  
	  ccl_log (log, FIELD_TAB FIELD_TAB);
	  ar_identifier_log (log, name);
	  ccl_log (log, " := ");
	  s_display_expr (log, value);
	  if (CDR (p) != NULL)
	    ccl_log (log, ", ");
	  ar_identifier_del_reference (name);
	}
      if (! ccl_list_is_empty (init_c))
	ccl_log (log, ", ");
    }

  if (! ccl_list_is_empty (init_c))
    {
      ccl_pair *p = FIRST (init_c);

      ccl_log (log, FIELD_TAB FIELD_TAB);
      s_display_expr (log, CAR (p));
      
      for (p = CDR (p); p; p = CDR (p))
	{
	  ccl_log (log, ", ");
	  s_display_expr (log, CAR (p));
	}
    }

  ccl_log(log,";\n");
}

			/* --------------- */
