/*
 * ar-ca-domain.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-ca-expression.h"
#include "ar-ca-domain.h"

# define BOOL  0x1
# define RANGE 0x2
# define ENUM  0x4
# define TYPE_MASK 0x7
# define TYPE_BIT_SIZE 3

struct ar_ca_domain_st {
  int refcount;
  int size_and_type;
  int values[1];
};


			/* --------------- */

static ar_ca_domain *
s_allocate_domain(int type, int nb_values);

static void
s_delete_domain(ar_ca_domain *domain);

			/* --------------- */

ar_ca_domain *
ar_ca_domain_create_range(int min, int max)
{
  ar_ca_domain *result = s_allocate_domain(RANGE,2);

  ccl_pre( min <= max );

  result->values[0] = min;
  result->values[1] = max;

  return result;
}

			/* --------------- */

static int
s_cmp_int(const void *pi1, const void *pi2)
{
  const int *i1 = pi1;
  const int *i2 = pi2;

  return *i1 - *i2;
}

			/* --------------- */

ar_ca_domain *
ar_ca_domain_create_enum(const int *values, int size)
{
  int sz = size;
  ar_ca_domain *result = s_allocate_domain(ENUM,size);

  while( sz-- )
    result->values[sz] = values[sz];

  qsort(&(result->values[0]),size,sizeof(int),s_cmp_int);

  return result;
}

			/* --------------- */

ar_ca_domain *
ar_ca_domain_create_bool(void)
{
  ar_ca_domain *result = s_allocate_domain(BOOL,2);

  result->values[0] = 0;
  result->values[1] = 1;

  return result;
}

			/* --------------- */

ar_ca_domain *
ar_ca_domain_add_reference(ar_ca_domain *domain)
{
  ccl_pre( domain != NULL );

  domain->refcount++;

  return domain;
}

			/* --------------- */

void
ar_ca_domain_del_reference(ar_ca_domain *domain)
{
  ccl_pre( domain != NULL );  ccl_pre( domain->refcount > 0 );
  
  if( --domain->refcount  == 0 )
    s_delete_domain(domain);
}

			/* --------------- */

int
ar_ca_domain_get_cardinality(const ar_ca_domain *domain)
{
  ccl_pre( domain != NULL ); 

  if( ar_ca_domain_is_integer(domain) )
    return domain->values[1]-domain->values[0]+1;
  return domain->size_and_type>>TYPE_BIT_SIZE;
}

			/* --------------- */

int
ar_ca_domain_get_ith_value (const ar_ca_domain *domain, int i)
{
  ccl_pre (domain != NULL);
  ccl_pre (0 <= i && i < ar_ca_domain_get_cardinality (domain));

  if (ar_ca_domain_is_integer(domain))
    return domain->values[0] + i;

  return domain->values[i];
}

			/* --------------- */

int
ar_ca_domain_get_value_index (const ar_ca_domain *domain, int value)
{
  int result = -1;

  ccl_pre (domain != NULL);
  
  if( (domain->size_and_type & ENUM) )
    {
      int i, sz = domain->size_and_type >> TYPE_BIT_SIZE;
     
      for (i = 0; i < sz && result < 0; i++)
	{
	  if (domain->values[i] == value)
	    result = i;
	}
    }
  else 
    {
      ccl_pre (domain->values[0] <= value && value <= domain->values[1]);

      result = value - domain->values[0];
    }

  ccl_post (0 <= result && result < ar_ca_domain_get_cardinality (domain));

  return result;
}

			/* --------------- */

void
ar_ca_domain_get_bounds(const ar_ca_domain *domain, ar_bounds *bounds)
{
  int min, max;

  ccl_pre( domain != NULL ); ccl_pre( bounds != NULL );

  min = domain->values[0];
  if( (domain->size_and_type & ENUM) )
    max = domain->values[(domain->size_and_type>>TYPE_BIT_SIZE)-1];
  else
    max = domain->values[1];
  ar_bounds_set(bounds,min,max);
}

			/* --------------- */

const int *
ar_ca_domain_get_enum_values(const ar_ca_domain *domain, int *psize)
{
  ccl_pre( domain != NULL ); ccl_pre( psize != NULL );

  *psize = domain->size_and_type >> TYPE_BIT_SIZE;

  return domain->values;
}

			/* --------------- */

int
ar_ca_domain_has_element(const ar_ca_domain *domain, int element)
{
  int result;

  ccl_pre( domain != NULL ); 

  if( (domain->size_and_type & (RANGE|BOOL)) )
    result = (domain->values[0] <= element && element <= domain->values[1]);
  else
    { 
      int sz = domain->size_and_type>>TYPE_BIT_SIZE;

      result = 0;
      if( domain->values[0] <= element && element <= domain->values[sz-1] )
	{
	  while( ! result && sz-- )
	    result = ( element == domain->values[sz] );
	}
    }

  return result;
}

			/* --------------- */

int
ar_ca_domain_is_boolean(const ar_ca_domain *domain)
{
  ccl_pre( domain != NULL );
  
  return (domain->size_and_type & BOOL);
}

			/* --------------- */

int
ar_ca_domain_is_enum(const ar_ca_domain *domain)
{
  ccl_pre( domain != NULL );

  return (domain->size_and_type & ENUM);
}

			/* --------------- */

int
ar_ca_domain_is_integer(const ar_ca_domain *domain)
{
  ccl_pre( domain != NULL );

  return (domain->size_and_type & RANGE);
}

			/* --------------- */

uint32_t
ar_ca_domain_hash(const ar_ca_domain *domain)
{
  int sz;
  uint32_t result;

  ccl_pre( domain != NULL );

  result = domain->size_and_type;
  sz = domain->size_and_type>>TYPE_BIT_SIZE;
  while( sz-- )
    result = 13*result+domain->values[sz];

  return result;
}

			/* --------------- */

int
ar_ca_domain_equals(const ar_ca_domain *domain, const ar_ca_domain *other)
{
  int sz;

  ccl_pre( domain != NULL ); ccl_pre( other != NULL );

  if( (sz=domain->size_and_type) != other->size_and_type )
    return 0;

  sz >>= TYPE_BIT_SIZE;

  while( sz-- )
    {
      if( domain->values[sz] != other->values[sz] )
	return 0;
    }

  return 1;
}

			/* --------------- */

void
ar_ca_domain_log (ccl_log_type log, const ar_ca_domain *domain,
		  struct ar_ca_expression_manager_st *man)
{
  ar_ca_domain_log_gen (log, domain, man, ".", NULL);
}

			/* --------------- */

void
ar_ca_domain_log_gen (ccl_log_type log, const ar_ca_domain *domain,
		      ar_ca_exprman *man, const char *separator, 
		      const char *quotes)
{
  ccl_pre (domain != NULL); 
  ccl_pre (man != NULL);

  if ((domain->size_and_type & BOOL))
    ccl_log (log, "bool");
  else if ((domain->size_and_type & RANGE))
    ccl_log (log, "[%d, %d]", domain->values[0], domain->values[1]);
  else
    {
      int i, sz = domain->size_and_type >> TYPE_BIT_SIZE;

      ccl_log (log, "{ ");
      for (i = 0; i < sz; i++)
	{
	  ar_ca_expr *enumval = 
	    ar_ca_expr_get_enum_constant_by_index (man, domain->values[i]);
	  
	  ar_ca_expr_log_gen (log, enumval, separator, quotes);
	  if (i != sz - 1)
	    ccl_log (log, ", ");

	  ar_ca_expr_del_reference (enumval);
	}
      ccl_log (log, " }");
    }
}

			/* --------------- */

char *
ar_ca_domain_value_to_string (const ar_ca_domain *dom, int value, 
			      ar_ca_exprman *man)
{  
  char *result;

  if ((dom->size_and_type & BOOL))
    result = ccl_string_dup (value ? "true" : "false");
  else if ((dom->size_and_type & RANGE))
    result = ccl_string_format_new ("%d", value);
  else
    {
      ar_ca_expr *enumval = ar_ca_expr_get_enum_constant_by_index (man, value);
      ar_identifier *name = ar_ca_expr_enum_constant_get_name (enumval);
      result = ar_identifier_to_string (name);
      ar_identifier_del_reference (name);
      ar_ca_expr_del_reference (enumval);
    }

  return result;
}

			/* --------------- */

void
ar_ca_domain_log_value (ccl_log_type log, const ar_ca_domain *dom, int value,
			ar_ca_exprman *man)
{
  ar_ca_domain_log_value_gen (log, dom, value, man, ".", NULL);
}

			/* --------------- */

void
ar_ca_domain_log_value_gen (ccl_log_type log, const ar_ca_domain *dom, 
			    int value, ar_ca_exprman *man, 
			    const char *separator, const char *quotes)
{
  ccl_pre (dom != NULL); 
  ccl_pre (man != NULL);

  if ((dom->size_and_type & BOOL))
    ccl_log (log, value ? "true" : "false");
  else if ((dom->size_and_type & RANGE))
    ccl_log (log, "%d", value);
  else
    {
      ar_ca_expr *enumval = ar_ca_expr_get_enum_constant_by_index (man, value);
      ar_ca_expr_log_gen (log, enumval, separator, quotes);
      ar_ca_expr_del_reference (enumval);
    }
}

			/* --------------- */

static ar_ca_domain *
s_allocate_domain (int type, int nb_values)
{
  ar_ca_domain *result = (ar_ca_domain *)
    ccl_malloc (sizeof (ar_ca_domain) + (nb_values - 1) * sizeof (int));

  result->refcount = 1;
  result->size_and_type = (nb_values << TYPE_BIT_SIZE) + type;

  return result;
}

			/* --------------- */

static void
s_delete_domain (ar_ca_domain *domain)
{  
  ccl_delete (domain);
}
