/*
 * sas-formats.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_FORMATS_H
# define SAS_FORMATS_H

# include <sas/sas-common.h>
# define SAS_COLUMN_WIDTH "15"
# define SAS_STRCOLUMN_WIDTH SAS_COLUMN_WIDTH

# define SAS_DOUBLE_FORMAT(L,p,f) "%" L "." p SAS_DOUBLE_MODIFIER f

# define SAS_DEFAULT_FORMAT SAS_DOUBLE_FORMAT("","5","E")
# define SAS_DEFAULT_TAB_FORMAT SAS_DOUBLE_FORMAT(SAS_COLUMN_WIDTH,"5","E")
# define SAS_DELAY_FORMAT SAS_DEFAULT_FORMAT
# define SAS_TIME_FORMAT SAS_DELAY_FORMAT
# define SAS_STRCOLUMN_FORMAT "%" SAS_STRCOLUMN_WIDTH "s"
# define SAS_INT_FORMAT "%" SAS_COLUMN_WIDTH "ld"

#endif /* ! SAS_FORMATS_H */
