/*
 * sas-params.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <math.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-model.h"
#include "sas-rnd.h"
#include "sas-formats.h"
#include "sas-params.h"

struct lp_methods
{
  void (*destroy) (sas_law_param *p);
  sas_double (*compute_value) (sas_law_param *p, sas_prng *prng);
  int (*uses_parameter) (const sas_law_param *p, const sas_law_param *other);
  void (*get_vars) (const sas_law_param *p, ccl_set *result);
  sas_law_param *(*instantiate) (sas_law_param *p, ar_identifier *prefix);  
  void (*display) (const sas_law_param *p, ccl_log_type log,
		   void (*idlog)(ccl_log_type log,
				 const ar_identifier *id));
};

struct lp_base
{
  int refcount;
  const struct lp_methods *methods;
  sas_law_param_kind kind;
  sas_timestamp timestamp;
  sas_double value;
};

struct lp_constant
{
  struct lp_base super;
};

struct lp_variable
{
  struct lp_base super;
  ar_identifier *name;
  sas_law_param *def;
};

struct lp_binary
{
  struct lp_base super;
  sas_law_param *args[2];
};

union sas_law_param
{
  struct lp_base base;
  struct lp_constant constant;
  struct lp_variable variable;
  struct lp_binary binary;
};

#define SELF(L, type)  \
  struct lp_ ## type *self = (ccl_pre ((L)!= NULL), &((L)->type))

#define CSTSELF(L,type)							\
  const struct lp_ ## type *self = (ccl_pre ((L)!= NULL), &((L)->type))

#define NEW_PARAM(type) \
  s_allocate_param (sizeof (struct lp_ ## type), &lp_ ## type ## _methods)

static sas_law_param *
s_allocate_param (size_t size, const struct lp_methods *methods);

#define LP_DESTROY(type, p_)				\
  void s_ ## type ## _destroy (sas_law_param *p_)
#define LP_COMPUTE_VALUE(type, p_, prng_)			\
  sas_double s_ ## type ## _compute_value (sas_law_param *p_, sas_prng *prng_)
#define LP_USES_PARAMETER(type, p_, other_)			     \
  int s_ ## type ## _uses_parameter (const sas_law_param *p_, \
				     const sas_law_param *other_)
#define LP_DISPLAY(type, p_, log_, idlog_)	\
  void s_ ## type ## _display (const sas_law_param *p_, ccl_log_type log_, \
			       void (*idlog_)(ccl_log_type, \
					     const ar_identifier *)) 

#define LP_GET_VARS(type, p_, result_)		      \
  void s_ ## type ## _get_vars (const sas_law_param *p_, ccl_set *result_)

#define LP_INSTANTIATE(type, p_, prefix_)			\
  sas_law_param *s_ ## type ## _instantiate (sas_law_param *p_, \
					     ar_identifier *prefix_)


#define DECLARE_LAW_PARAM(type)						\
  static LP_DESTROY (type, p);						\
  static LP_COMPUTE_VALUE (type, p, prng);				\
  static LP_USES_PARAMETER (type, p, other);				\
  static LP_GET_VARS (type, p, result);					\
  static LP_INSTANTIATE (type, p, prefix);				\
  static LP_DISPLAY (type, p, log, idlog);				\
  static struct lp_methods lp_ ## type ## _methods = {			\
    s_ ## type ## _destroy, s_ ## type ## _compute_value,		\
    s_ ## type ## _uses_parameter,					\
    s_ ## type ## _get_vars,						\
    s_ ## type ## _instantiate,						\
    s_ ## type ## _display }

static unsigned long LP_TIMESTAMP = 1L;
  
/******************************************************************************
 *
 * CONSTRUCTORS
 *
 ******************************************************************************/

DECLARE_LAW_PARAM (constant);

sas_law_param *
sas_law_param_crt_constant (sas_double p)
{
  sas_law_param *result = NEW_PARAM (constant);
  SELF (result, base);

  self->kind = SAS_LAW_PARAM_CONSTANT;
  self->value = p;
  
  return result;
}

DECLARE_LAW_PARAM (variable);

sas_law_param *
sas_law_param_crt_variable (ar_identifier *id)
{
  sas_law_param *result = NEW_PARAM (variable);
  SELF (result, variable);
  
  ccl_pre (id != NULL);
  self->super.kind = SAS_LAW_PARAM_VARIABLE;
  self->name = ar_identifier_add_reference (id);
  self->def = NULL;
  
  return result;
}

sas_law_param *
sas_law_param_crt_lognormal (sas_law_param *mean, sas_law_param *error_factor)
{
  return sas_law_param_crt_binary (SAS_LAW_PARAM_LOGNORMAL, mean, error_factor);
}

sas_law_param *
sas_law_param_crt_uniform (sas_law_param *min_value, sas_law_param *max_value)
{
  return sas_law_param_crt_binary (SAS_LAW_PARAM_UNIFORM, min_value, max_value);
}

sas_law_param *
sas_law_param_crt_normal (sas_law_param *mean, sas_law_param *stddev)
{
  return sas_law_param_crt_binary (SAS_LAW_PARAM_NORMAL, mean, stddev);
}

DECLARE_LAW_PARAM (binary);

sas_law_param *
sas_law_param_crt_binary (sas_law_param_kind kind, sas_law_param *p1, 
			  sas_law_param *p2)
{
  sas_law_param *result = NEW_PARAM (binary);
  SELF (result, binary);

  ccl_pre (p1 != NULL);
  ccl_pre (p2 != NULL);

  self->super.kind = kind;
  self->args[0] = sas_law_param_add_reference (p1);
  self->args[1] = sas_law_param_add_reference (p2);
  
  return result;  
}

/******************************************************************************
 *
 * GENERIC FUNCTIONS
 *
 ******************************************************************************/
sas_law_param *
sas_law_param_add_reference (sas_law_param *p)
{
  SELF (p, base);

  self->refcount++;
  
  return p;
}

void
sas_law_param_del_reference (sas_law_param *p)
{
  SELF (p, base);
  
  if (--self->refcount == 0)
    {
      self->methods->destroy (p);
      ccl_delete (self);
    }
}

void
sas_law_param_log (ccl_log_type log, const sas_law_param *p)
{
  sas_law_param_log_gen (log, p, ar_identifier_log_quote);
    
}

void
sas_law_param_log_gen (ccl_log_type log, const sas_law_param *p,
		       void (*idlog)(ccl_log_type log,
				     const ar_identifier *id))
{
  CSTSELF (p, base);

  ccl_pre (self->methods->display!= NULL);
  self->methods->display (p, log, idlog);
}

sas_double 
sas_law_param_get_value (sas_law_param *p, sas_prng *prng)
{
  SELF (p, base);
  
  if (self->timestamp != LP_TIMESTAMP)
    {
      self->value = self->methods->compute_value (p, prng);
      self->timestamp = LP_TIMESTAMP;
    }
  
  return self->value;
}

const ar_identifier *
sas_law_param_get_name_cst (sas_law_param *p)
{
  SELF (p, variable);

  ccl_pre (self->super.methods == &lp_variable_methods);
  
  return self->name;
}

ar_identifier *
sas_law_param_get_name (sas_law_param *p)
{
  SELF (p, variable);

  ccl_pre (self->super.methods == &lp_variable_methods);
  
  return ar_identifier_add_reference (self->name);
}

void
sas_law_param_set_definition (sas_law_param *var, sas_law_param *def)
{
  SELF (var, variable);

  ccl_pre (self->super.methods == &lp_variable_methods);

  if (self->def != NULL)
    sas_law_param_del_reference (self->def);
  ccl_pre (! sas_law_param_uses_parameter (def, var));
  
  self->def = sas_law_param_add_reference (def);
}

sas_law_param *
sas_law_param_get_definition (sas_law_param *var)
{
  SELF (var, variable);
  
  ccl_pre (self->super.kind == SAS_LAW_PARAM_VARIABLE);
  if (self->def == NULL)
    return NULL;
  
  return sas_law_param_add_reference (self->def);
}

int
sas_law_param_uses_parameter (const sas_law_param *p,
			      const sas_law_param *other)
{
  CSTSELF (p, base);

  if (p == other)
    return 1;
  return self->methods->uses_parameter (p, other);
}

void
sas_law_param_value_timestamp (void)
{
  LP_TIMESTAMP++;
}


sas_law_param *
sas_law_param_instantiate (sas_law_param *p, ar_identifier *prefix)
{
  CSTSELF (p, base);

  return self->methods->instantiate (p, prefix);
  
}

ccl_set *
sas_law_param_get_variables (sas_law_param *p, ccl_set *result)
{
  CSTSELF (p, base);
  if (result == NULL)
    result = ccl_set_create ();
  
  self->methods->get_vars (p, result);
  
  return result;
}

sas_law_param_kind
sas_law_param_get_kind (sas_law_param *p)
{
  CSTSELF (p, base);
  
  return self->kind;
}

sas_law_param *
sas_law_param_get_arg (sas_law_param *p, int i)
{
  CSTSELF (p, binary);

  ccl_pre (!(self->super.kind == SAS_LAW_PARAM_CONSTANT ||
	     self->super.kind == SAS_LAW_PARAM_VARIABLE));
  return sas_law_param_add_reference (self->args[i]);
}

int
sas_law_param_is_constant (sas_law_param *p, sas_double *pvalue)
{
  int result = 0;

  if (sas_law_param_get_kind (p) == SAS_LAW_PARAM_CONSTANT)
    {
      CSTSELF (p, constant);
      if (pvalue)
	*pvalue = self->super.value;
      result = 1;
    }
  else if (sas_law_param_get_kind (p) == SAS_LAW_PARAM_VARIABLE)
    {
      CSTSELF (p, variable);
      result = sas_law_param_is_constant (self->def, pvalue);
    }
  return result;
}

/******************************************************************************
 *
 * STATIC FUNCTIONS
 *
 ******************************************************************************/

static sas_law_param *
s_allocate_param (size_t size, const struct lp_methods *methods)
{
  sas_law_param *result = ccl_calloc (size, 1);
  SELF (result, base);

  self->refcount = 1;
  self->timestamp = 0;
  self->methods = methods;

  return result;
}

/* 
 * CONSTANT PARAMETERS 
 */
static LP_DESTROY (constant, p)
{
}

static LP_COMPUTE_VALUE (constant, p, prng)
{
  SELF (p, base);

  return self->value;
}

static LP_USES_PARAMETER (constant, p, other) 
{
  return 0;  
}

static LP_GET_VARS (constant, p, result) 
{
}

static LP_INSTANTIATE (constant, p, prefix) 
{
  return sas_law_param_add_reference (p);
}

static LP_DISPLAY (constant, p, log, idlog)
{
  CSTSELF (p, base);
  
  ccl_log (log, SAS_DEFAULT_FORMAT, self->value);
}

/* 
 * VARIABLE PARAMETERS 
 */
static LP_DESTROY (variable, p)
{
  SELF (p, variable);
  
  ar_identifier_del_reference (self->name);
  ccl_zdelete (sas_law_param_del_reference, self->def);
}

static LP_COMPUTE_VALUE (variable, p, prng)
{
  SELF (p, variable);

  ccl_pre (self->def != NULL);
  
  return sas_law_param_get_value (self->def, prng);
}

static LP_USES_PARAMETER (variable, p, other) 
{
  CSTSELF (p, variable);

  if (p->base.methods == other->base.methods &&
      self->name == ((const struct lp_variable *)other)->name)
    return 1;
  
  if (self->def == NULL)
    return 0;
  
  return sas_law_param_uses_parameter (self->def, other);
}

static LP_GET_VARS (variable, p, result) 
{
  CSTSELF (p, variable);

  if (! ccl_set_has (result, self->name))
    ccl_set_add (result, self->name);
}

static LP_INSTANTIATE (variable, p, prefix) 
{
  CSTSELF (p, variable);
  sas_law_param *result;
  
  if (ar_model_has_law_parameter (self->name))
    result = sas_law_param_add_reference (p);
  else
    {
      ar_identifier *instance = ar_identifier_add_prefix (self->name, prefix);
      result = sas_law_param_crt_variable (instance);  
      ar_identifier_del_reference (instance);
  
      if (self->def != NULL)
	result->variable.def = sas_law_param_instantiate (self->def, prefix);
    }
   
  return result;
}

static LP_DISPLAY (variable, p, log, idlog)
{
  CSTSELF (p, variable);

  idlog (log, self->name);
}

/* 
 * BINARY PARAMETERS 
 */
static LP_DESTROY (binary, p)
{
  SELF (p, binary);
  
  sas_law_param_del_reference (self->args[0]);
  sas_law_param_del_reference (self->args[1]);
}

static sas_double
s_compute_lognormal (sas_double mean, sas_double error_factor, sas_prng *prng)
{
  sas_double sigma = log (error_factor) / 1.645;
  sas_double mu = log (mean) - (sigma * sigma) / 2.0;

  return exp (sas_prng_normal (prng, mu, sigma));      
}

static sas_double
s_compute_uniform (sas_double a, sas_double b, sas_prng *prng)
{
  return sas_prng_uniform (prng, a, b);
}

static sas_double
s_compute_normal (sas_double mean, sas_double stddev, sas_prng *prng)
{
  return sas_prng_normal (prng, mean, stddev);
}

static LP_COMPUTE_VALUE (binary, p, prng)
{
  SELF (p, binary);
  sas_double (*compute) (sas_double, sas_double, sas_prng *);

  switch (self->super.kind) {
  case SAS_LAW_PARAM_LOGNORMAL:
    compute = s_compute_lognormal;
    break;
  case SAS_LAW_PARAM_UNIFORM:
    compute = s_compute_uniform;
    break;
  default:
    ccl_assert (self->super.kind == SAS_LAW_PARAM_NORMAL);
    compute = s_compute_normal;
    break;
  }
  
  return compute (sas_law_param_get_value (self->args[0], prng),
		  sas_law_param_get_value (self->args[1], prng),
		  prng);
}

static LP_USES_PARAMETER (binary, p, other) 
{
  CSTSELF (p, binary);

  return (sas_law_param_uses_parameter (self->args[0], other) ||
	  sas_law_param_uses_parameter (self->args[1], other));
}

static LP_GET_VARS (binary, p, result) 
{
  CSTSELF (p, binary);

  sas_law_param_get_variables (self->args[0], result);
  sas_law_param_get_variables (self->args[1], result);
}

static LP_INSTANTIATE (binary, p, prefix) 
{
  CSTSELF (p, binary);
  sas_law_param *p1 = sas_law_param_instantiate (self->args[0], prefix);
  sas_law_param *p2 = sas_law_param_instantiate (self->args[1], prefix);
  sas_law_param *result = sas_law_param_crt_binary (self->super.kind, p1, p2);
  sas_law_param_del_reference (p1);
  sas_law_param_del_reference (p2);

  return result;
}

static LP_DISPLAY (binary, p, log, idlog)
{
  const char *op;
  CSTSELF (p, binary);

  switch (self->super.kind) {
  case SAS_LAW_PARAM_LOGNORMAL: op = "lognormal"; break;
  case SAS_LAW_PARAM_UNIFORM: op = "uniform"; break;
  default:
    ccl_assert (self->super.kind == SAS_LAW_PARAM_NORMAL);
    op = "normal";
    break;
  }
    
  ccl_log (log, "%s(", op);
  sas_law_param_log_gen (log, self->args[0], idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->args[1], idlog);
  ccl_log (log, ")");
}
