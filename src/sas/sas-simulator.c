/*
 * sas-simulator.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <math.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-list.h>
#include <ccl/ccl-hash.h>

#if HAVE_PTHREAD
# include <pthread.h>
# include <signal.h>
#endif

#include "sas-rnd.h"
#include "sas-formats.h"
#include "sas-model.h"
#include "sas-scheduler.h"
#include "sas.h"
#include "sas-p.h"

typedef struct measure measure;
struct measure
{
  sas_double current_value;
  sas_double mean;
  sas_double variance;
  sas_double stddev;
  sas_double confidence;
  sas_double *histogram;
};

typedef struct transition transition;
struct transition
{
  int index;
  int priority;
  uint8_t has_memory;
  sas_action *act;
  sas_law *law;
};

typedef struct transition_simulation_state trans_simstate;
struct transition_simulation_state
{
  sas_event super;
  transition *t;
  uint8_t is_delayed;
  measure frequency;
};

typedef struct observer observer;
struct observer
{
  int index;
  sas_observer *obs;
  int is_boolean;  
};

typedef struct observer_simulation_state observer_simstate;
struct observer_simulation_state
{
  observer *o;
  sas_double current_value;
  sas_time last_change;
  
  measure cumulated_time;
  measure final_value;
  measure value;
  measure number_of_occurrences;
  measure first_occurrences;
  sas_int number_of_missing_occurrences;
};
  
enum simulation_status {
  SIMULATION_ERROR,
  SIMULATION_INTERRUPTED,
  SIMULATION_INFINITE_LOOP,
  SIMULATION_TIMEOUT,
  SIMULATION_TERMINATED,
  SIMULATION_IN_PROGRESS
};

typedef struct simulation_state simstate;
typedef struct sas_simulator sas_simulator;
struct sas_simulator
{  
  /* model to simulate */
  sas_model *model;
  int nb_trans;
  transition *trans;
  int nb_observers;
  observer *observers;

  const sas_simulation_parameters *params;
  int nb_states; 
  simstate **states;
  simstate *R;
};

typedef struct simulation_state simstate;
struct simulation_state
{
  int index;
  sas_simulator *sim;
  enum simulation_status status;
  int number_of_stories;
  int story;
  sas_time current_time;
  int nb_immediate_actions;

  sas_scheduler *sched;
  
  sas_model_state *mstate;
  trans_simstate *tstates;
  observer_simstate *ostates;

  sas_prng *prng;
};

int sas_debug_is_on = 0;
int sas_debug_sim_stories = 1;
int sas_debug_sim_scheduler = 1;
int sas_debug_sim_sequential_threads = 0;

#define DISPLAY_SCHED SAS_DBG(sim_scheduler)

static int
s_is_before (const trans_simstate *T1, const trans_simstate *T2)
{
  ccl_pre (sas_event_get_firing_time (T1) == sas_event_get_firing_time (T2));

  if (T1->t->priority > T2->t->priority)
    return 1;
      
  ccl_pre (T1->t != NULL);
  ccl_pre (T2->t != NULL);
  
  return ((T1->t->priority == T2->t->priority) &&
	  (T1->t->index < T2->t->index));
}

static simstate *
s_crt_simulation_state (sas_simulator *sim)
{
  int i;
  simstate *S = ccl_new (simstate);
  trans_simstate *t;
  observer_simstate *o;

  S->sim = sim;
  S->status = SIMULATION_ERROR;
  S->number_of_stories = 0;
  S->story = 0;
  S->current_time = 0;
  S->nb_immediate_actions = 0;
  switch (sim->params->scheduler) {
  case SAS_SCH_DLINK:
    S->sched =
      sas_scheduler_create_double_linked_list ((sas_priority_func *)
					       s_is_before);
    break;
  case SAS_SCH_CQ:
    S->sched =
      sas_scheduler_create_calendar_queue ((sas_priority_func *)s_is_before,
					   sim->nb_trans);
    break;
  };
  S->mstate = sim->model->crt_state (sim->model);
  t = S->tstates = ccl_new_array (trans_simstate, sim->nb_trans);
  for (i = 0; i < sim->nb_trans; i++, t++)
    {
      ccl_memzero (t, sizeof (*t));
      t->t = sim->trans + i;
    }
  
  o = S->ostates = ccl_new_array (observer_simstate, sim->nb_observers);
  for (i = 0; i < sim->nb_observers; i++, o++)
    {
      ccl_memzero (o, sizeof (*o));
      o->o = sim->observers + i;
    }
  
  return S;
}

static void
s_delete_simulation_state (simstate *S)
{
  int i;
  sas_simulator *sim = S->sim;
  trans_simstate *t = S->tstates;
  observer_simstate *o = S->ostates;

  sas_scheduler_del_reference (S->sched);
  
  sim->model->delete_state (sim->model, S->mstate);
  for (i = 0; i < sim->nb_trans; i++, t++)
    ccl_zdelete (ccl_delete, t->frequency.histogram);
  ccl_zdelete (ccl_delete, S->tstates);
  for (i = 0; i < sim->nb_observers; i++, o++)
    {
      ccl_zdelete (ccl_delete, o->cumulated_time.histogram);
      ccl_zdelete (ccl_delete, o->final_value.histogram);
      ccl_zdelete (ccl_delete, o->value.histogram);
      ccl_zdelete (ccl_delete, o->number_of_occurrences.histogram);
      ccl_zdelete (ccl_delete, o->first_occurrences.histogram);
    }
  ccl_zdelete (ccl_delete, S->ostates);
  ccl_delete (S);
}

sas_simulator *
sas_simulator_crt (sas_model *model)
{
  int i;
  transition *T;
  observer *O;
  sas_action * const *actions;
  sas_observer * const *observers;
  sas_simulator *result = ccl_new (sas_simulator);
  
  result->model = model;
  result->params = NULL;
  
  actions = model->get_actions (model, &result->nb_trans);
  T = result->trans = ccl_new_array (transition, result->nb_trans + 2);
  for (i = 0; i < result->nb_trans; i++, T++)
    {
      T->index = i;
      T->act = actions[i];
      T->priority = model->get_action_priority (model, T->act);      
      T->law = model->get_action_law (model, T->act);
      T->has_memory = (model->has_memory (model, T->act) ? 1 : 0);      
    }
  
  observers = model->get_observers (model, &result->nb_observers);
  O = result->observers = ccl_new_array (observer, result->nb_observers);  
  for (i = 0; i < result->nb_observers; i++, O++)
    {
      O->index = i;
      O->obs = observers[i];
      O->is_boolean = model->observer_is_boolean (model, O->obs);      
    }

  return result;
}

static void
s_destroy_transition (transition *T)
{  
  sas_law_del_reference (T->law);
}

static void
s_destroy_observer (observer *O)
{
}

void
sas_simulator_destroy (sas_simulator *sim)
{
  int i;

  if (sim->states != NULL)
    {
      for (i = 0; i < sim->nb_states; i++)
	s_delete_simulation_state (sim->states[i]);
      ccl_delete (sim->states);
    }
  
  if (sim->trans != NULL)
    {
      for (i = 0; i < sim->nb_trans; i++)
	s_destroy_transition (sim->trans + i);
      ccl_delete (sim->trans);
    }

  if (sim->observers != NULL)
    {
      for (i = 0; i < sim->nb_observers; i++)
	s_destroy_observer (sim->observers + i);
      ccl_delete (sim->observers);
    }
  ccl_delete (sim);
}

static void
s_update_measure_accumulators (measure *m, sas_int N)
{
  sas_double tmp;
  if (m->histogram != NULL)
    m->histogram[N - 1] = m->current_value;

  tmp = m->mean;
  m->mean += (m->current_value - tmp) / N;
  if (N > 1)
    {
      tmp = m->mean - tmp;
      m->variance += N * tmp * (N-1) * tmp;
    }
}

static void
s_compute_measure (measure *m, sas_int N)
{
  sas_double dN = (sas_double) N;

  if (N > 1)
    {
      m->stddev = sqrt (m->variance / (dN - 1.0)); 
      m->confidence = (1.64 * m->stddev) / sqrt(dN);
    }
}

static void
s_reset_for_one_story (simstate *S)
{
  int i;
  sas_time ftime;
  sas_simulator *sim = S->sim;
  trans_simstate *T;
  observer_simstate *O;

  /* set t0 and initial configuration */ 
  S->current_time = 0.0;
  sim->model->set_initial_state (sim->model, S->mstate);

  /* reset the loop detection counter */
  S->nb_immediate_actions = 0;

  /* set initial scheduler */
  sas_scheduler_make_empty (S->sched);
  sas_scheduler_start_insertion (S->sched);
  for (T = S->tstates, i = 0; i < sim->nb_trans; i++, T++)
    {
      sas_event_reset (T);
      T->is_delayed = 0;
      T->frequency.current_value = 0.0;
      
      if (sim->model->is_enabled (sim->model, S->mstate, T->t->act))
	{
	  ftime = sas_law_compute_delay (T->t->law, 0.0, S->prng, S->mstate);
	  sas_event_set_firing_time (T, ftime);				     
	  sas_scheduler_insert (S->sched, (sas_event *) T);
	}
    }
  sas_scheduler_end_insertion (S->sched);
  
  /* initialize measures for this story */
  for (O = S->ostates, i = 0; i < sim->nb_observers; i++, O++)
    {
      O->current_value =
	sim->model->get_observer_value (sim->model, S->mstate, O->o->obs);
      O->last_change = 0.0;
      O->cumulated_time.current_value = 0.0;
      O->value.current_value = 0.0;

      O->final_value.current_value = 0.0;
      if (O->o->is_boolean)
	{
	  if (O->current_value != 0.0)
	    {
	      O->number_of_occurrences.current_value = 1.0;
	      O->first_occurrences.current_value = 0.0;
	    }
	  else
	    {	    
	      O->number_of_occurrences.current_value = 0.0;
	      O->first_occurrences.current_value = -1.0;
	    }
	}
    }
}

static void
s_update_linked_actions (sas_model *m, sas_action *act, int actindex,
			 int is_enabled, void *data)
{
  sas_delay delay;
  trans_simstate *depT;
  simstate *S = data;  
  
  depT = S->tstates + actindex;
      
  if (sas_event_is_active (depT))
    {
      if (! is_enabled)
	{
	  if (depT->t->has_memory)
	    {
	      delay = sas_event_get_firing_time (depT) - S->current_time;
	      sas_event_set_firing_time (depT, delay);
	      depT->is_delayed = 1;
	    }
	  else
	    {
	      sas_event_set_firing_time (depT, 0.0);
	    }
	  sas_scheduler_remove (S->sched, (sas_event *) depT);
	}
    }
  else if (is_enabled)
    {
      if (depT->is_delayed)
	{
	  depT->is_delayed = 0;
	  delay = sas_event_get_firing_time (depT);
	}
      else
	{
	  delay = sas_law_compute_delay (depT->t->law, S->current_time,
					 S->prng, S->mstate);
	}
      sas_event_set_firing_time (depT, S->current_time + delay);
      sas_scheduler_insert (S->sched, (sas_event *) depT);
    }
}

static void
s_update_scheduler (simstate *S, trans_simstate *T)
{
  sas_simulator *sim = S->sim;
  sas_model *model = sim->model;
  
  sas_scheduler_start_insertion (S->sched);
  model->map_touched_actions (model, T->t->act, S->mstate,
			      s_update_linked_actions, S);
  sas_scheduler_end_insertion (S->sched);
}

static void
s_update_observer_measures (observer_simstate *o, sas_double value,
			    sas_time current_time)
{
  sas_double delta = current_time - o->last_change;

  ccl_pre (delta >= 0.0);

  if (o->o->is_boolean)
    {
      if (o->current_value != 0.0)
	o->cumulated_time.current_value += delta;
      else if (value != 0.0)
	{
	  o->number_of_occurrences.current_value += 1.0;
	  if (o->first_occurrences.current_value < 0.0)
	    o->first_occurrences.current_value = current_time;
	}
    }
  o->value.current_value += o->current_value * delta;
  o->current_value = value;
  o->last_change = current_time;
}

static void
s_map_dependent_observers (sas_model *m, sas_observer *act, int obsindex,
			   void *data)
{
  simstate *S = data;
  sas_simulator *sim = S->sim;
  observer_simstate *o = S->ostates + obsindex;
  sas_double value = sim->model->get_observer_value (sim->model, S->mstate, 
						     o->o->obs);
  s_update_observer_measures (o, value, S->current_time);
}

static void
s_update_measures (simstate *S, trans_simstate *T)
{
  sas_simulator *sim = S->sim;
  
  T->frequency.current_value += 1.0;
  sim->model->map_dependent_observers (sim->model, T->t->act,
				       s_map_dependent_observers, S);
}

static void
s_finalize_measures_for_story (simstate *S)
{
  int i;
  observer_simstate *o;
  trans_simstate *T;
  sas_double value;
  sas_simulator *sim = S->sim;
  
  ccl_pre ((sim->params->duration - S->current_time) >= 0.0);

  for (i = 0, T = S->tstates; i < sim->nb_trans; i++, T++)
    s_update_measure_accumulators (&T->frequency, S->story);

  for (i = 0, o = S->ostates; i < sim->nb_observers; i++, o++)
    {
      value = sim->model->get_observer_value (sim->model, S->mstate, o->o->obs);
      
      s_update_observer_measures (o, value, sim->params->duration);
      
      s_update_measure_accumulators (&o->cumulated_time, S->story);

      o->value.current_value /= sim->params->duration;
      s_update_measure_accumulators (&o->value, S->story);

      o->final_value.current_value = value;
      s_update_measure_accumulators (&o->final_value, S->story);

      s_update_measure_accumulators (&o->number_of_occurrences, S->story);

      if (o->first_occurrences.current_value < 0.0)
	o->number_of_missing_occurrences++;
      else
	{
	  int nb_stories = S->story - o->number_of_missing_occurrences;
	  s_update_measure_accumulators (&o->first_occurrences, nb_stories);
	}
    }
}

struct displaydata
{
  ccl_log_type log;
  sas_model *model;
};

static void
s_log_trans (sas_event *e, void *data)
{
  struct displaydata *dd = data;
  trans_simstate *T = (trans_simstate *) e;
  
  ccl_log (dd->log, "   %s %" SAS_DOUBLE_MODIFIER "e\n",
	   dd->model->get_action_desc (dd->model, T->t->act),
	   sas_event_get_firing_time (T));
}

void
sas_display_scheduler (simstate *S, ccl_log_type log)
{
  struct displaydata data = { log, S->sim->model };
  
  sas_scheduler_map (S->sched, s_log_trans, &data);
  ccl_log (log, "\n");
}

#ifndef NDEBUG
const char *
s_pr_trans (sas_simulator *sim, transition *T)
{
  return sim->model->get_action_desc (sim->model, T->act);
}
#endif

static int 
s_simulate_one_story (simstate *S)
{
  int result = 1;
  sas_double delta;
  sas_time Ttime;
  trans_simstate *T;
  sas_simulator *sim = S->sim;

  SAS_DBG_C_START_TIMER (SAS_DBG(sim_stories) &&
			 (S->story % sas_debug_sim_stories) == 0,
			 ("story %d", S->story));
  
  s_reset_for_one_story (S);
  
  while (! sas_scheduler_is_empty (S->sched) && result)
    {      
      if(DISPLAY_SCHED)
	sas_scheduler_log (CCL_LOG_DEBUG, S->sched);

      if (S->nb_immediate_actions > sim->params->loop_length)
	break;
	    
      T = (trans_simstate *) sas_scheduler_take_first (S->sched);
      Ttime = sas_event_get_firing_time (T);
      if (Ttime >= sim->params->duration)
	break;

      result =
	sim->model->trigger_action (sim->model, S->mstate, T->t->act, S->prng);
	
      delta = Ttime - S->current_time;
      ccl_assert (delta >= 0.0);
      
      if (delta == 0.0)
	S->nb_immediate_actions++;
      else
	{
	  S->nb_immediate_actions = 0;
	  S->current_time = Ttime;
	}
      s_update_measures (S, T);
      s_update_scheduler (S, T);
    }
  if (result)
    s_finalize_measures_for_story (S);
  SAS_DBG_C_END_TIMER (SAS_DBG(sim_stories) &&
		       (S->story % sas_debug_sim_stories == 0));
  
  return result;
}


static void
s_update_law_params (sas_simulator *sim, sas_prng *prng)
{
  int i;
  transition *T;
  
  for (T = sim->trans, i = 0; i < sim->nb_trans; i++, T++)
    sas_law_update_params (T->law, prng);
}

static void
s_reset_for_simulation (simstate *S)
{
  int i;
  trans_simstate *T;
  observer_simstate *O;
  sas_simulator *sim = S->sim;
  
  S->story = 1;
  S->status = SIMULATION_IN_PROGRESS;
  
  for (T = S->tstates, i = 0; i < sim->nb_trans; i++, T++)
    ccl_memzero (&T->frequency, sizeof (measure));
  for (O = S->ostates, i = 0; i < sim->nb_observers; i++, O++)
    {
      ccl_memzero (&O->cumulated_time, sizeof (measure));
      ccl_memzero (&O->final_value, sizeof (measure));
      ccl_memzero (&O->value, sizeof (measure));
      ccl_memzero (&O->number_of_occurrences, sizeof (measure));
      ccl_memzero (&O->first_occurrences, sizeof (measure));
      O->number_of_missing_occurrences = 0;
    }
}

static void
s_finalize_measures_for_simulation (simstate *S)
{
  int i;
  trans_simstate *T;
  observer_simstate *o;
  sas_simulator *sim = S->sim;
  sas_int N = S->number_of_stories;
  
  ccl_pre (S->story == N);

  for (i = 0, T = S->tstates; i < sim->nb_trans; i++, T++)
    s_compute_measure (&T->frequency, N);
	
  for (i = 0, o = S->ostates; i < sim->nb_observers; i++, o++)
    {
      s_compute_measure (&o->cumulated_time, N);
      s_compute_measure (&o->value, N);
      s_compute_measure (&o->final_value, N);
      s_compute_measure (&o->number_of_occurrences, N);
      s_compute_measure (&o->first_occurrences,
			 N - o->number_of_missing_occurrences);
    }
}

static void
s_display_measure (measure *m, ccl_log_type log)
{
  ccl_log (log, "\t" SAS_DEFAULT_TAB_FORMAT "\t" SAS_DEFAULT_TAB_FORMAT "\t"
	   SAS_DEFAULT_TAB_FORMAT,
	   m->mean, m->stddev, m->confidence);
}

static void
s_display_histograms (simstate *S, ccl_log_type log)
{
  int i;
  int has_histo = 0;
  observer_simstate *O;
  sas_simulator *sim = S->sim;

  for (O = S->ostates, i = 0; i < sim->nb_observers && !has_histo; i++, O++)    
    if ((has_histo = (O->cumulated_time.histogram != NULL)))
      break;

  if (! has_histo)
    return;

  ccl_log (log, "*** HISTOGRAMS\n");
  for (; i < sim->nb_observers; i++, O++)
    {
      if (O->cumulated_time.histogram)
	{
	  int j;
	  ccl_log (log, " *** OBSERVER %d\n", i);
	  for (j = 0; j < S->number_of_stories; j++)
	    if (O->cumulated_time.histogram)
	      ccl_log (log, SAS_DEFAULT_FORMAT "\n",
		       O->cumulated_time.histogram[j]);
	  ccl_log (log, "\n");
	}
    }
  ccl_log (log, "\n");      
}

#define measure_offset(m) ((uintptr_t)(&(((observer_simstate *)NULL)->m)))
#define measure_at_offset(obs, offset) ((measure *)(((void *)obs)+(offset)))

static void
s_display_observers_measure (simstate *S, const char *heading,
			     uintptr_t offset, ccl_log_type log,
			     int censured_data, int boolean_only)
{
  int i;
  int has_one = 0;
  observer_simstate *O;
  sas_simulator *sim = S->sim;

  for (O = S->ostates, i = 0; !has_one && i < sim->nb_observers; i++, O++)
    if ((has_one = ((boolean_only && O->o->is_boolean)||
		    (!boolean_only && !O->o->is_boolean))))
      break;

  if (! has_one)
    return;
  
  ccl_log (log, "*** %s\n"
	   SAS_STRCOLUMN_FORMAT "\t" SAS_STRCOLUMN_FORMAT "\t"
	   SAS_STRCOLUMN_FORMAT "\t" SAS_STRCOLUMN_FORMAT,
	   heading, "NAME", "MEAN", "STDDEV", "CONF.");
  if (censured_data)
    ccl_log (log, "\t" SAS_STRCOLUMN_FORMAT, "CENSURED");
  ccl_log (log, "\n");
  for (; i < sim->nb_observers; i++, O++)
    {
      if ((boolean_only && O->o->is_boolean)||
	  (!boolean_only && !O->o->is_boolean))
	{
	  measure *m = measure_at_offset(O, offset);
	  const char *odesc =
	    sim->model->get_observer_desc (sim->model, O->o->obs);
	  ccl_log (log, SAS_STRCOLUMN_FORMAT, odesc);
	  s_display_measure (m, log);
	  if (censured_data)
	    ccl_log (log, "\t" SAS_INT_FORMAT,
		     O->number_of_missing_occurrences);
	  ccl_log (log, "\n");
	}
    }
  ccl_log (log, "\n");
  
}
  
static void
s_display_measures (simstate *S, ccl_log_type log)
{
  int i;
  trans_simstate *T;
  sas_simulator *sim = S->sim;
  
  ccl_log (log, "*** ACTION FREQUENCIES\n");
  ccl_log (log,
	   SAS_STRCOLUMN_FORMAT "\t" SAS_STRCOLUMN_FORMAT "\t"
	   SAS_STRCOLUMN_FORMAT "\t" SAS_STRCOLUMN_FORMAT "\n",
	   "NAME", "MEAN", "STDDEV", "CONF.");  
  for (T = S->tstates, i = 0; i < sim->nb_trans; T++, i++)
    {
      const char *Tdesc = sim->model->get_action_desc (sim->model, T->t->act);
      ccl_log (log, SAS_STRCOLUMN_FORMAT, Tdesc);
      s_display_measure (&T->frequency, log);      
      ccl_log (log, "\n");
    }
  ccl_log (log, "\n");

  s_display_observers_measure (S, "CUMULATED TIME WITH EXPECTED VALUE",
			       measure_offset (cumulated_time), log, 0, 1);
  s_display_observers_measure (S, "VALUE",
			       measure_offset (value), log, 0, 0);
  s_display_observers_measure (S, "FINAL VALUE",
			       measure_offset (final_value), log, 0, 0);
  s_display_observers_measure (S, "NUMBER OF OCCURRENCES",
			       measure_offset (number_of_occurrences), log,
			       0, 1);
  s_display_observers_measure (S, "FIRST OCCURRENCES",
			       measure_offset (first_occurrences), log, 1, 1);
  s_display_histograms (S, log);
}

static void
s_display_results (sas_simulator *sim, simstate *S, ccl_log_type log)
{
  switch (S->status) {
  case SIMULATION_INTERRUPTED:
    ccl_log (log, "Simulation has been interrupted by user.\n");
    break;
    
  case SIMULATION_INFINITE_LOOP:
    ccl_log (log, ("Simulation has been aborted because simulation time has "
		   "not been incremented after %d actions. Last scheduled "
		   "actions are:\n"),
	     sim->params->loop_length);
    sas_display_scheduler (S, log);
    ccl_log (log, "\n");
    break;
	
  case SIMULATION_TIMEOUT:
    ccl_log (log, ("simulation has been interrupted after specified "
		   "timeout delay of %d seconds.\n"), sim->params->timeout);
    break;
    
  case SIMULATION_TERMINATED:
    s_display_measures (S, log);
    break;

  case SIMULATION_ERROR:
  case SIMULATION_IN_PROGRESS:
    ccl_unreachable ();
    break;
  }
}

void
sas_simulator_display_results (sas_simulator *sim, ccl_log_type log)
{
  if (sim->states == NULL || sim->R->status == SIMULATION_ERROR)
    {
      ccl_error ("Simulation yields an error:");
      if (sim->model->log_error)
	sim->model->log_error (sim->model, CCL_LOG_ERROR);
      ccl_error ("\n");
      return;    
    }

  if (sim->nb_states > 1)
    {      
      if (sim->R->status == SIMULATION_TERMINATED)
	s_display_results (sim, sim->R, log);
      else
	{
	  int i = 0;
	  
	  for (i = 0; i < sim->nb_states; i++)
	    {
	      if (sim->states[i]->status == sim->R->status)
		{
		  s_display_results (sim, sim->states[i], log);
		  break;
		}
	    }
	  ccl_assert (i < sim->nb_states);
	}
    }
  else
    s_display_results (sim, sim->R, log);
}

static void
s_create_simulation_states (sas_simulator *sim)
{
  int i;
  simstate **S;  
#if HAVE_PTHREAD
  int nb_threads = sim->params->nb_threads;
#else
  int nb_threads = 1;
#endif
  int nb_stories_per_thread = sim->params->number_of_stories / nb_threads;
  int remaining_stories = sim->params->number_of_stories % nb_threads;

  sim->nb_states = nb_threads + 1;
  S = sim->states = ccl_new_array (simstate *, sim->nb_states);  

  for (i = 0; i < nb_threads; i++, S++)
    {
      *S = s_crt_simulation_state (sim);
      (*S)->index = i;
      (*S)->number_of_stories = nb_stories_per_thread;
      if (remaining_stories > 0)
	{
	  (*S)->number_of_stories++;
	  remaining_stories--;
	}
      s_reset_for_simulation (*S);
    }

  sim->R = *S = s_crt_simulation_state (sim);
  (*S)->index = i;
  (*S)->number_of_stories = 0;
  (*S)->story = 0;
  s_reset_for_simulation (*S);
}

static void *
s_run_process (void *arg)
{
  time_t t0 = time (NULL);
  simstate *S = arg;
  sas_simulator *sim = S->sim;
  sas_model *model = sim->model;

  s_reset_for_simulation (S);
    
  while (S->status == SIMULATION_IN_PROGRESS)
    {
      s_simulate_one_story (S);
      if (model->error (model))
	S->status = SIMULATION_ERROR;
      else if (S->nb_immediate_actions > sim->params->loop_length)
	S->status = SIMULATION_INFINITE_LOOP;
      else if (sim->params->timeout > 0 &&
	       (time (NULL) - t0) > sim->params->timeout)
	S->status = SIMULATION_TIMEOUT;
      else if (sim->params->stop_simulation != NULL &&
	       sim->params->stop_simulation (sim->params->stop_data))
	S->status = SIMULATION_INTERRUPTED;
      else if (S->story + 1 > S->number_of_stories)
	S->status = SIMULATION_TERMINATED;
      else
	S->story++;
    }
  
#if HAVE_PTHREAD
  ccl_warning ("thread %d / %d terminated (#rnd=%d, #stories=%d).\n",
	       S->index + 1, S->sim->params->nb_threads,
	       sas_prng_get_counter (S->prng), S->story);
#endif
   
  return (void *) S->status;
}

/*!
 * Let X = A \cup B and A \cap B = {} .
 *
 *           card(A)*mean(A) + card(B)*mean(B) 
 * mean(X) = ---------------------------------
 *               card (A) + card (B)
 *
 * delta = mean (B) - mean (A)
 *
 *                                        card(A) * card (B)
 * var(X) = var (A) + var (B) + delta^2 * ------------------
 *                                        card(A) + card (B)
 */
static void
s_merge_measures (measure *m1, sas_int N1, measure *m2, sas_int N2)
{
  sas_double NR = (N1 + N2);
  sas_double delta = m2->mean - m1->mean;
  sas_double mean = (N1 * m1->mean + N2 * m2->mean) / NR;
  sas_double variance = (m1->variance + m2->variance +
			 (delta * delta * N1 * N2) / NR);
  m1->mean = mean;
  m1->variance = variance;
}

static void
s_merge_statistics (simstate *R, simstate *S)
{
  int i;
  sas_simulator *sim = R->sim;
  trans_simstate *t1;
  trans_simstate *t2;
  observer_simstate *o1;
  observer_simstate *o2;
  sas_int N1, N2;
  
  R->status = S->status;
  if (R->status != SIMULATION_TERMINATED)
    return;
  
  N1 = R->number_of_stories;
  N2 = S->number_of_stories;
  t1 = R->tstates;
  t2 = S->tstates;
  
  for (i = 0; i < sim->nb_trans; i++, t1++, t2++)
    s_merge_measures (&t1->frequency, N1, &t2->frequency, N2);
  
  o1 = R->ostates;
  o2 = S->ostates;

  for (i = 0; i < sim->nb_observers; i++, o1++, o2++)
    {
      s_merge_measures (&o1->cumulated_time, N1, &o2->cumulated_time, N2);
      s_merge_measures (&o1->final_value, N1, &o2->final_value, N2);
      s_merge_measures (&o1->value, N1, &o2->value, N2);
      s_merge_measures (&o1->number_of_occurrences, N1,
			&o2->number_of_occurrences, N2);
      s_merge_measures (&o1->first_occurrences, N1, &o2->first_occurrences, N2);
      o1->number_of_missing_occurrences += o2->number_of_missing_occurrences;
    }
  
  R->number_of_stories += S->number_of_stories;
  R->story += S->story;
}

static void
s_merge_results (sas_simulator *sim)
{
  int i;
  simstate **S = sim->states;
  
  sim->R->status = (*S)->status;
  if (sim->R->status != SIMULATION_TERMINATED)
    return;

  if (sim->R->number_of_stories == 0)
    {
      sim->R->number_of_stories = (*S)->number_of_stories;
      sim->R->story = (*S)->story;

      for (i = 0; i < sim->nb_trans; i++)
	sim->R->tstates[i].frequency = (*S)->tstates[i].frequency;
      for (i = 0; i < sim->nb_observers; i++)
	{
	  sim->R->ostates[i].cumulated_time = (*S)->ostates[i].cumulated_time;
	  sim->R->ostates[i].final_value = (*S)->ostates[i].final_value;
	  sim->R->ostates[i].value = (*S)->ostates[i].value;
	  sim->R->ostates[i].number_of_occurrences =
	    (*S)->ostates[i].number_of_occurrences;
	  sim->R->ostates[i].first_occurrences =
	    (*S)->ostates[i].first_occurrences;
	  sim->R->ostates[i].number_of_missing_occurrences =
	    (*S)->ostates[i].number_of_missing_occurrences;
	}
      i = 1;
      S++;
    }
  else
    {
      i = 0;
    }
  
  for (; i < sim->nb_states -1 && sim->R->status == SIMULATION_TERMINATED;
       i++, S++)
    s_merge_statistics (sim->R, *S);
}

#if HAVE_PTHREAD
static int
s_run_with_threads (sas_simulator *sim)
{
  int t, i, nb_started;
  int res = 1;
  pthread_t *tasks;
  sas_prng **prngs;
  
  ccl_pre (sim->params->nb_threads > 1);

  s_create_simulation_states (sim);
  ccl_assert (sim->nb_states == sim->params->nb_threads + 1);
  
  tasks = ccl_new_array (pthread_t, sim->params->nb_threads);
  
  prngs = ccl_new_array (sas_prng *, sim->nb_states);
  sas_prng_create_parallel (sim->params->prng, sim->params->seed,
			    sim->nb_states, prngs);
  sim->R->prng = prngs[0];

  for (t = 0; t < sim->params->number_of_tries; t++)
    {
      sas_law_param_value_timestamp ();    
      s_update_law_params (sim, sim->R->prng);
  
      CCL_DEBUG_START_TIMED_BLOCK (("simulate %d stories with %d threads",
				    sim->params->number_of_stories,
				    sim->params->nb_threads));
      
      nb_started = 0;
  
      for (i = 0; i < sim->params->nb_threads; i++)
	{
	  if (sas_debug_sim_sequential_threads)
	    sim->states[i]->prng = sim->R->prng;
	  else
	    sim->states[i]->prng = prngs[i + 1];
	  
	  if (pthread_create (&tasks[i], NULL, s_run_process, sim->states[i]))
	    {
	      int j;
	      for (j = 0; j < i; j++)
		pthread_kill (tasks[j], SIGTERM);
	  
	      ccl_error ("can't create thread %d.\n", i);
	      res = 0;
	      goto end;
	    }
	  nb_started++;
	  
	  if (sas_debug_sim_sequential_threads)
	    {
	      void *status;
	      pthread_join (tasks[i], &status);
	    }
	}
  
      for (i = 0; i < nb_started; i++)
	{
	  void *status;
	  pthread_join (tasks[i], &status);
	  if (status == (void *) SIMULATION_ERROR)
	    {
	      for (; i < nb_started; i++)
		pthread_kill (tasks[i], SIGTERM);
	    }
	}  
      s_merge_results (sim);
      CCL_DEBUG_END_TIMED_BLOCK ();
      if (sim->R->status != SIMULATION_TERMINATED)
	break;
    }
  
  res = (sim->R->status == SIMULATION_TERMINATED);
  if (res)
    s_finalize_measures_for_simulation (sim->R);
  
 end:
  for (i = 0; i < sim->nb_states; i++)
    sas_prng_delete (prngs[i]);
  ccl_delete (prngs);
  ccl_delete (tasks);

  
  return res;
}
#else
# define s_run_with_threads s_run
#endif

static int
s_run (sas_simulator *sim)
{
  int i;
  int res;
  sas_prng *prng;
    
  prng = sas_prng_create (sim->params->prng, sim->params->seed);
  s_create_simulation_states (sim);
  sim->states[0]->prng = prng;
  
  for (i = 0; i < sim->params->number_of_tries; i++)
    {
      sas_law_param_value_timestamp ();    
      s_update_law_params (sim, prng);

      CCL_DEBUG_START_TIMED_BLOCK (("simulate %d stories",
				    sim->params->number_of_stories));       
      s_run_process (sim->states[0]);
      s_merge_results (sim);
      CCL_DEBUG_END_TIMED_BLOCK ();
      
      if (sim->R->status != SIMULATION_TERMINATED)
	break;
    }
  sas_prng_delete (prng);
  
  res = (sim->R->status == SIMULATION_TERMINATED);
  if (res)
    s_finalize_measures_for_simulation (sim->R);

  return res;
}


int 
sas_simulator_run (sas_simulator *sim, const sas_simulation_parameters *params)
{
  int res;

  if (sim->model->error (sim->model))
    return 0;
  
  sim->params = params;

  CCL_DEBUG_START_TIMED_BLOCK (("start simulation for %d tries", 
				sim->params->number_of_tries));         
  if (params->nb_threads > 1)
    res = s_run_with_threads (sim);
  else
    res = s_run (sim);
  CCL_DEBUG_END_TIMED_BLOCK ();
  
  return res;
}
