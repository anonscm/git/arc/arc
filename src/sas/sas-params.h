/*
 * sas-params.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_PARAMS_H
# define SAS_PARAMS_H

# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-set.h>
# include "ar-identifier.h"
# include <sas/sas-common.h>
# include <sas/sas-rnd.h>

BEGIN_C_DECLS

typedef union sas_law_param sas_law_param;
typedef enum sas_law_param_kind {
  SAS_LAW_PARAM_CONSTANT,
  SAS_LAW_PARAM_VARIABLE,
  SAS_LAW_PARAM_UNIFORM,
  SAS_LAW_PARAM_LOGNORMAL,
  SAS_LAW_PARAM_NORMAL
} sas_law_param_kind;

extern sas_law_param *
sas_law_param_crt_constant (sas_double p);

extern sas_law_param *
sas_law_param_crt_variable (ar_identifier *id);

extern sas_law_param *
sas_law_param_crt_lognormal (sas_law_param *mean, sas_law_param *error_factor);

extern sas_law_param *
sas_law_param_crt_uniform (sas_law_param *min_value, sas_law_param *max_value);

extern sas_law_param *
sas_law_param_crt_normal (sas_law_param *mean, sas_law_param *stddev);

extern sas_law_param *
sas_law_param_crt_binary (sas_law_param_kind kind, sas_law_param *p1, 
			  sas_law_param *p2);

extern sas_law_param *
sas_law_param_add_reference (sas_law_param *p);

extern void
sas_law_param_del_reference (sas_law_param *p);

extern void
sas_law_param_log (ccl_log_type log, const sas_law_param *p);

extern void
sas_law_param_log_gen (ccl_log_type log, const sas_law_param *p,
		       void (*idlog)(ccl_log_type log,
				     const ar_identifier *id));

extern sas_double 
sas_law_param_get_value (sas_law_param *p, sas_prng *prng);

extern const ar_identifier *
sas_law_param_get_name_cst (sas_law_param *p);

extern ar_identifier *
sas_law_param_get_name (sas_law_param *p);

extern void
sas_law_param_set_definition (sas_law_param *var, sas_law_param *def);

extern sas_law_param *
sas_law_param_get_definition (sas_law_param *var);

extern int
sas_law_param_uses_parameter (const sas_law_param *p,
			      const sas_law_param *other);

extern void
sas_law_param_value_timestamp (void);

extern sas_law_param *
sas_law_param_instantiate (sas_law_param *p, ar_identifier *prefix);

extern ccl_set *
sas_law_param_get_variables (sas_law_param *p, ccl_set *result);

extern sas_law_param_kind
sas_law_param_get_kind (sas_law_param *p);

extern sas_law_param *
sas_law_param_get_arg (sas_law_param *p, int i);

/*!
 * returns true if p is a constant value or a variable defined by a constant
 * parameter.
 */
extern int
sas_law_param_is_constant (sas_law_param *p, sas_double *pvalue);

END_C_DECLS

#endif /* ! SAS_PARAMS_H */
