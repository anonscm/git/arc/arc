/*
 * sas-calqueue.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <math.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-heap.h>
#include "sas-scheduler-p.h"

typedef struct calendarqueue_st calendarqueue;

struct calendarqueue_st
{
  sas_scheduler super;
  sas_event *table;
  int table_size;
  
  int firstsub;
  sas_event *bucket;
  sas_double width;
  int nbuckets;
  int mask;
  int qsize;
  
  sas_double lastprio;  
  int lastbucket;  
  sas_double buckettop;
  
  int bot_threshold;
  int top_threshold;
  int resizeenabled;
};

#define SELF(s) SAS_SELF(calendarqueue, s)
#define CSTSELF(s) SAS_CSTSELF(calendarqueue, s)
#define DEQUEUE(e)				\
  do {						\
    (e)->prev->next = (e)->next;		\
    (e)->next->prev = (e)->prev;		\
    (e)->next = (e)->prev = NULL;		\
  } while (0)

static void 
s_calendarqueue_destroy (sas_scheduler *sch);

static void 
s_calendarqueue_make_empty (sas_scheduler *sch);

static void
s_calendarqueue_start_insertion (sas_scheduler *sch);

static void 
s_calendarqueue_insert (sas_scheduler *sch, sas_event *e);

static void
s_calendarqueue_end_insertion (sas_scheduler *sch);

static int 
s_calendarqueue_is_empty (sas_scheduler *sch);

static sas_event * 
s_calendarqueue_take_first (sas_scheduler *sch);

static void 
s_calendarqueue_remove (sas_scheduler *sch, sas_event *e);

static void 
s_calendarqueue_map (sas_scheduler *sch, void (*map)(sas_event *e, void *data),
		     void *data);

static void
s_calendarqueue_log (sas_scheduler *sch, ccl_log_type log);

static const sas_scheduler_methods CALQUEUE_METHODS = {
  s_calendarqueue_destroy,
  s_calendarqueue_make_empty,
  s_calendarqueue_start_insertion, 
  s_calendarqueue_insert,
  s_calendarqueue_end_insertion,
  s_calendarqueue_take_first, 
  s_calendarqueue_remove, 
  s_calendarqueue_is_empty,
  s_calendarqueue_map,
  s_calendarqueue_log
};


sas_scheduler *
sas_scheduler_create_calendar_queue (sas_priority_func *priority,
				     int nb_events)
{
  sas_scheduler *result =
    sas_scheduler_new (sizeof (calendarqueue), priority, &CALQUEUE_METHODS);
  SELF (result);

  self->table_size = (3 * nb_events + (nb_events&0x1)) >> 1;
  self->table = ccl_new_array (sas_event, self->table_size);

  sas_scheduler_make_empty (result);
  
  return result;		       
}

static void 
s_calendarqueue_destroy (sas_scheduler *sch)
{
  SELF (sch);

  ccl_delete (self->table);  
}

static void
s_localinit (calendarqueue *self, int qbase, int nbuck, sas_double bwidth,
	     sas_double startprio)
{
  int i;
  sas_int n;
  
  self->firstsub = qbase;
  self->bucket = &self->table[qbase];
  self->width = bwidth;
  self->nbuckets = nbuck;
  self->mask = ~(1+~nbuck); /* this assumes that nbuck is a power of 2 */
  self->qsize = 0;
  for (i = 0; i < nbuck; i++)
    {
      self->bucket[i].next = self->bucket[i].prev = &self->bucket[i];
      self->bucket[i].firing_time = -1.0;
      self->bucket[i].active = 1;
    }
  self->lastprio = startprio;
  n = startprio / bwidth;
  self->lastbucket = n & self->mask;
  self->buckettop = (n + 1) * bwidth + 0.5 * bwidth;
  self->bot_threshold = (nbuck >> 1) - 2;
  self->top_threshold = (nbuck << 1);
}

static void 
s_calendarqueue_make_empty (sas_scheduler *sch)
{
  SELF (sch);
  
  s_localinit (self, 0, 2, 1.0, 0.0);
  self->resizeenabled = 1;
}

static void
s_calendarqueue_start_insertion (sas_scheduler *sch)
{
  SELF (sch);
  
  self->resizeenabled = 1;  
}

static double
s_newwidth (calendarqueue *self)
{
  int n;
  int nsamples;
  int oldresizeenabled;
  sas_double oldlastprio;  
  int oldlastbucket;  
  sas_double oldbuckettop;
  sas_event *tmpstore = NULL;
  sas_event *e;
  double sep;
  double result;
  double mean1 = 0.0;
  double mean2 = 0.0;
  
  if (self->qsize < 2)
    return 1.0;
  if (self->qsize <= 5)
    nsamples = self->qsize;
  else
    nsamples = 5 + self->qsize / 10;
  if (nsamples > 25)
    nsamples = 25;

  oldlastprio = self->lastprio;
  oldlastbucket = self->lastbucket;
  oldbuckettop = self->buckettop;
  oldresizeenabled = self->resizeenabled;
  self->resizeenabled = 0;
  n = nsamples;
  
  while (n && !s_calendarqueue_is_empty ((sas_scheduler *) self))
    {
      e = sas_scheduler_take_first ((sas_scheduler *) self);
      if (tmpstore != NULL)
	{
	  sep = e->firing_time - tmpstore->firing_time;
	  if (sep != 0.0)
	    {
	      mean1 += sep;
	      n--;
	    }
	}
      e->next = tmpstore;
      tmpstore = e;
    }
  
  if (n != 0)
    {
      while (tmpstore)
	{
	  e = tmpstore->next;
	  sas_scheduler_insert ((sas_scheduler *) self, tmpstore);
	  tmpstore = e;
	}
      result = self->width;
    }
  else
    {
      mean1 /= (double) n;
      mean1 *= 2;
      ccl_assert (mean1 >= 0.0);
      nsamples = 0;
      while (tmpstore)
	{
	  e = tmpstore->next;
	  sas_scheduler_insert ((sas_scheduler *) self, tmpstore);
	  if (e != NULL)
	    {
	      sep = tmpstore->firing_time - e->firing_time;
	      if (sep < mean1)
		{
		  nsamples++;
		  mean2 += sep;
		}
	    }
	  tmpstore = e;
	}
      ccl_assert (mean2 >= 0.0);
      mean2 /= (double) nsamples;
      result = 3.0 * mean2;
    }
  
  self->lastprio = oldlastprio;
  self->lastbucket = oldlastbucket;
  self->buckettop = oldbuckettop;
  self->resizeenabled = oldresizeenabled;
  
  return result;
}

static void
s_resize (calendarqueue *self, int newsize)
{
  double bwidth;
  int i;
  int oldnbuckets;
  sas_event *oldbucket;
  sas_event *e;
  sas_event *end;
  sas_event *next;

  if (! self->resizeenabled)
    return;

  self->resizeenabled = 0;
  
  bwidth = s_newwidth (self);  
  oldbucket = self->bucket;
  oldnbuckets = self->nbuckets;

  s_localinit (self,
	       ((self->firstsub == 0) ? self->table_size - newsize : 0),
	       newsize, bwidth, self->lastprio);
  
  for (i = oldnbuckets - 1; i >= 0; i--)
    {
      end = &oldbucket[i];
      for (e = end->next; e != end; e = next)
	{
	  next = e->next;
	  s_calendarqueue_insert ((sas_scheduler *) self, e);
	}
    }
  self->resizeenabled = 1;
}

static void 
s_calendarqueue_insert (sas_scheduler *sch, sas_event *e)
{
  sas_int i;
  sas_event *pe;
  sas_event *end;
  SELF (sch);

  ccl_pre (! self->resizeenabled || ! e->active);

  i = floor (e->firing_time  / self->width);
  i = i & self->mask; /* i % nbuckets */

  end = &self->bucket[i];
  for (pe = end->next; pe != end; pe = pe->next)
    {
      if (SAS_IS_BEFORE (sch, e, pe))
	{
	  pe->prev->next = e;
	  e->prev = pe->prev;
	  e->next = pe;
	  pe->prev = e;
	  goto incsize;
	}
    }
  e->next = end;
  e->prev = end->prev;
  end->prev->next = e;
  end->prev = e;

 incsize:
  ++self->qsize;
  if (self->resizeenabled && self->qsize > self->top_threshold)
    s_resize (self, 2 * self->nbuckets);
}

static void
s_calendarqueue_end_insertion (sas_scheduler *sch)
{
  SELF (sch);
  
  self->resizeenabled = 1;  
  if (self->qsize > self->top_threshold)
    s_resize (self, 2 * self->nbuckets);
}

static int 
s_calendarqueue_is_empty (sas_scheduler *sch)
{
  SELF (sch);
  
  return self->qsize == 0;
}

static sas_event * 
s_calendarqueue_take_first (sas_scheduler *sch)
{
  int i;
  int minindex;
  sas_event *e;
  sas_event *end;

  SELF (sch);
  
  if (s_calendarqueue_is_empty (sch))
    return NULL;

 retry:  
  i = self->lastbucket;
  while (1)
    {
      end = &self->bucket[i];
      e = end->next;
      
      if (e != end && e->firing_time < self->buckettop)
	{
	  e->prev->next = e->next;
	  e->next->prev = e->prev;
	  self->lastbucket = i;
	  self->lastprio = e->firing_time;
	  self->qsize--;
	  if (self->resizeenabled && self->qsize < self->bot_threshold)
	    s_resize (self, self->nbuckets >> 1);
	  
	  return e;
	}
      else
	{
	  i++;
	  if (i == self->nbuckets)
	    i = 0;
	  self->buckettop += self->width;
	  ccl_assert (self->buckettop > 0);
	  if (i == self->lastbucket)
	    break;
	}
    }
  minindex = -1;
  for (i = 0; i < self->nbuckets; i++)
    {
      end = &self->bucket[i];
      e = end->next;
      if (e != end &&
	  (minindex == -1 ||
	   e->firing_time < self->bucket[minindex].next->firing_time))
	minindex = i;
    }
  ccl_assert (0 <= minindex && minindex < self->nbuckets);
  e = self->bucket[minindex].next;
  ccl_assert (e != &self->bucket[minindex]);
  
  self->lastbucket = minindex;
  self->lastprio = e->firing_time;
  self->buckettop = (floor(self->lastprio/ self->width)+1.5) * self->width;
  ccl_assert (self->buckettop > 0);
  
  goto retry;
    
  return NULL;
}

static void 
s_calendarqueue_remove (sas_scheduler *sch, sas_event *e)
{
  SELF (sch);

  self->qsize--;
  DEQUEUE (e);
  if (self->qsize < self->bot_threshold)
    s_resize (self, self->nbuckets >> 1);
}

static void 
s_calendarqueue_map (sas_scheduler *sch, void (*map)(sas_event *e, void *data),
		     void *data)
{
  int i;
  sas_event *e;
  sas_event *end;
  SELF (sch);
  ccl_heap *h;
				 
  if (sas_scheduler_is_empty (sch))
    return;

  h = ccl_heap_create_with_data (self->qsize, (ccl_compare_with_data_func *)
				 sas_scheduler_cmp_event, sch);
  for (i = 0; i < self->nbuckets; i++)
    {
      end = &self->bucket[i];
      for (e = self->bucket[i].next; e != end; e = e->next)
	ccl_heap_add (h, e);
    }
  while (! ccl_heap_is_empty (h))
    map (ccl_heap_take_first (h), data);
  ccl_heap_delete (h);
}

static void
s_calendarqueue_log (sas_scheduler *sch, ccl_log_type log)
{
  int i;
  sas_event *e;
  sas_event *end;
  
  SELF (sch);
  ccl_log (log, "scheduler:\n");
  ccl_log (log, " type      : calendar queue\n");
  ccl_log (log, " table_size : %d\n", self->table_size);
  ccl_log (log, " qsize      : %d\n", self->qsize);
  ccl_log (log, " firstsub   : %d\n", self->firstsub);
  ccl_log (log, " nbuckets   : %d\n", self->nbuckets);
  ccl_log (log, " width      : %" SAS_DOUBLE_MODIFIER "e\n", self->width);
  ccl_log (log, " lastprio   : %" SAS_DOUBLE_MODIFIER "e\n", self->lastprio);
  ccl_log (log, " lastbucket : %d\n", self->lastbucket);
  ccl_log (log, " buckettop  : %" SAS_DOUBLE_MODIFIER "e\n", self->buckettop);
  ccl_log (log, " bot_thrd   : %d\n", self->bot_threshold);
  ccl_log (log, " top_thrd   : %d\n", self->top_threshold);
  ccl_log (log, " structure :\n");
  
  for (i = 0; i < self->nbuckets; i++)
    {
      ccl_log (log, "bucket[%3d] = ", i);
      end = &self->bucket[i];
      e = end->next;
      if (e == end)
	ccl_log (log, "NULL\n");
      else
	{
	  for (; e != end && e->firing_time < self->buckettop; e = e->next)
	    {
	      if (e != end->next)
		ccl_log (log, "              ");
	      ccl_log (log, "%p %" SAS_DOUBLE_MODIFIER "e", e, e->firing_time);
	      if (!e->active)
		ccl_log (log, " (inactive event)");
	      ccl_log (log, "\n");
	    }
	  if (e != end)
	    {
	      if (e != end->next)
		ccl_log (log, "              ");
	      ccl_log (log, "next year events\n");
	      for (; e != end; e = e->next)
		{
		  ccl_log (log, "              ");
		  ccl_log (log, "%p %" SAS_DOUBLE_MODIFIER "e", e,
			   e->firing_time);
		  if (!e->active)
		    ccl_log (log, " (inactive event)");
		  ccl_log (log, "\n");
		}
	    }
	  
	}
    }  
}  
		 
