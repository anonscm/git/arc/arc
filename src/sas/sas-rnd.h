/*
 * sas-rnd.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_RND_H
# define SAS_RND_H

# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <sas/sas-common.h>

BEGIN_C_DECLS

typedef struct sas_random_generator sas_prng;
typedef enum sas_prng_kind {
  SAS_PRNG_ERARD_DEGUENON,
  SAS_PRNG_MARSAGLIA_KISS_SWB,
  SAS_PRNG_MARSAGLIA_KISS_LFIB4
} sas_prng_kind;

typedef struct sas_prng_state sas_prng_state;

extern sas_prng *
sas_prng_create (sas_prng_kind kind, uint32_t seed);

extern void 
sas_prng_create_parallel (sas_prng_kind kind, uint32_t seed, int N,
			  sas_prng **prngs);

extern void
sas_prng_delete (sas_prng *prng);

extern sas_uint
sas_prng_modulo (sas_prng *prng, sas_uint modvalue);

extern sas_double
sas_prng_probability (sas_prng *prng);

extern sas_prng_state *
sas_prng_get_state (sas_prng *prng);

extern void
sas_prng_set_state (sas_prng *prng, sas_prng_state *state);

extern void
sas_prng_delete_state (sas_prng_state *state);

extern sas_double
sas_prng_normal (sas_prng *prng,
		 sas_double mean, sas_double standard_deviation);

extern sas_double
sas_prng_uniform (sas_prng *prng,
		  sas_double min, sas_double max);

extern uint32_t
sas_prng_get_counter (sas_prng *prng);

END_C_DECLS

#endif /* ! SAS_RND_H */
