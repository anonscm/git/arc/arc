/*
 * sas.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_H
# define SAS_H

# include <time.h>
# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <sas/sas-model.h>
# include <sas/sas-rnd.h>

BEGIN_C_DECLS

typedef struct sas_simulator sas_simulator;
typedef struct sas_simulation_parameters sas_simulation_parameters;
enum sas_scheduler_type {
  SAS_SCH_DLINK,
  SAS_SCH_CQ
};

struct sas_simulation_parameters
{
  sas_int number_of_tries; /* number of simulation for uncertainty evaluation */
  sas_time duration; /* length in time unit of a story */
  sas_int number_of_stories; /* number of execution to simulate */
  sas_prng_kind prng; /* PRNG to use */
  uint32_t seed; /* seed of the PRNG */
  int loop_length; /* if the number of consecutive immediate transition is 
		    * greater that this parameter, we consider the system 
		    * entered an infinite loop.
		    */
  int timeout; /* budget of seconds allowed for the whole simulation */
  
  int (*stop_simulation)(void *stop_data); /* This callback is called at the 
					    * end of each story. It should 
					    * returns a non-null value if the
					    * user decides to interrupt the 
					    * simulation 
					    */
  void *stop_data;
  int nb_threads;
  enum sas_scheduler_type scheduler;
};

extern int sas_debug_is_on;
extern int sas_debug_sim_stories;
extern int sas_debug_sim_scheduler;
extern int sas_debug_sim_sequential_threads;

extern sas_simulator *
sas_simulator_crt (sas_model *model);

extern void
sas_simulator_destroy (sas_simulator *sim);

extern int
sas_simulator_run (sas_simulator *sim, const sas_simulation_parameters *params);

extern void
sas_simulator_display_results (sas_simulator *sim, ccl_log_type log);

extern int
sas_unit_tests (ccl_log_type log);

END_C_DECLS

#endif /* ! SAS_H */
