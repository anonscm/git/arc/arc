/*
 * sas-rnd.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <math.h>
#include <limits.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "sas-rnd.h"

#define SELF(type, v) type *self = (type *)(v);
#define CSTSELF(type, v) const type *self = (const type *)(v);

typedef struct sas_random_generator_methods prng_methods;

struct sas_random_generator_methods
{
  void (*init_seed) (sas_prng *prng, sas_uint seed);
  sas_uint (*modulo) (sas_prng *prng, sas_uint modvalue);
  sas_double (*proba) (sas_prng *prng);
  void * (*get_state) (sas_prng *prng);
  void (*set_state) (sas_prng *prng, void *state);
  void (*delete_state) (void *state);
  void (*destroy)(sas_prng *prng);
};

struct sas_random_generator
{
  const prng_methods *methods;
  uint32_t nb_calls;  
};

struct sas_prng_state
{
  sas_prng *prng;
  void *state;
};

struct uint_prng
{
  sas_prng super;
  uint32_t seed;
};

struct marsaglia_state
{
  uint32_t z, w, jsr, jcong;
  uint32_t a, b, t[256];
  uint32_t x, y, bro;
  uint8_t c;
};

struct marsaglia_prng
{
  sas_prng super;
  struct marsaglia_state state;
};

static const prng_methods ED_METHODS;
static const prng_methods MKS_METHODS;
static const prng_methods MKL_METHODS;

sas_prng *
sas_prng_create (sas_prng_kind kind, uint32_t seed)
{
  size_t size;
  const prng_methods *methods = NULL;
  sas_prng *result;
  
  switch (kind) {
  case SAS_PRNG_ERARD_DEGUENON:
    size = sizeof (struct uint_prng);
    methods = &ED_METHODS;
    break;
  case SAS_PRNG_MARSAGLIA_KISS_SWB:
    size = sizeof (struct marsaglia_prng);
    methods = &MKS_METHODS;
    break;
  case SAS_PRNG_MARSAGLIA_KISS_LFIB4:
    size = sizeof (struct marsaglia_prng);
    methods = &MKL_METHODS;
    break;
  };
  
  ccl_assert (methods != NULL);
  
  result = ccl_malloc (size); 
  result->methods = methods;
  result->nb_calls = 0;
  methods->init_seed (result, seed);
  
  return result;
}

void 
sas_prng_create_parallel (sas_prng_kind kind, uint32_t seed, int N,
			  sas_prng **result)
{
  ccl_pre (N >= 1);
  
  if (N > 1)
    {
      int i;
      sas_prng *keygen;
      sas_prng_kind keykind;
      
      if (kind == SAS_PRNG_ERARD_DEGUENON)
	keykind = SAS_PRNG_MARSAGLIA_KISS_SWB;
      else
	keykind = SAS_PRNG_ERARD_DEGUENON; 
      keygen = sas_prng_create (keykind, seed);

      for (i = 0; i < N; i++, result++)
	{
	  uint32_t s = sas_prng_modulo (keygen, UINT_MAX);
	  *result = sas_prng_create (kind, (119 * i) + s);
	}
      sas_prng_delete (keygen);
    }
  else
    {
      *result = sas_prng_create (kind, seed);
    }
}

void
sas_prng_delete (sas_prng *prng)
{
  ccl_pre (prng != NULL);

  if (prng->methods->destroy != NULL)
    prng->methods->destroy (prng);
  ccl_delete (prng);
}

sas_uint
sas_prng_modulo (sas_prng *prng, sas_uint modvalue)
{
  sas_uint res;
  
  ccl_pre (prng != NULL);

  if (prng->methods->modulo != NULL)
    {
      if (prng->nb_calls < UINT32_MAX)
	prng->nb_calls++;
      res = prng->methods->modulo (prng, modvalue);
    }
  else
    {
      ccl_assert (prng->methods->proba != NULL);
      res = (sas_uint) floor (sas_prng_probability (prng) * modvalue);
    }
  return res;
}

sas_double
sas_prng_probability (sas_prng *prng)
{
  sas_double res;
  
  ccl_pre (prng != NULL);
  
  if (prng->methods->proba != NULL)
    {
      if (prng->nb_calls < UINT32_MAX)
	prng->nb_calls++;
      res = prng->methods->proba (prng);
    }
  else
    {
      ccl_assert (prng->methods->modulo != NULL);
      res = (sas_double) prng->methods->modulo (prng, UINT_MAX);
      res /= (sas_double) UINT_MAX;
    }
  ccl_post (0.0 <= res && res <= 1.0);
  return res;
}

sas_prng_state *
sas_prng_get_state (sas_prng *prng)
{
  sas_prng_state *result = ccl_new (sas_prng_state);

  ccl_pre (prng != NULL);
  
  result->prng = prng;
  result->state = prng->methods->get_state (prng);
  
  return result;
}

void
sas_prng_set_state (sas_prng *prng, sas_prng_state *state)
{
  ccl_pre (prng != NULL);
  ccl_pre (state != NULL);

  prng->nb_calls = 0;
  state->prng->methods->set_state (prng, state->state);
}

void
sas_prng_delete_state (sas_prng_state *state)
{
  ccl_pre (state != NULL);
  
  state->prng->methods->delete_state (state->state);
  ccl_delete (state);
}

/*! Marsaglia polar method 
 * stddev should be < mean, no ?
 */
sas_double
sas_prng_normal (sas_prng *prng, sas_double mean, sas_double standard_deviation)
{  
  double u, v, s;
  
  do {
       u = sas_prng_probability (prng);   /* 0  <= u1 <= 1 */ 
       v = sas_prng_probability (prng);   /* 0  <= u2 <= 1 */
       u = 2 * u - 1;                    /* -1 <= v1 <= 1 */
       v = 2 * v - 1;                    /* -1 <= v2 <= 1 */
       s = u * u + v * v;
     }
  while ((s >= 1) || (s == 0));
  s = sqrt(-2.0 * log(s) / s);
  
  return (mean + standard_deviation * u * s);
}

sas_double
sas_prng_uniform (sas_prng *prng, sas_double min, sas_double max)
{
  return min + sas_prng_probability (prng) * (max - min);
}

uint32_t
sas_prng_get_counter (sas_prng *prng)
{
  ccl_pre (prng != NULL);
  
  return prng->nb_calls;
}

/*
 * Methods common for PRNGs based on a integer seed.
 */
static void
s_uint_init_seed (sas_prng *prng, sas_uint seed)
{
  SELF (struct uint_prng, prng);

  self->seed = seed;
}

static void *
s_uint_get_state (sas_prng *prng)
{
  SELF (struct uint_prng, prng);
  
  uint32_t *res = ccl_new (uint32_t);
  *res = self->seed;
  
  return res;
}

static void
s_uint_set_state (sas_prng *prng, void *state)
{
  SELF (struct uint_prng, prng);
  
  self->seed = *(uint32_t *) state;
}

static void
s_uint_delete_state (void *state)
{
  ccl_delete (state);
}

/******************************************************************************
 * The following random generator is the one proposed in
 * 
 * P.-J. Erard & P. Deguenon. Simulation par evenements discrets. 
 * Presses Polytechniques et Universitaires Romandes. Collection Informatique.
 * pp 248, ISBN 2-88074-295-1, 1996.
 ******************************************************************************/
#define THE_m ((int32_t) 2147483647)
#define THE_a ((int32_t) 16807)
#define THE_q ((int32_t) 127773)
#define THE_r ((int32_t) 2836)


#define s_ed_init_seed s_uint_init_seed
#define s_ed_modulo NULL

static uint32_t
s_ed_random (uint32_t *pseed)
{
  int32_t seed = *pseed;
  
  seed = THE_a * (seed % THE_q) - THE_r * (seed / THE_q);
  if (seed < 0)
    seed += THE_m;  
  *pseed = seed;
  
  return seed;
}

static sas_double
s_ed_proba (sas_prng *prng)
{
  SELF (struct uint_prng, prng);
  sas_double res = s_ed_random (&self->seed);
  
  res /= (sas_double) THE_m;
  
  return res;
}

#define s_ed_get_state s_uint_get_state
#define s_ed_set_state s_uint_set_state
#define s_ed_delete_state s_uint_delete_state
#define s_ed_destroy NULL

static const prng_methods ED_METHODS = {
  s_ed_init_seed,
  s_ed_modulo,
  s_ed_proba,
  s_ed_get_state,
  s_ed_set_state,
  s_ed_delete_state,
  s_ed_destroy
};

/******************************************************************************
 *
 * Implementation of the PRNGs based on George Marsaglia code he proposed on
 * Usenet forums in 1999.
 * 
 ******************************************************************************/

#define UC(v) ((unsigned char)(v))
#define gm_ZNEW(s) ((s).z=36969*((s).z&65535)+((s).z>>16))
#define gm_WNEW(s) ((s).w=18000*((s).w&65535)+((s).w>>16))
#define gm_MWC(s) ((gm_ZNEW(s)<<16)+gm_WNEW(s))
#define gm_SHR3(s) ((s).jsr^=((s).jsr<<17), (s).jsr^=((s).jsr>>13), \
		    (s).jsr^=((s).jsr<<5))
#define gm_CONG(s) ((s).jcong=69069*(s).jcong+1234567)
#define gm_FIB(s) (((s).b=(s).a+(s).b),((s).a=(s).b-(s).a))
#define gm_KISS(s) ((gm_MWC(s)^gm_CONG(s))+gm_SHR3(s))
#define gm_LFIB4(s) ((s).c++,(s).t[(s).c]=(s).t[(s).c]+(s).t[UC((s).c+58)]+ \
		     (s).t[UC((s).c+119)]+(s).t[UC((s).c+178)])
#define gm_SWB(s) ((s).c++,(s).bro=((s).x<(s).y), \
		   (s).t[(s).c]=(((s).x=(s).t[UC((s).c+34)])-	\
				 ((s).y=(s).t[UC((s).c+19)]+(s).bro)))
#define gm_UNI(s) (gm_KISS(s)*2.328306e-10)
#define gm_VNI(s) ((long) gm_KISS(s))*4.656613e-10

static void
s_gm_set_table (struct marsaglia_state *s, uint32_t s0, uint32_t s1,
		uint32_t s2, uint32_t s3, uint32_t s4, uint32_t s5)
{
  int i;
  s->z = s0;
  s->w = s1;
  s->jsr = s2;
  s->jcong = s3;
  s->a = s4;
  s->b = s5;
  for (i = 0; i < 256; i++)
    s->t[i] = gm_KISS (*s);
  s->x = s->y = s->bro = 0;
  s->c = 0;
}

static void
s_gm_init_seed (sas_prng *prng, sas_uint seed)
{
  SELF (struct marsaglia_prng, prng);
  uint32_t s0 = s_ed_random (&seed);
  uint32_t s1 = s_ed_random (&seed);
  uint32_t s2 = s_ed_random (&seed);
  uint32_t s3 = s_ed_random (&seed);
  uint32_t s4 = s_ed_random (&seed);
  uint32_t s5 = s_ed_random (&seed);
  
  s_gm_set_table (&(self->state), s0, s1, s2, s3, s4, s5);
}

#define s_gm_modulo NULL

static sas_double
s_gm_KISS_SWB_proba (sas_prng *prng)
{
  SELF (struct marsaglia_prng, prng);
  sas_double val = gm_KISS (self->state) + gm_SWB (self->state);
  val /= UINT32_MAX;

  return val;
}

static sas_double
s_gm_KISS_LFIB4_proba (sas_prng *prng)
{
  SELF (struct marsaglia_prng, prng);
  sas_double val = gm_KISS (self->state) + gm_LFIB4 (self->state);
  val /= UINT32_MAX;

  return val;
}

static void *
s_gm_get_state (sas_prng *prng)
{
  SELF (struct marsaglia_prng, prng);
  void *result = ccl_new (struct marsaglia_state);
  ccl_memcpy (result, &self->state, sizeof (struct marsaglia_state));
  
  return result;
}

static void
s_gm_set_state (sas_prng *prng, void *state)
{
  SELF (struct marsaglia_prng, prng);

  ccl_memcpy (&self->state, state, sizeof (struct marsaglia_state));
}

static void
s_gm_delete_state (void *state)
{
  ccl_delete (state);
}

#define s_gm_destroy NULL

#if 0
#define UC (unsigned char) /*a cast operation*/
typedef unsigned long UL;
/* Global static variables: */
static UL z=362436069, w=521288629, jsr=123456789, jcong=380116160;
static UL a=224466889, b=7584631, t[256];
/* Use random seeds to reset z,w,jsr,jcong,a,b, and the table
t[256]*/
static UL x=0,y=0,bro; static unsigned char c=0;
/* Example procedure to set the table, using KISS: */
void settable(UL i1,UL i2,UL i3,UL i4,UL i5, UL i6)
{ int i; z=i1;w=i2,jsr=i3; jcong=i4; a=i5; b=i6;
for(i=0;i<256;i=i+1) t[i]=KISS;
}
/* This is a test main program. It should compile and print 7
0's. */
int main(void){
int i; UL k;
settable(12345,65435,34221,12345,9983651,95746118);
for(i=1;i<1000001;i++){k=LFIB4;} printf("%u\n", k-1064612766U);
for(i=1;i<1000001;i++){k=SWB ;} printf("%u\n", k- 627749721U);
for(i=1;i<1000001;i++){k=KISS ;} printf("%u\n", k-1372460312U);
for(i=1;i<1000001;i++){k=CONG ;} printf("%u\n", k-1529210297U);
for(i=1;i<1000001;i++){k=SHR3 ;} printf("%u\n", k-2642725982U);
for(i=1;i<1000001;i++){k=MWC ;} printf("%u\n", k- 904977562U);
for(i=1;i<1000001;i++){k=FIB ;} printf("%u\n", k-3519793928U);
}

#endif

static const prng_methods MKS_METHODS = {
  s_gm_init_seed,
  s_gm_modulo,
  s_gm_KISS_SWB_proba,
  s_gm_get_state,
  s_gm_set_state,
  s_gm_delete_state,
  s_gm_destroy
};

static const prng_methods MKL_METHODS = {
  s_gm_init_seed,
  s_gm_modulo,
  s_gm_KISS_LFIB4_proba,
  s_gm_get_state,
  s_gm_set_state,
  s_gm_delete_state,
  s_gm_destroy
};

#if 0
sas_int 
sas_rnd_modulo (sas_int modvalue)
{
  return sas_rnd_modulo_gen (&THE_SEED, modvalue);
}

sas_proba
sas_rnd_probability (void)
{
  return sas_rnd_probability_gen (&THE_SEED);
}

sas_proba
sas_rnd_normal (sas_double mean, sas_double standard_deviation)
{
  return sas_rnd_normal_gen (&THE_SEED, mean, standard_deviation);
}

sas_proba
sas_rnd_uniform (sas_double min, sas_double max)
{
  return sas_rnd_uniform_gen (&THE_SEED, min, max);
}

sas_proba
sas_rnd_proba_callback (void *data)
{
  return sas_rnd_proba_callback_gen (&THE_SEED);
}

void
sas_rnd_set_seed (sas_int seed)
{
  THE_SEED = seed;
}

sas_double
sas_rnd_get_seed (void)
{
  return (sas_double) THE_SEED;
}

			/* --------------- */

sas_int
sas_rnd_modulo_gen (sas_int *seed, int modvalue)
{
  *seed = THE_a * (*seed % THE_q) - THE_r * (*seed / THE_q);
  if (*seed < 0)
    *seed += THE_m;
  return (*seed % modvalue);
}

sas_proba
sas_rnd_probability_gen (sas_int *seed)
{
  sas_proba result;

  *seed = THE_a * (*seed % THE_q) - THE_r * (*seed / THE_q);
  if (*seed < 0)
    *seed += THE_m;
  result = ( ((sas_double) *seed) / ((sas_double) THE_m) );

  return result;
}

sas_proba
sas_rnd_normal_gen (sas_int *seed, sas_double mean,
		    sas_double standard_deviation)
{
}

sas_proba
sas_rnd_uniform_gen (sas_int *seed, sas_double min, sas_double max)
{
  return min + sas_rnd_probability_gen (seed) * (max - min);
}

sas_proba
sas_rnd_proba_callback_gen (void *seed_addr)
{
  return sas_rnd_probability_gen (seed_addr);
}
#endif 
