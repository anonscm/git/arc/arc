/*
 * sas-scheduler-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_SCHEDULER_P_H
# define SAS_SCHEDULER_P_H

#include "sas-scheduler.h"

typedef struct sas_scheduler_methods_st sas_scheduler_methods;
struct sas_scheduler_methods_st
{
  void (*destroy) (sas_scheduler *sch);
  void (*make_empty)(sas_scheduler *sch);
  void (*start_insertion) (sas_scheduler *sch);
  void (*insert) (sas_scheduler *sch, sas_event *e);
  void (*end_insertion) (sas_scheduler *sch);
  sas_event * (*take_first) (sas_scheduler *sch);
  void (*remove) (sas_scheduler *sch, sas_event *e);
  int (*is_empty) (sas_scheduler *sch);
  void (*map) (sas_scheduler *sch, void (*map)(sas_event *e, void *data),
	       void *data);
  void (*log)(sas_scheduler *sch, ccl_log_type log);
};

struct sas_scheduler_st
{
  int refcount;
  sas_priority_func *priority;
  const sas_scheduler_methods *methods;
};

extern void *
sas_scheduler_new (size_t size, sas_priority_func *priority,
		   const sas_scheduler_methods *methods);

# define SAS_SELF(type, var) type *self = ((type *) var)
# define SAS_CSTSELF(type, var) const type *self = ((const type *) var)
# define SAS_IS_BEFORE(sch,e1,e2) \
  (((e1)->firing_time < (e2)->firing_time) ||		\
   (((e1)->firing_time == (e2)->firing_time) &&		\
    ((sas_scheduler *)(sch))->priority (e1,e2)))

extern int
sas_scheduler_cmp_event (const sas_event *e1, const sas_event *e2,
			 sas_scheduler *sch);

#endif /* ! SAS_SCHEDULER_P_H */
