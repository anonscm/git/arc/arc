/*
 * sas-scheduler.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_SCHEDULER_H
# define SAS_SCHEDULER_H

# include <ccl/ccl-log.h>
# include <sas/sas-common.h>

BEGIN_C_DECLS

typedef struct sas_scheduler_st sas_scheduler;
typedef struct sas_event_st sas_event;
struct sas_event_st
{
  sas_event *prev;
  sas_event *next;
  sas_double firing_time;
  int active;
};

typedef int sas_priority_func (const sas_event *e1, const sas_event *e2);

extern sas_scheduler *
sas_scheduler_create_double_linked_list (sas_priority_func *priority);

extern sas_scheduler *
sas_scheduler_create_calendar_queue (sas_priority_func *priority,
				     int nb_events);

extern sas_scheduler * 
sas_scheduler_add_reference (sas_scheduler *sch);

extern void
sas_scheduler_del_reference (sas_scheduler *sch);

extern void
sas_scheduler_make_empty (sas_scheduler *sch);

extern void
sas_scheduler_start_insertion (sas_scheduler *sch);

extern void
sas_scheduler_insert (sas_scheduler *sch, sas_event *e);

extern void
sas_scheduler_end_insertion (sas_scheduler *sch);

extern sas_event *
sas_scheduler_take_first (sas_scheduler *sch);

extern void
sas_scheduler_remove (sas_scheduler *sch, sas_event *e);

extern int 
sas_scheduler_is_empty (sas_scheduler *sch);

extern void
sas_scheduler_map (sas_scheduler *sch,
		   void (*map) (sas_event *e, void *data),
		   void *data);

extern void
sas_event_reset_ (sas_event *e);

# define sas_event_reset(e) (sas_event_reset_ ((sas_event *)(e)))
# define sas_event_get_firing_time(e) (((sas_event *)(e))->firing_time)
# define sas_event_set_firing_time(e, t) (((sas_event *)(e))->firing_time = (t))
# define sas_event_is_active(e) (((sas_event *)(e))->active)

extern void
sas_scheduler_log (ccl_log_type log, sas_scheduler *sch);

END_C_DECLS

#endif /* ! SAS_SCHEDULER_H */
