/*
 * sas-model.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_MODEL_H
# define SAS_MODEL_H

# include <ccl/ccl-iterator.h>
# include <sas/sas-laws.h>

typedef struct sas_model sas_model;
typedef struct sas_model_state sas_model_state;
typedef struct sas_action sas_action;
typedef struct sas_observer sas_observer;

typedef void sas_map_action_proc (sas_model *m, sas_action *act, int actindex,
				  int is_enabled, void *data);
typedef void sas_map_observer_proc (sas_model *m, sas_observer *act,
				    int obsindex, void *data);

struct sas_model
{
  sas_model_state *(*crt_state) (sas_model *model);
  void (*delete_state) (sas_model *model, sas_model_state *state);
  
  sas_action * const * (*get_actions) (sas_model *model, int *p_size);
  int (*get_action_priority)(sas_model *model, sas_action *act);
  int (*has_memory) (sas_model *model, sas_action *act);
  void (*map_dependent_observers) (sas_model *model, sas_action *act,
				   sas_map_observer_proc *map, void *data);

  int (*trigger_action) (sas_model *model, sas_model_state *state,
			 sas_action *action, sas_prng *prng);
  
  void (*map_touched_actions) (sas_model *model, sas_action *act,
			       sas_model_state *state, 
			       sas_map_action_proc *map, void *data);
  
  sas_law *(*get_action_law) (sas_model *model, sas_action *action);
  int (*is_enabled) (sas_model *model, sas_model_state *state, sas_action *act);
  const char *(*get_action_desc)(sas_model *model, sas_action *act);
  
  void (*set_initial_state)(sas_model *model, sas_model_state *state);

  sas_observer * const * (*get_observers) (sas_model *model, int *p_size);
  int (*observer_is_boolean) (sas_model *model, sas_observer *obs);
  
  sas_double (*get_observer_value) (sas_model *model, sas_model_state *state,
				    sas_observer *obs);
  
  const char *(*get_observer_desc)(sas_model *model, sas_observer *obs);

  int (*error)(sas_model *model);
  void (*log_error)(sas_model *model, ccl_log_type log);
};

#endif /* ! SAS_MODEL_H */
