/*
 * sas-unit-tests.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <math.h>

#include "sas-formats.h"
#include "sas-laws.h"
#include "sas-rnd.h"
#include "sas.h"

#define EPSILON ((sas_double)1e-6)
#define LAMBDA1 1e-4
#define LAMBDA2 1e-5
#define DELAY1 1e4
#define DELAY2 1e5

static int
s_reach_epsilon (sas_double v, sas_double expected)
{
  sas_double t = fabsl (v - expected) / expected;
  
  return t < EPSILON;
}

static void
s_compute_mean_of_law (ccl_log_type log, sas_law *l, sas_prng *prng)
{
  sas_int i = 0;
  sas_double total = 0.0;
  sas_double mean = sas_law_get_expected_value (l);
  sas_double m;

  ccl_log (log, "mean computation of:");
  sas_law_log (log, l);
  ccl_log (log, "\n"); 

  do
    {
      i++;
      total += sas_law_compute_delay (l, 0.0, prng, NULL);
      m = total/(sas_double) i;
    }
  while (! s_reach_epsilon (m, mean));
  ccl_log (log, "get mean (" SAS_DEFAULT_FORMAT ") after %d iterations\n",
	   m, i);  
}

static void
s_test_mean (ccl_log_type log, sas_law *(*crt)(), size_t arity,
	     sas_double *params, size_t nb_params)
{
  size_t i;
  sas_law_param **lp;
  ccl_pre (1 <= arity && arity <= 3);
  sas_prng *prng = sas_prng_create (SAS_PRNG_ERARD_DEGUENON, 1);
  
  lp = ccl_new_array (sas_law_param *, nb_params);
  for (i = 0; i < nb_params; i += arity)
    lp[i] = sas_law_param_crt_constant (params[i]);
  
  for (i = 0; i < nb_params; i += arity)
    {
      sas_law *l = sas_law_crt_dirac (lp[0]);

      if (arity == 1)
	l = crt (lp[i]);
      else if (arity == 2)
	l = crt (lp[i], lp[i + 1]);
      else
	l = crt (lp[i], lp[i + 1], lp[i + 2]);

      s_compute_mean_of_law (log, l, prng);
      sas_law_del_reference (l);
    }
  for (i = 0; i < nb_params; i += arity)
    sas_law_param_del_reference (lp[i]);
  sas_prng_delete (prng);
  ccl_delete (lp);
}

int 
sas_unit_tests (ccl_log_type log)
{
  ccl_log (log, "SAS unit tests\n");
  ccl_log (log, " Checking laws\n");
  {
    sas_double params[] = { LAMBDA1, LAMBDA2 }; 
    s_test_mean (log, sas_law_crt_exponential, 1, params, 2);
  }
  
  {
    sas_double params[] = { DELAY1, DELAY2 }; 
    s_test_mean (log, sas_law_crt_dirac, 1, params, 2);
  }

  {
    sas_double params[] = { LAMBDA2, LAMBDA1, DELAY1, DELAY2 }; 
    s_test_mean (log, sas_law_crt_uniform, 2, params, 4);
  }
  
  {
    sas_double params[] = { LAMBDA2, (sas_double) 2, DELAY1, (sas_double) 3 }; 
    s_test_mean (log, sas_law_crt_erlang, 2, params, 4);
  }
  
  return 1;
}
