/*
 * sas-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_P_H
# define SAS_P_H

# include "sas.h"

# define SAS_DBG_IS_ON (ccl_debug_is_on && sas_debug_is_on)
# define SAS_DBG(f_) (SAS_DBG_IS_ON && sas_debug_ ## f_)
  
# define SAS_DBG_C_START_TIMER(cond_,args_) \
  CCL_DEBUG_COND_START_TIMED_BLOCK (cond_, args_)
# define SAS_DBG_C_END_TIMER(cond_) \
  CCL_DEBUG_COND_END_TIMED_BLOCK (cond_)

# define SAS_DBG_F_START_TIMER(f_,args_) \
  SAS_DBG_C_START_TIMER (SAS_DBG (f_), args_) 
# define SAS_DBG_F_END_TIMER(f_) SAS_DBG_C_END_TIMER (SAS_DBG (f_))

# define SAS_DBG_START_TIMER(args_) SAS_DBG_C_START_TIMER (SAS_DBG_IS_ON, args_)
# define SAS_DBG_END_TIMER() SAS_DBG_C_END_TIMER (SAS_DBG_IS_ON)

#endif /* ! SAS_P_H */
