/*
 * sas-scheduler.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "sas-scheduler-p.h"


void *
sas_scheduler_new (size_t size, sas_priority_func *priority, 
		   const sas_scheduler_methods *methods)
{
  sas_scheduler *result = ccl_calloc (size, 1);

  result->refcount = 1;
  result->priority = priority;
  result->methods = methods;

  return result;
}

sas_scheduler *
sas_scheduler_add_reference (sas_scheduler *sch)
{
  ccl_pre (sch != NULL);
  
  sch->refcount++;
  
  return sch;
}

void
sas_scheduler_del_reference (sas_scheduler *sch)
{
  ccl_pre (sch != NULL);
  
  sch->refcount--;
  if (sch->refcount == 0)
    {
      if (sch->methods->destroy)
	sch->methods->destroy (sch);
      ccl_delete (sch);
    }
}

void
sas_scheduler_make_empty (sas_scheduler *sch)
{
  ccl_pre (sch != NULL);

  sch->methods->make_empty (sch);
}

void
sas_scheduler_start_insertion (sas_scheduler *sch)
{
  ccl_pre (sch != NULL);
  
  if (sch->methods->start_insertion)
    sch->methods->start_insertion (sch);
}

void
sas_scheduler_insert (sas_scheduler *sch, sas_event *e)
{
  ccl_pre (sch != NULL);

  sch->methods->insert (sch, e);
  e->active = 1;
}

void
sas_scheduler_end_insertion (sas_scheduler *sch)
{
  ccl_pre (sch != NULL);
  
  if (sch->methods->end_insertion)
    sch->methods->end_insertion (sch);
}

sas_event *
sas_scheduler_take_first (sas_scheduler *sch)
{
  sas_event *result;
  
  ccl_pre (! sas_scheduler_is_empty (sch));
  
  result = sch->methods->take_first (sch);
  result->active = 0;
  
  return result;
}

void
sas_scheduler_remove (sas_scheduler *sch, sas_event *e)
{
  ccl_pre (sch != NULL);
  
  sch->methods->remove (sch, e);
  e->active = 0;
}

int 
sas_scheduler_is_empty (sas_scheduler *sch)
{
  ccl_pre (sch != NULL);
  
  return sch->methods->is_empty (sch);
}

void
sas_scheduler_map (sas_scheduler *sch,
		   void (*map) (sas_event *e, void *data),
		   void *data)
{
  ccl_pre (sch != NULL);

  if (sch->methods->map)
    sch->methods->map (sch, map, data);
}

void
sas_scheduler_log (ccl_log_type log, sas_scheduler *sch)
{
  ccl_pre (sch != NULL);

  if (sch->methods->log)
    sch->methods->log (sch, log);
}

int
sas_scheduler_cmp_event (const sas_event *e1, const sas_event *e2,
			 sas_scheduler *sch)
{  
  if (e1->firing_time < e2->firing_time)
    return -1;
  if (e1->firing_time == e2->firing_time)
    {
      if (sch->priority (e1, e2))
	return -1;
      if (sch->priority (e2, e1))
	return 1;
      return 0;
    }
  return 1;
}

void
sas_event_reset_ (sas_event *e)
{
  ccl_pre (e != NULL);

  e->prev = e->next = NULL;
  e->firing_time = -1.0;
  e->active = 0;
}

