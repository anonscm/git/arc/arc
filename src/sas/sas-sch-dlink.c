/*
 * sas-dlinksched.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <ccl/ccl-assert.h>
#include "sas-formats.h"
#include "sas-scheduler-p.h"

typedef struct dlink_scheduler_st dlinksched;
struct dlink_scheduler_st
{
  sas_scheduler super;
  sas_event queue;
  sas_event insertion_queue;
};

#define SELF(s) SAS_SELF(dlinksched, s)
#define CSTSELF(s) SAS_CSTSELF(dlinksched, s)

#define queue_is_empty(q) ((q)->next == q)
#define s_dlinksched_destroy NULL
#define s_dlinksched_start_insertion NULL

static void
s_dlinksched_make_empty (sas_scheduler *sch)
{
  SELF (sch);

  self->queue.next = &self->queue;
  self->queue.prev = &self->queue;
  self->insertion_queue.next = &self->insertion_queue;
  self->insertion_queue.prev = &self->insertion_queue;
}
  
static void 
s_dlinksched_insert (sas_scheduler *sch, sas_event *e)
{
  SELF (sch);
  sas_event *t;  
  sas_event *Q = &self->insertion_queue;
    
  ccl_pre (e->next == NULL); /* T should not be in the scheduler */
  
  if (queue_is_empty (Q)) /* scheduler is empty */
    {
      Q->next = Q->prev = e;
      e->next = e->prev = Q;
    }
  else if (SAS_IS_BEFORE (sch, e, Q->next)) /* T becomes the first action */
    {
      e->next = Q->next;
      e->prev = Q;
      Q->next = e;
      e->next->prev = e;
    }
  else if (SAS_IS_BEFORE (sch, Q->prev, e)) /* T becomes the last action */
    {
      e->next = Q;
      e->prev = Q->prev;
      Q->prev = e;
      e->prev->next = e;
    }
  else
    {
      ccl_assert (Q->next->next != Q);
      for (t = Q->next->next; t != Q; t = t->next)
	if (SAS_IS_BEFORE (sch, e, t))
	  break;
      ccl_assert (t != Q);
      e->next = t;
      e->prev = t->prev;
      t->prev = e;
      e->prev->next = e;
    }
}
  
static void 
s_dlinksched_end_insertion (sas_scheduler *sch)
{
  sas_event *aux;
  sas_event *t1;
  sas_event *t2;
  SELF (sch);
  sas_event *Q1 = &self->queue;
  sas_event *Q2 = &self->insertion_queue;

  if (queue_is_empty (Q2))
    return;
  
  if (queue_is_empty (Q1))
    {
      Q1->next = Q2->next;
      Q1->prev = Q2->prev;
      Q2->next->prev = Q1;
      Q2->prev->next = Q1;
    }
  else if (SAS_IS_BEFORE (sch, Q2->prev, Q1->next))
    { /* put Q2 at the beginning */
      Q2->prev->next = Q1->next;
      Q1->next->prev = Q2->prev;
      Q1->next = Q2->next;
      Q2->next->prev = Q1;
    }
  else if (SAS_IS_BEFORE (sch, Q1->prev, Q2->next)) 
    { /* put Q2 at the end */
      Q1->prev->next = Q2->next;
      Q2->next->prev = Q1->prev;
      Q2->prev->next = Q1;
      Q1->prev = Q2->prev;      
    }
  else
    {
      t1 = Q1->next;
      t2 = Q2->next;
      
      while (t2 != Q2 && t1 != Q1)
	{
	  if (SAS_IS_BEFORE (sch, t2, t1))
	    {
	      aux = t2->next;
	      t2->next = t1;
	      t2->prev = t1->prev;
	      t1->prev->next = t2;
	      t1->prev = t2;
	      t2 = aux;
	    }
	  else
	    {
	      t1 = t1->next;
	    }
	}
      if (t2 != Q2) 
	{ /* put the remain of Q2 at the end */
	  t2->prev = Q1->prev;
	  Q1->prev->next = t2;
	  Q1->prev = Q2->prev;
	  Q2->prev->next = Q1;
	}
    }
  /* make Q2 empty */
  Q2->next = Q2->prev = Q2;
}

static sas_event * 
s_dlinksched_take_first (sas_scheduler *sch)
{
  SELF (sch);
  sas_event *result = self->queue.next;
  sas_scheduler_remove (sch, result);
  
  return result;
}

static void 
s_dlinksched_remove (sas_scheduler *sch, sas_event *e)
{  
  e->next->prev = e->prev;
  e->prev->next = e->next;
  e->prev = e->next = NULL;
}

static int 
s_dlinksched_is_empty (sas_scheduler *sch)
{
  SELF (sch);
  
  return queue_is_empty (&self->queue);
}

static void 
s_dlinksched_map (sas_scheduler *sch, void (*map)(sas_event *e, void *data),
		  void *data)
{
  SELF (sch);
  sas_event *e = self->queue.next;
  sas_event *end = &self->queue;
  
  for (; e != end; e = e->next)
    map (e, data);
}

static int
s_queue_size (sas_event *e, sas_event *end)
{
  int result = 0;
  
  for (; e != end; e = e->next)
    result++;

  return result;
}

static void
s_dlinksched_log (sas_scheduler *sch, ccl_log_type log)
{
  sas_event *e;
  sas_event *end;
  
  SELF (sch);
  end = &self->queue;
  e = end->next;
  ccl_log (log, "scheduler:\n");
  ccl_log (log, " type      : double linked list\n");
  ccl_log (log, " size      : %d\n", s_queue_size (e, end));
  ccl_log (log, " structure :\n");
  for (; e != end; e = e->next)
    {
      ccl_log (log, " %p %" SAS_DOUBLE_MODIFIER "e", e, e->firing_time);
      if (!e->active)
	ccl_log (log, " (WARNING: inactive event)");
      ccl_log (log, "\n");
    }
}

static const sas_scheduler_methods DLINKSCHED_METHODS = {
  s_dlinksched_destroy,
  s_dlinksched_make_empty,
  s_dlinksched_start_insertion, 
  s_dlinksched_insert,
  s_dlinksched_end_insertion,
  s_dlinksched_take_first, 
  s_dlinksched_remove, 
  s_dlinksched_is_empty,
  s_dlinksched_map,
  s_dlinksched_log
};
  
sas_scheduler *
sas_scheduler_create_double_linked_list (sas_priority_func *priority)
{
  sas_scheduler *result =
    sas_scheduler_new (sizeof (dlinksched), priority, &DLINKSCHED_METHODS);
  SELF (result);
  
  sas_event_reset (&self->queue);
  sas_event_reset (&self->insertion_queue);
  sas_scheduler_make_empty (result);
  
  return result;
}
