/*
 * sas-laws.h -- SAS Probability Laws
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_LAWS_H
# define SAS_LAWS_H

# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-list.h>
# include <sas/sas-common.h>
# include <sas/sas-rnd.h>
# include <sas/sas-params.h>

BEGIN_C_DECLS

typedef union sas_law sas_law;
typedef struct sas_conditional_law sas_conditional_law;

/*!
 * Enumeration of implemented laws
 */
#define SAS_LAWS \
  SAS_LAW(dirac)   \
  SAS_LAW(empiric) \
  SAS_LAW(erlang) \
  SAS_LAW(exponential) \
  SAS_LAW(exponential_wow) \
  SAS_LAW(gen_erlang) \
  SAS_LAW(ifa) \
  SAS_LAW(ipa) \
  SAS_LAW(nlog) \
  SAS_LAW(optional) \
  SAS_LAW(triangle) \
  SAS_LAW(truncated_weibull) \
  SAS_LAW(uniform) \
  SAS_LAW(weibull)

extern sas_law *
sas_law_crt_dirac_cst (sas_double delay);

extern sas_law *
sas_law_crt_dirac (sas_law_param *delay);

extern sas_law *
sas_law_crt_dirac_zero (void);

/*!
 * histo contains nb_classes + 1 values.
 */
extern sas_law *
sas_law_crt_empiric_cst (int nb_classes, const sas_double *histo);

extern sas_law *
sas_law_crt_empiric (int nb_classes, sas_law_param * const *histo);

extern sas_law *
sas_law_crt_erlang_cst (sas_double mean, sas_double beta);

extern sas_law *
sas_law_crt_erlang (sas_law_param *mean, sas_law_param *beta);

extern sas_law *
sas_law_crt_exponential_cst (sas_double fault_rate);

extern sas_law *
sas_law_crt_exponential (sas_law_param *fault_rate);

extern sas_law *
sas_law_crt_exponential_wow_cst (sas_double fault_rate,
				 const sas_double *delays);

extern sas_law *
sas_law_crt_exponential_wow (sas_law_param *fault_rate,
			     sas_law_param * const *delays);

extern sas_law *
sas_law_crt_generalized_erlang_cst (sas_int order, const sas_double *lambdas);

extern sas_law *
sas_law_crt_generalized_erlang (sas_int order, sas_law_param * const *lambdas);

extern sas_law *
sas_law_crt_ifa_cst (sas_delay delay, sas_time first_time);

extern sas_law *
sas_law_crt_ifa (sas_law_param *delay, sas_law_param *first_time);

extern sas_law *
sas_law_crt_ipa_cst (sas_double delay);

extern sas_law *
sas_law_crt_ipa (sas_law_param *delay);

extern sas_law *
sas_law_crt_nlog_cst (sas_double mean, sas_double error_factor);

extern sas_law *
sas_law_crt_nlog (sas_law_param *mean, sas_law_param *error_factor);

extern sas_law *
sas_law_crt_optional_law (ccl_list *cond_laws);

extern sas_law *
sas_law_crt_triangle_cst (sas_double min, sas_double max, sas_double mode);

extern sas_law *
sas_law_crt_triangle (sas_law_param *min, sas_law_param *max,
		      sas_law_param *mode);

extern sas_law *
sas_law_crt_truncated_weibull_cst (sas_double mean, sas_double beta,
				   sas_double age);

extern sas_law *
sas_law_crt_truncated_weibull (sas_law_param *mean, sas_law_param *beta,
			       sas_law_param *age);

extern sas_law *
sas_law_crt_uniform_cst (sas_time min, sas_time max);

extern sas_law *
sas_law_crt_uniform (sas_law_param *min, sas_law_param *max);

extern sas_law *
sas_law_crt_weibull_cst (sas_double mean, sas_double beta);

extern sas_law *
sas_law_crt_weibull (sas_law_param *mean, sas_law_param *beta);

extern sas_law *
sas_law_add_reference (sas_law *l);

extern void
sas_law_del_reference (sas_law *law);

extern void
sas_law_update_params (sas_law *law, sas_prng *prng);

extern sas_delay
sas_law_compute_delay (const sas_law *l, sas_time current_time,
		       sas_prng *prng, void *runtime_data);

extern sas_double
sas_law_get_expected_value (const sas_law *law);

extern void
sas_law_log (ccl_log_type log, const sas_law *law);

extern void
sas_law_log_gen (ccl_log_type log, const sas_law *law,
		 void (*idlog) (ccl_log_type log, const ar_identifier *id));
  
extern sas_law *
sas_law_instantiate (sas_law *law, ar_identifier *prefix,
		     void *(*instantiate_cond_data) (void *cond_data,
						     ar_identifier *prefix,
						     void *cbdata),
		     void *cbdata);
		     

extern sas_law *
sas_law_substitute_parameters (sas_law *law,
			       sas_law_param *(*subst) (sas_law_param *p,
							void *data),
			       void *data,
			       sas_conditional_law *(*substcond) (void *cond,
								  sas_law *law,
								  void *cbdata),
			       void *cbdata);

extern int
sas_law_is_optional (sas_law *law);

extern void
sas_law_map_conditionals (sas_law *law,
			  void (*map) (void *cond, sas_law *l, void *mapdata),
			  void *mapdata);
			  
extern sas_conditional_law *
sas_conditional_law_crt (sas_law *law,
			 int (*cond)(void *data, void *runtime_data),
			 ccl_to_string_func *to_string_data, 
			 ccl_delete_proc *del_data,
			 void *data);

extern void
sas_conditional_law_destroy (sas_conditional_law *cl);

END_C_DECLS

#endif /* ! SAS_LAWS_H */
