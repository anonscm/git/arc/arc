/*
 * sas-laws.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <math.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "sas-formats.h"
#include "sas-laws.h"

/*!
 * Some usefull math functions
 */
#define FMOD(x,y) (fmod ((x),(y)))
#define LN(x) log(x)
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)<(b))?(b):(a))
#define SQR(x) ((x)*(x))
#define PI M_PI

#define LAW_DISPLAY_METHOD_NAME(arg) law_ ## arg ## _display
#define LAW_DISPLAY_PROTO(arg) \
  void arg (ccl_log_type log, const sas_law *l, \
	    void (*idlog) (ccl_log_type log, const ar_identifier *id))
#define LAW_DISPLAY(arg) LAW_DISPLAY_PROTO(LAW_DISPLAY_METHOD_NAME(arg))

#define LAW_DESTROY_METHOD_NAME(arg) law_ ## arg ## _destroy
#define LAW_DESTROY_PROTO(arg) \
  void arg (sas_law *l)
#define LAW_DESTROY(arg) LAW_DESTROY_PROTO(LAW_DESTROY_METHOD_NAME(arg))

#define LAW_EXPECTED_VALUE_METHOD_NAME(arg) law_ ## arg ## _expected_value
#define LAW_EXPECTED_VALUE_PROTO(arg) \
  sas_double arg (const sas_law *l)
#define LAW_EXPECTED_VALUE(arg) \
  LAW_EXPECTED_VALUE_PROTO(LAW_EXPECTED_VALUE_METHOD_NAME(arg))

#define LAW_COMPUTE_DELAY_METHOD_NAME(arg) law_ ## arg ## _compute_delay
#define LAW_COMPUTE_DELAY_PROTO(arg) \
  sas_delay arg (const sas_law *l, sas_time current_time, \
		 sas_prng *prng, void *runtime_data)
#define LAW_COMPUTE_DELAY(arg) \
  LAW_COMPUTE_DELAY_PROTO(LAW_COMPUTE_DELAY_METHOD_NAME(arg))

#define LAW_UPDATE_PARAMS_METHOD_NAME(arg) law_ ## arg ## _update_params
#define LAW_UPDATE_PARAMS_PROTO(arg) \
  void arg (sas_law *l, sas_prng *prng)
#define LAW_UPDATE_PARAMS(arg) \
  LAW_UPDATE_PARAMS_PROTO(LAW_UPDATE_PARAMS_METHOD_NAME(arg))

#define LAW_SUBSTITUTE_METHOD_NAME(arg) law_ ## arg ## _substitute
#define LAW_SUBSTITUTE_PROTO(arg) \
  sas_law * arg (sas_law *l, \
		 sas_law_param *(*subst) (sas_law_param *p, void *data), \
		 void *data, \
		 sas_conditional_law *(*substcond) (void *cond_data, \
						    sas_law *law,    \
						    void *cbdata),   \
		 void *cbdata)

#define LAW_SUBSTITUTE(arg) \
  LAW_SUBSTITUTE_PROTO(LAW_SUBSTITUTE_METHOD_NAME(arg))


struct law_methods
{
  LAW_DISPLAY_PROTO((*display));
  LAW_DESTROY_PROTO((*destroy));
  LAW_COMPUTE_DELAY_PROTO((*compute_delay));
  LAW_UPDATE_PARAMS_PROTO((*update_params));
  LAW_EXPECTED_VALUE_PROTO((*expected_value));
  LAW_SUBSTITUTE_PROTO((*substitute));
};

struct base_law
{
  int refcount;
  const struct law_methods *methods;
};

struct dirac_law
{
  struct base_law super;
  sas_law_param *p_delay;
  sas_delay delay;
};

struct empiric_law
{
  struct base_law super;
  int nb_classes;
  sas_law_param **p_classes;
  sas_delay *classes;
};

struct erlang_law
{
  struct base_law super;
  sas_law_param *p_mean;
  sas_double mean;
  sas_law_param *p_beta;
  sas_double beta;
  sas_double alpha_inv;
};

struct exponential_law
{
  struct base_law super;
  sas_law_param *p_fault_rate;
  sas_double fault_rate;
};

struct exponential_wow_law
{
  struct base_law super;
  sas_law_param *p_fault_rate;
  sas_law_param **p_delays;
  sas_double fault_rate;
  sas_delay *delays;
};

struct gen_erlang_law
{
  struct base_law super;
  sas_int beta;
  sas_law_param **p_lambdas;
  sas_double *lambdas;  
};

struct ifa_law
{
  struct base_law super;
  sas_law_param *p_delay;
  sas_law_param *p_first_time;
  sas_delay delay;
  sas_time first_time;
};

struct ipa_law
{
  struct base_law super;
  sas_law_param *p_delay;
  sas_delay delay;
};

struct nlog_law
{
  struct base_law super;
  sas_law_param *p_mean;
  sas_law_param *p_error_factor;
  sas_double a;
  sas_double b;
};

struct optional_law
{
  struct base_law super;
  ccl_list *cond_laws;
};

struct triangle_law
{
  struct base_law super;
  sas_law_param *p_min;
  sas_law_param *p_max;
  sas_law_param *p_mode;
  sas_double a, b, c, fc, phi1, phi2;
};

struct truncated_weibull_law
{
  struct base_law super;
  sas_law_param *p_mean;
  sas_law_param *p_beta;
  sas_law_param *p_age;
  sas_double d0;
  sas_double d1;
  sas_double age;
};

struct uniform_law
{
  struct base_law super;
  sas_law_param *p_min_time;
  sas_law_param *p_max_time;
  sas_delay min_time, max_time, delay;
};

struct weibull_law
{
  struct base_law super;
  sas_law_param *p_mean;
  sas_law_param *p_beta;
  sas_double d0;
  sas_double d1;
};

union sas_law
{
  struct base_law base;
#define SAS_LAW(id)  struct id ## _law id;
  SAS_LAWS
#undef SAS_LAW
};

#define SAS_LAW(id)			\
  static LAW_DISPLAY (id);		\
  static LAW_DESTROY (id);		\
  static LAW_COMPUTE_DELAY (id);	\
  static LAW_UPDATE_PARAMS (id);	\
  static LAW_EXPECTED_VALUE (id);	\
  static LAW_SUBSTITUTE (id);		

SAS_LAWS
#undef SAS_LAW
		  
#define SAS_LAW(id)							\
  static const struct law_methods id ## _methods = {			\
    LAW_DISPLAY_METHOD_NAME (id),					\
    LAW_DESTROY_METHOD_NAME (id),					\
    LAW_COMPUTE_DELAY_METHOD_NAME (id),					\
    LAW_UPDATE_PARAMS_METHOD_NAME (id),					\
    LAW_EXPECTED_VALUE_METHOD_NAME (id),				\
    LAW_SUBSTITUTE_METHOD_NAME (id)					\
  };

SAS_LAWS
#undef SAS_LAW

struct sas_conditional_law
{
  sas_law *law;
  void *cond_data;
  int (*cond) (void *data, void *runtime);
  ccl_to_string_func *to_string_data;
  ccl_delete_proc *del_cond_data;
};

#define s_new_law(l) (s_new_law_ (sizeof (struct l ## _law), &l ## _methods))

#define SELF(L,type)  \
  struct type ## _law *self = (ccl_pre ((L)!= NULL), &((L)->type))

#define CSTSELF(L,type)							\
  const struct type ## _law *self = (ccl_pre ((L)!= NULL), &((L)->type))

static sas_law *
s_new_law_ (size_t law_size, const struct law_methods *methods)
{
  sas_law *result = ccl_calloc (law_size, 1);
  SELF(result, base);

  self->refcount = 1;
  self->methods = methods;
  
  return result;
}

sas_law * 
sas_law_add_reference (sas_law *l)
{
  SELF(l, base);
  
  self->refcount++;
  
  return l;
}

void
sas_law_del_reference (sas_law *l)
{
  SELF (l, base);
  
  if (self->refcount == 1)
    {
      self->methods->destroy (l);
      ccl_delete (l);
    }
  else
    self->refcount--;
}

void
sas_law_update_params (sas_law *l, sas_prng *prng)
{
  CSTSELF (l, base);
  self->methods->update_params (l, prng);
}

sas_delay
sas_law_compute_delay (const sas_law *l, sas_time current_time,
		       sas_prng *prng, void *runtime_data)
{
  sas_delay result;
  CSTSELF (l, base);
  
  result = self->methods->compute_delay (l, current_time, prng, runtime_data);
  ccl_post (result >= 0.0);
  
  return result;
}

sas_double
sas_law_get_expected_value (const sas_law *l)
{
  CSTSELF (l, base);
  
  ccl_pre (self->methods->expected_value != NULL);

  return self->methods->expected_value (l);
}

void
sas_law_log (ccl_log_type log, const sas_law *l)
{
  sas_law_log_gen (log, l, ar_identifier_log_quote);
}

void
sas_law_log_gen (ccl_log_type log, const sas_law *l,
		 void (*idlog) (ccl_log_type log, const ar_identifier *id))
{
  CSTSELF (l, base);
  
  self->methods->display (log, l, idlog);
}

static sas_law_param *
s_instantiate_param (sas_law_param *lp, void *prefix)
{
  return sas_law_param_instantiate (lp, prefix);
}

sas_law *
sas_law_instantiate (sas_law *law, ar_identifier *prefix,
		     void *(*instantiate_cond_data) (void *cond_data,
						     ar_identifier *prefix,
						     void *cbdata),
		     void *cbdata)
{
  sas_law *result =
    sas_law_substitute_parameters (law, s_instantiate_param, prefix,
				   NULL, NULL);
  if (result->base.methods == &optional_methods &&
      instantiate_cond_data != NULL)
    {
      ccl_pair *p;      
      SELF (result, optional);
      for (p = FIRST (self->cond_laws); p; p = CDR (p))
	{
	  sas_conditional_law *cl = CAR (p);
	  void *idata =instantiate_cond_data (cl->cond_data, prefix, cbdata);
	  cl->cond_data = idata;
	}
    }
  
  return result;
}

sas_law *
sas_law_substitute_parameters (sas_law *law,
			       sas_law_param *(*subst) (sas_law_param *p,
							void *data),
			       void *data,
			       sas_conditional_law *(*substcond) (void *cond,
								  sas_law *law,
								  void *cbdata),
			       void *cbdata)
  
{
  CSTSELF (law, base);
  
  return self->methods->substitute (law, subst, data, substcond, cbdata);
}

int
sas_law_is_optional (sas_law *law)
{
  CSTSELF (law, base);
  
  ccl_pre (law != NULL);

  return self->methods == &optional_methods;
}

void
sas_law_map_conditionals (sas_law *law,
			  void (*map) (void *cond, sas_law *l, void *mapdata),
			  void *mapdata)
{
  ccl_pair *p;
  CSTSELF (law, optional);
  
  ccl_pre (law != NULL);

  for (p = FIRST (self->cond_laws); p; p = CDR (p))
    {
      sas_conditional_law *cl = CAR (p);
      map (cl->cond_data, cl->law, mapdata);
    }
}

sas_conditional_law *
sas_conditional_law_crt (sas_law *law, int (*cond)(void *data,
						   void *runtime_data),
			 ccl_to_string_func *to_string_data,
			 ccl_delete_proc *del_data,
			 void *data)
{
  sas_conditional_law *result = ccl_new (sas_conditional_law);
  result->law = sas_law_add_reference (law);
  result->cond = cond;
  result->cond_data = data;
  result->to_string_data = to_string_data;
  result->del_cond_data = del_data;
  
  return result;
}

void
sas_conditional_law_destroy (sas_conditional_law *cl)
{
  ccl_pre (cl != NULL);
  
  sas_law_del_reference (cl->law);
  if (cl->del_cond_data)
    cl->del_cond_data (cl->cond_data);
  ccl_delete (cl);
}

/*!
 * DIRAC LAW 
 */
sas_law *
sas_law_crt_dirac_zero (void)
{
  return sas_law_crt_dirac_cst (0.0);
}

sas_law *
sas_law_crt_dirac_cst (sas_double delay)
{
  sas_law_param *d = sas_law_param_crt_constant (delay);
  sas_law *result = sas_law_crt_dirac (d);
  sas_law_param_del_reference (d);
  
  return result;  
}

sas_law *
sas_law_crt_dirac (sas_law_param *delay)
{
  sas_law *result = s_new_law (dirac);
  SELF (result, dirac);
  
  self->p_delay = sas_law_param_add_reference (delay);  
  
  return result;
}

static LAW_DISPLAY (dirac)
{
  CSTSELF (l, dirac);

  ccl_log (log, "dirac(");
  sas_law_param_log_gen (log, self->p_delay, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (dirac)
{
  SELF (l, dirac);

  sas_law_param_del_reference (self->p_delay);
}

static LAW_UPDATE_PARAMS (dirac)
{
  SELF (l, dirac);
  self->delay = sas_law_param_get_value (self->p_delay, prng);
}

static LAW_COMPUTE_DELAY (dirac)
{
  CSTSELF (l, dirac);
  return self->delay;
}

static LAW_EXPECTED_VALUE (dirac)
{
  CSTSELF (l, dirac);
  return self->delay;
}

static LAW_SUBSTITUTE (dirac)
{
  CSTSELF (l, dirac);
  sas_law_param *p = subst (self->p_delay, data);
  sas_law *result = sas_law_crt_dirac (p);
  sas_law_param_del_reference (p);
  
  return result;
}

/*!
 * EMPIRIC LAW
 */
sas_law *
sas_law_crt_empiric_cst (int nb_classes, const sas_double *histo)
{
  int i;
  sas_law_param **p_histo = ccl_new_array (sas_law_param *, nb_classes + 1);
  sas_law *result;
  
  for (i = 0; i <= nb_classes; i++)
    p_histo[i] = sas_law_param_crt_constant (histo[i]);
  result = sas_law_crt_empiric (nb_classes, p_histo);
  for (i = 0; i <= nb_classes; i++)
    sas_law_param_del_reference (p_histo[i]);
  ccl_delete (p_histo);
  
  return result;
}

sas_law *
sas_law_crt_empiric (int nb_classes, sas_law_param * const *histo)
{
  int i;
  sas_law *result = s_new_law (empiric);
  SELF (result, empiric);
  
  ccl_pre (nb_classes > 0);
  ccl_pre (histo != NULL);
  
  self->nb_classes = nb_classes;
  self->p_classes = ccl_new_array (sas_law_param *, nb_classes + 1);
  for (i = 0; i <= nb_classes; i++)
    self->p_classes[i] = sas_law_param_add_reference (histo[i]);  
  self->classes = ccl_new_array (sas_delay, nb_classes + 1);
    
  return result;
}

static LAW_DISPLAY (empiric)
{
  int i;
  CSTSELF (l, empiric);
  
  ccl_log (log, "empiric(%d", self->nb_classes);
  for (i = 0; i <= self->nb_classes; i++)
    {
      ccl_log (log, ", ");
      sas_law_param_log_gen (log, self->p_classes[i], idlog);
    }
  ccl_log (log, ")");
}

static LAW_DESTROY (empiric)
{
  int i;
  SELF (l, empiric);

  for (i = 0; i <= self->nb_classes; i++)
    sas_law_param_del_reference (self->p_classes[i]);
  ccl_delete (self->p_classes);
  ccl_delete (self->classes);
}

static LAW_UPDATE_PARAMS (empiric)
{
  int i;
  SELF (l, empiric);

  for (i = 0; i <= self->nb_classes; i++)
    self->classes[i] = sas_law_param_get_value (self->p_classes[i], prng);
}

static LAW_COMPUTE_DELAY (empiric)
{
  CSTSELF (l, empiric);
  sas_double Z =  sas_prng_probability (prng);
  sas_double NZ = Z * (sas_double) self->nb_classes;
  sas_double di = floor (NZ);
  sas_int i = (sas_int)(di + 1);    
  sas_delay Xi = self->classes[i - 1];
  sas_delay Xip1 = self->classes[i];   
  sas_delay result = Xi + (Xip1 - Xi) * (NZ - di);
  
  return result;
}

static LAW_EXPECTED_VALUE (empiric)
{
  ccl_unreachable ();
  
  return 0.0;
}

static LAW_SUBSTITUTE (empiric)
{
  CSTSELF (l, empiric);
  int i;
  sas_law_param **classes =
    ccl_new_array (sas_law_param *, self->nb_classes + 1);
  sas_law *result;
  
  for (i = 0; i <= self->nb_classes; i++)
    classes[i] = subst (self->p_classes[i], data);
  result = sas_law_crt_empiric (self->nb_classes, classes);
  for (i = 0; i <= self->nb_classes; i++)
    sas_law_param_del_reference (classes[i]);
  ccl_delete (classes);
  
  return result;
}

/*!
 * ERLANG LAW
 */
sas_law *
sas_law_crt_erlang_cst (sas_double mean, sas_double beta)
{
  sas_law_param *p_mean = sas_law_param_crt_constant (mean);
  sas_law_param *p_beta = sas_law_param_crt_constant (beta);
  sas_law *result = sas_law_crt_erlang (p_mean, p_beta);
  sas_law_param_del_reference (p_mean);
  sas_law_param_del_reference (p_beta);
  
  return result;
}

sas_law *
sas_law_crt_erlang (sas_law_param *mean, sas_law_param *beta)
{
  sas_law *result = s_new_law (erlang);
  SELF (result, erlang);
  
  self->p_mean = sas_law_param_add_reference (mean);
  self->p_beta = sas_law_param_add_reference (beta);
    
  return result;
}

static LAW_DISPLAY (erlang)
{
  CSTSELF (l, erlang);
  ccl_log (log, "erlang(");
  sas_law_param_log_gen (log, self->p_mean, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_beta, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (erlang)
{
  SELF (l, erlang);
  sas_law_param_del_reference (self->p_mean);
  sas_law_param_del_reference (self->p_beta);
}

static LAW_UPDATE_PARAMS (erlang)
{
  SELF (l, erlang);

  self->mean = sas_law_param_get_value (self->p_mean, prng);
  self->beta = sas_law_param_get_value (self->p_beta, prng);
  self->alpha_inv = -self->mean / self->beta;
}

static LAW_COMPUTE_DELAY (erlang)
{
  sas_double i;
  CSTSELF (l, erlang);
  sas_delay result = 1.0;
  
  for (i = 0.0; i < self->beta; i += 1.0)
    result *= sas_prng_probability (prng);
  result = self->alpha_inv * log (result);
    
  return result;
}

static LAW_EXPECTED_VALUE (erlang)
{
  CSTSELF (l, erlang);
  
  return self->mean;
}

static LAW_SUBSTITUTE (erlang)
{
  CSTSELF (l, erlang);
  sas_law_param *m = subst (self->p_mean, data);
  sas_law_param *b = subst (self->p_beta, data);
  sas_law *result = sas_law_crt_erlang (m, b);
  sas_law_param_del_reference (m);
  sas_law_param_del_reference (b);  
  
  return result;
}

/*!
 * EXPONENTIAL LAW
 */
sas_law *
sas_law_crt_exponential_cst (sas_double fault_rate)
{
  sas_law_param *p = sas_law_param_crt_constant (fault_rate);
  sas_law *result = sas_law_crt_exponential (p);
  sas_law_param_del_reference (p);
  
  return result;
}

sas_law *
sas_law_crt_exponential (sas_law_param *fault_rate)
{
  sas_law *result = s_new_law (exponential);
  SELF (result, exponential);
  
  self->p_fault_rate = sas_law_param_add_reference (fault_rate);
  
  return result;
}

static LAW_DISPLAY (exponential)
{
  CSTSELF (l, exponential);
  
  ccl_log (log, "exponential(");
  sas_law_param_log_gen (log, self->p_fault_rate, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (exponential)
{
  SELF (l, exponential);
  sas_law_param_del_reference (self->p_fault_rate);
}

static LAW_UPDATE_PARAMS (exponential)
{
  SELF (l, exponential);
  self->fault_rate = sas_law_param_get_value (self->p_fault_rate, prng);
}

static LAW_COMPUTE_DELAY (exponential)
{
  CSTSELF (l, exponential);
  sas_proba Z = sas_prng_probability (prng);  
  sas_delay result = (Z == 0.0 ? 0.0 : -logl (Z));
  result /= self->fault_rate;
  
  return result;
}

static LAW_EXPECTED_VALUE (exponential)
{
  CSTSELF (l, exponential);
  
  return 1.0 / self->fault_rate;
}

static LAW_SUBSTITUTE (exponential)
{
  CSTSELF (l, exponential);
  sas_law_param *p = subst (self->p_fault_rate, data);
  sas_law *result = sas_law_crt_exponential (p);
  sas_law_param_del_reference (p);  
  
  return result;
}

/*!
 * EXPONENTIAL + WOW LAW
 */
sas_law *
sas_law_crt_exponential_wow_cst (sas_double fault_rate,
				 const sas_double *delays)
{
  int i;
  sas_law_param *p[13];
  sas_law *result;
  
  for (i = 0; i < 12; i++)
    p[i] = sas_law_param_crt_constant (delays[i]);
  p[i] = sas_law_param_crt_constant (fault_rate);
  result = sas_law_crt_exponential_wow (p[12], p);
  for (i = 0; i < 13; i++)
    sas_law_param_del_reference (p[i]);
  
  return result;
}

sas_law *
sas_law_crt_exponential_wow (sas_law_param *fault_rate,
			     sas_law_param * const *delays)
{
  int i;
  sas_law *result = s_new_law (exponential_wow);
  SELF (result, exponential_wow);
 
  self->p_fault_rate = sas_law_param_add_reference (fault_rate);
  self->p_delays = ccl_new_array (sas_law_param *, 12);
  for (i = 0; i < 12; i++)
    self->p_delays[i] = sas_law_param_add_reference (delays[i]);
  self->delays = ccl_new_array (sas_delay, 12);
  
  return result;
}

static LAW_DISPLAY (exponential_wow)
{
  int i;
  CSTSELF (l, exponential_wow);
  
  ccl_log (log, "exponential_wow(");
  sas_law_param_log_gen (log, self->p_fault_rate, idlog);
  for (i = 0; i < 12; i++)
    {
      ccl_log (log, ", ");
      sas_law_param_log_gen (log, self->p_delays[i], idlog);
    }
    
  ccl_log (log, ")");
}

static LAW_DESTROY (exponential_wow)
{
  int i;
  SELF (l, exponential_wow);
  
  sas_law_param_del_reference (self->p_fault_rate);
  for (i = 0; i < 12; i++)
    sas_law_param_del_reference (self->p_delays[i]);
  ccl_delete (self->p_delays);
  ccl_delete (self->delays);
}

static LAW_UPDATE_PARAMS (exponential_wow)
{
  int i;
  SELF (l, exponential_wow);

  self->fault_rate = sas_law_param_get_value (self->p_fault_rate, prng);
  for (i = 0; i < 12; i++)
    self->delays[i] = sas_law_param_get_value (self->p_delays[i], prng);
}

static LAW_COMPUTE_DELAY (exponential_wow)
{
  CSTSELF (l, exponential_wow);
  sas_double Z = sas_prng_probability (prng);
  sas_int month =  (((sas_int) current_time) / ((sas_int) 730)) % 12 + 1;
  sas_delay result = self->delays[month - 1];
  
  if (Z > 0.0)
    result += (-log (Z) / self->fault_rate);
  
  return result;
}

static LAW_EXPECTED_VALUE (exponential_wow)
{
  ccl_unreachable ();
  return 0.0;
}

static LAW_SUBSTITUTE (exponential_wow)
{
  CSTSELF (l, exponential_wow);
  int i;
  sas_law_param *fr = subst (self->p_fault_rate, data);
  sas_law_param *delays[12];
  sas_law *result;
  
  for (i = 0; i < 12; i++)
    delays[i] = subst (self->p_delays[i], data);
  result = sas_law_crt_exponential_wow (fr, delays);
  for (i = 0; i < 12; i++)
    sas_law_param_del_reference (delays[i]);
  sas_law_param_del_reference (fr);
  
  return result;
}

/*!
 * GENERALIZED ERLANG LAW
 */
sas_law *
sas_law_crt_generalized_erlang_cst (sas_int beta, const sas_double *lambdas)
{
  sas_int i;
  sas_law_param **p = ccl_new_array (sas_law_param *, beta);
  sas_law *result;
  
  for (i = 0; i < beta; i++)
    p[i] = sas_law_param_crt_constant (lambdas[i]);
  result = sas_law_crt_generalized_erlang (beta, p);
  for (i = 0; i < beta; i++)
    sas_law_param_del_reference (p[i]);
  ccl_delete (p);
  
  return result;
}

sas_law *
sas_law_crt_generalized_erlang (sas_int beta, sas_law_param * const *lambdas)
{
  sas_int i;
  sas_law *result = s_new_law (gen_erlang);
  SELF (result, gen_erlang);

  ccl_pre (beta > 0);
  
  self->beta = beta;
  self->p_lambdas =  ccl_new_array (sas_law_param *, beta);
  for (i = 0; i < beta; i++)
    self->p_lambdas[i] = sas_law_param_add_reference (lambdas[i]);
  self->lambdas =  ccl_new_array (sas_double, beta);
 
  return result;
}

static LAW_DISPLAY (gen_erlang)
{
  int i;
  CSTSELF (l, gen_erlang);
  
  ccl_log (log, "gen_erlang(%d", self->beta);
  for (i = 0; i < self->beta; i++)
    {
      ccl_log (log, ", ");
      sas_law_param_log_gen (log, self->p_lambdas[i], idlog);
    }
  ccl_log (log, ")");
}

static LAW_DESTROY (gen_erlang)
{
  sas_int i;
  SELF (l, gen_erlang);
  for (i = 0; i < self->beta; i++)
    sas_law_param_del_reference (self->p_lambdas[i]);
  ccl_delete (self->p_lambdas);
  ccl_delete (self->lambdas);
}

static LAW_UPDATE_PARAMS (gen_erlang)
{
  sas_int i;
  SELF (l, gen_erlang);

  for (i = 0; i < self->beta; i++)
    self->lambdas[i] = sas_law_param_get_value (self->p_lambdas[i], prng);
}

static LAW_COMPUTE_DELAY (gen_erlang)
{
  sas_int i;
  CSTSELF (l, gen_erlang);
  sas_delay result = 0.0;

  for(i = 0; i < self->beta; i++)
    {
      sas_double p = sas_prng_probability (prng);
      result += log (p) / self->lambdas[i];
    }
  result = -result;

  return result;
}

static LAW_EXPECTED_VALUE (gen_erlang)
{
  ccl_unreachable ();
  return 0.0;
}

static LAW_SUBSTITUTE (gen_erlang)
{
  CSTSELF (l, gen_erlang);
  int i;
  sas_law *result;  
  sas_law_param **lambdas = ccl_new_array (sas_law_param *, self->beta);
  
  for (i = 0; i < self->beta; i++)
    lambdas[i] = subst (self->p_lambdas[i], data);
  result = sas_law_crt_generalized_erlang (self->beta, lambdas);
  for (i = 0; i < self->beta; i++)
    sas_law_param_del_reference (lambdas[i]);
  ccl_delete (lambdas);
  
  return result;
}

/*!
 * IFA LAW
 */
sas_law *
sas_law_crt_ifa_cst (sas_double delay, sas_double first_time)
{
  sas_law_param *p_delay = sas_law_param_crt_constant (delay);
  sas_law_param *p_first_time = sas_law_param_crt_constant (first_time);
  sas_law *result = sas_law_crt_ifa (p_delay, p_first_time);
  sas_law_param_del_reference (p_delay);
  sas_law_param_del_reference (p_first_time);
  
  return result;

}

sas_law *
sas_law_crt_ifa (sas_law_param *delay, sas_law_param *first_time)
{
  sas_law *result = s_new_law (ifa);
  SELF (result, ifa);
  
  self->p_delay = sas_law_param_add_reference (delay);
  self->p_first_time = sas_law_param_add_reference (first_time);
  
  return result;
}

static LAW_DISPLAY (ifa)
{
  CSTSELF (l, ifa);
  
  ccl_log (log, "ifa(");
  sas_law_param_log_gen (log, self->p_delay, idlog);
  ccl_log (log, ",");
  sas_law_param_log_gen (log, self->p_first_time, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (ifa)
{
  SELF (l, ifa);
  
  sas_law_param_del_reference (self->p_delay);
  sas_law_param_del_reference (self->p_first_time);
}

static LAW_UPDATE_PARAMS (ifa)
{
  SELF (l, ifa);
  
  self->delay = sas_law_param_get_value (self->p_delay, prng);
  self->first_time = sas_law_param_get_value (self->p_first_time, prng);
}

static LAW_COMPUTE_DELAY (ifa)
{
  CSTSELF (l, ifa);
  sas_delay result = current_time - self->first_time;

  if (result > 0.0) 
    result = self->delay - FMOD (result, self->delay);
  else
    result = -result;

  return result;
}

static LAW_EXPECTED_VALUE (ifa)
{
  CSTSELF (l, ifa);
  
  return self->first_time;
}

static LAW_SUBSTITUTE (ifa)
{
  CSTSELF (l, ifa);
  sas_law_param *f = subst (self->p_first_time, data);
  sas_law_param *d = subst (self->p_delay, data);
  sas_law *result = sas_law_crt_ifa (d, f);
  sas_law_param_del_reference (f);
  sas_law_param_del_reference (d);  
  
  return result;
}

/*!
 * IPA LAW
 */
sas_law *
sas_law_crt_ipa_cst (sas_double delay)
{
  sas_law_param *p_delay = sas_law_param_crt_constant (delay);
  sas_law *result = sas_law_crt_ipa (p_delay);
  sas_law_param_del_reference (p_delay);
  
  return result;

}

sas_law *
sas_law_crt_ipa (sas_law_param *delay)
{
  sas_law *result = s_new_law (ipa);
  SELF (result, ipa);
  
  self->p_delay = sas_law_param_add_reference (delay);
  
  return result;
}

static LAW_DISPLAY (ipa)
{
  CSTSELF (l, ipa);
  
  ccl_log (log, "ipa(");
  sas_law_param_log_gen (log, self->p_delay, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (ipa)
{
  SELF (l, ipa);
  
  sas_law_param_del_reference (self->p_delay);
}

static LAW_UPDATE_PARAMS (ipa)
{
  SELF (l, ipa);
  
  self->delay = sas_law_param_get_value (self->p_delay, prng);
}

static LAW_COMPUTE_DELAY (ipa)
{
  CSTSELF (l, ipa);
  sas_delay result;

  if (current_time < self->delay) 
    result = self->delay - current_time;
  else 
    result = self->delay - FMOD (current_time, self->delay);

  return result;
}

static LAW_EXPECTED_VALUE (ipa)
{
  CSTSELF (l, ipa);
  
  return self->delay;
}

static LAW_SUBSTITUTE (ipa)
{
  CSTSELF (l, ipa);
  sas_law_param *d = subst (self->p_delay, data);
  sas_law *result = sas_law_crt_ipa (d);
  sas_law_param_del_reference (d);  
  
  return result;
}

/*!
 * NLOG LAW
 */
sas_law *
sas_law_crt_nlog_cst (sas_double mean, sas_double error_factor)
{
  sas_law_param *p_mean = sas_law_param_crt_constant (mean);
  sas_law_param *p_error_factor = sas_law_param_crt_constant (error_factor);
  sas_law *result = sas_law_crt_nlog (p_mean, p_error_factor);
  sas_law_param_del_reference (p_mean);
  sas_law_param_del_reference (p_error_factor);
  
  return result;
}

sas_law *
sas_law_crt_nlog (sas_law_param *mean, sas_law_param *error_factor)
{
  sas_law *result = s_new_law (nlog);
  SELF (result, nlog);
  
  self->p_mean = sas_law_param_add_reference (mean);
  self->p_error_factor = sas_law_param_add_reference (error_factor);
  
  return result;
}

static LAW_DISPLAY (nlog)
{
  CSTSELF (l, nlog);
  ccl_log (log, "nlog(");
  sas_law_param_log_gen (log, self->p_mean, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_error_factor, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (nlog)
{
  SELF (l, nlog);
  sas_law_param_del_reference (self->p_mean);
  sas_law_param_del_reference (self->p_error_factor);
}

static LAW_UPDATE_PARAMS (nlog)
{
  SELF (l, nlog);
  sas_double mean = sas_law_param_get_value (self->p_mean, prng);
  sas_double error_factor =
    sas_law_param_get_value (self->p_error_factor, prng);

  self->b = log (error_factor) / 1.64;
  self->a = log (mean)- SQR (self->b) / 2.0;
}

static LAW_COMPUTE_DELAY (nlog)
{
  CSTSELF (l, nlog);
  sas_proba Z1 = sas_prng_probability (prng);
  sas_proba Z2 =  sas_prng_probability (prng);
  sas_delay result = sqrt (-2.0 * log (Z1)) * cos (2 * PI * Z2);
  result = exp (self->b * result + self->a);

  return result;
}

static LAW_EXPECTED_VALUE (nlog)
{
  CSTSELF (l, nlog);
  
  return sas_law_param_get_value (self->p_mean, NULL);
}

static LAW_SUBSTITUTE (nlog)
{
  CSTSELF (l, nlog);
  sas_law_param *m = subst (self->p_mean, data);
  sas_law_param *ef = subst (self->p_error_factor, data);
  sas_law *result = sas_law_crt_nlog (m, ef);
  sas_law_param_del_reference (m);
  sas_law_param_del_reference (ef);  
  
  return result;
}

/*!
 * OPTIONAL LAW
 */
sas_law *
sas_law_crt_optional_law (ccl_list *cond_laws)
{
  sas_law *result = s_new_law (optional);
  SELF (result, optional);
  
  self->cond_laws = cond_laws;
  
  return result;
}

static LAW_DISPLAY (optional)
{
  ccl_pair *p;
  CSTSELF (l, optional);
  
  ccl_log (log, "optional(%d", ccl_list_get_size (self->cond_laws));
  for (p = FIRST (self->cond_laws); p; p = CDR (p))
    {
      sas_conditional_law *cl = CAR (p);

      ccl_log (log, ", [");
      cl->to_string_data (cl->cond_data);
      ccl_log (log, ", ");
      sas_law_log (log, cl->law);
      ccl_log (log, "]");
    }
  ccl_log (log, ")");	    
}

static LAW_DESTROY (optional)
{
  CSTSELF (l, optional);
  
  ccl_list_clear_and_delete (self->cond_laws, (ccl_delete_proc *)
			     sas_conditional_law_destroy);
}

static LAW_UPDATE_PARAMS (optional)
{
  ccl_pair *p;
  sas_conditional_law *cl = NULL;
  SELF (l, optional);  

  for (p = FIRST (self->cond_laws); p; p = CDR (p))
    {
      cl = CAR (p);
      sas_law_update_params (cl->law, prng);
    }
}

static LAW_COMPUTE_DELAY (optional)
{
  ccl_pair *p;
  sas_conditional_law *cl = NULL;
  CSTSELF (l, optional);  
  
  for (p = FIRST (self->cond_laws); p; p = CDR (p))
    {
      cl = CAR (p);
      ccl_pre (cl->cond != NULL);
      if (cl->cond (cl->cond_data, runtime_data))
	break;
    }
  ccl_assert (p != NULL);
  
  return sas_law_compute_delay (cl->law, current_time, prng, runtime_data);
}

static LAW_EXPECTED_VALUE (optional)
{
  ccl_unreachable ();
  return 0.0;
}

static LAW_SUBSTITUTE (optional)
{
  ccl_pair *p;
  CSTSELF (l, optional);
  ccl_list *cond_laws = ccl_list_create ();
  sas_law *result;
  
  for (p = FIRST (self->cond_laws); p; p = CDR (p))
    {
      sas_conditional_law *cl = CAR (p);
      sas_law *im = sas_law_substitute_parameters (cl->law, subst, data,
						   substcond, cbdata);
      sas_conditional_law *icl;

      if (substcond != NULL)
	icl = substcond (cl->cond_data, im, cbdata);
      else
	icl = sas_conditional_law_crt (im, cl->cond, cl->to_string_data,
				       cl->del_cond_data, cl->cond_data);
      sas_law_del_reference (im);
      ccl_list_add (cond_laws, icl);
    }
  result = sas_law_crt_optional_law (cond_laws);
  
  return result;
}

/*!
 * TRIANGLE LAW
 */
sas_law *
sas_law_crt_triangle_cst (sas_double min, sas_double max, sas_double mode)
{
  sas_law_param *p_min = sas_law_param_crt_constant (min);
  sas_law_param *p_max = sas_law_param_crt_constant (max);
  sas_law_param *p_mode = sas_law_param_crt_constant (mode);
  sas_law *result = sas_law_crt_triangle (p_min, p_max, p_mode);
  sas_law_param_del_reference (p_min);
  sas_law_param_del_reference (p_max);
  sas_law_param_del_reference (p_mode);
  
  return result;
}

sas_law *
sas_law_crt_triangle (sas_law_param *min, sas_law_param *max,
		      sas_law_param *mode)
{
  sas_law *result = s_new_law (triangle);
  SELF (result, triangle);
  
  self->p_min = sas_law_param_add_reference (min);
  self->p_max = sas_law_param_add_reference (max);
  self->p_mode = sas_law_param_add_reference (mode);

  return result;
}

static LAW_DISPLAY (triangle)
{
  CSTSELF (l, triangle);
  ccl_log (log, "triangle(");
  sas_law_param_log_gen (log, self->p_min, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_max, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_mode, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (triangle)
{
  SELF (l, triangle);
  sas_law_param_del_reference (self->p_min);
  sas_law_param_del_reference (self->p_max);
  sas_law_param_del_reference (self->p_mode);
}

static LAW_UPDATE_PARAMS (triangle)
{
  SELF (l, triangle);
  sas_delay delta;
  
  self->a = sas_law_param_get_value (self->p_min, prng);
  self->b = sas_law_param_get_value (self->p_max, prng);
  delta = self->b - self->a;
  self->c = sas_law_param_get_value (self->p_mode, prng);
  self->fc = (self->c - self->a) / delta;
  self->phi1 = sqrt ((self->c - self->a) * delta);
  self->phi2 = -sqrt ((self->b - self->c) * delta);
}

static LAW_COMPUTE_DELAY (triangle)
{
  sas_proba Z = sas_prng_probability (prng);
  sas_delay result;
  CSTSELF (l, triangle);
  
  if (Z == 0.0) 
    result = self->a;
  else if (Z < self->fc) 
    result = self->a + self->phi1 * sqrt (Z);
  else if (Z < 1.0) 
    result = self->b + self->phi2 * sqrt (1.0 - Z);
  else 
    result = self->b;
  
  return result;
}

static LAW_EXPECTED_VALUE (triangle)
{
  CSTSELF (l, triangle);
  sas_double d = 2.0 / (self->b - self->a);
  sas_double a1 = d / (self->c - self->a);
  sas_double a2 = d / (self->c - self->b);
  sas_double a3 = self->a*self->a*self->a;
  sas_double b3 = self->b*self->b*self->b;
  sas_double c3 = self->c*self->c*self->c;   
  sas_double mean = -(a1 * (c3- a3) + a2 * (b3-c3)) / 6.0;
  
  return mean;
}

static LAW_SUBSTITUTE (triangle)
{
  CSTSELF (l, triangle);
  sas_law_param *min = subst (self->p_min, data);
  sas_law_param *max = subst (self->p_max, data);
  sas_law_param *mode = subst (self->p_mode, data);
  sas_law *result = sas_law_crt_triangle (min, max, mode);
  sas_law_param_del_reference (min);
  sas_law_param_del_reference (max);
  sas_law_param_del_reference (mode);  
  
  return result;
}

/*!
 * TRUNCATED WEIBULL LAW
 */
#define WEIBULL_POWER(x,y) (exp((y) * log ((x))))

static sas_double
s_gamma (sas_double x)
{
  sas_double result = lgamma (x);
  result *= (sas_double) signgam;
  
  return exp (result);
}

sas_law *
sas_law_crt_truncated_weibull_cst (sas_double mean, sas_double beta,
				   sas_double age)
{
  sas_law_param *p_mean = sas_law_param_crt_constant (mean);
  sas_law_param *p_beta = sas_law_param_crt_constant (beta);
  sas_law_param *p_age = sas_law_param_crt_constant (age);
  sas_law *result = sas_law_crt_truncated_weibull (p_mean, p_beta, p_age);
  sas_law_param_del_reference (p_mean);
  sas_law_param_del_reference (p_age);
  sas_law_param_del_reference (p_beta);
  
  return result;
}

sas_law *
sas_law_crt_truncated_weibull (sas_law_param *mean, sas_law_param *beta,
			       sas_law_param *age)
{
  sas_law *result = s_new_law (truncated_weibull);
  SELF (result, truncated_weibull);
  
  self->p_mean = sas_law_param_add_reference (mean);
  self->p_beta = sas_law_param_add_reference (beta);
  self->p_age = sas_law_param_add_reference (age);
  
  return result;
}

static LAW_DISPLAY (truncated_weibull)
{
  CSTSELF (l, truncated_weibull);
  
  ccl_log (log, "truncated_weibull(");
  sas_law_param_log_gen (log, self->p_mean, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_beta, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_age, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (truncated_weibull)
{
  SELF (l, truncated_weibull);
  sas_law_param_del_reference (self->p_mean);
  sas_law_param_del_reference (self->p_beta);
  sas_law_param_del_reference (self->p_age);
}

static LAW_UPDATE_PARAMS (truncated_weibull)
{
  SELF (l, truncated_weibull);
  sas_double mean = sas_law_param_get_value (self->p_mean, prng);
  sas_double beta = sas_law_param_get_value (self->p_beta, prng);
  sas_double age = sas_law_param_get_value (self->p_age, prng);
  
  self->d0 = 1.0 / beta;
  self->d1 = WEIBULL_POWER (age * s_gamma (1.0 + 1.0 / beta) / mean, beta);
  self->age = age;
}

static LAW_COMPUTE_DELAY (truncated_weibull)
{
  CSTSELF (l, truncated_weibull);
  sas_proba Z = sas_prng_probability (prng);
  sas_delay result = 1 - log (Z) / self->d1;
  
  result = self->age * (WEIBULL_POWER (result, self->d0) - 1.0);
  
  return result;
}

static LAW_EXPECTED_VALUE (truncated_weibull)
{
  CSTSELF (l, truncated_weibull);
  return sas_law_param_get_value (self->p_mean, NULL);
}

static LAW_SUBSTITUTE (truncated_weibull)
{
  CSTSELF (l, truncated_weibull);
  sas_law_param *mean = subst (self->p_mean, data);
  sas_law_param *beta = subst (self->p_beta, data);
  sas_law_param *age = subst (self->p_age, data);
  sas_law *result = sas_law_crt_truncated_weibull (mean, beta, age);
  sas_law_param_del_reference (mean);
  sas_law_param_del_reference (beta);
  sas_law_param_del_reference (age);  
  
  return result;
}

/*!
 * UNIFORM LAW
 */
sas_law *
sas_law_crt_uniform_cst (sas_double min_time, sas_double max_time)
{
  sas_law_param *p_min_time = sas_law_param_crt_constant (min_time);
  sas_law_param *p_max_time = sas_law_param_crt_constant (max_time);
  sas_law *result = sas_law_crt_uniform (p_min_time, p_max_time);
  sas_law_param_del_reference (p_min_time);
  sas_law_param_del_reference (p_max_time);
  
  return result;
}

sas_law *
sas_law_crt_uniform (sas_law_param *min_time, sas_law_param *max_time)
{
  sas_law *result = s_new_law (uniform);
  SELF (result, uniform);
  
  self->p_min_time = sas_law_param_add_reference (min_time);
  self->p_max_time = sas_law_param_add_reference (max_time);
  
  return result;
}

static LAW_DISPLAY (uniform)
{
  CSTSELF (l, uniform);
  ccl_log (log, "uniform(");
  sas_law_param_log_gen (log, self->p_min_time, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_max_time, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (uniform)
{
  SELF (l, uniform);
  sas_law_param_del_reference (self->p_min_time);
  sas_law_param_del_reference (self->p_max_time);
}

static LAW_UPDATE_PARAMS (uniform)
{  
  SELF (l, uniform);
  self->min_time = sas_law_param_get_value (self->p_min_time, prng);
  self->max_time = sas_law_param_get_value (self->p_max_time, prng);
  self->delay = self->max_time - self->min_time;
}

static LAW_COMPUTE_DELAY (uniform)
{
  CSTSELF (l, uniform);
  sas_proba Z = sas_prng_probability (prng);
  sas_delay result = self->delay * Z + self->min_time;
  
  return result;
}

static LAW_EXPECTED_VALUE (uniform)
{
  CSTSELF (l, uniform);
  return (self->min_time + self->max_time) / 2.0;
}

static LAW_SUBSTITUTE (uniform)
{
  CSTSELF (l, uniform);
  sas_law_param *min = subst (self->p_min_time, data);
  sas_law_param *max = subst (self->p_max_time, data);
  sas_law *result = sas_law_crt_uniform (min, max);
  sas_law_param_del_reference (min);
  sas_law_param_del_reference (max);
  
  return result;
}

/*!
 * WEIBULL LAW
 *
 */
#define WEIBULL_DELAY(z,d1,d2) ((d1) * WEIBULL_POWER (-log ((z)), (d2)))

sas_law *
sas_law_crt_weibull_cst (sas_double mean, sas_double beta)
{
  sas_law_param *p_mean = sas_law_param_crt_constant (mean);
  sas_law_param *p_beta = sas_law_param_crt_constant (beta);
  sas_law *result = sas_law_crt_weibull (p_mean, p_beta);
  sas_law_param_del_reference (p_mean);
  sas_law_param_del_reference (p_beta);
  
  return result;
}

sas_law *
sas_law_crt_weibull (sas_law_param *mean, sas_law_param *beta)
{
  sas_law *result = s_new_law (weibull);
  SELF (result, weibull);
  
  self->p_mean = sas_law_param_add_reference (mean);
  self->p_beta = sas_law_param_add_reference (beta);
  
  return result;  
}

static LAW_DISPLAY (weibull)
{
  CSTSELF (l, weibull);
  ccl_log (log, "weibull(");
  sas_law_param_log_gen (log, self->p_mean, idlog);
  ccl_log (log, ", ");
  sas_law_param_log_gen (log, self->p_beta, idlog);
  ccl_log (log, ")");
}

static LAW_DESTROY (weibull)
{
  SELF (l, weibull);
  sas_law_param_del_reference (self->p_mean);
  sas_law_param_del_reference (self->p_beta);
}

static LAW_UPDATE_PARAMS (weibull)
{
  SELF (l, weibull);
  sas_double mean = sas_law_param_get_value (self->p_mean, prng);
  sas_double beta = sas_law_param_get_value (self->p_beta, prng);
  self->d0 = mean / s_gamma (1.0 + 1.0 / beta);
  self->d1 = 1.0 / beta;
}

static LAW_COMPUTE_DELAY (weibull)
{
  CSTSELF (l, weibull);
  sas_proba Z = sas_prng_probability (prng);
  sas_delay result = WEIBULL_DELAY (Z, self->d0, self->d1);
  
  return result;
}

static LAW_EXPECTED_VALUE (weibull)
{
  CSTSELF (l, weibull);
  
  return sas_law_param_get_value (self->p_mean, NULL);
}

static LAW_SUBSTITUTE (weibull)
{
  CSTSELF (l, weibull);
  sas_law_param *mean = subst (self->p_mean, data);
  sas_law_param *beta = subst (self->p_beta, data);
  sas_law *result = sas_law_crt_weibull (mean, beta);
  sas_law_param_del_reference (mean);
  sas_law_param_del_reference (beta);
  
  return result;
}
