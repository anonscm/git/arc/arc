/*
 * sas-common.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SAS_COMMON_H
# define SAS_COMMON_H

# include <ccl/ccl-common.h>

# ifdef SAS_SIMPLE_DOUBLE
typedef double sas_double;
# define SAS_DOUBLE_MODIFIER ""
# else
typedef long double sas_double;
# define SAS_DOUBLE_MODIFIER "L"
#endif

typedef sas_double sas_delay;
typedef sas_double sas_time;
typedef sas_double sas_proba;
typedef int32_t sas_int;
typedef uint32_t sas_uint;
typedef uint32_t sas_timestamp;

# define SQR(x) ((x)*(x))

#endif /* ! SAS_COMMON_H */
