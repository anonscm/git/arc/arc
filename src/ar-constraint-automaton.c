/*
 * ar-constraint-automaton.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-array.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-model.h"
#include "ar-solver.h"
#include "deps/fdset-comp.h"
#include "ar-constraint-automaton.h"
#include "ar-ca-rewriters.h"
#include "ar-ca-p.h"

			/* --------------- */

typedef CCL_ARRAY(void *) array_of_pointers;

			/* --------------- */

# define m_delete_expr_array(a) \
  do { \
    int i; \
    for(i = 0; i < (a).size; i++) \
      ar_ca_expr_del_reference((a).data[i]); \
    ccl_array_delete(a); \
  } while(0)


			/* --------------- */

static ccl_pointer_iterator *
s_create_iterator_from_array (array_of_pointers *a);

			/* --------------- */

int ar_ca_debug_is_on = 0;

ar_ca *
ar_ca_allocate_ca (ar_node *node, ar_ca_exprman *man)
{
  ar_ca *result = ccl_new (ar_ca);

  result->refcount = 1;
  result->node = ar_node_add_reference (node);
  result->man = ar_ca_exprman_add_reference (man);
  result->hierarchy = ar_node_get_hierarchy_identifiers (node);
  
  result->flow_variables = ccl_list_create ();
  result->state_variables = ccl_list_create ();
  
  ccl_array_init (result->events);
  result->assertions = ccl_list_create ();
  ccl_array_init (result->transitions);
  result->initial_assignments = ccl_list_create ();
  result->initial_constraints = ccl_list_create ();
  result->reduced = NULL;
  result->fds = NULL;
  result->epsilon = NULL;
  result->law_params = NULL;
  result->laws = NULL;
  result->observers = NULL;
  result->obsids = NULL;
  result->preemptibles = NULL;
  result->bucket_event_probas = NULL;
  result->event_to_bucket_table = NULL;
  result->buckets = NULL;
  result->priorities = NULL;
  
  return result;
}


ar_ca *
ar_ca_create(ar_node *node)
{
  ar_ca *result = ccl_new(ar_ca);

  result->refcount = 1;
  result->node = ar_node_add_reference(node);
  result->man = ar_ca_exprman_create();
  result->hierarchy = ar_node_get_hierarchy_identifiers (node);
  
  result->flow_variables = ccl_list_create ();
  result->state_variables = ccl_list_create ();

  ccl_array_init_with_size (result->events, 
		 	    ar_node_get_number_of_events (node));
  result->assertions = ccl_list_create ();
  ccl_array_init_with_size (result->transitions,
			    ar_node_get_number_of_transitions (node));
  result->initial_assignments = ccl_list_create ();
  result->initial_constraints = ccl_list_create ();
  result->reduced = NULL;
  result->fds = NULL;
  result->epsilon = NULL;
  result->law_params = NULL;
  result->laws = NULL;
  result->observers = NULL;
  result->obsids = NULL;
  result->preemptibles = NULL;
  result->bucket_event_probas = NULL;
  result->event_to_bucket_table = NULL;
  result->buckets = NULL;
  result->priorities = NULL;
  
  return result;
}

			/* --------------- */

ar_ca *
ar_ca_add_reference(ar_ca *ca)
{
  ccl_pre( ca != NULL );
  
  ca->refcount++;

  return ca;
}

			/* --------------- */

void
ar_ca_del_reference(ar_ca *ca)
{
  int i;

  ccl_pre (ca != NULL); 
  ccl_pre (ca->refcount > 0);

  ca->refcount--;
  if (ca->refcount > 0) 
    return;
  
  ar_node_del_reference (ca->node);
  ccl_list_clear_and_delete (ca->hierarchy, (ccl_delete_proc *)
			     ar_identifier_del_reference);
  ccl_list_clear_and_delete (ca->flow_variables, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);
  ccl_list_clear_and_delete (ca->state_variables, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);

  ccl_list_clear_and_delete (ca->assertions, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);
  ccl_list_clear_and_delete (ca->initial_assignments, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);
  ccl_list_clear_and_delete (ca->initial_constraints, (ccl_delete_proc *)
			     ar_ca_expr_del_reference);

  ccl_zdelete (ar_ca_del_reference, ca->reduced);

  for (i = 0; i < ca->events.size; i++)
    ccl_zdelete (ar_ca_event_del_reference, ca->events.data[i]);
  ccl_array_delete (ca->events);

  for (i = 0; i < ca->transitions.size; i++)
    ccl_zdelete (ar_ca_trans_del_reference, ca->transitions.data[i]);
  ccl_array_delete (ca->transitions);

  ccl_zdelete (fdset_del_reference, ca->fds);

  if (ca->law_params != NULL)
    ccl_list_clear_and_delete (ca->law_params,
			       (ccl_delete_proc *) sas_law_param_del_reference);
  ccl_zdelete (ar_idtable_del_reference, ca->laws);
  ccl_zdelete (ar_idtable_del_reference, ca->observers);
  ccl_zdelete (ar_idtable_del_reference, ca->priorities);
  ccl_zdelete (ccl_list_delete, ca->obsids);
  ccl_zdelete (ccl_set_delete, ca->preemptibles);
  if (ca->buckets)
    {
      while (! ccl_list_is_empty (ca->buckets))
	{
	  ccl_list_clear_and_delete (ccl_list_take_first (ca->buckets),
				     (ccl_delete_proc *)
				     ar_identifier_del_reference);
	}
      ccl_list_delete (ca->buckets);
      ar_idtable_del_reference (ca->bucket_event_probas);
      ar_idtable_del_reference (ca->event_to_bucket_table);
    }
  
  ar_ca_exprman_del_reference (ca->man);
  ccl_delete (ca);
}

			/* --------------- */

void
ar_ca_log_info (ccl_log_type log, const ar_ca *ca, int comments)
{
  const char *prefix = comments ? "// " : "";

  ccl_log (log, "%sstatistics: \n", prefix);
  ccl_log (log, "%s number of variables : %d\n", prefix,
	   ar_ca_get_number_of_variables (ca));
  ccl_log (log, "%s   flow variables : %d\n", prefix, 
	   ar_ca_get_number_of_flow_variables (ca));
  ccl_log (log, "%s   state variables : %d\n", prefix, 
	   ar_ca_get_number_of_state_variables (ca));
  ccl_log (log, "%s max cardinality : %d\n", prefix, 
	   ar_ca_get_max_cardinality (ca));
  ccl_log (log, "%s number of events : %d\n", prefix, 
	   ar_ca_get_number_of_events (ca));
  ccl_log (log, "%s number of assertions : %d\n", prefix, 
	   ar_ca_get_number_of_assertions (ca));
  ccl_log (log, "%s number of transitions : %d\n", prefix, 
	   ar_ca_get_number_of_transitions (ca));
}

			/* --------------- */

static void
s_log_vardecl (ccl_log_type log, const ar_ca *ca, const char *kind, 
	       const ccl_list *vars, const char *sep, const char *quotes)
{
  ccl_pair *pv;

  if (ccl_list_is_empty (vars))
    return;

  ccl_log (log, " %s // %d %s variables\n", kind, ccl_list_get_size (vars),
	   kind);

  for (pv = FIRST (vars); pv; pv = CDR (pv))
    {
      ar_ca_expr *var = CAR (pv);
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
      ccl_list *attrs = ar_ca_expr_variable_get_attributes (var);

      ccl_log (log, "   ");
      ar_ca_expr_log_gen (log, var, sep, quotes);
      ccl_log (log, " : ");
      ar_ca_domain_log_gen (log, dom, ca->man, sep, quotes);
      if (! ccl_list_is_empty (attrs))
	{
	  ccl_pair *p = FIRST (attrs);

	  ccl_log (log, " : ");
	  ar_identifier_log (log, CAR (p));
	  for (p = CDR (p); p; p = CDR (p))
	    {
	      ccl_log (log, ", ");
	      ar_identifier_log (log, CAR (p));
	    }
	}
      ccl_log (log, ";\n");
    }
}

			/* --------------- */

void
ar_ca_log (ccl_log_type log, const ar_ca *ca)
{
  ar_ca_log_gen (log, ca, 0, ".", "''");
}

			/* --------------- */

void
ar_ca_log_gen (ccl_log_type log, const ar_ca *ca, int show_epsilon, 
	       const char *separator, const char *quotes)
{
  int i;

  ar_ca_log_info (log, ca, 1);
  ccl_log (log, "node ");
  {
    ar_identifier *name = ar_node_get_name (ca->node);
    ar_identifier_log_global_quote_gen (log, name, separator, quotes);
    ccl_log (log, "\n");
    ar_identifier_del_reference (name);
  }

  s_log_vardecl (log, ca, "flow", ca->flow_variables, separator, quotes);
  s_log_vardecl (log, ca, "state", ca->state_variables, separator, quotes);

  if (ccl_list_get_size (ca->initial_assignments) +
      ccl_list_get_size (ca->initial_constraints)) 
    {
      const ccl_pair *p;
      ccl_log (log, " init\n");

      for (p = FIRST (ca->initial_assignments); p; p = CDDR (p))
	{
	  ar_ca_expr *v = CAR (p);
	  ar_ca_expr *val = CADR (p);
	  ccl_log (log,"   ");
	  ar_ca_expr_log_gen (log, v, separator, quotes);
	  ccl_log (log, " := ");
	  ar_ca_expr_log_gen (log, val, separator, quotes);
	  if (CDDR (p) != NULL || ! ccl_list_is_empty (ca->initial_constraints))
	    ccl_log (log, ",\n");
	}

      for (p = FIRST (ca->initial_constraints); p; p = CDR (p))
	{
	  ccl_log (log,"   ");
	  ar_ca_expr_log_gen (log, CAR (p), separator, quotes);
	  if (CDR (p) != NULL)
	    ccl_log (log, ",\n");
	}
      
      ccl_log (log, ";\n");
    }
    
  if (ca->events.size > 1) 
    {
      ccl_log (log," event // %d events\n", ca->events.size);
      for (i = 0; i < ca->events.size; i++)
	{
	  if (! ar_ca_event_is_epsilon (ca->events.data[i]))
	    {
	      ccl_list *attrs = ar_ca_event_get_attributes (ca->events.data[i]);

	      ccl_log (log, "   ");
	      ar_ca_event_log_gen (log, ca->events.data[i], show_epsilon, 
				   separator, quotes);
	      if (!ccl_list_is_empty (attrs))
		{
		  ccl_pair *p = FIRST (attrs);

		  ccl_log (log, " : ");
		  ar_identifier_log (log, CAR (p));
		  for (p = CDR (p); p; p = CDR (p))
		    {
		      ccl_log (log, ", ");
		      ar_identifier_log (log, CAR (p));
		    }
		}
	      ccl_log (log, ";\n");
	    }
	}
    }


  if (! ccl_list_is_empty (ca->assertions))
    {
      ccl_pair *p;
      
      ccl_log (log, " assert // %d assertions\n",
	       ccl_list_get_size (ca->assertions));
      
      for (p = FIRST (ca->assertions); p; p = CDR (p))
	{
	  ccl_log (log, "   ");
	  ar_ca_expr_log_gen (log, CAR (p), separator, quotes);
	  ccl_log (log, ";\n");
	}
    }

  if (ca->transitions.size) 
    {
      int nb_non_epsilon = 0;

      for (i = 0; i < ca->transitions.size; i++)
	{
	  ar_ca_event *e = ar_ca_trans_get_event (ca->transitions.data[i]);
	  if (! ar_ca_event_is_epsilon (e))
	    nb_non_epsilon++;
	  ar_ca_event_del_reference (e);
	}
      if (nb_non_epsilon)
	{
	  ccl_log (log, " trans\n"); 
	  for (i = 0; i < ca->transitions.size; i++)
	    {
	      ar_ca_event *e = ar_ca_trans_get_event (ca->transitions.data[i]);
	      if (! ar_ca_event_is_epsilon (e))
		{
		  ccl_log (log, "   ");
		  ar_ca_trans_log_gen (log, ca->transitions.data[i], 
				       show_epsilon, separator, quotes);
		  ccl_log (log, ";\n");
		}
	      ar_ca_event_del_reference (e);
	    }
	}
    }

  {
    int first_extern = 1;
    
    if (ca->law_params != NULL)
      {
	ccl_pair *p;

	if (first_extern)
	  {
	    ccl_log (log, "  extern \n");
	    first_extern = 0;
	  }
      
	for (p = FIRST (ca->law_params); p ; p = CDR (p))
	  {
	    const ar_identifier *id = sas_law_param_get_name_cst  (CAR (p));
	    sas_law_param *val = sas_law_param_get_definition (CAR (p));
	    ccl_pre (sas_law_param_get_kind (CAR (p)) == SAS_LAW_PARAM_VARIABLE);
	    ccl_log (log, "    parameter ");
	    ar_identifier_log (log, id);
	    ccl_log (log, " = ");
	    sas_law_param_log (log, val);
	    ccl_log (log, "; \n");
	    sas_law_param_del_reference (val);
	  }
      }
  
    if (ca->laws != NULL)
      {
	ar_identifier_iterator *ii = ar_idtable_get_keys (ca->laws);
 
	if (first_extern)
	  {
	    ccl_log (log, "  extern \n");
	    first_extern = 0;
	  }

	while (ccl_iterator_has_more_elements (ii))
	  {
	    ar_identifier *id = ccl_iterator_next_element (ii);
	    sas_law *l = ar_idtable_get (ca->laws, id);

	    ccl_log (log, "    law <event ");
	    ar_identifier_log (log, id);
	    ccl_log (log, "> = ");
	    sas_law_log (log, l);
	    ccl_log (log, "; \n");
	    ar_identifier_del_reference (id);
	    sas_law_del_reference (l);
	  }
	ccl_iterator_delete (ii);
      }
  
    if (ca->observers != NULL)
      {
	ccl_pair *p; 
 
	if (first_extern)
	  {
	    ccl_log (log, "  extern \n");
	    first_extern = 0;
	  }

	for (p = FIRST (ca->obsids); p; p = CDR (p))
	  {
	    ar_identifier *id = CAR (p);
	    ar_ca_expr *o = ar_idtable_get (ca->observers, id);

	    if (ar_ca_expr_is_boolean_expr (o))
	      ccl_log (log, "    predicate ");
	    else
	      ccl_log (log, "    property ");
	    ar_identifier_log (log, id);
	    ccl_log (log, " = <term (");
	    ar_ca_expr_log (log, o);
	    ccl_log (log, ")>; \n");
	  
	    ar_ca_expr_del_reference (o);
	  }
      }
    
    if (ca->preemptibles != NULL)
      {
	ccl_pointer_iterator *pi = ccl_set_get_elements (ca->preemptibles);
	
	if (first_extern)
	  {
	    ccl_log (log, "  extern \n");
	    first_extern = 0;
	  }
	
	while (ccl_iterator_has_more_elements (pi))
	  {
	    ar_identifier *id = ccl_iterator_next_element (pi);
	    ccl_log (log, "    preemptible <event ");
	    ar_identifier_log (log, id);
	    ccl_log (log, ">; \n");
	  }
	ccl_iterator_delete (pi);
      }
    
    if (ca->buckets != NULL)
      {
	ccl_pair *pb;
	if (first_extern)
	  {
	    ccl_log (log, "  extern \n");
	    first_extern = 0;
	  }
	
	for (pb = FIRST (ca->buckets); pb; pb = CDR (pb))
	  {
	    ccl_list *events = CAR (pb);
	    ccl_pair *pe = FIRST (events);
	    
	    ccl_log (log, "    bucket '");
	    ar_identifier_log (log, CAR (pe));
	    ccl_log (log, "' = { ");
	    for (pe = CDR (pe); pe; pe = CDR (pe))
	      {
		ar_identifier *id = CAR (pe);
		ccl_log (log, "<event ");
		ar_identifier_log (log, id);
		ccl_log (log, ">");
		if (CDR (pe) != NULL)
		  {
		    sas_law_param *p =
		      ar_idtable_get (ca->bucket_event_probas, id);
		    ccl_log (log, ", ");
		    sas_law_param_log (log, p);
		    sas_law_param_del_reference (p);
		    ccl_log (log, ",\n             ");
		  }
	      }
	    ccl_log (log, "};\n");
	    
	  }
      }
    
    if (ca->priorities != NULL)
      {
	ar_identifier_iterator *ii = ar_idtable_get_keys (ca->priorities);
	if (first_extern)
	  {
	    ccl_log (log, "  extern \n");
	    first_extern = 0;
	  }

	while (ccl_iterator_has_more_elements (ii))
	  {
	    ar_identifier *id = ccl_iterator_next_element (ii);
	    int pl = (intptr_t) ar_idtable_get (ca->priorities, id);
	    ccl_log (log, "    priority '");
	    ar_identifier_log (log, id);
	    ccl_log (log, "' = %d\n", pl);
	  }
	ccl_iterator_delete (ii);
      }
  }
  ccl_log (log, "edon\n");
}

			/* --------------- */

ar_node *
ar_ca_get_node (const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ar_node_add_reference (ca->node);
}

const ccl_list *
ar_ca_get_hierarchy_identifiers(const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ca->hierarchy;
}

ar_node *
ar_ca_get_hierarchical_node (const ar_ca *ca)
{
  ar_node *node = ar_ca_get_node (ca);
  ar_identifier *nodename = ar_node_get_name (node);

  ar_node_del_reference (node);
  node = ar_model_get_node (nodename);
  ar_identifier_del_reference (nodename);

  return node;
}

ar_ca_exprman *
ar_ca_get_expression_manager (ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ar_ca_exprman_add_reference (ca->man);
}

			/* --------------- */

int
ar_ca_get_max_cardinality (const ar_ca *ca)
{
  int r = 2;
  int result = 0;
  const ccl_list *l = ca->flow_variables;

  ccl_pre (ca != NULL);
  
  while (r--)
    {
      const ccl_pair *p;

      for (p = FIRST (l); p; p = CDR (p))
	{
	  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (CAR (p));
	  int card = ar_ca_domain_get_cardinality (dom);
	  if (card > result)
	    result = card;
	}
      l = ca->state_variables;
    }

  return result;
}

			/* --------------- */

int
ar_ca_get_number_of_variables (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return (ar_ca_get_number_of_flow_variables (ca) +
	  ar_ca_get_number_of_state_variables (ca));
}

			/* --------------- */

int
ar_ca_get_number_of_flow_variables (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ccl_list_get_size (ca->flow_variables);
}

			/* --------------- */
void
ar_ca_add_flow_variable(ar_ca *ca, ar_ca_expr *var)
{
  ccl_pre( ca != NULL ); ccl_pre( var != NULL );

  var = ar_ca_expr_add_reference(var);
  ccl_list_add (ca->flow_variables,var);
}

			/* --------------- */

const ccl_list *
ar_ca_get_flow_variables (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ca->flow_variables;
}

			/* --------------- */

ccl_pointer_iterator *
ar_ca_get_flow_variables_iterator (ar_ca *ca)
{
  return ccl_list_get_iterator (ca->flow_variables, NULL, NULL);
}

			/* --------------- */

int
ar_ca_get_number_of_state_variables (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ccl_list_get_size (ca->state_variables);
}

			/* --------------- */

void
ar_ca_add_state_variable(ar_ca *ca, ar_ca_expr *var)
{
  ccl_pre( ca != NULL ); ccl_pre( var != NULL );

  var = ar_ca_expr_add_reference (var);
  ccl_list_add (ca->state_variables, var);
}

			/* --------------- */

const ccl_list * 
ar_ca_get_state_variables (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ca->state_variables;
}

			/* --------------- */

ccl_pointer_iterator *
ar_ca_get_state_variables_iterator (ar_ca *ca)
{
  return ccl_list_get_iterator (ca->state_variables, NULL, NULL);
}

			/* --------------- */

static void
s_add_varlist_to_htable (ccl_hash *ht, const ccl_list *l, intptr_t val)
{
  const ccl_pair *p;

  for (p = FIRST (l); p; p = CDR (p))
    {
      ccl_hash_find (ht, CAR (p));
      ccl_hash_insert (ht, (void *) val);
    }
}

ccl_hash *
ar_ca_get_variables (const ar_ca *ca, int which)
{
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, NULL);

  if (which & AR_CA_STATE_VARS)
    s_add_varlist_to_htable (result, ar_ca_get_state_variables (ca), 1);

  if (which & AR_CA_FLOW_VARS)
    s_add_varlist_to_htable (result, ar_ca_get_flow_variables (ca), 0);

  return result;
}

void
ar_ca_add_event(ar_ca *ca, ar_ca_event *ev)
{
  ccl_pre (ca != NULL); 
  ccl_pre (ev != NULL);

  ev = ar_ca_event_add_reference (ev);
  ccl_array_add (ca->events,ev);
}

			/* --------------- */

int
ar_ca_get_number_of_events (const ar_ca *ca)
{
  ccl_pre( ca != NULL );
  
  return ca->events.size;
}

			/* --------------- */

ar_ca_event **
ar_ca_get_events(ar_ca *ca)
{
  ccl_pre( ca != NULL );
  
  return ca->events.data;
}

			/* --------------- */

ccl_pointer_iterator *
ar_ca_get_events_iterator (ar_ca *ca)
{
  return 
    s_create_iterator_from_array ((array_of_pointers *) &ca->events);
}

			/* --------------- */

int 
ar_ca_has_subevent(ar_ca *ca, ar_identifier *id)
{
  int i;

  ccl_pre( ca != NULL ); ccl_pre( id != NULL );

  for(i = 0; i < ca->events.size; i++)
    {
      if( ar_ca_event_contains(ca->events.data[i],id) )
	return 1;
    }

  return 0;
}

			/* --------------- */

ar_ca_event *
ar_ca_get_epsilon_event (ar_ca *ca)
{
  int i;
  ar_ca_event *result;

  if (ca->epsilon != NULL)
    result = ca->epsilon;
  else
    {
      result = NULL;
      for (i = 0; result == NULL && i < ca->events.size; i++)
	{
	  if (ar_ca_event_is_epsilon (ca->events.data[i]))
	    result = ca->events.data[i];
	}
      ca->epsilon = result;
    }
  ccl_post (result != NULL);
  
  return ar_ca_event_add_reference (result);
}

			/* --------------- */

ar_ca_trans *
ar_ca_get_epsilon_transition (ar_ca *ca)
{
  int i;
  ar_ca_trans *result = NULL;
  ar_ca_event *epsilon = ar_ca_get_epsilon_event (ca);
  
  for (i = 0; i < ca->transitions.size; i++)
    {
      ar_ca_trans *t = ca->transitions.data[i];
      const ar_ca_event *e = ar_ca_trans_get_const_event (t);
      if (e == epsilon)
	{
	  ccl_assert (result == NULL);
	  result = t;
	}
    }
  ar_ca_event_del_reference (epsilon);
  ccl_assert (result != NULL);
  
  return ar_ca_trans_add_reference (result);
}

void
ar_ca_add_initial_assignment (ar_ca *ca, ar_ca_expr *var, ar_ca_expr *value)
{
  ar_ca_expr *val;
  ccl_pre (ca != NULL); 
  ccl_pre (var != NULL); 
  ccl_pre (value != NULL);

  var = ar_ca_expr_add_reference (var);
  ccl_list_add (ca->initial_assignments, var);

  if (ar_ca_expr_get_kind (value) == AR_CA_CST)
    val = ar_ca_expr_add_reference (value);
  else
    {
      int validx;
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);

      ar_ca_expr_evaluate (value, 1);
      
      ccl_assert (ar_ca_expr_get_min (value) == ar_ca_expr_get_max (value));
      validx = ar_ca_domain_get_value_index (dom, ar_ca_expr_get_min (value));
      val = ar_ca_expr_variable_get_ith_value (var, validx);
    }
  ccl_list_add (ca->initial_assignments, val);
}

			/* --------------- */

int
ar_ca_get_number_of_initial_assignments (const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ccl_list_get_size (ca->initial_assignments) / 2;
}

			/* --------------- */

const ccl_list *
ar_ca_get_initial_assignments (const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ca->initial_assignments;
}

			/* --------------- */

void
ar_ca_add_initial_constraint (ar_ca *ca, ar_ca_expr *ic)
{
  ccl_pre (ca != NULL); 
  ccl_pre (ic != NULL);

  ic = ar_ca_expr_add_reference (ic);
  ccl_list_add (ca->initial_constraints, ic);
}

			/* --------------- */

int
ar_ca_get_number_of_initial_constraints (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ccl_list_get_size (ca->initial_constraints);
}

			/* --------------- */

const ccl_list *
ar_ca_get_initial_constraints (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ca->initial_constraints;
}

			/* --------------- */

void
ar_ca_add_assertion (ar_ca *ca, ar_ca_expr *constraint)
{
  ccl_pre (ca != NULL);
  ccl_pre (constraint != NULL);

  if (ar_ca_expr_get_kind (constraint) == AR_CA_AND)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (constraint);
      ar_ca_add_assertion (ca, args[0]);
      ar_ca_add_assertion (ca, args[1]);
    }
  else
    {
      constraint = ar_ca_expr_add_reference (constraint);
      ccl_list_add (ca->assertions, constraint);
    }
}

			/* --------------- */

int
ar_ca_get_number_of_assertions (const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ccl_list_get_size (ca->assertions);
}

			/* --------------- */

const ccl_list * 
ar_ca_get_assertions (const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ca->assertions;
}

			/* --------------- */

void
ar_ca_add_transition(ar_ca *ca, ar_ca_trans *t)
{
  ccl_pre( ca != NULL ); ccl_pre( t != NULL );

  t = ar_ca_trans_add_reference(t);
  ccl_array_add(ca->transitions,t);
}

			/* --------------- */

int
ar_ca_get_number_of_transitions (const ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ca->transitions.size;
}

			/* --------------- */

ar_ca_trans **
ar_ca_get_transitions (const ar_ca *ca)
{
  ccl_pre( ca != NULL );

  return ca->transitions.data;
}

			/* --------------- */

ccl_pointer_iterator *
ar_ca_get_transitions_iterator (ar_ca *ca)
{
  return 
    s_create_iterator_from_array ((array_of_pointers *) &ca->transitions);
}

			/* --------------- */


void
ar_ca_add_law_parameter (ar_ca *ca, sas_law_param *p)
{
  ccl_pre (ca != NULL);
  ccl_pre (p != NULL);
  
  if (ca->law_params == NULL)
    ca->law_params = ccl_list_create ();
  ccl_list_add (ca->law_params, sas_law_param_add_reference (p));
}

void
ar_ca_add_law (ar_ca *ca, ar_identifier *id, sas_law *law)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);
  ccl_pre (law != NULL);

  if (ca->laws == NULL)
    ca->laws = ar_idtable_create (1,
				  (ccl_duplicate_func *) sas_law_add_reference,
				  (ccl_delete_proc *) sas_law_del_reference);
  ar_idtable_put (ca->laws, id, law);
}

int
ar_ca_has_law (ar_ca *ca, ar_identifier *id)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);
  
  return ar_idtable_has (ca->laws, id);
}

sas_law *
ar_ca_get_law (ar_ca *ca, ar_identifier *id)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);
  
  return ar_idtable_get (ca->laws, id);
 }

int 
ar_ca_get_number_of_laws (ar_ca *ca)
{
  ccl_pre (ca != NULL);
  
  if (ca->laws == NULL)
    return 0;
  
  return ar_idtable_get_size (ca->laws);
}

void
ar_ca_add_observer (ar_ca *ca, ar_identifier *id, ar_ca_expr *obs)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);
  ccl_pre (obs != NULL);

  if (ca->observers == NULL)
    {
      ca->obsids = ccl_list_create ();
      ca->observers =
	ar_idtable_create (1, (ccl_duplicate_func *) ar_ca_expr_add_reference,
			   (ccl_delete_proc *) ar_ca_expr_del_reference);
    }
  if (! ar_idtable_has (ca->observers, id))
    ccl_list_add (ca->obsids, id);
  ar_idtable_put (ca->observers, id, obs);
}

ar_ca_expr *
ar_ca_get_observer (ar_ca *ca, ar_identifier *id)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);
  ccl_pre (ca->observers != NULL);
  
  return ar_idtable_get (ca->observers, id);
}

ar_identifier_iterator *
ar_ca_get_observer_ids (ar_ca *ca)
{
  ccl_pre (ca != NULL);
  ccl_pre (ca->observers != NULL);
  
  return (ar_identifier_iterator *) 
    ccl_list_get_iterator (ca->obsids,
			   (ccl_duplicate_func *) ar_identifier_add_reference,
			   (ccl_delete_proc *) ar_identifier_del_reference);
}

int 
ar_ca_get_number_of_observers (ar_ca *ca)
{
  ccl_pre (ca != NULL);
  
  if (ca->observers == NULL)
    return 0;
  
  return ar_idtable_get_size (ca->observers);
}

void 
ar_ca_add_preemptible (ar_ca *ca, ar_identifier *id)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);

  if (ca->preemptibles == NULL)
    ca->preemptibles =
      ccl_set_create_for_objects (NULL, NULL, (ccl_delete_proc *)
				  ar_identifier_del_reference);
  if (! ccl_set_has (ca->preemptibles, ar_identifier_add_reference (id)))
    ccl_set_add (ca->preemptibles, ar_identifier_add_reference (id));
}

ccl_pointer_iterator *
ar_ca_get_preemptibles (ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ccl_set_get_elements (ca->preemptibles);
}

int 
ar_ca_is_preemptible (ar_ca *ca, ar_identifier *id)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);
  
  return (ca->preemptibles != NULL) && ccl_set_has (ca->preemptibles, id);
}

void
ar_ca_add_bucket (ar_ca *ca, ar_identifier *id, ccl_list *events,
		  ccl_list *probas)
{
  sas_double sum = 0.0;
  sas_law_param *last;
  ccl_pair *pe = FIRST (events);
  ccl_pair *pp = FIRST (probas);
  ccl_list *b = ccl_list_deep_dup (events, (ccl_duplicate_func *)
				   ar_identifier_add_reference);
  if (ca->buckets == NULL)
    {
      ca->buckets = ccl_list_create ();
      ca->bucket_event_probas =
	ar_idtable_create (1,
			   (ccl_duplicate_func *) sas_law_param_add_reference,
			   (ccl_delete_proc *) sas_law_param_del_reference);
      ca->event_to_bucket_table = ar_idtable_create (0, NULL, NULL);
    }

  ccl_list_add (ca->buckets, b);
  ccl_list_put_first (b, ar_identifier_add_reference (id));
		      
  ccl_pre (ccl_list_get_size (probas) == ccl_list_get_size (events) - 1);

  for (; pp; pp = CDR (pp), pe = CDR (pe))
    {
      ccl_pre (! ar_idtable_has (ca->bucket_event_probas, CAR (pe)));
      ar_idtable_put (ca->bucket_event_probas, CAR (pe), CAR (pp));
      ar_idtable_put (ca->event_to_bucket_table, CAR (pe), b);
      sum += sas_law_param_get_value (CAR (pp), NULL);
      ccl_pre (sum < 1.0);
    }
  ccl_pre (! ar_idtable_has (ca->bucket_event_probas, CAR (pe)));
  
  last = sas_law_param_crt_constant (1.0 - sum);
  ar_idtable_put (ca->bucket_event_probas, CAR (pe), last);
  ar_idtable_put (ca->event_to_bucket_table, CAR (pe), b);
  
  sas_law_param_del_reference (last);
}

const ccl_list *
ar_ca_get_buckets (ar_ca *ca)
{
  ccl_pre (ca != NULL);

  return ca->buckets;
}

sas_law_param *
ar_ca_get_bucket_proba (ar_ca *ca, const ar_identifier *id)
{
  sas_law_param *result;
  
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);

  if (ca->bucket_event_probas != NULL)
    result = ar_idtable_get (ca->bucket_event_probas, id);
  else
    result = NULL;
  
  return result;
}

ccl_list *
ar_ca_get_bucket (ar_ca *ca, const ar_identifier *id)
{
  ccl_list *result;
  
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);

  if (ca->event_to_bucket_table != NULL)
    result = ar_idtable_get (ca->event_to_bucket_table, id);
  else
    result = NULL;
  
  return result;
}

void
ar_ca_add_priority (ar_ca *ca, ar_identifier *id, int pl)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);	   

  if (ca->priorities == NULL)
    ca->priorities = ar_idtable_create (1, NULL, NULL);
  ar_idtable_put (ca->priorities, id, (void *) (intptr_t) pl);
}

int
ar_ca_has_priority (ar_ca *ca, ar_identifier *id)
{
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);	   

  return ca->priorities != NULL && ar_idtable_has (ca->priorities, id);
}

int
ar_ca_get_priority (ar_ca *ca, ar_identifier *id)
{
  int result;
  
  ccl_pre (ca != NULL);
  ccl_pre (id != NULL);	   

  if (ca->priorities == NULL)
    result = 0;
  else
    result = (intptr_t) ar_idtable_get (ca->priorities, id);
  return result;
}

const ar_ca *
ar_ca_get_reduced (ar_ca *ca)
{
  ccl_pre (ca != NULL);

  if (ca->reduced == NULL)
    {
      const fdset *fds = ar_ca_get_fdset (ca);
      ca->reduced = ar_ca_remove_functional_dependencies (ca, fds);
    }

  return ca->reduced;
}

			/* --------------- */

static ar_ca_domain *
s_clone_domain (ar_ca_exprman *dstman, ar_ca_exprman *srcman, 
		const ar_ca_domain *dom)
{
  ar_ca_domain *result;

  if (ar_ca_domain_is_boolean (dom))
    result = ar_ca_domain_create_bool ();
  else if (ar_ca_domain_is_integer (dom))
    {
      ar_bounds bounds;
      ar_ca_domain_get_bounds (dom, &bounds);
      result = ar_ca_domain_create_range (bounds.min, bounds.max);
    }
  else
    {
      int i;
      int card = 0;
      const int *elements = ar_ca_domain_get_enum_values (dom, &card);
      int *new_elements = ccl_new_array (int, card);
						    
      for (i = 0; i < card; i++)
	{
	  ar_ca_expr *src_cst = 
	    ar_ca_expr_get_enum_constant_by_index (srcman, elements[i]);
	  ar_identifier *val = ar_ca_expr_enum_constant_get_name (src_cst);
	  ar_ca_expr *cst = ar_ca_expr_crt_enum_constant (dstman, val);
	  ar_identifier_del_reference (val);
	  ar_ca_expr_del_reference (src_cst);
	    
	  new_elements[i] = ar_ca_expr_enum_constant_get_index (cst);	  
	  ar_ca_expr_del_reference (cst);	  
	}
      result = ar_ca_domain_create_enum (new_elements, card);
      ccl_delete (new_elements);
    }

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_clone_expr (ar_ca_exprman *man, ar_ca_expr *e, ccl_hash *cache)
{
  ar_ca_expr *R = NULL;

  if (ccl_hash_find (cache, e))
    R = ccl_hash_get (cache);
  else
    {
      ar_ca_expression_kind kind = ar_ca_expr_get_kind (e);
 
      if (kind == AR_CA_CST)
	{
	  int val = ar_ca_expr_get_min (e);

	  if (ar_ca_expr_is_boolean_expr (e))
	    R = ar_ca_expr_crt_boolean_constant (man, val);
	  else if (ar_ca_expr_is_integer_expr (e))
	    R = ar_ca_expr_crt_integer_constant (man, val);
	  else
	    {
	      ar_identifier *id = ar_ca_expr_enum_constant_get_name (e);
	      R = ar_ca_expr_crt_enum_constant (man, id);
	      ar_identifier_del_reference (id);
	    }
	}
      else if (kind == AR_CA_VAR)
	{
	  ar_ca_exprman *eman = ar_ca_expr_get_manager (e);
	  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (e);
	  ar_ca_domain *ndom = s_clone_domain (man, eman, dom);
	  ccl_list *attrs = ar_ca_expr_variable_get_attributes (e);
	  ar_identifier *name = ar_ca_expr_variable_get_name (e);
	  R = ar_ca_expr_crt_variable (man, name, ndom, attrs);
	  ar_identifier_del_reference (name);
	  ar_ca_domain_del_reference (ndom);
	  ar_ca_exprman_del_reference (eman);
	}
      else
	{
	  int i;
	  ar_ca_expr **src_args = ar_ca_expr_get_args (e);
	  ar_ca_expr *args[3] = { NULL, NULL, NULL };

	  for (i = 0; src_args[i] && i < 3 ; i++) 
	    args[i] = s_clone_expr (man, src_args[i], cache);

	  switch (kind) 
	    {
	    case AR_CA_CST: 
	    case AR_CA_VAR: 
	      ccl_throw_no_msg (internal_error);
	      break;

	    case AR_CA_NEG:
	      R = ar_ca_expr_crt_neg (args[0]);
	      break;

	    case AR_CA_NOT:
	      R = ar_ca_expr_crt_not (args[0]);
	      break;

	    case AR_CA_AND: 
	      R = ar_ca_expr_crt_and (args[0], args[1]);
	      break;

	    case AR_CA_OR:
	      R = ar_ca_expr_crt_or (args[0], args[1]);
	      break;

	    case AR_CA_ADD: 
	      R = ar_ca_expr_crt_add (args[0], args[1]);
	      break;

	    case AR_CA_MUL:
	      R = ar_ca_expr_crt_mul (args[0], args[1]);
	      break;

	    case AR_CA_DIV:
	      R = ar_ca_expr_crt_div (args[0], args[1]);
	      break;

	    case AR_CA_MOD:
	      R = ar_ca_expr_crt_mod (args[0], args[1]);
	      break;

	    case AR_CA_EQ:
	      R = ar_ca_expr_crt_eq (args[0], args[1]);
	      break;
	    
	    case AR_CA_LT:
	      R = ar_ca_expr_crt_lt (args[0], args[1]);
	      break;

	    case AR_CA_MIN:
	      R = ar_ca_expr_crt_min (args[0], args[1]);
	      break;

	    case AR_CA_MAX:
	      R = ar_ca_expr_crt_max (args[0], args[1]);
	      break;

	    case AR_CA_ITE: 
	      R = ar_ca_expr_crt_ite (args[0], args[1], args[2]);
	      break;
	    }

	  for(i = 0; i < 3 ; i++) 
	    ccl_zdelete (ar_ca_expr_del_reference, args[i]);
	}

      ccl_hash_find (cache, e);
      ccl_hash_insert (cache, R);
    }
  ccl_post (R != NULL);

  return ar_ca_expr_add_reference (R);
}

			/* --------------- */

static void
s_clone_expr_list (ar_ca_exprman *dstman, ccl_list *dst, ccl_list *src,
		   ccl_hash *cache)
{
  ccl_pair *p;

  ccl_pre (ccl_list_is_empty (dst));
  
  for (p = FIRST (src); p; p = CDR (p))
    ccl_list_add (dst, s_clone_expr (dstman, CAR (p), cache));
}

			/* --------------- */

static ar_ca_trans *
s_clone_transition (ar_ca_exprman *man, ar_ca_trans *t, ccl_hash *cache)
{
  int i;
  ar_ca_expr *g = ar_ca_trans_get_guard (t);
  ar_ca_expr *ng = s_clone_expr (man, g, cache);
  ar_ca_event *e = ar_ca_trans_get_event (t);
  int nb_assign = ar_ca_trans_get_number_of_assignments (t);
  ar_ca_expr **assign = ar_ca_trans_get_assignments (t);
  ar_ca_trans *result = ar_ca_trans_create (ng, e);
  ar_ca_expr_del_reference (g);
  ar_ca_expr_del_reference (ng);
  ar_ca_event_del_reference (e);

  for (i = 0; i < nb_assign; i++, assign += 2)
    {
      ar_ca_expr *v = s_clone_expr (man, assign[0], cache);
      ar_ca_expr *val = s_clone_expr (man, assign[1], cache);
      ar_ca_trans_add_assignment (result, v, val);
      ar_ca_expr_del_reference (v);
      ar_ca_expr_del_reference (val);
    }

  return result;
}

			/* --------------- */

ar_ca *
ar_ca_clone (ar_ca *ca)
{
  int i;
  ar_ca *result = ccl_new (ar_ca);
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				     ar_ca_expr_del_reference);

  result->refcount = 1;
  result->node = ar_node_add_reference (ca->node);
  result->man = ar_ca_exprman_create ();
  result->hierarchy = ccl_list_deep_dup (ca->hierarchy, (ccl_duplicate_func *)
					 ar_identifier_add_reference);
  s_clone_expr_list (result->man, result->flow_variables, 
		     ca->flow_variables, cache);
  s_clone_expr_list (result->man, result->state_variables,
		     ca->state_variables, cache);
  s_clone_expr_list (result->man, result->assertions, ca->assertions, cache);  
  s_clone_expr_list (result->man, result->initial_assignments, 
		     ca->initial_assignments, cache);
  s_clone_expr_list (result->man, result->initial_constraints, 
		     ca->initial_constraints, cache);
  
  ccl_array_init_with_size (result->events, ca->events.size);
  for (i = 0; i < ca->events.size; i++)
    result->events.data[i] = ar_ca_event_add_reference (ca->events.data[i]);

  ccl_array_init_with_size (result->transitions, ca->transitions.size);
  for (i = 0; i < ca->transitions.size; i++)
    result->transitions.data[i] = 
      s_clone_transition (result->man, ca->transitions.data[i], cache);
  ccl_hash_delete (cache);
  result->reduced = NULL;
  result->fds = NULL;
  result->epsilon = NULL;
  
  return result;
}

			/* --------------- */

struct array_of_pointers_iterator {
  ccl_pointer_iterator super;
  array_of_pointers *a;
  int index;
};

			/* --------------- */

static int 
s_aopi_has_more_elements (const ccl_pointer_iterator *i)
{
  struct array_of_pointers_iterator *aopi = 
    (struct array_of_pointers_iterator *) i;

  return  (aopi->index < aopi->a->size);
}

			/* --------------- */

static void *
s_aopi_next_element (ccl_pointer_iterator *i)
{
  struct array_of_pointers_iterator *aopi = 
    (struct array_of_pointers_iterator *) i;
  void *result = aopi->a->data[aopi->index];
  aopi->index++;

  return result;
}

			/* --------------- */

static void
s_aopi_delete_iterator (ccl_pointer_iterator *i)
{
  ccl_delete (i);
}

			/* --------------- */

static ccl_pointer_iterator *
s_create_iterator_from_array (array_of_pointers *a)
{
  struct array_of_pointers_iterator *result = 
    ccl_new (struct array_of_pointers_iterator);
  result->super.has_more_elements = s_aopi_has_more_elements;
  result->super.next_element = s_aopi_next_element;
  result->super.delete_iterator = s_aopi_delete_iterator;
  result->a = a;
  result->index = 0;

  return (ccl_pointer_iterator *) result;
}

			/* --------------- */

static void
s_expr_log (ccl_log_type log, void *var, void *data)
{
  ar_ca_expr_log (log, var);
}

			/* --------------- */

varorder *
ar_ca_build_varorder (ar_ca *ca)
{
  const ccl_pair *p;
  int i;
  varorder *result = 
    varorder_create ((ccl_delete_proc *) ar_ca_expr_del_reference, &s_expr_log,
		     NULL);
  const ccl_list *vars = ar_ca_get_flow_variables (ca);
    
  for (i = 0; i < 2; i++)
    {
      for (p = FIRST (vars); p; p = CDR (p))
	{
	  ar_ca_expr *var = ar_ca_expr_add_reference (CAR (p));
	  varorder_add_variable (result, var);
	}
      vars = ar_ca_get_state_variables (ca);
    }

  return result;
}

			/* --------------- */

const fdset *
ar_ca_get_fdset (ar_ca *ca)
{
  ccl_pre (ca != NULL);

  if (ca->fds == NULL)
    ca->fds = fdset_compute (ca, 1, 0);
  return ca->fds;
}

static int 
s_check_declared_variables (ccl_hash *declvars, ccl_list *vars)
{
  int result = 1;

  while (result && ! ccl_list_is_empty (vars))
    {
      ar_ca_expr *v = ccl_list_take_first (vars);
      result = ccl_hash_find (declvars, v);
      
      if (!result && CA_DBG_IS_ON)
	{
	  ccl_debug ("missing declaration of ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, v);
	  ccl_debug ("\n");
	}
    }
  ccl_list_delete (vars);

  return result;
}

int 
ar_ca_check (const ar_ca *ca)
{
  int i;
  int result = 1;
  ccl_hash *declvars = ar_ca_get_variables (ca, AR_CA_ALL_VARS);
  const ccl_list *elists[] = { ca->assertions, ca->initial_constraints,
			       ca->initial_assignments };
  const int nb_elists = sizeof (elists)/sizeof (elists[0]);

  /* check that variables used in expressions are declared */
  
  for (i = 0; i < nb_elists; i++)
    {
      ccl_pair *p;
      const ccl_list *l = elists[i];

      for (p = FIRST (l); result && p; p = CDR (p))
	{
	  ccl_list *vars = ar_ca_expr_get_variables (CAR (p), NULL);
	  result = s_check_declared_variables (declvars, vars);
	}
    }

  for (i = 0; result && i < ca->transitions.size; i++)
    {
      ar_ca_trans *t = ca->transitions.data[i];
      ccl_list *vars = ar_ca_trans_get_variables (t, NULL, AR_CA_RW_VARS);
      result = s_check_declared_variables (declvars, vars);
    }


  if (result && ca->fds != NULL)
    {
      ccl_list *V = fdset_get_ordered_variables (ca->fds, 0);
      while (! ccl_list_is_empty (V) && result) 
	{
	  ar_ca_expr *v = ccl_list_take_first (V);
	  funcdep *Fv = fdset_get_funcdep (ca->fds, v);
	  ccl_list *vars = ccl_list_dup (funcdep_get_inputs (Fv));
	  ccl_list_add (vars, v);
	  result = s_check_declared_variables (declvars, vars);
	}
      ccl_list_delete (V);
    }
  ccl_hash_delete (declvars);

  return result;
}
