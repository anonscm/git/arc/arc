/*
 * obfuscation-sequential.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "obfuscation-p.h"

typedef struct sequential_obfuscation_st
{
  obfuscation super;
  int counter;  
} sequential_obfuscation;

			/* --------------- */

static char * 
s_sequential_id (obfuscation *o, ar_identifier_context ctx, const char *id)
{
  sequential_obfuscation *so = (sequential_obfuscation *) o;

  return ccl_string_format_new ("_%d", so->counter++);
}

			/* --------------- */

obfuscation *
obfuscation_crt_sequential (void)
{
  obfuscation *result = 
    obfuscation_create (sizeof (sequential_obfuscation), s_sequential_id, NULL);

  ((sequential_obfuscation *) result)->counter = 0;

  return result;
}
