/*
 * ar-idpath.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_IDPATH_H
# define AR_IDPATH_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-string.h>

typedef struct ar_idpath_st *ar_idpath;

extern ar_idpath
ar_idpath_create(ccl_ustring id);

extern void
ar_idpath_append_identifier(ar_idpath idp, ccl_ustring id);

extern void
ar_idpath_append_position(ar_idpath idp, uint32_t pos);

extern ar_idpath
ar_idpath_duplicate(ar_idpath idp);

extern ar_idpath
ar_idpath_add_reference(ar_idpath idp);

extern void
ar_idpath_del_reference(ar_idpath idp);

extern void
ar_idpath_log(ccl_log_type log, ar_idpath idp);

#endif /* ! AR_IDPATH_H */
