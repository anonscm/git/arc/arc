/*
 * ar-event-poset.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "ar-event-poset.h"

ar_event_poset *
ar_event_poset_create(void)
{
  return ar_poset_create ((ccl_hash_func *) ar_event_hash,
			  (ccl_duplicate_func *) ar_event_add_reference,
			  (ccl_compare_func *) ar_event_compare,
			  (ccl_delete_proc *) ar_event_del_reference);
}
