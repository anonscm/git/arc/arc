/*
 * arc.c -- Main routine of ARC.
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif
#include <unistd.h>

#include "arc.h"
#include <ccl/ccl-exception.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-config-table.h>
#include "ar-help.h"
#include "arc-shell.h"
#include "arc-readline.h"
#include "arc-shell-preferences.h"
#include "ar-util.h"
#include "arc-debug.h"

#define BANNER_MANPAGE "banner"
#define VERSION_MANPAGE "version"

static void
usage(void);

static int
s_parse_options(int argc, char **argv);

static int OPT_CR_MODE = 0;
static int OPT_HELP = 0;
static int OPT_VERSION = 0;
static int OPT_BATCH_MODE = 0;

int
main (int argc, char **argv)
{
  int result = 0;

#ifndef CCL_ENABLE_ASSERTIONS
  ccl_try(exception) 
#endif /* CCL_ENABLE_ASSERTIONS */
    { 
      int filename_index;
      int verbose;

      arc_init ();     
      filename_index = s_parse_options (argc, argv);

      verbose = 
	ccl_config_table_get_boolean (ARC_PREFERENCES, ARC_SHELL_VERBOSE);

      if (OPT_HELP) 
	usage ();
      else if (OPT_VERSION)
	{
	  if (verbose)
	    ar_help_log_manpage (CCL_LOG_DISPLAY, VERSION_MANPAGE);
	  else
	    ccl_display (PACKAGE_STRING);
	}
      else 
	{
	  int i;
	  char *history_file = 
	    ar_tilde_substitution(ccl_config_table_get(ARC_PREFERENCES,
						       ARC_SHELL_HISTORY_FILE));
	  arc_readline_init (history_file, ! (OPT_CR_MODE ||OPT_BATCH_MODE));
	  ccl_string_delete (history_file);

	  if (verbose)
	    ar_help_log_manpage (CCL_LOG_DISPLAY, BANNER_MANPAGE);

	  for (i = filename_index; i < argc; i++)
	    {
	      if (strcmp (argv[i], "-c") == 0)
		{
		  if (i < argc - 1)
		    {
		      arc_shell_interp_script_string (argv[i + 1]);
		      i++;
		    }
		  else
		    {
		      ccl_throw (exception, "missing argument to -c option\n");
		    }
		}
	      else if (strcmp (argv[i], "-C") == 0)
		{
		  if (chdir (argv[i + 1]) != 0)
		    {
		      ccl_error ("can not move to directory '%s'\n",
				 argv[i + 1]);
		      ccl_throw_no_msg (exception);
		    }
		  i++;
		}
	      else
		{
		  if (!arc_shell_load_file (argv[i], AR_INPUT_ANY) &&
		      OPT_BATCH_MODE)
		    result = 1;
		}
	    }

	  if (! OPT_BATCH_MODE)
	    arc_shell_interactive_loop (OPT_CR_MODE);
	  arc_readline_terminate();  
	}

      arc_terminate ();
    }
#ifndef CCL_ENABLE_ASSERTIONS
  ccl_catch
    {
      fprintf(stderr,"ARC termination on error : %s\n",
	      ccl_exception_current_message);
      result = 1;
    }
  ccl_end_try;
#endif /* CCL_ENABLE_ASSERTIONS */

  return result;
}

static void
usage(void)
{
  fprintf(stderr,
	  "usage: arc [options] [file_1|-c cmd] ... [file_n|-c cmd]\n"
	  "with options:\n"
	  "-b : don't start the interactive mode after the load of files "
	  " file_i.\n"
	  "-c cmd : execute command 'cmd'\n"
	  "-d : display debug traces\n"
	  "-q : quiet mode\n"
	  "-x : command/response mode\n"
	  "-h|--help : print this help message.\n"
	  "-V|--version : display version of this program.\n"
	  "-l logfile : log all operations into the file logfile\n"
	  "\n");
}

static int
s_parse_options (int argc, char **argv)
{
  int i;

  for (i = 1; i < argc; i++)
    {
      char *optstr = argv[i];

      if (*optstr != '-')
	break;
      optstr++;

      if (*optstr == '-')
	{
	  optstr++;

	  if (strcmp (optstr, "version") == 0)
	    OPT_VERSION = 1; 
	  else if (strcmp (optstr, "help") == 0)
	    {
	      OPT_HELP = 1;
	      return argc;
	    }
	  else
	    {
	      ccl_error ("unknown option '--%s'.\n", optstr);
	      ccl_throw_no_msg (exception);
	    }
	}
      else
	{
	  while (*optstr)
	    {
	      switch (*optstr)
		{
		case 'V':
		  OPT_VERSION = 1; 
		  break;

		case 'b': 
		  OPT_BATCH_MODE = 1; 
		  break;
		case 'C': 
		  if (i < argc - 1)
		    {
		      if (chdir (argv[i + 1]) != 0)
			{
			  ccl_error ("can not move to directory '%s'\n",
				     argv[i + 1]);
			  ccl_throw_no_msg (exception);
			}
		      i++;
		    }
		  else
		    {
		      ccl_throw (exception, "missing argument to -c option\n");
		    }
		  break;
		case 'h': 
		  OPT_HELP = 1; 
		  return argc;
		case 'x': 
		  OPT_CR_MODE = 1; 
		  break;
		case 'd':
		  ccl_config_table_set (ARC_PREFERENCES, "debug.max-log-level",
					"-1");
		  arc_debug_update_flags (ARC_PREFERENCES);
		  break;
		case 'q':
		  ccl_config_table_set (ARC_PREFERENCES, ARC_SHELL_VERBOSE, 
					"false");
		  break;
		case 'c':
		  if (i < argc - 1)
		    {
		      arc_shell_interp_script_string (argv[i + 1]);
		      i++;
		    }
		  else
		    {
		      ccl_throw (exception, "missing argument to -c option\n");
		    }
		  break;
		case 'l':
		  if (i < argc - 1)
		    {
		      FILE *lf = fopen (argv[i + 1], "w");
		      if (lf == NULL)
			ccl_throw (exception, "can not open logfile.");
		      arc_set_logfile (lf);
		      i++;
		    }
		  else
		    {
		      ccl_throw(exception,"invalid option");
		    }
		  break;
		default :	      
		  ccl_throw(exception,"invalid option");
		}
	      optstr++;
	    }
	}
    }

  return i;
}

