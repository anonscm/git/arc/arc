/*
 * ar-flat-semantics.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_FLAT_SEMANTICS_H
# define AR_FLAT_SEMANTICS_H

# include "ar-interp-altarica-exception.h"
# include "ar-node.h"

extern int ar_flattening_debug_is_on;
extern int ar_flattening_debug_show_fd;
extern int ar_flattening_debug_log_vectors;

extern ar_node *
ar_compute_flat_semantics(ar_node *node, ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception);

#endif /* ! AR_FLAT_SEMANTICS_H */
