/*
 * ar-ca-to-sas.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_TO_SAS_H
# define AR_CA_TO_SAS_H

# include <sas/sas-model.h>
# include "ar-constraint-automaton.h"

extern int sas_debug_ca_bytecodes;
extern int sas_debug_ca_checker;
extern int sas_debug_ca_bytecode_exec;
extern int sas_debug_ca_bytecodes_lengths;

extern sas_model *
ar_ca_to_sas_model (ar_ca *ca);

extern void
ar_ca_destroy_sas_model (sas_model *model);

#endif /* ! AR_CA_TO_SAS_H */
