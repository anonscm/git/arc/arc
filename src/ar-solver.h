/*
 * ar-solver.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_SOLVER_H
# define AR_SOLVER_H

# include "ar-ca-expression.h"
# include "ar-backtracking-stack.h"

typedef struct ar_solver_st ar_solver;
typedef void
ar_solver_start_solution_se_proc(ar_solver *solver, void *clientdata);
typedef int
ar_solver_new_solution_proc(ar_solver *solver, void *clientdata);

typedef void
ar_solver_end_solution_set_proc(ar_solver *solver,void *clientdata);


extern ar_solver *
ar_create_gp_solver (ar_ca_expr **constraints, int nb_constraints,
		     ar_ca_expr **variables, int nb_variables,
		     ar_backtracking_stack *bstack,
		     ar_solver_start_solution_se_proc *set_start,
		     ar_solver_new_solution_proc *set_add,
		     ar_solver_end_solution_set_proc *set_end);

extern ar_solver *
ar_create_glucose_solver (ar_ca_expr **constraints, int nb_constraints,
			  ar_ca_expr **variables, int nb_variables,
			  ar_backtracking_stack *bstack,
			  ar_solver_start_solution_se_proc *set_start,
			  ar_solver_new_solution_proc *set_add,
			  ar_solver_end_solution_set_proc *set_end);

extern void
ar_solver_delete(ar_solver *solver);

extern void
ar_solver_solve(ar_solver *solver, void *clientdata);

extern ar_ca_expr **
ar_solver_get_variables(ar_solver *solver, int *psize);

/*!
 * Returns the number of solutions of the set constraints.
 * If @limit is < 0 then the exact size if computed even if the number of
 * solutions is large.
 * If @limit is >= 0 then if the number of solutions is in the range [0,@limit]
 * then this cardinality is returned; else @limit + 1 is returned. 
 */
extern int
ar_solver_count_solutions (ar_ca_expr **constraints, int nb_constraints,
			   int limit);

#endif /* ! AR_SOLVER_H */
