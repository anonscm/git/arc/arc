/*
 * ar-interp-mec5.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_INTERP_MEC5_H
# define AR_INTERP_MEC5_H

# include <ccl/ccl-config-table.h>
# include "parser/altarica-tree.h"

extern int
ar_interp_mec5_tree (altarica_tree *t, ccl_config_table *conf);

extern int
ar_interp_mec5_equation_system (altarica_tree *t, ccl_config_table *conf, 
				int loginfo);

#endif /* ! AR_INTERP_MEC5_H */
