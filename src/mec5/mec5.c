/*
 * mec5.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-config-table.h>
#include "ar-semantics.h"
#include "ar-model.h"
#include "ar-dd.h"
#include "mec5-preferences.h"
#include "mec5.h"

static int init_counter = 0;

			/* --------------- */

ar_context *MEC5_CONTEXT = NULL;
ar_ddm MEC5_DDM = NULL;
static ar_idtable *MEC5_RELATIONS = NULL;

			/* --------------- */

const char *AR_MEC5_PREF_NAMES[] = {
#define AR_MEC5_PREF(_enumid, _strid) "mec5." _strid,
#include "mec5-preferences.def"
#undef AR_MEC5_PREF
};

# define AR_MEC5_PREF(_enumid, _strid) 	\
  const char *MEC5_PREF_ ## _enumid ## _STR = "mec5." _strid; 
# include "mec5-preferences.def"
# undef AR_MEC5_PREF

			/* --------------- */

int
ar_mec5_init (ccl_config_table *conf)
{
  if (init_counter++)
    return 1;

  MEC5_DDM = DDM;
  MEC5_CONTEXT = ar_context_create (AR_MODEL_CONTEXT);
  MEC5_RELATIONS = 
    ar_idtable_create (1,
		       (ccl_duplicate_func *) ar_mec5_relation_add_reference,
		       (ccl_delete_proc *) ar_mec5_relation_del_reference);

  return 1;
}

			/* --------------- */

void
ar_mec5_terminate (void)
{
  ccl_pre (init_counter > 0);

  init_counter--;
  if (init_counter > 0)
    return;

  ar_idtable_del_reference (MEC5_RELATIONS);
  ar_context_del_reference (MEC5_CONTEXT);
}

			/* --------------- */

ar_mec5_relation *
ar_mec5_get_relation (ar_identifier *name)
{
  ccl_pre (MEC5_RELATIONS != NULL);
  ccl_pre (name != NULL);
  
  return ar_idtable_get (MEC5_RELATIONS, name);
}

			/* --------------- */

static int
s_is_bang_id (ar_identifier *name, ar_node **p_n, ar_bang_t *p_bang)
{
  int result = 0;
  char *s = ar_identifier_to_string (name);
  char *bang = strrchr (s, '!');

  if (bang == NULL)
    goto end;
  
  *bang++ = '\0';
  name = ar_identifier_create (s);
  if (ar_model_has_node (name) && ar_bang_str_to_bang_t (bang, p_bang))
    {
      *p_n = ar_model_get_node (name);
      result = 1;
    }
  ar_identifier_del_reference (name);

 end:
  ccl_string_delete (s);

  return result;
}

			/* --------------- */

int
ar_mec5_find_or_add_bang_relation (ar_identifier *name)
{
  ar_node *n = NULL;
  ar_bang_t bang;
  int result = s_is_bang_id (name, &n, &bang);

  if (result)
    {
      ccl_assert (n != NULL);
      ccl_try (exception)
        {
	  ar_mec5_build_bang_relation (n, bang);
        }
      ccl_catch
	{
	  if (ccl_exception_is_raised (domain_cardinality_exception))
	    {
	      ccl_error ("Can't compute relation '");
	      ar_identifier_log (CCL_LOG_ERROR, name);
	      ccl_error ("'. Some variables have a domain with a "
			 "cardinality exceeding %d.\n", AR_DD_MAX_ARITY);
	    }
	  else
	    ccl_rethrow ();
	}
      ccl_end_try;
      result = ar_idtable_has (MEC5_RELATIONS, name);
      ar_node_del_reference (n);
    }

  return result;
}

			/* --------------- */

int
ar_mec5_has_relation (ar_identifier *name)
{
  ccl_pre (MEC5_RELATIONS != NULL);
  ccl_pre (name != NULL);

  return (ar_idtable_has (MEC5_RELATIONS, name) ||
	  ar_mec5_find_or_add_bang_relation (name));
}

			/* --------------- */

int
ar_mec5_has_relation_no_bang_creation (ar_identifier *name)
{
  ccl_pre (MEC5_RELATIONS != NULL);
  ccl_pre (name != NULL);

  return ar_idtable_has (MEC5_RELATIONS, name);
}

void
ar_mec5_add_relation (ar_identifier *name, ar_mec5_relation *mrel)
{
  ccl_pre (MEC5_RELATIONS != NULL);
  ccl_pre (mrel != NULL);
  ccl_pre (name != NULL);

  ar_idtable_put (MEC5_RELATIONS, name, mrel);
}

			/* --------------- */

void
ar_mec5_remove_relation (ar_identifier *name)
{
  ccl_pre (MEC5_RELATIONS != NULL);
  ccl_pre (name != NULL);
  
  ar_idtable_remove (MEC5_RELATIONS, name);
}

			/* --------------- */

ccl_list *
ar_mec5_get_relations (void)
{
  ccl_pre (MEC5_RELATIONS != NULL);
  
  return ar_idtable_get_list_of_keys (MEC5_RELATIONS);
}


