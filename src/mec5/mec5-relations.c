/*
 * ar-mec5-relation.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-model.h"
#include "ar-semantics.h"
#include "ar-state-graph-semantics.h"
#include "ar-rel-semantics.h"
#include "ar-bang-ids.h"
#include "mec5-acheck-rels.h"
#include "mec5.h"

typedef struct ar_mec5_relation_arg_st ar_mec5_relation_arg;
struct ar_mec5_relation_arg_st 
{
  ar_identifier *name;
  int index;
  ar_mec5_relation_arg *alias;
  ar_type *type;
};

struct ar_mec5_relation_st
{
  int refcount;
  ar_dd *rels;
  int nb_rels;
  int nb_args;
  int recompute_aliases;
  int max_index;
  ar_mec5_relation_arg *args;
};

			/* --------------- */

struct display_data
{
  ar_mec5_relation *mrel;
  ccl_log_type log;
  ar_dd zero;
  ar_dd one;
  int *min;
  int *max;
};

			/* --------------- */

#define REL_INDEX_OF_ARG(rel, arg) ((int)((arg)-&((rel)->args[0])))

#define ITH_ARG(rel, i)						\
  ((ccl_pre (0 <= (i) && (i) < ar_mec5_relation_get_arity (rel))), \
     &((rel)->args[(i)]))

			/* --------------- */

static ccl_list *
s_get_ordered_arg_list (ar_mec5_relation *mrel, int with_aliases);

static void
s_compute_aliases (ar_mec5_relation *mrel);

			/* --------------- */

ar_mec5_relation *
ar_mec5_relation_create (void)
{
  ar_mec5_relation *result = ccl_new (ar_mec5_relation);

  result->refcount = 1;
  result->rels = NULL;
  result->nb_rels = 0;
  result->nb_args = 0;
  result->args = ccl_new_array (ar_mec5_relation_arg, 1);
  result->max_index = -1;
  result->recompute_aliases = 1;

  return result;
}

			/* --------------- */

static ar_dd 
s_pick_one_element (ar_ddm ddm, ccl_pair *P, ar_dd rel)
{
  ar_dd result;

  if (ar_dd_zero (ddm) == rel)
    {
      result = ar_dd_zero (ddm);
      result = AR_DD_DUP (result);
    }
  else if (P == NULL)
    {
      ccl_assert (rel == ar_dd_one (ddm));
      result = ar_dd_one (ddm);
      result = AR_DD_DUP (result);
    }
  else
    {  
      int i;
      ar_dd zero = ar_dd_zero (ddm);
      ar_mec5_relation_arg *arg = CAR (P);
      int arity = ar_type_get_cardinality (arg->type);
      ar_ddcode encoding = (arity > 2) ? COMPACTED : EXHAUSTIVE;
      ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

      if (rel == ar_dd_one (ddm) || arg->index < AR_DD_INDEX (rel))
	{
	  ar_dd son = s_pick_one_element (ddm, CDR (P), rel);

	  ccl_assert (son != zero);

	  if (encoding == COMPACTED)	
	    {	      
	      AR_DD_MIN_OFFSET_NTHDD (sons, 0) = 0;
	      AR_DD_MAX_OFFSET_NTHDD (sons, 0) = 0;
	      AR_DD_CNTHDD (sons, 0) = son;

	      for (i = 1; i < arity; i++)
		{
		  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
		  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
		  AR_DD_CNTHDD (sons, i) = AR_DD_DUP (zero);
		}
	    }
	  else
	    {
	      AR_DD_ENTHDD (sons, 0) = son;

	      for (i = 1; i < arity; i++)
		AR_DD_ENTHDD (sons, i) = AR_DD_DUP (zero);
	    }
	}
      else
	{
	  ccl_assert (arg->index == AR_DD_INDEX (rel));

	  if (encoding == COMPACTED)	
	    {
	      for (i = 0; i < AR_DD_ARITY (rel); i++)
		{
		  int j;
		  const int min = AR_DD_MIN_OFFSET_NTHSON (rel, i);
		  const int max = AR_DD_MAX_OFFSET_NTHSON (rel, i);
		  ar_dd son = s_pick_one_element (ddm, CDR (P), 
						  AR_DD_CPNTHSON(rel, i));

		  if (son == zero)
		    {
		      for (j = min; j <= max; j++)
			{
			  AR_DD_MIN_OFFSET_NTHDD (sons, j) = j;
			  AR_DD_MAX_OFFSET_NTHDD (sons, j) = j;
			  AR_DD_CNTHDD (sons, j) = AR_DD_DUP (zero);
			}
		      AR_DD_FREE (son);
		    }
		  else
		    {
		      AR_DD_MIN_OFFSET_NTHDD (sons, min) = min;
		      AR_DD_MAX_OFFSET_NTHDD (sons, min) = min;
		      AR_DD_CNTHDD (sons, min) = son;
		      for (j = min + 1; j < arity; j++)
			{
			  AR_DD_MIN_OFFSET_NTHDD (sons, j) = j;
			  AR_DD_MAX_OFFSET_NTHDD (sons, j) = j;
			  AR_DD_CNTHDD (sons, j) = AR_DD_DUP (zero);
			}
		      break;
		    }
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		{
		  AR_DD_ENTHDD (sons, i) = 
		    s_pick_one_element (ddm, CDR (P), AR_DD_EPNTHSON(rel, i));
		  if (AR_DD_ENTHDD (sons, i) != zero)
		    break;
		}
	      for (i++; i < arity; i++)
		AR_DD_ENTHDD (sons, i) = AR_DD_DUP (zero);
	    }	  
	}

      result = ar_dd_find_or_add (ddm, arg->index, arity, encoding, sons);
      ar_dd_free_dd_table (ddm, arity, encoding, sons);
    }

  return result;
}

			/* --------------- */

ar_mec5_relation *
ar_mec5_relation_pick_one_element (ar_mec5_relation *mrel)
{
  int i;
  ar_mec5_relation *result = ar_mec5_relation_create ();
  ar_dd rel = ar_mec5_relation_get_dd (mrel);

  for (i = 0; i < mrel->nb_args; i++)
    ar_mec5_relation_add_column (result, mrel->args[i].name, 
				 mrel->args[i].type, mrel->args[i].index);
  result->nb_rels = 1;
  result->rels = ccl_new_array (ar_dd, 1);
  if (ar_dd_zero (MEC5_DDM) == rel)
    result->rels[0] = AR_DD_DUP (rel);
  else
    {
      ccl_list *args = s_get_ordered_arg_list (mrel, 0);

      result->rels[0] = s_pick_one_element (MEC5_DDM, FIRST (args), rel);
      ccl_list_delete (args);
    }
  AR_DD_FREE (rel);

  return result;
}

			/* --------------- */

ar_mec5_relation *
ar_mec5_relation_add_reference (ar_mec5_relation *mrel)
{
  ccl_pre (mrel != NULL);
  ccl_pre (0 < mrel->refcount);

  mrel->refcount++;

  return mrel;
}

			/* --------------- */

void
ar_mec5_relation_del_reference (ar_mec5_relation *mrel)
{
  int i;

  ccl_pre (mrel != NULL);
  ccl_pre (0 < mrel->refcount);

  mrel->refcount--;
  if (mrel->refcount > 0)
    return;

  for (i = 0; i < mrel->nb_args; i++)
    {
      ar_identifier_del_reference (mrel->args[i].name);
      ar_type_del_reference (mrel->args[i].type);
    }

  if (mrel->rels != NULL)
    {
      for (i = 0; i < mrel->nb_rels; i++)
	AR_DD_FREE (mrel->rels[i]);
      ccl_delete (mrel->rels);
    }
  ccl_delete (mrel->args);
  ccl_delete (mrel);
}

			/* --------------- */

int
ar_mec5_relation_get_arity (const ar_mec5_relation *mrel)
{
  ccl_pre (mrel != NULL);

  return mrel->nb_args;
}

			/* --------------- */

void
ar_mec5_relation_get_ordered_columns (ar_mec5_relation *mrel, int *result)
{
  int i = 0;
  ccl_list *cols = s_get_ordered_arg_list (mrel, 1);

  while (!ccl_list_is_empty (cols))
    {
      ar_mec5_relation_arg *arg = ccl_list_take_first (cols);
      result[i++] = REL_INDEX_OF_ARG(mrel, arg);
    }
  ccl_list_delete (cols);
}

			/* --------------- */

void
ar_mec5_relation_add_column (ar_mec5_relation *mrel, ar_identifier *name,
			     ar_type *type, int index)
{
  ar_mec5_relation_arg *arg;

  ccl_pre (mrel != NULL);
  ccl_pre (name != NULL);
  ccl_pre (type != NULL);

  mrel->args = (ar_mec5_relation_arg *) 
    ccl_realloc (mrel->args, ((mrel->nb_args + 1) * 
			      sizeof (ar_mec5_relation_arg)));
  
  arg = &mrel->args[mrel->nb_args];
  arg->name = ar_identifier_add_reference (name);
  arg->type = ar_type_add_reference (type);
  arg->index = index;
  arg->alias = NULL;
  mrel->nb_args++;
  mrel->recompute_aliases = 1;
}

			/* --------------- */

ar_dd *
ar_mec5_relation_get_dds (const ar_mec5_relation *mrel, int *p_nb_rels)
{
  ccl_pre (mrel != NULL);

  *p_nb_rels = mrel->nb_rels;

  return mrel->rels;
}

			/* --------------- */

ar_dd 
ar_mec5_relation_get_dd (ar_mec5_relation *mrel)
{
  ccl_pre (mrel != NULL);

  if (mrel->nb_rels == 0)
    return ar_dd_dup_one (MEC5_DDM);

  if (mrel->nb_rels > 1)
    {
      int i;

      for (i = 1; i < mrel->nb_rels; i++)
	{
	  ar_dd tmp = ar_dd_compute_and (MEC5_DDM, mrel->rels[0], 
					 mrel->rels[i]);
	  AR_DD_FREE (mrel->rels[0]);
	  AR_DD_FREE (mrel->rels[i]);
	  mrel->rels[0] = tmp;
	}
      mrel->nb_rels = 1;
    }

  return AR_DD_DUP (mrel->rels[0]);
}

			/* --------------- */

int
ar_mec5_relation_set_dds (ar_mec5_relation *mrel, int nb_dds, ar_dd *dds)
{
  int diff;

  ccl_pre (mrel != NULL);
  ccl_pre (dds != NULL);
  
  diff = ((nb_dds != mrel->nb_rels) || 
	  ccl_memcmp (dds, mrel->rels, sizeof (ar_dd) * nb_dds) != 0);

  if (diff)
    {
      int i;

      for (i = 0; i < mrel->nb_rels; i++)
	AR_DD_FREE (mrel->rels[i]);
      if (nb_dds == 0)
	{
	  ccl_free (mrel->rels);
	  mrel->rels = NULL;
	}
      else if (mrel->rels == NULL)
	mrel->rels = ccl_new_array (ar_dd, nb_dds);
      else
	mrel->rels = (ar_dd *) 
	  ccl_realloc (mrel->rels, sizeof (ar_dd) * nb_dds);

      for (i = 0; i < nb_dds; i++)
	mrel->rels[i] = AR_DD_DUP (dds[i]);
      mrel->nb_rels = nb_dds;
    }

  return diff;
}

			/* --------------- */

int
ar_mec5_relation_get_max_dd_index (ar_mec5_relation *mrel)
{
  s_compute_aliases (mrel);

  return mrel->max_index;
}

			/* --------------- */

int
ar_mec5_relation_get_ith_dd_index (const ar_mec5_relation *mrel, int i)
{
  ar_mec5_relation_arg *arg = ITH_ARG (mrel, i);

  return arg->index;
}

			/* --------------- */

void
ar_mec5_relation_set_ith_dd_index (ar_mec5_relation *mrel, int i, int index)
{
  ar_mec5_relation_arg *arg = ITH_ARG (mrel, i);

  mrel->recompute_aliases = 1;
  arg->index = index;
}

			/* --------------- */

ar_identifier *
ar_mec5_relation_get_ith_arg_name (const ar_mec5_relation *mrel, int i)
{
  ar_mec5_relation_arg *arg = ITH_ARG (mrel, i);

  return ar_identifier_add_reference (arg->name);
}

			/* --------------- */

void
ar_mec5_relation_set_ith_arg_name (ar_mec5_relation *mrel, int i, 
				   ar_identifier *id)
{
  ar_mec5_relation_arg *arg = ITH_ARG (mrel, i);

  if (arg->name == id)
    return;

  ar_identifier_del_reference (arg->name);
  arg->name = ar_identifier_add_reference (id);
}

			/* --------------- */

ar_type *
ar_mec5_relation_get_ith_arg_type (const ar_mec5_relation *mrel, int i)
{
  ar_mec5_relation_arg *arg = ITH_ARG (mrel, i);

  return ar_type_add_reference (arg->type);
}

			/* --------------- */

void
ar_mec5_relation_set_ith_arg_type (ar_mec5_relation *mrel, int i, 
				   ar_type *type)
{
  ar_mec5_relation_arg *arg = ITH_ARG (mrel, i);

  if (arg->type == type)
    return;

  ar_type_del_reference (arg->type);
  arg->type = ar_type_add_reference (type);
}

			/* --------------- */

static void
s_compute_aliases (ar_mec5_relation *mrel)
{
  int i;
  ar_mec5_relation_arg *arg;
  ccl_hash *aliases;

  if (! mrel->recompute_aliases)
    return;

  mrel->max_index = -1;
  aliases = ccl_hash_create (NULL, NULL, NULL, NULL);
  for (arg = mrel->args, i = 0; i < mrel->nb_args; i++, arg++)
    {
      if (mrel->max_index < arg->index)
	mrel->max_index = arg->index;

      if (ccl_hash_find (aliases, (void *) (intptr_t) arg->index))
	arg->alias = ccl_hash_get (aliases);
      else
	{
	  ccl_hash_insert (aliases, arg);
	  arg->alias = NULL;
	}
    }
  ccl_hash_delete (aliases);
  mrel->recompute_aliases = 0;
}

			/* --------------- */

static int
s_cmp_args (const void *p1, const void *p2)
{
  return (((ar_mec5_relation_arg *) p1)->index - 
	  ((ar_mec5_relation_arg *) p2)->index);
}

			/* --------------- */

static ccl_list *
s_get_ordered_arg_list (ar_mec5_relation *mrel, int with_aliases)
{
  int i;
  ccl_list *result = ccl_list_create ();

  s_compute_aliases (mrel);
  for (i = 0; i < mrel->nb_args; i++)
    {
      ar_mec5_relation_arg *arg = mrel->args + i;
      if (with_aliases || arg->alias ==  NULL)
	ccl_list_add (result, arg);
    }
  ccl_list_sort (result, s_cmp_args);

  return result;
}

			/* --------------- */

static void
s_log_constant (ccl_log_type log, ar_type *type, int index)
{
  ar_constant *cst = ar_type_get_ith_element (type, index);
  ar_constant_log (log, cst);
  ar_constant_del_reference (cst);
}

			/* --------------- */

static void
s_log_arg_assignment (struct display_data *data, ar_mec5_relation_arg *arg)
{
  int i = REL_INDEX_OF_ARG (data->mrel, arg);
  int min = data->min[i];
  int max = data->max[i];

  ar_identifier_log (data->log, arg->name);

  if (arg->alias != NULL)
    {
      ccl_log (data->log, " = ");
      ar_identifier_log (data->log, arg->alias->name);
    }
  else if (min == max)
    {
      ccl_log (data->log, " = ");
      s_log_constant (data->log, arg->type, min);
    }
  else
    {
      int i;
      ar_type_kind kind = ar_type_get_kind (arg->type);
	  
      ccl_log (data->log, " in ");
      switch (kind)
	{
	case AR_TYPE_RANGE:
	  ccl_log (data->log, "[");
	  s_log_constant (data->log, arg->type, min);
	  ccl_log (data->log, ", ");
	  s_log_constant (data->log, arg->type, max);
	  ccl_log (data->log, "]");
	  break;
	      
	case AR_TYPE_SYMBOL_SET: case AR_TYPE_ENUMERATION:
	  ccl_log (data->log, "{");
	  for (i = min; i <= max; i++)
	    {
	      s_log_constant (data->log, arg->type, i);
	      if (i < max)
		ccl_log (data->log, ", ");
	    }
	  ccl_log (data->log, "}");
	  break;
	      
	default:
	  ccl_assert (kind == AR_TYPE_BOOLEANS);
	  ccl_log (data->log, "bool");
	  break;
	}
    }
}

			/* --------------- */

static void
s_log_assignment (struct display_data *data)
{
  int i;

  s_log_arg_assignment (data, ITH_ARG (data->mrel, 0));
  for (i = 1; i < data->mrel->nb_args; i++)
    {  
      ccl_log (data->log, ", ");
      s_log_arg_assignment (data, ITH_ARG (data->mrel, i));
    }
}

			/* --------------- */

static void
s_log_relation_rec (struct display_data *data, ccl_pair *parg, ar_dd X)
{
  if (X == data->zero)
    return;

  if (parg == NULL)
    {
      ccl_assert (X == data->one);
      s_log_assignment (data);
      ccl_log (data->log, "\n");
    }
  else 
    {
      ar_mec5_relation_arg *arg = CAR (parg);
      int argindex = REL_INDEX_OF_ARG (data->mrel, arg);

      if (arg->alias != NULL)
	{
	  s_log_relation_rec (data, CDR (parg), X);
	}
      else if (X != data->one && AR_DD_INDEX (X) == arg->index)
	{
	  int i;
	  int arity = AR_DD_ARITY (X);

	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = (AR_DD_POLARITY (X)
			       ? AR_DD_NOT(AR_DD_ENTHSON (X, i))
			       : AR_DD_ENTHSON (X, i));
		  
		  data->min[argindex] = data->max[argindex] = i;
		  s_log_relation_rec (data, CDR (parg), son);
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = (AR_DD_POLARITY (X)
			       ? AR_DD_NOT(AR_DD_CNTHSON (X,i))
			       : AR_DD_CNTHSON (X, i));
		  
		  data->min[argindex] = AR_DD_MIN_OFFSET_NTHSON (X, i);
		  data->max[argindex] = AR_DD_MAX_OFFSET_NTHSON (X, i);
		  s_log_relation_rec (data, CDR (parg), son);
		}
	    }
	}
      else 
	{
	  data->min[argindex] = 0;
	  data->max[argindex] = ar_type_get_cardinality (arg->type) - 1;

	  s_log_relation_rec (data, CDR (parg), X);
	}
    }
}

			/* --------------- */

static void
s_log_relation_proto_with_dd (ccl_log_type log, ar_mec5_relation *mrel,
			      ar_dd X)
{
  struct display_data data;
  ccl_list *sorted_args = s_get_ordered_arg_list (mrel, 1);

  data.mrel = mrel;
  data.log = log;
  data.zero = ar_dd_zero (MEC5_DDM);
  data.one = ar_dd_one (MEC5_DDM);
  data.min = ccl_new_array (int, 2 * mrel->nb_args);
  data.max = data.min + mrel->nb_args;

  s_log_relation_rec (&data, FIRST (sorted_args), X);

  ccl_list_delete (sorted_args);
  ccl_delete (data.min);
}

			/* --------------- */

void
ar_mec5_relation_log (ccl_log_type log, ar_mec5_relation *mrel)
{
  ar_dd rel = ar_mec5_relation_get_dd (mrel);

  if (rel == ar_dd_zero (MEC5_DDM))
    ccl_log (log, "false");
  else if (rel == ar_dd_one (MEC5_DDM) && mrel->nb_args == 0)
    ccl_log (log, "true");
  else
    s_log_relation_proto_with_dd (log, mrel, rel);

  AR_DD_FREE (rel);
}


			/* --------------- */

double
ar_mec5_relation_get_cardinality (ar_mec5_relation *mrel)
{
  int i;
  double result;
  ar_dd rel = ar_mec5_relation_get_dd (mrel);
  ar_dd card_list = ar_dd_create_card_list (MEC5_DDM);
  s_compute_aliases (mrel);

  for(i = 0; i < mrel->nb_args; i++)
    {
      int card = ar_type_get_cardinality(mrel->args[i].type);
      ccl_assert (card > 0);
      ar_dd_card_list_add (MEC5_DDM, &card_list, mrel->args[i].index, 
			   COMPACTED, card);
    }
  
  result = ar_dd_cardinality (MEC5_DDM, rel, card_list);
  AR_DD_FREE (card_list);
  AR_DD_FREE (rel);

  return result;
}

			/* --------------- */

double
ar_mec5_relation_get_data_structure_size (ar_mec5_relation *mrel)
{
  ar_dd rel = ar_mec5_relation_get_dd (mrel);
  double result = (double) ar_dd_get_number_of_nodes (rel);
  AR_DD_FREE (rel);
  
  return result;
}

			/* --------------- */

static ar_mec5_relation *
s_create_mec5_relation_for_configuration (ar_relsem *rs, ar_node *node, 
					  ar_identifier *rid, int is_c)
{
  ar_mec5_relation *mrel = ar_mec5_relation_create ();
  ar_type *t = (is_c 
		? ar_node_get_configuration_type (node)
		: ar_node_get_configuration_base_type (node));
  ccl_list *l = ar_type_expand_with_prefix (AR_EMPTY_ID, t, NULL);

  ar_signature *sig = ar_signature_create (rid, AR_BOOLEANS);
  ar_signature_add_arg_domain (sig, t);
  ar_context_add_signature (MEC5_CONTEXT, rid, sig);
  ar_signature_del_reference (sig);
  ar_type_del_reference (t);

  while (!ccl_list_is_empty (l))
    {
      ar_identifier *name = ccl_list_take_first (l);
      ar_type *type = ccl_list_take_first (l);
      int index = ar_relsem_get_var_dd_index (rs, name);
      ar_mec5_relation_add_column (mrel, name, type, index);
      ar_identifier_del_reference (name);
      ar_type_del_reference (type);
    }
  ccl_list_delete (l);

  ar_mec5_add_relation (rid, mrel);


  return mrel;
}

/*!
 * Return a DD that encodes an elementary event. 
 * - ev is the identifier of the event; the prefix is the identifier of the node
 *   in the hierarchy.
 * - indexes contains DD indexes for slots.
 * - type contains event types for each node slot.
 */
static ar_dd
s_compute_dd_for_event_id (ar_ddm ddm, ar_identifier *ev, ar_idtable *indexes, 
			   ar_idtable *types)
{
  int i;
  ar_dd result;
  ar_identifier *tmp[2];
  ar_identifier *slname;
  ar_dd one = ar_dd_one (ddm);
  ar_dd zero = ar_dd_zero (ddm);
  ar_identifier *label;
  ar_type *et;
  int index;
  int valindex;
  int arity;
  ar_ddcode encoding;
  ar_ddtable sons;

  label = ar_identifier_get_name_and_prefix(ev, &tmp[0]);
  tmp[1] = ar_identifier_create ("");
  slname = ar_identifier_add_prefix (tmp[1], tmp[0]);
  ar_identifier_del_reference (tmp[0]);
  ar_identifier_del_reference (tmp[1]);

  et = ar_idtable_get (types, slname);
  index = (intptr_t) ar_idtable_get (indexes, slname);
  valindex = ar_type_enum_get_value_index (et, label);
  arity = ar_type_get_cardinality (et);
  encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
  sons =  ar_dd_allocate_dd_table (ddm, arity, encoding);

  ccl_assert (valindex >= 0);

  if (encoding == COMPACTED)
    {
      for (i = 0; i < arity; i++)
	{
	  ar_dd tmp = i == valindex ? one : zero;

	  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_CNTHDD (sons,i) = AR_DD_DUP (tmp);
	}
    }  
  else
    {
      for (i = 0; i < arity; i++)
	{
	  ar_dd tmp = i == valindex ? one : zero;
	  AR_DD_ENTHDD (sons,i) = AR_DD_DUP (tmp);
	}
    }

  result = ar_dd_find_or_add (ddm, index, arity, encoding, sons);

  ar_dd_free_dd_table (ddm, arity, encoding, sons);
  ar_identifier_del_reference (slname);
  ar_identifier_del_reference (label);
  ar_type_del_reference (et);

  return result;
}

/*
 * Conjoin 'rel' with the DD for a global event. It iterates 
 * s_compute_dd_for_event_id on each component and intersects the DD with 'rel'.
 */
static ar_dd
s_compute_dd_for_event (ar_ddm ddm, ar_ca_event *ev, ar_idtable *indexes, 
			ar_idtable *types, ar_dd rel, int with_events)
{ 
  ar_dd result = AR_DD_DUP (rel);

  if (with_events)
    {
      ar_identifier_iterator *ii = ar_ca_event_get_ids (ev);

      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *id = ccl_iterator_next_element (ii);
	  ar_dd tmp1 = s_compute_dd_for_event_id (ddm, id, indexes, types);
	  ar_dd tmp2 = ar_dd_compute_and (ddm, result, tmp1);
	  AR_DD_FREE (tmp1);
	  AR_DD_FREE (result);
	  result = tmp2;
	  ar_identifier_del_reference (id);
	}
      ccl_iterator_delete (ii);
    }

  return result;
}

			/* --------------- */

static ar_dd
s_compute_relation_transition (ar_ddm ddm, ar_relsem *rs, ar_idtable *indexes,
			       ar_idtable *types, int prepost,
			       const ar_dd_array *rels, int with_events)
{
  int i;
  ar_dd result = NULL;
  ar_ca *ca = ar_relsem_get_automaton (rs);
  ar_ca_event **events = ar_ca_get_events(ca);

  result = s_compute_dd_for_event (ddm, events[0], indexes, types, 
				   rels->data[0], with_events);
  
  for (i = 1; i < rels->size; i++)
    {
      ar_dd tmp1 = 
	s_compute_dd_for_event (ddm, events[i], indexes, types, 
				rels->data[i], with_events);
      ar_dd tmp2 = ar_dd_compute_or (ddm, tmp1, result);
      AR_DD_FREE (tmp1);
      AR_DD_FREE (result);
      result = tmp2;
    }
  ar_ca_del_reference (ca);

  return result;
}

			/* --------------- */

static ar_mec5_relation *
s_build_relation_for_transitions (ar_relsem *rs, ar_node *node, 
				  ar_identifier *rid, ar_dd *p_set, int prepost,
				  const ar_dd_array *rels, int with_events)
{
  int i;
  ar_signature *sig;
  ccl_pair *p;
  int max_cdd_index = -1;
  ar_mec5_relation *mrel = ar_mec5_relation_create ();
  ar_type *ct = (prepost 
		 ? ar_node_get_configuration_type (node)
		 : ar_node_get_configuration_base_type (node));
  ccl_list *cl = ar_type_expand_with_prefix (AR_EMPTY_ID, ct, NULL);
  ar_type *et = NULL;
  ccl_list *el = NULL;
  ar_idtable *indexes_of_events = NULL;
  ar_idtable *types_of_events = NULL;
  int  nb_events = 0;
  int events_start = 0;

  if (with_events)
    {
      et = ar_node_get_event_type (node);
      el = ar_type_expand_with_prefix (AR_EMPTY_ID, et, NULL);
      indexes_of_events = ar_idtable_create (0, NULL, NULL);
      types_of_events = 
	ar_idtable_create (0, (ccl_duplicate_func *) ar_type_add_reference, 
			   (ccl_delete_proc *) ar_type_del_reference);
      nb_events = ccl_list_get_size (el) >> 1;
      events_start = ccl_list_get_size (cl) >> 1;
    }

  for (p = FIRST (cl); p; p = CDDR (p))
    {      
      ar_identifier *name = CAR (p);
      ar_type *type = CADR (p);
      int index = ar_relsem_get_var_dd_index (rs, name);
      if (index > max_cdd_index)
	max_cdd_index = index;
      ar_mec5_relation_add_column (mrel, name, type, index);
    }

  if (with_events)
    {
      for (p = FIRST (el); p; p = CDDR (p))
	{      
	  ar_identifier *name = CAR (p);
	  ar_type *type = CADR (p);
	  ar_mec5_relation_add_column (mrel, name, type, -1);
	  ar_idtable_put (types_of_events, name, type);
	  ar_identifier_del_reference (name);
	  ar_type_del_reference (type);
	}
    }


  for (p = FIRST (cl); p; p = CDDR (p))
    {      
      ar_identifier *name = CAR (p);
      ar_identifier *primed_name =
	ar_identifier_rename_with_suffix (CAR (p), "'");
      ar_type *type = CADR (p);
      int index = ar_relsem_get_primed_var_dd_index (rs, name);
      if (index > max_cdd_index)
	max_cdd_index = index;
      ar_type_del_reference (type);
      ar_mec5_relation_add_column (mrel, primed_name, type, index);
      ar_identifier_del_reference (name);
      ar_identifier_del_reference (primed_name);
    }

  ccl_list_delete (cl);

  if (with_events)
    {
      ccl_list_delete (el);
      
      for (i = 0; i < nb_events; i++)
	{
	  ar_identifier *name = 
	    ar_mec5_relation_get_ith_arg_name (mrel, i + events_start);
	  int dd_index = ar_relsem_get_event_var_dd_index (rs, name);
	  if (dd_index >= 0)
	    {
	      if (dd_index > max_cdd_index)
		max_cdd_index = dd_index;
	      ar_idtable_put (indexes_of_events, name, 
			      (void *) (intptr_t) dd_index);
	      ar_mec5_relation_set_ith_dd_index (mrel, i + events_start, 
						 dd_index);
	    }
	  ar_identifier_del_reference (name);
	}

      max_cdd_index++;
      for (i = 0; i < nb_events; i++)
	{
	  ar_identifier *name = 
	    ar_mec5_relation_get_ith_arg_name (mrel, i + events_start);
	  int dd_index = ar_relsem_get_event_var_dd_index (rs, name);
	  if (dd_index < 0)
	    {
	      dd_index = max_cdd_index++;
	      ar_idtable_put (indexes_of_events, name, 
			      (void *) (intptr_t) dd_index);
	      ar_mec5_relation_set_ith_dd_index (mrel, i + events_start, 
						 dd_index);
	    }
	  ar_identifier_del_reference (name);
	}
    }

  ar_mec5_add_relation (rid, mrel);

  sig = ar_signature_create (rid, AR_BOOLEANS);
  ar_signature_add_arg_domain (sig, ct);
  if (with_events)
    ar_signature_add_arg_domain (sig, et);
  ar_signature_add_arg_domain (sig, ct);
  ar_context_add_signature (MEC5_CONTEXT, rid, sig);
  ar_signature_del_reference (sig);

  ar_type_del_reference (ct);
  if (with_events)
    ar_type_del_reference (et);
  *p_set = s_compute_relation_transition (MEC5_DDM, rs, indexes_of_events,
					  types_of_events, prepost, rels,
					  with_events);
  if (with_events)
    {
      ar_idtable_del_reference (indexes_of_events);
      ar_idtable_del_reference (types_of_events);
    }

  return mrel;
}

			/* --------------- */

typedef ar_dd_array 
(*get_transition_relations_func) (ar_relsem *, int, int);

static ar_mec5_relation *
s_build_relation_from_accessor (ar_relsem *rs, ar_node *node, 
			       ar_identifier *rid, ar_dd *p_set, 
			       int prepost,
			       get_transition_relations_func get_relations,
			       int with_events)
{
  ar_dd_array event_rels = get_relations (rs, prepost, prepost);
  ar_mec5_relation *R = 
    s_build_relation_for_transitions (rs, node, rid, p_set, prepost,
				      &event_rels, with_events);  
  AR_DD_DELETE_ARRAY (event_rels);

  return R;
}

			/* --------------- */

static ar_mec5_relation *
s_build_mec5_relation_for_epsilon (ar_relsem *rs, ar_node *node, 
				   ar_identifier *rid, ar_dd *p_set)
{
  int i;
  ar_signature *sig;
  ccl_pair *p;
  ar_dd one = ar_dd_one (MEC5_DDM);
  ar_ca *ca = ar_relsem_get_automaton (rs);
  ar_ca_event **events = ar_ca_get_events(ca);
  ar_mec5_relation *mrel = ar_mec5_relation_create ();
  ar_type *et = ar_node_get_event_type (node);
  ccl_list *el = ar_type_expand_with_prefix (AR_EMPTY_ID, et, NULL);
  ar_idtable *indexes = ar_idtable_create (0, NULL, NULL);
  ar_idtable *types = 
    ar_idtable_create (0, (ccl_duplicate_func *) ar_type_add_reference, 
		       (ccl_delete_proc *) ar_type_del_reference);
  int nb_events = ar_ca_get_number_of_events(ca);
  
  for (p = FIRST (el); p; p = CDDR (p))
    {      
      ar_identifier *name = CAR (p);
      ar_type *type = CADR (p);
      int dd_index = ar_relsem_get_event_var_dd_index (rs, name);
      ccl_assert (dd_index >= 0);
      ar_mec5_relation_add_column (mrel, name, type, dd_index);
      ar_idtable_put (types, name, type);
      ar_idtable_put (indexes, name, (void *) (intptr_t) dd_index);
      ar_identifier_del_reference (name);
      ar_type_del_reference (type);	
    }
  ccl_list_delete (el);

  ar_mec5_add_relation (rid, mrel);

  sig = ar_signature_create (rid, AR_BOOLEANS);
  ar_signature_add_arg_domain (sig, et);
  ar_context_add_signature (MEC5_CONTEXT, rid, sig);
  ar_signature_del_reference (sig);
  ar_type_del_reference (et);

  *p_set = NULL;
  for (i = 0; *p_set == NULL && i < nb_events; i++)
    {
      if (ar_ca_event_is_epsilon (events[i]))
	*p_set = s_compute_dd_for_event (MEC5_DDM, events[i], indexes, types, 
					 one, 1);
    }

  ccl_assert (*p_set != NULL);
  ar_ca_del_reference (ca);
  ar_idtable_del_reference (indexes);
  ar_idtable_del_reference (types);

  return mrel;
}

			/* --------------- */

void
ar_mec5_add_configuration_relation (ar_relsem *rs, ar_identifier *relid,
				    ar_bang_t bang, int nb_rels, ar_dd *rels)
{
  ar_node *flatnode = ar_sgs_get_node ((ar_sgs *) rs);
  ar_identifier *nodename = ar_node_get_name (flatnode);
  ar_node *actual_node = ar_model_get_node (nodename);
  int isc = (bang == AR_BANG_CONFIGURATIONS);
  ar_mec5_relation *mrel = 
    s_create_mec5_relation_for_configuration (rs, actual_node, relid, isc);
  ar_mec5_relation_set_dds (mrel, nb_rels, rels);
  ccl_post (ar_mec5_is_a_configuration_relation (mrel, actual_node));
  ar_mec5_relation_del_reference (mrel);

  ar_node_del_reference (flatnode);
  ar_node_del_reference (actual_node);
  ar_identifier_del_reference (nodename);
}

			/* --------------- */

void
ar_mec5_add_transition_relation (ar_relsem *rs, ar_identifier *relid,
				 const ar_dd_array *rels)
{
  ar_dd set = NULL;
  ar_node *flatnode = ar_sgs_get_node ((ar_sgs *) rs);
  ar_identifier *nodename = ar_node_get_name (flatnode);
  ar_node *actual_node = ar_model_get_node (nodename);
  ar_mec5_relation *mrel = 
    s_build_relation_for_transitions (rs, actual_node, relid, &set, 0, rels, 1);

  ar_mec5_relation_set_dds (mrel, 1, &set);
  ccl_post (ar_mec5_is_a_transition_relation (mrel, actual_node));
  ar_mec5_relation_del_reference (mrel);
  AR_DD_FREE (set);
  ar_node_del_reference (flatnode);
  ar_node_del_reference (actual_node);
  ar_identifier_del_reference (nodename);
}

			/* --------------- */

void
ar_mec5_build_bang_relation (ar_node *node, ar_bang_t bang)
  CCL_THROW ((altarica_interpretation_exception, 
	      domain_cardinality_exception))
{
  ar_dd set = NULL;
  ar_relsem *rs = NULL;
  ar_identifier *big = ar_bang_id_for_node (node, bang);
  ar_mec5_relation *mrel = ar_mec5_get_relation (big);

  if (mrel != NULL || bang == AR_BANG_EVENTS || 
      bang == AR_BANG_SUPER_CONFIGURATIONS)
    goto end;

  ccl_try (exception)
    {
      rs = ar_semantics_get (node, AR_SEMANTICS_RELATIONS);
    }
  ccl_catch
    {
      ar_identifier_del_reference (big);
      ccl_zdelete (ar_mec5_relation_del_reference, mrel);
      ccl_rethrow ();
    }
  ccl_end_try;

  ccl_assert (rs != NULL);

  switch (bang)
    {
    case AR_BANG_CONFIGURATIONS:
      {
	int nb_rels = 0;
	ar_dd *rels = ar_relsem_get_assertions (rs, &nb_rels);
	mrel = s_create_mec5_relation_for_configuration (rs, node, big, 1);
	ar_mec5_relation_set_dds (mrel, nb_rels, rels);
      }
      break;

    case AR_BANG_VALID_STATES:
      mrel = s_create_mec5_relation_for_configuration (rs, node, big, 0);
      set = ar_relsem_get_valid_states (rs);      
      break;

    case AR_BANG_INIT:
      mrel = s_create_mec5_relation_for_configuration (rs, node, big, 1);
      set = ar_relsem_get_initial (rs);
      break;

    case AR_BANG_REACHABLES:
    case AR_BANG_ANY_S:
      mrel = s_create_mec5_relation_for_configuration (rs, node, big, 1);
      set = ar_relsem_get_reachables (rs);
      break;

    case AR_BANG_TRANS:
      mrel = s_build_relation_from_accessor (rs, node, big, &set, 1, 
					     ar_relsem_get_event_relations, 
					     1);
      break;
    case AR_BANG_SUPER_TRANS:
      mrel = s_build_relation_from_accessor (rs, node, big, &set, 0, 
					     ar_relsem_get_event_relations,
					     1);
      break;
    case AR_BANG_MOVES:
      mrel = s_build_relation_from_accessor (rs, node, big, &set, 0, 
					     ar_relsem_get_valid_moves, 0);
      break;
    case AR_BANG_NSEMOVES:
      mrel = s_build_relation_from_accessor (rs, node, big, &set, 0, 
					     ar_relsem_get_no_self_epsilon_moves, 
					     0);
      break;

    case AR_BANG_VALID_STATE_CHANGES:
      mrel = s_build_relation_from_accessor (rs, node, big, &set, 0, 
					     ar_relsem_get_valid_moves, 1);
      break;


    case AR_BANG_EPSILON:
      mrel = s_build_mec5_relation_for_epsilon (rs, node, big, &set);
      break;

    default:
      break;
    }

  if (set != NULL)
    {
      ccl_assert (mrel != NULL);
      ar_mec5_relation_set_dds (mrel, 1, &set);
      AR_DD_FREE (set);
    }

  ar_relsem_del_reference (rs);
 end:
  ar_identifier_del_reference (big);
  ccl_zdelete (ar_mec5_relation_del_reference, mrel);
}

			/* --------------- */

void
ar_mec5_relation_write (ar_mec5_relation *mrel, FILE *out, 
			ccl_serializer_status *p_err)
{
  int i;

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  s_compute_aliases (mrel);
  
  ccl_serializer_write_uint16 (mrel->nb_args, out, p_err);
  for (i = 0; i < mrel->nb_args && *p_err == CCL_SERIALIZER_OK; i++)
    {
      ar_mec5_relation_arg *arg = mrel->args + i;

      ar_identifier_write (arg->name, out, p_err);
      ccl_serializer_write_int32 (arg->index, out, p_err);
      ar_type_write (arg->type, out, p_err);
    }
  ccl_serializer_write_int32 (mrel->nb_rels, out, p_err);
  for (i = 0; i < mrel->nb_rels && *p_err == CCL_SERIALIZER_OK; i++)
    ar_dd_write_node (MEC5_DDM, mrel->rels[i], out, p_err);
}

			/* --------------- */

void
ar_mec5_relation_read (ar_mec5_relation **p_rel, FILE *in, 
		       ccl_serializer_status *p_err)
{
  ar_mec5_relation *result;
  int i;

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  result = ar_mec5_relation_create ();
  ccl_serializer_read_int32 (&result->nb_args, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    goto end;
  ccl_delete (result->args);
  result->args = ccl_new_array (ar_mec5_relation_arg, result->nb_args);

  for (i = 0; i < result->nb_args && *p_err == CCL_SERIALIZER_OK; i++)
    {
      ar_mec5_relation_arg *arg = result->args + i;

      ar_identifier_read (&arg->name, in, p_err);
      ccl_serializer_read_int32 (&arg->index, in, p_err);
      ar_type_read (&arg->type, in, p_err);
    }
  ccl_serializer_read_int32 (&result->nb_rels, in, p_err);
  result->rels = ccl_new_array (ar_dd, result->nb_rels);
  for (i = 0; i < result->nb_rels && *p_err == CCL_SERIALIZER_OK; i++)
    ar_dd_read_node (MEC5_DDM, &result->rels[i], in, p_err);

 end:
  if (*p_err != CCL_SERIALIZER_OK)
    ar_mec5_relation_del_reference (result);
  else
    {
      s_compute_aliases (result);
      *p_rel = result;
    }
}

static int
s_compare_rel_type_to_list (ar_mec5_relation *mrel, ccl_list *l)
{
  int i = 0;
  int result = (ccl_list_get_size (l)>>1) == mrel->nb_args;
  
  while (!ccl_list_is_empty (l))
    {
      ar_identifier *name = ccl_list_take_first (l);
      ar_type *type = ccl_list_take_first (l);

      result = result && ar_type_have_same_base_type (type, mrel->args[i].type);
      ar_type_del_reference (type);
      ar_identifier_del_reference (name);
      i++;
    }
  ccl_list_delete (l);
  
  return result;
}

int
ar_mec5_is_a_configuration_relation (ar_mec5_relation *mrel, ar_node *node)
{
  ar_type *t = ar_node_get_configuration_base_type (node);
  ccl_list *l = ar_type_expand_with_prefix (AR_EMPTY_ID, t, NULL);
  int result = s_compare_rel_type_to_list (mrel, l);

  ar_type_del_reference (t);  
  
  return result;
}

int
ar_mec5_is_a_transition_relation (ar_mec5_relation *mrel, ar_node *node)
{
  ar_type *t = ar_node_get_configuration_base_type (node);
  ar_type *et = ar_node_get_event_type (node);
  ccl_list *l = ar_type_expand_with_prefix (AR_EMPTY_ID, t, NULL);

  l = ar_type_expand_with_prefix (AR_EMPTY_ID, et, l);
  l = ar_type_expand_with_prefix (AR_EMPTY_ID, t, l);
  ar_type_del_reference (t);
  ar_type_del_reference (et);  

  return s_compare_rel_type_to_list (mrel, l);
}
