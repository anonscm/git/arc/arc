/*
 * mec5-equations.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef MEC5_EQUATIONS_H
# define MEC5_EQUATIONS_H

# include <ccl/ccl-log.h>
# include "ar-expr.h"

typedef struct ar_equations_system_st ar_equations_system;

extern ar_equations_system *
ar_equations_system_create (ar_context *parent);

extern ar_equations_system *
ar_equations_system_add_reference (ar_equations_system *eqsys);

extern void
ar_equations_system_del_reference (ar_equations_system *eqsys);

extern ar_context *
ar_equations_system_get_context (ar_equations_system *eqsys);

extern int
ar_equations_system_has_equation (ar_equations_system *eqsys, 
				  ar_identifier *eqname);

extern void
ar_equations_system_add_equation (ar_equations_system *eqsys, ar_context *ctx, 
				  int parity, ar_identifier *name, 
				  int nb_params, ar_expr **params, 
				  ar_expr *definition,
				  int is_local);

extern void
ar_equations_system_log (ccl_log_type log, ar_equations_system *eqsys);

extern void
ar_equations_system_log_info (ccl_log_type log, ar_equations_system *eqsys);

extern void
ar_equations_system_solve (ar_equations_system *eqsys);

#endif /* ! MEC5_EQUATIONS_H */
