/*
 * ar-interp-mec5.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdio.h>
#include <time.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-time.h>
#include "arc.h"
#include "ar-semantics.h"
#include "ar-state-graph-semantics.h"

#include "ar-ca-semantics.h"
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-expr.h"
#include "ar-interp-altarica-p.h"
#include "ar-interp-acheck.h"
#include "mec5-equations.h"
#include "mec5.h"
#include "mec5-preferences.h"
#include "mec5-interp-ast.h"

			/* --------------- */

struct equation_prototype
{
  struct equation_prototype *next;
  altarica_tree *loc;
  altarica_tree *def;
  int is_local;
  int parity;
  ar_identifier *id;
  ar_context *ctx;
  int nb_params;
  ar_expr *params[1];
};
			/* --------------- */

CCL_DEFINE_EXCEPTION (mec5_interpretation_exception, 
		      altarica_interpretation_exception);

#define mec5_interp_exception() ccl_throw_no_msg(mec5_interpretation_exception)

#define MEC5_TIMERS_ARE_ENABLED(_conf) \
  ccl_config_table_get_boolean (_conf, MEC5_PREF_TIMERS_STR)

			/* --------------- */

static ar_signature *
s_add_equation_signature (ar_context *ctx, ar_identifier *id, int nb_params,
			  ar_expr **params)
{
  int i;
  ar_signature *sig = ar_signature_create (id, AR_BOOLEANS);

  for (i = 0; i < nb_params; i++)
    {
      ar_type *t = ar_expr_get_type (params[i]);      
      ar_signature_add_arg_domain (sig, t);
      ar_type_del_reference (t);
    }

  ar_context_add_signature (ctx, id, sig);

  return sig;
}

			/* --------------- */

static struct equation_prototype *
s_look_for_eqname (struct equation_prototype *protos, ar_identifier *name)
{
  for (; protos && protos->id != name; protos = protos->next)
    CCL_NOP ();

  return protos;
}

			/* --------------- */

static void
s_interp_equation_proto (altarica_tree *t, ar_context *ctx, 
			 struct equation_prototype *first,
			 struct equation_prototype **p_next)
{
  int nb_params;
  ccl_list *slots;
  ccl_list *ids;
  altarica_tree *loc = t;
  struct equation_prototype *proto;
  ar_identifier *eqname;

  ccl_pre (IS_LABELLED_BY (t, IDENTIFIER) || IS_LABELLED_BY (t, BANG_ID));
  ccl_pre (IS_LABELLED_BY (t->next, EQ_PARAMETERS));

  eqname = ar_interp_id_or_bang_id (t, ctx);
    
  proto = s_look_for_eqname (first, eqname);
  if (proto != NULL)
    {
      ar_identifier_del_reference (eqname);
      ccl_error ("%s:%d: error: redefinition of fixpoint '%s'.\n",
		 t->filename, t->line, t->value.id_value);
      ccl_error ("%s:%d: error: previously declared here.\n",
		 proto->loc->filename, proto->loc->line);
      mec5_interp_exception ();
    }
  t = t->next;
  slots = ccl_list_create ();
  ids = ccl_list_create ();

  for (t = t->child; t; t = t->next)
    {      
      int err = 0;
      ar_identifier *varname = NULL;
      ar_type *type = NULL;

      ccl_try (altarica_interpretation_exception)
        { 
	  if (!IS_LABELLED_BY (t, TYPED_ID))
	    {
	      ccl_error ("%s:%d: error: arc does not support yet type "
			 "inference for MEC V specifications.\n", t->filename, 
			 t->line);
	      INTERP_EXCEPTION ();
	    }
      
	  varname = ar_interp_symbol_as_identifier (t->child);
	  if (ccl_list_has (ids, varname))
	    {
	      ccl_error ("%s:%d: error: parameter '", t->filename, t->line);
	      ar_identifier_log (CCL_LOG_ERROR, varname);
	      ccl_error ("' is defined twice.\n", t->filename, t->line);
	      INTERP_EXCEPTION ();

	    }

	  type = ar_interp_domain (t->child->next, ctx, 
				   ar_interp_member_access);

	  varname = ar_identifier_add_reference (varname);
	  ccl_list_add (ids, varname);
	  ar_context_slot_expand (-1, varname, type, 0, NULL, NULL, slots);
	}
      ccl_catch
	{	  
	  ar_identifier_del_reference (eqname);
	  ccl_list_clear_and_delete (ids, (ccl_delete_proc *)
				     ar_identifier_del_reference);
	  ccl_list_clear_and_delete (slots, (ccl_delete_proc *)
				     ar_context_slot_del_reference);
	  err = 1;
	}
      ccl_end_try;

      ccl_zdelete (ar_type_del_reference, type);
      ccl_zdelete (ar_identifier_del_reference, varname);

      if (err)
	ccl_rethrow ();
    }

  nb_params = ccl_list_get_size (ids);
  proto = ccl_malloc (sizeof (struct equation_prototype) +
		       (((nb_params <= 1) ? 0 : (nb_params - 1)) * 
			sizeof (ar_expr *)));
  proto->next = NULL;
  proto->loc = loc;
  proto->id = eqname;
  proto->ctx = ar_context_create_from_slots (ctx, slots);
  ccl_list_clear_and_delete (slots, (ccl_delete_proc *)
			     ar_context_slot_del_reference);
  proto->nb_params = 0;
  while (! ccl_list_is_empty (ids))
    {
      ar_identifier *varname = ccl_list_take_first(ids);
      ar_context_slot *sl = ar_context_get_slot (proto->ctx, varname);

      ccl_assert (sl != NULL);

      proto->params[proto->nb_params++] = ar_expr_crt_variable (sl);

      ar_identifier_del_reference (varname);
      ar_context_slot_del_reference (sl);
    }
  ccl_list_delete (ids);

  *p_next = proto;
}

			/* --------------- */
static void
s_delete_equation_prototype (struct equation_prototype *ep)
{
  int i;

  ar_identifier_del_reference (ep->id);
  ar_context_del_reference (ep->ctx);
  for (i = 0; i < ep->nb_params; i++)
    ar_expr_del_reference (ep->params[i]);    
  ccl_delete (ep);
}

			/* --------------- */

static void
s_delete_equation_prototypes (struct equation_prototype *ep)
{
  struct equation_prototype *next = ep->next;
  s_delete_equation_prototype (ep);
  if (next != NULL)
    s_delete_equation_prototypes (next);
}

			/* --------------- */

static struct equation_prototype *
s_build_protos (altarica_tree *te, ar_equations_system *eqsys)
{
  ar_context *ctx = ar_equations_system_get_context (eqsys);
  struct equation_prototype *protos = NULL;


  ccl_try (altarica_interpretation_exception)
    {
      struct equation_prototype **p_protos = &protos;

      for (; te; te = te->next, p_protos = &((*p_protos)->next))
	{
	  int is_local = IS_LABELLED_BY (te, LOCAL_EQUATION);
	  altarica_tree *t = is_local ? te->child : te;
	  int parity = t->value.int_value;

	  ccl_assert (ccl_imply (IS_LABELLED_BY (t, EQ_DEF), (parity < 0)));

	  if (!IS_LABELLED_BY (t, EQ_DEF))
	    parity *= 2;

	  if (IS_LABELLED_BY (t, EQ_LFP)) 
	    parity++;

	  s_interp_equation_proto (t->child, ctx, protos, p_protos);
	  (*p_protos)->is_local = is_local;
	  (*p_protos)->parity = parity;
	  (*p_protos)->def = t->child->next->next;
	}
    }
  ccl_catch
    {
      ccl_zdelete (s_delete_equation_prototypes, protos);
      protos = NULL;
    }
  ccl_end_try;

  ar_context_del_reference (ctx);
  if (protos == NULL)
    ccl_rethrow ();

  return protos;
}

			/* --------------- */

static void
s_register_prototype (struct equation_prototype *proto, 
		      ar_equations_system *eqsys, ccl_list *signatures)
{
  ar_signature *sig;
  ar_context *ctx = ar_equations_system_get_context (eqsys);
  ar_context *pctx = ar_context_get_parent (ctx);
  ar_context *rctx = (proto->is_local) ? ctx : pctx;
  sig = s_add_equation_signature (rctx, proto->id, proto->nb_params, 
				  proto->params);
  ar_context_del_reference (ctx);
  ar_context_del_reference (pctx);

  ccl_list_add (signatures, sig);
}

			/* --------------- */

static void
s_register_prototypes (struct equation_prototype *protos, 
		       ar_equations_system *eqsys, ccl_list *signatures)
{
  for (; protos; protos = protos->next)
    {
      ccl_assert (protos->parity >= 0);

      s_register_prototype (protos, eqsys, signatures);
    }
}

			/* --------------- */

static void
s_interp_equation (struct equation_prototype *proto, ar_equations_system *eqsys)
{
  ar_expr *def = ar_interp_boolean_expression (proto->def, proto->ctx, 
					       ar_interp_member_access);
  ar_expr *rdef = ar_expr_remove_composite_slots (def, proto->ctx);
  ar_equations_system_add_equation (eqsys, proto->ctx, proto->parity, 
				    proto->id, proto->nb_params, proto->params,
				    rdef, proto->is_local);
  
  ar_expr_del_reference (rdef);
  ar_expr_del_reference (def);
}

			/* --------------- */

static void
s_interp_constants (struct equation_prototype **pproto, 
		    ar_equations_system *eqsys, ccl_list *signatures)
{
  struct equation_prototype **pp = pproto;

  while (*pp)
    {
      struct equation_prototype *p = *pp;

      if (p->parity >= 0)
	pp = &((*pp)->next);
      else
	{
	  *pp = p->next;
	  ccl_try (exception)
	    {
	      s_interp_equation (p, eqsys);
	    }
	  ccl_catch
	    {
	      s_delete_equation_prototype (p);
	      ccl_rethrow ();
	    }
	  ccl_end_try;

	  s_register_prototype (p, eqsys, signatures);
	  s_delete_equation_prototype (p);
	}
    }
}

			/* --------------- */

void
s_interp_equations_system (altarica_tree *t, ar_context *ctx,
			   ccl_config_table *conf, int loginfo)
{
  int error = 0;
  uint32_t t0 = ccl_time_get_cpu_time_in_milliseconds ();
  struct equation_prototype *protos = NULL;
  ar_equations_system *eqsys = ar_equations_system_create (ctx);
  ccl_list *signatures = ccl_list_create ();

  ccl_pre (IS_LABELLED_BY (t, EQUATIONS_SYSTEM));
  
  ccl_try (altarica_interpretation_exception)
    {
      struct equation_prototype *p;

      protos = s_build_protos (t->child, eqsys);
      s_interp_constants (&protos, eqsys, signatures);
      s_register_prototypes (protos, eqsys, signatures);

      for (p = protos; p; p = p->next)
	s_interp_equation (p, eqsys);

      ar_equations_system_solve (eqsys);
      if (loginfo)
	{
	  ar_equations_system_log_info (CCL_LOG_WARNING, eqsys);
	  if (MEC5_TIMERS_ARE_ENABLED (conf))
	    {
	      ccl_display ("computation done within ");
	      t0 = ccl_time_get_cpu_time_in_milliseconds () - t0;
	      if (t0 == 0)
		ccl_display ("in a time < 1 ms");
	      else
		ccl_time_log (CCL_LOG_DISPLAY, t0);	  
	      ccl_display (".\n");
	    }
	}
    }
  ccl_catch
    {
      while (! ccl_list_is_empty (signatures))
	{
	  ar_signature *sig = ccl_list_take_first (signatures);
	  ar_identifier *id = ar_signature_get_name (sig);
	  ar_context_remove_signature (ctx, id);
	  ar_identifier_del_reference (id);
	  ar_signature_del_reference (sig);
	}
      error = 1;
    }
  ccl_end_try;
  
  ccl_list_clear_and_delete (signatures, (ccl_delete_proc *)
			     ar_signature_del_reference);
  ccl_zdelete (s_delete_equation_prototypes, protos);
  ar_equations_system_del_reference (eqsys);

  if (error)
    ccl_rethrow ();
}

			/* --------------- */

int
ar_interp_mec5_tree (altarica_tree *t, ccl_config_table *conf)
{
  int result = 1;
  altarica_tree *next;

  ccl_try(altarica_interpretation_exception)
    {
      for(; t != NULL; t = next)
	{
	  next = t->next;	  
	  switch (t->node_type) 
	    {
	    case AR_TREE_CONSTANT :  
	      ar_interp_altarica_constant_decl (t, MEC5_CONTEXT);
	      break;

	    case AR_TREE_DOMAIN : 
	      ar_interp_altarica_domain_decl (t, MEC5_CONTEXT);
	      break;

	    case AR_TREE_EQUATIONS_SYSTEM :  
	      s_interp_equations_system (t, MEC5_CONTEXT, conf, 1);
	      break;

	    case AR_TREE_MECV_COMMAND :  
	      ccl_warning ("%s:%d: warning: ignoring mec5 command '%s'.\n",
			   t->filename, t->line,
			   t->value.string_value);
	      break;

	    default:
	      INVALID_NODE_TYPE ();
	    }
	}
    }
  ccl_catch
    {      
      result = 0;
    }
  ccl_end_try;

  return result;
}

			/* --------------- */

int
ar_interp_mec5_equation_system (altarica_tree *t, ccl_config_table *conf, 
				int loginfo)
{
  int result = 1;

  ccl_try (altarica_interpretation_exception)
    {
      s_interp_equations_system (t, MEC5_CONTEXT, conf, loginfo);
    }
  ccl_catch
    {      
      result = 0;
    }
  ccl_end_try;

  return result;
}

			/* --------------- */
#if 0
typedef struct type_class_st *type_class;
struct type_class_st
{
  type_class *parent;
  int rank;
  altarica_tree *node;
  ar_type *base_type;
  ar_type *domain;
};

			/* --------------- */

static type_class *
s_get_representative (type_class *tc)
{
  if (tc->parent != tc)
    tc->parent = s_get_representative (tc->parent);

  return tc->parent;
}

			/* --------------- */

static type_class *
s_merge_classes (type_class *tc1, type_class *tc2)
{
  ccl_pre (tc1->parent == tc1);
  ccl_pre (tc2->parent == tc2);
  ccl_pre (tc1 != tc2);

  if ((tc1->type == NULL && tc2->type == NULL) ||
      
  return tc->parent;
}

			/* --------------- */

static void
s_infer_types_of_equation_system (altarica_tree *t)
{
  ccl_pre (IS_LABELLED_BY (t, EQUATIONS_SYSTEM);
}


#endif
