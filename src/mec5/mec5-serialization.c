#include <ccl/ccl-assert.h>
#include "mec5.h"
#include "mec5-serialization.h"


static int
s_store_relation (ar_identifier *id, ar_identifier *newname, FILE *out, 
		  ccl_config_table *config);

			/* --------------- */

static void
s_store_signature_dependencies (ar_signature *sig, FILE *out, 
				ccl_config_table *config)
{
  int i;
  int arity = ar_signature_get_arity(sig);
  int ok = 1;
  ar_idtable *cache = ar_idtable_create (0, NULL, NULL);
  ar_identifier *id = ar_signature_get_name (sig);
  ar_idtable_put (cache, id, id);
  ar_identifier_del_reference (id);

  for (i = 0; i < arity && ok; i++)
    {
      ar_type *type = ar_signature_get_ith_arg_domain (sig, i);
      if (ar_type_get_kind (type) == AR_TYPE_BANG)
	{
	  char *stype = ar_type_to_string (type);
	  id = ar_identifier_create (stype);
	  ccl_string_delete (stype);

	  ar_mec5_find_or_add_bang_relation (id);

	  if (ar_mec5_has_relation_no_bang_creation (id) && 
	      !ar_idtable_has (cache, id))
	    {
	      ok = s_store_relation (id, NULL, out, config);
	      ar_idtable_put (cache, id, id);
	    }
	  ar_identifier_del_reference (id);
	}
      ar_type_del_reference (type);
    }
  ar_idtable_del_reference (cache);
}

			/* --------------- */

static int
s_store_relation (ar_identifier *id, ar_identifier *newname, FILE *out, 
		  ccl_config_table *config)
{
  ar_signature *sig = NULL;
  ar_mec5_relation *rel = NULL;
  ccl_serializer_status err = CCL_SERIALIZER_OK;

  ccl_pre (ar_mec5_has_relation_no_bang_creation (id));
  ccl_pre (out != NULL);

  sig = ar_context_get_signature (MEC5_CONTEXT, id);
  ccl_pre (sig != NULL);
  if (newname)
    {
      ar_signature *tmp = ar_signature_rename (sig, newname);
      ar_signature_del_reference (sig);
      sig = tmp;
    }

  rel = ar_mec5_get_relation (id);
  s_store_signature_dependencies (sig, out, config);
  ar_signature_write (sig, out, &err);
  ar_mec5_relation_write (rel, out, &err);
  ar_signature_del_reference (sig);
  ar_mec5_relation_del_reference (rel);

  return (err ==  CCL_SERIALIZER_OK);
}

			/* --------------- */

int
ar_mec5_store_relation (ar_identifier *id, ar_identifier *newname,
			const char *filename, 
			ccl_config_table *config)
{
  FILE *out;
  int result = 0;

  ccl_pre (ar_mec5_has_relation_no_bang_creation (id));
  ccl_pre (filename != NULL);

  out = fopen (filename, "a");
  if (out != NULL)
    {
      result = s_store_relation (id, newname, out, config);
      fflush (out);
      fclose (out);
    }

  return result;
}

			/* --------------- */

int
ar_mec5_load_relation (const char *filename, ccl_config_table *config)
{
  ar_identifier *name = NULL;
  ar_signature *sig = NULL;
  ar_mec5_relation *rel = NULL;
  FILE *in = fopen (filename, "r");
  ccl_serializer_status err = CCL_SERIALIZER_OK;

  if (in == NULL)
    return 0;

  while (!feof (in) && err == CCL_SERIALIZER_OK)
    {
      ar_signature_read (&sig, in, &err);      
      if (err != CCL_SERIALIZER_OK)
	continue;

      name = ar_signature_get_name (sig);
      ar_context_add_signature (MEC5_CONTEXT, name, sig);
      ar_signature_del_reference (sig);

      ar_mec5_relation_read (&rel, in, &err);
      if (err == CCL_SERIALIZER_OK)
	{
	  ar_mec5_add_relation (name, rel);
	  ar_mec5_relation_del_reference (rel);
	}
      ar_identifier_del_reference (name);
    }

  if (err == CCL_SERIALIZER_DATA_ERROR)
    ccl_error ("wrong data or missing info in file '%s'.\n", filename);
  else if (err == CCL_SERIALIZER_IO_ERROR)
    ccl_error ("IO error while reading file '%s'.\n", filename);

  return (err == CCL_SERIALIZER_OK || err == CCL_SERIALIZER_EOF);
}
