/*
 * ar-equations-system.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-list.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "ar-context.h"
#include "ar-expr-p.h"
#include "ar-model.h"
#include "ar-expr2dd.h"
#include "ar-semantics.h"
#include "ar-rel-semantics.h"
#include "mec5.h"
#include "mec5-equations.h"

typedef struct equation_st equation;

struct ar_equations_system_st
{
  int refcount;
  ar_context *ctx;
  int nb_equations;
  equation *toplevel;
};

typedef struct computation_data
{
  ar_equations_system *eqsys;
  ar_ddm ddm;
  ar_dd zero;
  ar_dd one;
  ccl_list *slots;
  int *slots_to_dd_indexes;
  ccl_list *predicates;
  ccl_hash *info_cache;
  ccl_hash *predicates_contexts;
  int *p_last_dd_index;
  ccl_list *unused_indexes;
} compdata;

struct equation_st
{
  equation *next_in_level;
  equation *next_level;
  ar_mec5_relation *mrel;
  int eqindex;
  int last_slot_index;
  int is_local;
  ar_context *ctx;
  int parity;
  ar_identifier *name;
  ar_expr *definition;
  ccl_list *slots;
  ccl_list *qslots;
  int nb_iterations;
  int nb_params;
  ar_expr *params[1];  
};



static equation *
s_equation_create (ar_context *ctx, int parity, ar_identifier *name, 
		   int nb_params, ar_expr **params, ar_expr *definition, 
		   int is_local);

static void
s_equation_delete (equation *eq);

static void
s_equation_log_proto (ccl_log_type log, equation *eq);

static void
s_equation_log (ccl_log_type log, equation *eq);


ar_equations_system *
ar_equations_system_create (ar_context *parent)
{
  ar_equations_system *result = ccl_new (ar_equations_system);

  result->refcount = 1;
  result->ctx = ar_context_create (parent);
  result->toplevel = NULL;
  result->nb_equations = 0;

  return result;
}


ar_equations_system *
ar_equations_system_add_reference (ar_equations_system *eqsys)
{
  ccl_pre (eqsys != NULL); 
  ccl_pre (eqsys->refcount > 0);

  eqsys->refcount++;

  return eqsys;    
}


void
ar_equations_system_del_reference (ar_equations_system *eqsys)
{
  ccl_pre (eqsys != NULL); 
  ccl_pre (eqsys->refcount > 0);

  eqsys->refcount--;
  if (eqsys->refcount == 0)
    {
      equation *e;
      equation *next_level;

      for (e = eqsys->toplevel; e; e = next_level)
	{
	  equation *next_in_level;
	  
	  next_level = e->next_level;
	  
	  for (; e; e = next_in_level)
	    {
	      next_in_level = e->next_in_level;
	      s_equation_delete (e);
	    }
	}
      ar_context_del_reference (eqsys->ctx);
      ccl_delete (eqsys);
    }
}


ar_context *
ar_equations_system_get_context (ar_equations_system *eqsys)
{
  ccl_pre (eqsys != NULL);

  return ar_context_add_reference (eqsys->ctx);
}


static equation *
s_equations_system_get_equation (ar_equations_system *eqsys, 
				 ar_identifier *eqname)
{
  equation *e;
  equation *next_level;

  ccl_pre (eqsys != NULL);
  ccl_pre (eqname != NULL);


  for (e = eqsys->toplevel; e; e = next_level)
    {
      equation *next_in_level;

      next_level = e->next_level;

      for (; e; e = next_in_level)
	{
	  next_in_level = e->next_in_level;

	  if (e->name == eqname)
	    return e;
	}
    }

  return NULL;
}


int
ar_equations_system_has_equation (ar_equations_system *eqsys, 
				  ar_identifier *eqname)
{
  return (s_equations_system_get_equation (eqsys, eqname) != NULL);
}


void
ar_equations_system_add_equation (ar_equations_system *eqsys, ar_context *ctx,
				  int parity, ar_identifier *name, 
				  int nb_params, ar_expr **params, 
				  ar_expr *definition, int is_local)
{
  equation **peq = &eqsys->toplevel;
  equation *eq = s_equation_create (ctx, parity, name, nb_params, params, 
				    definition, is_local);

  ccl_pre (!ar_equations_system_has_equation (eqsys, name));

  eqsys->nb_equations++;

  while (*peq && (*peq)->parity > parity)
    peq = &((*peq)->next_level);

  if (*peq == NULL || (*peq)->parity < parity)
    eq->next_level = *peq;
  else 
    {
      eq->next_level = (*peq)->next_level;
      eq->next_in_level = *peq;
      (*peq)->next_level = NULL;
    }

  *peq = eq;
}


void
ar_equations_system_log (ccl_log_type log, ar_equations_system *eqsys)
{
  int several_eq;
  equation *e;
  equation *next_level;
  
  ccl_pre (eqsys != NULL);

  several_eq = (eqsys->toplevel->next_level || eqsys->toplevel->next_in_level); 

  if (several_eq)
    ccl_log (log, "begin\n");

  for (e = eqsys->toplevel; e; e = next_level)
    {
      equation *next_in_level;

      next_level = e->next_level;

      for (; e; e = next_in_level)
	{
	  next_in_level = e->next_in_level;
	  if (several_eq)
	    ccl_log (log, "  ");
	  if (e->is_local)
	    ccl_log (log, "local ");
	  s_equation_log (log, e);
	  ccl_log (log, ";\n");
	}
    }

  if (several_eq)
    ccl_log (log, "end\n");
}


void
ar_equations_system_log_info (ccl_log_type log, ar_equations_system *eqsys)
{
  equation *e;
  equation *next_level;

  ccl_pre (eqsys != NULL);

  for (e = eqsys->toplevel; e; e = next_level)
    {
      equation *next_in_level;

      next_level = e->next_level;

      for (; e; e = next_in_level)
	{
	  ar_dd rel;
	  double card;

	  next_in_level = e->next_in_level;
	  if (e->is_local)
	    continue;

	  s_equation_log_proto (log, e);
	  rel = ar_mec5_relation_get_dd (e->mrel);
	  ccl_assert (rel != NULL);
	  card = ar_mec5_relation_get_cardinality (e->mrel);

	  ccl_log (log, ": ");
	  if (e->nb_params == 0)
	    {
	      if (rel == ar_dd_one (MEC5_DDM))
		ccl_log (log, "true");
	      else
		ccl_log (log, "false");		
	    }
	  else if (rel == ar_dd_zero (MEC5_DDM))
	    ccl_log (log, "empty");
	  else if (rel == ar_dd_one (MEC5_DDM))
	    ccl_log (log, "complete (%.0f elements)",  card);
	  else
	    {
	      double size = ar_mec5_relation_get_data_structure_size (e->mrel);

	      ccl_log (log, "%.0f elements / %.0f nodes",  card, size);
	    }
	  ccl_log (log, "\n");
	  AR_DD_FREE  (rel);
	}
    }
}


static equation *
s_equation_create (ar_context *ctx, int parity, ar_identifier *name,
		   int nb_params, ar_expr **params, ar_expr *definition, 
		   int is_local)
{ 
  int i;
  equation *result;
  size_t reqsize = sizeof (equation);

  if (nb_params > 1)
    reqsize += sizeof (ar_expr *) * (nb_params - 1);

  result = (equation *) ccl_calloc (reqsize, 1);
  result->ctx = ar_context_add_reference (ctx);
  result->parity = parity;
  result->mrel = NULL;
  result->name = ar_identifier_add_reference (name);
  result->is_local = is_local;
  result->slots = ccl_list_create ();
  result->qslots = ccl_list_create ();
  result->nb_params = nb_params;
  for (i = 0; i < nb_params; i++)
    result->params[i] = ar_expr_add_reference (params[i]);
  result->definition = ar_expr_add_reference (definition);

  return result;
}


static void
s_equation_delete (equation *eq)
{
  int i;

  ccl_list_clear_and_delete (eq->slots, (ccl_delete_proc *)
			     ar_context_slot_del_reference);
  ccl_list_clear_and_delete (eq->qslots, (ccl_delete_proc *)
			     ar_context_slot_del_reference);
  ar_identifier_del_reference (eq->name);
  ccl_zdelete (ar_mec5_relation_del_reference, eq->mrel);
  ar_context_del_reference (eq->ctx);
  for (i = 0; i < eq->nb_params; i++)
    ar_expr_del_reference (eq->params[i]);
  ar_expr_del_reference (eq->definition);
  ccl_delete (eq);
}


static void
s_log_type_var (ccl_log_type log, ar_expr *var)
{
  ar_type *t = ar_expr_get_type (var);
  ar_expr_log (log, var);
  ccl_log (log, " : ");
  ar_type_log (log, t);
  ar_type_del_reference (t);
}


static void
s_equation_log_proto (ccl_log_type log, equation *eq)
{
  ar_identifier_log (log, eq->name);
  ccl_log (log, " (");
  if (eq->nb_params > 0)
    {
      int i;

      s_log_type_var (log, eq->params[0]);

      for (i = 1; i < eq->nb_params; i++)
	{
	  ccl_log (log, ", ");
	  s_log_type_var (log, eq->params[i]);
	}
    }
  ccl_log (log, ") ");
}


static void
s_equation_log (ccl_log_type log, equation *eq)
{
  const char *op;

  ccl_pre (eq != NULL);

  s_equation_log_proto (log, eq);

  if (eq->parity < 0)
    op = ":=";
  else if (eq->parity & 1)
    op = (eq->parity >= 2 ? "+%d=" : "+=");
  else
    op = (eq->parity >= 2 ? "-%d=" : "-=");
  ccl_log (log, op, eq->parity >> 1);
  ccl_log (log, " ");
  ar_expr_log (log, eq->definition);
}


#if 0
static int
s_is_a_slot_ref (ar_expr *e)
{
  return (e->op == AR_EXPR_PARAM || e->op == AR_EXPR_VAR || 
	  e->op == AR_EXPR_STRUCT_MEMBER || e->op == AR_EXPR_ARRAY_MEMBER); 
}
#endif

static void
s_display_slots (ccl_log_type log, compdata *cd)
{
  equation *level;

  for (level = cd->eqsys->toplevel; level; level = level->next_level)
    {
      equation *E;

      for (E = level; E; E = E->next_in_level)
	{
	  int i;
	  ccl_pair *p =  FIRST (E->slots);

	  ar_identifier_log (CCL_LOG_DISPLAY, E->name);
	  ccl_display ("\n");
	  for (i = 0; i < 2; i++)
	    {
	      for (; p; p = CDR (p))
		{
		  ar_identifier *name = ar_context_slot_get_name (CAR (p));
		  int index = ar_context_slot_get_index (CAR (p));
		  ccl_display ("  %04d - %04d ", 
			       cd->slots_to_dd_indexes[index], index);
		  ar_identifier_log (CCL_LOG_DISPLAY, name);
		  ccl_display ("\n");
		  ar_identifier_del_reference (name);
		}
	      p = FIRST (E->qslots);
	    }
	}
    }
}


static void
s_init_equation (compdata *cd, equation *E)
{
  ar_dd dd;

  if (E->parity < 0)
    {
      dd = ar_boolean_expr_to_dd (E->ctx, cd->ddm, cd->slots_to_dd_indexes, 
				  cd->info_cache, E->definition);
    }
  else
    {
      dd = (E->parity & 1) ? cd->zero : cd->one;
      dd = AR_DD_DUP (dd);      
    }

  ar_mec5_relation_set_dds (E->mrel, 1, &dd);
  AR_DD_FREE (dd);
}


static void
s_init_level (compdata *cd, equation *level)
{
  CCL_FIXME(ugly code : the list of equation is not in the declaration order);

  if (level == NULL)
    return;
  s_init_level (cd, level->next_in_level);
  s_init_equation (cd, level);
}


static void
s_compute_fixpoint_for_level (compdata *cd, equation *level)
{
  int changed = 1;

  s_init_level (cd, level);

  while (changed)
    {
      equation *e;

      if (level->next_level != NULL)
	s_compute_fixpoint_for_level (cd, level->next_level);

      changed = 0;
      for (e = level; e; e = e->next_in_level)    
	{
	  ar_dd newval;

	  if (e->parity < 0)
	    continue;
	  e->nb_iterations++;
	  newval = ar_boolean_expr_to_dd (e->ctx, cd->ddm, 
					  cd->slots_to_dd_indexes, 
					  cd->info_cache,
					  e->definition);
	  {
	    ar_dd old = ar_mec5_relation_get_dd (e->mrel);
	    ar_dd tmp;
	    if (e->parity % 2 == 1)
	      tmp = ar_dd_compute_or (cd->ddm, old, newval);
	    else
	      tmp = ar_dd_compute_and (cd->ddm, old, newval);

	    AR_DD_FREE (old);
	    AR_DD_FREE (newval);
	    newval = tmp;
	  }
	  if (ar_mec5_relation_set_dds (e->mrel, 1, &newval))
	    {
	      changed = 1;
	      if (ccl_debug_is_on)
		{
		  double card = 
		    ar_mec5_relation_get_data_structure_size (e->mrel);
		  ccl_debug ("|");
		  ar_identifier_log (CCL_LOG_DEBUG, e->name);
		  ccl_debug ("| = %.0f encoded with %.0f nodes\n", 
			     ar_mec5_relation_get_cardinality (e->mrel),
			     ar_mec5_relation_get_data_structure_size (e->mrel)
			     );
		  if (card < 10)
		    {
		      ar_mec5_relation_log (CCL_LOG_DEBUG, e->mrel);
		      ccl_debug("\n");
		    }
		}
	    }
	  AR_DD_FREE (newval);
	}
    }
}


static void
s_prepare_computation_data (ar_equations_system *eqsys, ar_ddm ddm, 
			    compdata *cd);

static void
s_clear_computation_data (compdata *cd);


void
ar_equations_system_solve (ar_equations_system *eqsys)
{
  compdata data;

  s_prepare_computation_data (eqsys, MEC5_DDM, &data);
  if(0) 
    s_display_slots (CCL_LOG_ERROR, &data);
  s_compute_fixpoint_for_level (&data, eqsys->toplevel);
  
#if 0

  if(0) 
    ar_equations_system_log (CCL_LOG_DISPLAY, eqsys); 
  {
    equation *level;
    for (level = eqsys->toplevel; level; level = level->next_level)
      {
	equation *E;

	for (E = level; E; E = E->next_in_level)
	  {
	    ar_identifier_log (CCL_LOG_DISPLAY, E->name);
	    ccl_display (" (cardinality=%g) computed  in %d iterations \n", 
			 ar_mec5_relation_get_cardinality (E->mrel),
			 E->nb_iterations);
	    if(0)ar_mec5_relation_log (CCL_LOG_DISPLAY, E->mrel);
	    ccl_display ("\n");
	  }
      }
  }
#endif
  s_clear_computation_data (&data);
}


static void
s_index_sub_slots (ar_context *ctx, ar_expr *e, ccl_list *l, compdata *cd)
{
  ccl_pair *p;
  ar_context_slot *sl = ar_expr_get_slot (e);
  ccl_list *slots = ar_context_expand_composite_slot (ctx, sl, NULL);

  ar_context_slot_del_reference (sl);

  for (p = FIRST (slots); p; p = CDR (p))
    {
      sl = CAR (p);      
      ccl_assert (ar_context_slot_get_index (sl) < 0);
      ar_context_slot_set_index (sl, ccl_list_get_size (cd->slots));
      ccl_list_add (cd->slots, ar_context_slot_add_reference (sl));
    }
  ccl_list_append (l, slots);
  ccl_list_delete (slots);
}


static ccl_list *
s_collect_function_calls (ar_context *ctx, ar_expr *e, ccl_list *result,
			  ccl_hash *contexts)
{
  int i;
  ar_expr_op op = ar_expr_get_op (e);
  ar_context *lctx;
  
  if (result == NULL)
    result = ccl_list_create ();

  if (op == AR_EXPR_EXIST || op == AR_EXPR_FORALL)
    lctx = ar_expr_get_quantifier_context (e);
  else
    lctx = ar_context_add_reference (ctx);

  switch (op)
    {
    case AR_EXPR_VAR: case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
    case AR_EXPR_PARAM: case AR_EXPR_CST: 
      break;

    case AR_EXPR_CALL:
      if (!ccl_hash_find (contexts, e))
	{
	  ccl_list_add (result, ar_expr_add_reference (e));
	  ccl_hash_insert (contexts, lctx);
	}

    case AR_EXPR_MIN: case AR_EXPR_MAX:
    case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ: 
    case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ:
    case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL:
    case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
    case AR_EXPR_EXIST: case AR_EXPR_FORALL: case AR_EXPR_ARRAY: 
    case AR_EXPR_STRUCT:
      for (i = 0; i < e->arity; i++)
	s_collect_function_calls (lctx, e->args[i], result, contexts);
      break;

    default:
      ccl_unreachable ();
      break;
    }

  ar_context_del_reference (lctx);

  return result;
}


static void
s_index_slots_n_get_predicates (equation *E, compdata *cd)
{
  int i;
  ccl_pair *p;
  ccl_list *qvars;

  if (cd->predicates_contexts == NULL)
    cd->predicates_contexts = ccl_hash_create (NULL, NULL, NULL, NULL);
  cd->predicates = s_collect_function_calls (E->ctx, E->definition, 
					     cd->predicates,
					     cd->predicates_contexts);
  for (i = 0; i < E->nb_params; i++)
    s_index_sub_slots (E->ctx, E->params[i], E->slots, cd);

  qvars = ar_expr_get_quantified_variables_rec (E->definition, NULL);
  for (p = FIRST (qvars); p; p = CDDR (p))
    {
      ar_context *ctx = CAR (p);
      ar_expr *var = CADR (p);
        
      s_index_sub_slots (ctx, var, E->qslots, cd);

      ar_expr_del_reference (var);
      ar_context_del_reference (ctx);
    }
  ccl_list_delete (qvars);
}


static int
s_allocate_dd_index (compdata *cd)
{
  if (! ccl_list_is_empty (cd->unused_indexes))
    return (int) (intptr_t) ccl_list_take_first (cd->unused_indexes);
  return (*cd->p_last_dd_index)++;
}


#if 0
static void
s_interleave_dd_indexes_for_parameters (equation *E, compdata *cd)
{
  int i;
  int stop;
  ccl_list **slots;

  if (E->nb_params <= 1)
    return;

  slots = ccl_new_array (ccl_list *, E->nb_params);
  for (i = 0; i < E->nb_params; i++)
    {
      ar_context_slot *sl = ar_expr_get_slot (E->params[i]);

      slots[i] = ar_context_expand_composite_slot (E->ctx, sl, NULL);
      if (ccl_list_is_empty (slots[i]))
	{
	  ccl_list_delete (slots[i]);
	  slots[i] = NULL;
	}
      ar_context_slot_del_reference (sl);
    }

  do
    {
      stop = 1;
      for (i = 0; i < E->nb_params; i++)
	{
	  if (slots[i] != NULL)
	    {
	      ar_context_slot *sl = ccl_list_take_first (slots[i]);
	      int index = ar_context_slot_get_index (sl);

	      if (cd->slots_to_dd_indexes[index] < 0)
		cd->slots_to_dd_indexes[index] = s_allocate_dd_index (cd);

	      if (ccl_list_is_empty (slots[i]))
		{
		  ccl_list_delete (slots[i]);
		  slots[i] = NULL;
		}
	      else
		{
		  stop = 0;
		}
	    }
	}
    }
  while (! stop);
  ccl_delete (slots);
}
#endif

static ar_expr *
s_crt_e_in_dom_e (ar_expr *e)
{
  ar_type *type = ar_expr_get_type (e);
  ar_expr *in_dom = ar_expr_crt_in_domain (e, type);
  ar_type_del_reference (type);

  return in_dom;
}


static void
s_add_type_constraints (equation *E, compdata *cd)
{
  int i;
  ar_expr *tmp;
  ar_expr *tc;

  ar_expr_add_type_constraints_in_quantifiers (E->definition);
  if (E->nb_params == 0)
    return;

  tc = s_crt_e_in_dom_e (E->params[0]);
  for (i = 1; i < E->nb_params; i++)
    {
      ar_expr *in_dom = s_crt_e_in_dom_e (E->params[i]);
      tmp = ar_expr_crt_binary (AR_EXPR_AND, tc, in_dom);
      ar_expr_del_reference (tc);
      ar_expr_del_reference (in_dom);
      tc = tmp;
    }
  tmp = ar_expr_crt_binary (AR_EXPR_AND, E->definition, tc);
  ar_expr_del_reference (E->definition);
  ar_expr_del_reference (tc);
  E->definition = tmp;
}


static void
s_foreach_equation (compdata *cd, void (*apply)(equation *, compdata *data))
{
  equation *level;

  for (level = cd->eqsys->toplevel; level; level = level->next_level)
    {
      equation *E;

      for (E = level; E; E = E->next_in_level)
	apply (E, cd);
    }
}


static int
s_is_external_reference (const compdata *cd, ar_identifier *name)
{
  equation *level;

  for (level = cd->eqsys->toplevel; level; level = level->next_level)
    {
      equation *E;

      for (E = level; E; E = E->next_in_level)
	if (E->name == name)
	  return 0;
    }

  return 1;
}

#if 0
static int
s_max_arg_width (ar_expr *P)
{
  int i;
  int result = 0;
  int arity;
  ar_expr **args = ar_expr_get_args(P, &arity);

  for (i = 0; i < arity; i++)
    {
      ar_type *t = ar_expr_get_type (args[i]);
      int w = ar_type_get_width (t);
      if (result < w)
	result = w;
      ar_type_del_reference (t);
    }

  return result;
}
#endif
#if 0
static ccl_list *
s_collect_var_exprs (ar_expr *e, ccl_list *result)
{
  ar_expr_op op = ar_expr_get_op (e);

  if (result == NULL)
    result = ccl_list_create ();

  if (op == AR_EXPR_VAR)
    {
      ar_context_slot *sl = ar_expr_get_slot (e);
      if (! ccl_list_has (result, sl))
	ccl_list_add (result, sl);
      else
	ar_context_slot_del_reference (sl);
    }
  else 
    {
      int i;
      int arity; 
      ar_expr **args = ar_expr_get_args (e, &arity);
      for (i = 0; i < arity; i++)
	s_collect_var_exprs (args[i], result);
    }

  return result;
}
#endif
#if 0
static int
s_get_nb_slots (ar_expr *e)
{
  int result = 0;
  ccl_list *vars = s_collect_var_exprs (e, NULL);

  while (!ccl_list_is_empty (vars))
    {
      ar_context_slot *sl = ccl_list_take_first (vars);
      ar_type *t = ar_context_slot_get_type (sl);
      result += ar_type_get_width (t);      
      ar_type_del_reference (t);
      ar_context_slot_del_reference (sl);
    }
  ccl_list_delete (vars);

  return result;
}
#endif
#if 0
static int
s_get_max_nb_slots_in_args (ar_expr *e)
{
  int arity;
  int result = 0;
  ar_expr **args = ar_expr_get_args (e, &arity);
  
  while (arity--)
    {
      int n = s_get_nb_slots (args[arity]);
      if (result < n)
	result = n;
    }
  return result;
}
#endif

static void
s_release_dd_index_of_qslots (compdata *cd, ar_expr *e)
{
  ccl_pair *p;
  ccl_list *slots = NULL;
  ccl_list *qvars = ar_expr_get_quantified_variables (e);

  for (p = FIRST (qvars); p; p = CDR (p))
    {
      ar_context_slot *sl = ar_expr_get_slot ((ar_expr *) CAR (p));
      slots = ar_context_expand_composite_slot(e->qctx, sl, slots);
      ar_context_slot_del_reference (sl);
    }

  for (p = FIRST (slots); 0 && p; p = CDR (p))
    {
      int index = ar_context_slot_get_index (CAR (p));

      if (cd->slots_to_dd_indexes[index] < 0)
	cd->slots_to_dd_indexes[index] = s_allocate_dd_index (cd);
    }

  while (!ccl_list_is_empty (slots))
    {
      ar_context_slot *sl = ccl_list_take_first (slots);
      int index = ar_context_slot_get_index (sl);
      int ddindex = cd->slots_to_dd_indexes[index];

      if (ddindex >= 0)
	{
	  ccl_assert (! ccl_list_has (cd->unused_indexes, 
				      (void *) (intptr_t) ddindex));
	  ccl_list_insert (cd->unused_indexes, (void *) (intptr_t) ddindex, 
			   NULL);
	}
      ar_context_slot_del_reference (sl);
    }
  ccl_list_delete (slots);
}


static void
s_make_dd_indexes (compdata *cd, ar_context *ctx, ar_expr *e)
{
  switch (e->op)
    {
    case AR_EXPR_CST: 
      return;

    case AR_EXPR_PARAM: case AR_EXPR_VAR: case AR_EXPR_STRUCT_MEMBER: 
    case AR_EXPR_ARRAY_MEMBER:       
      CCL_FIXME (explode the slot sl in order to index sub-slots);
      {
	ar_context_slot *sl = ar_expr_eval_slot (e, ctx);
	int index = ar_context_slot_get_index (sl);

	if (index >= 0)
	  {
	    if (cd->slots_to_dd_indexes[index] == -1)
	      {
		cd->slots_to_dd_indexes[index] = s_allocate_dd_index (cd);
		
		if (ccl_debug_is_on)
		  {
		    ar_expr_log (CCL_LOG_DEBUG, e);
		    ccl_debug ("-> %d\n", cd->slots_to_dd_indexes[index]);
		  }
		
	      }
	  }
	ar_context_slot_del_reference (sl);
      }
      return;

    case AR_EXPR_EQ: case AR_EXPR_NEQ:
      if (ar_expr_is_scalar (e))
	{
	  s_make_dd_indexes (cd, ctx, e->args[0]);
	  s_make_dd_indexes (cd, ctx, e->args[1]);
	}
      else
	{
	  ccl_list *arg1 = 
	    ar_expr_expand_composite_types (e->args[0], ctx, NULL);
	  ccl_list *arg2 = 
	    ar_expr_expand_composite_types (e->args[1], ctx, NULL);

	  ccl_assert (ccl_list_get_size(arg1) == ccl_list_get_size(arg2));
	  ccl_assert (ccl_list_get_size(arg1) > 0);

	  while (!ccl_list_is_empty(arg1))
	    {
	      ar_expr *a1 = (ar_expr *) ccl_list_take_first (arg1);
	      ar_expr *a2 = (ar_expr *) ccl_list_take_first (arg2);

	      s_make_dd_indexes (cd, ctx, a1);
	      s_make_dd_indexes (cd, ctx, a2);

	      ar_expr_del_reference(a1);
	      ar_expr_del_reference(a2);
	    }
	  ccl_list_delete(arg1);
	  ccl_list_delete(arg2);
	}
      return;

    case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:case AR_EXPR_ITE:
    case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_LT: case AR_EXPR_GT: 
    case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: 
    case AR_EXPR_MUL: case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: 
    case AR_EXPR_NEG: case AR_EXPR_EXIST: case AR_EXPR_FORALL:       
    case AR_EXPR_CALL: case AR_EXPR_MIN: case AR_EXPR_MAX:
      {
	int i;
	ar_context *lctx = ctx;

	if (e->op == AR_EXPR_EXIST || e->op == AR_EXPR_FORALL)
	  {
	    lctx = e->qctx;
	  }

	for (i = 0; i < e->arity; i++)
	  s_make_dd_indexes (cd, lctx, e->args[i]);

	if (e->op == AR_EXPR_EXIST || e->op == AR_EXPR_FORALL)
	  {
	    if(0)
	      s_release_dd_index_of_qslots (cd, e);
	  }
      }
      return;
    };

  ccl_unreachable ();
}


static ar_context *
s_get_predicate_context (compdata *cd, ar_expr *e)
{
  ccl_pre (ccl_hash_find (cd->predicates_contexts, e));
  ccl_hash_find (cd->predicates_contexts, e);
  
  return ccl_hash_get (cd->predicates_contexts);
}


static void
s_make_external_predicate_dd_relabelling (compdata *cd, ar_expr *P)
{
  ar_signature *sig = ar_expr_call_get_signature (P);
  ar_identifier *name = ar_signature_get_name (sig);

  if (s_is_external_reference (cd, name))	
    {	  
      int i;
      int width = ar_signature_get_size (sig);
      ar_context *ctx = s_get_predicate_context (cd, P);
      ar_mec5_relation *mrel = ar_mec5_get_relation (name);
      int *ocols = ccl_new_array (int, width);
      ccl_list *subargs = ar_expr_call_get_subargs (P, ctx);

      ccl_assert (ar_mec5_relation_get_arity (mrel) == width);
      ar_mec5_relation_get_ordered_columns (mrel, ocols);
      i = 0;
      while (i < width)
	{
	  int j = i;
	  int cddindex = ar_mec5_relation_get_ith_dd_index (mrel, ocols[i]);


	  while (j < width && 
		 cddindex == ar_mec5_relation_get_ith_dd_index (mrel, 
								ocols[j]))
	    {
	      int c = ocols[j];
	      ar_expr *arg = ccl_list_get_at (subargs, c);

	      s_make_dd_indexes (cd, ctx, arg);
	      j++;
	    }
	  i = j;
	}
      ccl_delete (ocols);
      ar_mec5_relation_del_reference (mrel);
    }
  ar_signature_del_reference (sig);
  ar_identifier_del_reference (name);
}


static int
s_is_external_call (const compdata *cd, const ar_expr *fc)
{
  ar_signature *sig = ar_expr_call_get_signature (fc);
  ar_identifier *id = ar_signature_get_name (sig);
  int result = s_is_external_reference (cd, id);
  ar_identifier_del_reference (id);
  ar_signature_del_reference (sig);

  return result;
}


static int
s_weight (const compdata *cd, const ar_expr *e)
{
  int i;
  int result = 0;
  ar_expr_op op = ar_expr_get_op (e);
  
  switch (op)
    {
    case AR_EXPR_VAR: 
      result = 1;
      break;


    case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
    case AR_EXPR_PARAM: case AR_EXPR_CST: 
      break;

    case AR_EXPR_CALL:
      if (s_is_external_call (cd, e))
	{
	  ar_signature *sig = ar_expr_call_get_signature (e);
	  result = ar_signature_get_size (sig);
	  ar_signature_del_reference (sig);
	}

    case AR_EXPR_MIN: case AR_EXPR_MAX:
    case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ: 
    case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ:
    case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL:
    case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
    case AR_EXPR_EXIST: case AR_EXPR_FORALL: case AR_EXPR_ARRAY: 
    case AR_EXPR_STRUCT:
      for (i = 0; i < e->arity; i++)
	result += s_weight (cd, e->args[i]);
      break;

    default:
      ccl_unreachable ();
      break;
    }

  return result;
}


static int
s_cmp_args (const void *E1, const void *E2, void *data)
{
  int result = s_weight (data, E2) - s_weight (data, E1);
  
  return result;
}


static void
s_make_dd_indexes_for_predicates (compdata *cd, ar_context *ctx, ar_expr *e)
{
  switch (e->op)
    {
    case AR_EXPR_CST: 
    case AR_EXPR_PARAM: case AR_EXPR_VAR: case AR_EXPR_STRUCT_MEMBER: 
    case AR_EXPR_ARRAY_MEMBER:       
      return;

    case AR_EXPR_EQ: case AR_EXPR_NEQ:
      if (ar_expr_is_scalar (e))
	{
	  s_make_dd_indexes_for_predicates (cd, ctx, e->args[0]);
	  s_make_dd_indexes_for_predicates (cd, ctx, e->args[1]);
	}
      else
	{
	  ccl_list *arg1 = 
	    ar_expr_expand_composite_types (e->args[0], ctx, NULL);
	  ccl_list *arg2 = 
	    ar_expr_expand_composite_types (e->args[1], ctx, NULL);

	  ccl_assert (ccl_list_get_size(arg1) == ccl_list_get_size(arg2));
	  ccl_assert (ccl_list_get_size(arg1) > 0);

	  while (!ccl_list_is_empty(arg1))
	    {
	      ar_expr *a1 = (ar_expr *) ccl_list_take_first (arg1);
	      ar_expr *a2 = (ar_expr *) ccl_list_take_first (arg2);

	      s_make_dd_indexes_for_predicates (cd, ctx, a1);

	      ar_expr_del_reference(a1);
	      ar_expr_del_reference(a2);
	    }
	  ccl_list_delete(arg1);
	  ccl_list_delete(arg2);
	}
      return;

    case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:case AR_EXPR_ITE:
    case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_LT: case AR_EXPR_GT: 
    case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: 
    case AR_EXPR_MUL: case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: 
    case AR_EXPR_NEG: case AR_EXPR_CALL: case AR_EXPR_MIN: case AR_EXPR_MAX:
      {
	int i;
	ccl_list *ordered_args = ccl_list_create ();

	for (i = 0; i < e->arity; i++)
	  ccl_list_add (ordered_args, e->args[i]);

	ccl_list_sort_with_data (ordered_args, s_cmp_args, cd);
	while (! ccl_list_is_empty (ordered_args))
	  {
	    ar_expr *arg = ccl_list_take_first (ordered_args);
	    s_make_dd_indexes_for_predicates (cd, ctx, arg);
	  }
	ccl_list_delete (ordered_args);

	if (e->op == AR_EXPR_CALL)
	  s_make_external_predicate_dd_relabelling (cd, e);
      }
      return;

    case AR_EXPR_EXIST: case AR_EXPR_FORALL: 
      {
	int all_indexed = 1;
	ccl_list *vars;

	s_make_dd_indexes_for_predicates (cd, e->qctx, e->args[0]);
	s_make_dd_indexes (cd, e->qctx, e->args[0]);
	vars = ar_expr_get_slots (e->args[0], e->qctx, NULL);
	while (!ccl_list_is_empty (vars))
	  {
	    ar_context_slot *sl = ccl_list_take_first (vars);
	    int index = ar_context_slot_get_index (sl);
	    int ddindex = cd->slots_to_dd_indexes[index];

	    all_indexed = all_indexed && (ddindex >= 0);
	    ar_context_slot_del_reference (sl);
	  }
	ccl_list_delete (vars);
	if(all_indexed) 
	  s_release_dd_index_of_qslots (cd, e);
      }
      return;
    };

  ccl_unreachable ();
}


static void
s_make_external_predicates_dd_relabellings (compdata *cd)
{
  equation *level;
   
  for (level = cd->eqsys->toplevel; level; level = level->next_level)
    {
      equation *E;

      for (E = level; E; E = E->next_in_level)
	{
	  s_make_dd_indexes_for_predicates (cd, E->ctx, E->definition);
	  ccl_list_clear (cd->unused_indexes, NULL);
	}
    }
}


static int 
s_current_max_dd_index (compdata *cd)
{
  ccl_pair *p;
  int result = 0;
  
  for (p = FIRST (cd->predicates); p; p = CDR (p))
    {
      ar_expr *P = CAR (p);
      ar_signature *sig = ar_expr_call_get_signature (P);
      ar_identifier *name = ar_signature_get_name (sig);

      if (s_is_external_reference (cd, name))
	{	  
	  ar_mec5_relation *mrel = ar_mec5_get_relation (name);
	  int max = ar_mec5_relation_get_max_dd_index (mrel);
	  if (result < max)
	    result = max;
	  ar_mec5_relation_del_reference (mrel);
	}

      ar_identifier_del_reference (name);
      ar_signature_del_reference (sig);
    }

  return result;
}


static void
s_make_dd_indexes_for_equation (equation *E, compdata *cd)
{
  s_make_dd_indexes (cd, E->ctx, E->definition);
}


static void
s_crt_mec5_relation (equation *E, compdata *cd)
{
  ccl_pair *p;

  E->mrel = ar_mec5_relation_create ();

  for (p = FIRST (E->slots); p; p = CDR (p))
    {
      ar_context_slot *sl = (ar_context_slot *) CAR (p);
      ar_identifier *name = ar_context_slot_get_name (sl);
      ar_type *type = ar_context_slot_get_type (sl);
      int index = cd->slots_to_dd_indexes[ar_context_slot_get_index (sl)];
      ccl_assert (index >= 0);
      ar_mec5_relation_add_column (E->mrel, name, type, index);
      ar_identifier_del_reference (name);
      ar_type_del_reference (type);
    }

  ar_mec5_add_relation (E->name, E->mrel);
}


static ccl_list *
s_ordered_equations (compdata *cd)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create ();

  ccl_list_add (result, cd->eqsys->toplevel);
  for (p = FIRST (cd->predicates); p; p = CDR (p))
    {
      ar_signature *sig = ar_expr_call_get_signature (CAR (p));
      ar_identifier *id = ar_signature_get_name (sig);
      equation *e = s_equations_system_get_equation (cd->eqsys, id);
      if (e != NULL && !ccl_list_has (result, e))
	ccl_list_add (result, e);
      ar_signature_del_reference (sig);      
      ar_identifier_del_reference (id);
    }

  return result;
}

static void
s_graph_for_dd_indexing (equation *e, compdata *cd);


static void
s_prepare_computation_data (ar_equations_system *eqsys, ar_ddm ddm, 
			    compdata *cd)
{
  int i, nb_slots;
  int last_dd_index;
  ccl_pair *p;
  ccl_list *ordered_equations;
  ccl_memzero (cd, sizeof (*cd));
  cd->p_last_dd_index = &last_dd_index;
  cd->ddm = ddm;
  cd->one = ar_dd_one (cd->ddm);
  cd->zero = ar_dd_zero (cd->ddm);
  cd->eqsys = eqsys;
  cd->unused_indexes = ccl_list_create ();
  cd->info_cache = ar_expr2dd_create_info_cache ();

  s_foreach_equation (cd, s_add_type_constraints);
  cd->slots = ccl_list_create ();
  s_foreach_equation (cd, s_index_slots_n_get_predicates);
  nb_slots = ccl_list_get_size (cd->slots);
  cd->slots_to_dd_indexes = ccl_new_array (int, nb_slots);
  for (i = 0; i < nb_slots; i++)
    cd->slots_to_dd_indexes[i] = -1;
  last_dd_index = s_current_max_dd_index (cd) + 1;  
  s_foreach_equation (cd, s_graph_for_dd_indexing);
  s_make_external_predicates_dd_relabellings (cd);

  ordered_equations = s_ordered_equations (cd);
  for (p = FIRST (ordered_equations); p; p = CDR (p))
    s_make_dd_indexes_for_equation (CAR (p), cd);

  for (p = FIRST (cd->slots); p; p = CDR (p))
    {
      ar_context_slot *sl = CAR (p);
      int index = ar_context_slot_get_index (sl);
      
      if (cd->slots_to_dd_indexes[index] == -1)
	cd->slots_to_dd_indexes[index] = (*cd->p_last_dd_index)++;
    }
  ccl_list_delete (ordered_equations);
  s_foreach_equation (cd, s_crt_mec5_relation);
}

static int
s_is_bang_rel_of (equation *e, ar_node *n, ar_identifier **pid)
{
  char *estr = ar_identifier_to_string (e->name);
  ar_identifier *nid = ar_node_get_name (n);
  char *str = ar_identifier_to_string (nid);
  int result = 0;
  const char *relid = NULL;
  
  ccl_string_format_append (&str, "!");
  relid = ccl_string_has_prefix (str, estr);
  if (relid != NULL)
    {
      result = 1;
      *pid = ar_identifier_create (relid);
    }
  ccl_string_delete (str);
  ccl_string_delete (estr);
  
  return result;
}

static void
s_register_in_acheck (equation *e, compdata *cd)
{
  ar_type *t;
  
  if (e->is_local || (e->nb_params != 1 && e->nb_params != 3))
    return;
  
  t = ar_expr_get_type (e->params[0]);
  if (ar_type_get_kind (t) == AR_TYPE_BANG)
    {
      ar_identifier *ename;
      ar_node *n = ar_type_bang_get_node (t);
      
      if (s_is_bang_rel_of (e, n, &ename))
	{
	  ccl_try (exception)
  	    {
	      int is_state = 0;
	      ar_relsem *rs = ar_semantics_get (n, AR_SEMANTICS_RELATIONS);
	      ar_sgs *sgs = (ar_sgs *) rs;
	      
	      if (! ar_sgs_has_set (sgs, ename, &is_state))
		{
		  ar_sgs_set *set = NULL;
		  if ((is_state =
		       ar_mec5_is_a_configuration_relation (e->mrel, n)))
		    set = ar_relsem_translate_configuration_relation (rs,
								      e->mrel);
		  else if (ar_mec5_is_a_transition_relation (e->mrel, n))
		    set = ar_relsem_translate_transition_relation (rs, e->mrel);
		  if (set != NULL)
		    {
		      if (is_state)
			ar_sgs_add_state_set (sgs, ename, set);
		      else
			ar_sgs_add_trans_set (sgs, ename, set);
		      ar_sgs_set_del_reference (set);
		    }
		}
	      ar_relsem_del_reference (rs);
	    }
	  ccl_no_catch;
	  ar_identifier_del_reference (ename);
	}
      ar_node_del_reference (n);
    }
  ar_type_del_reference (t);
}

static void
s_remove_local_equations (equation *e, compdata *cd)
{
  if (!e->is_local)
    return;
  ar_mec5_remove_relation (e->name);
}

			
static void
s_clear_computation_data (compdata *cd)
{
  ar_dd_remove_foreign_memoization_records (cd->ddm);
  s_foreach_equation (cd, s_register_in_acheck);
  s_foreach_equation (cd, s_remove_local_equations);
  ccl_list_clear_and_delete (cd->slots, (ccl_delete_proc *)
			     ar_context_slot_del_reference);
  ccl_delete (cd->slots_to_dd_indexes);
  ccl_hash_delete (cd->info_cache);
  ccl_hash_delete (cd->predicates_contexts);
  ccl_list_clear_and_delete (cd->predicates, (ccl_delete_proc *)
			     ar_expr_del_reference);
  ccl_list_delete (cd->unused_indexes);
}

static ccl_graph * 
s_compute_graph (compdata *cd, ar_context *ctx, ar_expr *e, ccl_graph *G)
{
  switch (e->op)
    {
    case AR_EXPR_CST: 
      return G;

    case AR_EXPR_PARAM: case AR_EXPR_VAR: case AR_EXPR_STRUCT_MEMBER: 
    case AR_EXPR_ARRAY_MEMBER:       
      {
	ar_context_slot *sl = ar_expr_eval_slot (e, ctx);

#ifdef CCL_ENABLE_ASSERTIONS
	ar_type *t = ar_context_slot_get_type (sl);
	ccl_assert (ar_type_is_scalar (t));
	ar_type_del_reference (t);
#endif

	ccl_graph_find_or_add_vertex (G, sl);
	ar_context_slot_del_reference (sl);
      }
      return G;

    case AR_EXPR_EQ: case AR_EXPR_NEQ:
      {
	ccl_list *arg1 = 
	  ar_expr_expand_composite_types (e->args[0], ctx, NULL);
	ccl_list *arg2 = 
	  ar_expr_expand_composite_types (e->args[1], ctx, NULL);
	
	ccl_assert (ccl_list_get_size(arg1) == ccl_list_get_size(arg2));
	ccl_assert (ccl_list_get_size(arg1) > 0);
	
	while (!ccl_list_is_empty (arg1))
	  {
	    ccl_pair *p;
	    ar_expr *a1 = (ar_expr *) ccl_list_take_first (arg1);
	    ccl_list *slots1 = ar_expr_get_slots (a1, ctx, NULL);
	    ar_expr *a2 = (ar_expr *) ccl_list_take_first (arg2);
	    ccl_list *slots2 = ar_expr_get_slots (a2, ctx, NULL);

	    for (p = FIRST (slots2); p; p = CDR (p))
	      {
		ccl_pair *pp;

		for (pp = FIRST (slots1); pp; pp = CDR (pp))
		  ccl_graph_add_data_edge (G, CAR (p), CAR (pp), NULL);
	      }
	    ccl_list_clear_and_delete (slots1 , (ccl_delete_proc *)
				       ar_context_slot_del_reference);
	    ccl_list_clear_and_delete (slots2 , (ccl_delete_proc *)
				       ar_context_slot_del_reference);
	    ar_expr_del_reference(a1);
	    ar_expr_del_reference(a2);
	  }
	ccl_list_delete(arg1);
	ccl_list_delete(arg2);
      }
      return G;

    case AR_EXPR_EXIST: case AR_EXPR_FORALL: 
      ctx = e->qctx;

    case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:case AR_EXPR_ITE:
    case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_LT: case AR_EXPR_GT: 
    case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: 
    case AR_EXPR_MUL: case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: 
    case AR_EXPR_NEG: case AR_EXPR_MIN: case AR_EXPR_MAX:
      {
	int i;

	for (i = 0; i < e->arity; i++)
	  s_compute_graph (cd, ctx, e->args[i], G);
      }
      return G;

    case AR_EXPR_CALL: 
      {
	ar_signature *sig = ar_expr_call_get_signature (e);
	int width = ar_signature_get_size (sig);
	ar_identifier *name = ar_signature_get_name (sig);
	ccl_list *subargs = ar_expr_call_get_subargs (e, ctx);
	ccl_list *slots = ccl_list_create ();

	if (s_is_external_reference (cd, name))	
	  {	  
	    int i;
	    ar_mec5_relation *mrel = ar_mec5_get_relation (name);
	    int *ocols = ccl_new_array (int, width);
	    
	    ccl_assert (ar_mec5_relation_get_arity (mrel) == width);
	    ar_mec5_relation_get_ordered_columns (mrel, ocols);
	    i = 0;
	    while (i < width)
	      {
		int j = i;
		int cddindex = 
		  ar_mec5_relation_get_ith_dd_index (mrel, ocols[i]);
				
		while (j < width && 
		       cddindex == ar_mec5_relation_get_ith_dd_index (mrel, 
								      ocols[j]))
		  {
		    int c = ocols[j];
		    ar_expr *arg = ccl_list_get_at (subargs, c);
		    slots = ar_expr_get_slots (arg, ctx, slots);
		    j++;
		  }
		i = j;
	      }
	    ccl_delete (ocols);
	    ar_mec5_relation_del_reference (mrel);
	  }
	else if (0)
	  {
	    ccl_pair *p;

	    for (p = FIRST (subargs); p; p = CDR (p))
	      slots = ar_expr_get_slots (CAR (p), ctx, slots);
	  }
	if (! ccl_list_is_empty (slots))
	  {
	    ar_context_slot *sl = ccl_list_take_first (slots);
	    ccl_vertex *v = ccl_graph_find_or_add_vertex (G, sl);

	    ar_identifier_log (CCL_LOG_DEBUG, ar_context_slot_get_name (sl));

	    ar_context_slot_del_reference (sl);

	    while (! ccl_list_is_empty (slots))
	      {
		ar_context_slot *sl = ccl_list_take_first (slots);
		ccl_vertex *u = ccl_graph_find_or_add_vertex (G, sl);
		ccl_graph_add_edge (G, u, v, NULL);
		ccl_debug (" <- ");
		ar_identifier_log (CCL_LOG_DEBUG, ar_context_slot_get_name (sl));
		v = u;
		ar_context_slot_del_reference (sl);
	      }
	    ccl_debug ("\n");
	  }
	ccl_list_delete (slots);
	ar_signature_del_reference (sig);
	ar_identifier_del_reference (name);
      }
      return G;
    };

  ccl_unreachable ();
  return NULL;
}

#if 0
static void 
s_log_slot (ccl_log_type log, ccl_vertex *v, void *cbdata)
{
  void *data = ccl_vertex_get_data (v);
  ar_identifier *id = ar_context_slot_get_name (data);
  ar_identifier_log (log, id);
  ar_identifier_del_reference (id);
}
#endif

static int 
s_enqueue (ccl_vertex *v, void *data)
{
  void **t = data;
  ccl_list_put_first (t[0], ccl_vertex_get_data (v));
  ccl_hash_find (t[1], v);
  ccl_hash_insert (t[1], v);

  return 0;
}
			
static void
s_graph_for_dd_indexing (equation *e, compdata *cd)
{
  static ccl_vertex_data_methods VM = { NULL, NULL, NULL };
  static ccl_edge_data_methods EM = { NULL, NULL, NULL };

  int stop = 0;
  ccl_hash *visited = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_graph *G = ccl_graph_create (&VM, &EM);
  ccl_list *order = ccl_list_create ();

  s_compute_graph (cd, e->ctx, e->definition, G);

  while (!stop)
    {
      int mindegree = ccl_graph_get_number_of_vertices (G);
      ccl_vertex *minv = NULL;
      ccl_vertex_iterator *iv = ccl_graph_get_vertices (G);

      while (ccl_iterator_has_more_elements (iv))
	{
	  ccl_vertex *v = ccl_iterator_next_element (iv);
	  if (! ccl_hash_find (visited, v))
	    {
	      int ind = ccl_vertex_get_in_degree (v);

	      if (minv == NULL || ind < mindegree)
		{
		  mindegree = ind;
		  minv = v;
		}
	    }
	}
      ccl_iterator_delete (iv);

      if (minv != NULL)
	{
	  void *t[2] = { order, visited };

	  ccl_graph_bfs (G, minv, 0, s_enqueue, t);
	}
      stop = 
	(ccl_hash_get_size (visited) == ccl_graph_get_number_of_vertices (G));
    }
  ccl_hash_delete (visited);

  ccl_graph_del_reference (G);
  while (!ccl_list_is_empty (order))
    {
      ar_context_slot *sl = ccl_list_take_first (order);
      int index = ar_context_slot_get_index (sl);

      if (cd->slots_to_dd_indexes[index] < 0)
	cd->slots_to_dd_indexes[index] = s_allocate_dd_index (cd);
      
    }
  ccl_list_delete (order);
}

