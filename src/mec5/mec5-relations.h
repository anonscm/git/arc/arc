/*
 * mec5-relations.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef MEC5_RELATIONS_H
# define MEC5_RELATIONS_H

# include "ar-bang-ids.h"
# include "ar-interp-altarica-exception.h"
# include "ar-context.h"
# include "ar-node.h"
# include "ar-dd.h"

typedef struct ar_mec5_relation_st ar_mec5_relation;

extern ar_mec5_relation *
ar_mec5_relation_create (void);

extern ar_mec5_relation *
ar_mec5_relation_pick_one_element (ar_mec5_relation *mrel);

extern ar_mec5_relation *
ar_mec5_relation_add_reference (ar_mec5_relation *mrel);

extern void
ar_mec5_relation_del_reference (ar_mec5_relation *mrel);

extern int
ar_mec5_relation_get_arity (const ar_mec5_relation *mrel);

extern void
ar_mec5_relation_get_ordered_columns (ar_mec5_relation *mrel, int *cols);

extern void
ar_mec5_relation_add_column (ar_mec5_relation *mrel, ar_identifier *name,
			     ar_type *type, int index);

extern ar_dd *
ar_mec5_relation_get_dds (const ar_mec5_relation *mrel, int *p_nb_dds);

extern ar_dd 
ar_mec5_relation_get_dd (ar_mec5_relation *mrel);

extern int
ar_mec5_relation_set_dds (ar_mec5_relation *mrel, int nb_dds, ar_dd *dd);

extern int
ar_mec5_relation_get_max_dd_index (ar_mec5_relation *mrel);

extern int
ar_mec5_relation_get_ith_dd_index (const ar_mec5_relation *mrel, int i);

extern void
ar_mec5_relation_set_ith_dd_index (ar_mec5_relation *mrel, int i, int index);

extern ar_identifier *
ar_mec5_relation_get_ith_arg_name (const ar_mec5_relation *mrel, int i);

extern void
ar_mec5_relation_set_ith_arg_name (ar_mec5_relation *mrel, int i, 
				   ar_identifier *id);

extern ar_type *
ar_mec5_relation_get_ith_arg_type (const ar_mec5_relation *mrel, int i);

extern void
ar_mec5_relation_set_ith_arg_type (ar_mec5_relation *mrel, int i, 
				   ar_type *type);

extern double
ar_mec5_relation_get_cardinality (ar_mec5_relation *mrel);

extern double
ar_mec5_relation_get_data_structure_size (ar_mec5_relation *mrel);

extern void
ar_mec5_relation_log (ccl_log_type log, ar_mec5_relation *mrel);

extern void
ar_mec5_relation_write (ar_mec5_relation *mrel, FILE *out, 
			ccl_serializer_status *p_err);


extern void
ar_mec5_relation_read (ar_mec5_relation **p_rel, FILE *in, 
		       ccl_serializer_status *p_err);


			/* --------------- */

extern void
ar_mec5_build_bang_relation (ar_node *node, ar_bang_t bang)
  CCL_THROW ((altarica_interpretation_exception, domain_cardinality_exception));

/*!
 * return a non-null value if mrel is a relation over configurations of 'node'
 * i.e. its type is 'node!sc'.
 */
extern int
ar_mec5_is_a_configuration_relation (ar_mec5_relation *mrel, ar_node *node);

/*!
 * return a non-null value if mrel is a transition relation over 'node'
 * i.e. its type is 'node!sc x node!ev x node!sc'.
 */
extern int
ar_mec5_is_a_transition_relation (ar_mec5_relation *mrel, ar_node *node);

#endif /* ! MEC5_RELATIONS_H */
