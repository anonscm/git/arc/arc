/*
 * mec5.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef MEC5_H
# define MEC5_H

# include <ccl/ccl-config-table.h>
# include "ar-context.h"
# include "ar-dd.h"
# include "mec5/mec5-relations.h"

extern ar_context *MEC5_CONTEXT;
extern ar_ddm MEC5_DDM;

extern int
ar_mec5_init (ccl_config_table *conf);

extern void
ar_mec5_terminate (void);

extern ar_mec5_relation *
ar_mec5_get_relation (ar_identifier *name);

extern int
ar_mec5_has_relation (ar_identifier *name);

extern int
ar_mec5_has_relation_no_bang_creation (ar_identifier *name);

extern void
ar_mec5_add_relation (ar_identifier *name, ar_mec5_relation *mrel);

extern void
ar_mec5_remove_relation (ar_identifier *name);

extern ccl_list *
ar_mec5_get_relations (void);

extern int
ar_mec5_find_or_add_bang_relation (ar_identifier *name);

#endif /* ! MEC5_H */
