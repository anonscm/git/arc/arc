/*
 * ar-mec5-preferences.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_MEC5_PREFERENCES_H
# define AR_MEC5_PREFERENCES_H

enum ar_mec5_pref {
# define AR_MEC5_PREF(_enumid, _strid) MEC5_PREF_ ## _enumid,
# include "mec5-preferences.def"
# undef AR_MEC5_PREF
  LAST_AND_UNUSED_AR_MEC5_PREF
};

extern const char *AR_MEC5_PREF_NAMES[];

# define AR_MEC5_PREF(_enumid, _strid) \
  extern const char *MEC5_PREF_  ## _enumid ## _STR;
# include "mec5-preferences.def"
# undef AR_MEC5_PREF

#endif /* ! AR_MEC5_PREFERENCES_H */
