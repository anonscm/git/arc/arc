/*
 * mec5-serialization.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef MEC5_SERIALIZATION_H
# define MEC5_SERIALIZATION_H

# include <ccl/ccl-config-table.h>
# include "mec5/mec5-relations.h"

extern int
ar_mec5_store_relation (ar_identifier *id, ar_identifier *newname,
			const char *filename, ccl_config_table *config);

extern int
ar_mec5_load_relation (const char *filename, ccl_config_table *config);

#endif /* ! MEC5_SERIALIZATION_H */
