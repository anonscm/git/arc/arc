/*
 * mec5-acheck-rels.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef MEC5_ACHECK_RELS_H
# define MEC5_ACHECK_RELS_H

# include "ar-rel-semantics.h"

extern void
ar_mec5_add_configuration_relation (ar_relsem *rs, ar_identifier *relid,
				    ar_bang_t bang, int nb_rels, ar_dd *rels);

extern void
ar_mec5_add_transition_relation (ar_relsem *rs, ar_identifier *relid,
				 const ar_dd_array *rels);


#endif /* ! MEC5_ACHECK_RELS_H */
