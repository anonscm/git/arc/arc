/*
 * ar-solver-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_SOLVER_P_H
# define AR_SOLVER_P_H

# include "ar-solver.h"

struct ar_solver_st
{
  ar_ca_expr **variables;
  ar_bounds **bounds;
  int nb_variables;

  void (*delete_solver) (ar_solver *s);
  void (*solve) (ar_solver *s);
  
  ar_backtracking_stack *bstack;
  ar_solver_start_solution_se_proc *set_start;
  ar_solver_new_solution_proc *set_add;
  ar_solver_end_solution_set_proc *set_end;
  void *set_data;
};

# define SOLVER(_S) ((ar_solver *) _S)
# define BSTACK(_S) (SOLVER (_S)->bstack)
# define NB_VARIABLES(_S) (SOLVER (_S)->nb_variables)
# define VARIABLES(_S,_i) (SOLVER (_S)->variables[_i])
# define BOUNDS(_S,_i) (SOLVER (_S)->bounds[_i])

# define SET_START(_S)					\
  do {							\
    if (SOLVER (_S)->set_start)			\
      (SOLVER (_S)->set_start (SOLVER (_S), SET_DATA (_S)));	\
  } while (0)

# define SET_ADD(_S) \
  (SOLVER (_S)->set_add (SOLVER (_S), SET_DATA (_S)))

# define SET_END(_S)					\
  do {							\
    if (SOLVER (_S)->set_end)			\
      (SOLVER (_S)->set_end (SOLVER (_S), SET_DATA (_S)));	\
  } while (0)

# define SET_DATA(_S) (SOLVER (_S)->set_data)


extern ar_solver *
ar_allocate_solver (size_t size,
		    void (*delete_solver) (ar_solver *s),
		    void (*solve) (ar_solver *s),
		    ar_ca_expr **variables, int nb_variables,
		    ar_backtracking_stack *bstack,
		    ar_solver_start_solution_se_proc *set_start,
		    ar_solver_new_solution_proc *set_add,
		    ar_solver_end_solution_set_proc *set_end);

#endif /* ! AR_SOLVER_P_H */
