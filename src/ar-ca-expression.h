/*
 * ar-ca-expression.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_EXPRESSION_H
# define AR_CA_EXPRESSION_H

# include <ccl/ccl-array.h>
# include <ccl/ccl-hash.h>
# include <ccl/ccl-set.h>
# include "ar-identifier.h"
# include "ar-ca-domain.h"

typedef struct ar_ca_expression_manager_st ar_ca_exprman;
typedef struct ar_ca_expression_st ar_ca_expr;
typedef CCL_ARRAY(ar_ca_expr *) ar_ca_expr_array;

# define AR_CA_EXPR_KINDS			\
  AR_CA_EXPR_OP (CST)				\
  AR_CA_EXPR_OP (VAR)				\
  AR_CA_EXPR_OP (AND)				\
  AR_CA_EXPR_OP (OR)				\
  AR_CA_EXPR_OP (NOT)				\
  AR_CA_EXPR_OP (NEG)				\
  AR_CA_EXPR_OP (ADD)				\
  AR_CA_EXPR_OP (MUL)				\
  AR_CA_EXPR_OP (DIV)				\
  AR_CA_EXPR_OP (MOD)				\
  AR_CA_EXPR_OP (EQ)				\
  AR_CA_EXPR_OP (LT)				\
  AR_CA_EXPR_OP (ITE)				\
  AR_CA_EXPR_OP (MIN)				\
  AR_CA_EXPR_OP (MAX)

typedef enum ar_ca_expression_kind_enum {
# define AR_CA_EXPR_OP(k) AR_CA_ ## k,
AR_CA_EXPR_KINDS
# undef AR_CA_EXPR_OP
} ar_ca_expression_kind;

			/* --------------- */

extern ar_ca_exprman *
ar_ca_exprman_create(void);

extern ar_ca_exprman *
ar_ca_exprman_add_reference(ar_ca_exprman *eman);

extern void
ar_ca_exprman_del_reference(ar_ca_exprman *eman);

			/* --------------- */

extern ar_ca_expr *
ar_ca_expr_crt_boolean_constant(ar_ca_exprman *man, int value);

extern ar_ca_expr *
ar_ca_expr_crt_integer_constant(ar_ca_exprman *man, int value);


extern ar_ca_expr *
ar_ca_expr_crt_enum_constant(ar_ca_exprman *man, ar_identifier *name);

extern uint32_t
ar_ca_expr_get_number_of_enum_constants (ar_ca_exprman *man);

extern ar_ca_expr *
ar_ca_expr_get_enum_constant_by_name(ar_ca_exprman *man, ar_identifier *name);

extern ar_ca_expr *
ar_ca_expr_get_enum_constant_by_index(ar_ca_exprman *man, int value);

extern ar_ca_expr *
ar_ca_expr_crt_variable(ar_ca_exprman *man, ar_identifier *name,
			ar_ca_domain *domain, ccl_list *attrs);

extern ar_ca_expr *
ar_ca_expr_get_variable_by_name(ar_ca_exprman *man, ar_identifier *name);

			/* --------------- */

extern ar_ca_expr *
ar_ca_expr_crt_and(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_or(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_imply(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_not(ar_ca_expr *op);

extern ar_ca_expr *
ar_ca_expr_crt_neg(ar_ca_expr *op);

extern ar_ca_expr *
ar_ca_expr_crt_min(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_max(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_mod(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_add(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_sub(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_mul(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_div(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_eq(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_eq_for_assign (ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_neq(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_leq(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_geq(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_lt(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_gt(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_min (ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_max (ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_exist(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_forall(ar_ca_expr *lop, ar_ca_expr *rop);

extern ar_ca_expr *
ar_ca_expr_crt_ite(ar_ca_expr *i, ar_ca_expr *t, ar_ca_expr *e);

extern ar_ca_expr *
ar_ca_expr_crt_in_domain(ar_ca_expr *e, const ar_ca_domain *dom);

extern ar_ca_expr *
ar_ca_expr_crt_in_domain_of (ar_ca_expr *e, const ar_ca_expr *var);

			/* --------------- */

extern ar_ca_expr *
ar_ca_expr_add_reference(ar_ca_expr *e);

extern void
ar_ca_expr_del_reference(ar_ca_expr *e);

extern ar_ca_exprman *
ar_ca_expr_get_manager (ar_ca_expr *e);

			/* --------------- */

extern ar_identifier *
ar_ca_expr_variable_get_name(const ar_ca_expr *var);

extern const ar_identifier *
ar_ca_expr_variable_get_const_name(const ar_ca_expr *var);

extern const ar_ca_domain *
ar_ca_expr_variable_get_domain(const ar_ca_expr *var);

extern ccl_list *
ar_ca_expr_variable_get_attributes (const ar_ca_expr *var);

extern ar_ca_domain *
ar_ca_expr_variable_get_not_const_domain(const ar_ca_expr *var);

extern int
ar_ca_expr_variable_is_boolean(const ar_ca_expr *var);

extern int
ar_ca_expr_variable_is_enum(const ar_ca_expr *var);

extern int
ar_ca_expr_variable_is_integer(const ar_ca_expr *var);

extern void
ar_ca_expr_variable_reset_bounds(ar_ca_expr *var);

extern ar_ca_expr *
ar_ca_expr_variable_get_ith_value (const ar_ca_expr *var, int i);

			/* --------------- */

extern int
ar_ca_expr_enum_constant_get_index(const ar_ca_expr *cst);

extern ar_identifier *
ar_ca_expr_enum_constant_get_name(const ar_ca_expr *cst);

			/* --------------- */

extern void
ar_ca_expr_log (ccl_log_type log, const ar_ca_expr *expr);

extern void
ar_ca_expr_log_quoted (ccl_log_type log, const ar_ca_expr *expr);

extern void
ar_ca_expr_log_gen (ccl_log_type log, const ar_ca_expr *expr, 
		    const char *separator, const char *quotes);

			/* --------------- */

extern int
ar_ca_expr_get_min(const ar_ca_expr *expr);

extern int
ar_ca_expr_get_max(const ar_ca_expr *expr);

extern void
ar_ca_expr_evaluate(ar_ca_expr *expr, int rec);

extern int
ar_ca_expr_evaluate_min (ar_ca_expr *expr, int rec);

extern void
ar_ca_expr_set_bounds(ar_ca_expr *e, int min, int max);

extern void
ar_ca_expr_set_bounds_to_ith_domain_value (ar_ca_expr *e, int i);

extern ar_bounds *
ar_ca_expr_get_bounds_address(ar_ca_expr *e);

extern ar_ca_expr **
ar_ca_expr_get_args(ar_ca_expr *e);

extern ar_ca_expression_kind
ar_ca_expr_get_kind(ar_ca_expr *e);

extern ar_ca_exprman *
ar_ca_expr_get_expression_manager(ar_ca_expr *e);

extern void
ar_ca_expr_reset_bounds(ar_ca_expr *e);

extern void
ar_ca_expr_save_bounds(ar_ca_expr *e, ar_backtracking_stack *bs);

extern ar_ca_expr *
ar_ca_expr_replace(ar_ca_expr *expr, ar_ca_expr *subterm, 
		   ar_ca_expr *replacement);

extern ar_ca_expr *
ar_ca_expr_replace_with_table (ar_ca_expr *expr, ccl_hash *subst);

# define ar_ca_expr_is_boolean_constant(e) \
(ar_ca_expr_is_boolean_expr (e) && ar_ca_expr_get_kind (e) == AR_CA_CST)

# define ar_ca_expr_is_true(e) \
  (ar_ca_expr_is_boolean_constant (e) && ar_ca_expr_get_min (e))

# define ar_ca_expr_is_false(e)					\
  (ar_ca_expr_is_boolean_constant (e) && !ar_ca_expr_get_min (e))

extern int
ar_ca_expr_is_boolean_expr(ar_ca_expr *expr);

extern int
ar_ca_expr_is_enum_expr(ar_ca_expr *expr);

extern int
ar_ca_expr_is_integer_expr(ar_ca_expr *expr);

extern ccl_list *
ar_ca_expr_get_variables (ar_ca_expr *expr, ccl_list *result);

extern ccl_hash *
ar_ca_expr_get_variables_in_table (ar_ca_expr *expr, ccl_hash *result);

extern int
ar_ca_expr_has_variable (ar_ca_expr *expr, ar_ca_expr *var);

extern int
ar_ca_expr_has_variable_in_table (ar_ca_expr *expr, ccl_set *vars);

extern int
ar_ca_expr_is_satisfiable (ar_ca_expr *e);

extern int
ar_ca_expr_nb_solutions (ar_ca_expr *e);

extern ar_ca_expr *
ar_ca_expr_negate (ar_ca_expr *e);

extern ccl_list * 
ar_ca_expr_get_dnf_clauses (ar_ca_expr *e);

extern ccl_list *
ar_ca_expr_get_modules (ar_ca_expr *e);

			/* --------------- */

extern void
ar_ca_expr_log_list_of_expressions (ccl_log_type log, ccl_list *list);

extern int
ar_ca_expr_get_size (ar_ca_expr *e);

extern char *
ar_ca_expr_to_string (const ar_ca_expr *e);

#endif /* ! AR_CA_EXPRESSION_H */
