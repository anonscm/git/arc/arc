/*
 * ar-stepper-state.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-bounds.h"
#include "ar-stepper-state.h"

struct ar_stepper_state_st {
  int width;
  ar_bounds values[1];
};

			/* --------------- */

ar_stepper_state *
ar_stepper_state_create (int width)
{
  ar_stepper_state *result;
  size_t sz = (sizeof (ar_stepper_state) +
	       (width > 0 ? (width - 1) : 0) * sizeof (ar_bounds));
  result = (ar_stepper_state *) ccl_calloc (sz, 1);
	  
  result->width = width;

  return result;
}
			/* --------------- */

void
ar_stepper_state_delete(ar_stepper_state *state)
{
  ccl_pre( state != NULL );

  ccl_delete(state);
}

			/* --------------- */

uint32_t
ar_stepper_state_hash(ar_stepper_state *state)
{
  int i;
  uint32_t result = 0;

  ccl_pre( state != NULL );

  for(i = 0; i < state->width; i++)
    result = 9*result+17*state->values[i].min+311*state->values[i].max;

  return result;
}

			/* --------------- */

int
ar_stepper_state_equal (const ar_stepper_state *s1, const ar_stepper_state *s2)
{
  int i, w;

  ccl_pre (s1 != NULL); 
  ccl_pre (s2 != NULL);

  w = s1->width;
  if (w != s2->width)
    return 0;

  for (i = 0; i < w; i++)
    {
      if( ! ar_bounds_are_equal (s1->values + i,s2->values + i) )
	return 0;
    }

  return 1;
}

			/* --------------- */

int
ar_stepper_state_compare (const ar_stepper_state *s1, 
			  const ar_stepper_state *s2)
{
  int i, w;

  ccl_pre (s1 != NULL); 
  ccl_pre (s2 != NULL);
  ccl_pre (s1->width == s2->width);

  w = s1->width;
  
  for (i = 0; i < w; i++)
    {
      if (! ar_bounds_are_equal (s1->values + i, s2->values + i))
	{
	  int r = s1->values[i].min - s2->values[i].min;
	  if (r == 0)
	    r = s1->values[i].max - s2->values[i].max;

	  return r;
	}
    }

  return 0;
}

			/* --------------- */

int
ar_stepper_state_get_width(ar_stepper_state *state)
{
  ccl_pre( state != NULL );
  
  return state->width;
}

			/* --------------- */

void
ar_stepper_state_set_range(ar_stepper_state *state, 
			   int index, int min, int max)
{
  ccl_pre( 0 <= index && index < ar_stepper_state_get_width(state) );

  ar_bounds_set(state->values+index,min,max);
}

			/* --------------- */

int
ar_stepper_state_set_values (ar_stepper_state *state, const varorder *vo, 
			     ar_ca_expr **variables, int nb_variables)
{
  int is_singleton = 1;

  for(; nb_variables--; variables++)
    {
      int index = varorder_get_index (vo, *variables);
      int min = ar_ca_expr_get_min(*variables);
      int max = ar_ca_expr_get_max(*variables);
      ar_stepper_state_set_range(state,index,min,max);
      is_singleton = is_singleton && (min == max);
    }

  return is_singleton;
}

			/* --------------- */

void
ar_stepper_state_set_variables(ar_stepper_state *state, const varorder *vo, 
			       ar_ca_expr **variables, int nb_variables)
{
  for(; nb_variables--; variables++)
    {
      int index = varorder_get_index (vo, *variables);
      int min = state->values[index].min;
      int max = state->values[index].max;
      ar_ca_expr_set_bounds(*variables,min,max);
    }
}

			/* --------------- */

void
ar_stepper_state_copy(ar_stepper_state *dst, ar_stepper_state *src)
{
  int w = ar_stepper_state_get_width(dst);

  ccl_pre( w == ar_stepper_state_get_width(src) );

  ccl_memcpy(dst,src,sizeof(ar_stepper_state)+(w-1)*sizeof(ar_bounds));
}

			/* --------------- */

ar_stepper_state *
ar_stepper_state_dup(ar_stepper_state *state)
{
  ar_stepper_state *result =
    ar_stepper_state_create(ar_stepper_state_get_width(state));

  ar_stepper_state_copy(result,state);

  return result;
}

			/* --------------- */

void
ar_stepper_state_log(ccl_log_type log, ar_stepper_state *state)
{
  int i;

  ccl_pre( state != NULL );

  for(i = 0; i < state->width; i++)
    {
      const char *comma = ( i == state->width-1 )?"":", ";

      if( ar_bounds_is_singleton(state->values+i) )
	ccl_log(log,"%d%s",state->values[i].min,comma);
      else
	ccl_log(log,"%d%s",state->values[i].min,state->values[i].max,comma);
    }
}

			/* --------------- */

void
ar_stepper_state_get_range(ar_stepper_state *state, int i, int *min, int *max)
{
  ar_bounds *b = ar_stepper_state_get_bounds_address(state,i);

  *min = b->min;
  *max = b->max;
}

			/* --------------- */

ar_bounds *
ar_stepper_state_get_bounds_address(ar_stepper_state *state, int i)
{
  ccl_pre( 0 <= i && i < ar_stepper_state_get_width(state) );

  return state->values+i;
}

			/* --------------- */

void
ar_stepper_state_save(ar_stepper_state *state,  ar_backtracking_stack *bstack)
{
  int i;

  ccl_pre( state != NULL ); ccl_pre( bstack != NULL ); 

  for(i = 0; i < state->width; i++)
    ar_bounds_save(&state->values[i],bstack);
}

			/* --------------- */

struct expand_cell {
  struct expand_cell *next;
  int value;
};

struct expand_data {
  ar_stepper_state *s;
  ar_stepper_state_array *result;
  int *presult_card;
  int max;
  int (*indom)(int varindex, int val, void *data);
  void *indom_data;
};

			/* --------------- */

static void
s_expand_state_rec(struct expand_data *data, int i, struct expand_cell *c)
{
  if( i == data->s->width )
    {
      ar_stepper_state *new_state;

      if( data->max >= 0 && *data->presult_card >= data->max )
	return;

      new_state = ar_stepper_state_create(data->s->width);
      while( i-- )
	{
	  ar_bounds_set(new_state->values+i,c->value,c->value);
	  c = c->next;
	}
      
      if( *data->presult_card >= data->result->size )
	ccl_array_ensure_size((*data->result),2*data->result->size);
      data->result->data[(*data->presult_card)++] = new_state;
    }
  else
    {
      struct expand_cell here;
      int max = data->s->values[i].max;

      here.next = c;
      here.value = data->s->values[i].min;

      if (max == here.value)
	{
	  ccl_assert (data->indom (i, here.value, data->indom_data));
	  s_expand_state_rec(data,i+1,&here);
	}
      else
	{
	  do
	    {
	      if (data->indom (i, here.value, data->indom_data))
		{
		  s_expand_state_rec(data,i+1,&here);
		  if( data->max >= 0 && *data->presult_card >= data->max )
		    return;
		}
	      here.value++;
	    }
	  while( here.value <= max );
	}
    }
}

			/* --------------- */

void
ar_stepper_state_expand_state_with_limit(ar_stepper_state *state, 
					 ar_stepper_state_array *set,
					 int *pset_card,
					 int max,
					 int (*in_domain)(int varindex,
							  int val, void *data),
					 void *indom_data)
{
  struct expand_data data;

  data.s = state;
  data.result = set;
  data.presult_card = pset_card;
  data.max = max;
  data.indom = in_domain;
  data.indom_data = indom_data;
  s_expand_state_rec(&data,0,NULL);
}

			/* --------------- */

