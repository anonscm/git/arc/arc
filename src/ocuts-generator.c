/*
 * ocuts-generator.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-buffer.h>
#include <ccl/ccl-graph.h>
#include "ar-rldd.h"
#include "ar-stepper.h"
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-rel-semantics.h"
#include "sequences-generator.h"
#include "sequences-generator-p.h"
#include "observer.h"

#define EVENT_VARIABLE_PATTERN "$%d_th_event"

static ar_ca_expr **
s_add_event_memorization (ar_ca *ca, ccl_set *visibles,  
			  ccl_set *disabled, int bound, 
			  ccl_list ***elementary_events);

static void
s_compute_ordered_cuts (ar_ca *ca, ar_ca_expr *target, ar_ca_expr **vars, 
			ccl_list **elementary_events, int bound, int mincuts, 
			int bf, ccl_log_type log, const char *prefix);

			/* --------------- */

void
ar_orderered_cuts_generator (ar_ca *ca, ar_ca_expr *target,
			     ccl_set *visibles, ccl_set *disabled, 
			     int bound, int mincuts, int bf,
			     ccl_log_type log, const char *prefix)
{ 
  int i;
  ccl_list **elem_events = NULL;
  ar_ca_expr **vars = 
    s_add_event_memorization (ca, visibles, disabled, bound, &elem_events);

  s_compute_ordered_cuts (ca, target, vars, elem_events, bound, mincuts, bf, 
			  log, prefix);

  for (i = 0; i < bound; i++)
    ar_ca_expr_del_reference (vars[i]);
  ccl_delete (vars);

  for (i = 0; elem_events[i] != NULL; i++)
    ccl_list_clear_and_delete (elem_events[i], ccl_string_delete);
  ccl_delete (elem_events);
}

			/* --------------- */

static int
s_is_tagged_in (ar_ca_event *e, ccl_set *tag)
{
  ccl_pair *p;
  ccl_list *attrs = ar_ca_event_get_attributes (e);

  for (p = FIRST (attrs); p; p = CDR (p))
    if (ccl_set_has (tag, CAR (p)))
	return 1;

  return 0;
}

			/* --------------- */

static ccl_list **
s_build_visible_events (ar_ca *ca, ccl_set *visibles, ccl_set *disabled, 
			ccl_hash *visible_events,
			ccl_hash *disabled_events)
{
  int i;
  int nb_events = ar_ca_get_number_of_events (ca);
  ar_ca_event **events = ar_ca_get_events (ca);
  ccl_list **result = ccl_new_array (ccl_list *, nb_events + 1);
  ar_node *node = ar_ca_get_node (ca);
  ar_identifier *nodename = ar_node_get_name (node);
  ar_node_del_reference (node);
  node = ar_model_get_node (nodename);
  ar_identifier_del_reference (nodename);

  for (i = 0; i < nb_events; i++)
    {
      if (s_is_tagged_in (events[i], disabled))
	{
	  ccl_hash_find (disabled_events, events[i]);
	  ccl_hash_insert (disabled_events, events[i]);
	}
      else if (s_is_tagged_in (events[i], visibles))
	{
	  ar_identifier_iterator *ii = ar_ca_event_get_ids (events[i]);
	  intptr_t index = ccl_hash_get_size (visible_events);

	  ccl_hash_find (visible_events, events[i]);
	  ccl_hash_insert (visible_events, (void *) index);
	  result[index] = ccl_list_create ();
	  while (ccl_iterator_has_more_elements (ii))
	    {
	      ar_identifier *eventname = ccl_iterator_next_element (ii);
	      if (ar_elementary_event_has_one_of_attrs (node, eventname, 
							visibles))
		{
		  char *str = ar_identifier_to_string (eventname);
		  if (ccl_list_get_index (result[index], str, 
					  ccl_string_compare) >= 0)
		    ccl_string_delete (str);
		  else
		    ccl_list_add (result[index], str);
		}
	      ar_identifier_del_reference (eventname);
	    }
	  ccl_iterator_delete (ii);
	}
    }
  ar_node_del_reference (node);

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_create_additional_guard (ar_ca_exprman *man, ar_ca_expr **vars, int nb_vars, 
			   ar_ca_expr *maxval)
{
  ar_ca_expr *result;

  if (nb_vars == 0)
    result = ar_ca_expr_crt_boolean_constant (man, 1);
  else
    {
      int i;

      result = ar_ca_expr_crt_eq (vars[0], maxval);
      
      for (i = 1; i < nb_vars; i++)
	{
	  ar_ca_expr *tmp0 = ar_ca_expr_crt_eq (vars[i], maxval);
	  ar_ca_expr *tmp1 = ar_ca_expr_crt_or (result, tmp0);
	  ar_ca_expr_del_reference (tmp0);
	  ar_ca_expr_del_reference (result);
	  result = tmp1;
	}
    }

  return result;
}

			/* --------------- */

static void
s_add_var_assignment (ar_ca_exprman *man, ar_ca_trans *t, ar_ca_expr **vars, 
		      int nb_vars, ar_ca_expr *emax, int value)
{
  int i;
  ar_ca_expr *evalue = ar_ca_expr_crt_integer_constant (man, value);

  for (i = 0; i < nb_vars; i++)
    {
      int j;
      ar_ca_expr *cond = ar_ca_expr_crt_eq (vars[i], emax);
      ar_ca_expr *ite;

      for (j = 0; j < i; j++)
	{
	  ar_ca_expr *tmp0 = ar_ca_expr_crt_neq (vars[j], emax);
	  ar_ca_expr *tmp1 = ar_ca_expr_crt_and (cond, tmp0);
	  ar_ca_expr_del_reference (tmp0);
	  ar_ca_expr_del_reference (cond);
	  cond = tmp1;
	}
      /* cond ::= e_{0} != emax & ... & e_{i-1} != emax & e_i = emax */
      ite = ar_ca_expr_crt_ite (cond, evalue, vars[i]);
      ar_ca_trans_add_assignment (t, vars[i], ite);
      ar_ca_expr_del_reference (ite);
      ar_ca_expr_del_reference (cond);
    }
  ar_ca_expr_del_reference (evalue);
}

			/* --------------- */

static ar_ca_expr **
s_add_event_memorization (ar_ca *ca, ccl_set *visibles, ccl_set *disabled,
			  int bound, ccl_list ***p_elem_events)
{
  int i;
  ar_ca_domain *dom;
  ar_ca_expr **vars;
  ar_ca_expr *guard;
  ar_ca_expr *ff;
  ar_ca_expr *maxval;
  ar_ca_exprman *eman = ar_ca_get_expression_manager (ca);
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  ccl_hash *visible_events = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_hash *disabled_events = ccl_hash_create (NULL, NULL, NULL, NULL);
  int nb_visible_events;
    
  *p_elem_events = s_build_visible_events (ca, visibles, disabled, 
					   visible_events, disabled_events);
  nb_visible_events = ccl_hash_get_size (visible_events);
  maxval = ar_ca_expr_crt_integer_constant (eman, nb_visible_events);
  dom = ar_ca_domain_create_range (0, nb_visible_events);
  vars = ccl_new_array (ar_ca_expr *, bound + 1);

  for (i = 0; i < bound; i++)
    {
      char *enamestr = ccl_string_format_new (EVENT_VARIABLE_PATTERN, i);
      ar_identifier *varname = ar_identifier_create (enamestr);

      vars[i] = ar_ca_expr_crt_variable (eman, varname, dom, NULL);

      ar_ca_add_state_variable (ca, vars[i]);
      ar_ca_add_initial_assignment (ca, vars[i], maxval);
      ccl_string_delete (enamestr);
    }

  guard = s_create_additional_guard (eman, vars, bound, maxval);
  ff = ar_ca_expr_crt_boolean_constant (eman, 0);

  for (i = 0; i < nb_trans; i++)
    {
      ar_ca_event *label = ar_ca_trans_get_event (trans[i]);

      if (ccl_hash_find (disabled_events, label))
	{
	  ar_ca_trans_set_guard (trans[i], ff);
	}
      else
	{
	  ar_ca_expr *g = ar_ca_trans_get_guard (trans[i]);
	  ar_ca_expr *tmp = ar_ca_expr_crt_and (guard, g);
      
	  ar_ca_trans_set_guard (trans[i], tmp);
	  ar_ca_expr_del_reference (tmp);
	  ar_ca_expr_del_reference (g);
      
	  if (ccl_hash_find (visible_events, label))
	    {
	      int value = (intptr_t) ccl_hash_get (visible_events);

	      s_add_var_assignment (eman, trans[i], vars, bound, maxval, value);
	    }
	}
      ar_ca_event_del_reference (label);
    }

  ccl_hash_delete (visible_events);
  ccl_hash_delete (disabled_events);
  ar_ca_expr_del_reference (maxval);
  ar_ca_expr_del_reference (guard);
  ar_ca_expr_del_reference (ff);
  ar_ca_domain_del_reference (dom);
  ar_ca_exprman_del_reference (eman);

  return vars;
}

			/* --------------- */

static ar_dd 
s_built_varlist (ar_relsem *rs, ar_ca_expr **vars)
{
  int new_index;
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_dd varlist = ar_dd_create_projection_list (ddm);  

  for (new_index = 0; *vars; vars++, new_index++)
    {
      ar_identifier *id = ar_ca_expr_variable_get_name (*vars);
      int ddindex = ar_relsem_get_var_dd_index (rs, id);

      ar_dd_projection_list_add (ddm, &varlist, ddindex, new_index);
      ar_identifier_del_reference (id);
    }

  return varlist;
}

/*!
 * Compute the intersection of reachable configurations of the decorated 
 * constraint automaton with bad configurations described by 'target'.
 * 
 * Parameters:
 *   ca     : the studied constraint automaton
 *   rs     : the DD encoded semantics of 'ca'
 *   target : the Booolean formula describing expected states.
 * Result:
 *   the DD that encodes the intersection
 */
static ar_dd 
s_compute_bad (ar_ca *ca, ar_relsem *rs, ar_ca_expr *target)
{
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_dd tmp = ar_relsem_compute_simple_set (rs, target);
  ar_dd reachables = ar_relsem_get_reachables (rs);
  ar_dd bad = ar_dd_compute_and (ddm, reachables, tmp);

  if (ccl_debug_is_on)
    {
      ccl_debug ("#bad = %f/%f\n", 
		 ar_relsem_get_cardinality (rs, bad, 0),
		 ar_relsem_get_cardinality (rs, reachables, 0));
    }
  AR_DD_FREE (tmp);
  AR_DD_FREE (reachables);

  return bad;
}

			/* --------------- */

static ar_rldd *
s_dd_to_rldd (ar_ddm ddm, ar_dd dd, ar_rlddm *rlddm, ccl_hash *cache)
{
  ar_rldd *R;

  if (ar_dd_is_zero (ddm, dd))
    R = ar_rldd_empty (rlddm);
  else if (ar_dd_is_one (ddm, dd))
    R = ar_rldd_epsilon (rlddm);
  else if (ccl_hash_find (cache, dd))
    R = ar_rldd_dup (rlddm, ccl_hash_get (cache));
  else 
    {
      intptr_t i;
      intptr_t arity = AR_DD_ARITY (dd);
      ar_ddcode encoding = AR_DD_CODE (dd);

      R = ar_rldd_empty (rlddm);

      for (i = 0; i < arity; i++)
	{
	  ar_rldd *tmp;
	  ar_rldd *sonL;

	  if (encoding == EXHAUSTIVE)
	    {
	      tmp = s_dd_to_rldd (ddm, AR_DD_EPNTHSON (dd, i), rlddm, cache);
	      if (i == arity - 1)
		sonL = tmp;
	      else
		{
		  sonL = ar_rldd_concat_letter (rlddm, (void *) i, tmp);
		  ar_rldd_delete (rlddm, tmp); 
		}
	    }
	  else
	    {
	      int j;
	      int min = AR_DD_MIN_OFFSET_NTHSON (dd, i);
	      int max = AR_DD_MAX_OFFSET_NTHSON (dd, i);

	      sonL = ar_rldd_empty (rlddm);
	      tmp = s_dd_to_rldd (ddm, AR_DD_CPNTHSON (dd, i), rlddm, cache);

	      if (tmp != sonL)
		{
		  for (j = min; j <= max; j++)
		    {
		      ar_rldd *tmp2;
		      ar_rldd *tmp3;
		      
		      if (i == arity - 1 && j == max)
			tmp2 = ar_rldd_epsilon (rlddm);
		      else
			tmp2 = 
			  ar_rldd_concat_letter (rlddm, (void *) (intptr_t) j, 
						 tmp);
		      tmp3 = ar_rldd_union (rlddm, sonL, tmp2);
		      
		      ar_rldd_delete (rlddm, tmp2);
		      ar_rldd_delete (rlddm, sonL);
		      sonL = tmp3;
		    }
		}
	      ar_rldd_delete (rlddm, tmp);
	    }	 

	  tmp = ar_rldd_union (rlddm, R, sonL);
	  ar_rldd_delete (rlddm, R);
	  ar_rldd_delete (rlddm, sonL);
	  R = tmp;
	}
      if (! ccl_hash_find (cache, dd))
	ccl_hash_insert (cache, R);
    }

  return R;
}

			/* --------------- */

static void 
s_rldd_log_label (ccl_log_type log, const void *label, void *data)
{
  ccl_pair *p;
  intptr_t l = (intptr_t) label;
  ccl_list **elementary_events = data;

  ccl_assert (elementary_events[l] != NULL);
  if (ccl_list_get_size (elementary_events[l]) > 1)
    ccl_log (log, "<");
  for (p = FIRST (elementary_events[l]); p; p = CDR (p))
    {
      ccl_log (log, "%s", CAR (p));
      if (CDR (p) != NULL)
	ccl_log (log, ", ");
    }
  if (ccl_list_get_size (elementary_events[l]) > 1)
    ccl_log (log, ">");
}

			/* --------------- */

static void
s_compute_ordered_cuts (ar_ca *ca, ar_ca_expr *target, ar_ca_expr **vars, 
			ccl_list **elementary_events, int bound, int mincuts, 
			int bf, ccl_log_type log, const char *prefix)
{  
  ar_relsem *rs = ar_compute_relational_semantics (ca, NULL, NULL);
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_dd tmp = s_compute_bad (ca, rs, target);
  ar_dd varlist = s_built_varlist (rs, vars);
  ar_dd badseqs = ar_dd_project_on_variables (ddm, tmp, varlist);
  ar_rlddm *rlddm = 
    ar_rldd_create_manager (10001, 3, 100001, 100, NULL, NULL, NULL);
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
  ar_rldd *cuts = s_dd_to_rldd (ddm, badseqs, rlddm, cache);

  AR_DD_FREE (tmp);
  AR_DD_FREE (varlist);

  if (mincuts)
    {
      ar_rldd *tmp = ar_rldd_min (rlddm, cuts);
      ar_rldd_delete (rlddm, cuts);
      cuts = tmp;
    }

  if (bf)    
    ar_rldd_to_aralia (rlddm, cuts, log, s_rldd_log_label, elementary_events,
		       prefix);
  else
    ar_rldd_display_node_sequences (rlddm, cuts, log, s_rldd_log_label, 
				    elementary_events);
  ar_rldd_delete (rlddm, cuts);
  ar_rldd_destroy_manager (rlddm);

  if (ccl_debug_is_on)
    ccl_debug ("[%p] number of nodes=%d\n", badseqs,
	       ar_dd_get_number_of_nodes (badseqs));
  AR_DD_FREE (badseqs);
  ar_relsem_del_reference (rs);    
  ccl_hash_delete (cache);
}

