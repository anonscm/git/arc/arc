/*
 * arc-shell.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_SHELL_H
# define ARC_SHELL_H

# include <stdio.h>
# include "parser/altarica-tree.h"
# include "parser/altarica-input.h"
# include <ccl/ccl-config-table.h>

typedef struct arc_shell_context_st *arc_shell_context;
struct arc_shell_context_st {
  arc_shell_context next;
  const char *filename;
  int line;
  int is_interactive;
  int cr_mode;

  void *state;
  ccl_parse_tree *nodes;
  FILE *stream;
  int terminated;
  int prompt_level;
  ccl_parse_tree_value current_tok_value;
};

extern arc_shell_context ARC_SHELL_CURRENT_CONTEXT;

extern void
arc_shell_init (void);

extern void
arc_shell_terminate(void);

extern void
arc_shell_interactive_loop (int cr_mode);

extern altarica_tree *
arc_shell_load_preprocessed_file (const char *filename, int modes);

extern int
arc_shell_load_file(const char *filename, int modes);

extern int
arc_shell_load_script_file(const char *filename);

extern int
arc_shell_interp_script_string(const char *script);

extern void
arc_shell_pop_prompt_level(void);

extern void 
arc_shell_exit(void);

extern int
arc_shell_parser_parse(void);

extern void 
arc_shell_error(const char *fmt, ...);

extern void
arc_shell_interp(ccl_parse_tree *t);

extern char *
arc_shell_find_file_in_path (const char *filename);

#endif /* ! ARC_SHELL_H */
