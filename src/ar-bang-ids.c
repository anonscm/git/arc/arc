/*
 * ar-bang-ids.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-node.h"
#include "ar-bang-ids.h"

#define AR_BANG_DEF(_id, _label, _hlp, _desc) _label, 
const char * const ar_bang_labels[] = {
#include "ar-bang-ids.def"
  NULL
};
#undef AR_BANG_DEF

			/* --------------- */

ar_identifier *
ar_bang_id_for_node (ar_node *node, ar_bang_t bang)
{
  ar_identifier *name = ar_node_get_name (node);
  ar_identifier *result = ar_bang_id_for_id (name, bang);
  ar_identifier_del_reference (name);

  return result;
}

			/* --------------- */

ar_identifier *
ar_bang_id_for_id (ar_identifier *id, ar_bang_t bang)
{
  char *s_id = ar_identifier_to_string (id);
  char *tmp = ccl_string_format_new ("%s!%s", s_id, ar_bang_labels[bang]);
  ar_identifier *result = ar_identifier_create (tmp);
  ccl_string_delete (tmp);
  ccl_string_delete (s_id);

  return result;
}

			/* --------------- */

int 
ar_bang_str_to_bang_t (const char *str, ar_bang_t *pbang)
{
  int b;

  for (b = 0; b < AR_BANG_LAST_AND_UNUSED_ID; b++)
    {
      if (strcmp (str, ar_bang_labels[b]) == 0)
	{
	  if (pbang != NULL)
	    *pbang = b;
	  return 1;
	}
    }

  return 0;
}
