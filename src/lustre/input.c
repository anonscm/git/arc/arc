/*
 * input.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "input-p.h"
#include "input.h"

ar_lustre_tree *
ar_lustre_read_file(const char *filename)
{
  ar_lustre_tree *result = NULL;
  FILE *input_stream = fopen(filename,"r");

  if( input_stream != NULL )
    {
      result = ar_lustre_read_stream(input_stream,filename);
      fclose(input_stream);
    }

  return result;
}

			/* --------------- */

ar_lustre_tree *
ar_lustre_read_stream(FILE *stream, const char *input_name)
{
  ar_lustre_tree *result;

  ar_lustre_number_of_errors = 0;
  result = ar_lustre_syntax_tree = ar_lustre_syntax_tree_container = NULL;
  ar_lustre_init_lexer(stream,input_name);
  if( ar_lustre_parse() == 0 )
    result = ar_lustre_syntax_tree;
  else
    ccl_parse_tree_delete_container(ar_lustre_syntax_tree_container);
  
  ar_lustre_terminate_lexer();

  return result;
}

			/* --------------- */

extern char *ar_lustre_text;

void
ar_lustre_error(const char *message)
{
  ar_lustre_number_of_errors++; 

  ccl_error("%s:%d: %s (tok='%s')\n",
	    ar_lustre_current_filename,
	    ar_lustre_current_line,
	    message,
	    ar_lustre_text);
}

			/* --------------- */

int
ar_lustre_wrap(void)
{
  return 1;
}

			/* --------------- */

