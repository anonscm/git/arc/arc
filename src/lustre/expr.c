/*
 * expr.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "expr.h"

typedef union {
  int  ival;
  float rval;
} expr_value;

struct ar_lustre_expr_st {
  int refcount;
  ar_lustre_domain *domain;
  ar_lustre_expr_type type;
  expr_value value;
  ccl_list *operands;
  ar_identifier *name;
  ar_lustre_proto *proto;
};


			/* --------------- */

static ar_lustre_expr *
s_crt_constant(expr_value value, ar_lustre_expr_type type);

static ar_lustre_expr *
s_crt_unary(ar_lustre_expr_type type, ar_lustre_expr *op);

static ar_lustre_expr *
s_crt_binary(ar_lustre_expr_type type,
	     ar_lustre_expr *op1, ar_lustre_expr *op2);

static ar_lustre_expr *
s_crt_boolean_binary(ar_lustre_expr_type type,
		     ar_lustre_expr *op1, ar_lustre_expr *op2);

			/* --------------- */

# define s_alloc_expr() ccl_new(ar_lustre_expr)

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_true(void)
{
  expr_value i;
  i.ival = 1;
  return s_crt_constant(i,AL_EXPR_BCST);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_false(void)
{
  expr_value i;
  i.ival = 0;
  return s_crt_constant(i,AL_EXPR_BCST);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_int(int i, ar_identifier *name)
{
  ar_lustre_expr *result;
  expr_value v;
  v.ival = i;
  result = s_crt_constant(v,AL_EXPR_ICST);
  if( name != NULL )
    result->name = ar_identifier_add_reference(name);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_real (float f, ar_identifier *name)
{
  ar_lustre_expr *result;
  expr_value i;

  i.rval = f;
  result = s_crt_constant (i, AL_EXPR_RCST);
  if (name != NULL)
    result->name = ar_identifier_add_reference (name);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_abstract_cst(ar_identifier *name, ar_lustre_domain *dom)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_domain_add_reference(dom);
  result->type = AL_EXPR_ACST;
  result->operands = NULL;
  result->value.ival = 0;
  result->name = ar_identifier_add_reference(name);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_variable(ar_identifier *name, ar_lustre_domain *dom)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_domain_add_reference(dom);
  result->type = AL_EXPR_VAR;
  result->operands = NULL;
  result->value.ival = 0;
  result->name = ar_identifier_add_reference(name);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_pre(ar_lustre_expr *p)
{
  return s_crt_unary(AL_EXPR_PRE,p);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_followed(ar_lustre_expr *f, ar_lustre_expr *next)
{
  return s_crt_binary(AL_EXPR_FOLLOWED,f,next);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_ite(ar_lustre_expr *i,
		       ar_lustre_expr *t,
		       ar_lustre_expr *e)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_expr_get_domain(t);
  result->type = AL_EXPR_ITE;
  result->operands = ccl_list_create();
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(i));
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(t));
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(e));
  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_not(ar_lustre_expr *a)
{
  return s_crt_unary(AL_EXPR_NOT,a);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_or(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_OR,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_and(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_AND,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_xor(ar_lustre_expr *a, ar_lustre_expr *b)
{
  ar_lustre_expr *e1 = ar_lustre_expr_crt_not(b);
  ar_lustre_expr *e2 = ar_lustre_expr_crt_and(a,e1);
  ar_lustre_expr *e3 = ar_lustre_expr_crt_not(a);
  ar_lustre_expr *e4 = ar_lustre_expr_crt_and(e3,b);
  ar_lustre_expr *result = ar_lustre_expr_crt_or(e2,e4);
  ar_lustre_expr_del_reference(e1);
  ar_lustre_expr_del_reference(e2);
  ar_lustre_expr_del_reference(e3);
  ar_lustre_expr_del_reference(e4);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_imply(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_IMPLY,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_neg(ar_lustre_expr *a)
{
  return s_crt_unary(AL_EXPR_NEG,a);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_add(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_ADD,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_sub(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_SUB,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_mul(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_MUL,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_div(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_DIV,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_min (ar_lustre_expr *a, ar_lustre_expr *b)
{
  ar_lustre_expr *cond = ar_lustre_expr_crt_lt (a, b);
  ar_lustre_expr *result = ar_lustre_expr_crt_ite (cond, a, b);
  ar_lustre_expr_del_reference (cond);
  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_max (ar_lustre_expr *a, ar_lustre_expr *b)
{
  ar_lustre_expr *cond = ar_lustre_expr_crt_gt (a, b);
  ar_lustre_expr *result = ar_lustre_expr_crt_ite (cond, a, b);
  ar_lustre_expr_del_reference (cond);
  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_eq(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_boolean_binary(AL_EXPR_EQ,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_neq(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_boolean_binary(AL_EXPR_NEQ,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_leq(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_boolean_binary(AL_EXPR_LEQ,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_lt(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_boolean_binary(AL_EXPR_LT,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_geq(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_boolean_binary(AL_EXPR_GEQ,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_gt(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_boolean_binary(AL_EXPR_GT,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_current(ar_lustre_expr *op)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_expr_get_domain(op);
  result->type = AL_EXPR_CURRENT;
  result->operands = ccl_list_create();
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op));

  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_when(ar_lustre_expr *op1, ar_lustre_expr *op2)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_expr_get_domain(op1);
  result->type = AL_EXPR_WHEN;
  result->operands = ccl_list_create();
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op1));
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op2));

  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_mod(ar_lustre_expr *a, ar_lustre_expr *b)
{
  return s_crt_binary(AL_EXPR_MOD,a,b);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_atmost(ccl_list *a)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_domain_crt_bool();
  result->type = AL_EXPR_ATMOST;
  result->operands = ccl_list_deep_dup(a,(ccl_duplicate_func *)
				       ar_lustre_expr_add_reference);
  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_expr_list(ccl_list *a)
{
  ar_lustre_expr *result;
  ccl_list *domain;
  ccl_pair *p;

  if( ccl_list_get_size(a) == 1 )
    return ar_lustre_expr_add_reference((ar_lustre_expr *)CAR(FIRST(a)));

  result = s_alloc_expr();
  domain = ccl_list_create();

  for(p = FIRST(a); p; p = CDR(p))
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      ar_lustre_domain *d = ar_lustre_expr_get_domain(e);
      ccl_list_add(domain,ar_lustre_domain_add_reference(d));
      ar_lustre_domain_del_reference(d);
    }
  
  result->refcount = 1;
  result->domain   = ar_lustre_domain_crt_composite(domain);
  ccl_list_clear_and_delete(domain,(ccl_delete_proc *)
			    ar_lustre_domain_del_reference);

  result->type = AL_EXPR_EXPR_LIST;
  result->operands = ccl_list_deep_dup(a,(ccl_duplicate_func *)
				       ar_lustre_expr_add_reference);
  result->value.ival = 0;
  result->name = NULL;

  return result;
}


			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_node_call(ar_identifier *node, ar_lustre_domain *outdomain,
			     ccl_list *args)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_domain_add_reference(outdomain);
  result->type = AL_EXPR_NODE_CALL;
  result->operands = ccl_list_deep_dup(args,(ccl_duplicate_func *)
				       ar_lustre_expr_add_reference);
  result->value.ival = 0;
  result->name = ar_identifier_add_reference(node);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_crt_func_call(ar_lustre_proto *proto, ccl_list *args)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_proto_get_output_domain(proto);
  result->type = AL_EXPR_FUNC_CALL;
  result->operands = ccl_list_deep_dup(args,(ccl_duplicate_func *)
				       ar_lustre_expr_add_reference);
  result->value.ival = 0;
  result->name = ar_lustre_proto_get_name(proto);
  result->proto = ar_lustre_proto_add_reference(proto);

  return result;
}

			/* --------------- */


int
ar_lustre_expr_get_ival(ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );
  return expr->value.ival;
}

			/* --------------- */

double
ar_lustre_expr_get_rval(ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );
  return expr->value.rval;
}

			/* --------------- */

ar_identifier *
ar_lustre_expr_get_name(const ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );
  ccl_pre( expr->type == AL_EXPR_VAR || expr->type == AL_EXPR_NODE_CALL ||
	   expr->type == AL_EXPR_FUNC_CALL || expr->type == AL_EXPR_ACST ||
	   expr->type == AL_EXPR_ICST || expr->type == AL_EXPR_RCST );

  return ar_identifier_add_reference(expr->name);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_expr_get_domain(const ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );

  return ar_lustre_domain_add_reference(expr->domain);
}

			/* --------------- */

int
ar_lustre_expr_is_real(const ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );

  return ar_lustre_domain_is_real(expr->domain);
}

			/* --------------- */

ar_lustre_expr_type
ar_lustre_expr_get_type(ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );

  return expr->type;
}
			 
			/* --------------- */

# define OP1(e) ((ar_lustre_expr *)(CAR(FIRST((e)->operands))))
# define OP2(e) ((ar_lustre_expr *)(CADR(FIRST((e)->operands))))
# define OP3(e) ((ar_lustre_expr *)(CADDR(FIRST((e)->operands))))

ccl_list *
ar_lustre_expr_get_operands(ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );

  if( expr->operands == NULL )
    return NULL;

  return ccl_list_deep_dup(expr->operands,(ccl_duplicate_func *)
			   ar_lustre_expr_add_reference);
}

			/* --------------- */

ar_lustre_proto *
ar_lustre_expr_get_proto(const ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL ); ccl_pre( expr->type == AL_EXPR_FUNC_CALL );

  return ar_lustre_proto_add_reference(expr->proto);
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_add_reference(ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );

  expr->refcount++;

  return expr;
}

			/* --------------- */

void
ar_lustre_expr_del_reference(ar_lustre_expr *expr)
{
  ccl_pre( expr != NULL );

  if( --expr->refcount == 0 )
    {
      ar_lustre_domain_del_reference(expr->domain);
      if( expr->operands != NULL )
	ccl_list_clear_and_delete(expr->operands,(ccl_delete_proc *)
				 ar_lustre_expr_del_reference);
      ccl_zdelete(ar_identifier_del_reference,expr->name);
      ccl_zdelete(ar_lustre_proto_del_reference,expr->proto);
      ccl_delete(expr);
    }
}

			/* --------------- */

void
ar_lustre_expr_log(ccl_log_type log, ar_lustre_expr *expr)
{
  ar_lustre_expr_log_gen(log,expr,ar_identifier_log);
}

			/* --------------- */

static struct { int nary; int priority; char *id; } EXPR_DESC[] = {
  { 0, 10, NULL },      /* AL_EXPR_VAR */
  { 0, 10, NULL },      /* AL_EXPR_BCST */
  { 0, 10, NULL },      /* AL_EXPR_ICST */
  { 0, 10, NULL },      /* AL_EXPR_RCST */
  { 0, 10, NULL },      /* AL_EXPR_ACST */
  { 0, 9, "pre" },     /* AL_EXPR_PRE */ 
  { 0, 0, "->" },      /* AL_EXPR_FOLLOWED */
  { 0, 1, NULL },      /* AL_EXPR_ITE */
  { 0, 4, "not" },     /* AL_EXPR_NOT */
  { 1, 2, "or" },      /* AL_EXPR_OR */ 
  { 1, 3, "and" },     /* AL_EXPR_AND */
  { 0, 3, "=>" },      /* AL_EXPR_IMPLY */
  { 0, 8, "-" },       /* AL_EXPR_NEG */
  { 1, 6, "+" },       /* AL_EXPR_ADD */
  { 1, 6, "-" },       /* AL_EXPR_SUB */
  { 1, 7, "*" },       /* AL_EXPR_MUL */ 
  { 1, 7, "/" },       /* AL_EXPR_DIV */
  { 0, 5, "=" },       /* AL_EXPR_EQ */ 
  { 0, 5, "<>" },      /* AL_EXPR_NEQ */ 
  { 0, 5, "<" },       /* AL_EXPR_LT */ 
  { 0, 5, "<=" },      /* AL_EXPR_LEQ */ 
  { 0, 5, ">" },       /* AL_EXPR_GT */ 
  { 0, 5, ">=" },      /* AL_EXPR_GEQ */
  { 0, 0, "#" },       /* AL_EXPR_ATMOST */ 
  { 0, 10, NULL },      /* AL_EXPR_EXPR_LIST */ 
  { 0, 10, NULL },      /* AL_EXPR_NODE_CALL */ 
  { 0, 10, NULL },      /* AL_EXPR_FUNC_CALL */
  { 0, 9, "current" }, /* AL_EXPR_CURRENT */ 
  { 0, 7, "mod" },     /* AL_EXPR_MOD */ 
  { 0, 0, "when" }     /* AL_EXPR_WHEN */
};

# define NEED_PAR(e,op) \
  (((op)->type != (e)->type || ! EXPR_DESC[(e)->type].nary) &&		\
   EXPR_DESC[(op)->type].priority <= EXPR_DESC[(e)->type].priority)
   

static void
s_expr_log_gen(ccl_log_type log, ar_lustre_expr *expr,
		       ar_identifier_log_proc idlog, int with_par)
{
  const char *op = NULL;

  if( with_par )
    ccl_log(log,"(");

  switch( expr->type ) {
  case AL_EXPR_BCST:  
    ccl_log(log,expr->value.ival?"true":"false");
    break;
  case AL_EXPR_ICST:
    if( expr->name != NULL ) 
      idlog(log,expr->name);
    else 
      ccl_log(log,"%d",expr->value.ival);
    break;
  case AL_EXPR_ACST:
    idlog(log,expr->name);
    break;
  case AL_EXPR_RCST:
    ccl_log(log,"%g",expr->value.rval);
    break;

  case AL_EXPR_PRE : case AL_EXPR_NOT : case AL_EXPR_NEG : 
  case AL_EXPR_CURRENT :
    op = EXPR_DESC[expr->type].id;
    ccl_log(log,"%s ",op);
    s_expr_log_gen(log,OP1(expr),idlog,NEED_PAR(expr,OP1(expr)));
    break;

  case AL_EXPR_ITE:
    ccl_log(log,"if ");
    s_expr_log_gen(log,OP1(expr),idlog,NEED_PAR(expr,OP1(expr)));
    ccl_log(log," then ");
    s_expr_log_gen(log,OP2(expr),idlog,NEED_PAR(expr,OP2(expr)));
    ccl_log(log," else ");
    s_expr_log_gen(log,OP3(expr),idlog,NEED_PAR(expr,OP3(expr)));
    break;

  case AL_EXPR_FOLLOWED: case AL_EXPR_OR: case AL_EXPR_AND: case AL_EXPR_IMPLY:
  case AL_EXPR_ADD:  case AL_EXPR_SUB: case AL_EXPR_MUL: case AL_EXPR_DIV:
  case AL_EXPR_EQ: case AL_EXPR_NEQ: case AL_EXPR_LT: case AL_EXPR_LEQ: 
  case AL_EXPR_GT: case AL_EXPR_GEQ: case AL_EXPR_MOD:  case AL_EXPR_WHEN: 
    op = EXPR_DESC[expr->type].id;
    s_expr_log_gen(log,OP1(expr),idlog,NEED_PAR(expr,OP1(expr)));
    ccl_log(log," %s ",op);
    s_expr_log_gen(log,OP2(expr),idlog,NEED_PAR(expr,OP2(expr)));
    break;

  case AL_EXPR_ATMOST: case AL_EXPR_NODE_CALL: case AL_EXPR_FUNC_CALL:
    if( expr->type == AL_EXPR_ATMOST ) 
      ccl_log(log,EXPR_DESC[expr->type].id);
    else 
      idlog(log,expr->name);

  case AL_EXPR_EXPR_LIST: 
    ccl_log(log,"(");
    {
      ccl_pair *p;
      for(p = FIRST(expr->operands); p; p = CDR(p))
	{
	  s_expr_log_gen(log,CAR(p),idlog,0);
	  if( CDR(p) != NULL )
	    ccl_log(log,",");
	}
    }
    ccl_log(log,")");
    break;

  default: 
    ccl_assert( expr->type == AL_EXPR_VAR );
    idlog(log,expr->name);
    break;
  }

  if( with_par )
    ccl_log(log,")");
}

			/* --------------- */
void
ar_lustre_expr_log_gen(ccl_log_type log, ar_lustre_expr *expr,
		       ar_identifier_log_proc idlog)
{
  s_expr_log_gen(log,expr,idlog,0);
}

			/* --------------- */

static int
s_expr_comp(const ar_lustre_expr *expr1, const ar_lustre_expr *expr2)
{
  if( ar_lustre_expr_equals(expr1,expr2) ) 
    return 0;
  return 1;
}

			/* --------------- */

int
ar_lustre_expr_equals(const ar_lustre_expr *expr1, const ar_lustre_expr *expr2)
{
  int result = 0;  

  if( expr1 == expr2 ) return 1;

  if( expr1->type != expr2->type ) return 0;

  switch( expr1->type ) {
  case AL_EXPR_BCST: 
  case AL_EXPR_ICST:
    result = expr1->value.ival == expr2->value.ival;
    break;
  case AL_EXPR_ACST:
    result = expr1->name == expr2->name;
    break;

  case AL_EXPR_RCST:
    result = expr1->value.rval == expr2->value.rval;
    break;

  case AL_EXPR_FOLLOWED: case AL_EXPR_OR: case AL_EXPR_AND: case AL_EXPR_IMPLY:
  case AL_EXPR_ADD:  case AL_EXPR_SUB: case AL_EXPR_MUL: case AL_EXPR_DIV:
  case AL_EXPR_EQ: case AL_EXPR_NEQ: case AL_EXPR_LT: case AL_EXPR_LEQ: 
  case AL_EXPR_GT: case AL_EXPR_GEQ: case AL_EXPR_ATMOST: 
  case AL_EXPR_EXPR_LIST: case AL_EXPR_ITE:
  case AL_EXPR_PRE : case AL_EXPR_NOT : case AL_EXPR_NEG :
    result = ccl_list_compare(expr1->operands,expr2->operands,
			      (ccl_compare_func *)s_expr_comp) == 0;
    break;

  case AL_EXPR_NODE_CALL:
    result = expr1->name == expr2->name
      && ar_lustre_domain_equals(expr1->domain,expr2->domain)
      && ccl_list_compare(expr1->operands,expr2->operands,
			  (ccl_compare_func *)s_expr_comp) == 0;
    break;

  case AL_EXPR_FUNC_CALL:
    result = expr1->proto == expr2->proto
      && ccl_list_compare(expr1->operands,expr2->operands,
			  (ccl_compare_func *)s_expr_comp) == 0;
    break;

  default : 
    ccl_assert( expr1->type == AL_EXPR_VAR );
    result = expr1->name == expr2->name
      && ar_lustre_domain_equals(expr1->domain,expr2->domain);
    break;
  };

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_expr_get_ith_value(ar_lustre_expr *expr, int i)
{
  ar_lustre_expr *result = NULL;
  ar_lustre_expr *op[3] = { NULL, NULL, NULL };
  ccl_list *tmp = NULL;
  ccl_pair *p;

  if( i < 0 )
    return NULL;

  switch( expr->type ) {
  case AL_EXPR_BCST: case AL_EXPR_ICST: case AL_EXPR_RCST: case AL_EXPR_VAR:
  case AL_EXPR_ACST:
    result = ar_lustre_expr_add_reference(expr);
    break;

  case AL_EXPR_PRE : 
    i = i-1;
  case AL_EXPR_NOT : case AL_EXPR_NEG :
    op[0] = ar_lustre_expr_get_ith_value(OP1(expr),i);
    if( op[0] == NULL )
      break;

    if( ar_lustre_expr_equals(op[0],OP1(expr)) )
      result = ar_lustre_expr_add_reference(expr);
    else 
      result = s_crt_unary(expr->type,op[0]);
    break;

  case AL_EXPR_ITE:
    if( (op[0] = ar_lustre_expr_get_ith_value(OP1(expr),i)) == NULL ||
	(op[1] = ar_lustre_expr_get_ith_value(OP2(expr),i)) == NULL ||
	(op[2] = ar_lustre_expr_get_ith_value(OP3(expr),i)) == NULL )
      break;
    if( ar_lustre_expr_equals(op[0],OP1(expr)) &&
	ar_lustre_expr_equals(op[1],OP2(expr)) &&
	ar_lustre_expr_equals(op[2],OP3(expr)) )
      result = ar_lustre_expr_add_reference(expr);
    else
      result = ar_lustre_expr_crt_ite(op[0],op[1],op[2]);
    break;

  case AL_EXPR_FOLLOWED :
    if( i == 0 ) result = ar_lustre_expr_add_reference(OP1(expr));
    else result = ar_lustre_expr_add_reference(OP2(expr));
    break;

  case AL_EXPR_OR: case AL_EXPR_AND: case AL_EXPR_IMPLY:
  case AL_EXPR_ADD: case AL_EXPR_SUB: case AL_EXPR_MUL: case AL_EXPR_DIV:
  case AL_EXPR_MOD:
    if( (op[0] = ar_lustre_expr_get_ith_value(OP1(expr),i)) == NULL ||
	(op[1] = ar_lustre_expr_get_ith_value(OP2(expr),i)) == NULL )
      break;
    if( ar_lustre_expr_equals(op[0],OP1(expr)) &&
	ar_lustre_expr_equals(op[1],OP2(expr)) )
      result = ar_lustre_expr_add_reference(expr);
    else
      result = s_crt_binary(expr->type,op[0],op[1]);
    break;

  case AL_EXPR_EQ: case AL_EXPR_NEQ: case AL_EXPR_LT: case AL_EXPR_LEQ: 
  case AL_EXPR_GT: case AL_EXPR_GEQ:
    if( (op[0] = ar_lustre_expr_get_ith_value(OP1(expr),i)) == NULL ||
	(op[1] = ar_lustre_expr_get_ith_value(OP2(expr),i)) == NULL )
      break;
    if( ar_lustre_expr_equals(op[0],OP1(expr)) &&
	ar_lustre_expr_equals(op[1],OP2(expr)) )
      result = ar_lustre_expr_add_reference(expr);
    else
      result = s_crt_boolean_binary(expr->type,op[0],op[1]);
    break;

  case AL_EXPR_FUNC_CALL:
  case AL_EXPR_ATMOST: 
  case AL_EXPR_EXPR_LIST: 
    tmp = ccl_list_create();

    for(p = FIRST(expr->operands); p; p = CDR(p))
      {
	ar_lustre_expr *e = 
	  ar_lustre_expr_get_ith_value((ar_lustre_expr *)CAR(p),i);

	if( e == NULL )
	  {
	    ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
				      ar_lustre_expr_del_reference);
	    tmp = NULL;
	    break;
	  }
	ccl_list_add(tmp,e);
      }

    if( tmp == NULL )
      break;

    if( ccl_list_compare(tmp,expr->operands,
			 (ccl_compare_func *)s_expr_comp) == 0 )
      result = ar_lustre_expr_add_reference(expr);
    else if( expr->type == AL_EXPR_ATMOST )
      result = ar_lustre_expr_crt_atmost(tmp);
    else if( expr->type == AL_EXPR_EXPR_LIST )
      result = ar_lustre_expr_crt_expr_list(tmp);
    else
      result = ar_lustre_expr_crt_func_call(expr->proto,tmp);
    ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
    break;

  case AL_EXPR_CURRENT: ccl_throw_no_msg (internal_error);
  case AL_EXPR_WHEN: ccl_throw_no_msg (internal_error); 
  case AL_EXPR_NODE_CALL: ccl_throw_no_msg (internal_error); 
  default : ccl_throw_no_msg (internal_error); 
  }

  ccl_zdelete(ar_lustre_expr_del_reference,op[0]);
  ccl_zdelete(ar_lustre_expr_del_reference,op[1]);
  ccl_zdelete(ar_lustre_expr_del_reference,op[2]);

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_crt_constant(expr_value value, ar_lustre_expr_type type)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;

  if( type == AL_EXPR_BCST ) 
    result->domain = ar_lustre_domain_crt_bool();
  else if( type == AL_EXPR_ICST ) 
    result->domain = ar_lustre_domain_crt_int();
  else 
    result->domain = ar_lustre_domain_crt_real();

  result->type = type;
  result->operands = NULL;
  result->value = value;
  result->name = NULL;

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_crt_unary(ar_lustre_expr_type type, ar_lustre_expr *op)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_expr_get_domain(op);
  result->type = type;
  result->operands = ccl_list_create();
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op));

  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_crt_binary(ar_lustre_expr_type type, 
	     ar_lustre_expr *op1, ar_lustre_expr *op2)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_expr_get_domain(op1);
  result->type = type;
  result->operands   = ccl_list_create();
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op1));
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op2));
  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_crt_boolean_binary(ar_lustre_expr_type type,
		     ar_lustre_expr *op1, ar_lustre_expr *op2)
{
  ar_lustre_expr *result = s_alloc_expr();

  result->refcount = 1;
  result->domain = ar_lustre_domain_crt_bool();
  result->type = type;
  result->operands = ccl_list_create();
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op1));
  ccl_list_add(result->operands,ar_lustre_expr_add_reference(op2));
  result->value.ival = 0;
  result->name = NULL;

  return result;
}

			/* --------------- */


