/*
 * syntax.y -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

%{
#include <ccl/ccl-memory.h>
#include "lustre/input-p.h"

#define YYSTYPE ar_lustre_tree *

#define ZN ((ar_lustre_tree *)NULL)

#define NN(_type,_vtype,_child,_next) \
(NODES = ccl_parse_tree_create(_type,#_type,_vtype,	\
 (_child)==ZN?ar_lustre_current_line:(_child)->line, \
 (_child)==ZN?ar_lustre_current_filename:(_child)->filename, \
 _child,_next,NODES))

#define NE(_type,_child,_next) NN(_type,CCL_PARSE_TREE_EMPTY,_child,_next)

#define NID()  NN(AL_IDENTIFIER,CCL_PARSE_TREE_IDENT,ZN,ZN)
#define NINT() NN(AL_UINT,CCL_PARSE_TREE_INT,ZN,ZN)
#define NSTR() NN(AR_TREE_STRING,CCL_PARSE_TREE_STRING,ZN,ZN)

# define SL(_x_) do { (_x_)->line = ar_lustre_current_line; } while(0)

#define malloc  ccl_malloc
#define realloc ccl_realloc
#define calloc  ccl_calloc
#define free    ccl_free

ar_lustre_tree *ar_lustre_syntax_tree;
ar_lustre_tree *ar_lustre_syntax_tree_container;

#define NODES ar_lustre_syntax_tree_container

extern void ar_lustre_error(const char *error);
extern int ar_lustre_lex(void);
extern char *ar_lustre_text;

#define MAX_NB_SYNTAX_ERROR 100
 int ar_lustre_number_of_errors;

%}


%token LTOK_CONST LTOK_BOOL LTOK_INTEGER LTOK_FLOAT
%token LTOK_NODE LTOK_ASSERT LTOK_OR LTOK_AND LTOK_IMPLY
%token LTOK_TRUE LTOK_FALSE LTOK_NOT LTOK_IF LTOK_ELSE
%token LTOK_NEQ LTOK_LEQ LTOK_GEQ LTOK_FOLLOWED
%token LTOK_UINTEGER LTOK_UFLOAT LTOK_IDENTIFIER LTOK_TYPE
%token LTOK_FUNCTION LTOK_RETURNS LTOK_WHEN LTOK_VAR 
%token LTOK_LET LTOK_TEL LTOK_PRE LTOK_CURRENT LTOK_XOR 
%token LTOK_DIV LTOK_MOD LTOK_THEN LTOK_INT LTOK_REAL

%start lustre_description

%%

lustre_description : 
  global_declaration_list 
  { ar_lustre_syntax_tree = $1; } 
;

global_declaration_list : 
  global_declaration global_declaration_list 
  { $$ = $1; $1->next = $2; }
| global_declaration 
;

global_declaration :
  LTOK_CONST constant_declaration_list 
  { $$ = NE(AL_CST_DECL_LIST,$2,ZN);  }
| type_declaration
| function_declaration
| node_declaration
;

constant_declaration_list :
  constant_declaration constant_declaration_list
  { $$ = $1; $1->next = $2; }
| constant_declaration
;

/************** constants declaration *****************/
constant_declaration : 
  constant_name '=' expression ';'
  { $$ = NE(AL_CST_DECL,$1,ZN); $1->next = $3; }
| constant_name_list ':' type ';'
  { $$ = NE(AL_CST_DECL,$1,ZN); $1->next = $3; }
;

constant_name :
  identifier 
;

constant_name_list :
  constant_name_list_rec
  { $$ = NE(AL_CST_NAME_LIST,$1,ZN); }
;

constant_name_list_rec :
  identifier ',' constant_name_list_rec 
  { $$ = $1; $1->next = $3; }
| identifier
  { $$ = $1; }
;

type :
  LTOK_BOOL
  { $$ = NE(AL_BOOLEANS,ZN,ZN);  }
| LTOK_INT
  { $$ = NE(AL_INTEGERS,ZN,ZN);  }
| LTOK_REAL
  { $$ = NE(AL_REALS,ZN,ZN);  }
| imported_type
  { $$ = NE(AL_IMPORTED_TYPE,$1,ZN);  }
;

/************** nodes declaration *****************/
node_declaration :
  node_header vars_decl equations_decl 
  { $$ = NE(AL_NODE,$1,ZN);  $1->next = $2; $2->next = $3; }
;

node_header :
  name_node '(' clocked_params_list ')' 
  LTOK_RETURNS 
  '(' clocked_returns_list ')' opt_semi_colon
  { $$ = NE(AL_NODE_SIGNATURE,$1,ZN);  $1->next = $3; $3->next = $7; }
;

name_node :
  LTOK_NODE identifier
  { $$ = $2; }
;

clocked_params_list :
  clocked_params_list_rec
  { $$ = NE(AL_NODE_PARAMS,$1,ZN);  }
;

clocked_params_list_rec :
  clocked_vars_param ';' clocked_params_list_rec
  { $$ = $1; $1->next = $3; }
| clocked_vars_param opt_semi_colon
;

clocked_vars_param :
  name_list ':' type 
  { $$ = NE(AL_NODE_PARAM,$1,ZN);  $1->next = $3; }
| LTOK_CONST name_list ':' type 
  { $$ = NE(AL_CST_DECL,$2,ZN);  $2->next = $4; }
| '(' name_list ')' ':' type LTOK_WHEN identifier 
  { $$ = NE(AL_NODE_PARAM,$2,ZN);  $2->next = $5; $5->next = $7; }
;

clocked_returns_list :
  clocked_returns_list_rec
  { $$ = NE(AL_NODE_RETURNS,$1,ZN);  }
;

clocked_returns_list_rec :
  clocked_vars_return ';' clocked_returns_list_rec
  { $$ = $1; $1->next = $3; }
| clocked_vars_return opt_semi_colon
;

clocked_vars_return :
  name_list ':' type 
  { $$ = NE(AL_NODE_RETURN,$1,ZN);  $1->next = $3; }
| '(' name_list ')' ':' type LTOK_WHEN identifier 
  { $$ = NE(AL_NODE_RETURN,$2,ZN);  $2->next = $5; $5->next = $7; }
;


name_list :
  name_list_rec
  { $$ = NE(AL_NAME_LIST,$1,ZN);  }
;

name_list_rec :
  name ',' name_list_rec
  { $$ = $1; $1->next = $3; }
| name
;

name :
  identifier
;

/*************** variables declaration ***************/
vars_decl :
  LTOK_VAR var_decl_list
  { $$ = NE(AL_NODE_VARS,$2,ZN);  }
| /* empty */
  { $$ = NE(AL_NODE_VARS,ZN,ZN);  }
;

var_decl_list :
  clocked_vars ';' var_decl_list
  { $$ = $1; $1->next = $3; }
| clocked_vars ';'
;


clocked_vars :
  name_list ':' type 
  { $$ = NE(AL_NODE_VAR,$1,ZN);  $1->next = $3; }
| '(' name_list ')' ':' type LTOK_WHEN identifier 
  { $$ = NE(AL_NODE_VAR,$2,ZN);  $2->next = $5; $5->next = $7; }
;

opt_semi_colon_or_dot: ';' | '.' | /* empty */ ;

equations_decl :
  LTOK_LET equations_list LTOK_TEL opt_semi_colon_or_dot
  { $$ = NE(AL_NODE_EQUATIONS,$2,ZN);  }
;

equations_list :
  equation equations_list
  { $$ = $1; $1->next = $2; }
| /* empty */ 
  { $$ = ZN; }
;

/*************** equations declaration ***************/
equation :
  left_equation '=' expression ';'
  { $$ = NE(AL_NODE_EQUATION,$1,ZN); $1->next = $3; }
| LTOK_ASSERT expression ';'
  { $$ = NE(AL_NODE_ASSERT,$2,ZN);  }
;

left_equation :
  '(' name_list ')'
  { $$ = $2; }
| name
  { $$ = NE(AL_NAME_LIST,$1,ZN);  }
;

expression : 
  expr0
;

expr0 :
  expr1
| expr1  LTOK_FOLLOWED expr0
  { $$ = NE(AL_FOLLOWED,$1,ZN); $1->next = $3; }
| expr1 LTOK_WHEN expr0
  { $$ = NE(AL_WHEN,$1,ZN); $1->next = $3; }
;

expr1 : 
  expr2
| LTOK_IF  expr0  LTOK_THEN  expr0  LTOK_ELSE  expr1
  { $$ = NE(AL_ITE,$2,ZN); $2->next = $4; $4->next = $6; } 
;
 
expr2 :
  expr3
| expr2  LTOK_OR  expr3
  { $$ = NE(AL_OR,$1,ZN); $1->next = $3; }
| expr2  LTOK_XOR  expr3
  { $$ = NE(AL_XOR,$1,ZN); $1->next = $3; }
; 

expr3 : 
  expr4
| expr3  LTOK_AND  expr4
  { $$ = NE(AL_AND,$1,ZN); $1->next = $3; }
| expr3  LTOK_IMPLY expr4 
  { $$ = NE(AL_IMPLY,$1,ZN); $1->next = $3; }
;

expr4 : 
  expr5
| LTOK_NOT  expr4
  { $$ = NE(AL_NOT,$2,ZN); }
;

expr5 :
  expr6
| expr6  '='  expr6
  { $$ = NE(AL_EQ,$1,ZN); $1->next = $3; }
| expr6  LTOK_NEQ  expr6
  { $$ = NE(AL_NEQ,$1,ZN); $1->next = $3; }
| expr6  '<'  expr6
  { $$ = NE(AL_LT,$1,ZN); $1->next = $3; }
| expr6  LTOK_LEQ  expr6
  { $$ = NE(AL_LEQ,$1,ZN); $1->next = $3; }
| expr6  '>'  expr6
  { $$ = NE(AL_GT,$1,ZN); $1->next = $3; }
| expr6  LTOK_GEQ  expr6
  { $$ = NE(AL_GEQ,$1,ZN); $1->next = $3; }
;

expr6 :
  expr7
| expr6  '+'  expr7
  { $$ = NE(AL_ADD,$1,ZN); $1->next = $3; }
| expr6  '-'  expr7
  { $$ = NE(AL_SUB,$1,ZN); $1->next = $3; }
;

expr7 :
  expr8
| expr7  '*'  expr8
  { $$ = NE(AL_MUL,$1,ZN); $1->next = $3; }
| expr7  LTOK_DIV  expr8
  { $$ = NE(AL_DIV,$1,ZN); $1->next = $3; }
| expr7  '/'  expr8
  { $$ = NE(AL_DIV,$1,ZN); $1->next = $3; }
| expr7  LTOK_MOD  expr8
  { $$ = NE(AL_MOD,$1,ZN); $1->next = $3; }
;
        
expr8 :
  expr9
| '-' expr8
  { $$ = NE(AL_NEG,$2,ZN); } 

  /*
| expr8 LTOK_WHEN expr9
  { $$ = NE(AL_WHEN,$1,ZN); $1->next = $3; }
;
  */

expr9 :
  expr10
| LTOK_PRE  expr9
  { $$ = NE(AL_PRE,$2,ZN); }
| LTOK_CURRENT expr9 
  { $$ = NE(AL_CURRENT,$2,ZN); }
;

expr10 :
  expressionlist
| atomic_expression
| node_name  expressionlist
  { $$ = NE(AL_CALL,$1,ZN); $1->next = $2; }
| '#'  '(' exprs ')'
  { $$ = NE(AL_ATMOST,$3,ZN); }
;

node_name :
  identifier 
;

expressionlist :
  '(' exprs  ')'
  { $$ = NE(AL_EXPR_LIST,$2,ZN); }
;


exprs :
  expression
| expression  ',' exprs
  { $$ = $1; $1->next = $3; }
;


atomic_expression :
  LTOK_TRUE
  { $$ = NE(AL_TRUE,ZN,ZN); }
| LTOK_FALSE
  { $$ = NE(AL_FALSE,ZN,ZN); }
| LTOK_UINTEGER
  { $$ = NINT(); $$->value = ar_lustre_token_value; }
| identifier
| LTOK_UFLOAT
  { $$ = NE(AL_UFLOAT,ZN,ZN); $$->value = ar_lustre_token_value; }
;


identifier :
  LTOK_IDENTIFIER 
  { $$ = NID(); $$->value = ar_lustre_token_value; }
;

opt_semi_colon : 
  ';' 
| /* empty */
;

type_declaration :
  LTOK_TYPE name_list ';' 
  { $$ = NE(AL_TYPE_DECL,$2,ZN); }
;

imported_type : 
  identifier  
;

function_declaration :
  LTOK_FUNCTION identifier '(' typed_params ')' 
  LTOK_RETURNS 
  '(' typed_returns ')' opt_semi_colon
  { 
    ar_lustre_tree *params = NE(AL_PARAMS,$4,ZN);
    ar_lustre_tree *returns = NE(AL_RETURNS,$8,ZN);

    $$ = NE(AL_FUNCTION_DECL,$2,ZN); 
    $2->next = params; 
    params->next = returns; 
  }
;

typed_params :
  param_decl_list ';' typed_params
  { $$ = $1; $1->next = $3; }
| param_decl_list opt_semi_colon
  { $$ = $1; }
;

param_decl_list :
  name_list ':' type
  { $$ = NE(AL_TYPED_ID_LIST,$1,ZN); $1->next = $3; }
;

typed_returns :
  return_decl_list ';' typed_returns
  { $$ = $3; $3->next = $1; }
| return_decl_list opt_semi_colon
  { $$ = $1; }
;

return_decl_list :
  name_list ':' type
  { $$ = NE(AL_TYPED_ID_LIST,$1,ZN); $1->next = $3; }
;

%%
