/*
 * proto.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "proto.h"

struct ar_lustre_proto_st {
  int refcount;
  ar_identifier *name;
  ar_lustre_domain *input_domain;
  ar_lustre_domain *output_domain;
};

			/* --------------- */

ar_lustre_proto *
ar_lustre_proto_create(ar_identifier *name,
		      ar_lustre_domain *input_domain,
		      ar_lustre_domain *output_domain)
{
  ar_lustre_proto *result = ccl_new(ar_lustre_proto);

  result->refcount = 1;
  result->name = ar_identifier_add_reference(name);
  result->input_domain = ar_lustre_domain_add_reference(input_domain);
  result->output_domain = ar_lustre_domain_add_reference(output_domain);

  return result;
}

			/* --------------- */

ar_lustre_proto *
ar_lustre_proto_add_reference(ar_lustre_proto *proto)
{
  ccl_pre( proto != NULL );

  proto->refcount++;

  return proto;
}

			/* --------------- */

void
ar_lustre_proto_del_reference(ar_lustre_proto *proto)
{
  ccl_pre( proto != NULL ); ccl_pre( proto->refcount > 0 );

  if( --proto->refcount == 0 )
    {
      ar_identifier_del_reference(proto->name);
      ar_lustre_domain_del_reference(proto->input_domain);
      ar_lustre_domain_del_reference(proto->output_domain);
      ccl_delete(proto);
    }
}

			/* --------------- */

ar_identifier *
ar_lustre_proto_get_name(ar_lustre_proto *proto)
{
  ccl_pre( proto != NULL );

  return ar_identifier_add_reference(proto->name);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_proto_get_input_domain(ar_lustre_proto *proto)
{
  ccl_pre( proto != NULL );

  return ar_lustre_domain_add_reference(proto->input_domain);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_proto_get_output_domain(ar_lustre_proto *proto)
{
  ccl_pre( proto != NULL );

  return ar_lustre_domain_add_reference(proto->output_domain);
}

			/* --------------- */

void
ar_lustre_proto_log(ccl_log_type log, ar_lustre_proto *proto)
{
  ccl_pre( proto != NULL );

  ccl_log(log,"function ");
  ar_identifier_log(log,proto->name);

  if( ! ar_lustre_domain_is_composite(proto->input_domain) ) ccl_log(log,"(");
  ar_lustre_domain_log(log,proto->input_domain);
  if( ! ar_lustre_domain_is_composite(proto->input_domain) ) ccl_log(log,")");
  ccl_log(log," returns ");
  if( ! ar_lustre_domain_is_composite(proto->output_domain) ) ccl_log(log,"(");
  ar_lustre_domain_log(log,proto->output_domain);
  if( ! ar_lustre_domain_is_composite(proto->output_domain) ) ccl_log(log,")");
  ccl_log(log,";");
}

			/* --------------- */

int
ar_lustre_proto_equals(const ar_lustre_proto *p1, const ar_lustre_proto *p2)
{
  ccl_pre( p1 != NULL ); ccl_pre( p2 != NULL );

  return p1->name == p2->name
    && ar_lustre_domain_equals(p1->input_domain,p2->input_domain)
    && ar_lustre_domain_equals(p1->output_domain,p2->output_domain);
}
