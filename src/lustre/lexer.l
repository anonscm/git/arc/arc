/*
 * lexer.l -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

%{
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "lustre/input-p.h"
#include "lustre/syntax.h"

#define YY_NO_UNPUT
/* Some systems define fileno as a preprocessor macro. */
#ifndef fileno
extern int fileno (FILE *);
#endif

ccl_parse_tree_value ar_lustre_token_value;
int ar_lustre_current_line;
const char *ar_lustre_current_filename;
%}

unsigned_integer ([1-9][0-9]*)|0

unsigned_float  ([0-9])*"."([0-9])+([eE]([+-])?([0-9])+)?

			/* --------------- */

simple_identifier  [a-zA-Z_][a-zA-Z_0-9]*

			/* --------------- */

lustre_comment      "--".* 

                    /* --------------- */

blank_character  [ \t\r]
newline          [\n]


%%

"const"          { return LTOK_CONST; }
"bool"           { return LTOK_BOOL; }
"int"            { return LTOK_INT; }
"real"           { return LTOK_REAL; } 
"node"           { return LTOK_NODE; }
"assert"         { return LTOK_ASSERT; }
"or"             { return LTOK_OR; }
"and"            { return LTOK_AND; }
"imply"          { return LTOK_IMPLY; }
"true"           { return LTOK_TRUE; }
"false"          { return LTOK_FALSE; }
"not"            { return LTOK_NOT; }
"if"             { return LTOK_IF; }
"then"           { return LTOK_THEN; }
"else"           { return LTOK_ELSE; }
"type"           { return LTOK_TYPE; }
"function"       { return LTOK_FUNCTION; }
"returns"        { return LTOK_RETURNS; }
"when"           { return LTOK_WHEN; }
"var"            { return LTOK_VAR; }
"let"            { return LTOK_LET; }
"tel"            { return LTOK_TEL; }
"pre"            { return LTOK_PRE; }
"current"        { return LTOK_CURRENT; }
"xor"            { return LTOK_XOR; }
"div"            { return LTOK_DIV; }
"mod"            { return LTOK_MOD; }

"<>"             { return LTOK_NEQ; }
"<="             { return LTOK_LEQ; }
">="             { return LTOK_GEQ; }
"=>"             { return LTOK_IMPLY; }
"->"             { return LTOK_FOLLOWED; }

{unsigned_float}    { 
  ar_lustre_token_value.flt_value = atof(yytext);
  return LTOK_UFLOAT; 
}
{unsigned_integer}  { 
  ar_lustre_token_value.int_value = atoi(yytext);
  return LTOK_UINTEGER; 
}

{simple_identifier} { 
  ar_lustre_token_value.id_value = ccl_string_make_unique(yytext);
  return LTOK_IDENTIFIER; 
}
{newline}            { ar_lustre_current_line++; }


{blank_character}+ { ((void)0); }
{lustre_comment} { ((void)0); }

.  { return (int) yytext[0];}

%%

void
ar_lustre_init_lexer(FILE *stream, const char *input_name)
{
  ar_lustre_in = stream;
  ar_lustre_current_line = 1;
  ar_lustre_current_filename = input_name;
  yyrestart(stream);
}

			/* --------------- */

void
ar_lustre_terminate_lexer(void)
{  
  if( YY_CURRENT_BUFFER != NULL )
      yy_delete_buffer(YY_CURRENT_BUFFER);

  ar_lustre_in = NULL;
  ar_lustre_current_line = -1;
  ar_lustre_current_filename = NULL;
}

			/* --------------- */

