/*
 * domain.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef DOMAIN_H
# define DOMAIN_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-list.h>
# include "ar-identifier.h"

typedef struct ar_lustre_domain_st ar_lustre_domain;

extern ar_lustre_domain *
ar_lustre_domain_crt_int(void);

extern ar_lustre_domain *
ar_lustre_domain_crt_real(void);

extern ar_lustre_domain *
ar_lustre_domain_crt_bool(void);

extern ar_lustre_domain *
ar_lustre_domain_crt_abstract(ar_identifier *name);

extern ar_lustre_domain *
ar_lustre_domain_crt_composite(ccl_list *comp);

extern ar_lustre_domain *
ar_lustre_domain_add_reference(ar_lustre_domain *dom);

extern void
ar_lustre_domain_del_reference(ar_lustre_domain *dom);

extern int
ar_lustre_domain_is_numerical(ar_lustre_domain *dom);

extern int
ar_lustre_domain_is_real(ar_lustre_domain *dom);

extern int
ar_lustre_domain_is_boolean(ar_lustre_domain *dom);

extern int
ar_lustre_domain_is_composite(ar_lustre_domain *dom);

extern int
ar_lustre_domain_is_abstract(ar_lustre_domain *dom);

extern int
ar_lustre_domain_get_width(ar_lustre_domain *dom);

extern ccl_list *
ar_lustre_domain_get_components(ar_lustre_domain *dom);

extern ar_identifier *
ar_lustre_domain_get_name(ar_lustre_domain *dom);

extern uint32_t
ar_lustre_domain_hashcode(ar_lustre_domain *dom);

extern int
ar_lustre_domain_equals(const ar_lustre_domain *dom1, 
			const ar_lustre_domain *dom2);

extern int
ar_lustre_domain_compare(const ar_lustre_domain *dom1, 
			 const ar_lustre_domain *dom2);

extern void
ar_lustre_domain_log(ccl_log_type log, ar_lustre_domain *dom);

extern void
ar_lustre_domain_log_gen(ccl_log_type log, ar_lustre_domain *dom,
			 ar_identifier_log_proc idlog);

#endif /* ! DOMAIN_H */
