/*
 * input-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef INPUT_P_H
# define INPUT_P_H

# include <stdio.h>
# include "tree.h"
# include "input.h"

extern ccl_parse_tree_value ar_lustre_token_value;
extern int ar_lustre_current_line;
extern const char *ar_lustre_current_filename;
extern FILE *ar_lustre_in;
extern ar_lustre_tree *ar_lustre_syntax_tree;
extern ar_lustre_tree *ar_lustre_syntax_tree_container;
extern int ar_lustre_number_of_errors;

extern void
ar_lustre_init_lexer(FILE *stream, const char *input_name);

extern void
ar_lustre_terminate_lexer(void);

extern int
ar_lustre_lex(void);

extern int
ar_lustre_parse(void);

extern void
ar_lustre_error(const char *message);

extern int
ar_lustre_wrap(void);

#endif /* ! INPUT_P_H */
