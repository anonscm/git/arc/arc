/*
 * a2l.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include "ar-attributes.h"
#include "ar-model.h"
#include "expr.h"
#include "translators-p.h"

			/* --------------- */

# define AR_TO_LUSTRE_DETERMINISTIC (0x1<<0)
# define AR_TO_LUSTRE_VERBOSE       (0x1<<1)
# define AR_TO_LUSTRE_WARNING       (0x1<<2)
# define AR_TO_LUSTRE_SHOW_SECTIONS (0x1<<3)

			/* --------------- */

typedef struct {
  int index;
  char *name;
  ar_node *node;
  int reference;
  int nb_sub;
  ar_identifier **subnames;
  int *sub;
  ar_idtable *init;
  ccl_list *comments;
  
  ccl_list *assigned;
  ccl_list *to_assign;
  ccl_list *assignments;
  ccl_list *assertions;

  ccl_list *parameters;
  ccl_list *local_vars;
  ccl_list *parent_inputs;
  ccl_list *public_inputs;
  ccl_list *parent_outputs;
  ccl_list *public_outputs;

  ar_idtable *event_interface;
  ar_idtable *event_assignments;
} COMPONENT;

typedef struct {
  ccl_log_type out;
  int flags;
  int nb_components;
  COMPONENT *components;
  ar_idtable *init;
  ar_idtable *protos;
  ar_idtable *constants;

  ar_lustre_domain *bool_dom;
  const char *noreturn;
  const char *noinput;
  const char *enum_prefix;
  const char *parameter_prefix;
  const char *state_var_prefix;
  const char *flow_var_prefix;
  const char *event_var_suffix;
  const char *missing_init_prefix;
  const char *guard_var_prefix;
  const char *signature_arg_prefix;
  const char *signature_ret_suffix;
} TRANSLATOR;

			/* --------------- */


static void
s_delete_component(COMPONENT *c);

static COMPONENT *
s_get_sub(TRANSLATOR *C, COMPONENT *node, int i);

static void
s_output_lustre_node(TRANSLATOR *C, int comp);

static void
s_translate_global_data(TRANSLATOR *C, ar_node *node);

static int
s_build_and_compile_node(TRANSLATOR *C, ar_identifier *name, ar_node *node,
			 int *pindex);

static void
s_compile_component(TRANSLATOR *C, int comp);

static ar_lustre_domain *
s_translate_domain(TRANSLATOR *C, ar_type *t);

static ar_lustre_expr *
s_translate_domain_constraint(TRANSLATOR *C, ar_context_slot *var);

static ar_lustre_expr *
s_translate_slot(TRANSLATOR *C, ar_context_slot *sl);

static ar_lustre_expr *
s_translate_formula(TRANSLATOR *C, ar_expr *e);

static void
s_ID2LUSTRE(ccl_log_type log, const ar_identifier *id);

static char *
s_ID2LUSTRE_STR(const ar_identifier *id);

			/* --------------- */

void
ar_altarica_to_lustre_init(ccl_config_table *conf)
{
}

			/* --------------- */


void
ar_altarica_to_lustre_terminate(void)
{
}

			/* --------------- */

#define PROPID(_prop) (AR_LUSTRE_PREF_NAMES[LUSTRE_PREF_ ## _prop])
void
ar_node_to_lustre(ar_node *node, ccl_log_type out, ccl_config_table *conf)
{
  TRANSLATOR C;
  int i;
  ar_identifier *nodename;

  C.out = out;
  C.flags = 0;

  if( ccl_config_table_get_boolean (conf, PROPID(A2L_DETERMINISTIC)) )
    C.flags |= AR_TO_LUSTRE_DETERMINISTIC;
  if( ccl_config_table_get_boolean (conf, PROPID(A2L_VERBOSE)) )
    C.flags |= AR_TO_LUSTRE_VERBOSE;
  if( ccl_config_table_get_boolean (conf, PROPID(A2L_WARNING)) )
    C.flags |= AR_TO_LUSTRE_WARNING;
  if( ccl_config_table_get_boolean (conf, PROPID(A2L_SHOW_SECTIONS)) )
    C.flags |= AR_TO_LUSTRE_SHOW_SECTIONS;

  C.nb_components = 0;
  C.components = NULL;
  C.init = ar_idtable_create(1, (ccl_duplicate_func *)ar_expr_add_reference,
			     (ccl_delete_proc *)ar_expr_del_reference);
  C.protos = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_proto_add_reference,
		      (ccl_delete_proc *)ar_lustre_proto_del_reference);
  C.constants = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_expr_add_reference,
		      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  C.bool_dom = ar_lustre_domain_crt_bool();
  C.enum_prefix = ccl_config_table_get (conf, PROPID(A2L_ENUM_PREFIX));
  C.parameter_prefix = ccl_config_table_get (conf, PROPID(A2L_PARAMETER_PREFIX));
  C.state_var_prefix = ccl_config_table_get (conf, PROPID(A2L_STATE_VAR_PREFIX));
  C.flow_var_prefix = ccl_config_table_get (conf, PROPID(A2L_FLOW_VAR_PREFIX));
  C.event_var_suffix  = ccl_config_table_get (conf, PROPID(A2L_EVENT_VAR_SUFFIX));
  C.missing_init_prefix = ccl_config_table_get (conf, PROPID(A2L_MISSING_PREFIX));
  C.guard_var_prefix = ccl_config_table_get (conf, PROPID(A2L_GUARD_VAR_PREFIX));
  C.noreturn  = ccl_config_table_get (conf, PROPID(A2L_NORETURN));
  C.noinput  = ccl_config_table_get (conf, PROPID(A2L_NOINPUT));
  C.signature_arg_prefix = ccl_config_table_get (conf, PROPID(A2L_SIGARG_PREFIX));
  C.signature_ret_suffix = ccl_config_table_get (conf, PROPID(A2L_SIGRET_SUFFIX));
  i = 0;
  nodename = ar_node_get_name(node);
  s_translate_global_data(&C,node);

  s_build_and_compile_node(&C,nodename,node,&i);
  ar_identifier_del_reference(nodename);
  ccl_assert( i == C.nb_components );

  if( (C.flags & AR_TO_LUSTRE_SHOW_SECTIONS) )
    ccl_log(C.out,
	    "--\n"
	    "-- TRANSLATED NODES\n"
	    "--\n"
	    "\n");
  
  for(i = 0; i < C.nb_components; i++)
    {
      if( C.components[i].reference == -1 )
	{
	  s_output_lustre_node(&C,i);
	  ccl_log(C.out,"\n");
	}
      s_delete_component(C.components+i);
    }
  ar_idtable_del_reference(C.init);
  ar_idtable_del_reference(C.protos);
  ar_idtable_del_reference(C.constants);
  ar_lustre_domain_del_reference(C.bool_dom);
  ccl_delete(C.components);
}

			/* --------------- */

static void
s_delete_component(COMPONENT *c)
{
  ar_node_del_reference(c->node);
  ccl_zdelete(ccl_delete,c->sub);
  ccl_zdelete(ccl_string_delete,c->name);
  ccl_zdelete(ar_idtable_del_reference,c->init);

  if( c->comments != NULL )
    ccl_list_clear_and_delete(c->comments,ccl_string_delete);

  if( c->assigned != NULL )
    ccl_list_clear_and_delete(c->assigned,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->to_assign != NULL )
    ccl_list_clear_and_delete(c->to_assign,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->assertions != NULL )
    ccl_list_clear_and_delete(c->assertions,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->assignments != NULL )
    ccl_list_clear_and_delete(c->assignments,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->parameters != NULL )
    ccl_list_clear_and_delete(c->parameters,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->local_vars != NULL )
    ccl_list_clear_and_delete(c->local_vars,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->parent_inputs != NULL )
    ccl_list_clear_and_delete(c->parent_inputs,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->public_inputs != NULL )
    ccl_list_clear_and_delete(c->public_inputs,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->parent_outputs != NULL )
    ccl_list_clear_and_delete(c->parent_outputs,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  if( c->public_outputs != NULL )
    ccl_list_clear_and_delete(c->public_outputs,
			      (ccl_delete_proc *)ar_lustre_expr_del_reference);

  if( c->event_interface != NULL )
    {
      ar_identifier_iterator *it = ar_idtable_get_keys(c->event_interface);

      while( ccl_iterator_has_more_elements(it) )
	{
	  ar_identifier *id = ccl_iterator_next_element(it);
	  ccl_list *l = ar_idtable_get(c->event_interface,id);
	  ccl_list_clear_and_delete(l,(ccl_delete_proc *)
				    ar_identifier_del_reference);
	  ar_identifier_del_reference(id);
	}
      ccl_iterator_delete(it);
      ar_idtable_del_reference(c->event_interface);
    }

  if( c->event_assignments != NULL )
    {
      ar_identifier_iterator *it = ar_idtable_get_keys(c->event_assignments);
      
      while( ccl_iterator_has_more_elements(it) )
	{
	  ar_identifier *id = ccl_iterator_next_element(it);
	  ar_lustre_expr *e = ar_idtable_get(c->event_assignments,id);
	  ar_lustre_expr_del_reference(e);
	  ar_identifier_del_reference(id);
	}
      ccl_iterator_delete(it);
      ar_idtable_del_reference(c->event_assignments);
    }

  if( c->nb_sub > 0 )
    {
      int i;

      for(i = 0; i < c->nb_sub; i++)
	ar_identifier_del_reference(c->subnames[i]);
      ccl_delete(c->subnames);
    }
}

			/* --------------- */

static COMPONENT *
s_get_sub(TRANSLATOR *C, COMPONENT *node, int i)
{
  int index = node->sub[i];

  if( C->components[index].reference >= 0 )
    index = C->components[index].reference;
  return C->components+index;
}

			/* --------------- */

static int
s_cmp_variables(const void *p1, const void *p2)
{
  const ar_lustre_expr *v1 = p1;
  const ar_lustre_expr *v2 = p2;
  int result = 0;

  if( v1 != v2 )
    {
      ar_lustre_domain *d1 = ar_lustre_expr_get_domain(v1);
      ar_lustre_domain *d2 = ar_lustre_expr_get_domain(v2);
      result = ar_lustre_domain_compare(d1,d2);
      ar_lustre_domain_del_reference(d1);
      ar_lustre_domain_del_reference(d2);

      if( result == 0 )
	{
	  ar_identifier *name1 = ar_lustre_expr_get_name(v1);
	  ar_identifier *name2 = ar_lustre_expr_get_name(v2);
	  result = ar_identifier_compare(name1,name2);
	  ar_identifier_del_reference(name1);
	  ar_identifier_del_reference(name2);
	}
    }

  return result;
}

			/* --------------- */

static char *
s_ID2LUSTRE_STR(const ar_identifier *id)
{
  char *s;
  char *str = ar_identifier_to_string(id);


  if( *str != '_' && ! ('a' <= *str && *str <= 'z') 
      && ! ('A' <= *str && *str <= 'Z') )
    {
      s = ccl_string_format_new("_%s",str);
      ccl_string_delete(str);
      str = s;
    }

  for(s = str; *s; s++)
    {
      if( *s == '_' || ('0' <= *s && *s <= '9') || ('a' <= *s && *s <= 'z') || 
	  ('A' <= *s && *s <= 'Z') )
	continue;
      *s = '_';
    }

  return str;
}

			/* --------------- */

static void
s_ID2LUSTRE(ccl_log_type log, const ar_identifier *id)
{
  char *str = s_ID2LUSTRE_STR(id);
  ccl_log(log,"%s",str);
  ccl_string_delete(str);
}

			/* --------------- */

static void
s_output_var_list(TRANSLATOR *C, ccl_list *l, const char *tab,
		  const char *newline)
{
  ccl_pair *p;
  ar_lustre_domain *curdom = NULL;

  for(p = FIRST(l); p; p = CDR(p))
    {
      ar_lustre_expr *var = CAR(p);
      ar_lustre_domain *vardom = ar_lustre_expr_get_domain(var);
      
      if( curdom == NULL )
	{
	  ccl_log(C->out,"%s",tab);
	  ar_lustre_expr_log_gen(C->out,var,s_ID2LUSTRE);
	  curdom = ar_lustre_domain_add_reference(vardom);
	}
      else if( ar_lustre_domain_equals(vardom,curdom) )
	{
	  ccl_log(C->out,", ");
	  ar_lustre_expr_log_gen(C->out,var,s_ID2LUSTRE);
	}
      else
	{
	  ccl_log(C->out," : ");
	  ar_lustre_domain_log_gen(C->out,curdom,s_ID2LUSTRE);
	  ccl_log(C->out,";%s%s",newline,tab);
	  ar_lustre_expr_log_gen(C->out,var,s_ID2LUSTRE);
	  ar_lustre_domain_del_reference(curdom);
	  curdom = ar_lustre_domain_add_reference(vardom);
	}
      ar_lustre_domain_del_reference(vardom);
    }

  if( curdom != NULL )
    {
      ccl_log(C->out," : ");
      ar_lustre_domain_log_gen(C->out,curdom,s_ID2LUSTRE);
      ar_lustre_domain_del_reference(curdom);
    }
}

			/* --------------- */

static ccl_list *
s_event_declaration(TRANSLATOR *C, int comp)
{
  COMPONENT *self = C->components+comp;
  ar_identifier_iterator *it = ar_idtable_get_keys(self->event_interface);
  ccl_list *tmp = ccl_list_create();
  ccl_list *l = ccl_list_create();

  while( ccl_iterator_has_more_elements(it) )
    {
      ar_identifier *id = ccl_iterator_next_element(it);
      ccl_list *evars = ar_idtable_get(self->event_interface,id);
      
      if( id != AR_EPSILON_ID || ! ccl_list_is_empty(evars) )
	{
	  ccl_pair *p;
	  
	  for(p = FIRST(evars); p; p = CDR(p))
	    {
	      ar_lustre_expr *evar = 
		ar_lustre_expr_crt_variable(CAR(p),C->bool_dom);
	      if( ccl_list_get_index(l,evar,s_cmp_variables) < 0 )
		ccl_list_add(l,ar_lustre_expr_add_reference(evar));
	      ccl_list_add(tmp,ar_lustre_expr_add_reference(evar));
	      ar_lustre_expr_del_reference(evar);
	    }
	}

      ar_identifier_del_reference(id);
    }

  if( ccl_list_get_size(tmp) > 1 )
    {      
      ar_lustre_expr *event_constraint = ar_lustre_expr_crt_atmost(tmp);
      ccl_list_add(self->assertions,event_constraint);
    }

  ccl_list_clear_and_delete(tmp,
			    (ccl_delete_proc *)ar_lustre_expr_del_reference);
  ccl_iterator_delete(it);

  return l;
}

			/* --------------- */
static void
s_output_lustre_node(TRANSLATOR *C, int comp)
{
  ccl_pair *p;
  COMPONENT *self = C->components+comp;

  for(p = FIRST(self->comments); p; p = CDR(p))    
    ccl_log(C->out,"-- %s\n",CAR(p));
  
  ccl_log(C->out,"node %s(",self->name);  
  {
    ccl_list *l;
    ccl_list *le;

    if( ! ccl_list_is_empty(self->parameters) )
      {
	l = ccl_list_dup(self->parameters);
	ccl_list_sort(l,s_cmp_variables);
	s_output_var_list(C,l,"const "," ");
	ccl_list_delete(l);
	ccl_log(C->out,"; ");
      }

    l = ccl_list_dup(self->public_inputs);
    le = s_event_declaration(C,comp);
    ccl_list_append(l,self->parent_inputs);

    ccl_list_append(l,le);

    if( ccl_list_is_empty(l) )
      ccl_log(C->out,"%s : bool",C->noinput);
    else
      {
	ccl_list_sort(l,s_cmp_variables);
	s_output_var_list(C,l,""," ");
      }
    ccl_list_delete(l);
    ccl_list_clear_and_delete(le,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
    ccl_log(C->out,")\n");
    ccl_log(C->out,"    returns (");
    l = ccl_list_dup(self->public_outputs);
    ccl_list_append(l,self->parent_outputs);

    if( ccl_list_is_empty(l) )
      ccl_log(C->out,"%s : bool",C->noreturn);
    else
      {
	ccl_list_sort(l,s_cmp_variables);
	s_output_var_list(C,l,""," ");
      }
    ccl_list_delete(l);
    
    ccl_log(C->out,");\n");
    if( ! ccl_list_is_empty(self->local_vars) )
      {
	ccl_log(C->out,"var\n");
	l = ccl_list_dup(self->local_vars);
	ccl_list_sort(l,s_cmp_variables);
	s_output_var_list(C,l,"   ","\n");
	ccl_list_delete(l);
	ccl_log(C->out,";\n");
      }
  }
  ccl_log(C->out,"let\n");
  if( ! ccl_list_is_empty(self->assignments) )
    {
      if( (C->flags & AR_TO_LUSTRE_SHOW_SECTIONS ) )
	ccl_log(C->out,"-- Equations\n");
      for(p = FIRST(self->assignments); p; p = CDR(p))
	{
	  ar_lustre_expr *eq = CAR(p);
	  ccl_list *ops = ar_lustre_expr_get_operands(eq);
	  ccl_log(C->out,"   ");	  
	  ar_lustre_expr_log_gen(C->out,CAR(FIRST(ops)),s_ID2LUSTRE);
	  ccl_log(C->out," = ");
	  ar_lustre_expr_log_gen(C->out,CADR(FIRST(ops)),s_ID2LUSTRE);
	  ccl_log(C->out,";\n");	  
	  ccl_list_clear_and_delete(ops,(ccl_delete_proc *)
				    ar_lustre_expr_del_reference);
	}
      if( ! ccl_list_is_empty(self->assertions) )
	ccl_log(C->out,"\n");
    }

  if( ! ccl_list_is_empty(self->assertions) )
    {
      if( (C->flags & AR_TO_LUSTRE_SHOW_SECTIONS ) )
	ccl_log(C->out,"-- Constraints\n");
      for(p = FIRST(self->assertions); p; p = CDR(p))
	{
	  ccl_log(C->out,"   assert( ");
	  ar_lustre_expr_log_gen(C->out,CAR(p),s_ID2LUSTRE);
	  ccl_log(C->out," );\n");	  
	}
    }
  ccl_log(C->out,"tel\n");
}

			/* --------------- */

static void
s_collect_global_data_from_type(ar_type *type, ccl_list *symbolic_constants,
				ccl_list *abstract_types)
{
  if( ar_type_get_kind(type) == AR_TYPE_SYMBOL_SET )
    {
      ccl_int_iterator *v = ar_type_symbol_set_get_values(type);

      while( ccl_iterator_has_more_elements(v) )
	{
	  int vid = ccl_iterator_next_element(v);
	  ar_identifier *id = ar_type_symbol_set_get_value_name(vid);
	  if( ! ccl_list_has(symbolic_constants,id) )
	    ccl_list_add(symbolic_constants,id);
	  else
	    ar_identifier_del_reference(id);
	}
      ccl_iterator_delete(v);
    } 
  else if( ar_type_get_kind(type) == AR_TYPE_ABSTRACT )
    {
      ar_identifier *aname = ar_type_abstract_get_name(type);

      if( ! ccl_list_has(abstract_types,aname) )
	ccl_list_add(abstract_types,aname);
      else
	ar_identifier_del_reference(aname);
    }
  else if( ar_type_get_kind(type) == AR_TYPE_ARRAY )
    {
      ar_type *subtype = ar_type_array_get_base_type(type);

      s_collect_global_data_from_type(subtype,symbolic_constants,
				      abstract_types);
      ar_type_del_reference(subtype);
    }
  else if( ar_type_get_kind(type) == AR_TYPE_STRUCTURE )
    {
      ar_identifier_iterator *i = ar_type_struct_get_fields(type);
      while( ccl_iterator_has_more_elements(i) )
	{
	  ar_identifier *fid = ccl_iterator_next_element(i);
	  ar_type *ftype = ar_type_struct_get_field(type,fid);
	  s_collect_global_data_from_type(ftype,symbolic_constants,
					  abstract_types);
	  ar_type_del_reference(ftype);
	  ar_identifier_del_reference(fid);
	}
      ccl_iterator_delete(i);
    }
}

			/* --------------- */

static void
s_collect_data_in_expr(TRANSLATOR *C, ar_node *node, ar_expr *e,
		       ccl_list *symbolic_constants, 
		       ccl_list *abstract_types,
		       ccl_list *signatures,
		       ccl_list *constants)
{
  switch( ar_expr_get_op(e) ) {
  case AR_EXPR_ITE: case AR_EXPR_NOT: case AR_EXPR_NEG: case AR_EXPR_OR: 
  case AR_EXPR_AND: case AR_EXPR_EQ:  case AR_EXPR_NEQ: case AR_EXPR_LT: 
  case AR_EXPR_GT:  case AR_EXPR_LEQ: case AR_EXPR_GEQ: case AR_EXPR_ADD: 
  case AR_EXPR_SUB: case AR_EXPR_MUL: case AR_EXPR_DIV: case AR_EXPR_MOD: 
  case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
  case AR_EXPR_ARRAY: case AR_EXPR_STRUCT: case AR_EXPR_EXIST: 
  case AR_EXPR_FORALL: case AR_EXPR_CALL:
  case AR_EXPR_MIN: case AR_EXPR_MAX:
    {
      int i;
      int arity;
      ar_expr **args = ar_expr_get_args(e,&arity);
      for(i = 0; i < arity; i++)
	s_collect_data_in_expr(C,node,args[i],symbolic_constants,
			       abstract_types,signatures,constants);
      if( ar_expr_get_op(e) == AR_EXPR_CALL )
	{
	  ar_signature *sig = ar_expr_call_get_signature(e);

	  if( ! ccl_list_has(signatures,sig) )
	    {
	      ar_type *t;

	      ccl_list_add(signatures,sig);

	      for(i = 0; i < arity; i++)
		{

		  t = ar_signature_get_ith_arg_domain(sig,i);
		  s_collect_global_data_from_type(t,symbolic_constants,
						  abstract_types);
		  ar_type_del_reference(t);
		}
	      t = ar_signature_get_return_domain(sig);
	      s_collect_global_data_from_type(t,symbolic_constants,
					      abstract_types);
	      ar_type_del_reference(t);
	    }
	  else
	    ar_signature_del_reference(sig);
	}
    }
    break;
  
  case AR_EXPR_CST:     
  case AR_EXPR_VAR: 
    {
      ar_type *type = ar_expr_get_type(e);

      if( ar_type_get_kind(type) == AR_TYPE_SYMBOLS )
	{
	  ar_constant *cst = ar_expr_eval(e, NULL);
	  int index = ar_constant_get_symbol_value(cst);
	  ar_identifier *id = ar_type_symbol_set_get_value_name(index);
	  if( ! ccl_list_has(symbolic_constants,id) )
	    ccl_list_add(symbolic_constants,id);
	  else
	    ar_identifier_del_reference(id);
	  ar_constant_del_reference(cst);
	}
      else
	s_collect_global_data_from_type(type,symbolic_constants,
					abstract_types);
      ar_type_del_reference(type);
    }
    break;

  case AR_EXPR_PARAM:
    {
      ar_context_slot *sl = ar_expr_get_slot(e);
      ar_expr *value = ar_context_slot_get_value(sl);
      ar_identifier *id = ar_context_slot_get_name(sl);      

      if( ar_context_has_slot(AR_MODEL_CONTEXT,id) )
	{
	  ar_type *type = ar_context_slot_get_type(sl);
	  s_collect_global_data_from_type(type,symbolic_constants,
					  abstract_types);
	  ar_type_del_reference(type);
	  ar_context_expand_composite_slot(AR_MODEL_CONTEXT,sl,constants);
	}
      ar_context_slot_del_reference(sl);
      ar_identifier_del_reference(id);

      if( value != NULL )
	{
	  s_collect_data_in_expr(C,node,value,symbolic_constants,
				 abstract_types,signatures,constants);
	  ar_expr_del_reference(value);
	}
    }
    break;
  }
}

			/* --------------- */

static int
s_collect_global_data(TRANSLATOR *C, ar_node *node, 
		      ccl_list *symbolic_constants, ccl_list *abstract_types,
		      ccl_list *signatures, ccl_list *constants)
{
  int nb_components = 1;

  /* parameters & variables */
  {
    int i;

    for(i = 0; i < 2; i++)
      {
	ar_context_slot_iterator *it;

	if( i == 0 ) it = ar_node_get_slots_for_parameters(node);
	else it = ar_node_get_slots_for_variables(node);

	while( ccl_iterator_has_more_elements(it) )
	  {
	    ar_context_slot *sl = ccl_iterator_next_element(it);
	    ar_type *type = ar_context_slot_get_type(sl);
	    s_collect_global_data_from_type(type,symbolic_constants,
					    abstract_types);
	    if( i == 0 )
	      {
		ar_expr *value = ar_context_slot_get_value(sl);
		if( value != NULL )
		  {
		    s_collect_data_in_expr(C,node,value,symbolic_constants, 
					   abstract_types,signatures,
					   constants);
		    ar_expr_del_reference(value);
		  }
	      }
	    ar_type_del_reference(type);
	    ar_context_slot_del_reference(sl);
	  }
	ccl_iterator_delete(it);
      }
  }

  {
    ccl_pair *p;
    ccl_list *assertions = ar_node_get_assertions(node);

    for(p = FIRST(assertions); p; p = CDR(p))
      s_collect_data_in_expr(C,node,CAR(p),symbolic_constants,abstract_types,
			     signatures,constants);
  }

  {
    ccl_pair *p;
    ccl_list *init_c = ar_node_get_initial_constraints(node);

    for(p = FIRST (init_c); p; p = CDR (p))
      s_collect_data_in_expr (C, node, CAR (p), symbolic_constants,
			      abstract_types, signatures,constants);
  }

  {
    ccl_pair *p;
    ccl_list *initial_states = ar_node_get_initialized_slots (node);

    for(p = FIRST(initial_states); p; p = CDR(p))
      {
	ar_expr *val = ar_node_get_initial_value (node, CAR (p));
	s_collect_data_in_expr (C, node, val, symbolic_constants,
				abstract_types, signatures, constants);
	ar_expr_del_reference (val);
      }
  }

  {
    ccl_pair *p;
    ccl_list *trans = ar_node_get_transitions(node);

    for(p = FIRST(trans); p; p = CDR(p))
      {
	const ar_trans_assignment *a;
	ar_trans *t = (ar_trans *)CAR(p);
	ar_expr *g = ar_trans_get_guard(t);

	s_collect_data_in_expr(C,node,g,symbolic_constants,abstract_types,
			     signatures,constants);
	ar_expr_del_reference(g);
	for(a = ar_trans_get_assignments(t); a; a = a->next)
	s_collect_data_in_expr(C,node,a->value,symbolic_constants,
			       abstract_types,signatures,constants);
      }
  }

  /* subnodes */
  if( ! ar_node_is_leaf(node) )
    {      
      ar_identifier_iterator *it = ar_node_get_names_of_subnodes (node);
      
      while (ccl_iterator_has_more_elements (it))
	{
	  ar_identifier *id = ccl_iterator_next_element (it);
	  ar_node *subnode = ar_node_get_subnode_model (node, id);

	  nb_components += s_collect_global_data(C,subnode,symbolic_constants,
						 abstract_types,
						 signatures,constants);
	  ar_identifier_del_reference (id);
	  ar_node_del_reference (subnode);
	}
      ccl_iterator_delete(it);
    }

  return nb_components;
}

			/* --------------- */

static ar_lustre_domain *
s_type_to_lustre_domain(TRANSLATOR *C, ar_type *type)
{
  ar_lustre_domain *result = NULL;

  if( ! ar_type_is_scalar(type) )
    {
      ccl_error("A2L: error: can't translate non-scalar type in signatures\n");
      ccl_throw_no_msg (internal_error);
    }

  switch( ar_type_get_kind(type) ) {
  case AR_TYPE_BOOLEANS:
    result = ar_lustre_domain_add_reference(C->bool_dom);
    break;
  case AR_TYPE_ABSTRACT:
    {
      ar_identifier *name = ar_type_abstract_get_name(type);
      result = ar_lustre_domain_crt_abstract(name);
      ar_identifier_del_reference(name);
    }
    break;

  case AR_TYPE_INTEGERS: case AR_TYPE_SYMBOLS:
  case AR_TYPE_RANGE:
  case AR_TYPE_SYMBOL_SET:
    result = ar_lustre_domain_crt_int();
    break;
  default : 
    ccl_throw_no_msg (internal_error);
  }

  return result;
}

			/* --------------- */

static ar_lustre_proto *
s_signature_to_lustre_proto(TRANSLATOR *C, ar_signature *sig)
{
  int arity = ar_signature_get_arity(sig);
  ar_lustre_domain *indom = NULL;
  ar_lustre_domain *outdom;
  ar_identifier *signame = ar_signature_get_name(sig);
  ar_lustre_proto *result = ar_idtable_get(C->protos,signame);

  
  if( result == NULL )
    {
      if( arity == 0 )
	{
	  ccl_error("A2L: error: does not support 0-arity signatures\n");
	  ccl_throw_no_msg (internal_error);
	}
      else if( arity == 1 )
	{
	  ar_type *t = ar_signature_get_ith_arg_domain(sig,0);
	  indom = s_type_to_lustre_domain(C,t);
	  ar_type_del_reference(t);
	}
      else
	{
	  int i; 
	  ccl_list *comp = ccl_list_create();
	  for(i = 0; i < arity; i++)
	    {
	      ar_type *t = ar_signature_get_ith_arg_domain(sig,0);
	      ccl_list_add(comp,s_type_to_lustre_domain(C,t));
	      ar_type_del_reference(t);
	    }
	  indom = ar_lustre_domain_crt_composite(comp);
	  ccl_list_clear_and_delete(comp,(ccl_delete_proc *)
				    ar_lustre_domain_del_reference);
	}

      {
	ar_type *t = ar_signature_get_ith_arg_domain(sig,0);
	outdom = s_type_to_lustre_domain(C,t);
	ar_type_del_reference(t);
      }
      result = ar_lustre_proto_create(signame,indom,outdom);
      ar_idtable_put(C->protos,signame,result);
      ar_lustre_domain_del_reference(indom);
      ar_lustre_domain_del_reference(outdom);
    }
  ar_identifier_del_reference(signame);

  return result;
}

			/* --------------- */

static void
s_translate_global_data(TRANSLATOR *C, ar_node *node)
{
  ccl_pair *p;
  ccl_list *symbolic_constants = ccl_list_create();
  ccl_list *abstract_types = ccl_list_create();
  ccl_list *signatures = ccl_list_create();
  ccl_list *constants = ccl_list_create();

  C->nb_components = 
    s_collect_global_data(C,node,symbolic_constants,abstract_types,signatures,
			  constants);

  C->components = ccl_new_array(COMPONENT,C->nb_components);
  if( ! ccl_list_is_empty(symbolic_constants) )
    {
      if( (C->flags & AR_TO_LUSTRE_SHOW_SECTIONS) )
	ccl_log(C->out,
		"--\n"
		"-- ENUMERATIONS\n"
		"--\n"
		"-- The following constant values are used to simulate "
		"enumerated values of\n"
		"-- the AltaRica language.\n"
		"--\n");

      for(p = FIRST(symbolic_constants); p; p = CDR(p))
	{
	  ar_identifier *sname = (ar_identifier *)CAR(p);
	  ccl_log(C->out,"const %s",C->enum_prefix);
	  s_ID2LUSTRE(C->out,sname);
	  ccl_log(C->out," = %d;\n",ar_type_symbol_set_get_value(sname));
	  ar_identifier_del_reference(sname);
	}
      ccl_log(C->out,"\n");
    }
  ccl_list_delete(symbolic_constants);
  
  if( ! ccl_list_is_empty(abstract_types) )
    {
      if( (C->flags & AR_TO_LUSTRE_SHOW_SECTIONS) )
	ccl_log(C->out,
		"--\n"
		"-- ABSTRACT TYPES\n"
		"--\n");
      ccl_log(C->out,"type");
      for(p = FIRST(abstract_types); p; p = CDR(p))
	{
	  ar_identifier *typename = (ar_identifier *)CAR(p);

	  ccl_log(C->out,"     ");
	  s_ID2LUSTRE(C->out,typename);
	   ccl_log(C->out,"%c\n",CDR(p)?',':';');
	  ar_identifier_del_reference(typename);
	}
      ccl_log(C->out,"\n");
    }
  ccl_list_delete(abstract_types);

  if( ! ccl_list_is_empty(signatures) )
    {
      if( (C->flags & AR_TO_LUSTRE_SHOW_SECTIONS) )
	ccl_log(C->out,
		"--\n"
		"-- SIGNATURES OF EXTERNAL FUNCTIONS\n"
		"--\n");

      for(p = FIRST(signatures); p; p = CDR(p))
	{
	  int i;
	  ar_signature *sig = CAR(p);
	  ar_identifier *name = ar_signature_get_name(sig);
	  int nb_args = ar_signature_get_arity(sig);
	  ar_type *atype;
	  ccl_log(C->out,"function ");
	  s_ID2LUSTRE(C->out,name);
	  ccl_log(C->out,"\n         (");
	  for(i = 0; i < nb_args; i++)
	    {
	      atype = ar_signature_get_ith_arg_domain(sig,i);
	      s_ID2LUSTRE(C->out,name);
	      ccl_log(C->out,"%s%d : ",C->signature_arg_prefix,i);
	      {
		ar_lustre_domain *ld = s_translate_domain(C,atype);
		ar_lustre_domain_log(C->out,ld);
		ar_lustre_domain_del_reference(ld);
	      }
	      if( i < nb_args-1 )
		ccl_log(C->out,"; ");
	      ar_type_del_reference(atype);
	    }
	  ccl_log(C->out,")\n   returns (");
	  s_ID2LUSTRE(C->out,name);
	  ccl_log(C->out,"%s : ",C->signature_ret_suffix);

	  atype = ar_signature_get_return_domain(sig);
	  {
	    ar_lustre_domain *ld = s_translate_domain(C,atype);
	    ar_lustre_domain_log(C->out,ld);
	    ar_lustre_domain_del_reference(ld);
	  }
	  ar_type_del_reference(atype);

	  ccl_log(C->out,");\n");
	  ar_identifier_del_reference(name);
	  ar_signature_del_reference(sig);

	  ccl_log(C->out,"\n");
	}
      ccl_log(C->out,"\n");
    }
  ccl_list_delete(signatures);

  if( ! ccl_list_is_empty(constants) )
    {
      if( (C->flags & AR_TO_LUSTRE_SHOW_SECTIONS) )
	ccl_log(C->out,
		"--\n"
		"-- GLOBAL CONSTANTS & PARAMETERS\n"
		"--\n");

      for(p = FIRST(constants); p; p = CDR(p))
	{
	  ar_context_slot *sl = (ar_context_slot *)CAR(p);
	  ar_identifier *name = ar_context_slot_get_name(sl);
	  ar_type *type = ar_context_slot_get_type(sl);
	  ar_expr *value = ar_context_slot_get_value(sl);
	  ar_lustre_domain *dom = s_type_to_lustre_domain(C,type);
	  ar_lustre_expr *cst = ar_lustre_expr_crt_abstract_cst(name,dom);

	  ccl_assert( ! ar_idtable_has(C->constants,name) );

	  ar_idtable_put(C->constants,name,cst);

	  ccl_log(C->out,"const ");
	  s_ID2LUSTRE(C->out,name);
	  if( value == NULL )
	    {
	      ccl_log(C->out," : ");
	      ar_lustre_domain_log(C->out,dom);
	    }
	  else
	    {
	      ar_lustre_expr *val = s_translate_formula(C,value);
	      ccl_log(C->out," = ");
	      ar_lustre_expr_log(C->out,val);	      
	      ar_expr_del_reference(value);
	      ar_lustre_expr_del_reference(val);
	    }
	  ccl_log(C->out,";\n");

	  ar_lustre_expr_del_reference(cst);
	  ar_lustre_domain_del_reference(dom);
	  ar_type_del_reference(type);
	  ar_identifier_del_reference(name);
	  ar_context_slot_del_reference(sl);
	}
      ccl_log(C->out,"\n");
    }
  ccl_list_delete(constants);
}

			/* --------------- */

static int
s_init_table_are_equal(const ar_idtable *idt1, const ar_idtable *idt2)
{
  int result;
  ar_identifier_iterator *i;

  if( ar_idtable_get_size(idt1) != ar_idtable_get_size(idt2) )
    return 0;

  result = 1;
  i = ar_idtable_get_keys(idt1);
  while( result && ccl_iterator_has_more_elements(i) )
    {
      ar_identifier *id = ccl_iterator_next_element(i);
      ar_expr *v1 = ar_idtable_get(idt1,id);
      ar_expr *v2 = ar_idtable_get(idt2,id);

      result = ar_expr_equals(v1,v2);

      ar_expr_del_reference(v1);
      ccl_zdelete(ar_expr_del_reference,v2);
      ar_identifier_del_reference(id);
    }
  ccl_iterator_delete(i);

  return result;
}

			/* --------------- */

static int
s_build_and_compile_node(TRANSLATOR *C, ar_identifier *name, ar_node *node,
			 int *pindex)
{
  int i, k;
  int *sub = NULL;
  ar_identifier **subnames = NULL;
  int nb_sub = 0;
  ccl_pair *p;
  ccl_list *l;
  COMPONENT *self;
  ar_idtable *local_init;
  ar_context_slot_iterator *it;

  l = ar_node_get_initialized_slots (node);
  for(p = FIRST(l); p; p = CDR(p))
    {
      ar_context_slot *sl = (ar_context_slot *)CAR(p);
      ar_identifier *varid = ar_context_slot_get_name(sl);
      ar_identifier *inst = ar_identifier_add_prefix(varid,name);
      ar_expr *value = ar_node_get_initial_value (node, sl);
      
      if( ! ar_idtable_has(C->init,inst) )
	ar_idtable_put(C->init,inst,value);
      ar_identifier_del_reference(varid);
      ar_identifier_del_reference(inst);
      ar_expr_del_reference (value);
    }

  local_init = ar_idtable_create(1, (ccl_duplicate_func *)ar_expr_add_reference,
				 (ccl_delete_proc *)ar_expr_del_reference);
  
  {
    ccl_pair *p;
    ar_context *ctx = ar_node_get_context(node);
    ccl_list *slots = ccl_list_create();

    it = ar_node_get_slots_for_variables(node);
    while( ccl_iterator_has_more_elements(it) )
      {
	ar_context_slot *sl = ccl_iterator_next_element(it);
	int flags = ar_context_slot_get_flags(sl);
	if( (flags & AR_SLOT_FLAG_STATE_VAR) )
	  ar_context_expand_composite_slot(ctx,sl,slots);
	ar_context_slot_del_reference(sl);
      }
    ccl_iterator_delete(it);
    for(p = FIRST(slots); p; p = CDR(p))
      {
	ar_context_slot *sl = CAR(p);
	ar_identifier *varid = ar_context_slot_get_name(sl);
	ar_identifier *inst = ar_identifier_add_prefix(varid,name);
	ar_expr *value = (ar_expr *)ar_idtable_get(C->init,inst);
	    
	if( value != NULL )
	  {
	    ar_idtable_put(local_init,varid,value);
	    ar_expr_del_reference(value);
	  }
	ar_identifier_del_reference(varid);
	ar_identifier_del_reference(inst);
	ar_context_slot_del_reference(sl);
      }
    ccl_list_delete(slots);
    ar_context_del_reference(ctx);
  }

  if( ! ar_node_is_leaf(node) )
    {
      ar_identifier_iterator *nit = ar_node_get_names_of_subnodes(node);
      nb_sub = ar_node_get_nb_subnodes(node);
      sub = ccl_new_array(int,nb_sub+1);
      subnames = ccl_new_array(ar_identifier *,nb_sub+1);
      i = 0;

      while (ccl_iterator_has_more_elements (nit))
	{
	  COMPONENT *subcomp;
	  ar_identifier *subnodename = ccl_iterator_next_element (nit);
	  ar_node *subnode = ar_node_get_subnode_model (node, subnodename);

	  ar_identifier *subname = ar_identifier_add_prefix(subnodename,name);
	  ar_identifier_iterator *itid;

	  sub[i] = s_build_and_compile_node(C,subname,subnode,pindex);
	  subnames[i] = ar_identifier_add_reference(subnodename);
	  subcomp = C->components+sub[i];
	  i++;

	  if( subcomp->reference >= 0)
	    subcomp = C->components+subcomp->reference;

	  itid = ar_idtable_get_keys(subcomp->init);
	  while( ccl_iterator_has_more_elements(itid) )
	    {
	      ar_identifier *id = ccl_iterator_next_element(itid);
	      ar_identifier *local_id = ar_identifier_add_prefix(id,
								 subnodename);
	      ar_expr *value = (ar_expr *)
		ar_idtable_get(subcomp->init,id);
	      ar_idtable_put(local_init,local_id,value);
	      ar_identifier_del_reference(id);
	      ar_identifier_del_reference(local_id);
	      ar_expr_del_reference(value);
	    }
	  ccl_iterator_delete(itid);
	  ar_node_del_reference(subnode);
	  ar_identifier_del_reference(subnodename);
	  ar_identifier_del_reference(subname);
	}
      ccl_iterator_delete(nit);
      ccl_assert( i == nb_sub );

      sub[i] = -1;
    }

  self = C->components+*pindex;
  self->index = *pindex;
  self->node = ar_node_add_reference(node);
  self->nb_sub = nb_sub;
  self->sub = sub;
  self->subnames = subnames;
  self->reference = -1;

  {
    int done = 0;

    ar_identifier *modelname = ar_node_get_name(node);
    for(i = 0, k = 0; i < *pindex && ! done; i++)
      {
	ar_identifier *nname = ar_node_get_name(C->components[i].node);

	if( C->components[i].node == node || modelname == nname )
	  {
	    k++;
	    if( C->components[i].init != NULL && 
		s_init_table_are_equal(C->components[i].init,local_init) ) 
	      done = 1;
	  }
	ar_identifier_del_reference(nname);
	if( done )
	  break;
      }
    ar_identifier_del_reference(modelname);
  }

  if( i == *pindex )
    {
      ar_identifier *nodename = ar_node_get_name(node);
      self->name = s_ID2LUSTRE_STR(nodename);

      if( k > 0 ) 
	{
	  char *tmp = ccl_string_format_new("%s_%d",self->name,k);
	  ccl_string_delete(self->name);
	  self->name = tmp;
	}
      ar_identifier_del_reference(nodename);

      self->init = ar_idtable_add_reference(local_init);      
      self->comments = ccl_list_create();
      self->assigned = ccl_list_create();
      self->to_assign = ccl_list_create();
      self->assignments = ccl_list_create();
      self->assertions = ccl_list_create();
      self->parameters = ccl_list_create();
      self->local_vars = ccl_list_create();
      self->parent_inputs = ccl_list_create();
      self->public_inputs = ccl_list_create();
      self->parent_outputs = ccl_list_create();
      self->public_outputs = ccl_list_create();
      self->event_interface = ar_idtable_create(1, NULL,NULL);
      self->event_assignments = ar_idtable_create(1, NULL,NULL);

      s_compile_component(C,i);
    }
  else
    {
      self->reference = i;
    }
  
  ar_idtable_del_reference(local_init);
  (*pindex)++;

  return self->index;
}

			/* --------------- */

static void
s_build_parameter_lists(TRANSLATOR *C, int comp);

static void
s_build_flow_lists(TRANSLATOR *C, int comp);

static void
s_build_event_list_for_leaf(TRANSLATOR *C, int comp);

static void
s_build_synchros(TRANSLATOR *C, int comp);

static void
s_build_subnodes_calls(TRANSLATOR *C, int comp);

static void
s_build_transitions(TRANSLATOR *C, int comp);

static void
s_build_assignments(TRANSLATOR *C, int comp);

			/* --------------- */

static void
s_compile_component(TRANSLATOR *C, int comp)
{
  ccl_pair *p;
  COMPONENT *self = C->components+comp;
  ar_node *node = self->node;

  {
    ar_event_poset *poset = ar_node_get_event_poset(node);
    if( ! ar_event_poset_is_empty(poset) && (C->flags & AR_TO_LUSTRE_WARNING) )
      ccl_warning("A2L: warning: the node '%s' uses an envet priorities.\n",
		  self->name);
    ar_event_poset_del_reference(poset);
  }

  s_build_parameter_lists(C,comp);
  s_build_flow_lists(C,comp);
  if( ar_node_is_leaf(node) )
    s_build_event_list_for_leaf(C,comp);
  else
    {
      s_build_synchros(C,comp);
      s_build_subnodes_calls(C,comp);
    }
  s_build_transitions(C,comp);
  s_build_assignments(C,comp);

  for(p = FIRST(self->assigned); p; p = CDR(p))
    {
      int pos = ccl_list_get_index(self->to_assign,CAR(p),s_cmp_variables);

      if( pos >= 0 )
	{
	  void *o = ccl_list_get_at(self->to_assign,pos);
	  ccl_list_remove(self->to_assign,o);
	  ar_lustre_expr_del_reference(o);
	}
    }

  if( ccl_list_is_empty(self->to_assign) )
    return;

  for(p = FIRST(self->to_assign); p; p = CDR(p))
    {
      ar_identifier *name = ar_lustre_expr_get_name(CAR(p));
      char *varname = s_ID2LUSTRE_STR(name);
      char *msg = 
	ccl_string_format_new("WARNING : output/local variable '%s' "
			      "in node '%s' is not defined.",varname,
			      self->name);
      ccl_list_add(self->comments,msg);
      ccl_string_delete(varname);
      ar_identifier_del_reference(name);

      if( (C->flags & AR_TO_LUSTRE_WARNING) )
	ccl_warning("%s\n",msg);
    }
}

			/* --------------- */

static ar_lustre_expr *
s_instanciate_slot(ar_lustre_expr *var, ar_identifier *nodename)
{
  ar_identifier *name = ar_lustre_expr_get_name(var);
  ar_lustre_domain *dom = ar_lustre_expr_get_domain(var);
  ar_identifier *instname = ar_identifier_add_prefix(name,nodename);
  ar_lustre_expr *result = ar_lustre_expr_crt_variable(instname,dom);

  ar_identifier_del_reference(instname);
  ar_lustre_domain_del_reference(dom);
  ar_identifier_del_reference(name);

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_crt_noreturn_var(TRANSLATOR *C)
{
  ar_identifier *noreturn = ar_identifier_create(C->noreturn);
  ar_lustre_domain *dom = ar_lustre_domain_crt_bool();
  ar_lustre_expr *var = ar_lustre_expr_crt_variable(noreturn,dom);

  ar_lustre_domain_del_reference(dom);
  ar_identifier_del_reference(noreturn);

  return var;
}

			/* --------------- */

static void
s_build_parameter_lists(TRANSLATOR *C, int comp)
{
  ccl_pair *p;
  COMPONENT *self = C->components+comp;
  ar_context *ctx = ar_node_get_context(self->node);
  ccl_list *slots = ccl_list_create();
  ar_context_slot_iterator *it = ar_node_get_slots_for_parameters(self->node);

  while( ccl_iterator_has_more_elements(it) )
    {
      ar_context_slot *sl = ccl_iterator_next_element(it);
      if( ! ar_context_slot_has_value(sl) )
	slots = ar_context_expand_composite_slot(ctx,sl,slots);
      ar_context_slot_del_reference(sl);
    }
  ccl_iterator_delete(it);


  for(p = FIRST(slots); p; p = CDR(p))
    {
      ar_context_slot *sl = CAR(p);
      ar_lustre_expr *param = s_translate_slot(C,sl);
      ar_lustre_expr *constraint = s_translate_domain_constraint(C,sl);

      ccl_list_add(self->parameters,param);

      if( constraint != NULL )
	ccl_list_add(self->assertions,constraint);

      ar_context_slot_del_reference(sl);
    }

  ccl_list_delete(slots);
  ar_context_del_reference(ctx);

  if( ! ar_node_is_leaf(self->node) )
    {
      int i = 0;
      it = ar_node_get_slots_for_subnodes(self->node);
      while( ccl_iterator_has_more_elements(it) )
	{
	  ccl_pair *p;
	  ar_context_slot *sl = ccl_iterator_next_element(it);
	  ar_identifier *name = ar_context_slot_get_name(sl);
	  COMPONENT *subnode = s_get_sub(C,self,i++);
	  
	  for(p = FIRST(subnode->parameters); p; p = CDR(p))
	    {
	      ar_lustre_expr *newparam = s_instanciate_slot(CAR(p),name);
	      ccl_list_add(self->parameters,newparam);
	    }
	  
	  ar_identifier_del_reference(name);
	  ar_context_slot_del_reference(sl);
	}
      ccl_iterator_delete(it);
    }  
}

			/* --------------- */

static void
s_build_flow_lists(TRANSLATOR *C, int comp)
{
  ccl_pair *p;
  COMPONENT *self = C->components+comp;
  ar_context *ctx = ar_node_get_context(self->node);
  ccl_list *slots = ccl_list_create();
  ar_context_slot_iterator *it = ar_node_get_slots_for_variables(self->node);

  while( ccl_iterator_has_more_elements(it) )
    {
      ar_context_slot *sl = ccl_iterator_next_element(it);
      slots = ar_context_expand_composite_slot(ctx,sl,slots);
      ar_context_slot_del_reference(sl);
    }
  ccl_iterator_delete(it);


  for(p = FIRST(slots); p; p = CDR(p))
    {
      ccl_list *varlist;
      ar_context_slot *sl = CAR(p);
      ar_lustre_expr *var = s_translate_slot(C,sl);
      ar_lustre_expr *constraint = s_translate_domain_constraint(C,sl);
      int flags = ar_context_slot_get_flags(sl);
      int is_input = (flags & AR_SLOT_FLAG_IN)
	&& ! (flags & (AR_SLOT_FLAG_STATE_VAR|AR_SLOT_FLAG_PRIVATE));
      
      if( (flags & AR_SLOT_FLAG_PUBLIC) )
	varlist = is_input?self->public_inputs:self->public_outputs;
      else if( (flags & AR_SLOT_FLAG_PARENT) )
	varlist = is_input?self->parent_inputs:self->parent_outputs;
      else 
	{
	  ccl_assert( (flags & AR_SLOT_FLAG_PRIVATE) );
	  varlist = self->local_vars;
	}

      ccl_list_add(varlist,var);

      if( ! is_input )
	ccl_list_add(self->to_assign,ar_lustre_expr_add_reference(var));

      if( constraint != NULL )
	ccl_list_add(self->assertions,constraint);

      ar_context_slot_del_reference(sl);
    }

  ccl_list_delete(slots);
  ar_context_del_reference(ctx);

  if( ! ar_node_is_leaf(self->node) )
    {
      int i = 0;
      it = ar_node_get_slots_for_subnodes(self->node);
      while( ccl_iterator_has_more_elements(it) )
	{
	  int k;
	  ccl_pair *p;
	  ar_context_slot *sl = ccl_iterator_next_element(it);
	  ar_identifier *name = ar_context_slot_get_name(sl);
	  COMPONENT *subnode = s_get_sub(C,self,i++);
	  
	  for(p = FIRST(subnode->public_inputs); p; p = CDR(p))
	    {
	      ar_lustre_expr *newvar = s_instanciate_slot(CAR(p),name);
	      ccl_list_add(self->public_inputs,newvar);
	    }
	  
	  for(p = FIRST(subnode->public_outputs); p; p = CDR(p))
	    {
	      ar_lustre_expr *newvar = s_instanciate_slot(CAR(p),name);
	      newvar = ar_lustre_expr_add_reference(newvar);
	      ccl_list_add(self->public_outputs,newvar);
	      ccl_list_add(self->to_assign,newvar);
	    }
	  
	  for(k = 0; k < 2; k++)
	    {
	      ccl_list *l = (k == 0)
		?subnode->parent_inputs
		:subnode->parent_outputs;
	  
	      for(p = FIRST(l); p; p = CDR(p))
		{
		  ar_lustre_expr *newvar = s_instanciate_slot(CAR(p),name);
		  newvar = ar_lustre_expr_add_reference(newvar);
		  ccl_list_add(self->local_vars,newvar);
		  ccl_list_add(self->to_assign,newvar);
		}
	    }

	  if( ccl_list_get_size(subnode->parent_outputs)+
	      ccl_list_get_size(subnode->public_outputs) == 0 )
	    {
	      ar_lustre_expr *noreturn = s_crt_noreturn_var(C);
	      ar_lustre_expr *instnoreturn = 
		s_instanciate_slot(noreturn,name);
	      ccl_list_add(self->local_vars,instnoreturn);
	      ar_lustre_expr_del_reference(noreturn);
	    }

	  ar_identifier_del_reference(name);
	  ar_context_slot_del_reference(sl);
	}
      ccl_iterator_delete(it);
    }  

  if( ccl_list_get_size(self->parent_outputs)+
      ccl_list_get_size(self->public_outputs) == 0 )
    {
      ar_lustre_expr *noreturn = s_crt_noreturn_var(C);
      ar_lustre_expr *ctrue = ar_lustre_expr_crt_true();
      ar_lustre_expr *assign = ar_lustre_expr_crt_eq(noreturn,ctrue);
      
      ccl_list_add(self->assignments,assign);
      ar_lustre_expr_del_reference(noreturn);
      ar_lustre_expr_del_reference(ctrue);
    }  
}

			/* --------------- */
#if 0
static char *
s_event_to_string(ar_identifier *id, const char *suf)
{
  
  char *result = NULL;
  ccl_pair *p;
  ccl_list *l;
  char *c;

  l = ar_identifier_get_path(id);
  for(p = FIRST(l); p; p = CDR(p))
    {
      if( result == NULL ) result = ccl_string_dup(CAR(p));
      else ccl_string_format_append(&result,"_%s",CAR(p));
    }
  ccl_string_format_append(&result,"%s",suf);

  for(c = result; *c; c++) 
    {
      if( ! (('a' <= *c && *c <= 'z') ||
	     ('A' <= *c && *c <= 'Z') ||
	     ('0' <= *c && *c <= '9') ||
	     ( *c == '_') ) )
	{
	  *c = '_';
	}
    }

  return result;
}
#endif
			/* --------------- */

static void
s_build_event_list_for_leaf(TRANSLATOR *C, int comp)
{
  COMPONENT *self = C->components+comp;
  ar_node *node = self->node;
  ar_identifier_iterator *event_ids = ar_node_get_event_identifiers(node);

  while( ccl_iterator_has_more_elements(event_ids) )
    {
      ar_identifier *eid = ccl_iterator_next_element(event_ids);      
      ccl_list *l = ccl_list_create();
      if( eid != AR_EPSILON_ID )
	{
	  ar_identifier *ename = 
	    ar_identifier_rename_with_suffix(eid,C->event_var_suffix);
	  ccl_list_add(l,ename);
	}
      ar_idtable_put(self->event_interface,eid,l);
      ar_identifier_del_reference(eid);
    }
  ccl_iterator_delete(event_ids);
}

			/* --------------- */

typedef struct vector_component_st vector_component;
struct vector_component_st {
  ar_identifier *ename;
  vector_component *next;
};

static void
s_generate_vectors(TRANSLATOR *C, int comp, ar_broadcast *v, int cur, 
		  vector_component *next)
{
  COMPONENT *self = C->components+comp;
  vector_component component;

  component.next = next;

  if( cur == self->nb_sub )
    {
      vector_component *c;
      ar_identifier *event = NULL;
      ccl_list *interface;
      ar_identifier *id = ar_broadcast_get_event(v,AR_EMPTY_ID);

      if( id == NULL )
	id = ar_identifier_add_reference(AR_EPSILON_ID);

      interface = (ccl_list *)ar_idtable_get(self->event_interface,id);
      if( interface == NULL )
	{
	  interface = ccl_list_create();
	  ar_idtable_put(self->event_interface,id,interface);
	}

      component.ename = ( id != AR_EPSILON_ID )
	? ar_identifier_rename_with_suffix(id,C->event_var_suffix)
	: NULL;
      
      for(c = &component; c != NULL; c = c->next)
	{
	  if( c->ename == NULL ) 
	    continue;

	  if( event == NULL )
	    event = ar_identifier_add_reference(c->ename);
	  else
	    {
	      char *s1 = ar_identifier_to_string(event);
	      char *s2 = ar_identifier_to_string(c->ename);

	      if( ! ccl_string_equals(s1,s2) )
		{
		  ar_identifier *tmp =
		    ar_identifier_add_prefix(c->ename,event);
		  ar_identifier_del_reference(event);
		  event = tmp;
		}
	      ccl_string_delete(s1);
	      ccl_string_delete(s2);
	    }
	}

      if( event != NULL )
	{
	  if( ! ccl_list_has(interface,event) )
	    ccl_list_add(interface,ar_identifier_add_reference(event));
	  
	  for(c = &component; c!= NULL; c = c->next)
	    {
	      ccl_list *a;

	      if( c->ename == NULL ) 
		continue;
	      
	      a = (ccl_list *)ar_idtable_get(self->event_assignments,c->ename);

	      if( a == NULL )
		{
		  a = ccl_list_create();
		  ar_idtable_put(self->event_assignments,c->ename,a);
		}

	      if( ! ccl_list_has(a,event) )
		ccl_list_add(a,ar_identifier_add_reference(event));
	    }
	}

      ar_identifier_del_reference(id);
      ccl_zdelete(ar_identifier_del_reference,component.ename);
      ccl_zdelete(ar_identifier_del_reference,event);
    }
  else
    {
      COMPONENT *sub = s_get_sub(C,self,cur);
      ar_identifier *id = ar_broadcast_get_event(v,self->subnames[cur]);
      ar_identifier *tmp = id != NULL
	?ar_identifier_get_name(id)
	:ar_identifier_add_reference(AR_EPSILON_ID);
      ccl_list *interface = (ccl_list *)
	ar_idtable_get(sub->event_interface,tmp);
      ccl_pair *p = FIRST(interface);

      if( p == NULL )
	{
	  component.ename = NULL;
	  s_generate_vectors(C,comp,v,cur+1,&component);
	}
      else 
	{
	  for(; p; p = CDR(p))
	    {
	      component.ename = 
		ar_identifier_add_prefix(CAR(p),self->subnames[cur]);
	      s_generate_vectors(C,comp,v,cur+1,&component);
	      ar_identifier_del_reference(component.ename);
	    }

	  if( AR_EPSILON_ID == tmp )
	    {
	      component.ename = NULL;
	      s_generate_vectors(C,comp,v,cur+1,&component);
	    }
	}

      ccl_zdelete(ar_identifier_del_reference,id);
      ar_identifier_del_reference(tmp);
    }
}

			/* --------------- */

static void
s_build_synchros(TRANSLATOR *C, int comp)
{
  COMPONENT *self = C->components+comp;
  ar_node *node = self->node;
  ccl_list *vectors;
  ccl_pair *p;
  ar_identifier_iterator *it;

  ccl_pre( ! ar_node_is_leaf(node) );

  vectors = ar_node_get_broadcasts(node);
  for(p = FIRST(vectors); p; p = CDR(p))
    {
      ar_broadcast *bv = (ar_broadcast *)CAR(p);
      s_generate_vectors(C,comp,bv,0,NULL);
    }

  {
    ar_idtable *new_assignments = ar_idtable_create(0, NULL,NULL);
    
    it = ar_idtable_get_keys(self->event_assignments);
    while( ccl_iterator_has_more_elements(it) )
      {
	ar_identifier *id = ccl_iterator_next_element(it);
	ccl_list *l = ar_idtable_get(self->event_assignments,id);
	ar_lustre_expr *value = NULL;

	for(p = FIRST(l); p; p = CDR(p))
	  {
	    ar_identifier *ename = CAR(p);
	    ar_lustre_expr *evar = 
	      ar_lustre_expr_crt_variable(ename,C->bool_dom);	  

	    if( value == NULL ) 
	      value = ar_lustre_expr_add_reference(evar);
	    else
	      {
		ar_lustre_expr *tmp = ar_lustre_expr_crt_or(evar,value);
		ar_lustre_expr_del_reference(value);
		value = tmp;
	      }
	    ar_lustre_expr_del_reference(evar);
	    ar_identifier_del_reference(ename);
	  }

	ccl_assert( value != NULL );

	ar_idtable_put(new_assignments,id,value);

	ccl_list_delete(l);
	ar_identifier_del_reference(id);
      }
    ccl_iterator_delete(it);
    ar_idtable_del_reference(self->event_assignments);
    self->event_assignments = new_assignments;
  }
}

			/* --------------- */

static void
s_build_subnodes_calls(TRANSLATOR *C, int comp)
{
  int i;
  COMPONENT *self = C->components+comp;

  if( self->sub == NULL )
    return;

  for(i = 0; i < self->nb_sub; i++)
    {
      ccl_pair *p;
      ar_lustre_expr *left;
      ar_lustre_expr *right;
      ar_lustre_expr *assignment;
      COMPONENT *sub = s_get_sub(C,self,i);
      ccl_list *inputs = ccl_list_create();
      ccl_list *outputs = ccl_list_create();
      ar_identifier_iterator *it;
      ccl_list *output_domains = ccl_list_create();
      ar_lustre_domain *call_out_domain;

      ccl_list_append(outputs,sub->public_outputs);
      ccl_list_append(outputs,sub->parent_outputs);

      if( ccl_list_is_empty(outputs) )
	{
	  ar_lustre_expr *tmp = s_crt_noreturn_var(C);
	  left = s_instanciate_slot(tmp,self->subnames[i]);
	  ccl_list_add(output_domains,
		       ar_lustre_domain_add_reference(C->bool_dom));
	  ccl_list_add(self->assigned,ar_lustre_expr_add_reference(left));
	  ar_lustre_expr_del_reference(tmp);
	}
      else if( ccl_list_get_size(outputs) == 1 )
	{
	  ar_lustre_expr *var = CAR(FIRST(outputs));
	  ccl_list_add(output_domains,ar_lustre_expr_get_domain(var));
	  left = s_instanciate_slot(var,self->subnames[i]);
	  ccl_list_add(self->assigned,ar_lustre_expr_add_reference(left));
	}
      else
	{
	  ccl_list_sort(outputs,s_cmp_variables);
	  for(p = FIRST(outputs); p; p = CDR(p))
	    {
	      ar_lustre_expr *var = CAR(p);
	      CAR(p) = s_instanciate_slot(var,self->subnames[i]);
	      ccl_list_add(output_domains,ar_lustre_expr_get_domain(var));
	      ccl_list_add(self->assigned,
			   ar_lustre_expr_add_reference(CAR(p)));
	    }
	  left = ar_lustre_expr_crt_expr_list(outputs);
	  ccl_list_clear(outputs,(ccl_delete_proc *)
			 ar_lustre_expr_del_reference);
	}
      ccl_list_delete(outputs);
      call_out_domain = ar_lustre_domain_crt_composite(output_domains);
      ccl_list_clear_and_delete(output_domains,(ccl_delete_proc *)
				ar_lustre_domain_del_reference);

      ccl_list_append(inputs,sub->public_inputs);
      ccl_list_append(inputs,sub->parent_inputs);
      for(p = FIRST(inputs); p; p = CDR(p))
	CAR(p) = s_instanciate_slot(CAR(p),self->subnames[i]);

      it = ar_idtable_get_keys(sub->event_interface);
      while( ccl_iterator_has_more_elements(it) )
	{
	  ar_identifier *id = ccl_iterator_next_element(it);	  
	  ccl_list *l = ar_idtable_get(sub->event_interface,id);

	  if( id != AR_EPSILON_ID || ! ccl_list_is_empty(l) )
	    {
	      for(p = FIRST(l); p; p = CDR(p))
		{
		  ar_identifier *e = CAR(p);
		  ar_lustre_expr *ve = 
		    ar_lustre_expr_crt_variable(e,C->bool_dom);
		  ar_lustre_expr *ive = 
		    s_instanciate_slot(ve,self->subnames[i]);

		  ccl_list_add(inputs,ive);
		  ar_lustre_expr_del_reference(ve);
		}
	    }
	  ar_identifier_del_reference(id);
	}
      ccl_iterator_delete(it);

      if( ccl_list_is_empty(inputs) )
	{
	  ccl_list_add(inputs,ar_lustre_expr_crt_true());
	}
      else
	{
	  ccl_list_sort(inputs,s_cmp_variables);

	  for(p = FIRST(inputs); p; p = CDR(p))
	    {
	      ar_lustre_expr *var = CAR(p);
	      ar_identifier *vname = ar_lustre_expr_get_name(var);
	      ar_lustre_expr *val = 
		ar_idtable_get(self->event_assignments,vname);

	      if( val != NULL )
		{
		  CAR(p) = ar_lustre_expr_add_reference(val);
		  ar_lustre_expr_del_reference(var);
		}
	      ar_identifier_del_reference(vname);	      
	    }
	}

      if( ! ccl_list_is_empty(sub->parameters) )
	{
	  ccl_list *l = ccl_list_create();
	  for(p = FIRST(sub->parameters); p; p = CDR(p))
	    {
	      ar_lustre_expr *par = 
		s_instanciate_slot(CAR(p),self->subnames[i]);
	      ccl_list_add(l,par);
	    }
	  ccl_list_sort(l,s_cmp_variables);
	  ccl_list_append(l,inputs);
	  ccl_list_delete(inputs);
	  inputs = l;
	}

      {
	ar_identifier *nodename = ar_identifier_create(sub->name);
	right = ar_lustre_expr_crt_node_call(nodename,call_out_domain,
					     inputs);
	ar_identifier_del_reference(nodename);
      }
      ccl_list_clear_and_delete(inputs,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ar_lustre_domain_del_reference(call_out_domain);
      
      assignment = ar_lustre_expr_crt_eq(left,right);
      ar_lustre_expr_del_reference(left);
      ar_lustre_expr_del_reference(right);
      ccl_list_add(self->assignments,assignment);
    }
}

			/* --------------- */

static ar_lustre_expr *
s_crt_guard_var(TRANSLATOR *C, int gt)
{
  char *vname = gt >= 0
    ? ccl_string_format_new("%s%d",C->guard_var_prefix,gt)
    : ccl_string_format_new("%sepsilon",C->guard_var_prefix);
  ar_identifier *vid = ar_identifier_create(vname);
  ar_lustre_expr *result = ar_lustre_expr_crt_variable(vid,C->bool_dom);

  ar_identifier_del_reference(vid);
  ccl_string_delete(vname);

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_add_guard_var(TRANSLATOR *C, int comp, int gt)
{
  COMPONENT *self = C->components+comp;
  ar_lustre_expr *result = s_crt_guard_var(C,gt);

  ccl_list_add(self->local_vars,ar_lustre_expr_add_reference(result));
  ccl_list_add(self->assigned,ar_lustre_expr_add_reference(result));
  ccl_list_add(self->to_assign,ar_lustre_expr_add_reference(result));

  return result;
}

			/* --------------- */

static void
s_build_guards(TRANSLATOR *C, int comp)
{
  ccl_pair *p;
  COMPONENT *self = C->components+comp;
  ar_node *node = self->node;
  ar_context *ctx;
  ccl_list *trans = ar_node_get_transitions(node);
  int gt = 0;
  ccl_list *event_conditions;
  ar_idtable *implications = NULL;

  if( ccl_list_get_size(trans) <= 1 )
    return;

  ctx = ar_node_get_context(node);
  event_conditions = ccl_list_create();
  implications = ar_idtable_create(0, NULL,NULL);

  for(p = FIRST(trans); p; p = CDR(p), gt++)
    {
      ar_trans *t = (ar_trans *)CAR(p);
      ar_event *e = ar_trans_get_event(t);
      ar_identifier *eid = ar_event_get_name(e);
      ar_expr *G;
      ar_lustre_expr *Gvar;
      ar_lustre_expr *value;
      ar_lustre_expr *eventcond;
      ar_lustre_expr *assignment;
      ccl_list *guards;

      ar_event_del_reference(e);

      if( eid == AR_EPSILON_ID )
	{
	  ar_identifier_del_reference(eid);
	  continue;
	}

      {
	ar_expr *tmp = ar_trans_get_guard(t);
	G = ar_expr_remove_composite_slots(tmp,ctx);	
	ar_expr_del_reference(tmp);
      }
      
      Gvar = s_add_guard_var(C,comp,gt);
      ccl_list_add(event_conditions,ar_lustre_expr_add_reference(Gvar));

      value = ar_lustre_expr_crt_false();

      {
	ar_identifier *ename = 
	  ar_identifier_rename_with_suffix(eid,C->event_var_suffix);
	ar_identifier_del_reference(eid);
	eid = ename;
      }

      if( (guards = ar_idtable_get(implications,eid)) == NULL)
	{
	  guards = ccl_list_create();
	  ar_idtable_put(implications,eid,guards);
	}
      
      if( ar_node_is_leaf(node) )
	eventcond = ar_lustre_expr_crt_variable(eid,C->bool_dom);
      else
	{
	  eventcond = ar_idtable_get(self->event_assignments,eid);
	  eventcond = ar_lustre_expr_add_reference(eventcond);
	}
      ar_identifier_del_reference(eid);

	{
	  ar_lustre_expr *lG = s_translate_formula(C,G);
	  ar_lustre_expr *preG = ar_lustre_expr_crt_pre(lG);
	  ar_lustre_expr *preG_and_event = 
	    ar_lustre_expr_crt_and(preG,eventcond);
	  ar_lustre_expr *tmp = 
	    ar_lustre_expr_crt_followed(value,preG_and_event);
	  ar_lustre_expr_del_reference(value);
	  value = tmp;

	  ccl_list_add(guards,ar_lustre_expr_add_reference(lG));

	  ar_lustre_expr_del_reference(lG);
	  ar_lustre_expr_del_reference(preG);
	  ar_lustre_expr_del_reference(preG_and_event);
	}

      ar_expr_del_reference(G);
      ar_lustre_expr_del_reference(eventcond);

      assignment = ar_lustre_expr_crt_eq(Gvar,value);
      ar_lustre_expr_del_reference(value);
      ar_lustre_expr_del_reference(Gvar);

      ccl_list_add(self->assignments,assignment);
    }
  
  ar_context_del_reference(ctx);

  {
    ar_identifier_iterator *ii = ar_idtable_get_keys(implications);
    
    while( ccl_iterator_has_more_elements(ii) )
      {
	ar_identifier *eid = ccl_iterator_next_element(ii);
	ccl_list *guards = ar_idtable_get(implications,eid);

	ccl_assert( ! ccl_list_is_empty(guards) );
	{
	  ccl_pair *p;
	  ar_lustre_expr *eventcond;
	  ar_lustre_expr *init = ar_lustre_expr_crt_false();
	  ar_lustre_expr *next = CAR(FIRST(guards));
	  ar_lustre_expr *init_next;
	  ar_lustre_expr *constraint;

	  if( ar_node_is_leaf(node) )
	    eventcond = ar_lustre_expr_crt_variable(eid,C->bool_dom);
	  else
	    {
	      eventcond = ar_idtable_get(self->event_assignments,eid);
	      eventcond = ar_lustre_expr_add_reference(eventcond);
	    }

	  for(p = CDR(FIRST(guards)); p; p = CDR(p))
	    {
	      ar_lustre_expr *tmp = ar_lustre_expr_crt_or(next,CAR(p));
	      ar_lustre_expr_del_reference(next);
	      ar_lustre_expr_del_reference(CAR(p));
	      next = tmp;
	    }
	    
	  {
	    ar_lustre_expr *tmp = ar_lustre_expr_crt_pre(next);
	    ar_lustre_expr_del_reference(next);
	    next = tmp;
	  }
	  init_next = ar_lustre_expr_crt_followed(init,next);
	  constraint = ar_lustre_expr_crt_imply(eventcond,init_next);
	  ccl_list_add(self->assertions,constraint);
	  ar_lustre_expr_del_reference(next);
	  ar_lustre_expr_del_reference(init_next);
	  ar_lustre_expr_del_reference(init);
	  ar_lustre_expr_del_reference(eventcond);
	  }

	ar_identifier_del_reference(eid);	
	ccl_list_delete(guards);
      }
    ccl_iterator_delete(ii);
    ar_idtable_del_reference(implications);
  }

  if( (C->flags & AR_TO_LUSTRE_DETERMINISTIC) && 
      ccl_list_get_size(event_conditions) >= 2 )
    {
      ar_lustre_expr *determinism_constraint = 
	ar_lustre_expr_crt_atmost(event_conditions);
      ccl_list_add(self->assertions,determinism_constraint);
    }

  ccl_list_clear_and_delete(event_conditions,(ccl_delete_proc *)
			    ar_lustre_expr_del_reference);

}

			/* --------------- */

static ar_lustre_expr *
s_add_missing_initial_value_for_slot(TRANSLATOR *C, int comp, 
				     ar_context_slot *sl)
{
  COMPONENT *self = C->components+comp;
  ar_type *type = ar_context_slot_get_type(sl);
  ar_lustre_domain *dom = s_translate_domain(C,type);
  ar_identifier *sl_name = ar_context_slot_get_name(sl);
  char *sl_str_name = ar_identifier_to_string(sl_name);
  char *missing_name = 
    ccl_string_format_new("%s%s",C->missing_init_prefix,sl_str_name);
  ar_identifier *id = ar_identifier_create(missing_name);
  ar_lustre_expr *result = ar_lustre_expr_crt_variable(id,dom);  

  ccl_list_add(self->parameters,ar_lustre_expr_add_reference(result));

  ar_identifier_del_reference(id);
  ar_identifier_del_reference(sl_name);
  ccl_string_delete(sl_str_name);
  ccl_string_delete(missing_name);
  ar_type_del_reference(type);
  ar_lustre_domain_del_reference(dom);

  return result;
}

			/* --------------- */

static void
s_build_transitions(TRANSLATOR *C, int comp)
{
  ar_lustre_expr *left;
  ar_lustre_expr *init;  
  ar_lustre_expr *next_init;
  COMPONENT *self = C->components+comp;
  ar_node *node = self->node;
  ar_context *ctx = ar_node_get_context(node);
  ccl_list *state_slots = ccl_list_create();
  
  s_build_guards(C,comp);

  {
    ar_context_slot_iterator *sit = ar_node_get_slots_for_variables(node);
    while( ccl_iterator_has_more_elements(sit) )
      {
	ar_context_slot *sl = ccl_iterator_next_element(sit);
	
	if( (ar_context_slot_get_flags(sl) & AR_SLOT_FLAG_STATE_VAR) )
	  state_slots = ar_context_expand_composite_slot(ctx,sl,state_slots);
	ar_context_slot_del_reference(sl);
      }
    ccl_iterator_delete(sit);

    if( ccl_list_is_empty(state_slots) )
      {
	ar_context_del_reference(ctx);
	ccl_list_delete(state_slots);
	return;
      }
  }
  
  { 
    ccl_pair *p;
    ccl_list *left_list = ccl_list_create();
    ccl_list *init_list = ccl_list_create();

    for(p = FIRST(state_slots); p; p = CDR(p))
      {
	ar_lustre_expr *init;
	ar_context_slot *sl = CAR(p);
	ar_identifier *id = ar_context_slot_get_name(sl);
	ar_expr *ainit = ar_idtable_get(self->init,id);
	ar_lustre_expr *var = s_translate_slot(C,sl);

	if( ainit == NULL )
	  init = s_add_missing_initial_value_for_slot(C,comp,sl);
	else
	  init = s_translate_formula(C,ainit);
	ccl_list_add(left_list,ar_lustre_expr_add_reference(var));
	ccl_list_add(self->assigned,ar_lustre_expr_add_reference(var));
	ccl_list_add(init_list,ar_lustre_expr_add_reference(init));
	    
	ar_lustre_expr_del_reference(init);
	ccl_zdelete(ar_expr_del_reference,ainit);
	ar_lustre_expr_del_reference(var);
	ar_identifier_del_reference(id);
      }
    ccl_assert( ccl_list_get_size(left_list) == ccl_list_get_size(init_list) );
    left = ar_lustre_expr_crt_expr_list(left_list);
    ccl_list_clear_and_delete(left_list,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
    init = ar_lustre_expr_crt_expr_list(init_list);
    ccl_list_clear_and_delete(init_list,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
  }

  {
    int gt = 0;
    ccl_pair *p;
    ccl_list *trans = ar_node_get_transitions(node);

    next_init = ar_lustre_expr_crt_pre(left);

    for(p = FIRST(trans); p; p = CDR(p), gt++)
      {
	ccl_pair *psl;
	const ar_trans_assignment *a;
	ar_idtable *alpha;
	ccl_list *tmp;
	ar_event *e = ar_trans_get_event(CAR(p));
	ar_identifier *name = ar_event_get_name(e);
	int is_epsilon = name == AR_EPSILON_ID;

	ar_event_del_reference(e);
	ar_identifier_del_reference(name);
	if( is_epsilon )
	  continue;
	
	a = ar_trans_get_assignments(CAR(p));
	alpha = 
	  ar_idtable_create(0,
			    (ccl_duplicate_func *)ar_lustre_expr_add_reference,
			    (ccl_delete_proc *)ar_lustre_expr_del_reference);
	tmp = ccl_list_create();
	
	for(; a; a = a->next)
	  {
	    ccl_list *sublvalues = 
	      ar_expr_expand_composite_types (a->lvalue, ctx,NULL);
	    ccl_list *subvalues = 
	      ar_expr_expand_composite_types(a->value,ctx, NULL);
	    ccl_pair *psl = FIRST(sublvalues);
	    ccl_pair *psv = FIRST(subvalues);


	    for(; psl != NULL; psl = CDR(psl), psv = CDR(psv))
	      {
		ar_identifier *id = ar_expr_get_name (CAR(psl));
		ar_lustre_expr *val = s_translate_formula(C,CAR(psv));

		ccl_assert( ! ar_idtable_has(alpha,id) );

		ar_idtable_put(alpha,id,val);
		ar_identifier_del_reference(id);
		ar_lustre_expr_del_reference(val);
	      }

	    ccl_assert( psv == NULL );

	    ccl_list_clear_and_delete(sublvalues,(ccl_delete_proc *)
				      ar_expr_del_reference);
	    ccl_list_clear_and_delete(subvalues,(ccl_delete_proc *)
				      ar_expr_del_reference);
	  }

	for(psl = FIRST(state_slots); psl; psl = CDR(psl))
	  {
	    ar_context_slot *sl = CAR(psl);
	    ar_identifier *id = ar_context_slot_get_name(sl);
	    ar_lustre_expr *val = ar_idtable_get(alpha,id);

	    if( val == NULL )
	      val = s_translate_slot(C,sl);

	    ccl_list_add(tmp,val);
	    ar_identifier_del_reference(id);
	  }
	
	{
	  ar_lustre_expr *guard = s_crt_guard_var(C,gt);
	  ar_lustre_expr *then_val = ar_lustre_expr_crt_expr_list(tmp);
	  ar_lustre_expr *pre_then_val = ar_lustre_expr_crt_pre(then_val);
	  ar_lustre_expr *tmpite = ar_lustre_expr_crt_ite(guard,pre_then_val,
							  next_init);
	  ar_lustre_expr_del_reference(guard);
	  ar_lustre_expr_del_reference(then_val);
	  ar_lustre_expr_del_reference(pre_then_val);
	  ar_lustre_expr_del_reference(next_init);
	  next_init = tmpite;
	}

	ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
				  ar_lustre_expr_del_reference);
	ar_idtable_del_reference(alpha);
      }
  }

  {
    ar_lustre_expr *right = ar_lustre_expr_crt_followed(init,next_init);
    ar_lustre_expr *assignment = ar_lustre_expr_crt_eq(left,right);


    ar_lustre_expr_del_reference(next_init);    
    ar_lustre_expr_del_reference(right);    

    ccl_list_add(self->assignments,assignment);
  }

  ar_lustre_expr_del_reference(init);
  ar_lustre_expr_del_reference(left);

  ar_context_del_reference(ctx);
  ccl_list_clear_and_delete(state_slots,(ccl_delete_proc *)
			    ar_context_slot_del_reference);
}

			/* --------------- */

static int
s_is_assignable(TRANSLATOR *C, int comp, ar_expr *e)
{
  int result;

  if( ar_expr_get_op(e) != AR_EXPR_VAR )
    result = 0;
  else
    {
      COMPONENT *self = C->components+comp;
      ar_lustre_expr *v = s_translate_formula(C,e);

      result = ccl_list_get_index(self->assigned,v,s_cmp_variables) < 0
	&& (ccl_list_get_index(self->local_vars,v,s_cmp_variables) >= 0 
	    || ccl_list_get_index(self->public_outputs,v,s_cmp_variables) >= 0 
	    || ccl_list_get_index(self->parent_outputs,v,s_cmp_variables)>= 0);
      ar_lustre_expr_del_reference(v);
    }
  return result;
}

			/* --------------- */

static void
s_add_assignment(TRANSLATOR *C, int comp, ar_expr *v, ar_expr *value)
{
  COMPONENT *self = C->components+comp;
  ar_lustre_expr *lv = s_translate_formula(C,v);
  ar_lustre_expr *lvalue = s_translate_formula(C,value);
  ar_lustre_expr *assign = ar_lustre_expr_crt_eq(lv,lvalue);
  ccl_list_add(self->assigned,ar_lustre_expr_add_reference(lv));
  ccl_list_add(self->assignments,ar_lustre_expr_add_reference(assign));
  ar_lustre_expr_del_reference(lv);
  ar_lustre_expr_del_reference(lvalue);
  ar_lustre_expr_del_reference(assign);
}

			/* --------------- */

static void
s_expand_assertion(ar_expr *e, ar_context *ctx, ccl_list *result)
{
  ar_expr *ae = ar_expr_remove_composite_slots(e,ctx);
  
  if( ar_expr_get_op(ae) == AR_EXPR_AND )
    {
      int i, nb_args;
      ar_expr **args = ar_expr_get_args(ae,&nb_args);

      for(i = 0; i < nb_args; i++)
	s_expand_assertion(args[i],ctx,result);
    }
  else
    {
      ccl_list_add(result,ar_expr_add_reference(ae));
    }
  ar_expr_del_reference(ae);
}

			/* --------------- */

static void
s_build_assignments(TRANSLATOR *C, int comp)
{
  ccl_pair *p;
  COMPONENT *self = C->components+comp;
  ar_context *ctx = ar_node_get_context(self->node);
  ccl_list *assertions = ar_node_get_assertions(self->node);
  ccl_list *flat_assertions = ccl_list_create();

  for(p = FIRST(assertions); p; p = CDR(p))
    s_expand_assertion(CAR(p),ctx,flat_assertions);

  while (! ccl_list_is_empty (flat_assertions))
    {      
      ar_expr *A = (ar_expr *) ccl_list_take_first (flat_assertions);

      if( ar_expr_get_op(A) != AR_EXPR_EQ )
	ccl_list_add(self->assertions,s_translate_formula(C,A));
      else
	{
	  int nb_args;
	  ar_expr **args = ar_expr_get_args(A,&nb_args);
	  
	  if( s_is_assignable(C,comp,args[0]) )
	    s_add_assignment(C,comp,args[0],args[1]);
	  else if( s_is_assignable(C,comp,args[1]) )
	    s_add_assignment(C,comp,args[1],args[0]);
	  else
	    ccl_list_add(self->assertions,s_translate_formula(C,A));
	}
      ar_expr_del_reference(A);
    }

  assertions = ar_node_get_initial_constraints (self->node);

  while (! ccl_list_is_empty (flat_assertions))
    {      
      ar_expr *A = (ar_expr *) ccl_list_take_first (assertions);
      ar_lustre_expr *T = ar_lustre_expr_crt_true ();
      ar_lustre_expr *I = s_translate_formula (C, A);
      
      ccl_list_add (self->assertions, ar_lustre_expr_crt_followed (I, T));
      ar_lustre_expr_del_reference (T);
      ar_lustre_expr_del_reference (I);
      ar_expr_del_reference(A);
    }
  ccl_list_delete(flat_assertions);
  ar_context_del_reference(ctx);
}

			/* --------------- */

static ar_lustre_domain *
s_translate_domain(TRANSLATOR *C, ar_type *t)
{
  ar_lustre_domain *result = NULL;
  ar_type_kind kind = ar_type_get_kind(t);

  switch (kind) 
    {
    case AR_TYPE_BOOLEANS:
      result = ar_lustre_domain_add_reference (C->bool_dom);
      break;
      
    case AR_TYPE_INTEGERS: 
    case AR_TYPE_RANGE: 
    case AR_TYPE_SYMBOL_SET:
      result = ar_lustre_domain_crt_int ();
      break;

    case AR_TYPE_ABSTRACT: 
      {
	ar_identifier *name = ar_type_abstract_get_name (t);
	result = ar_lustre_domain_crt_abstract (name);
	ar_identifier_del_reference (name);
      }
      break;

    case AR_TYPE_SYMBOLS: 
    case AR_TYPE_ARRAY: case AR_TYPE_STRUCTURE: case AR_TYPE_BANG:    
      ccl_unreachable ();
      break;

    default:
      ccl_unreachable ();
      break;
    }

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_translate_domain_constraint(TRANSLATOR *C, ar_context_slot *sl)
{
  ar_type *t;
  ar_expr *in_dom;
  ar_lustre_expr *result = NULL;
  ar_expr *e;

  if( (ar_context_slot_get_flags(sl) & AR_SLOT_FLAG_PARAMETER) )
    e = ar_expr_crt_parameter(sl);
  else
    e = ar_expr_crt_variable(sl);

  t = ar_context_slot_get_type(sl);
  in_dom = ar_expr_crt_in_domain(e,t);


  if( ! ar_expr_is_true(in_dom) )
    result = s_translate_formula(C,in_dom);
  ar_expr_del_reference(in_dom);
  ar_type_del_reference(t);
  ar_expr_del_reference(e);

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_translate_slot(TRANSLATOR *C, ar_context_slot *sl)
{
  const char *pref; 
  ar_identifier *ren;
  ar_type *t;
  ar_lustre_domain *ld;
  ar_lustre_expr *result;
  ar_identifier *name = ar_context_slot_get_name(sl);
  int flags = ar_context_slot_get_flags(sl);

  if( (flags&AR_SLOT_FLAG_FLOW_VAR) ) pref = C->flow_var_prefix;
  else if( (flags&AR_SLOT_FLAG_STATE_VAR) ) pref = C->state_var_prefix;
  else pref = C->parameter_prefix;
  ren = ar_identifier_rename_with_prefix(name,pref);
  t = ar_context_slot_get_type(sl);
  ld = s_translate_domain(C,t);
  result = ar_lustre_expr_crt_variable(ren,ld);

  ar_lustre_domain_del_reference(ld);
  ar_type_del_reference(t);
  ar_identifier_del_reference(ren);
  ar_identifier_del_reference(name);

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_translate_formula(TRANSLATOR *C, ar_expr *e)
{
  ar_lustre_expr *result = NULL;
  ar_expr_op op = ar_expr_get_op(e);
  int nb_args;
  ar_expr **args = ar_expr_get_args(e,&nb_args);
  ar_lustre_expr **t_args = NULL;

  if( nb_args > 0 )
    {
      int i;

      t_args = ccl_new_array(ar_lustre_expr *,nb_args);
      for(i = 0; i < nb_args; i++)
	t_args[i] = s_translate_formula(C,args[i]);
    }

  switch( op ) {
  case AR_EXPR_ITE :
    result = ar_lustre_expr_crt_ite(t_args[0],t_args[1],t_args[2]);
    break;
  case AR_EXPR_OR : case AR_EXPR_AND :
  case AR_EXPR_MIN : case AR_EXPR_MAX :
    {
      int i;
      ar_lustre_expr *(*crt)(ar_lustre_expr *, ar_lustre_expr *);

      if (op == AR_EXPR_OR) crt = ar_lustre_expr_crt_or;
      else if (op == AR_EXPR_AND) crt = ar_lustre_expr_crt_and;
      else if (op == AR_EXPR_MIN) crt = ar_lustre_expr_crt_min;
      else crt = ar_lustre_expr_crt_max;

      result = ar_lustre_expr_add_reference(t_args[0]);
      for(i = 1; i < nb_args; i++)
	{
	  ar_lustre_expr *tmp = crt (result,t_args[i]);
	  ar_lustre_expr_del_reference(result);
	  result = tmp;
	}
    }
    break;

  case AR_EXPR_EQ : 
    result = ar_lustre_expr_crt_eq(t_args[0],t_args[1]);
    break;
  case AR_EXPR_NEQ :
    result = ar_lustre_expr_crt_neq(t_args[0],t_args[1]);
    break;
  case AR_EXPR_LT : 
    result = ar_lustre_expr_crt_lt(t_args[0],t_args[1]);
    break;
  case AR_EXPR_GT : 
    result = ar_lustre_expr_crt_gt(t_args[0],t_args[1]);
    break;
  case AR_EXPR_LEQ : 
    result = ar_lustre_expr_crt_leq(t_args[0],t_args[1]);
    break;
  case AR_EXPR_GEQ :
    result = ar_lustre_expr_crt_geq(t_args[0],t_args[1]);
    break;
  case AR_EXPR_ADD : 
    result = ar_lustre_expr_crt_add(t_args[0],t_args[1]);
    break;
  case AR_EXPR_SUB : 
    result = ar_lustre_expr_crt_sub(t_args[0],t_args[1]);
    break;
  case AR_EXPR_MUL : 
    result = ar_lustre_expr_crt_mul(t_args[0],t_args[1]);
    break;
  case AR_EXPR_DIV : 
    result = ar_lustre_expr_crt_div(t_args[0],t_args[1]);
    break;
  case AR_EXPR_MOD : 
    result = ar_lustre_expr_crt_mod(t_args[0],t_args[1]);
    break;
  case AR_EXPR_NOT : 
    result = ar_lustre_expr_crt_not(t_args[0]);
    break;
  case AR_EXPR_NEG :
    result = ar_lustre_expr_crt_neg(t_args[0]);
    break;

  case AR_EXPR_CST : 
    {
      ar_constant *cst = ar_expr_eval(e, NULL);
      
      ccl_assert( ar_constant_get_kind(cst) == AR_CST_SCALAR );
      
      if( ar_constant_has_base_type(cst,AR_INTEGERS) )
	result = 
	  ar_lustre_expr_crt_int(ar_constant_get_integer_value(cst),NULL);
      else if( ar_constant_has_base_type(cst,AR_BOOLEANS) )
	{
	  if( ar_constant_get_boolean_value(cst) )
	    result = ar_lustre_expr_crt_true();
	  else
	    result = ar_lustre_expr_crt_false();
	}
      else if( ar_constant_has_base_type(cst,AR_SYMBOLS) )
	{
	  int ival = ar_constant_get_symbol_value(cst);
	  ar_identifier *ival_name = ar_type_symbol_set_get_value_name(ival);
	  ar_identifier *ren = 
	    ar_identifier_rename_with_prefix(ival_name,C->enum_prefix);
	  result = ar_lustre_expr_crt_int(ival,ren);

	  ar_identifier_del_reference(ival_name);
	  ar_identifier_del_reference(ren);
	}
      else
	{
	  ccl_throw_no_msg (internal_error);
	}
      ar_constant_del_reference(cst);
    }
    break;

  case AR_EXPR_VAR :
    {
      ar_context_slot *sl = ar_expr_get_slot(e);
      result = s_translate_slot(C,sl);
      ar_context_slot_del_reference(sl);
    }
    break;

  case AR_EXPR_CALL :
    {
      int i;
      ccl_list *largs = ccl_list_create();
      ar_signature *sig = ar_expr_call_get_signature(e);
      ar_lustre_proto *proto = s_signature_to_lustre_proto(C,sig);

      ar_signature_del_reference(sig);

      ccl_assert( proto != NULL );
      
      for(i = 0; i < nb_args; i++)
	ccl_list_add(largs,t_args[i]);

      result = ar_lustre_expr_crt_func_call(proto,largs);
      ccl_list_delete(largs);
      ar_lustre_proto_del_reference(proto);
    }
    break;

  case AR_EXPR_PARAM :
    {
      ar_context_slot *sl = ar_expr_get_slot(e);
      ar_identifier *id = ar_context_slot_get_name(sl);
      if( (result = ar_idtable_get(C->constants,id)) == NULL )
	result = s_translate_slot(C,sl);
      ar_identifier_del_reference(id);
      ar_context_slot_del_reference(sl);
    }
    break;

  case AR_EXPR_EXIST : case AR_EXPR_FORALL : 
    ccl_throw(internal_error,
	      "quantifiers are not supported by Lustre translators.");

  case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : case AR_EXPR_ARRAY :
  case AR_EXPR_STRUCT :
    ccl_throw(internal_error,"struct/array constant or member access");
  } 

  if( t_args != NULL )
    {
      int i;
      for(i = 0; i < nb_args; i++)
	ar_lustre_expr_del_reference(t_args[i]);
      ccl_delete(t_args);
    }

  ccl_post( result != NULL );

  return result;
}
