/*
 * interp.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef INTERP_H
# define INTERP_H

# include <ccl/ccl-exception.h>
# include "ar-identifier.h"
# include "tree.h"

typedef struct ar_lustre_program_st {
  int use_floats;
  ar_idtable *nodes;
  ar_idtable *functions;
  ar_idtable *types;
  ar_idtable *constants;
} ar_lustre_program;

CCL_DECLARE_EXCEPTION(lustre_interpretation_exception,exception);

extern ar_lustre_program *
ar_lustre_interp(ar_lustre_tree *t)
  CCL_THROW(lustre_interpretation_exception);

extern void
ar_lustre_program_delete(ar_lustre_program *prog);

#endif /* ! INTERP_H */
