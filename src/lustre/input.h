/*
 * input.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef INPUT_H
# define INPUT_H

# include <stdio.h>
# include "tree.h"

extern ar_lustre_tree *
ar_lustre_read_file(const char *filename);

extern ar_lustre_tree *
ar_lustre_read_stream(FILE *stream, const char *input_name);

#endif /* ! INPUT_H */
