/*
 * expr.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef EXPR_H
# define EXPR_H

# include "ar-identifier.h"
# include "domain.h"
# include "proto.h"

typedef struct ar_lustre_expr_st ar_lustre_expr;

typedef enum  ar_lustre_expr_type_enum {
  AL_EXPR_VAR,
  AL_EXPR_BCST, AL_EXPR_ICST, AL_EXPR_RCST, AL_EXPR_ACST,
  AL_EXPR_PRE, AL_EXPR_FOLLOWED,
  AL_EXPR_ITE,
  AL_EXPR_NOT, AL_EXPR_OR, AL_EXPR_AND, AL_EXPR_IMPLY,
  AL_EXPR_NEG, AL_EXPR_ADD, AL_EXPR_SUB, AL_EXPR_MUL, AL_EXPR_DIV,
  AL_EXPR_EQ, AL_EXPR_NEQ, AL_EXPR_LT, AL_EXPR_LEQ, AL_EXPR_GT, AL_EXPR_GEQ,
  AL_EXPR_ATMOST, AL_EXPR_EXPR_LIST, AL_EXPR_NODE_CALL, AL_EXPR_FUNC_CALL,
  AL_EXPR_CURRENT, AL_EXPR_MOD, AL_EXPR_WHEN
}  ar_lustre_expr_type;

extern ar_lustre_expr *
ar_lustre_expr_crt_true(void);

extern ar_lustre_expr *
ar_lustre_expr_crt_false(void);

extern ar_lustre_expr *
ar_lustre_expr_crt_int(int i, ar_identifier *name);

extern ar_lustre_expr *
ar_lustre_expr_crt_real(float f, ar_identifier *name);

extern ar_lustre_expr *
ar_lustre_expr_crt_abstract_cst(ar_identifier *name, ar_lustre_domain *dom);

extern ar_lustre_expr *
ar_lustre_expr_crt_variable(ar_identifier *name, ar_lustre_domain *dom);

extern ar_lustre_expr *
ar_lustre_expr_crt_pre(ar_lustre_expr *p);

extern ar_lustre_expr *
ar_lustre_expr_crt_followed(ar_lustre_expr *f, ar_lustre_expr *next);

extern ar_lustre_expr *
ar_lustre_expr_crt_ite(ar_lustre_expr *i, ar_lustre_expr *t, 
		       ar_lustre_expr *e);

extern ar_lustre_expr *
ar_lustre_expr_crt_not(ar_lustre_expr *a);

extern ar_lustre_expr *
ar_lustre_expr_crt_or(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_and(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_xor(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_imply(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_neg(ar_lustre_expr *a);

extern ar_lustre_expr *
ar_lustre_expr_crt_add(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_sub(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_mul(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_div(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_min (ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_max (ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_eq(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_neq(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_leq(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_lt(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_geq(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_gt(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_current(ar_lustre_expr *op);

extern ar_lustre_expr *
ar_lustre_expr_crt_when(ar_lustre_expr *op1, ar_lustre_expr *op2);

extern ar_lustre_expr *
ar_lustre_expr_crt_mod(ar_lustre_expr *a, ar_lustre_expr *b);

extern ar_lustre_expr *
ar_lustre_expr_crt_atmost(ccl_list *a);

extern ar_lustre_expr *
ar_lustre_expr_crt_expr_list(ccl_list *a);

extern ar_lustre_expr *
ar_lustre_expr_crt_node_call(ar_identifier *node, ar_lustre_domain *outdomain,
			     ccl_list *args);

extern ar_lustre_expr *
ar_lustre_expr_crt_func_call(ar_lustre_proto *proto, ccl_list *args);
			       
extern int
ar_lustre_expr_get_ival(ar_lustre_expr *expr);

extern double
ar_lustre_expr_get_rval(ar_lustre_expr *expr);

extern ar_identifier *
ar_lustre_expr_get_name(const ar_lustre_expr *expr);

extern ar_lustre_domain *
ar_lustre_expr_get_domain(const ar_lustre_expr *expr);

extern int
ar_lustre_expr_is_real(const ar_lustre_expr *expr);

extern ar_lustre_expr_type
ar_lustre_expr_get_type(ar_lustre_expr *expr);

extern ccl_list *
ar_lustre_expr_get_operands(ar_lustre_expr *expr);

extern ar_lustre_proto *
ar_lustre_expr_get_proto(const ar_lustre_expr *expr);

extern ar_lustre_expr *
ar_lustre_expr_add_reference(ar_lustre_expr *expr);

extern void
ar_lustre_expr_del_reference(ar_lustre_expr *expr);


extern void
ar_lustre_expr_log(ccl_log_type log, ar_lustre_expr *expr);

extern void
ar_lustre_expr_log_gen(ccl_log_type log, ar_lustre_expr *expr,
		       ar_identifier_log_proc idlog);

extern int
ar_lustre_expr_equals(const ar_lustre_expr *e1, const ar_lustre_expr *e2);

extern ar_lustre_expr *
ar_lustre_expr_get_ith_value(ar_lustre_expr *expr, int i);

#endif /* ! EXPR_H */
