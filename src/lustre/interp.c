/*
 * interp.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "domain.h"
#include "proto.h"
#include "expr.h"
#include "interp.h"
#include "node.h"


typedef ar_lustre_tree tree_t;

typedef struct {
  tree_t *root;
  ar_lustre_program *program;
  ccl_list *constant_stack;
  ccl_list *node_stack;
  ar_lustre_domain *DOM_INT;
  ar_lustre_domain *DOM_REAL;
  ar_lustre_domain *DOM_BOOL;
} INTERP;

			/* --------------- */

#define INVALID_NODE_TYPE() ccl_throw (internal_error,"invalid node type")
#define INTERP_EXCEPTION() ccl_throw_no_msg(lustre_interpretation_exception)

			/* --------------- */

#define INTERP_EXCEPTION_MSG(_args_) \
do { \
  ccl_error _args_ ; \
  INTERP_EXCEPTION(); \
} while(0)


			/* --------------- */

# define IS_LABELLED_BY(_t,_l) ( (_t) != NULL && (_t)->node_type == AL_ ## _l)

			/* --------------- */

static INTERP *interp = NULL;

CCL_DEFINE_EXCEPTION(lustre_interpretation_exception,exception);

			/* --------------- */

static ar_lustre_expr *
s_eval_typed_expression(ar_lustre_node *node, tree_t *tree, 
			ar_lustre_domain *dom);

static ar_lustre_expr *
s_eval_expression(ar_lustre_node *node, tree_t *tree);

static ar_lustre_expr *
s_interp_get_constant(tree_t *context, ar_identifier *name);

static ar_lustre_node *
s_interp_get_node(tree_t *context, ar_identifier *name);

			/* --------------- */

static ar_identifier *
s_eval_identifier(tree_t *t)
{
  ccl_pre( t->node_type == AL_IDENTIFIER );

  return ar_identifier_create_simple(t->value.id_value);
}

			/* --------------- */

static ar_lustre_expr *
s_eval_numerical_expression(ar_lustre_node *node, tree_t *tree)
{
  ar_lustre_expr *result = s_eval_expression(node,tree);
  ar_lustre_domain *rdom = ar_lustre_expr_get_domain(result);

  if( ! ar_lustre_domain_is_numerical(rdom) )
    {
      ccl_error("%s:%d: error: not numerical expression '",tree->filename,
		tree->line);
      ar_lustre_expr_log(CCL_LOG_ERROR,result);
      ccl_error("'\n");
      ar_lustre_expr_del_reference(result);
      result = NULL;
    }
  ar_lustre_domain_del_reference(rdom);

  if( result == NULL )
    INTERP_EXCEPTION();

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_eval_expression(ar_lustre_node *node, tree_t *T)
{
  ar_identifier *id = NULL;
  ar_lustre_expr *R = NULL;
  ar_lustre_expr *op[3] = { NULL, NULL, NULL };
  ar_lustre_domain *D = NULL;
  ar_lustre_node *N = NULL;
  ar_lustre_proto *P = NULL;
  ccl_list *tmp = NULL;
  tree_t *pt;

  ccl_try(lustre_interpretation_exception) 
    {
      switch( T->node_type ) {
      case AL_ITE: 
	op[0] = s_eval_typed_expression(node,T->child,interp->DOM_BOOL);
	op[1] = s_eval_expression(node,T->child->next);
	D = ar_lustre_expr_get_domain(op[1]);
	op[2] = s_eval_typed_expression(node,T->child->next->next,D);
	ar_lustre_domain_del_reference(D);
	D = NULL;
	R = ar_lustre_expr_crt_ite(op[0],op[1],op[2]);
	break;

      case AL_XOR: case AL_AND: case AL_IMPLY: case AL_OR:
	op[1] = s_eval_typed_expression(node,T->child->next,interp->DOM_BOOL);
      case AL_NOT:
	op[0] = s_eval_typed_expression(node,T->child,interp->DOM_BOOL);
	if( T->node_type == AL_NOT )
	  R = ar_lustre_expr_crt_not(op[0]);
	else if( T->node_type == AL_XOR )
	  R = ar_lustre_expr_crt_xor(op[0],op[1]);
	else if( T->node_type == AL_OR )
	  R = ar_lustre_expr_crt_or(op[0],op[1]);
	else if( T->node_type == AL_IMPLY )
	  R = ar_lustre_expr_crt_imply(op[0],op[1]);
	else
	  R = ar_lustre_expr_crt_and(op[0],op[1]);
	break;

      case AL_EQ: case AL_NEQ:
	op[0] = s_eval_expression(node,T->child);
	D = ar_lustre_expr_get_domain(op[0]);
	op[1] = s_eval_typed_expression(node,T->child->next,D);
	ar_lustre_domain_del_reference(D);
	D = NULL;

	if( T->node_type == AL_EQ )
	  R = ar_lustre_expr_crt_eq(op[0],op[1]);
	else
	  R = ar_lustre_expr_crt_neq(op[0],op[1]);
	break;

      case AL_GEQ: case AL_GT: case AL_LT: case AL_LEQ:
	op[0] = s_eval_numerical_expression(node,T->child);
	D = ar_lustre_expr_get_domain(op[0]);
	op[1] = s_eval_typed_expression(node,T->child->next,D);
	ar_lustre_domain_del_reference(D);
	D = NULL;
	if( T->node_type == AL_GEQ )
	  R = ar_lustre_expr_crt_geq(op[0],op[1]);
	else if( T->node_type == AL_GT ) 
	  R = ar_lustre_expr_crt_gt(op[0],op[1]);
	else if( T->node_type == AL_LEQ )
	  R = ar_lustre_expr_crt_leq(op[0],op[1]);
	else
	  R = ar_lustre_expr_crt_lt(op[0],op[1]);
	break;


      case AL_TRUE :
	R = ar_lustre_expr_crt_true(); 
	break;
      case AL_FALSE: 
	R = ar_lustre_expr_crt_false(); 
	break;
      case AL_UINT : 
	R = ar_lustre_expr_crt_int(T->value.int_value,NULL); 
	break;
      case AL_UFLOAT :  
	R = ar_lustre_expr_crt_real(T->value.flt_value,NULL);
	interp->program->use_floats = 1;
	break;



      case AL_NEG: case AL_ADD: case AL_SUB: case AL_MUL: case AL_DIV:
      case AL_MOD:
	op[0] = s_eval_numerical_expression(node,T->child);
	if( T->node_type != AL_NEG )
	  {
	    D = ar_lustre_expr_get_domain(op[0]);
	    op[1] = s_eval_typed_expression(node,T->child->next,D);
	    ar_lustre_domain_del_reference(D);
	    D = NULL;
	  }

	if( T->node_type == AL_NEG )
	  R = ar_lustre_expr_crt_neg(op[0]);
	else if( T->node_type == AL_ADD )
	  R = ar_lustre_expr_crt_add(op[0],op[1]);
	else if( T->node_type == AL_SUB )
	  R = ar_lustre_expr_crt_sub(op[0],op[1]);
	else if( T->node_type == AL_MUL )
	  R = ar_lustre_expr_crt_mul(op[0],op[1]);
	else if( T->node_type == AL_DIV )
	  R = ar_lustre_expr_crt_div(op[0],op[1]);
	else 
	  R = ar_lustre_expr_crt_mod(op[0],op[1]);
	break;

      case AL_PRE: 
	op[0] = s_eval_expression(node,T->child);
	R = ar_lustre_expr_crt_pre(op[0]);
	break;

      case AL_FOLLOWED:
	op[0] = s_eval_expression(node,T->child);
	D = ar_lustre_expr_get_domain(op[0]);
	op[1] = s_eval_typed_expression(node,T->child->next,D);
	ar_lustre_domain_del_reference(D);
	D = NULL;
	R = ar_lustre_expr_crt_followed(op[0],op[1]);
	break;

      case AL_CALL:
	{
	  ccl_list *args;

	  ccl_assert( T->child->node_type == AL_IDENTIFIER );
	  id = s_eval_identifier(T->child);
	  P = ar_idtable_get(interp->program->functions,id);
	  if( P == NULL )
	    {
	      if( node == NULL )
		N = (ar_lustre_node *)
		  ar_idtable_get(interp->program->nodes,id);
	      else 
		N = s_interp_get_node(T,id);		
	      
	      if( N == NULL )
		{
		  ccl_error("%s:%d: error: undefined node '",
			    T->filename,T->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ccl_error("'\n");
		  INTERP_EXCEPTION();
		}
	      
	      D = ar_lustre_node_get_input_domain(N);
	    }
	  else
	    {
	      D = ar_lustre_proto_get_input_domain(P);
	    }
	  
	  op[0] = s_eval_typed_expression(node,T->child->next,D);
	  
	  if( ar_lustre_expr_get_type(op[0]) == AL_EXPR_EXPR_LIST )
	    {
	      args = ar_lustre_expr_get_operands(op[0]);
	    }
	  else
	    {
	      args = ccl_list_create();
	      ccl_list_add(args,ar_lustre_expr_add_reference(op[0]));
	    }
	  ar_lustre_domain_del_reference(D);
	  
	  if( P == NULL ) 
	    {	    
	      D = ar_lustre_node_get_output_domain(N);
	      R = ar_lustre_expr_crt_node_call(id,D,args);
	      ar_lustre_domain_del_reference(D);
	    }
	  else 
	    {
	      R = ar_lustre_expr_crt_func_call(P,args);
	    }
	  D = NULL;
	  ar_identifier_del_reference(id);
	  id = NULL;
	  ccl_list_clear_and_delete(args,(ccl_delete_proc *)
				    ar_lustre_expr_del_reference);
	}
	break;

      case AL_IDENTIFIER:
	id = s_eval_identifier(T);
	if( node != NULL )
	  R = ar_lustre_node_get_variable(node,id);
	if( R == NULL )
	  R = s_interp_get_constant(T,id);
	if( R == NULL )
	  {
	    ccl_error("%s:%d: error: undefined symbol '",T->filename,T->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("'\n");
	    INTERP_EXCEPTION();
	  }
	ar_identifier_del_reference(id);
	id = NULL;
	break;

      case AL_ATMOST:
	tmp = ccl_list_create();
	for(pt = T->child; pt; pt = pt->next)
	  {
	    ar_lustre_expr *expr = 
	      s_eval_typed_expression(node,pt,interp->DOM_BOOL);
	    ccl_list_add(tmp,expr);
	  }

	R = ar_lustre_expr_crt_atmost(tmp);
	ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
				  ar_lustre_expr_del_reference);
	tmp = NULL;
	break;

      case AL_EXPR_LIST: 
	tmp = ccl_list_create();
	for(pt = T->child; pt; pt = pt->next)
	  {
	    ar_lustre_expr *expr = s_eval_expression(node,pt);
	    ccl_list_add(tmp,expr);
	  }

	if( ccl_list_get_size(tmp) > 1 )
	  R = ar_lustre_expr_crt_expr_list(tmp);
	else
	  R = ar_lustre_expr_add_reference(CAR(FIRST(tmp)));
	ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
				  ar_lustre_expr_del_reference);
	tmp = NULL;
	break;

      case AL_CURRENT :
	op[0] = s_eval_expression(node,T->child);
	R = ar_lustre_expr_crt_current(op[0]);
	break;

      case AL_WHEN : 
	op[0] = s_eval_expression(node,T->child);
	op[1] = s_eval_typed_expression(node,T->child->next,interp->DOM_BOOL);
	R = ar_lustre_expr_crt_when(op[0],op[1]);
	break;

      default : 
	INVALID_NODE_TYPE();
      }
    }
  ccl_no_catch;

  ccl_zdelete(ar_lustre_expr_del_reference,op[0]);
  ccl_zdelete(ar_lustre_expr_del_reference,op[1]);
  ccl_zdelete(ar_lustre_expr_del_reference,op[2]);
  ccl_zdelete(ar_lustre_domain_del_reference,D);
  ccl_zdelete(ar_identifier_del_reference,id);
  ccl_zdelete(ar_lustre_node_del_reference,N);
  ccl_zdelete(ar_lustre_proto_del_reference,P);

  if( tmp != NULL )
    ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);

  if( R == NULL )
    ccl_rethrow();

  return R;
}

			/* --------------- */

static ar_lustre_expr *
s_eval_typed_expression(ar_lustre_node *node, tree_t *t, ar_lustre_domain *dom)
{
  ar_lustre_expr *result = s_eval_expression(node,t);
  ar_lustre_domain *rdom = ar_lustre_expr_get_domain(result);

  if( ! ar_lustre_domain_equals(dom,rdom) )
    {
      ccl_error("%s:%d: error: wrong type (",t->filename,t->line);
      ar_lustre_domain_log(CCL_LOG_ERROR,rdom);
      ccl_error(") in expression '");
      ar_lustre_expr_log(CCL_LOG_ERROR,result);
      ccl_error("'.\n");
      ccl_error("%s:%d: error: expected type is '",t->filename,t->line);
      ar_lustre_domain_log(CCL_LOG_ERROR,dom);
      ccl_error("'.\n");
      ar_lustre_expr_del_reference(result);
      result = NULL;
    }
  ar_lustre_domain_del_reference(rdom);

  if( result == NULL )
    INTERP_EXCEPTION();

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_eval_constant_expression(tree_t *t)
{
  return s_eval_expression(NULL,t);
}

			/* --------------- */

static void
s_eval_const_list(tree_t *tree)
{
  tree_t *pt;

  ccl_pre( tree->node_type == AL_CST_DECL_LIST );

  for(pt = tree->child; pt; pt = pt->next)
    {
      ar_identifier *id;
      ar_lustre_expr *expr = NULL;

      ccl_assert( pt->node_type == AL_CST_DECL );
      
      if( pt->child->node_type == AL_CST_NAME_LIST ) 
	continue;

      id = s_eval_identifier(pt->child);

      if( ar_idtable_has(interp->program->constants,id) )
	{
	  ar_identifier_del_reference(id);
	  continue;
	}

      ccl_try(lustre_interpretation_exception) 
	{
	  expr = s_eval_constant_expression(pt->child->next);
	  ar_idtable_put(interp->program->constants,id,expr);
	  ar_lustre_expr_del_reference(expr);
	}
      ccl_no_catch;
      ar_identifier_del_reference(id);

      if( expr == NULL )
	ccl_rethrow();
    }
}

			/* --------------- */

static ar_lustre_domain *
s_eval_type(tree_t *tree)
{
  ar_lustre_domain *result = NULL;

  switch( tree->node_type ) {
  case AL_BOOLEANS :
    result = ar_lustre_domain_crt_bool();
    break;
  case AL_INTEGERS :
    result = ar_lustre_domain_crt_int();
    break;
  case AL_REALS : 
    result = ar_lustre_domain_crt_real();
    interp->program->use_floats = 1;
    break;

  case AL_IMPORTED_TYPE : 
    {
      ar_identifier *id = s_eval_identifier(tree->child);
      result = ar_idtable_get(interp->program->types,id);
      ar_identifier_del_reference(id);

      if( result == NULL )
	{
	  ccl_error("%s:%d: error: undefined type '%s'.",tree->filename,
		    tree->line,tree->child->value.id_value);
	  INTERP_EXCEPTION();
	}
    }
    break;
  default : 
    INVALID_NODE_TYPE();
  };

  return result;
}

			/* --------------- */

static void
s_eval_node_params(ar_lustre_node *N, tree_t *tree)
{
  tree_t *pt;
  tree_t *pvar;

  ccl_pre( tree->node_type == AL_NODE_PARAMS );

  for(pt = tree->child; pt; pt = pt->next)
    {
      ar_lustre_domain *dom;
      int is_constant = IS_LABELLED_BY(pt,CST_DECL);

      pvar = pt->child; 
      if( pvar->next->next != NULL )
	{
	  ccl_error("%s:%d: error: the operator 'when' is not supported.",
		    pvar->filename,pvar->line);
	  INTERP_EXCEPTION();
	}

      dom = s_eval_type(pvar->next);

      ccl_assert( pvar->node_type == AL_NAME_LIST );

      ccl_try(lustre_interpretation_exception)
	{
	  for(pvar = pvar->child; pvar; pvar = pvar->next)
	    {
	      ar_identifier *id = s_eval_identifier(pvar);

	      if( ar_lustre_node_has_variable(N,id) )
		{
		  ccl_error("%s:%d: error: variable/constant '",pvar->filename,
			    pvar->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ccl_error("' has aready been declared.\n");
		  INTERP_EXCEPTION();
		}
	      else if( is_constant )
		ar_lustre_node_add_parameter(N,id,dom);
	      else
		ar_lustre_node_add_input_variable(N,id,dom);
	      ar_identifier_del_reference(id);
	    }
	}
      ccl_catch
	{
	  ar_lustre_domain_del_reference(dom);
	  ccl_rethrow();
	}
      ccl_end_try;
      ar_lustre_domain_del_reference(dom);
    }
}

			/* --------------- */

static void
s_eval_node_returns(ar_lustre_node *N, tree_t *tree)
{
  tree_t *pt;
  tree_t *pvar;

  ccl_pre( tree->node_type == AL_NODE_RETURNS );

  for(pt = tree->child; pt; pt = pt->next)
    {
      ar_lustre_domain *dom;

      ccl_assert( pt->node_type == AL_NODE_RETURN );
      pvar = pt->child; 
      if( pvar->next->next != NULL )
	{
	  ccl_error("%s:%d: error: the operator 'when' is not supported.",
		    pvar->filename,pvar->line);
	  INTERP_EXCEPTION();
	}
      dom = s_eval_type(pvar->next);

      ccl_assert( pvar->node_type == AL_NAME_LIST );

      ccl_try(lustre_interpretation_exception)
	{
	  for(pvar = pvar->child; pvar; pvar = pvar->next)
	    {
	      ar_identifier *id = s_eval_identifier(pvar);
	      if( ar_lustre_node_has_variable(N,id) )
		{
		  ccl_error("%s:%d: error: variable '",pvar->filename,
			    pvar->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ccl_error("' has aready been declared.\n");
		  INTERP_EXCEPTION();
		}
	      else
		ar_lustre_node_add_output_variable(N,id,dom);
	      ar_identifier_del_reference(id);
	    }
	}
      ccl_catch
	{
	  ar_lustre_domain_del_reference(dom);
	  ccl_rethrow();
	}
      ccl_end_try;
      ar_lustre_domain_del_reference(dom);
    }
}

			/* --------------- */

static void
s_eval_node_signature(ar_lustre_node *N, tree_t *tree)
{
  ar_identifier *id;

  ccl_pre( tree->node_type == AL_NODE_SIGNATURE );

  id = s_eval_identifier(tree->child);
  ar_lustre_node_set_name(N,id);
  ar_identifier_del_reference(id);

  s_eval_node_params(N,tree->child->next);
  s_eval_node_returns(N,tree->child->next->next);
}

			/* --------------- */

static void
s_eval_node_vars(ar_lustre_node *N, tree_t *tree)
{
  tree_t *pt;
  tree_t *pvar;

  ccl_assert( tree->node_type == AL_NODE_VARS );

  if( tree->child == NULL ) 
    return;
  
  for(pt = tree->child; pt; pt = pt->next)
    {
      ar_lustre_domain *dom;

      ccl_assert( pt->node_type == AL_NODE_VAR );
      pvar = pt->child; 
      if( pvar->next->next != NULL )
	{
	  ccl_error("%s:%d: error: the operator 'when' is not supported.",
		    pvar->filename,pvar->line);
	  INTERP_EXCEPTION();
	}
      dom = s_eval_type(pvar->next);

      ccl_assert( pvar->node_type == AL_NAME_LIST );
      ccl_try(lustre_interpretation_exception)
	{
	  for(pvar = pvar->child; pvar; pvar = pvar->next)
	    {
	      ar_identifier *id = s_eval_identifier(pvar);
	      if( ar_lustre_node_has_variable(N,id) )
		{
		  ccl_error("%s:%d: error: variable '",pvar->filename,
			    pvar->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ccl_error("' has aready been declared.\n");
		  INTERP_EXCEPTION();
		}
	      else
		ar_lustre_node_add_local_variable(N,id,dom);
	      ar_identifier_del_reference(id);
	    }
	}
      ccl_catch
	{
	  ar_lustre_domain_del_reference(dom);
	  ccl_rethrow();
	}
      ccl_end_try;
      ar_lustre_domain_del_reference(dom);
    }
  
}

			/* --------------- */

static ccl_list *
s_eval_left_member(ar_lustre_node *N, tree_t *tree)
{
  tree_t *pt;
  ccl_list *result;

  ccl_pre( tree->node_type == AL_NAME_LIST );

  result = ccl_list_create();

  ccl_try(lustre_interpretation_exception)
    {
      for(pt = tree->child; pt; pt = pt->next)
	{
	  int error = 1;
	  ar_identifier *id = s_eval_identifier(pt);
	  int type = ar_lustre_node_has_variable(N,id);
	  
	  switch(type) {
	  case AR_LUSTRE_UNKNOWN:	    
	    ccl_error("%s:%d: error: undefined variable '",pt->filename,
		      pt->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("'.\n");
	    break;

	  case AR_LUSTRE_INPUT: 
	    ccl_error("%s:%d: error: try to assign the input variable '",
		      pt->filename,pt->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("'.\n");
	    break;

	  case AR_LUSTRE_OUTPUT: case AR_LUSTRE_LOCAL:
	    if( ar_lustre_node_is_defined(N,id) )
	      {
		ccl_error("%s:%d: error: the variable '",pt->filename,
			  pt->line);
		ar_identifier_log(CCL_LOG_ERROR,id);
		ccl_error("' is already defined.\n");
	      }
	    else
	      {
		ar_lustre_expr *var = ar_lustre_node_get_variable(N,id);
		ccl_assert( var != NULL );
		ccl_list_add(result,var);
		error = 0;
	      }
	    break;
	  default :
	    INVALID_NODE_TYPE();
	  };
	  
	  ar_identifier_del_reference(id);
	  if( error )
	    INTERP_EXCEPTION();
	}
    }
  ccl_catch
    {
      ccl_list_clear_and_delete(result,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_rethrow();
    }
  ccl_end_try;

  ccl_assert( ccl_list_get_size(result) > 0 );

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_eval_right_member(ar_lustre_node *N, tree_t *tree, ccl_list *vars)
{
  ccl_list *comp;
  ccl_pair *p;
  ar_lustre_domain *domain;
  ar_lustre_expr *result = NULL;

  comp = ccl_list_create();
  for(p = FIRST(vars); p; p = CDR(p))
    {
      ar_lustre_expr *var = (ar_lustre_expr *)CAR(p);
      ar_lustre_domain *d = ar_lustre_expr_get_domain(var);
      ccl_list_add(comp,d);
    }
  domain = ar_lustre_domain_crt_composite(comp);
  ccl_list_clear_and_delete(comp,(ccl_delete_proc *)
			    ar_lustre_domain_del_reference);

  ccl_try(lustre_interpretation_exception)
    {
      result = s_eval_typed_expression(N,tree,domain);
    }
  ccl_no_catch;

  ar_lustre_domain_del_reference(domain);

  if( result == NULL )
    ccl_rethrow();

  return result;
}

			/* --------------- */

static void
s_eval_node_equations(ar_lustre_node *N, tree_t *tree)
{
  tree_t *pt;

  ccl_pre( tree->node_type == AL_NODE_EQUATIONS );

  for(pt = tree->child; pt; pt = pt->next)
    {
      if( pt->node_type == AL_NODE_EQUATION )
	{
	  ccl_list *left = s_eval_left_member(N,pt->child);
	  ccl_try(lustre_interpretation_exception)
	    {
	      ar_lustre_expr *right = 
		s_eval_right_member(N,pt->child->next,left);

	      ar_lustre_node_add_equation(N,left,right);
	      ccl_list_clear_and_delete(left,(ccl_delete_proc *)
					ar_lustre_expr_del_reference);
	      ar_lustre_expr_del_reference(right);
	    }
	  ccl_catch
	    {
	      ccl_list_clear_and_delete(left,(ccl_delete_proc *)
					ar_lustre_expr_del_reference);
	      ccl_rethrow();
	    }
	  ccl_end_try;
	}
      else if( pt->node_type == AL_NODE_ASSERT )
	{
	  ar_lustre_expr *expr = 
	    s_eval_typed_expression(N,pt->child,interp->DOM_BOOL);
	  ar_lustre_node_add_assertion(N,expr);
	  ar_lustre_expr_del_reference(expr);
	}
      else
	{
	  INVALID_NODE_TYPE();
	}
    }
}

			/* --------------- */

static void
s_eval_node(tree_t *tree)
{
  int error = 1;
  ar_lustre_node *N = NULL;
  ar_identifier *id;

  ccl_pre( IS_LABELLED_BY(tree,NODE) );
  ccl_pre( IS_LABELLED_BY(tree->child,NODE_SIGNATURE) );
  ccl_pre( IS_LABELLED_BY(tree->child->child,IDENTIFIER) );

  id = s_eval_identifier(tree->child->child);

  if( ar_idtable_has(interp->program->nodes,id) )
    {
      ar_identifier_del_reference(id);
      return;
    }

  N = ar_lustre_node_create();

  ccl_try(lustre_interpretation_exception)
    {
      s_eval_node_signature(N,tree->child);
      s_eval_node_vars(N,tree->child->next);
      s_eval_node_equations(N,tree->child->next->next);     
      ar_idtable_put(interp->program->nodes,id,N);
      error = 0;
    }
  ccl_no_catch;

  ar_identifier_del_reference(id);
  ar_lustre_node_del_reference(N);
  if( error )
    ccl_rethrow();
}

			/* --------------- */

static ar_lustre_expr *
s_interp_get_constant(tree_t *context, ar_identifier *name)
{
  tree_t *ptree;
  ar_lustre_expr *result = (ar_lustre_expr *)
    ar_idtable_get(interp->program->constants,name);

  if( result != NULL )
    return result;

  if( ccl_list_has(interp->constant_stack,name) )
    {
      ccl_error("%s:%d: error: loop in the definition of the constant '",
		context->filename,context->line);
      ar_identifier_log(CCL_LOG_ERROR,name);
      ccl_error("'.\n");
      INTERP_EXCEPTION();
    }
  
  for(ptree = interp->root; result == NULL && ptree; ptree = ptree->next)
    {
      tree_t *pt;

      if( ! IS_LABELLED_BY(ptree,CST_DECL_LIST) )
	continue;

      for(pt = ptree->child; pt && result == NULL; pt = pt->next)
	{

	  if( IS_LABELLED_BY(pt->child,IDENTIFIER) ) 
	    {
	      ar_identifier *id = s_eval_identifier(pt->child);
	      int nok = ( id != name );
	      ar_identifier_del_reference(id);
	      
	      if( nok )
		continue;

	      nok = 1;
	      ccl_list_add(interp->constant_stack,name);
	      ccl_try(lustre_interpretation_exception)
		{
		  result = s_eval_constant_expression(pt->child->next);
		  nok = 0;
		  ar_idtable_put(interp->program->constants,name,result);
		  ccl_assert( result != NULL );
		}
	      ccl_no_catch;
	      ccl_list_remove(interp->constant_stack,name);

	      if( nok )
		ccl_rethrow();
	    }
	  else
	    {
	      ar_lustre_domain *dom;
	      ar_lustre_tree *pc;

	      ccl_assert( IS_LABELLED_BY(pt->child,CST_NAME_LIST) );
	      
	      dom = s_eval_type(pt->child->next);
	      for(pc = pt->child->child; pc && ! result; pc = pc->next)
		{
		  ar_identifier *id = s_eval_identifier(pc);
		  if( id == name ) 
		    {
		      result = ar_lustre_expr_crt_abstract_cst(id,dom);
		      ar_idtable_put(interp->program->constants,name,result);
		    }
		  ar_identifier_del_reference(id);
		}
	      ar_lustre_domain_del_reference(dom);
	    }
	}
    }

  return result;
}

			/* --------------- */

static ar_lustre_node *
s_interp_get_node(tree_t *context, ar_identifier *name)
{
  tree_t *ptree;
  ar_lustre_node *result = (ar_lustre_node *)
    ar_idtable_get(interp->program->nodes,name);

  if( result != NULL )
    return result;

  if( ccl_list_has(interp->node_stack,name) )
    {
      ccl_error("%s:%d: error: loop in the definition of the node '",
		context->filename,context->line);
      ar_identifier_log(CCL_LOG_ERROR,name);
      ccl_error("'.\n");
      INTERP_EXCEPTION();
    }
  
  for(ptree = interp->root; result == NULL && ptree; ptree = ptree->next)
    {
      int nok;
      ar_identifier *id;

      if( ! IS_LABELLED_BY(ptree,NODE) ) 
	continue;

      id = s_eval_identifier(ptree->child->child);
      nok = (id != name);
      ar_identifier_del_reference(id);

      if( nok ) continue;

      nok = 1;
      ccl_list_add(interp->node_stack,name);
      ccl_try(lustre_interpretation_exception) 
	{ 
	  s_eval_node(ptree);
	  nok = 0;
	  result = s_interp_get_node(context,name);
	  ccl_assert( result != NULL );
	}
      ccl_no_catch;
      ccl_list_remove(interp->node_stack,name);
      if( nok )
	ccl_rethrow();
    }

  return result;
}

			/* --------------- */

static INTERP *
s_create_interp(tree_t *tree)
{
  INTERP *result = ccl_new(INTERP);

  result->root = tree;
  result->program = ccl_new(ar_lustre_program);
  result->program->constants = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_expr_add_reference,
		      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  result->program->nodes = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_node_add_reference,
		      (ccl_delete_proc *)ar_lustre_node_del_reference);
  result->program->types = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_domain_add_reference,
		      (ccl_delete_proc *)ar_lustre_domain_del_reference);
  result->program->functions = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_proto_add_reference,
		      (ccl_delete_proc *)ar_lustre_proto_del_reference);

  result->constant_stack = ccl_list_create();
  result->node_stack     = ccl_list_create();
  result->DOM_INT        = ar_lustre_domain_crt_int();
  result->DOM_REAL       = ar_lustre_domain_crt_real();
  result->DOM_BOOL       = ar_lustre_domain_crt_bool();

  return result;
}

			/* --------------- */

static void
s_destroy_interp(INTERP *interp)
{
  ccl_zdelete(ar_lustre_program_delete,interp->program);
  ccl_list_delete(interp->constant_stack);
  ccl_list_delete(interp->node_stack);
  ar_lustre_domain_del_reference(interp->DOM_INT);
  ar_lustre_domain_del_reference(interp->DOM_REAL);
  ar_lustre_domain_del_reference(interp->DOM_BOOL);
  ccl_delete(interp);
}

			/* --------------- */

void
ar_lustre_program_delete(ar_lustre_program *prog)
{
  ar_idtable_del_reference(prog->constants);
  ar_idtable_del_reference(prog->nodes);
  ar_idtable_del_reference(prog->functions);
  ar_idtable_del_reference(prog->types);  
  ccl_delete(prog);
}

static void
s_check_duplicated_definitions(void)
{
  int error = 0;
  ar_identifier * id;
  ccl_list *nodes = ccl_list_create();
  ccl_list *constants = ccl_list_create();
  ccl_list *functions = ccl_list_create();
  ccl_list *types = ccl_list_create();
  tree_t *ptree;
  tree_t *p;

  for(ptree = interp->root; ptree && ! error; ptree = ptree->next)
    {
      switch( ptree->node_type ) {
      case AL_NODE : 
	id = s_eval_identifier(ptree->child->child);
	if( ccl_list_has(nodes,id) )
	  {
	    ccl_error("%s:%d: error: redefinition of the node '",
		      ptree->filename,ptree->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("'.\n");
	    error = 1;

	  }
	else if( ccl_list_has(functions,id) )
	  {
	    ccl_error("%s:%d: error: node '",
		      ptree->filename,ptree->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("' clashes with the external function with the same "
		      "name.\n");
	    error = 1;
	  }
	else 
	  {
	    ccl_list_add(nodes,ar_identifier_add_reference(id));
	  }
	ar_identifier_del_reference(id);
	break;

      case AL_CST_DECL_LIST : 
	for(p = ptree->child; p && ! error; p = p->next)
	  {
	    ccl_assert( IS_LABELLED_BY(p,CST_DECL) );

	    if( IS_LABELLED_BY(p->child,CST_NAME_LIST) )
	      {
		tree_t *ptaux;

		for(ptaux = p->child->child; ptaux; ptaux = ptaux->next)
		  {
		    id = s_eval_identifier(ptaux);
		    if( ccl_list_has(constants,id) )
		      {
			ccl_error("%s:%d: error: redefinition of the "
				  "constant '",
				  ptaux->child->filename,ptaux->child->line);
			ar_identifier_log(CCL_LOG_ERROR,id);
			ccl_error("'.\n");
			error = 1;
		      }
		    else 
		      {
			ccl_list_add(constants,
				     ar_identifier_add_reference(id));
		      }
		    ar_identifier_del_reference(id);
		  }
	      }
	    else
	      {
		ccl_assert( IS_LABELLED_BY(p->child,IDENTIFIER) );
		ccl_assert( p->child->next != NULL );

		id = s_eval_identifier(p->child);
		if( ccl_list_has(constants,id) )
		  {
		    ccl_error("%s:%d: error: redefinition of the constant '",
			      p->child->filename,p->child->line);
		    ar_identifier_log(CCL_LOG_ERROR,id);
		    ccl_error("'.\n");
		    error = 1;
		  }
		else 
		  {
		    ccl_list_add(nodes,		    
				 ar_identifier_add_reference(id));
		  }
		ar_identifier_del_reference(id);
	      }
	  }
	break;
	
      case AL_TYPE_DECL : 
	for(p = ptree->child->child; p ; p = p->next)
	  {
	    ccl_assert( IS_LABELLED_BY(p,IDENTIFIER) );
	    id = s_eval_identifier(p);

	    if( ccl_list_has(types,id) )
	      {
		ccl_warning("%s:%d: warning: abstract type '",
			    p->filename,p->line);
		ar_identifier_log(CCL_LOG_WARNING,id);
		ccl_warning("' is declared twice.\n");
	      }
	    else
	      {
		ccl_list_add(types,ar_identifier_add_reference(id));
	      }
	    ar_identifier_del_reference(id);
	  }
	break;

      case AL_FUNCTION_DECL :
	id = s_eval_identifier(ptree->child);
	if( ccl_list_has(functions,id) )
	  {
	    ccl_error("%s:%d: error: redefinition of the external function '",
		      ptree->filename,ptree->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("'.\n");
	    error = 1;
	    ar_identifier_del_reference(id);
	  }
	else if( ccl_list_has(nodes,id) )
	  {
	    ccl_error("%s:%d: error: external function '",
		      ptree->filename,ptree->line);
	    ar_identifier_log(CCL_LOG_ERROR,id);
	    ccl_error("' clashes with the node with the same name.\n");
	    error = 1;
	  }
	else 
	  {
	    ccl_list_add(functions,ar_identifier_add_reference(id));
	  }
	ar_identifier_del_reference(id);
	break;

      default : 
	INVALID_NODE_TYPE();
      };
    }

  ccl_list_clear_and_delete(nodes,
			    (ccl_delete_proc *)ar_identifier_del_reference);
  ccl_list_clear_and_delete(constants,
			    (ccl_delete_proc *)ar_identifier_del_reference);
  ccl_list_clear_and_delete(functions,
			    (ccl_delete_proc *)ar_identifier_del_reference);
  ccl_list_clear_and_delete(types,
			    (ccl_delete_proc *)ar_identifier_del_reference);
  if( error )
    INTERP_EXCEPTION();
}

			/* --------------- */

static void
s_eval_type_decl(ar_lustre_tree *t)
{
  ccl_pre( IS_LABELLED_BY(t,TYPE_DECL) );
  ccl_pre( IS_LABELLED_BY(t->child,NAME_LIST) );

  for(t = t->child->child; t; t = t->next)
    {
      ar_identifier *typename;
      ar_lustre_domain *dom;

      ccl_pre( IS_LABELLED_BY(t,IDENTIFIER) );

      typename = s_eval_identifier(t);
      dom = ar_lustre_domain_crt_abstract(typename);
      ar_idtable_put(interp->program->types,typename,dom);
      ar_lustre_domain_del_reference(dom);
      ar_identifier_del_reference(typename);
    }
}

			/* --------------- */

static ar_lustre_domain *
s_eval_function_domain(ar_lustre_tree *t)
{
  ar_lustre_domain *result = NULL;
  ccl_list *components = ccl_list_create();

  ccl_pre( IS_LABELLED_BY(t,PARAMS) || IS_LABELLED_BY(t,RETURNS) );

  ccl_try(lustre_interpretation_exception)
    {
      for(t = t->child; t; t = t->next)
	{
	  ar_lustre_tree *c;
	  ar_lustre_domain *dom;

	  ccl_assert( IS_LABELLED_BY(t,TYPED_ID_LIST) );
	  ccl_assert( IS_LABELLED_BY(t->child,NAME_LIST) );

	  dom = s_eval_type(t->child->next);
	  
	  for(c = t->child->child; c; c = c->next)
	    ccl_list_add(components,ar_lustre_domain_add_reference(dom));
	  ar_lustre_domain_del_reference(dom);
	}

      if( ccl_list_get_size(components) == 1 )
	result = ar_lustre_domain_add_reference(CAR(FIRST(components)));
      else
	result = ar_lustre_domain_crt_composite(components);
    }
  ccl_no_catch;

  ccl_list_clear_and_delete(components,(ccl_delete_proc *)
			    ar_lustre_domain_del_reference);
			    
  if( result == NULL )
    INTERP_EXCEPTION();

  return result;
}

			/* --------------- */

static void
s_eval_function_decl(ar_lustre_tree *t)
{
  int error = 1;
  ar_identifier *protoname = NULL;
  ar_lustre_domain *input_domain = NULL;
  ar_lustre_domain *output_domain = NULL;

  ccl_pre( IS_LABELLED_BY(t,FUNCTION_DECL) );
  ccl_pre( IS_LABELLED_BY(t->child,IDENTIFIER) );
  ccl_pre( IS_LABELLED_BY(t->child->next,PARAMS) );
  ccl_pre( IS_LABELLED_BY(t->child->next->next,RETURNS) );

  ccl_try(lustre_interpretation_exception)
    {
      ar_lustre_proto *proto;

      protoname = s_eval_identifier(t->child);
      ccl_assert( ! ar_idtable_has(interp->program->functions,protoname) );
      input_domain = s_eval_function_domain(t->child->next);
      output_domain = s_eval_function_domain(t->child->next->next);
      proto = ar_lustre_proto_create(protoname,input_domain,output_domain);

      ar_idtable_put(interp->program->functions,protoname,proto);
      ar_lustre_proto_del_reference(proto);
      error = 0;
    }
  ccl_no_catch;
  
  ccl_zdelete(ar_identifier_del_reference,protoname);
  ccl_zdelete(ar_lustre_domain_del_reference,input_domain);
  ccl_zdelete(ar_lustre_domain_del_reference,output_domain);
  
  if( error )
    INTERP_EXCEPTION();
}

			/* --------------- */

ar_lustre_program *
ar_lustre_interp(ar_lustre_tree *t)
{
  ar_lustre_program *result = NULL;

  interp = s_create_interp(t);

  ccl_try(lustre_interpretation_exception)
    {
      s_check_duplicated_definitions();

      for(; t; t = t->next)
	{
	  switch( t->node_type ) {
	  case AL_NODE          : s_eval_node(t); break;
	  case AL_CST_DECL_LIST : s_eval_const_list(t); break;
	  case AL_TYPE_DECL     : s_eval_type_decl(t); break; 
	  case AL_FUNCTION_DECL : s_eval_function_decl(t); break;
	  default               : INVALID_NODE_TYPE();
	  }
	}
      result = interp->program;
      interp->program = NULL;
    }
  ccl_no_catch;

  s_destroy_interp(interp);

  return result;
}

			/* --------------- */
