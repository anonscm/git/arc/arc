/*
 * translators.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "commands/allcmds.h"
#include "translators-p.h"

const char *AR_LUSTRE_PREF_NAMES[] = {
#define AR_LUSTRE_PREF(_enumid, _strid) "translators." _strid,
#include "preferences.def"
#undef AR_LUSTRE_PREF
};

			/* --------------- */

void
ar_lustre_translators_init(ccl_config_table *conf)
{
}

			/* --------------- */

void
ar_lustre_translators_terminate(void)
{
}

