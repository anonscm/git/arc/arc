/*
 * node.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef NODE_H
# define NODE_H

# include "ar-identifier.h"
# include "expr.h"

typedef struct ar_lustre_node_st ar_lustre_node;
typedef struct ar_lustre_equation_st {
  ccl_list *variables;
  ar_lustre_expr *value;
} ar_lustre_equation;

extern ar_lustre_node *
ar_lustre_node_create(void);

extern void
ar_lustre_node_set_name(ar_lustre_node *node, ar_identifier *name);

extern ar_identifier *
ar_lustre_node_get_name(ar_lustre_node *node);

extern void
ar_lustre_node_add_parameter(ar_lustre_node *node,
			     ar_identifier *name,
			     ar_lustre_domain *dom);

extern void
ar_lustre_node_add_input_variable(ar_lustre_node *node,
				  ar_identifier *name,
				  ar_lustre_domain *dom);

extern void
ar_lustre_node_add_output_variable(ar_lustre_node *node,
				   ar_identifier *name, 
				   ar_lustre_domain *dom);

extern void
ar_lustre_node_add_local_variable(ar_lustre_node *node,
				  ar_identifier *name, 
				  ar_lustre_domain *dom);

# define AR_LUSTRE_UNKNOWN 0
# define AR_LUSTRE_INPUT   1
# define AR_LUSTRE_OUTPUT  2
# define AR_LUSTRE_LOCAL   3
# define AR_LUSTRE_PARAMETER 4

extern int
ar_lustre_node_has_variable(ar_lustre_node *node, ar_identifier *name);

extern ar_lustre_expr *
ar_lustre_node_get_variable(ar_lustre_node *node, ar_identifier *name);

extern ar_lustre_domain *
ar_lustre_node_get_input_domain(ar_lustre_node *node);

extern ar_lustre_domain *
ar_lustre_node_get_output_domain(ar_lustre_node *node);

extern void
ar_lustre_node_add_assertion(ar_lustre_node *node, ar_lustre_expr *a);

extern void
ar_lustre_node_add_equation(ar_lustre_node *node, 
			    ccl_list *variables,
			    ar_lustre_expr *a);

extern int
ar_lustre_node_is_defined(ar_lustre_node *node, ar_identifier *varname);

extern int
ar_lustre_node_is_parameter(ar_lustre_node *node, ar_identifier *id);

extern ccl_list *
ar_lustre_node_get_parameters(ar_lustre_node *node);

extern ccl_list *
ar_lustre_node_get_input_variables(ar_lustre_node *node, int exclude_params);

extern ccl_list *
ar_lustre_node_get_output_variables(ar_lustre_node *node);

extern ccl_list *
ar_lustre_node_get_local_variables(ar_lustre_node *node);

extern ccl_list *
ar_lustre_node_get_equations(ar_lustre_node *node);

extern ccl_list *
ar_lustre_node_get_assertions(ar_lustre_node *node);

extern void
ar_lustre_node_log(ccl_log_type log, ar_lustre_node *node);

extern void
ar_lustre_node_log_gen(ccl_log_type log, ar_lustre_node *node,
		       ar_identifier_log_proc idlog);

extern ar_lustre_node *
ar_lustre_node_add_reference(ar_lustre_node *node);

extern void
ar_lustre_node_del_reference(ar_lustre_node *node);

#endif /* ! NODE_H */
