/*
 * tree.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TREE_H
# define TREE_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-parse-tree.h>

typedef ccl_parse_tree ar_lustre_tree;

enum ar_lustre_label {
  AL_BOOLEANS, AL_INTEGERS, AL_REALS, AL_IMPORTED_TYPE,
  AL_NODE, AL_NODE_SIGNATURE, AL_NODE_PARAMS, AL_NODE_PARAM,
  AL_NODE_RETURNS, AL_NODE_RETURN, AL_NODE_VARS, AL_NODE_VAR,
  AL_NODE_EQUATIONS, AL_NODE_EQUATION, AL_NODE_ASSERT,
  AL_NAME_LIST, AL_ITE, AL_OR, AL_XOR, AL_AND, AL_IMPLY, AL_NOT, AL_NEG,
  AL_EQ, AL_NEQ, AL_GT, AL_GEQ, AL_LT, AL_LEQ, AL_FOLLOWED, AL_CURRENT,
  AL_CALL, AL_ATMOST, AL_EXPR_LIST, 
  AL_TRUE, AL_FALSE, AL_UINT, AL_UFLOAT, AL_CST_NAME_LIST,
  AL_TYPE_DECL, AL_FUNCTION_DECL,
  AL_ADD, AL_SUB, AL_MUL, AL_DIV, AL_MOD, AL_WHEN, AL_PRE, 
  AL_IDENTIFIER,

  AL_CST_DECL_LIST, AL_CST_DECL,

  AL_PARAMS, AL_RETURNS, AL_TYPED_ID_LIST
};

#endif /* ! TREE_H */
