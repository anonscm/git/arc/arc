/*
 * translators.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TRANSLATORS_H
# define TRANSLATORS_H

# include <ccl/ccl-config-table.h>
# include <ccl/ccl-log.h>
# include "ar-node.h"


extern void
ar_lustre_translators_init(ccl_config_table *conf);

extern void
ar_lustre_translators_terminate(void);

extern void
ar_node_to_lustre(ar_node *node, ccl_log_type out, ccl_config_table *conf);

extern int
ar_lustre_to_altarica(const char *filename, ccl_config_table *conf);

#endif /* ! TRANSLATORS_H */
