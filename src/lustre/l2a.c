/*
 * l2a.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-translators.h"
#include "expr.h"
#include "interp.h"
#include "node.h"
#include "input.h"
#include "translators-p.h"

			/* --------------- */

#define AR_LUSTRE_TO_ALTARICA_VERBOSE 0x1
#define AR_LUSTRE_TO_ALTARICA_WARNING 0x2

			/* --------------- */

typedef struct {
  int flags;
  ar_lustre_program *program;
  ccl_list *compiled_nodes;
  ar_type *integers;
  ar_identifier *initial_state_name;
  ar_identifier *clock_event_name;
  ar_identifier *subnode_prefix;
  const char*pre_var_prefix;
  ar_identifier *tmp_flow_var_prefix;
  ar_identifier *struct_field_prefix;
  ar_identifier *real_typename;
  ar_type *reals;

  ar_identifier *real_zero;

  ar_identifier *real_add;
  ar_signature *radd_sig;

  ar_identifier *real_div;
  ar_signature *rdiv_sig;

  ar_identifier *real_mod;
  ar_signature *rmod_sig;

  ar_identifier *real_mul;
  ar_signature *rmul_sig;

  ar_identifier *real_sub;
  ar_signature *rsub_sig;

  ar_identifier *real_neg;
  ar_signature *rneg_sig;

  ar_identifier *real_lt;
  ar_signature *rlt_sig;
  ar_identifier *real_leq;
  ar_signature *rleq_sig;

  ar_identifier *real_gt;
  ar_signature *rgt_sig;
  ar_identifier *real_geq;
  ar_signature *rgeq_sig;
} TRANSLATOR;
			/* --------------- */

static void
s_compile_node(TRANSLATOR *C, ar_identifier *node);

static void
s_compile_variables(TRANSLATOR *C, ar_node *N, ar_lustre_node *NL);

static void
s_compile_var_list(TRANSLATOR *C, ar_node *N, ccl_list *vars, int flags, 
		   const char *property);

static void
s_remove_subnode_calls(TRANSLATOR *C, ar_node *N, ar_lustre_node *NL,
		       ccl_list *equations, ccl_list *assertions);

static ar_lustre_expr *
s_replace_calls(TRANSLATOR *C, ar_node *N, ar_lustre_node *NL, 
		ar_lustre_expr *e, ccl_list *assertions);

static ar_lustre_expr *
s_subnode_call(TRANSLATOR *C, ar_node *N, ar_lustre_expr *e, 
	       ccl_list *operands, ccl_list *assertions);

static ccl_list *
s_remove_pre(TRANSLATOR *C, ar_node *N, ccl_list *equations, 
	     ccl_list *assertions);

static void 
s_build_assignment_assertions(ccl_list *equations, ccl_list *assertions);

static void
s_generate_assertions(TRANSLATOR *C, ar_node *N, ccl_list *assertions,
		      ar_expr **pstate);

static ar_expr *
s_lustre_expr_to_formula(TRANSLATOR *C, ar_node *N, ar_lustre_expr *e,
			 ar_expr **pstate);

static void
s_generate_transitions(TRANSLATOR *C, ar_node *N, ccl_list *assignments,
		       ar_expr *state);

static void
s_compile_types(TRANSLATOR *C);

static void
s_compile_functions(TRANSLATOR *C);

static void
s_compile_constants(TRANSLATOR *C);

static ar_type *
s_translate_domain(TRANSLATOR *C, ar_lustre_domain *dom);

			/* --------------- */

#define PROPID(_prop) (AR_LUSTRE_PREF_NAMES[LUSTRE_PREF_ ## _prop])
# define GET_PROP(_conf,_id) ccl_config_table_get(_conf,_id)

# define GET_SPROP(_conf,_id) \
  ((GET_PROP(_conf,_id) == NULL) ? #_id "is not set" : GET_PROP(_conf,_id))

# define GET_PROP_ID(_conf,_id) \
  ar_identifier_create (GET_SPROP(_conf,_id))

int
ar_lustre_to_altarica(const char *filename, ccl_config_table *conf)
{
  int flags = 0;
  int result = 0;
  ar_lustre_program *prog;
  ar_lustre_tree *T;
  
  if( (T = ar_lustre_read_file(filename)) == NULL )
    return 0;
     
  if( ccl_config_table_get_boolean (conf, PROPID(L2A_VERBOSE)) )
    flags |= AR_LUSTRE_TO_ALTARICA_VERBOSE;
  if( ccl_config_table_get_boolean (conf, PROPID(L2A_WARNING)) )
    flags |= AR_LUSTRE_TO_ALTARICA_WARNING;

  if( (prog = ar_lustre_interp(T)) != NULL )
    {
      TRANSLATOR C;
      ar_identifier_iterator *i;

      result = 1;
      C.flags = flags;
      C.program = prog;
      C.compiled_nodes = ccl_list_create();
      C.initial_state_name = GET_PROP_ID (conf, PROPID(L2A_INITIAL_STATE_NAME));
      C.clock_event_name = GET_PROP_ID (conf, PROPID(L2A_CLOCK_EVENT_NAME));
      C.subnode_prefix = GET_PROP_ID (conf, PROPID(L2A_SUBNODES_PREFIX));
      C.tmp_flow_var_prefix = GET_PROP_ID (conf, PROPID(L2A_TMP_FLOW_VAR_PREFIX));
      C.pre_var_prefix = GET_SPROP (conf, PROPID(L2A_PRE_VAR_PREFIX));
      C.struct_field_prefix = GET_PROP_ID (conf, PROPID(L2A_STRUCT_FIELD_PREFIX));
      C.real_typename = GET_PROP_ID (conf, PROPID(L2A_REAL_TYPENAME));
      C.reals = NULL;
      C.real_zero = GET_PROP_ID (conf, PROPID(L2A_REAL_ZERO));
      C.real_add = GET_PROP_ID (conf, PROPID(L2A_REAL_ADD));
      C.radd_sig = NULL;
      C.real_div = GET_PROP_ID (conf, PROPID(L2A_REAL_DIV));
      C.rdiv_sig = NULL;
      C.real_mod = GET_PROP_ID (conf, PROPID(L2A_REAL_MOD));
      C.rmod_sig = NULL;
      C.real_mul = GET_PROP_ID (conf, PROPID(L2A_REAL_MUL));
      C.rmul_sig = NULL;
      C.real_sub = GET_PROP_ID (conf, PROPID(L2A_REAL_SUB));
      C.rsub_sig = NULL;
      C.real_neg = GET_PROP_ID (conf, PROPID(L2A_REAL_NEG));
      C.rneg_sig = NULL;

      C.real_lt = GET_PROP_ID (conf, PROPID(L2A_REAL_LT));
      C.rlt_sig = NULL;

      C.real_leq = GET_PROP_ID (conf, PROPID(L2A_REAL_LEQ));
      C.rleq_sig = NULL;

      C.real_gt = GET_PROP_ID (conf, PROPID(L2A_REAL_GT));
      C.rgt_sig = NULL;

      C.real_geq = GET_PROP_ID (conf, PROPID(L2A_REAL_GEQ));
      C.rgeq_sig = NULL;

      if( ccl_config_table_get_boolean (conf, PROPID(L2A_INTEGERS_AS_RANGE)) )
	{
	  int min = ccl_config_table_get_integer (conf, PROPID(L2A_INTEGERS_MIN));
	  int max = ccl_config_table_get_integer (conf, PROPID(L2A_INTEGERS_MAX));
	  
	  if( min > max ) 
	    { 
	      int tmp = min; 
	      min = max; 
	      max = tmp; 
	      if( (flags & AR_LUSTRE_TO_ALTARICA_WARNING) )
		ccl_warning("L2A: warning: %s > %d\n",
			    PROPID(L2A_INTEGERS_MIN), PROPID(L2A_INTEGERS_MAX));
	    }
	  
	  if( (flags & AR_LUSTRE_TO_ALTARICA_VERBOSE) )
	    ccl_warning("L2A: warning: setting integers to [%d,%d]\n",min,max);

	  C.integers = ar_type_crt_range(min,max);
	}
      else
	{
	  C.integers = ar_type_add_reference(AR_INTEGERS);
	}

      s_compile_types(&C);
      s_compile_functions(&C);
      s_compile_constants(&C);
      i = ar_idtable_get_keys(C.program->nodes);

      while( ccl_iterator_has_more_elements(i) )
	{
	  ar_identifier *id = ccl_iterator_next_element(i);
	  s_compile_node(&C,id);
	  ar_identifier_del_reference(id);
	}
      ccl_iterator_delete(i);
      ar_type_del_reference(C.integers);
      ccl_list_clear_and_delete(C.compiled_nodes,(ccl_delete_proc *)
				ar_identifier_del_reference);
      ar_identifier_del_reference(C.initial_state_name);
      ar_identifier_del_reference(C.clock_event_name);
      ar_identifier_del_reference(C.subnode_prefix);
      ar_identifier_del_reference(C.tmp_flow_var_prefix);
      ar_identifier_del_reference(C.struct_field_prefix);
      ar_identifier_del_reference(C.real_typename);
      ccl_zdelete(ar_type_del_reference,C.reals);
      ar_identifier_del_reference(C.real_zero);
      ar_identifier_del_reference(C.real_add);
      ccl_zdelete(ar_signature_del_reference,C.radd_sig);
      ar_identifier_del_reference(C.real_mul);
      ccl_zdelete(ar_signature_del_reference,C.rmul_sig);
      ar_identifier_del_reference(C.real_sub);
      ccl_zdelete(ar_signature_del_reference,C.rsub_sig);
      ar_identifier_del_reference(C.real_div);
      ccl_zdelete(ar_signature_del_reference,C.rdiv_sig);
      ar_identifier_del_reference(C.real_mod);
      ccl_zdelete(ar_signature_del_reference,C.rmod_sig);
      ar_identifier_del_reference(C.real_neg);
      ccl_zdelete(ar_signature_del_reference,C.rneg_sig);

      ar_identifier_del_reference(C.real_lt);
      ccl_zdelete(ar_signature_del_reference,C.rlt_sig);

      ar_identifier_del_reference(C.real_leq);
      ccl_zdelete(ar_signature_del_reference,C.rleq_sig);

      ar_identifier_del_reference(C.real_gt);
      ccl_zdelete(ar_signature_del_reference,C.rgt_sig);

      ar_identifier_del_reference(C.real_geq);
      ccl_zdelete(ar_signature_del_reference,C.rgeq_sig);

      ar_lustre_program_delete(C.program);
    }

  ccl_parse_tree_delete_tree(T);

  return result;
}

			/* --------------- */

static void
s_compile_node(TRANSLATOR *C, ar_identifier *nodename)
{
  ar_node *N;
  ar_lustre_node *NL;
  ccl_list *equations;
  ccl_list *assertions;
  ccl_list *assignments;
  ar_expr *state = NULL;

  if( ccl_list_has(C->compiled_nodes,nodename) )
    return;

  if( C->flags & AR_LUSTRE_TO_ALTARICA_VERBOSE )
    {
      ccl_warning("L2A: translating lustre node '");
      ar_identifier_log(CCL_LOG_WARNING,nodename);
      ccl_warning("'.\n");
    }

  NL = (ar_lustre_node *)ar_idtable_get(C->program->nodes,nodename);

  ccl_assert( NL != NULL );

  N = ar_node_create(AR_MODEL_CONTEXT);
  ar_node_set_name(N,nodename);

  equations  = ccl_list_create();
  assertions = ccl_list_create();
  s_remove_subnode_calls(C,N,NL,equations,assertions);

  s_compile_variables(C,N,NL);

  assignments = s_remove_pre(C,N,equations,assertions);
  s_build_assignment_assertions(equations,assertions);
  s_generate_assertions(C,N,assertions,&state);
  s_generate_transitions(C,N,assignments,state);

  if( ar_node_get_nb_subnodes(N) > 0 )
    {
      ar_identifier_iterator *subi = ar_node_get_names_of_subnodes (N);
      ar_broadcast *sync = ar_broadcast_create(1);

      ar_broadcast_add_event(sync,C->clock_event_name,0);

      while( ccl_iterator_has_more_elements(subi) )
	{
	  ar_identifier *subid = ccl_iterator_next_element(subi);
	  ar_identifier *id = 
	    ar_identifier_add_prefix(C->clock_event_name,subid);

	  ar_broadcast_add_event(sync,id,0);

	  ar_identifier_del_reference(id);
	  ar_identifier_del_reference(subid);
	}
      ar_node_add_broadcast(N,sync);

      ccl_iterator_delete(subi);
      ar_broadcast_del_reference(sync);

      /* add empty broadcast */
      sync = ar_broadcast_create(1);
      ar_node_add_broadcast(N,sync);
      ar_broadcast_del_reference(sync);
    }

  ccl_zdelete(ar_expr_del_reference,state);

  {
    ccl_pair *p;

    for(p = FIRST(equations); p; p = CDDR(p))
      ar_lustre_expr_del_reference(CADR(p));
    ccl_list_delete(equations);
  }

  ccl_list_clear_and_delete(assertions,(ccl_delete_proc *)
			    ar_lustre_expr_del_reference);	


  ccl_list_clear_and_delete(assignments,(ccl_delete_proc *)
			    ar_identifier_del_reference);	
	
  if( (C->flags & AR_LUSTRE_TO_ALTARICA_WARNING) &&
      ar_model_has_node(nodename) )
    {
      ccl_warning("L2A: warning: replacing node '");
      ar_identifier_log(CCL_LOG_WARNING,nodename);
      ccl_warning("' by its LUSTRE version.\n");
    }

  ar_model_set_node(nodename,N);
  ccl_list_add(C->compiled_nodes,ar_identifier_add_reference(nodename));

  ar_lustre_node_del_reference(NL);
  ar_node_del_reference(N);
}

			/* --------------- */

static void
s_compile_variables(TRANSLATOR *C, ar_node *N, ar_lustre_node *NL)
{
  ccl_list *vars;

  vars = ar_lustre_node_get_parameters(NL);

  s_compile_var_list(C,N,vars,AR_SLOT_FLAG_FLOW_VAR|AR_SLOT_FLAG_PARENT|
		     AR_SLOT_FLAG_IN,
		     "in");
  ccl_list_delete(vars);

  vars = ar_lustre_node_get_input_variables(NL,1);
  s_compile_var_list(C,N,vars,AR_SLOT_FLAG_FLOW_VAR|AR_SLOT_FLAG_PARENT|
		     AR_SLOT_FLAG_IN,
		     "in");
  ccl_list_delete(vars);

  vars = ar_lustre_node_get_output_variables(NL);
  s_compile_var_list(C,N,vars,AR_SLOT_FLAG_FLOW_VAR|AR_SLOT_FLAG_PARENT|
		     AR_SLOT_FLAG_OUT,
		     "out");
  ccl_list_delete(vars);

  vars = ar_lustre_node_get_local_variables(NL);
  s_compile_var_list(C,N,vars,AR_SLOT_FLAG_FLOW_VAR|AR_SLOT_FLAG_PRIVATE,
		     "private");
  ccl_list_delete(vars);
}

			/* --------------- */

static ar_identifier *
s_flow_to_altarica(ar_identifier *id)
{
  return ar_identifier_add_reference(id);
}

			/* --------------- */

static void
s_compile_var_list(TRANSLATOR *C, ar_node *N, ccl_list *vars, int flags, 
		   const char *property)
{
  ccl_pair *p;
  ar_lustre_expr *l_var;
  ar_identifier *l_var_name;
  ar_identifier *a_var_name;
  ar_lustre_domain *l_dom;
  ar_type *a_dom;
  ccl_list *properties;
  ar_identifier *prop;

  if( (flags & AR_SLOT_FLAG_PARAMETER) )
    {
      properties = NULL;
      prop = NULL;
    }
  else
    {
      properties = ccl_list_create();
      prop = ar_identifier_create(property);
      ccl_list_add(properties,prop);
    }

  for(p = FIRST(vars); p; p = CDR(p))
    {
      l_var = (ar_lustre_expr *)CAR(p);
      l_var_name = ar_lustre_expr_get_name(l_var);
      a_var_name = s_flow_to_altarica(l_var_name);
      l_dom = ar_lustre_expr_get_domain(l_var);

      a_dom = s_translate_domain(C,l_dom);
      
      if( (flags & AR_SLOT_FLAG_PARAMETER) )
	ar_node_add_local_parameter(N,a_var_name,a_dom,NULL);
      else
	ar_node_add_variable(N,a_var_name,a_dom,flags,properties);

      ar_type_del_reference(a_dom);
      ar_lustre_domain_del_reference(l_dom);
      ar_identifier_del_reference(l_var_name);
      ar_identifier_del_reference(a_var_name);
    }

  ccl_zdelete(ar_identifier_del_reference,prop);
  ccl_zdelete(ccl_list_delete,properties);
}

			/* --------------- */

static void
s_remove_subnode_calls(TRANSLATOR *C, ar_node *N, ar_lustre_node *NL,
		       ccl_list *equations, ccl_list *assertions)
{
  ccl_pair *p;
  ccl_list *l = ar_lustre_node_get_equations(NL);
  
  for(p = FIRST(l); p; p = CDR(p))
    {
      ar_lustre_equation *equation = CAR(p);
      ar_lustre_expr *f = s_replace_calls(C,N,NL,equation->value,assertions);
      ccl_list_add(equations,equation->variables);
      ccl_list_add(equations,f);
    }
  ccl_list_delete(l);

  l = ar_lustre_node_get_assertions(NL);
  for(p = FIRST(l); p; p = CDR(p))
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      ar_lustre_expr *f = s_replace_calls(C,N,NL,e,assertions);
      ccl_list_add(assertions,f);
    }
  ccl_list_delete(l);
}

			/* --------------- */

#define OP1(e) ((ar_lustre_expr *)(CAR(FIRST((e)))))
#define OP2(e) ((ar_lustre_expr *)(CADR(FIRST((e)))))
#define OP3(e) ((ar_lustre_expr *)(CADDR(FIRST((e)))))

			/* --------------- */

static int
s_exist_local_in_lustre_node(ar_identifier *id, void *data)
{
  return 
    ar_lustre_node_has_variable((ar_lustre_node *)data,id) == AR_LUSTRE_LOCAL;
}

			/* --------------- */

static ar_identifier *
s_create_tmp_symbol(TRANSLATOR *C, 
		    int (*exist)(ar_identifier *id, void *data), 
		    void *data)
{
  int i = 0;
  char tmp[100];
  ar_identifier *name;

  for(;;)
    {
      sprintf(tmp,"%d",i++);
      name = ar_identifier_rename_with_suffix(C->tmp_flow_var_prefix,tmp);
      if( ! exist(name,data) )
	break;
      ar_identifier_del_reference(name);
    }

  return name;
}

			/* --------------- */

static ar_lustre_expr *
s_replace_calls(TRANSLATOR *C, ar_node *N, ar_lustre_node *NL,
		ar_lustre_expr *e, ccl_list *assertions)
{
  ccl_pair *p;
  ar_lustre_expr_type type = ar_lustre_expr_get_type(e);
  ar_lustre_expr *result = NULL;
  ccl_list *rcallops = NULL;
  ccl_list *operands = ar_lustre_expr_get_operands(e);

  if( operands == NULL )
    return ar_lustre_expr_add_reference(e);

  ccl_try(translation_exception)
    {
      rcallops = ccl_list_create();
      for(p = FIRST(operands); p; p = CDR(p))
	{
	  ar_lustre_expr *e = 
	    s_replace_calls(C,N,NL,(ar_lustre_expr *)CAR(p),assertions);
	  ccl_list_add(rcallops,e);
	}

      if( (type == AL_EXPR_NODE_CALL || type == AL_EXPR_FUNC_CALL) &&
	  ccl_list_get_size(rcallops) == 1 &&
	  ar_lustre_expr_get_type(CAR(FIRST(rcallops)))== AL_EXPR_EXPR_LIST )
	{
	  ccl_list *ops = ar_lustre_expr_get_operands(CAR(FIRST(rcallops)));
	  ccl_list_clear_and_delete(rcallops,(ccl_delete_proc *)
				    ar_lustre_expr_del_reference);
	  rcallops = ops;
	}

      switch( type ) {
      case AL_EXPR_NOT : 
	result = ar_lustre_expr_crt_not(OP1(rcallops));
	break;
      case AL_EXPR_NEG :
	result = ar_lustre_expr_crt_neg(OP1(rcallops));
	break;
      case AL_EXPR_ITE:
	result = ar_lustre_expr_crt_ite(OP1(rcallops),OP2(rcallops),
					OP3(rcallops));
	break;
      case AL_EXPR_OR:
	result = ar_lustre_expr_crt_or(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_AND:
	result = ar_lustre_expr_crt_and(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_IMPLY:
	result = ar_lustre_expr_crt_imply(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_ADD:
	result = ar_lustre_expr_crt_add(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_SUB:
	result = ar_lustre_expr_crt_sub(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_MUL:
	result = ar_lustre_expr_crt_mul(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_DIV:
	result = ar_lustre_expr_crt_div(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_MOD:
	result = ar_lustre_expr_crt_mod(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_EQ:
	result = ar_lustre_expr_crt_eq(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_NEQ:
	result = ar_lustre_expr_crt_neq(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_LT:
	result = ar_lustre_expr_crt_lt(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_LEQ: 
	result = ar_lustre_expr_crt_leq(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_GT:
	result = ar_lustre_expr_crt_gt(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_GEQ:
	result = ar_lustre_expr_crt_geq(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_WHEN: 
	result = ar_lustre_expr_crt_when(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_ATMOST: 
	result = ar_lustre_expr_crt_atmost(rcallops);
	break;
      case AL_EXPR_EXPR_LIST: 
	result = ar_lustre_expr_crt_expr_list(rcallops);
	break;
      case AL_EXPR_NODE_CALL:
	result = s_subnode_call(C,N,e,rcallops,assertions);
	break;
      case AL_EXPR_FUNC_CALL:
	{
	  ar_lustre_proto *p = ar_lustre_expr_get_proto(e);

	  result = ar_lustre_expr_crt_func_call(p,rcallops);
	  ar_lustre_proto_del_reference(p);
	}
	break;
      case AL_EXPR_FOLLOWED:
	result = ar_lustre_expr_crt_followed(OP1(rcallops),OP2(rcallops));
	break;
      case AL_EXPR_PRE : 
	result = ar_lustre_expr_crt_pre(OP1(rcallops));
	break;

      case AL_EXPR_CURRENT: 
	{
	  ar_lustre_expr *op = OP1(rcallops);
      
	  if( ar_lustre_expr_get_type(op) == AL_EXPR_WHEN )
	    {
	      /* current( X when B ) --> Y = if B then X else pre(Y) */
	      ar_lustre_expr *pre_Y;
	      ar_lustre_expr *a;
	      ar_lustre_expr *ite;
	      ar_identifier *Y = 
		s_create_tmp_symbol(C,s_exist_local_in_lustre_node,NL);
	      ccl_list *when_op = ar_lustre_expr_get_operands(op);
	      ar_lustre_expr *X = OP1(when_op);
	      ar_lustre_expr *B = OP2(when_op);
	      ar_lustre_domain *dom = ar_lustre_expr_get_domain(X);

	      ar_lustre_node_add_local_variable(NL,Y,dom);

	      result = ar_lustre_node_get_variable(NL,Y);
	      pre_Y = ar_lustre_expr_crt_pre(result);
	      ite = ar_lustre_expr_crt_ite(B,X,pre_Y);
	      a = ar_lustre_expr_crt_eq(result,ite);
	      ar_lustre_node_add_assertion(NL,a);

	      ar_lustre_expr_del_reference(ite);
	      ar_lustre_expr_del_reference(a);
	      ar_lustre_expr_del_reference(pre_Y);
	      ar_lustre_domain_del_reference(dom);
	      ar_identifier_del_reference(Y);
	      ccl_list_clear_and_delete(when_op,(ccl_delete_proc *)
					ar_lustre_expr_del_reference);
	    }
	  else
	    {
	      ccl_error("L2A: error: can't translate expression '");
	      ar_lustre_expr_log(CCL_LOG_ERROR,e);
	      ccl_error("'\n");
	      ccl_throw_no_msg (translation_exception);
	    }
	}
	break;

      case AL_EXPR_BCST: case AL_EXPR_ICST: case AL_EXPR_RCST: 
      case AL_EXPR_VAR: case AL_EXPR_ACST: 
	ccl_throw_no_msg (internal_error);
      };
    }
  ccl_no_catch;

  if( operands != NULL )
    ccl_list_clear_and_delete(operands,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
  if( rcallops != NULL )
    ccl_list_clear_and_delete(rcallops,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);

  if( result == NULL )
    ccl_rethrow();

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_subnode_call(TRANSLATOR *C, ar_node *N, ar_lustre_expr *e, ccl_list *operands,
	       ccl_list *assertions)
{
  ar_node *sub;
  ar_identifier *subid;
  ar_lustre_node *subnl;
  ar_lustre_expr *result = NULL;


  {
    ar_identifier *nodename = ar_lustre_expr_get_name(e);

    s_compile_node(C,nodename);
    sub = ar_model_get_node(nodename);
    ccl_assert( sub != NULL );

    {
      char tmp[10];
      ar_type *subtype = ar_node_get_configuration_type(sub);

      sprintf(tmp,"%d",ar_node_get_nb_subnodes(N));
      subid = ar_identifier_rename_with_suffix(C->subnode_prefix,tmp);
      ar_node_add_subnode(N,subid,sub,subtype);
      ar_type_del_reference(subtype);
    }

    ar_node_del_reference(sub);  
    subnl = (ar_lustre_node *)ar_idtable_get(C->program->nodes,nodename);
    ccl_pre( subnl != NULL );

    ar_identifier_del_reference(nodename);
  }

  {
    ccl_pair *p;
    ccl_list *outputs = ar_lustre_node_get_output_variables(subnl);
    ccl_list *sub_outputs = ccl_list_create();

    for(p = FIRST(outputs); p; p = CDR(p))
      {
	ar_lustre_expr *var = (ar_lustre_expr *)CAR(p);
	ar_identifier *vname = ar_lustre_expr_get_name(var);
	ar_identifier *subvname = ar_identifier_add_prefix(vname,subid);
	ar_lustre_domain *dom = ar_lustre_expr_get_domain(var);      
	
	var = ar_lustre_expr_crt_variable(subvname,dom);
	ccl_list_add(sub_outputs,var);
	
	ar_lustre_domain_del_reference(dom);
	ar_identifier_del_reference(subvname);
	ar_identifier_del_reference(vname);
      }
    result = ar_lustre_expr_crt_expr_list(sub_outputs);

    ccl_list_delete(outputs);
    ccl_list_clear_and_delete(sub_outputs,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
  }

  {
    ccl_pair *p;
    ccl_pair *pp;
    ccl_list *inputs = ar_lustre_node_get_input_variables(subnl,0);
    ccl_list *params = ccl_list_deep_dup(operands,(ccl_duplicate_func *)
					 ar_lustre_expr_add_reference);

    for(p = FIRST(inputs), pp = FIRST(params); p && pp; 
	p = CDR(p), pp = CDR(pp))
      {
	ar_lustre_expr *var = (ar_lustre_expr *)CAR(p);
	ar_identifier *vname = ar_lustre_expr_get_name(var);
	ar_identifier *subvname = ar_identifier_add_prefix(vname,subid);
	ar_lustre_domain *dom = ar_lustre_expr_get_domain(var);      
	ar_lustre_expr *eq;

	var = ar_lustre_expr_crt_variable(subvname,dom);
	eq = ar_lustre_expr_crt_eq(var,CAR(pp));
	ccl_list_add(assertions,eq);

	ar_lustre_expr_del_reference(var);
	ar_lustre_domain_del_reference(dom);
	ar_identifier_del_reference(subvname);
	ar_identifier_del_reference(vname);
      }
    ccl_list_delete(inputs);
    ccl_list_clear_and_delete(params,(ccl_delete_proc *)
			      ar_lustre_expr_del_reference);
  }

  ar_identifier_del_reference(subid);
  ar_lustre_node_del_reference(subnl);

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_remove_pre_in_expr(TRANSLATOR *C, ccl_list *assignments, ccl_list *assertions,
		     ar_node *N, ar_lustre_expr *e);

static ccl_list *
s_remove_pre_in_list(TRANSLATOR *C, ccl_list *assignments, 
		     ccl_list *assertions, ar_node *N, ccl_list *l)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create();

  for(p = FIRST(l); p; p = CDR(p))
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      ar_lustre_expr *pre = s_remove_pre_in_expr(C,assignments,assertions,N,e);
      ccl_list_add(result,pre);
    }

  return result;
}

			/* --------------- */

static int
s_exist_flow_in_altarica_node(ar_identifier *id, void *data)
{
  return ar_node_has_flow_variable((ar_node *)data,id);
}

			/* --------------- */

static ar_identifier *
s_create_tmpflow(TRANSLATOR *C, ar_node *N)
{
  return s_create_tmp_symbol(C,s_exist_flow_in_altarica_node,N);
}

			/* --------------- */


static ar_identifier *
s_build_pre_identifier(TRANSLATOR *C, ar_identifier *id)
{  
  return ar_identifier_rename_with_prefix(id,C->pre_var_prefix);
}

			/* --------------- */

static ar_lustre_expr *
s_compile_pre(TRANSLATOR *C, ccl_list *assignments, ccl_list *assertions,
	      ar_node *N, ar_lustre_expr *e)
{
  ar_lustre_expr *result;
  ar_lustre_expr_type type = ar_lustre_expr_get_type(e);

  if( type == AL_EXPR_VAR )
    {
      ar_lustre_domain *dom = ar_lustre_expr_get_domain(e);
      ar_identifier *name = ar_lustre_expr_get_name(e);
      ar_identifier *pre_var_name = s_build_pre_identifier(C,name);

      if( ! ar_node_has_state_variable(N,pre_var_name) )
	{
	  ar_context_slot *sl;
	  ar_type *sltype = s_translate_domain(C,dom);
	  ar_expr *init = NULL;

	  
	  if( ar_lustre_domain_is_boolean(dom) )
	    init = ar_expr_crt_false();	    
	  else if( ar_lustre_domain_is_real(dom) )
	    {
	      ar_context_slot *p = ar_model_get_parameter(C->real_zero);
	      init = ar_expr_crt_parameter(p);
	      ar_context_slot_del_reference(p);
	    }
	  else if( ar_lustre_domain_is_abstract(dom) )
	    {
	      if( (C->flags & AR_LUSTRE_TO_ALTARICA_WARNING) )
		{
		  ccl_warning("L2A: warning: can't assign an initial value "
			      "to pre(");
		  ar_identifier_log(CCL_LOG_WARNING,name);
		  ccl_warning(") for values taken in the abstract "
			      "domain '");
		  ar_lustre_domain_log(CCL_LOG_WARNING,dom);
		  ccl_warning("'.\n");
		}
	    }
	  else
	    {
	      ccl_assert( ! ar_lustre_domain_is_composite(dom) );
	      init = ar_expr_crt_integer(0);
	    }

	  ar_node_add_variable(N,pre_var_name,sltype,
			       AR_SLOT_FLAG_STATE_VAR|AR_SLOT_FLAG_PRIVATE,
			       NULL);
	  sl = ar_node_get_variable_slot (N,pre_var_name);

	  if( init != NULL )
	    {
	      ar_node_add_initial_state(N,sl,init);
	      ar_expr_del_reference(init);
	    }
	  ar_type_del_reference(sltype);
	  ar_context_slot_del_reference(sl);
	}

      if( ! ccl_list_has(assignments,pre_var_name) )
	{
	  ccl_list_add(assignments,ar_identifier_add_reference(pre_var_name));
	  ccl_list_add(assignments,ar_identifier_add_reference(name));
	}

      result = ar_lustre_expr_crt_variable(pre_var_name,dom);

      ar_lustre_domain_del_reference(dom);
      ar_identifier_del_reference(name);
      ar_identifier_del_reference(pre_var_name);
    }
  else if( type == AL_EXPR_EXPR_LIST )
    {
      ccl_pair *p;
      ccl_list *operands = ar_lustre_expr_get_operands(e);
      ccl_list *tmp = ccl_list_create();

      ccl_assert( operands != NULL );

      for(p = FIRST(operands); p; p = CDR(p))
	{
	  ar_lustre_expr *pre = 
	    s_compile_pre(C,assignments,assertions,N,(ar_lustre_expr *)CAR(p));
	  ccl_list_add(tmp,pre);
	}
      result = ar_lustre_expr_crt_expr_list(tmp);

      ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_list_clear_and_delete(operands,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
    }
  else
    {
      ar_identifier *tmpflow = s_create_tmpflow(C,N);
      ar_lustre_domain *dom = ar_lustre_expr_get_domain(e);
      ar_lustre_expr *tmpvar = ar_lustre_expr_crt_variable(tmpflow,dom);
      ar_lustre_expr *ass = ar_lustre_expr_crt_eq(tmpvar,e);
      int flags = AR_SLOT_FLAG_FLOW_VAR|AR_SLOT_FLAG_PRIVATE;
      ccl_list *props = ccl_list_create();
      ar_identifier *prop = ar_identifier_create("private");
      ar_type *sltype = s_translate_domain(C,dom);

      ccl_list_add(props,prop);
      
      ar_node_add_variable(N,tmpflow,sltype,flags,props);

      ccl_list_add(assertions,ar_lustre_expr_add_reference(ass));

      result = s_compile_pre(C,assignments,assertions,N,tmpvar);

      ar_type_del_reference(sltype);
      ar_lustre_domain_del_reference(dom);
      ar_lustre_expr_del_reference(ass);
      ar_lustre_expr_del_reference(tmpvar);
      ar_identifier_del_reference(tmpflow);
      ccl_list_delete(props);
      ar_identifier_del_reference(prop);
    }

  return result;
}

			/* --------------- */

static ar_lustre_expr *
s_remove_pre_in_expr(TRANSLATOR *C, ccl_list *assignments, ccl_list *assertions,
		     ar_node *N, ar_lustre_expr *e)
{
  ar_lustre_expr_type type = ar_lustre_expr_get_type(e);
  ar_lustre_expr *result = NULL;
  ccl_list *pre_operands = NULL;
  ccl_list *operands = ar_lustre_expr_get_operands(e);

  if( operands == NULL )
    return ar_lustre_expr_add_reference(e);

  pre_operands = s_remove_pre_in_list(C,assignments,assertions,N,operands);
  if( ccl_list_equals(operands,pre_operands) && type != AL_EXPR_PRE )
    {
      ccl_list_clear_and_delete(operands,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_list_clear_and_delete(pre_operands,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);

      return ar_lustre_expr_add_reference(e);
    }

  switch( type ) {
  case AL_EXPR_NOT : 
    result = ar_lustre_expr_crt_not(OP1(pre_operands));
    break;
  case AL_EXPR_NEG :
    result = ar_lustre_expr_crt_neg(OP1(pre_operands));
    break;
  case AL_EXPR_ITE:
    result = ar_lustre_expr_crt_ite(OP1(pre_operands),OP2(pre_operands),
				    OP3(pre_operands));
    break;
  case AL_EXPR_OR:
    result = ar_lustre_expr_crt_or(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_AND:
    result = ar_lustre_expr_crt_and(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_IMPLY:
    result = ar_lustre_expr_crt_imply(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_ADD:
    result = ar_lustre_expr_crt_add(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_SUB:
    result = ar_lustre_expr_crt_sub(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_MUL:
    result = ar_lustre_expr_crt_mul(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_DIV:
    result = ar_lustre_expr_crt_div(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_MOD:
    result = ar_lustre_expr_crt_mod(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_EQ:
    result = ar_lustre_expr_crt_eq(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_NEQ:
    result = ar_lustre_expr_crt_neq(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_LT:
    result = ar_lustre_expr_crt_lt(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_LEQ: 
    result = ar_lustre_expr_crt_leq(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_GT:
    result = ar_lustre_expr_crt_gt(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_GEQ:
    result = ar_lustre_expr_crt_geq(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_FOLLOWED:
    result = ar_lustre_expr_crt_followed(OP1(pre_operands),OP2(pre_operands));
    break;
  case AL_EXPR_ATMOST: 
    result = ar_lustre_expr_crt_atmost(pre_operands);
    break;
  case AL_EXPR_EXPR_LIST: 
    result = ar_lustre_expr_crt_expr_list(pre_operands);
    break;
  case AL_EXPR_PRE : 
    result = s_compile_pre(C,assignments,assertions,N,OP1(pre_operands));
    break;
  case AL_EXPR_FUNC_CALL: 
    {
      ar_lustre_proto *p = ar_lustre_expr_get_proto(e);
      result = ar_lustre_expr_crt_func_call(p,pre_operands);
      ar_lustre_proto_del_reference(p);
    }
    break;

  case AL_EXPR_WHEN: 
    ccl_error("L2A: error: can't translate expression '");
    ar_lustre_expr_log(CCL_LOG_ERROR,e);
    ccl_error("'.\n");
    break;

  case AL_EXPR_BCST: case AL_EXPR_ACST: case AL_EXPR_ICST: case AL_EXPR_RCST: 
  case AL_EXPR_VAR:
    /* tackled by the test at the beginning of the function */

  case AL_EXPR_NODE_CALL: 
    /* removed by previous operations */

  case AL_EXPR_CURRENT:
    /* removed by previous operations */ 
    break;

  default : 
    ccl_throw_no_msg (internal_error);
    break;
  };

  ccl_list_clear_and_delete(operands,(ccl_delete_proc *)
			    ar_lustre_expr_del_reference);
  ccl_list_clear_and_delete(pre_operands,(ccl_delete_proc *)
			    ar_lustre_expr_del_reference);

  if( result == NULL )
    ccl_throw_no_msg (translation_exception);

  return result;
}

			/* --------------- */

static ccl_list *
s_remove_pre(TRANSLATOR *C, ar_node *N, ccl_list *equations, 
	     ccl_list *assertions)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create();

  for(p = FIRST(equations); p; p = CDDR(p))
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CADR(p);
      CADR(p) = s_remove_pre_in_expr(C,result,assertions,N,e);
      ar_lustre_expr_del_reference(e);
    }

  for(p = FIRST(assertions); p; p = CDR(p))
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      CAR(p) = s_remove_pre_in_expr(C,result,assertions,N,e);
      ar_lustre_expr_del_reference(e);
    }

  return result;
}

			/* --------------- */

static void 
s_build_assignment_assertions(ccl_list *equations, ccl_list *assertions) 
{
  ccl_pair *p;

  for(p = FIRST(equations); p; p = CDDR(p))
    {
      ar_lustre_expr *var = ar_lustre_expr_crt_expr_list(CAR(p));
      ar_lustre_expr *val = (ar_lustre_expr *)CADR(p);
      ar_lustre_expr *eq = ar_lustre_expr_crt_eq(var,val);
      ar_lustre_domain *dom1 = ar_lustre_expr_get_domain(var);
      ar_lustre_domain *dom2 = ar_lustre_expr_get_domain(val);
      ccl_assert( ar_lustre_domain_equals(dom1,dom2) );
      ar_lustre_domain_del_reference(dom1);
      ar_lustre_domain_del_reference(dom2);
      ccl_list_add(assertions,eq);
      ar_lustre_expr_del_reference(var);
    }
}

			/* --------------- */

static void
s_generate_assertions(TRANSLATOR *C, ar_node *N, ccl_list *assertions,
		      ar_expr **pstate)
{
  ccl_pair *p;

  for(p = FIRST(assertions); p; p = CDR(p))
    {
      ar_expr *a = 
	s_lustre_expr_to_formula(C,N,(ar_lustre_expr *)CAR(p),pstate);
      ar_node_add_assertion(N,a);
      ar_expr_del_reference(a);
    }
}

			/* --------------- */

#define AOP1(e) ((ar_expr *)(CAR(FIRST((e)))))
#define AOP2(e) ((ar_expr *)(CADR(FIRST((e)))))
#define AOP3(e) ((ar_expr *)(CADDR(FIRST((e)))))

static ar_expr *
s_atmost_to_formula_rec(TRANSLATOR *C, ar_node *N, ccl_pair *p, int one)
{
  ar_expr *result;
  ar_expr *A = (ar_expr *)CAR(p);

  if( CDR(p) == NULL )
    {
      if( one ) result = ar_expr_crt_unary(AR_EXPR_NOT,A);
      else result = ar_expr_add_reference(A);
    }
  else if( one )
    {
      ar_expr *B = s_atmost_to_formula_rec(C,N,CDR(p),one);
      ar_expr *not_A = ar_expr_crt_unary(AR_EXPR_NOT,A);
      result = ar_expr_crt_binary(AR_EXPR_AND,not_A,B);
      ar_expr_del_reference(B);
      ar_expr_del_reference(not_A);
    }
  else 
    {
      ar_expr *B = s_atmost_to_formula_rec(C,N,CDR(p),0);
      ar_expr *X = s_atmost_to_formula_rec(C,N,CDR(p),1);
      ar_expr *not_A = ar_expr_crt_unary(AR_EXPR_NOT,A);
      ar_expr *not_A_and_B = ar_expr_crt_binary(AR_EXPR_AND,not_A,B);

      result = ar_expr_crt_binary(AR_EXPR_OR,not_A_and_B,X);
      ar_expr_del_reference(X);
      ar_expr_del_reference(B);
      ar_expr_del_reference(not_A);
      ar_expr_del_reference(not_A_and_B);
    }

  return result;
}

			/* --------------- */

static ar_expr *
s_atmost_to_formula(TRANSLATOR *C, ar_node *N, ccl_pair *p)
{
  ar_expr *result;
  ar_expr *A = (ar_expr *)CAR(p);

  if( CDR(p) == NULL )
    result = ar_expr_crt_true();
  else 
    {
      ar_expr *B = s_atmost_to_formula_rec(C,N,CDR(p),0);
      ar_expr *X = s_atmost_to_formula_rec(C,N,CDR(p),1);
      ar_expr *not_A = ar_expr_crt_unary(AR_EXPR_NOT,A);
      ar_expr *not_A_and_B = ar_expr_crt_binary(AR_EXPR_AND,not_A,B);

      result = ar_expr_crt_binary(AR_EXPR_OR,not_A_and_B,X);

      ar_expr_del_reference(X);
      ar_expr_del_reference(B);
      ar_expr_del_reference(not_A);
      ar_expr_del_reference(not_A_and_B);
    }

  return result;
}

			/* --------------- */

static ar_expr *
s_mk_imply(ar_expr *a, ar_expr *b)
{
  ar_expr *not_a = ar_expr_crt_unary(AR_EXPR_NOT,a);
  ar_expr *not_a_or_b = ar_expr_crt_binary(AR_EXPR_OR,not_a,b);
  ar_expr_del_reference(not_a);

  return not_a_or_b;
}

			/* --------------- */

static ar_signature *
s_crt_real_op(TRANSLATOR *C, ar_identifier *id, int arity, ar_type *out)
{
  ar_signature *result = ar_signature_create(id,out);

  while( arity-- )
    ar_signature_add_arg_domain(result,C->reals);

  ar_model_set_signature(id,result);

  return result;
}

			/* --------------- */

static ar_expr *
s_lustre_expr_to_formula(TRANSLATOR *C, ar_node *N, ar_lustre_expr *e,
			 ar_expr **pstate)
{
  ccl_pair *p;
  ar_identifier *id;
  ar_lustre_expr_type type = ar_lustre_expr_get_type(e);
  ar_expr *R = NULL;
  ccl_list *model_operands = NULL;
  ccl_list *operands = ar_lustre_expr_get_operands(e);

  if( operands != NULL )
    {
      model_operands = ccl_list_create();
      for(p = FIRST(operands); p; p = CDR(p))
	{
	  ar_lustre_expr *ex = (ar_lustre_expr *)CAR(p);
	  ar_expr *F = s_lustre_expr_to_formula(C,N,ex,pstate);
	  ccl_list_add(model_operands,F);
	}
    }

  switch( type ) {
  case AL_EXPR_BCST: 
    if( ar_lustre_expr_get_ival(e) ) 
      R = ar_expr_crt_true();
    else 
      R = ar_expr_crt_false();
    break;

  case AL_EXPR_ICST: 
    R = ar_expr_crt_integer(ar_lustre_expr_get_ival(e));
    break;

  case AL_EXPR_VAR:
    {
      ar_context_slot *sl;
      id = ar_lustre_expr_get_name(e);    
      sl = ar_node_get_variable_slot (N,id);
      R = ar_expr_crt_variable(sl);
      ar_context_slot_del_reference(sl);
      ar_identifier_del_reference(id);
      ccl_post( R != NULL );
    }
    break;

  case AL_EXPR_NOT : 
    R = ar_expr_crt_unary(AR_EXPR_NOT,AOP1(model_operands));
    break;
  case AL_EXPR_NEG :
    if( ar_lustre_expr_is_real(e) )
      {
	ar_expr *arg = AOP1(model_operands);

	if( C->rneg_sig == NULL )
	  C->rneg_sig = s_crt_real_op(C,C->real_neg,1,C->reals);
	R = ar_expr_crt_function_call(C->rneg_sig,&arg);
      }
    else
      {
	R = ar_expr_crt_unary(AR_EXPR_NEG,AOP1(model_operands));      
      }
    break;
  case AL_EXPR_ITE:
    R = ar_expr_crt_ternary(AR_EXPR_ITE,AOP1(model_operands),
			    AOP2(model_operands),AOP3(model_operands));
    break;
  case AL_EXPR_OR:
    R = ar_expr_crt_binary(AR_EXPR_OR,AOP1(model_operands),
			   AOP2(model_operands));
    break;
  case AL_EXPR_AND:
    R = ar_expr_crt_binary(AR_EXPR_AND,AOP1(model_operands),
			   AOP2(model_operands));
    break;
  case AL_EXPR_IMPLY:
    R = s_mk_imply(AOP1(model_operands),AOP2(model_operands));
    break;
  case AL_EXPR_ADD:
    if( ar_lustre_expr_is_real(e) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->radd_sig == NULL )
	  C->radd_sig = s_crt_real_op(C,C->real_add,2,C->reals);
	R = ar_expr_crt_function_call(C->radd_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_ADD,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;
  case AL_EXPR_SUB:
    if( ar_lustre_expr_is_real(e) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rsub_sig == NULL )
	  C->rsub_sig = s_crt_real_op(C,C->real_sub,2,C->reals);
	R = ar_expr_crt_function_call(C->rsub_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_SUB,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;
  case AL_EXPR_MUL:
    if( ar_lustre_expr_is_real(e) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rmul_sig == NULL )
	  C->rmul_sig = s_crt_real_op(C,C->real_mul,2,C->reals);
	R = ar_expr_crt_function_call(C->rmul_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_MUL,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;
  case AL_EXPR_DIV:
    if( ar_lustre_expr_is_real(e) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rdiv_sig == NULL )
	  C->rdiv_sig = s_crt_real_op(C,C->real_div,2,C->reals);
	R = ar_expr_crt_function_call(C->rdiv_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_DIV,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;
  case AL_EXPR_MOD:
    if( ar_lustre_expr_is_real(e) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rmod_sig == NULL )
	  C->rmod_sig = s_crt_real_op(C,C->real_mod,2,C->reals);
	R = ar_expr_crt_function_call(C->rmod_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_MOD,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;
  case AL_EXPR_EQ:
    R = ar_expr_crt_binary(AR_EXPR_EQ,AOP1(model_operands),
			   AOP2(model_operands));
    break;
  case AL_EXPR_NEQ:
    R = ar_expr_crt_binary(AR_EXPR_NEQ,AOP1(model_operands),
			   AOP2(model_operands));
    break;
  case AL_EXPR_LT:
    if( ar_lustre_expr_is_real(OP1(operands)) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rlt_sig == NULL )
	  C->rlt_sig = s_crt_real_op(C,C->real_lt,2,AR_BOOLEANS);
	R = ar_expr_crt_function_call(C->rlt_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_LT,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;
  case AL_EXPR_LEQ: 
    if( ar_lustre_expr_is_real(OP1(operands)) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rleq_sig == NULL )
	  C->rleq_sig = s_crt_real_op(C,C->real_leq,2,AR_BOOLEANS);
	R = ar_expr_crt_function_call(C->rleq_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_LEQ,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;

  case AL_EXPR_GT:
    if( ar_lustre_expr_is_real(OP1(operands)) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rgt_sig == NULL )
	  C->rgt_sig = s_crt_real_op(C,C->real_gt,2,AR_BOOLEANS);
	R = ar_expr_crt_function_call(C->rgt_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_GT,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;

  case AL_EXPR_GEQ:
    if( ar_lustre_expr_is_real(OP1(operands)) )
      {
	ar_expr *arg[2];

	arg[0] = AOP1(model_operands);
	arg[1] = AOP2(model_operands);

	if( C->rgeq_sig == NULL )
	  C->rgeq_sig = s_crt_real_op(C,C->real_geq,2,AR_BOOLEANS);
	R = ar_expr_crt_function_call(C->rgeq_sig,arg);
      }
    else
      {    
	R = ar_expr_crt_binary(AR_EXPR_GEQ,AOP1(model_operands),
			       AOP2(model_operands));
      }
    break;

  case AL_EXPR_ATMOST: 
    R = s_atmost_to_formula(C,N,FIRST(model_operands));
    break;

  case AL_EXPR_FUNC_CALL:
    {
      int i;
      ccl_pair *p;
      ar_lustre_proto *proto = ar_lustre_expr_get_proto(e);
      ar_identifier *name = ar_lustre_proto_get_name(proto);
      ar_signature *sig = ar_model_get_signature(name);
      int nb_args = ccl_list_get_size(model_operands);
      ar_expr **args = ccl_new_array(ar_expr *,nb_args);

      ccl_assert( sig != NULL );
      ccl_assert( ar_signature_get_arity(sig) == nb_args );

      for(i = 0, p = FIRST(model_operands); p; p = CDR(p), i++)
	args[i] = CAR(p);
      R = ar_expr_crt_function_call(sig,args);

      ccl_delete(args);
      ar_signature_del_reference(sig);
      ar_identifier_del_reference(name);
      ar_lustre_proto_del_reference(proto);
    }
    break;

  case AL_EXPR_RCST:
    {
      ar_identifier *id;
      ar_context_slot *sl;
      double rval = ar_lustre_expr_get_rval(e);

      if( rval == 0.0 )
	id = ar_identifier_add_reference(C->real_zero);
      else
	{
	  char *rvalstr = ccl_string_format_new("%g",rval);
	  id = ar_identifier_create(rvalstr);

	  if( ! ar_model_has_parameter(id) )
	    ar_model_add_parameter(id,C->reals,NULL);

	  ccl_string_delete(rvalstr);
	}

      sl = ar_model_get_parameter(id);
      ccl_assert( sl != NULL );

      R = ar_expr_crt_parameter(sl);
      ar_context_slot_del_reference(sl);  
      ar_identifier_del_reference(id);
    }
    break;

  case AL_EXPR_ACST:
    {
      ar_identifier *name = ar_lustre_expr_get_name(e);
      ar_context_slot *sl = ar_model_get_parameter(name);

      ccl_assert( sl != NULL );
      R = ar_expr_crt_parameter(sl);
      ar_identifier_del_reference(name);
      ar_context_slot_del_reference(sl);
    }
    break;

  case AL_EXPR_EXPR_LIST:
    {
      ccl_pair *p;
      char tmp[10];
      int i, width = ccl_list_get_size(model_operands);
      ar_identifier **fnames = ccl_new_array(ar_identifier *,width);
      ar_expr **fvals = ccl_new_array(ar_expr *,width);
      
      for(i = 0, p = FIRST(model_operands); p; p = CDR(p), i++)
	{
	  sprintf(tmp,"%d",i);
	  fnames[i] = 
	    ar_identifier_rename_with_suffix(C->struct_field_prefix,tmp);
	  fvals[i] = CAR(p);
	}

      ccl_assert( i == width );
      R = ar_expr_crt_structure(fnames,fvals,width);
      while( width-- )
	ar_identifier_del_reference(fnames[width]);
      ccl_delete(fnames);
      ccl_delete(fvals);
    }
    break;

  case AL_EXPR_FOLLOWED:  
    if( *pstate == NULL ) 
      {
	ar_context_slot *sl;
	ar_expr *init = ar_expr_crt_true();

	ar_node_add_variable(N,C->initial_state_name,AR_BOOLEANS,
			     AR_SLOT_FLAG_STATE_VAR|AR_SLOT_FLAG_PRIVATE,
			     NULL);
	sl = ar_node_get_variable_slot (N, C->initial_state_name);
	
	ar_node_add_initial_state(N,sl,init);
	ar_expr_del_reference(init);
	*pstate = ar_expr_crt_variable(sl);
	ar_context_slot_del_reference(sl);
      }
    R = ar_expr_crt_ternary(AR_EXPR_ITE,*pstate,AOP1(model_operands),
			    AOP2(model_operands));

    break;

  case AL_EXPR_CURRENT: 
  case AL_EXPR_WHEN: case AL_EXPR_NODE_CALL:  
  case AL_EXPR_PRE : 
    ccl_throw_no_msg (internal_error);
    break;
  };

  if( operands != NULL )
    {
      ccl_list_clear_and_delete(operands,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_list_clear_and_delete(model_operands,(ccl_delete_proc *)
				ar_expr_del_reference);
    }

  ccl_post( R != NULL );

  return R;
}

			/* --------------- */

static void
s_generate_transitions(TRANSLATOR *C, ar_node *N, ccl_list *assignments,
		       ar_expr *state)
{  
  ccl_pair *p;
  ar_event *clock_event = ar_event_create(C->clock_event_name);

  ar_node_add_event(N,clock_event);

  if( state != NULL )
    {
      ar_expr *g0 = ar_expr_add_reference(state);
      ar_expr *g1 = ar_expr_crt_unary(AR_EXPR_NOT,state);
      ar_expr *set_false = ar_expr_crt_false();
      ar_trans *T0 = ar_trans_create(g0,clock_event);
      ar_trans *T1 = ar_trans_create(g1,clock_event);

      ar_trans_add_assignment(T0,state,set_false);
      
      for(p = FIRST(assignments); p; p = CDDR(p))
	{
	  ar_expr *v = ar_node_get_variable (N, (ar_identifier *) CAR (p));
	  ar_expr *f = ar_node_get_variable (N, (ar_identifier *) CADR (p));
	  
	  ar_trans_add_assignment (T0, v, f);
	  ar_trans_add_assignment (T1, v, f);
	  
	  ar_expr_del_reference (v);
	  ar_expr_del_reference (f);
	}
  
      ar_node_add_transition(N,T0);
      ar_node_add_transition(N,T1);
      
      ar_trans_del_reference(T0);
      ar_trans_del_reference(T1);
      ar_expr_del_reference(g0);
      ar_expr_del_reference(set_false);
      ar_expr_del_reference(g1);
    }
  else
    {
      ar_expr *g = ar_expr_crt_true();
      ar_trans *T = ar_trans_create(g,clock_event);

      for(p = FIRST(assignments); p; p = CDDR(p))
	{
	  ar_expr *v = ar_node_get_variable (N, (ar_identifier *) CAR (p));
	  ar_expr *f = ar_node_get_variable (N, (ar_identifier *) CADR (p));
	  
	  ar_trans_add_assignment (T, v, f);
	  
	  ar_expr_del_reference (v);
	  ar_expr_del_reference (f);
	}
  
      ar_node_add_transition(N,T);
      
      ar_trans_del_reference(T);
      ar_expr_del_reference(g);
    }

  
  {
    /* add epsilon transition */
    ar_expr *g = ar_expr_crt_true();
    ar_trans *T = ar_trans_create(g,AR_EPSILON);
    
    ar_node_add_event(N,AR_EPSILON);
    ar_node_add_transition(N,T);
    
    ar_trans_del_reference(T);
    ar_expr_del_reference(g);    
  }

  ar_event_del_reference(clock_event);
}

			/* --------------- */

static void
s_compile_types(TRANSLATOR *C)
{
  ar_identifier_iterator *it;

  if( C->program->use_floats )
    {
      ar_lustre_domain *r = ar_lustre_domain_crt_real();
      ar_idtable_put(C->program->types,C->real_typename,r);
      ar_lustre_domain_del_reference(r);
    }

  it = ar_idtable_get_keys(C->program->types);

  while( ccl_iterator_has_more_elements(it) )
    {
      ar_identifier *id = ccl_iterator_next_element(it);
      ar_type *newtype = ar_type_crt_abstract(id);     

      if( ar_model_has_type(id) )
	{
	  ar_type *type = ar_model_get_type(id);
	  
	  if( ! ar_type_equals(newtype,type) )
	    {
	      if( (C->flags&AR_LUSTRE_TO_ALTARICA_WARNING) )
		{
		  ccl_warning("L2A: warning: the type '");
		  ar_identifier_log(CCL_LOG_WARNING,id);
		  ccl_warning("is now replaced by an abstract version.\n'");
		}
	    }
	  else if( (C->flags&AR_LUSTRE_TO_ALTARICA_VERBOSE) )
	    {
	      ccl_warning("L2A: warning: the abstract type '");
	      ar_identifier_log(CCL_LOG_WARNING,id);
	      ccl_warning("' is already defined.\n");
	    }
	  ar_type_del_reference(type);
	}

      ar_model_set_type(id,newtype);
      ar_type_del_reference(newtype);
      ar_identifier_del_reference(id);
    }

  ccl_iterator_delete(it);

  if( C->program->use_floats )
    {
      C->reals = ar_model_get_type(C->real_typename);

      ccl_assert( C->reals != NULL );

      ar_model_add_parameter(C->real_zero,C->reals,NULL);      
    }
}

			/* --------------- */

static ar_signature *
s_lustre_function_to_signature(TRANSLATOR *C, ar_lustre_proto *proto)
{
  ar_identifier *name = ar_lustre_proto_get_name(proto);
  ar_lustre_domain *in = ar_lustre_proto_get_input_domain(proto);
  ar_lustre_domain *out = ar_lustre_proto_get_output_domain(proto);
  ar_type *outtype = s_translate_domain(C,out);
  ar_signature *result = ar_signature_create(name,outtype);
  
  if( ar_lustre_domain_is_composite(in) )
    {
      ccl_pair *p;
      ccl_list *comp = ar_lustre_domain_get_components(in);

      for(p = FIRST(comp); p; p = CDR(p))
	{
	  ar_lustre_domain *dom = CAR(p);
	  ar_type *intype = s_translate_domain(C,dom);
	  ar_signature_add_arg_domain(result,intype);
	  ar_type_del_reference(intype);
	  ar_lustre_domain_del_reference(dom);	  
	}
      ccl_list_delete(comp);
    }
  else
    {
      ar_type *intype = s_translate_domain(C,in);
      ar_signature_add_arg_domain(result,intype);
      ar_type_del_reference(intype);
    }

  ar_lustre_domain_del_reference(in);
  ar_lustre_domain_del_reference(out);
  ar_type_del_reference(outtype);
  ar_identifier_del_reference(name);

  return result;
}

			/* --------------- */

static void
s_compile_functions(TRANSLATOR *C)
{
  ar_identifier_iterator *it = ar_idtable_get_keys(C->program->functions);

  while( ccl_iterator_has_more_elements(it) )
    {
      ar_identifier *id = ccl_iterator_next_element(it);
      ar_lustre_proto *proto = ar_idtable_get(C->program->functions,id);
      ar_signature *sig = s_lustre_function_to_signature(C,proto);

      ar_lustre_proto_del_reference(proto);
      
      if( ar_model_has_signature(id) && 
	  (C->flags&AR_LUSTRE_TO_ALTARICA_VERBOSE) )
	{
	  ccl_warning("L2A: warning: the abstract signature '");
	  ar_identifier_log(CCL_LOG_WARNING,id);
	  ccl_warning("is already defined.\n'");
	}

      ar_model_set_signature(id,sig);
      ar_signature_del_reference(sig);
      ar_identifier_del_reference(id);
    }

  ccl_iterator_delete(it);
}

			/* --------------- */

static void
s_collect_abstract_constant(ar_lustre_expr *e, ccl_list *result)
{
  ccl_list *operands;

  if( ar_lustre_expr_get_type(e) == AL_EXPR_ACST )
    {
      ar_identifier *name = ar_lustre_expr_get_name(e);

      if( ! ccl_list_has(result,name) )
	ccl_list_add(result,name);
      else
	ar_identifier_del_reference(name);
    }
  else if( (operands=ar_lustre_expr_get_operands(e)) != NULL )
    {
      ccl_pair *p;

      for(p = FIRST(operands); p; p = CDR(p))
	s_collect_abstract_constant(CAR(p),result);
      ccl_list_clear_and_delete(operands,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
    }
}

			/* --------------- */

static void
s_compile_constants(TRANSLATOR *C)
{
  ccl_pair *p;
  ccl_list *constants = ccl_list_create();
  ar_identifier_iterator *it = ar_idtable_get_keys(C->program->constants);

  while( ccl_iterator_has_more_elements(it) )
    {
      ar_identifier *id = ccl_iterator_next_element(it);
      ar_lustre_expr *expr = ar_idtable_get(C->program->constants,id);
      s_collect_abstract_constant(expr,constants);
      if( ccl_list_has(constants,id) )
	ccl_list_add(constants,id);
      else
	ar_identifier_del_reference(id);
      ar_lustre_expr_del_reference(expr);
    }
  ccl_iterator_delete(it);

  for(p = FIRST(constants); p; p = CDR(p))
    {
      ar_identifier *id = (ar_identifier *)CAR(p);
      ar_lustre_expr *expr = ar_idtable_get(C->program->constants,id);
      ar_lustre_domain *dom = ar_lustre_expr_get_domain(expr);
      ar_type *type = s_translate_domain(C,dom);
      ar_expr *value = NULL;

      if( ar_lustre_expr_get_type(expr) != AL_EXPR_ACST )
	value = s_lustre_expr_to_formula(C,NULL,expr,NULL);
      ar_model_add_parameter(id,type,value);
    
      ccl_zdelete(ar_expr_del_reference,value);
      ar_type_del_reference(type);
      ar_lustre_expr_del_reference(expr);
      ar_lustre_domain_del_reference(dom);
      ar_identifier_del_reference(id);
    }

  ccl_list_delete(constants);
}

			/* --------------- */

static ar_type *
s_translate_domain(TRANSLATOR *C, ar_lustre_domain *dom)
{
  ar_type *result;

  if( ar_lustre_domain_is_boolean(dom) )
    {
      result = ar_type_add_reference(AR_BOOLEANS);
    }
  else if( ar_lustre_domain_is_abstract(dom) )
    {
      ar_identifier *name = ar_lustre_domain_get_name(dom);
      result = ar_model_get_type(name);

      ar_identifier_del_reference(name);
    }
  else if( ar_lustre_domain_is_real(dom) )
    {
      result = ar_model_get_type(C->real_typename);
    }
  else if( ar_lustre_domain_is_composite(dom) )
    {
      int i;
      ccl_pair *p;
      char tmp[10];
      ar_identifier *fname;
      ar_type *ftype;
      ccl_list *comps = ar_lustre_domain_get_components(dom);
      result = ar_type_crt_structure();

      for(p = FIRST(comps), i = 0; p; p = CDR(p), i++)
	{
	  sprintf(tmp,"%d",i);
	  fname = ar_identifier_rename_with_suffix(C->struct_field_prefix,tmp);
	  ftype = s_translate_domain(C,CAR(p));
	  ar_type_struct_add_field(result,fname,ftype);
	  ar_identifier_del_reference(fname);
	  ar_type_del_reference(ftype);
	}
      ccl_list_clear_and_delete(comps,(ccl_delete_proc *)
				ar_lustre_domain_del_reference);
    }
  else
    {
      result = ar_type_add_reference(C->integers);
    }

  ccl_assert( result != NULL );

  return result;
}
