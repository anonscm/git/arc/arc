/*
 * translators-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TRANSLATORS_P_H
# define TRANSLATORS_P_H

# include "translators.h"

enum ar_lustre_pref {
# define AR_LUSTRE_PREF(_enumid, _strid) LUSTRE_PREF_ ## _enumid,
# include "preferences.def"
# undef AR_LUSTRE_PREF
  LAST_AND_UNUSED_AR_LUSTRE_PREF
};

extern const char *AR_LUSTRE_PREF_NAMES[];

#endif /* ! TRANSLATORS_P_H */
