/*
 * node.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "node.h"

struct ar_lustre_node_st {
  int refcount;
  ar_identifier *name;
  ar_lustre_domain *input_domain;
  ar_idtable *parameters;
  ccl_list *inputs;
  ccl_list *outputs;
  ar_lustre_domain* output_domain;
  ccl_list *locals;
  ccl_list *equations;
  ccl_list *assertions;
  ccl_list *defined;
};

			/* --------------- */

ar_lustre_node *
ar_lustre_node_create(void)
{
  ar_lustre_node *result = ccl_new(ar_lustre_node);

  result->refcount = 1;
  result->name = NULL;
  result->input_domain = NULL;
  result->parameters = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_lustre_expr_add_reference,
		      (ccl_delete_proc *)ar_lustre_expr_del_reference);
  result->inputs = ccl_list_create();
  result->outputs = ccl_list_create();
  result->output_domain = NULL;
  result->locals = ccl_list_create();
  result->equations = ccl_list_create();
  result->assertions = ccl_list_create();
  result->defined = ccl_list_create();

  return result;
}

			/* --------------- */

void
ar_lustre_node_set_name(ar_lustre_node *node, ar_identifier *name)
{
  ccl_pre( node != NULL && name != NULL );

  if( node->name != NULL )
    ar_identifier_del_reference(node->name);
  node->name = ar_identifier_add_reference(name);
}

			/* --------------- */

ar_identifier *
ar_lustre_node_get_name(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  return ar_identifier_add_reference(node->name);
}

			/* --------------- */

void
ar_lustre_node_add_parameter(ar_lustre_node *node,
			     ar_identifier *name,
			     ar_lustre_domain *dom)
{
  ar_lustre_expr *var;

  ccl_pre( node != NULL ); ccl_pre( name != NULL ); ccl_pre( dom != NULL );

  var = ar_lustre_expr_crt_variable(name,dom);
  ccl_list_add(node->inputs,var);
  ar_idtable_put(node->parameters,name,var);
}

			/* --------------- */

void
ar_lustre_node_add_input_variable(ar_lustre_node *node, ar_identifier *name, 
				  ar_lustre_domain *dom)
{
  ar_lustre_expr *var;

  ccl_pre( node != NULL ); ccl_pre( name != NULL ); ccl_pre( dom != NULL );

  var = ar_lustre_expr_crt_variable(name,dom);
  ccl_list_add(node->inputs,var);
}

			/* --------------- */

void
ar_lustre_node_add_output_variable(ar_lustre_node *node, ar_identifier *name, 
				   ar_lustre_domain *dom)
{
  ar_lustre_expr *var;

  ccl_pre( node != NULL ); ccl_pre( name != NULL ); ccl_pre( dom != NULL );

  var = ar_lustre_expr_crt_variable(name,dom);
  ccl_list_add(node->outputs,var);
}

			/* --------------- */

void
ar_lustre_node_add_local_variable(ar_lustre_node *node, ar_identifier *name, 
				  ar_lustre_domain *dom)
{
  ar_lustre_expr *var;

  ccl_pre( node != NULL ); ccl_pre( name != NULL ); ccl_pre( dom != NULL );

  var = ar_lustre_expr_crt_variable(name,dom);
  ccl_list_add(node->locals,var);
}

			/* --------------- */

static ar_lustre_expr *
s_get_variable(ccl_list *l, const ar_identifier *name)
{
  ar_lustre_expr *result = NULL;
  ccl_pair *p;

  for(p = FIRST(l); result == NULL && p; p = CDR(p))
    {
      ar_lustre_expr *var = (ar_lustre_expr *)CAR(p);
      ar_identifier *n = ar_lustre_expr_get_name(var);

      if( n == name )
	result = ar_lustre_expr_add_reference(var);
      ar_identifier_del_reference(n);
    }

  return result;
}

			/* --------------- */

int
ar_lustre_node_has_variable(ar_lustre_node *node, ar_identifier *name)
{
  int result = AR_LUSTRE_UNKNOWN;
  ar_lustre_expr *var = NULL;

  ccl_pre( node != NULL );

  if( ar_idtable_has(node->parameters,name) )
    result = AR_LUSTRE_PARAMETER;

  if( (var = s_get_variable(node->inputs,name)) != NULL )
    result = AR_LUSTRE_INPUT;

  if( var == NULL && (var = s_get_variable(node->outputs,name)) != NULL )
    result = AR_LUSTRE_OUTPUT;

  if( var == NULL && (var = s_get_variable(node->locals,name)) != NULL )
    result = AR_LUSTRE_LOCAL;

  ccl_zdelete(ar_lustre_expr_del_reference,var);

  return result;
}

			/* --------------- */

ar_lustre_expr *
ar_lustre_node_get_variable(ar_lustre_node *node, ar_identifier *name)
{
  ar_lustre_expr *result;

  ccl_pre( node != NULL );

  if( (result = s_get_variable(node->inputs,name)) == NULL )
    {
      if( (result = s_get_variable(node->outputs,name)) == NULL )
	result = s_get_variable(node->locals,name);
    }

  return result;
}

			/* --------------- */

static ar_lustre_domain *
s_compute_domain(ccl_list *vars)
{
  ccl_list *domain = ccl_list_create();
  ar_lustre_domain *result;
  ccl_pair *p;

  for(p = FIRST(vars); p; p = CDR(p))
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      ar_lustre_domain *d = ar_lustre_expr_get_domain(e);
      ccl_list_add(domain,ar_lustre_domain_add_reference(d));
      ar_lustre_domain_del_reference(d);
    }
  result = ar_lustre_domain_crt_composite(domain);
  ccl_list_clear_and_delete(domain,(ccl_delete_proc *)
			    ar_lustre_domain_del_reference);

  return result;
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_node_get_input_domain(ar_lustre_node *node)
{
  ccl_pre( node != NULL && ccl_list_get_size(node->inputs) > 0 );

  if( node->input_domain == NULL )
    node->input_domain = s_compute_domain(node->inputs);

  return ar_lustre_domain_add_reference(node->input_domain);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_node_get_output_domain(ar_lustre_node *node)
{
  ccl_pre( node != NULL && ccl_list_get_size(node->outputs) > 0 );

  if( node->output_domain == NULL )
    node->output_domain = s_compute_domain(node->outputs);

  return ar_lustre_domain_add_reference(node->output_domain);
}

			/* --------------- */

void
ar_lustre_node_add_assertion(ar_lustre_node *node, ar_lustre_expr *a)
{
  ccl_pre( node != NULL );

  ccl_list_add(node->assertions,ar_lustre_expr_add_reference(a));
}

			/* --------------- */

void
ar_lustre_node_add_equation(ar_lustre_node *node, ccl_list *variables,
			    ar_lustre_expr *a)
{
  ccl_pair *p;
  ar_lustre_equation *eq;

  ccl_pre( node != NULL ); 
  ccl_pre( variables != NULL && ccl_list_get_size(variables) > 0 );
  ccl_pre( a != NULL );
  
  for(p = FIRST(variables); p; p = CDR(p))
    {
      ar_lustre_expr *var = (ar_lustre_expr *)CAR(p);
      ar_identifier *name = ar_lustre_expr_get_name(var);

      ccl_assert( ! ar_lustre_node_is_defined(node,name) );

      ccl_list_add(node->defined,ar_identifier_add_reference(name));
      ar_identifier_del_reference(name);
    }

  eq = ccl_new(ar_lustre_equation);
  eq->variables = ccl_list_deep_dup(variables,(ccl_duplicate_func *)
				    ar_lustre_expr_add_reference);
  eq->value = ar_lustre_expr_add_reference(a);

  ccl_list_add(node->equations,eq);
}

			/* --------------- */

int
ar_lustre_node_is_defined(ar_lustre_node *node, ar_identifier *varname)
{
  ccl_pre( node != NULL ); ccl_pre( varname != NULL );

  return ccl_list_has(node->defined,varname);
}

			/* --------------- */

int
ar_lustre_node_is_parameter(ar_lustre_node *node, ar_identifier *id)
{
  ccl_pre( node != NULL ); ccl_pre( id != NULL );

  return ar_idtable_has(node->parameters,id);
}

			/* --------------- */

ccl_list *
ar_lustre_node_get_parameters(ar_lustre_node *node)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create();

  ccl_pre( node != NULL );
  
  for(p = FIRST(node->inputs); p; p = CDR(p))
    {
      ar_identifier *id = ar_lustre_expr_get_name(CAR(p));
      
      if( ar_lustre_node_is_parameter(node,id) )
	ccl_list_add(result,CAR(p));
      ar_identifier_del_reference(id);
    }

  return result;
}

			/* --------------- */

ccl_list *
ar_lustre_node_get_input_variables(ar_lustre_node *node, int exclude_params)
{
  ccl_list *result;

  ccl_pre( node != NULL );

  if( ! exclude_params )
    result = ccl_list_dup(node->inputs);
  else
    {
      ccl_pair *p;

      result = ccl_list_create();
      for(p = FIRST(node->inputs); p; p = CDR(p))
	{
	  ar_identifier *id = ar_lustre_expr_get_name(CAR(p));
	  
	  if( ! ar_lustre_node_is_parameter(node,id) )
	    ccl_list_add(result,CAR(p));
	  ar_identifier_del_reference(id);
	}
    }

  return result;
}

			/* --------------- */

ccl_list *
ar_lustre_node_get_output_variables(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  return ccl_list_dup(node->outputs);
}

			/* --------------- */

ccl_list *
ar_lustre_node_get_local_variables(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  return ccl_list_dup(node->locals);
}

			/* --------------- */

ccl_list *
ar_lustre_node_get_equations(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  return ccl_list_dup(node->equations);
}

			/* --------------- */

ccl_list *
ar_lustre_node_get_assertions(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  return ccl_list_dup(node->assertions);
}

			/* --------------- */


void
ar_lustre_node_log(ccl_log_type log, ar_lustre_node *node)
{
  ar_lustre_node_log_gen(log,node,ar_identifier_log);
}

			/* --------------- */

void
ar_lustre_node_log_gen(ccl_log_type log, ar_lustre_node *node,
		       ar_identifier_log_proc idlog)
{
  ccl_pair *p;

  ar_identifier_log(log,node->name);
  ccl_log(log,"(");
  for(p = FIRST(node->inputs); p; p = CDR(p)) 
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      ar_lustre_domain *d = ar_lustre_expr_get_domain(e);
      ar_identifier *name = ar_lustre_expr_get_name(e);

      if( ar_lustre_node_is_parameter(node,name) )
	ccl_log(log,"const ");
      ar_identifier_del_reference(name);

      ar_lustre_expr_log_gen(log,e,idlog);
      ccl_log(log," : ");
      ar_lustre_domain_log_gen(log,d,idlog);
      ar_lustre_domain_del_reference(d);

      if( CDR(p) != NULL )
	ccl_log(log,";");
    }
  ccl_log(log,") returns (");
  for(p = FIRST(node->outputs); p; p = CDR(p)) 
    {
      ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
      ar_lustre_domain *d = ar_lustre_expr_get_domain(e);

      ar_lustre_expr_log_gen(log,e,idlog);
      ccl_log(log," : ");
      ar_lustre_domain_log_gen(log,d,idlog);
      ar_lustre_domain_del_reference(d);

      if( CDR(p) != NULL )
	ccl_log(log,";");
    }
  ccl_log(log,");\n");

  if( ccl_list_get_size(node->locals) > 0 )
    {
      ccl_log(log,"vars\n");
      for(p = FIRST(node->locals); p; p = CDR(p)) 
	{
	  ar_lustre_expr *e = (ar_lustre_expr *)CAR(p);
	  ar_lustre_domain *d = ar_lustre_expr_get_domain(e);
	  
	  ar_lustre_expr_log_gen(log,e,idlog);
	  ccl_log(log," : ");
	  ar_lustre_domain_log_gen(log,d,idlog);
	  ccl_log(log,";");

	  ar_lustre_domain_del_reference(d);
	}
      ccl_log(log,"\n");
    }

  ccl_log(log,"let\n");
  for(p = FIRST(node->equations); p; p = CDR(p)) 
    {
      ar_lustre_equation *eq = (ar_lustre_equation *)CAR(p);
      ccl_list *vars = eq->variables;
      ar_lustre_expr *def = eq->value;

      if( ccl_list_get_size(vars) == 1 )
	ar_lustre_expr_log_gen(log,CAR(FIRST(vars)),idlog);
      else
	{
	  ccl_pair *pv;

	  ccl_log(log,"(");
	  for(pv = FIRST(vars); pv; pv = CDR(pv))
	    {
	      ar_lustre_expr_log_gen(log,CAR(pv),idlog);
	      if( CDR(pv) != NULL )
		ccl_log(log,", ");
	    }
	  ccl_log(log,")");
	}
      ccl_log(log," = ");
      ar_lustre_expr_log_gen(log,def,idlog);
      ccl_log(log,";\n");
    }

  if( ccl_list_get_size(node->assertions) > 0 )
    {
      ccl_log(log,"\n");

      for(p = FIRST(node->assertions); p; p = CDR(p)) 
	{
	  ccl_log(log,"  assert(");
	  ar_lustre_expr_log_gen(log,CAR(p),idlog);
	  ccl_log(log,");\n");
	}
    }

  ccl_log(log,"tel;\n");
}

			/* --------------- */

ar_lustre_node *
ar_lustre_node_add_reference(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  node->refcount++;

  return node;
}

			/* --------------- */

void
ar_lustre_node_del_reference(ar_lustre_node *node)
{
  ccl_pre( node != NULL );

  if( --node->refcount == 0 )
    {
      ccl_pair *p;

      ccl_zdelete(ar_identifier_del_reference,node->name);
      ccl_zdelete(ar_lustre_domain_del_reference,node->input_domain);
      ccl_zdelete(ar_lustre_domain_del_reference,node->output_domain);
      ar_idtable_del_reference(node->parameters);
      ccl_list_clear_and_delete(node->inputs,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_list_clear_and_delete(node->outputs,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_list_clear_and_delete(node->locals,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      for(p = FIRST(node->equations); p; p = CDR(p))
	{
	  ar_lustre_equation *eq = CAR(p);
	  ccl_list_clear_and_delete(eq->variables,(ccl_delete_proc *)
				    ar_lustre_expr_del_reference);
	  ar_lustre_expr_del_reference(eq->value);
	  ccl_delete(eq);
	}
      ccl_list_delete(node->equations);     
      ccl_list_clear_and_delete(node->assertions,(ccl_delete_proc *)
				ar_lustre_expr_del_reference);
      ccl_list_clear_and_delete(node->defined,(ccl_delete_proc *)
				ar_identifier_del_reference);
      ccl_delete(node);
    }
}

			/* --------------- */
