/*
 * domain.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "domain.h"

typedef enum domain_type_enum {
  DOM_INT,
  DOM_REAL,
  DOM_ABSTRACT,
  DOM_BOOL,
  DOM_COMPOSITE
} domain_type;

struct ar_lustre_domain_st {
  int refcount;
  domain_type type;
  ar_identifier *name;
  ccl_list *components;
};

			/* --------------- */

static ar_lustre_domain *
s_new_domain(domain_type type,  ar_identifier *name, ccl_list *comp);

			/* --------------- */

ar_lustre_domain *
ar_lustre_domain_crt_int(void)
{
  return s_new_domain(DOM_INT,NULL,NULL);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_domain_crt_real(void)
{
  return s_new_domain(DOM_REAL,NULL,NULL);
}


			/* --------------- */

ar_lustre_domain *
ar_lustre_domain_crt_bool(void)
{
  return s_new_domain(DOM_BOOL,NULL,NULL);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_domain_crt_abstract(ar_identifier *name)
{
  return s_new_domain(DOM_ABSTRACT,name,NULL);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_domain_crt_composite(ccl_list *comp)
{
  ccl_pre( comp != NULL );

  if( ccl_list_get_size(comp) == 1 )
    return ar_lustre_domain_add_reference(CAR(FIRST(comp)));

  return s_new_domain(DOM_COMPOSITE,NULL,comp);
}

			/* --------------- */

ar_lustre_domain *
ar_lustre_domain_add_reference(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  dom->refcount++;

  return dom;
}

			/* --------------- */

void
ar_lustre_domain_del_reference(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  if( --dom->refcount == 0 )
    {
      ccl_zdelete(ar_identifier_del_reference,dom->name);
      if( dom->components != NULL )
	ccl_list_clear_and_delete(dom->components,(ccl_delete_proc *)
				  ar_lustre_domain_del_reference);
      ccl_delete(dom);
    }
}

			/* --------------- */

int
ar_lustre_domain_is_numerical(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  return dom->type == DOM_INT || dom->type == DOM_REAL;
}

			/* --------------- */

int
ar_lustre_domain_is_real(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  return dom->type == DOM_REAL;
}

			/* --------------- */

int
ar_lustre_domain_is_boolean(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  return dom->type == DOM_BOOL;
}

			/* --------------- */

int
ar_lustre_domain_is_composite(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  return dom->type == DOM_COMPOSITE;
}

			/* --------------- */

int
ar_lustre_domain_is_abstract(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  return dom->type == DOM_ABSTRACT;
}

			/* --------------- */

int
ar_lustre_domain_get_width(ar_lustre_domain *dom)
{
  int result = 1;

  ccl_pre( dom != NULL );

  if( dom->components != NULL )
    result = ccl_list_get_size(dom->components);

  return result;
}

			/* --------------- */

ccl_list *
ar_lustre_domain_get_components(ar_lustre_domain *dom)
{
  ccl_list *result = NULL;

  ccl_pre( dom != NULL );

  if( dom->components != NULL )
    result = 
      ccl_list_deep_dup(dom->components,
			(ccl_duplicate_func *)ar_lustre_domain_add_reference);

  return result;
}

			/* --------------- */

ar_identifier *
ar_lustre_domain_get_name(ar_lustre_domain *dom)
{
  ccl_pre( dom != NULL );

  return ar_identifier_add_reference(dom->name);
}

			/* --------------- */

static ar_lustre_domain *
s_new_domain(domain_type type, ar_identifier *name, ccl_list *comp)
{
  ar_lustre_domain *result = ccl_new(ar_lustre_domain);

  result->refcount = 1;
  result->type = type;
  result->name = NULL;

  if( name != NULL )
    result->name = ar_identifier_add_reference(name);
  
  if( (result->components = comp) != NULL )
    result->components = 
      ccl_list_deep_dup(comp,
			(ccl_duplicate_func *)ar_lustre_domain_add_reference);

  return result;
}

			/* --------------- */

uint32_t
ar_lustre_domain_hashcode(ar_lustre_domain *dom)
{
  uintptr_t result = (uintptr_t) dom->type + 19 * (uintptr_t) dom->name;

  if( dom->components != NULL )
    result += ccl_list_hash_all(dom->components,
				(ccl_hash_func *)ar_lustre_domain_hashcode);
  return result;
}

			/* --------------- */

int
ar_lustre_domain_equals(const ar_lustre_domain *d1, const ar_lustre_domain *d2)
{
  return ar_lustre_domain_compare(d1,d2) == 0;
}

			/* --------------- */

int
ar_lustre_domain_compare(const ar_lustre_domain *dom1, 
			 const ar_lustre_domain *dom2)
{
  if( dom1 == dom2 )
    return 0;

  if( dom1->type != dom2->type ) 
    return (int)dom1->type - (int)dom2->type;

  if( dom1->type == DOM_ABSTRACT )
    return (dom1->name==dom2->name)?0:1;
  else if( dom1->type == DOM_COMPOSITE )
    return ccl_list_compare(dom1->components,dom2->components,
			    (ccl_compare_func *)ar_lustre_domain_compare);

  ccl_assert( dom1->name == dom2->name );

  return 0;
}

			/* --------------- */

void
ar_lustre_domain_log(ccl_log_type log, ar_lustre_domain *dom)
{
  ar_lustre_domain_log_gen(log,dom,ar_identifier_log);
}

			/* --------------- */

void
ar_lustre_domain_log_gen(ccl_log_type log, ar_lustre_domain *dom,
			 ar_identifier_log_proc idlog)
{
  switch( dom->type ) {
  case DOM_INT : ccl_log(log,"int"); break;
  case DOM_REAL : ccl_log(log,"real"); break;
  case DOM_BOOL :  ccl_log(log,"bool"); break;
  case DOM_ABSTRACT :  idlog(log,dom->name); break;
  default:
    ccl_assert( dom->type == DOM_COMPOSITE );
    {
      ccl_pair *p;
      
      ccl_log(log,"<");
      for(p = FIRST(dom->components); p; p = CDR(p))
	{
	  ar_lustre_domain_log_gen(log,(ar_lustre_domain *)CAR(p),idlog);
	  if( CDR(p) != NULL )
	    ccl_log(log,", ");
	}
      ccl_log(log,">");
    break;
    }
  }
}
