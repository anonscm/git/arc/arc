/*
 * proto.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef PROTO_H
# define PROTO_H

#include "ar-identifier.h"
#include "domain.h"

typedef struct ar_lustre_proto_st ar_lustre_proto;

extern ar_lustre_proto *
ar_lustre_proto_create(ar_identifier *name,
		       ar_lustre_domain *input_domain,
		       ar_lustre_domain *output_domain);

extern ar_lustre_proto *
ar_lustre_proto_add_reference(ar_lustre_proto *proto);

extern void
ar_lustre_proto_del_reference(ar_lustre_proto *proto);

extern ar_identifier *
ar_lustre_proto_get_name(ar_lustre_proto *proto);

extern ar_lustre_domain *
ar_lustre_proto_get_input_domain(ar_lustre_proto *proto);

extern ar_lustre_domain *
ar_lustre_proto_get_output_domain(ar_lustre_proto *proto);

extern void
ar_lustre_proto_log(ccl_log_type log, ar_lustre_proto *proto);

extern int
ar_lustre_proto_equals(const ar_lustre_proto *p1, const ar_lustre_proto *p2);

#endif /* ! PROTO_H */
