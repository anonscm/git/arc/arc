/*
 * observer.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "observer.h"

#define PREFIX_FOR_EVENT_VARIABLES "ev$"

static void
s_find_or_add_variables_for_event (ar_ca *ca, ar_node *node, 
				   ar_ca_exprman *eman, 
				   ccl_set *visibles, ccl_set *disabled,
				   ar_ca_event *e, ccl_hash *e2vl,
				   ccl_set *vars, ccl_set **p_invvars);

static void
s_delete_varlist (void *ptr);

ar_identifier *
observer_get_event_variable_name (const ar_identifier *eventname)
{
  return
    ar_identifier_rename_with_prefix (eventname, PREFIX_FOR_EVENT_VARIABLES);
}

char *
observer_get_event_name_from_var (const ar_ca_expr *variable)
{
  const ar_identifier *varname = ar_ca_expr_variable_get_const_name (variable);
  ar_identifier *prefix = NULL;
  ar_identifier *name = ar_identifier_get_name_and_prefix (varname, &prefix);
  char *sname = ar_identifier_to_string (name);
  const char *rname = ccl_string_has_prefix (PREFIX_FOR_EVENT_VARIABLES, sname);
  char *result;
  ar_identifier_del_reference (name);
  
  if (rname == NULL)
    result = ar_identifier_to_string (varname);
  else
    {
      ar_identifier *tmp;
      name = ar_identifier_create (rname);
      tmp = ar_identifier_add_prefix (name, prefix);
      ar_identifier_del_reference (name);
      result = ar_identifier_to_string (tmp);
      ar_identifier_del_reference (tmp);
    }
  ar_identifier_del_reference (prefix);
  ccl_string_delete (sname);
  
  return result;
}

void 
ar_ca_add_event_observer (ar_ca *ca, ccl_set *visible, ccl_set *disabled,
			  ccl_set **p_vars, ccl_hash **p_e2vl,
			  ccl_set **p_invvars)
{
  int i;
  ar_ca_exprman *eman = ar_ca_get_expression_manager (ca);
  int nb_events = ar_ca_get_number_of_events (ca);
  ar_ca_event **events = ar_ca_get_events (ca);
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  ar_ca_expr *tt = ar_ca_expr_crt_boolean_constant (eman, 1);
  ar_ca_expr *ff = ar_ca_expr_crt_boolean_constant (eman, 0);
  ar_node *node = ar_ca_get_node (ca);
  ar_identifier *nodename = ar_node_get_name (node);
  ar_node_del_reference (node);
  node = ar_model_get_node (nodename);
  ar_identifier_del_reference (nodename);

  *p_e2vl = ccl_hash_create (NULL, NULL, NULL, s_delete_varlist);
  *p_vars = ccl_set_create ();
  if (p_invvars != NULL)
    *p_invvars = ccl_set_create ();
  for (i = 0; i < nb_events; i++)
    s_find_or_add_variables_for_event (ca, node, eman, visible, disabled,
				       events[i], *p_e2vl, *p_vars, p_invvars);
  ar_node_del_reference (node);

  for (i = 0; i < nb_trans; i++)
    {
      ar_ca_event *label = ar_ca_trans_get_event (trans[i]);

      if (ccl_hash_find (*p_e2vl, label))
	{
	  ccl_list *vars = ccl_hash_get (*p_e2vl);

	  if (ccl_list_is_empty (vars))
	    {
	      ar_ca_trans_set_guard (trans[i], ff);
	    }
	  else
	    {
	      ccl_pair *p;

	      for (p = FIRST (vars); p; p = CDR (p))
		{
		  ar_ca_expr *var = CAR (p);
		  ar_ca_trans_add_assignment (trans[i], var, tt);
		}
	    }
	}
      ar_ca_event_del_reference (label);
    }

  ar_ca_expr_del_reference (tt);
  ar_ca_expr_del_reference (ff);
  ar_ca_exprman_del_reference (eman);
}

/*!
 * remove duplicated identifiers due to 'public' attribute
 */
static ccl_list *
s_event_ids (const ar_ca_event *e)
{
  ccl_list *result;
  ar_identifier_iterator *ii = ar_ca_event_get_ids (e);
  ar_idtable *reptable = ar_idtable_create (1, NULL, NULL);

  /* collect identifiers with the longest path length */
  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *ename = ccl_iterator_next_element (ii);
      ar_identifier *id = ar_identifier_collapse (ename);

      if (ar_idtable_has (reptable, id))
	{
	  ar_identifier *other = ar_idtable_get (reptable, id);

	  if (ar_identifier_get_path_length (other) <
	      ar_identifier_get_path_length (ename))
	    {
	      ar_identifier_del_reference (other);
	      ar_idtable_put (reptable, id,
			      ar_identifier_add_reference (ename));
	    }
	}
      else
	{
	  ar_idtable_put (reptable, id,
			  ar_identifier_add_reference (ename));
	}
      ar_identifier_del_reference (ename);
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);

  result = ccl_list_create ();
  ii = ar_idtable_get_keys (reptable);
  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *rep = ccl_iterator_next_element (ii);
      ccl_list_add (result,
		    ar_idtable_get (reptable, rep));
      ar_identifier_del_reference (rep);
    }
  ccl_iterator_delete (ii);
  ar_idtable_del_reference (reptable);
  
  return result;
}

/*!
 * Find or create event variables. This function visits components of the
 * synchronization vector 'e'. Each component is an elementary event of the
 * model. If this event has been marked with an attribute in 'disabled' then 
 * transitions labelled with this event are disabled else, if the event 
 * possesses an attribute in 'visibles' then it is considered as visible for 
 * cuts. In this case a Boolean variable is created and added to a list 
 * attached to 'e' in the table 'e2vl'. A table 'v2str' attaches to each 
 * created variable a human-readable string that is used to output cuts. 
 * 
 * Parameters:
 *  ca       : the studied constraint automaton
 *  node     : the non-flatten ar_node from which ca has been obtained
 *  eman     : the expression manager of constraint automaton
 *  visibles : attributes used to identify visible events
 *  disabled : attributes used to identify disabled events
 *  e        : the synchronization vector
 * Result:
 *  e2vl     : a table that associates to each vector a list of event variables
 */
static void
s_find_or_add_variables_for_event (ar_ca *ca, ar_node *node, 
				   ar_ca_exprman *eman, 
				   ccl_set *visibles, ccl_set *disabled,
				   ar_ca_event *e, ccl_hash *e2vl,
				   ccl_set *vars, ccl_set **p_invvars)
{
  ar_identifier_iterator *ii;
  ccl_list *ids;
  ccl_list *varlist = NULL;
  ar_ca_domain *booleans = ar_ca_domain_create_bool ();  
  ar_ca_expr *ff = ar_ca_expr_crt_boolean_constant (eman, 0);
  
  ccl_pre (!ccl_hash_find (e2vl, e));

  ii = ar_ca_event_get_ids (e);
  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      int is_disabled = 
	ar_elementary_event_has_one_of_attrs (node, id, disabled);

      if (is_disabled)
	{
	  varlist = ccl_list_create ();
	  ar_identifier_del_reference (id);
	  ccl_iterator_delete (ii);
	  goto end;
	}
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);
  
  ids = s_event_ids (e);
  while (! ccl_list_is_empty (ids))
    {
      ar_identifier *ename = ccl_list_take_first (ids);
      int is_visible = 
	ar_elementary_event_has_one_of_attrs (node, ename, visibles);

      if (ar_identifier_has_name (ename, AR_EPSILON_ID))
	{
	  ar_identifier_del_reference (ename);
	  continue;
	}
      if (is_visible || p_invvars)
	{
	  ar_ca_expr *var;
	  ar_identifier *varname = observer_get_event_variable_name (ename);
	  
	  /* 
	   * Public elementary events can be duplicated. We create only
	   * one instance of the event variable; thus we check if the
	   * variable already exists in the manager.
	   */
	  var = ar_ca_expr_get_variable_by_name (eman, varname);
	  
	  if (var == NULL)
	    {
	      var = ar_ca_expr_crt_variable (eman, varname, booleans, NULL);
	      ar_ca_add_state_variable (ca, var);
	      ar_ca_add_initial_assignment (ca, var, ff);

	      ccl_assert (!ccl_set_has (vars, var));
	      ccl_set_add (vars, var);

	      if (p_invvars && ! is_visible)
		{
		  ccl_assert (!ccl_set_has (*p_invvars, var));
		  ccl_set_add (*p_invvars, var);
		}
	    }
	  ar_identifier_del_reference (varname);

	  if (varlist == NULL)
	    varlist = ccl_list_create ();
	  if (ccl_list_has (varlist, var))
	    ar_ca_expr_del_reference (var);
	  else
	    ccl_list_add (varlist, var);
	}
      ar_identifier_del_reference (ename);
    }
  ccl_list_delete (ids);
 end:
  if (varlist != NULL)
    {
      ccl_hash_find (e2vl, e);
      ccl_hash_insert (e2vl, varlist);
    }

  ar_ca_expr_del_reference (ff);
  ar_ca_domain_del_reference (booleans);
}


static void
s_delete_varlist (void *ptr)
{
  ccl_list_clear_and_delete (ptr, (ccl_delete_proc *) ar_ca_expr_del_reference);
}

static int
s_has_attrs_from_table (ar_identifier *attr, void *data)
{
  return ccl_set_has (data, attr);
}

int
ar_elementary_event_has_one_of_attrs (ar_node *node, ar_identifier *eventname, 
				      ccl_set *attrs)
{
  return ar_elementary_event_has_attribute_gen (node, eventname,
						s_has_attrs_from_table,
						attrs);
}

static int
s_are_identifier_equal (ar_identifier *attr, void *data)
{
  return attr == (ar_identifier *) data;
}

int
ar_elementary_event_has_attribute (ar_node *node, ar_identifier *eventname, 
				   ar_identifier *attr)
{
  return ar_elementary_event_has_attribute_gen (node, eventname,
						s_are_identifier_equal,
						attr);
}

int
ar_elementary_event_has_attribute_gen (ar_node *node, ar_identifier *eventname, 
				       int (*has_attr) (ar_identifier *attr,
							void *data),
				       void *data)
{
  int result = 0;
  ccl_pair *p;
  const ccl_list *attributes;
  ccl_list *path = ar_identifier_get_path (eventname);
  ar_event *event;

  ccl_pre (! ccl_list_is_empty (path));

  node = ar_node_add_reference (node);
  for (p = FIRST (path); CDR (p); p = CDR (p))
    {
      ar_identifier *subname = ar_identifier_create (CAR (p));
      ar_node *sub = ar_node_get_subnode_model (node, subname);
      ccl_assert (sub != NULL);
      ar_identifier_del_reference (subname);
      ar_node_del_reference (node);
      node = sub;
    }

  eventname = ar_identifier_create (CAR (p));
  ccl_assert (ar_node_has_event (node, eventname));
  event = ar_node_get_event  (node, eventname);
  attributes = ar_event_get_attr (event);
  if (attributes != NULL)
    {
      result = 0;
      for (p = FIRST (attributes); p && ! result; p = CDR (p))
	result = has_attr (CAR (p), data);
    }
  ar_node_del_reference (node);
  ar_identifier_del_reference (eventname);
  ar_event_del_reference (event);

  return result;
}
