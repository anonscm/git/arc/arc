/*
 * obfuscation-none.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "obfuscation-p.h"


static char * 
s_dup (obfuscation *o, ar_identifier_context ctx, const char *id)
{
  return ccl_string_dup (id);
}

			/* --------------- */

obfuscation *
obfuscation_crt_none (void)
{
  return obfuscation_create (sizeof (obfuscation), s_dup, NULL);
}

