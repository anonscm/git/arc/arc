/*
 * ar-attributes.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-attributes.h"

ar_identifier *AR_ATTR_PUBLIC = NULL;
ar_identifier *AR_ATTR_PRIVATE = NULL;
ar_identifier *AR_ATTR_PARENT = NULL;
ar_identifier *AR_ATTR_IN = NULL;
ar_identifier *AR_ATTR_OUT = NULL;

			/* --------------- */

void
ar_attributes_init(void)
{
  AR_ATTR_PUBLIC = ar_identifier_create("public");
  AR_ATTR_PRIVATE = ar_identifier_create("private");
  AR_ATTR_PARENT = ar_identifier_create("parent");
  AR_ATTR_IN = ar_identifier_create("in");
  AR_ATTR_OUT = ar_identifier_create("out");
}

			/* --------------- */

void
ar_attributes_terminate(void)
{
  ccl_zdelete(ar_identifier_del_reference,AR_ATTR_PUBLIC);
  ccl_zdelete(ar_identifier_del_reference,AR_ATTR_PARENT);
  ccl_zdelete(ar_identifier_del_reference,AR_ATTR_PRIVATE);
  ccl_zdelete(ar_identifier_del_reference,AR_ATTR_IN);
  ccl_zdelete(ar_identifier_del_reference,AR_ATTR_OUT);
}

