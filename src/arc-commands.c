/*
 * arc-commands.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
#include "arc-commands.h"

			/* --------------- */

struct arc_command_st 
{
  arc_command next;
  char *cmdid;
  const char *cmdname;
  arc_command_proc *cmd;
  void *clientdata;
  ccl_delete_proc *delclientdata;
};

			/* --------------- */

static arc_command COMMANDS = NULL;

void
arc_command_init (void)
{
  ccl_pre (COMMANDS == NULL); 
}

			/* --------------- */

void
arc_command_terminate (void)
{
  arc_command c, next;

  for (c = COMMANDS; c != NULL; c = next)
    {
      next = c->next;
      ccl_string_delete (c->cmdid);
      ccl_zdelete (c->delclientdata, c->clientdata);
      ccl_delete (c);
    }
  COMMANDS = NULL;
}

			/* --------------- */

void
arc_command_register(const char *cmdname, arc_command_proc *proc, 
		     void *clientdata, ccl_delete_proc *delclientdata)
{
  arc_command  cmd;
  arc_command *pcmd = NULL;

  for (pcmd = &COMMANDS; *pcmd != NULL; pcmd = &((*pcmd)->next))
    {
      if (ccl_string_cmp ((*pcmd)->cmdname, cmdname) >= 0)
	break;
    }

  if (*pcmd == NULL)
    *pcmd = cmd = ccl_new (struct arc_command_st);
  else if (ccl_string_cmp ((*pcmd)->cmdname, cmdname) > 0)
    {
      cmd = ccl_new (struct arc_command_st);
      cmd->next = *pcmd;
      *pcmd = cmd;
    }
  else
    {
      cmd = *pcmd;
      ccl_string_delete (cmd->cmdid);
    }

  ccl_assert (cmd != NULL);

  cmd->cmdname = cmdname;
  cmd->cmdid = ccl_string_dup (cmdname);
  cmd->cmd = proc;
  cmd->clientdata = clientdata;
  cmd->delclientdata = delclientdata;
}

			/* --------------- */

arc_command
arc_command_find (const char *cmdname)
{
  arc_command cmd;

  ccl_pre (cmdname != NULL);

  for (cmd = COMMANDS; cmd != NULL; cmd = cmd->next)
    {
      int c = ccl_string_cmp (cmd->cmdname, cmdname);

      if (c == 0)
	return cmd;
      else if ( c > 0 )
	break;
    }

  return NULL;
}

			/* --------------- */

arc_command_retval
arc_command_execute(arc_command cmd, ccl_config_table *config, int nb_params, 
		    arc_command_parameter *params, 
		    arc_command_parameter *result)
{
  ccl_pre (cmd != NULL);

  return cmd->cmd (cmd->cmdid, config, cmd->clientdata, nb_params, params, 
		   result);
}

			/* --------------- */

arc_command 
arc_command_get_all_commands (void)
{
  return COMMANDS;
}

			/* --------------- */

arc_command 
arc_command_get_next_command (const arc_command cmd)
{
  ccl_pre (cmd != NULL);

  return cmd->next;
}

			/* --------------- */

const char *
arc_command_get_name (const arc_command cmd)
{
  ccl_pre (cmd != NULL);

  return cmd->cmdid;
}

			/* --------------- */

arc_command_parameter
arc_command_parameter_create_int(int value)
{
  arc_command_parameter result = ccl_new(struct arc_command_parameter_st);

  result->type = ARC_CMDP_INTEGER;
  result->u.int_value = value;

  return result;
}

			/* --------------- */

arc_command_parameter
arc_command_parameter_create_float(double value)
{
  arc_command_parameter result = ccl_new(struct arc_command_parameter_st);

  result->type = ARC_CMDP_FLOAT;
  result->u.flt_value = value;

  return result;
}

			/* --------------- */

arc_command_parameter
arc_command_parameter_create_string(const char *value)
{
  arc_command_parameter result = ccl_new(struct arc_command_parameter_st);

  result->type = ARC_CMDP_STRING;
  if( value != NULL ) 
    result->u.string_value = ccl_string_dup(value);
  else 
    result->u.string_value = NULL;

  return result;
}

			/* --------------- */

arc_command_parameter
arc_command_parameter_create_identifier(ar_identifier *value)
{
  arc_command_parameter result = ccl_new(struct arc_command_parameter_st);

  result->type = ARC_CMDP_IDENTIFIER;
  result->u.ident_value = ar_identifier_add_reference(value);

  return result;
}

			/* --------------- */

arc_command_parameter
arc_command_parameter_create_custom(void *value, ccl_delete_proc del)
{
  arc_command_parameter result = ccl_new(struct arc_command_parameter_st);

  result->type = ARC_CMDP_CUSTOM;
  result->u.custom = value;
  result->delcustom = del;

  return result;
}

			/* --------------- */

void
arc_command_parameter_delete(arc_command_parameter param)
{
  ccl_pre( param != NULL );

  if( param->type == ARC_CMDP_STRING && param->u.string_value != NULL )
    {
      ccl_string_delete(param->u.string_value);
    }
  else if( param->type == ARC_CMDP_IDENTIFIER && param->u.ident_value != NULL )
    {
      ar_identifier_del_reference(param->u.ident_value);
    }
  else if( param->type == ARC_CMDP_CUSTOM && param->delcustom != NULL )
    {
      param->delcustom(param->u.custom);
    }

  ccl_delete(param);
}
