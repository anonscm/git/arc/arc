/*
 * arc-readline-norl.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdio.h>
#include <ctype.h>
#include <ccl/ccl-buffer.h>
#include <ccl/ccl-string.h>
#include "arc-readline.h"
#include "arc-readline-p.h"

#define TMPBUFSZ 61
static char tmpbuf[TMPBUFSZ];
static ccl_buffer *LINE;
static int allow_rl_lib = 1;

void
arc_readline_init (const char *history_filename, int allow_rl)
{
  LINE = ccl_buffer_create ();
  allow_rl_lib = allow_rl;
#if HAVE_LIBREADLINE  
  if (allow_rl_lib)
    arc_readline_init_rl (history_filename);
#endif
}

			/* --------------- */

void
arc_readline_terminate (void)
{
#if HAVE_LIBREADLINE
  if (allow_rl_lib)
    arc_readline_terminate_rl ();
#endif
  ccl_buffer_del_reference (LINE);
}

			/* --------------- */

char *
arc_readline_get_line_no_rl (const char *prompt)
{
  char *res;
  int size = TMPBUFSZ;
  ccl_buffer_content result;

  if (prompt)
    {
      printf("%s", prompt);
      fflush(stdout);
    }

  for(;;) 
    {
      int i = 0;

      if (fgets (tmpbuf, size, stdin) == NULL)
	return NULL;

      ccl_buffer_append (LINE, tmpbuf, strlen (tmpbuf));

      while (i < size && tmpbuf[i] != '\0')
	if (tmpbuf[i++] == '\n')
	  goto got_a_line;
    }
 got_a_line:
  tmpbuf[0] = 0;
  ccl_buffer_append (LINE, tmpbuf, 1);
  size = ccl_buffer_get_content_size (LINE);
  ccl_array_init (result);
  ccl_buffer_get_content (LINE, &result);
  ccl_buffer_reset (LINE);
  res = ccl_string_dup ((char *) result.data);
  ccl_array_delete (result);
    
  return res;
}

			/* --------------- */

char *
arc_readline_get_line (int allow_rl, const char *prompt)
{
  char *(*reader) (const char *prompt) = arc_readline_get_line_no_rl;
  prompt = allow_rl ? prompt : NULL;
#if HAVE_LIBREADLINE
  if (allow_rl && allow_rl_lib)
    reader = arc_readline_get_line_rl;
#endif

  return reader (prompt);
}
