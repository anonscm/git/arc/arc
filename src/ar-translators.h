/*
 * ar-translators.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_TRANSLATORS_H
# define AR_TRANSLATORS_H

# include <ccl/ccl-exception.h>
# include <ccl/ccl-config-table.h>

CCL_DECLARE_EXCEPTION(translation_exception,exception);

# include <ts/ts-translators.h>
# include <lustre/translators.h>

extern void
ar_translators_init(ccl_config_table *conf);

extern void
ar_translators_terminate(void);

#endif /* ! AR_TRANSLATORS_H */
