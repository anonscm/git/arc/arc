/*
 * ar-signature.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-signature.h"

struct ar_signature_st 
{
  int refcount;
  int arity;
  ar_identifier *name;
  ar_type **ret_args;
};

			/* --------------- */

ar_signature *
ar_signature_create (ar_identifier *signame, ar_type *ret)
{
  ar_signature *result = ccl_new (ar_signature);

  ccl_pre (signame != NULL);  ccl_pre (ret != NULL);

  result->refcount = 1;
  result->arity = 0;
  result->name = ar_identifier_add_reference (signame);
  result->ret_args = ccl_new_array (ar_type *, 1);
  result->ret_args[0] = ar_type_add_reference (ret);

  return result;
}

			/* --------------- */

ar_signature *
ar_signature_add_reference (ar_signature *sig)
{
  ccl_pre (sig != NULL);

  sig->refcount++;

  return sig;
}

			/* --------------- */

void
ar_signature_del_reference (ar_signature *sig)
{
  ccl_pre (sig != NULL); 
  ccl_pre (sig->refcount > 0);

  if (--sig->refcount == 0)
    {
      int i;

      ar_identifier_del_reference (sig->name);
      for (i = 0; i < sig->arity+1; i++)
	ar_type_del_reference (sig->ret_args[i]);
      ccl_delete (sig->ret_args);
      ccl_delete (sig);
    }
}

			/* --------------- */

void
ar_signature_log (ccl_log_type log, const ar_signature *sig)
{
  int i;

  ccl_pre (sig != NULL);

  ar_identifier_log (log, sig->name);
  ccl_log (log, " : ");
  for (i = 1; i <= sig->arity; i++ )
    {
      ar_type_log (log, sig->ret_args[i]);
      if (i < sig->arity)
	ccl_log (log, " * ");
    }
  ccl_log (log, " -> ");
  ar_type_log (log, sig->ret_args[0]);
}

			/* --------------- */

void
ar_signature_add_arg_domain (ar_signature *sig, ar_type *type)
{
  ccl_pre (sig != NULL); 
  ccl_pre (type != NULL);

  sig->ret_args = ccl_realloc (sig->ret_args,
			       sizeof (ar_type *) * (sig->arity + 2));
  sig->ret_args[1 + sig->arity] = ar_type_add_reference (type);
  sig->arity++;
}

			/* --------------- */

ar_identifier *
ar_signature_get_name (const ar_signature *sig)
{
  ccl_pre (sig != NULL);

  return ar_identifier_add_reference (sig->name);
}

			/* --------------- */

int
ar_signature_get_arity (const ar_signature *sig)
{
  ccl_pre (sig != NULL);

  return sig->arity;
}

			/* --------------- */

ar_type *
ar_signature_get_ith_arg_domain (const ar_signature *sig, int i)
{
  ccl_pre (0 <= i && i < ar_signature_get_arity (sig));

  return ar_type_add_reference (sig->ret_args[i + 1]);
}

			/* --------------- */

int
ar_signature_get_size (const ar_signature *sig)
{
  int i;
  int result = 0;

  ccl_pre (sig != NULL);

  for (i = 0; i < sig->arity; i++)
    result += ar_type_get_size (sig->ret_args[i + 1]);

  return result;
}

			/* --------------- */

ar_type *
ar_signature_get_return_domain (const ar_signature *sig)
{
  ccl_pre (sig != NULL);

  return ar_type_add_reference (sig->ret_args[0]);
}

			/* --------------- */

int
ar_signature_equals (const ar_signature *sig, const ar_signature *other)
{
  int i;

  if (sig == other) return 1;
  if (sig->name != other->name) return 0;
  if (sig->arity != other->arity) return 0;

  for (i = 0; i< sig->arity+1; i++)
    {
      if (!ar_type_equals (sig->ret_args[i], other->ret_args[i]))
	return 0;
    }

  return 1;
}

			/* --------------- */

void
ar_signature_write (ar_signature *sig, FILE *out, ccl_serializer_status *p_err)
{
  int i;

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  ccl_serializer_write_uint16 (sig->arity, out, p_err);
  ar_identifier_write (sig->name, out, p_err);
  ar_type_write (sig->ret_args[0], out, p_err);

  for (i = 1; i <= sig->arity && *p_err == CCL_SERIALIZER_OK; i++)
    ar_type_write (sig->ret_args[i], out, p_err);
}

			/* --------------- */

void
ar_signature_read (ar_signature **p_sig, FILE *in, 
		   ccl_serializer_status *p_err)
{  
  uint16_t i, arity = 0;
  ar_identifier *name = NULL;
  ar_type *ret = NULL;
  ar_signature *sig = NULL;

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  ccl_serializer_read_uint16 (&arity, in, p_err);
  ar_identifier_read (&name, in, p_err);
  ar_type_read (&ret, in, p_err);

  if (*p_err == CCL_SERIALIZER_OK)
    {
      sig = ar_signature_create (name, ret);
      for (i = 1; i <= arity && *p_err == CCL_SERIALIZER_OK; i++)
	{
	  ar_type *t = NULL;

	  ar_type_read (&t, in, p_err);
	  if (*p_err == CCL_SERIALIZER_OK)
	    ar_signature_add_arg_domain (sig, t);
	  ccl_zdelete (ar_type_del_reference, t);
	}
    }
  ccl_zdelete (ar_identifier_del_reference, name);
  ccl_zdelete (ar_type_del_reference, ret);

  if (*p_err != CCL_SERIALIZER_OK)
    {
      ccl_zdelete (ar_signature_del_reference, sig);
      sig = NULL;
    }

  *p_sig = sig;
}

			/* --------------- */

ar_signature *
ar_signature_rename (ar_signature *sig, ar_identifier *newname)
{
  int i;
  ar_signature *result;
			
  ccl_pre (sig != NULL);
  ccl_pre (newname != NULL);

  result = ar_signature_create (newname, sig->ret_args[0]);

  for (i = 1; i <= sig->arity; i++)
    ar_signature_add_arg_domain (result, sig->ret_args[i]);

  return result;
}
