/*
 * arc-commands.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_COMMANDS_H
# define ARC_COMMANDS_H

# include <ccl/ccl-protos.h>
# include <ccl/ccl-string.h>
# include <ccl/ccl-config-table.h>
# include "ar-identifier.h"

typedef struct arc_command_parameter_st *arc_command_parameter;
typedef enum arc_command_parameter_type_enum {
  ARC_CMDP_INTEGER,
  ARC_CMDP_FLOAT,
  ARC_CMDP_STRING,
  ARC_CMDP_IDENTIFIER,
  ARC_CMDP_CUSTOM
} arc_command_parameter_type;

struct arc_command_parameter_st {
  arc_command_parameter_type type;
  union {
    int int_value;
    double flt_value;
    char *string_value;
    ar_identifier *ident_value;
    void *custom;
  } u;
  ccl_delete_proc *delcustom;
};

typedef enum arc_command_retval_enum {
  ARC_CMD_ERROR = 0,
  ARC_CMD_SUCCESS
} arc_command_retval;

# define ARC_COMMAND_PROTO(_cmdname_)					\
  arc_command_retval							\
  _cmdname_ (const char *cmd, ccl_config_table *config, void *clientdata, \
	     int argc, arc_command_parameter *argv,			\
	     arc_command_parameter *result)

typedef ARC_COMMAND_PROTO(arc_command_proc);

typedef struct arc_command_st *arc_command;

# define ARC_DECLARE_COMMAND(_cmdname_) \
  extern ARC_COMMAND_PROTO(_cmdname_)

# define ARC_DEFINE_COMMAND(_cmdname_) ARC_COMMAND_PROTO(_cmdname_)


			/* --------------- */

extern void
arc_command_init(void);

extern void
arc_command_terminate(void);

			/* --------------- */

extern void
arc_command_register(const char *cmdname, arc_command_proc *proc, 
		     void *clientdata, ccl_delete_proc *delclientdata);

extern arc_command
arc_command_find (const char *cmdname);

extern arc_command_retval
arc_command_execute(arc_command cmd, ccl_config_table *config, 
		    int nb_params,  arc_command_parameter *params,
		    arc_command_parameter *result);

extern arc_command 
arc_command_get_all_commands (void);

extern arc_command 
arc_command_get_next_command (const arc_command cmd);

extern const char *
arc_command_get_name (const arc_command cmd);


			/* --------------- */

extern arc_command_parameter
arc_command_parameter_create_int(int value);

extern arc_command_parameter
arc_command_parameter_create_float(double value);

extern arc_command_parameter
arc_command_parameter_create_string(const char *value);

extern arc_command_parameter
arc_command_parameter_create_identifier(ar_identifier *value);

extern arc_command_parameter
arc_command_parameter_create_custom(void *value, ccl_delete_proc del);

extern void
arc_command_parameter_delete(arc_command_parameter p);

#endif /* ! ARC_COMMANDS_H */
