/*
 * ar-expr.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_EXPR_H
# define AR_EXPR_H

# include <ccl/ccl-hash.h>
# include "ar-context.h"
# include "ar-constant.h"
# include "ar-signature.h"
# include "ar-type.h"

typedef struct ar_expr_st ar_expr;
typedef enum ar_expr_op_enum {
  AR_EXPR_ITE,
  AR_EXPR_OR, AR_EXPR_AND,
  AR_EXPR_EQ, AR_EXPR_NEQ, 
  AR_EXPR_LT, AR_EXPR_GT, AR_EXPR_LEQ, AR_EXPR_GEQ,
  AR_EXPR_ADD, AR_EXPR_SUB, 
  AR_EXPR_MUL, AR_EXPR_DIV, AR_EXPR_MOD,
  AR_EXPR_NOT, AR_EXPR_NEG,
  AR_EXPR_EXIST, AR_EXPR_FORALL, 
  AR_EXPR_CST, 
  AR_EXPR_VAR,
  AR_EXPR_STRUCT_MEMBER, 
  AR_EXPR_ARRAY_MEMBER,
  AR_EXPR_CALL,
  AR_EXPR_PARAM,
  AR_EXPR_ARRAY,
  AR_EXPR_STRUCT,
  AR_EXPR_MAX,
  AR_EXPR_MIN
} ar_expr_op;

			/* --------------- */

extern void
ar_expr_init(void);
extern void
ar_expr_terminate(void);

			/* --------------- */

extern ar_expr *
ar_expr_crt_true(void);
extern ar_expr *
ar_expr_crt_false(void);
extern ar_expr *
ar_expr_crt_integer(int i);
extern ar_expr *
ar_expr_crt_constant(ar_constant *cst);
extern ar_expr *
ar_expr_crt_parameter(ar_context_slot *p);
extern ar_expr *
ar_expr_crt_variable(ar_context_slot *slot);
extern ar_expr *
ar_expr_crt_struct_member(ar_expr *s, ar_identifier *id);
extern ar_expr *
ar_expr_crt_unary(ar_expr_op op, ar_expr *e1);
extern ar_expr *
ar_expr_crt_binary(ar_expr_op op, ar_expr *e1, ar_expr *e2);
extern ar_expr *
ar_expr_crt_ternary(ar_expr_op op, ar_expr *e1, ar_expr *e2, ar_expr *e3);
extern ar_expr *
ar_expr_crt_quantifier(ar_expr_op op, ar_context *ctx, ccl_list *qvars, 
		       ar_expr *F);
extern ar_expr *
ar_expr_crt_function_call(ar_signature *sig, ar_expr **args);
extern ar_expr *
ar_expr_crt_array(ar_expr **a, int asize);
extern ar_expr *
ar_expr_crt_structure(ar_identifier **fnames, ar_expr **fvals, int width);
extern ar_expr *
ar_expr_crt_in_domain(ar_expr *e, ar_type *t);
extern ar_expr *
ar_expr_crt_and(int arity, ar_expr **ops);
extern ar_expr *
ar_expr_crt_or(int arity, ar_expr **ops);
extern ar_expr *
ar_expr_crt_min (int arity, ar_expr **ops);
extern ar_expr *
ar_expr_crt_max (int arity, ar_expr **ops);

			/* --------------- */

extern ar_expr *
ar_expr_add_reference(ar_expr *e);
extern void
ar_expr_del_reference(ar_expr *e);
extern ar_expr_op
ar_expr_get_op (const ar_expr *e);
extern ar_expr **
ar_expr_get_args(ar_expr *e, int *psize);
extern int
ar_expr_get_arity(ar_expr *e);
extern ar_context_slot *
ar_expr_get_slot(ar_expr *e);
extern ar_identifier *
ar_expr_get_name (ar_expr *e);
extern ccl_list *
ar_expr_get_attributes (ar_expr *e);
extern ccl_list *
ar_expr_get_quantified_variables(ar_expr *e);
extern ccl_list *
ar_expr_get_quantified_variables_rec (ar_expr *e, ccl_list *result);
extern ar_context *
ar_expr_get_quantifier_context (ar_expr *e);

extern void
ar_expr_log(ccl_log_type log, const ar_expr *e);
extern void
ar_expr_log_gen(ccl_log_type log, const ar_expr *e,
		void (*idlog)(ccl_log_type log, const ar_identifier *id));

extern void
ar_expr_log_expr_list (ccl_log_type log, const ccl_list *l);

extern ar_signature *
ar_expr_call_get_signature (const ar_expr *e);

extern ccl_list *
ar_expr_call_get_subtypes (ar_expr *e);

extern ccl_list *
ar_expr_call_get_subargs (ar_expr *e, ar_context *ctx);

			/* --------------- */

extern int
ar_expr_is_true(ar_expr *e);
extern int
ar_expr_is_false(ar_expr *e);

			/* --------------- */

extern ar_type *
ar_expr_get_type(ar_expr *e);
extern ar_type_kind
ar_expr_get_type_kind(ar_expr *e);
extern int
ar_expr_has_base_type(ar_expr *e, ar_type *t);

			/* --------------- */

# define ar_expr_is_scalar(_e) \
  (!(ar_expr_is_struct (_e) || ar_expr_is_array (_e) || ar_expr_is_bang(_e)))
# define ar_expr_is_struct(_e) \
  (ar_expr_get_type_kind (_e) == AR_TYPE_STRUCTURE)
# define ar_expr_is_bang(_e) \
  (ar_expr_get_type_kind (_e)== AR_TYPE_BANG)
# define ar_expr_is_array(_e) \
  (ar_expr_get_type_kind (_e) == AR_TYPE_ARRAY)

extern int
ar_expr_is_constant(ar_expr *e, ar_context *ctx);
extern ar_constant *
ar_expr_eval(ar_expr *e, ar_context *ctx);
extern ar_context_slot *
ar_expr_eval_slot(ar_expr *m, ar_context *ctx);
extern ccl_list *
ar_expr_get_variables(ar_expr *e, ar_context *ctx, ccl_list *result);
extern ccl_list *
ar_expr_get_slots (ar_expr *e, ar_context *ctx, ccl_list *result);

extern int
ar_expr_is_computable(ar_expr *e);

extern char *
ar_expr_to_string(ar_expr *e);

extern ar_expr *
ar_expr_replace(ar_expr *e, ar_context_slot *to_replace, ar_expr *newvalue);

extern ar_expr *
ar_expr_replace_with_tables(ar_expr *e, ar_context *ctx, ccl_hash **subst, 
			    int nb_tables, ccl_hash *cache);

extern ar_expr *
ar_expr_replace_with_context(ar_expr *e, ar_context *ctx);

extern ccl_list *
ar_expr_expand_composite_types(ar_expr *e, ar_context *ctx, ccl_list *result);

extern ar_expr *
ar_expr_remove_composite_slots(ar_expr *e, ar_context *ctx);

extern ar_expr *
ar_expr_remove_composite_slots_with_cache (ar_expr *e, ar_context *ctx,
					   ccl_hash *cache);

			/* --------------- */

extern int 
ar_expr_is_simplifiable (ar_expr *e);

extern ar_expr *
ar_expr_simplify(ar_expr *e);

extern ar_expr *
ar_expr_simplify_with_cache (ar_expr *e, ccl_hash *cache);

			/* --------------- */

extern ccl_list *
ar_expr_get_signatures (const ar_expr *e, ccl_list *result);

extern int
ar_expr_equals(const ar_expr *e1, const ar_expr *e2);

extern void
ar_expr_add_type_constraints_in_quantifiers (ar_expr *e);

			/* --------------- */

extern ccl_list *
ar_expr_get_array_indexes (ar_expr *e, ccl_list *result);

extern ar_expr *
ar_expr_crt_case (ar_expr **condval, int nb_args, ar_expr *dft);

extern ar_expr *
ar_expr_subst (ar_expr *e, ar_context *ctx, ar_expr *src, ar_expr *rep);

#endif /* ! AR_EXPR_H */
