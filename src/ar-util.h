/*
 * ar-util.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_UTIL_H
# define AR_UTIL_H

# include <ccl/ccl-common.h>
# include <ccl/ccl-set.h>

extern void
ar_util_init (void);

extern void
ar_util_terminate (void);

extern char *
ar_tilde_substitution (const char *filename);

extern uint32_t
ar_bit_size (uint32_t v);

/*!
 * Parse taglist as a list of coma-separated strings. Each string is
 * stored as an ar_identifier into the set 'result'. If 'result' is NULL
 * a new ccl_set is allocated and returned; else 'result' is returned.
 */
extern ccl_set * 
ar_parse_id_list (const char *taglist, ccl_set *result);

#endif /* ! AR_UTIL_H */
