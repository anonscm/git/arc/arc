/*
 * ar-event.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_EVENT_H
# define AR_EVENT_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-hash.h>
# include "ar-identifier.h"

typedef struct ar_event_st ar_event;

# define AR_EVENT_PUBLIC   0x01
# define AR_EVENT_PARENT   0x02
# define AR_EVENT_PRIVATE  0x04

extern ar_identifier *AR_EPSILON_ID;
extern ar_event *AR_EPSILON;

extern void
ar_event_init(void);

extern void
ar_event_terminate(void);

# define ar_event_create(name) ar_event_create_vector (name, 0, NULL)

extern ar_event *
ar_event_create_vector(ar_identifier *name, int vectsize, ar_event **vect);

extern ar_event *
ar_event_add_reference(ar_event *e);

extern void
ar_event_del_reference(ar_event *e);

extern void
ar_event_log (ccl_log_type log, ar_event *e);

extern void
ar_event_log_gen (ccl_log_type log, ar_event *e,
		  void (*idlog)(ccl_log_type log, const ar_identifier *id));

extern ar_identifier *
ar_event_get_name(const ar_event *e);

extern int
ar_event_has_epsilon_name (const ar_event *e);

extern int
ar_event_get_flags(ar_event *e);

extern void
ar_event_set_flags(ar_event *e, int flags);

extern const ccl_list *
ar_event_get_attr(ar_event *e);

extern void
ar_event_set_attr(ar_event *e, const ccl_list *attr);

extern int 
ar_event_has_attr (ar_event *e, ar_identifier *attribute);

extern int
ar_event_equals(const ar_event *e1, const ar_event *e2);

extern int
ar_event_compare (const ar_event *e1, const ar_event *e2);

extern unsigned int 
ar_event_hash (const ar_event *e);
 
extern ar_event *
ar_event_add_prefix(ar_event *ev, ar_identifier *pref, ccl_hash *cache);

extern int
ar_event_get_width(const ar_event *ev);

extern ar_event **
ar_event_get_components(const ar_event *ev);

extern int
ar_event_is_epsilon_vector (const ar_event *ev);

#endif /* ! AR_EVENT_H */
