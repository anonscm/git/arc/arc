/*
 * ar-expr-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_EXPR_P_H
# define AR_EXPR_P_H

# include "ar-expr.h"

struct ar_expr_st {
  int refcount;
  ar_expr_op op;
  ar_type *type;
  int arity;
  ar_expr **args;
  ar_context *qctx;
  ccl_list *subtypes;
  ccl_list *subargs;

  union {
    ar_identifier *field;
    ar_context_slot *slot;
    ar_constant *cst;
    ar_signature *sig;
    ccl_list *qvars;
    ar_identifier **fnames;
  } u;
};

#endif /* ! AR_EXPR_P_H */
