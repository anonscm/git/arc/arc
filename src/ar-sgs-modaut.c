/*
 * ar-sgs-modaut.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-state-graph-semantics-p.h"

#define QUOT_NODES_LABELS_PROP 	   "translators.ts.quot.nodes.labels"
#define QUOT_NODES_TOOLTIPS_PROP   "translators.ts.quot.nodes.tooltips"   
#define QUOT_NODES_ATTRIBUTES_PROP "translators.ts.quot.nodes.attributes"
#define QUOT_EDGES_LABELS_PROP 	   "translators.ts.quot.edges.labels"  
#define QUOT_EDGES_ATTRIBUTES_PROP "translators.ts.quot.edges.attributes"
#define QUOT_EDGES_EPSILON_PROP    "translators.ts.quot.edges.epsilon"

			/* --------------- */

static void
s_digraph_header (ccl_log_type log, ar_sgs *sgs, ccl_config_table *conf);

static ccl_hash *
s_compute_states_from_set (ar_sgs *sgs, ar_sgs_set *S, int states)
{
  ccl_hash *result = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *) ar_ca_expr_del_reference, 
		     (ccl_delete_proc *) ar_sgs_set_del_reference);
  ar_sgs_set *Ssv = ar_sgs_compute_proj (sgs, S, states);

  while (! ar_sgs_set_is_empty (sgs, Ssv))
    {
      ar_sgs_set *tmp;
      ar_sgs_set *s = ar_sgs_compute_pick (sgs, Ssv);
      ar_sgs_set *ssv = ar_sgs_compute_proj (sgs, s, states);
      ar_ca_expr *f = ar_sgs_set_states_to_ca_expr (sgs, ssv, NULL);

      if (ccl_hash_find (result, f))
	{
	  ar_ca_expr_del_reference (f);
	  ar_sgs_set_del_reference (ssv);
	}
      else
	{
	  ccl_hash_insert (result, ssv);
	}

      tmp = ar_sgs_compute_difference (sgs, Ssv, s);
      ar_sgs_set_del_reference (Ssv);
      ar_sgs_set_del_reference (s);
      Ssv = tmp;     
    }
  ar_sgs_set_del_reference (Ssv);

  return result;
}

			/* --------------- */

void
ar_sgs_to_modes_automaton (ccl_log_type log, ar_sgs *sgs, 
			   ccl_config_table *conf)
{
  ar_identifier *any_s_id = ar_identifier_create ("any_s");
  ccl_hash *svars = ar_ca_get_variables (sgs->ca, AR_CA_STATE_VARS);
  ccl_hash *fvars = ar_ca_get_variables (sgs->ca, AR_CA_FLOW_VARS);
  ar_sgs_set *any_s = ar_sgs_get_state_set (sgs, any_s_id);
  ccl_hash *ctrlstates = s_compute_states_from_set (sgs, any_s, 1);

  s_digraph_header (log, sgs, conf);
  
  {
    ccl_pointer_iterator *pi = ccl_hash_get_keys (ctrlstates);

    while (ccl_iterator_has_more_elements (pi))
      {
	ar_ca_expr *f = ccl_iterator_next_element (pi);

	ccl_log (log, "N%p[label=\"", f);
	ar_ca_expr_log (log, f);
	ccl_log (log, "\"]; \n");
      }
    ccl_iterator_delete (pi);
  }

  {
    ccl_pointer_iterator *pi = ccl_hash_get_keys (ctrlstates);
    ar_ca_event *epsilon = ar_ca_get_epsilon_event (sgs->ca);

    while (ccl_iterator_has_more_elements (pi))
      {
	ar_ca_expr *f = ccl_iterator_next_element (pi);
	ar_sgs_set *S = ccl_hash_get_with_key (ctrlstates, f);
	ar_sgs_set *rsrcS = ar_sgs_compute_rsrc (sgs, S);
	ccl_hash *events = ar_sgs_set_get_events (sgs, rsrcS);
	ccl_pointer_iterator *pe = ccl_hash_get_keys (events);

	ccl_log (log, "N%p[label=\"", f);
	ar_ca_expr_log (log, f);
	ccl_log (log, "\"]; \n");

	while (ccl_iterator_has_more_elements (pe))
	  {
	    ar_ca_event *e = ccl_iterator_next_element (pe);
	    ar_sgs_set *T = ar_sgs_set_get_rel_for_event (sgs, rsrcS, e);
	    ar_sgs_set *tmp = ar_sgs_compute_tgt (sgs, T);
	    ar_sgs_set *tgt = ar_sgs_compute_intersection (sgs, any_s, tmp);

	    ccl_hash *succstates = s_compute_states_from_set (sgs, tgt, 1);
	    ccl_pointer_iterator *spi = ccl_hash_get_keys (succstates);
	    while (ccl_iterator_has_more_elements (spi))
	      {
		ar_ca_expr *succ = ccl_iterator_next_element (spi);
		ar_sgs_set *succs = ccl_hash_get_with_key (succstates, succ);
		ar_sgs_set *tmp0 = ar_sgs_compute_rtgt (sgs, succs);
		ar_sgs_set *tmp1 = ar_sgs_compute_intersection (sgs, tmp0, T);
		ar_sgs_set *src = ar_sgs_compute_src (sgs, tmp1);
		ar_sgs_set *fsrc = ar_sgs_compute_proj (sgs, src, 0);
		ar_ca_expr *fe = ar_sgs_set_states_to_ca_expr (sgs, fsrc, NULL);
		int flow_is_true =
		  (ar_ca_expr_is_boolean_constant (fe) &&
		   ar_ca_expr_get_min (fe));

		ccl_assert (ccl_hash_find (ctrlstates, succ));

		ccl_log (log, "N%p -> N%p [label=\"", f, succ);
		if (! flow_is_true)
		  {
		    ccl_log (log, "flow = ");
		    ar_ca_expr_log (log, fe);
		  }
		if (e != epsilon)
		  {
		    if (! flow_is_true)
		      ccl_log (log, "\\nevent=");
		    ar_ca_event_log (log, e);
		  }
		ccl_log (log, "\"];\n");

		ar_sgs_set_del_reference (tmp1); 
		ar_sgs_set_del_reference (tmp0);
		ar_sgs_set_del_reference (src);
		ar_sgs_set_del_reference (fsrc);
		ar_ca_expr_del_reference (fe);
	      }
	    ccl_iterator_delete (spi);

	    ar_sgs_set_del_reference (T);	    
	    ar_sgs_set_del_reference (tgt);
	    ar_sgs_set_del_reference (tmp);
	    ccl_hash_delete (succstates);
	  }
	ccl_iterator_delete (pe);
	ccl_hash_delete (events);
	ar_sgs_set_del_reference (rsrcS);
      }
    ccl_iterator_delete (pi);
    ar_ca_event_del_reference (epsilon);
  }
  ccl_log (log, "}\n");  

  ar_sgs_set_del_reference (any_s);
  ar_identifier_del_reference (any_s_id);
  ccl_hash_delete (svars);
  ccl_hash_delete (fvars);
  ccl_hash_delete (ctrlstates);
}


			/* --------------- */

static void
s_digraph_header (ccl_log_type log, ar_sgs *sgs, ccl_config_table *conf)
{
  const char *tmp = ccl_config_table_get (conf, QUOT_NODES_ATTRIBUTES_PROP);
  ar_identifier *name = ar_sgs_get_name(sgs);

  ccl_log (log, "digraph \"");
  ar_identifier_log (log, name);
  ccl_log (log, " (mode automaton)\" {\n");
  ccl_log (log, "  label=\"");
  ar_identifier_log (log, name);
  ccl_log (log, "\";\n");

  ar_identifier_del_reference (name);

  if (tmp != NULL && *tmp != 0)
    ccl_log (log, "\tnode[%s];\n", tmp);

  tmp = ccl_config_table_get (conf, QUOT_EDGES_ATTRIBUTES_PROP);
  if (tmp != NULL && *tmp != 0) 
    ccl_log (log, "\tedge[%s];\n", tmp);
  ccl_log(log,"\n");
}
