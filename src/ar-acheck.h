/*
 * ar-acheck.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_ACHECK_H
# define AR_ACHECK_H

# include <ccl/ccl-config-table.h>

extern int
ar_acheck_init (ccl_config_table *conf);

extern void
ar_acheck_terminate (void);

#endif /* ! AR_ACHECK_H */
