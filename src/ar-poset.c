/*
 * ar-poset.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include "ar-poset.h"

typedef struct poset_element_st *poset_element;
typedef struct poset_edge_st *poset_edge;

struct poset_element_st {
  poset_element next_element;
  void *obj;
  poset_edge sons;
  poset_edge parents;
};

struct poset_edge_st {
  poset_edge next;
  poset_element element;
};

struct ar_poset_st {
  uint32_t refcount;
  ccl_hash *top_elements;
  ccl_hash *bottom_elements;
  ccl_hash *helements;
  poset_element elements;
  int set_size;
  ccl_hash_func *hash;
  ccl_duplicate_func *dup;
  ccl_compare_func *cmp;
  ccl_delete_proc *del;  
};

typedef struct poset_iterator_st {
  ccl_pointer_iterator super;  
  poset_element current_element; 
  ar_poset *poset;
} poset_iterator;

			/* --------------- */

ar_poset *
ar_poset_create (ccl_hash_func hash, ccl_duplicate_func dup, 
		 ccl_compare_func cmp, ccl_delete_proc del)
{
  ar_poset *result = ccl_new(struct ar_poset_st);

  result->refcount = 1;
  result->top_elements = ccl_hash_create (NULL, NULL, NULL, NULL);
  result->bottom_elements = ccl_hash_create (NULL, NULL, NULL, NULL);
  result->helements = ccl_hash_create (hash, cmp, NULL, NULL);
  result->elements = NULL;
  result->set_size = 0;
  result->hash = hash;
  result->dup = dup;
  result->del = del;
  result->cmp = cmp;

  return result;
}

			/* --------------- */

ar_poset *
ar_poset_add_reference(ar_poset *poset)
{
  ccl_pre( poset != NULL );

  poset->refcount++;

  return poset;
}

			/* --------------- */

static void
s_delete_edge_list(poset_edge pe)
{
  poset_edge next;

  for(; pe != NULL; pe = next)
    {
      next = pe->next;
      ccl_delete(pe);
    }
}

			/* --------------- */

void
ar_poset_del_reference(ar_poset *poset)
{
  ccl_pre( poset != NULL ); ccl_pre( poset->refcount > 0 );

  if( --poset->refcount == 0 )
    {
      poset_element el, next;

      ccl_hash_delete (poset->top_elements);
      ccl_hash_delete (poset->bottom_elements);

      for(el = poset->elements; el; el = next)
	{
	  next = el->next_element;

	  if( poset->del != NULL )
	    poset->del(el->obj);
	  s_delete_edge_list(el->sons);
	  s_delete_edge_list(el->parents);
	  ccl_delete(el);
	}
      ccl_hash_delete (poset->helements);
      ccl_delete(poset);
    }
}

			/* --------------- */

int
ar_poset_get_set_size(ar_poset *poset)
{
  ccl_pre( poset != NULL );

  return poset->set_size;
}

			/* --------------- */

static poset_element 
s_find_element (ar_poset *poset, void *obj)
{
  if (ccl_hash_find (poset->helements, obj))
    return ccl_hash_get (poset->helements);
  return NULL;
}

			/* --------------- */

static poset_element 
s_find_or_add_element(ar_poset *poset, void *obj)
{
  poset_element el = s_find_element (poset,obj);

  if (el == NULL)
    {
      el = ccl_new (struct poset_element_st);
      ccl_hash_insert (poset->helements, el);
      el->next_element = poset->elements;
      poset->elements = el;
      el->obj = poset->dup ? poset->dup (obj) : obj;
      el->sons = NULL;
      el->parents = NULL;

      poset->set_size++;
      ccl_hash_find (poset->top_elements, el);
      ccl_hash_insert (poset->top_elements, el);
      ccl_hash_find (poset->bottom_elements, el);
      ccl_hash_insert (poset->bottom_elements, el);
    }

  return el;
}

			/* --------------- */

void
ar_poset_add(ar_poset *poset, void *obj)
{
  s_find_or_add_element (poset, obj);
}

			/* --------------- */

int
ar_poset_has(ar_poset *poset, void *obj)
{
  return (s_find_element (poset, obj) != NULL);
}

			/* --------------- */

static int
s_look_for_a_path(poset_element pe1, poset_element pe2)
{
  int result; 
  poset_edge e;

  if(pe1 != NULL && pe1 == pe2 ) 
    return 1;

  if( pe1 == NULL || pe2 == NULL || pe1->sons == NULL || pe2->parents == NULL )
    return 0;

  result = 0;
    
  for(e = pe1->sons; e && ! result; e = e->next)
    result = ( e->element == pe2 );

  for(e = pe1->sons; e && ! result; e = e->next)
    result = s_look_for_a_path(e->element,pe2);

  return result;
}

			/* --------------- */

int
ar_poset_has_path (ar_poset *poset, void *obj1, void *obj2)
{
  poset_element pe1 = s_find_element (poset, obj1);
  poset_element pe2 = s_find_element (poset, obj2);

  return s_look_for_a_path (pe1, pe2);
}

			/* --------------- */

void *
ar_poset_get_object(ar_poset *poset, void *obj)
{
  poset_element pe = s_find_element (poset, obj);

  if (pe == NULL)
    return NULL;

  if (poset->dup != NULL)
    return poset->dup (pe->obj);
  return pe->obj;
}

			/* --------------- */

static poset_edge *
s_find_edge(poset_edge *list, poset_element el)
{
  poset_edge *pe;

  for(pe = list; *pe && (*pe)->element != el; pe = &((*pe)->next))
    /* do nothing */;

  return pe;
}

			/* --------------- */

int
ar_poset_add_pair(ar_poset *poset, void *obj1, void *obj2)
{
  poset_edge   *pe;
  poset_element el1 = s_find_or_add_element (poset, obj1);
  poset_element el2 = s_find_or_add_element (poset, obj2);

  if (ar_poset_has_path (poset, obj2, obj1))
    return 0;

  if (el1->sons == NULL) 
    {
      ccl_assert (ccl_hash_find (poset->bottom_elements, el1));
      ccl_hash_find (poset->bottom_elements, el1);
      ccl_hash_remove (poset->bottom_elements);
    }

  if (el2->parents == NULL) 
    {
      ccl_assert (ccl_hash_find (poset->top_elements, el2));
      ccl_hash_find (poset->top_elements, el2);
      ccl_hash_remove (poset->top_elements);
    }

  pe = s_find_edge (&el1->sons, el2);
  if (*pe != NULL)
    {
      ccl_assert (*s_find_edge(&el2->parents,el1) != NULL);
      ccl_assert ((*s_find_edge(&el2->parents,el1))->element == el1);

      return 1;
    }

  *pe = ccl_new (struct poset_edge_st);
  (*pe)->next = NULL;
  (*pe)->element = el2;

  pe = s_find_edge (&el2->parents, el1);
  ccl_assert (*pe == NULL);

  *pe = ccl_new (struct poset_edge_st);
  (*pe)->next = NULL;
  (*pe)->element = el1;

  return 1;
}

			/* --------------- */

void
ar_poset_add_pair_no_check (ar_poset *poset,  void *obj1, void *obj2)
{
  poset_edge e;
  poset_element el1 = s_find_or_add_element (poset, obj1);
  poset_element el2 = s_find_or_add_element (poset, obj2);

  if (el1->sons == NULL) 
    {
      ccl_assert (ccl_hash_find (poset->bottom_elements, el1));
      ccl_hash_find (poset->bottom_elements, el1);
      ccl_hash_remove (poset->bottom_elements);
    }

  if (el2->parents == NULL) 
    {
      ccl_assert (ccl_hash_find (poset->top_elements, el2));
      ccl_hash_find (poset->top_elements, el2);
      ccl_hash_remove (poset->top_elements);
    }

  e = ccl_new (struct poset_edge_st);
  e->next = el1->sons;
  el1->sons = e;
  e->element = el2;

  e = ccl_new (struct poset_edge_st);
  e->next = el2->parents;
  el2->parents = e;
  e->element = el1;
}

			/* --------------- */

int
ar_poset_add_poset(ar_poset *poset, ar_poset *other)
{
  poset_element el;
  poset_edge son;

  ccl_pre( poset != NULL ); ccl_pre( other != NULL );

  for(el = other->elements; el; el = el->next_element)
    {
      ar_poset_add(poset,el->obj);
      for(son = el->sons; son; son = son->next)
	{
	  if( ! ar_poset_add_pair(poset,el->obj,son->element->obj) )
	    return 0;
	}
    }

  return 1;
}

			/* --------------- */

int
ar_poset_add_lt(ar_poset *poset, ar_poset *poset1, ar_poset *poset2)
{
  int result;
  ccl_pointer_iterator *i2; 

  ccl_pre( poset != NULL ); ccl_pre( poset1 != NULL ); 
  ccl_pre( poset2 != NULL );

  if (! ar_poset_add_poset (poset, poset1))
    return 0;

  if (! ar_poset_add_poset (poset, poset2))
    return 0;

  result = 1;
  i2 = ccl_hash_get_elements (poset2->bottom_elements); 
  while (ccl_iterator_has_more_elements (i2) && result)
    {
      poset_element e2 = (poset_element) ccl_iterator_next_element (i2);
      ccl_pointer_iterator *i1 = 
	ccl_hash_get_elements (poset1->top_elements); 
      while (ccl_iterator_has_more_elements (i1) && result)
	{
	  poset_element e1 = (poset_element) ccl_iterator_next_element (i1);

	  if (! ar_poset_add_pair (poset, e2->obj, e1->obj))
	    result = 0;
	}
      ccl_iterator_delete (i1);
    }
  ccl_iterator_delete (i2);

  return result;
}

			/* --------------- */

void
ar_poset_reverse(ar_poset *poset)
{
  poset_element el;
  
  ccl_pre( poset != NULL );

  {
    ccl_hash *aux = poset->top_elements;
    poset->top_elements = poset->bottom_elements;
    poset->bottom_elements = aux;
  }

  for(el = poset->elements; el; el = el->next_element)
    {
      poset_edge tmp = el->sons;
      
      el->sons = el->parents;
      el->parents = tmp;
    }
}

			/* --------------- */

ccl_list *
ar_poset_get_greater_objects(ar_poset *poset, void *obj)
{
  ccl_list *todo = ccl_list_create ();
  poset_edge pe;
  poset_element el;
  ccl_list *result = ccl_list_create ();
  
  ccl_pre (ar_poset_has (poset, obj));

  el = s_find_element (poset, obj);
  for (pe = el->parents; pe; pe = pe->next)
    ccl_list_add (todo, pe->element);

  while (! ccl_list_is_empty (todo))
    {
      poset_element el = (poset_element) ccl_list_take_first (todo);
      
      if (CCL_PTRHASBIT (el->obj))
	continue;

      ccl_list_add (result, el->obj);
      el->obj = CCL_BITPTR (void *, el->obj);

      for (pe = el->parents; pe; pe = pe->next)
	{
	  if (! CCL_PTRHASBIT (pe->element->obj))
	    ccl_list_add (todo, pe->element);
	}
    }
  
  for (el = poset->elements; el; el = el->next_element)
    el->obj = CCL_BITPTR2PTR (void *, el->obj);
  ccl_list_delete (todo);

  return result;
}

			/* --------------- */

static int
s_poset_iterator_has_more_elements(const ccl_pointer_iterator *i)
{
  poset_iterator *pei = (poset_iterator *)i;

  ccl_pre( pei != NULL );


  return pei->current_element != NULL;
}

			/* --------------- */

static void *
s_poset_iterator_next_element(ccl_pointer_iterator *i)
{
  void *result;
  poset_iterator *pei = (poset_iterator *)i;

  ccl_pre( pei != NULL );  ccl_pre( pei->current_element != NULL );

  result = pei->current_element->obj;
  if( pei->poset->dup != NULL )
    result = pei->poset->dup(result);

  pei->current_element = pei->current_element->next_element;

  return result;
}

			/* --------------- */

static void
s_poset_iterator_delete_iterator(ccl_pointer_iterator *i)
{
  poset_iterator *pei = (poset_iterator *)i;

  ar_poset_del_reference(pei->poset);
  ccl_delete(i);
}

			/* --------------- */

ccl_pointer_iterator *
ar_poset_get_objects(ar_poset *poset)
{
  static const ccl_pointer_iterator iterator = {
    s_poset_iterator_has_more_elements,
    s_poset_iterator_next_element,
    s_poset_iterator_delete_iterator
  };
  poset_iterator *result = ccl_new(poset_iterator);

  result->super = iterator;
  result->poset = ar_poset_add_reference(poset);
  result->current_element = poset->elements;
  
  return (ccl_pointer_iterator *)result;
}

			/* --------------- */

static ccl_list *
s_get_ordered_objects(ar_poset *poset, int top_down)
{
  ccl_list *result = ccl_list_create();
  ccl_hash *ht = top_down ? poset->top_elements : poset->bottom_elements;
  ccl_pointer_iterator *i = ccl_hash_get_elements (ht);
  ccl_list *stack = ccl_list_create_from_iterator (i);
  ccl_iterator_delete (i);

  while( ! ccl_list_is_empty(stack) )
    {
      poset_edge e;
      poset_element pe = (poset_element)ccl_list_take_first(stack);
      
      if( poset->dup ) 
	ccl_list_add(result,poset->dup(pe->obj));
      else
	ccl_list_add(result,pe->obj);

      for(e = top_down?pe->sons:pe->parents; e; e = e->next)
	if( ! ccl_list_has(stack,e->element) )
	  ccl_list_add(stack,e->element);      
    }
  ccl_list_delete(stack);

  return result;
}

			/* --------------- */

ccl_list *
ar_poset_get_top_to_down_objects(ar_poset *poset)
{
  return s_get_ordered_objects(poset,1);
}

			/* --------------- */

ccl_list *
ar_poset_get_down_to_top_objects(ar_poset *poset)
{
  return s_get_ordered_objects(poset,0);
}

			/* --------------- */

int
ar_poset_is_empty(ar_poset *poset)
{
  ccl_pre( poset != NULL );

  return (poset->set_size == 0 ||
	  poset->set_size == ccl_hash_get_size (poset->top_elements));
}

			/* --------------- */

void
ar_poset_log(ccl_log_type log, ar_poset *poset, 
	     void (*logproc)(ccl_log_type log, void *obj))
{  
  ccl_pointer_iterator *i = ar_poset_get_objects(poset);

  if( ! ccl_iterator_has_more_elements(i) )
    ccl_log(log,"empty\n");
  else while( ccl_iterator_has_more_elements(i) )
    {
      ccl_pair *p;
      void *obj = ccl_iterator_next_element(i);
      ccl_list *gt = ar_poset_get_greater_objects(poset,obj);
      
      logproc(log,obj);
      if( ccl_list_get_size(gt) > 0 )
	{
	  if( ccl_list_get_size(gt) > 1 ) ccl_log(log, " < { ");
	  else ccl_log(log, " < ");

	  for(p = FIRST(gt); p; p = CDR(p))
	    {
	      logproc(log,CAR(p));
	      if( CDR(p) != NULL )
		ccl_log(log,", ");
	      if( poset->del != NULL )
		poset->del(CAR(p));
	    }

	  if( ccl_list_get_size(gt) > 1 ) ccl_log(log," }");
	}
      ccl_log(log,"\n");
      if( poset->del != NULL )
	poset->del(obj);

      ccl_list_delete(gt);
    }
  ccl_iterator_delete(i);
}

			/* --------------- */

ar_poset *
ar_poset_dup(ar_poset *poset)
{
  int i;
  poset_element pe;
  poset_element re;
  poset_element *pre;
  ar_poset *result = ar_poset_create (poset->hash, poset->dup, poset->cmp, 
				      poset->del);
  ccl_hash *p2r = ccl_hash_create(NULL,NULL,NULL,NULL);

  for(pre = &result->elements, pe = poset->elements; pe; 
      pe = pe->next_element, pre = &((*pre)->next_element))
    {
      *pre = ccl_new(struct poset_element_st);
      (*pre)->next_element = NULL;
      (*pre)->obj = poset->dup?poset->dup(pe->obj):pe->obj;
      (*pre)->sons = NULL;
      (*pre)->parents = NULL;

      ccl_hash_find(p2r,pe);
      ccl_hash_insert(p2r,*pre);
      ccl_hash_find (result->helements, (*pre)->obj);
      ccl_hash_insert (result->helements, *pre);
    }

  for (i = 0; i < 2; i++)
    {
      ccl_hash *rt;
      ccl_pointer_iterator *pi;
      
      if (i == 0)
	{
	  rt = result->top_elements;
	  pi = ccl_hash_get_elements (poset->top_elements);
	}
      else 
	{
	  rt = result->bottom_elements;
	  pi = ccl_hash_get_elements (poset->bottom_elements);
	}
      while (ccl_iterator_has_more_elements (pi))
	{
	  poset_element el = ccl_iterator_next_element (pi);
	  ccl_hash_find (rt, el);
	  ccl_hash_insert (rt, el);
	}
      ccl_iterator_delete (pi);
    }

  result->set_size = poset->set_size;

  for(re = result->elements, pe = poset->elements; pe; 
      pe = pe->next_element, re = re->next_element)
    {
      poset_edge edge = pe->sons;
      poset_edge *redge = &(re->sons);

      for(i = 0; i < 2; i++)
	{

	  for(; edge; edge = edge->next, redge = &((*redge)->next))
	    {
	      ccl_hash_find(p2r,edge->element);

	      *redge = ccl_new(struct poset_edge_st);
	      (*redge)->next = NULL;
	      (*redge)->element = ccl_hash_get(p2r);
	    }
	  edge = pe->parents;
	  redge = &(re->parents);
	}
    }
  ccl_hash_delete(p2r);

  return result;
}

			/* --------------- */

void 
s_topological_order (ar_poset *poset, poset_element el, ccl_list *result, 
		     ccl_hash *done)
{
  poset_edge pe;

  if (ccl_hash_find (done, el))
    return;
  ccl_hash_insert (done, el);

  for (pe = el->sons; pe; pe = pe->next)
    s_topological_order (poset, pe->element, result, done);
  ccl_list_add (result, poset->dup ? poset->dup (el->obj) : el->obj);  
}

			/* --------------- */

ccl_list *
ar_poset_get_topological_order (ar_poset *poset)
{
  ccl_list *result = ccl_list_create ();
  ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_pointer_iterator *pi = ccl_hash_get_elements (poset->top_elements);

  while (ccl_iterator_has_more_elements (pi))
    {
      poset_element el = ccl_iterator_next_element (pi);
      s_topological_order (poset, el, result, done);
    }
  ccl_iterator_delete (pi);
  ccl_hash_delete (done);

  return result;
}
