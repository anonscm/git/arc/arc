/*
 * ar-ca-event.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-array.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-set.h>
#include "ar-event.h"
#include "ar-constraint-automaton.h"

struct ar_ca_event_st {
  int refcount;
  const ccl_list *hierarchy;
  ccl_list *attrs;
  ar_idtable *nodes_to_names;
  ccl_set *event_names;
  ccl_list *ordered_ids;
};

struct id_iterator {
  ar_identifier_iterator super;
  ccl_pair *current;
  const ar_ca_event *event;
};

static int
s_ii_has_more_elements (const ar_identifier_iterator *i);

static ar_identifier *
s_ii_next_element (ar_identifier_iterator *i);

static void
s_ii_delete_iterator (ar_identifier_iterator *i);


			/* --------------- */

ar_ca_event *
ar_ca_event_create (const ccl_list *H, ccl_list *ids)
{
  ar_ca_event *result = ccl_new (ar_ca_event);

  result->refcount = 1;
  result->attrs = ccl_list_create ();
  result->hierarchy = H;
  result->nodes_to_names =
    ar_idtable_create (0, (ccl_duplicate_func *) ar_identifier_add_reference,
		       (ccl_delete_proc *) ar_identifier_del_reference);
  
  result->event_names = ccl_set_create ();
  result->ordered_ids = ccl_list_create ();
  
  while (! ccl_list_is_empty (ids))
    {
      ar_identifier *id = ccl_list_take_first (ids);
      ar_identifier *node;

      if (ar_identifier_has_name (id, AR_EPSILON_ID))
	{
	  ar_identifier_del_reference (id);
	  continue;
	}
      node = ar_identifier_get_prefix (id);
      ccl_pre (! ar_idtable_has (result->nodes_to_names, node));
      
      ar_idtable_put (result->nodes_to_names, node, id);
      ccl_set_add (result->event_names, id);
      ccl_list_add (result->ordered_ids, id);
      ar_identifier_del_reference (node);
    }  
  ccl_list_delete (ids);
  
  return result;
}

			/* --------------- */

ar_ca_event *
ar_ca_event_add_reference(ar_ca_event *ev)
{
  ccl_pre( ev != NULL );

  ev->refcount++;

  return ev;
}

			/* --------------- */

void
ar_ca_event_del_reference(ar_ca_event *ev)
{
  ccl_pre( ev != NULL );   ccl_pre( ev->refcount > 0 );

  if( --ev->refcount == 0 )
    {
      ccl_list_clear_and_delete (ev->ordered_ids, (ccl_delete_proc *)
				 ar_identifier_del_reference);
      ccl_list_clear_and_delete (ev->attrs, (ccl_delete_proc *)
				 ar_identifier_del_reference);
      ccl_set_delete (ev->event_names);
      ar_idtable_del_reference (ev->nodes_to_names);
      ccl_delete(ev);
    }
}

			/* --------------- */

void
ar_ca_event_log (ccl_log_type log, ar_ca_event *ev)
{
  ar_ca_event_log_gen (log, ev, 0, ".", NULL);
}

			/* --------------- */

void
ar_ca_event_log_gen (ccl_log_type log, ar_ca_event *ev, int show_epsilon, 
		     const char *separator, const char *quotes)
{
  int need_par, first;
  ar_identifier_iterator *ii;
  
  if (show_epsilon)
    need_par = (ccl_list_get_size (ev->hierarchy) > 1);
  else
    need_par = ccl_set_get_size (ev->event_names) > 1;

  if (need_par)
    {
      if (quotes)
	ccl_log (log, "%c<", quotes[0]);
      else
	ccl_log (log, "<");
    }
  
  if (show_epsilon)
    {
      ii = ar_ca_event_get_ids (ev);
      first = 1;
      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *id = ccl_iterator_next_element (ii);
	  
	  if (show_epsilon || ! ar_identifier_has_name (id, AR_EPSILON_ID))
	    {
	      if (! first) 
		ccl_log (log, ", ");
	      else 
		first = 0;
	      
	      if (quotes && !need_par)
		ar_identifier_log_global_quote_gen (log, id, separator, quotes);
	      else
		ar_identifier_log (log, id);
	    }
	  
	  ar_identifier_del_reference(id);
	}
    }
  else
    {
      ii = ar_ca_event_get_non_epsilon_ids (ev);
      first = 1;
      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *id = ccl_iterator_next_element (ii);
	  
	  if (! first) 
	    ccl_log (log, ", ");
	  else 
	    first = 0;
	      
	  if (quotes && !need_par)
	    ar_identifier_log_global_quote_gen (log, id, separator, quotes);
	  else
	    ar_identifier_log (log, id);
	  ar_identifier_del_reference(id);
	}
    }
  
  ccl_iterator_delete (ii);
  if (need_par)
    {
      if (quotes)
	ccl_log (log, ">%c",  quotes[1]);
      else
	ccl_log (log, ">");
    }
}

			/* --------------- */

int
ar_ca_event_contains (ar_ca_event *ev, ar_identifier *id)
{
  ccl_pre (ev != NULL);
  ccl_pre (id != NULL);

  return ccl_set_has (ev->event_names, id);
}

			/* --------------- */

int
ar_ca_event_equals (ar_ca_event *ev, ar_ca_event *other)
{
  return ev == other;
}

			/* --------------- */

int
ar_ca_event_is_epsilon(ar_ca_event *ev)
{
  ccl_pre (ev != NULL);
  
  return ccl_set_get_size (ev->event_names) == 0;
}

			/* --------------- */

ar_identifier_iterator *
ar_ca_event_get_ids (const ar_ca_event *ev)
{
  struct id_iterator *result;
  
  ccl_pre (ev != NULL);

  result = ccl_new (struct id_iterator);
  result->super.has_more_elements = s_ii_has_more_elements;
  result->super.next_element = s_ii_next_element;
  result->super.delete_iterator = s_ii_delete_iterator;
  result->current = FIRST (ev->hierarchy);
  result->event = ev;
  
  return (ar_identifier_iterator *) result;
}

ar_identifier_iterator *
ar_ca_event_get_non_epsilon_ids (const ar_ca_event *ev)
{
  ccl_pre (ev != NULL);

  return (ar_identifier_iterator *)
    ccl_list_get_iterator (ev->ordered_ids, 
			   (ccl_duplicate_func *) ar_identifier_add_reference,
			   NULL);
}

			/* --------------- */

void
ar_ca_event_add_attribute (ar_ca_event *ev, ar_identifier *id)
{
  ccl_pre (ev != NULL);
  ccl_pre (id != NULL);

  id = ar_identifier_add_reference (id);
  ccl_list_add (ev->attrs, id);
}

			/* --------------- */

int
ar_ca_event_has_attribute (ar_ca_event *ev, ar_identifier *id)
{
  ccl_pre (ev != NULL);

  return ccl_list_has (ev->attrs, id);
}

			/* --------------- */

ccl_list *
ar_ca_event_get_attributes (ar_ca_event *ev)
{
  ccl_pre (ev != NULL);

  return ev->attrs;
}

static int
s_ii_has_more_elements (const ar_identifier_iterator *i)
{
  struct id_iterator *ii = (struct id_iterator *) i;

  return (ii->current != NULL);
}

static ar_identifier *
s_ii_next_element (ar_identifier_iterator *i)
{
  struct id_iterator *ii = (struct id_iterator *) i;
  ar_identifier *node = CAR (ii->current);
  ar_identifier *id = ar_idtable_get (ii->event->nodes_to_names, node);

  if (id == NULL)
    id = ar_identifier_add_prefix (AR_EPSILON_ID, node);
  ii->current = CDR (ii->current);
  
  return id;
}

static void
s_ii_delete_iterator (ar_identifier_iterator *i)
{
  ccl_delete (i);
}
