/*
 * observer.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef OBSERVER_H
# define OBSERVER_H

# include <ccl/ccl-set.h>
# include "ar-constraint-automaton.h"

/*!
 * The function decorates the constraint automaton 'ca' with additional Boolean
 * variables; one variable per elementary and visible event. The algorithm 
 * works as follows. First each vector of events is visited to establish the 
 * table of visible or disabled elementary events. Then for each transition 
 * labelled by a visible vector, we add new assignments that set to true 
 * Boolean variables associated to the vector.
 *
 * The routine also build a table *p_e2vl which associates to each 
 * synchronization vector a list of Boolean variables; each variable corresponds
 * to one elementary event belonging to the vector and that has the ``visible''
 * attribute.
 *
 * The function returns the list of Boolean variables it creates.
 * 
 * Parameters:
 *   ca       : the studied constraint automaton
 *   visible  : the attributes that identify visible events.
 *   disabled : the attributes that identify disabled events.
 *   prefix   : a string that prefixes each identifier of create variables.
 * Result:
 *   *p_e2vl : table event -> Boolean variables
 *   *p_vars : the set of created variables
 */
extern void
ar_ca_add_event_observer (ar_ca *ca, ccl_set *visible, ccl_set *disabled,
			  ccl_set **p_vars, ccl_hash **p_e2vl,
			  ccl_set **p_invvars);

/*!
 * Check if an elementary event possesses an attribute in table 'attrs'. The
 * function search through the hiearchy starting at 'node' for the ar_node 
 * where the event pointed by 'eventname' is declared. The function then 
 * returns true if the event has an attribute that appears in 'attrs'.
 * 
 * Parameter:
 *   node      : the root node for the search of event 'eventname'
 *   eventname : the identifier of the event
 *   attrs     : the table that contains searched attributes
 * Result:
 *   returns true if eventname has an attribute in 'attrs'
 */
extern int
ar_elementary_event_has_one_of_attrs (ar_node *node, ar_identifier *eventname, 
				      ccl_set *attrs);

extern int
ar_elementary_event_has_attribute (ar_node *node, ar_identifier *eventname, 
				   ar_identifier *attribute);

extern int
ar_elementary_event_has_attribute_gen (ar_node *node, ar_identifier *eventname, 
				       int (*has_attr) (ar_identifier *attr,
							void *data),
				       void *data);

extern ar_identifier *
observer_get_event_variable_name (const ar_identifier *eventname);

extern char *
observer_get_event_name_from_var (const ar_ca_expr *variable);

#endif /* ! OBSERVER_H */
