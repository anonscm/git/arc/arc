/*
 * arc-shell-interp.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdio.h>
#include <signal.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-time.h>
#include "parser/altarica-tree.h"
#include "arc.h"
#include "arc-commands.h"
#include "arc-shell.h"
#include "arc-shell-preferences.h"

static int
s_get_nb_nodes(ccl_parse_tree *t)
{
  int result = 0;
  for(; t != NULL; t = t->next)
    result++;

  return result;
}

			/* --------------- */

static int
s_interp_command(ccl_parse_tree *top, arc_command_parameter *presult)
{
  int result = 0;
  int error;
  arc_command cmd;

  ccl_pre( top->child->node_type == AR_TREE_STRING );

  *presult = NULL;

  cmd = arc_command_find(top->child->value.string_value);

  if( cmd == NULL )
    {
      ccl_log(CCL_LOG_ERROR,"%s:%d: unknown command '%s'\n",
	      top->filename,top->line,top->child->value.string_value);
    }
  else
    {
      ccl_parse_tree *t = top->child->next;
      int i = 0;
      int nb_params = s_get_nb_nodes(t);
      arc_command_parameter *params = ccl_new_array(arc_command_parameter,
						    nb_params);
      error = 0;
      for(; t != NULL && ! error; t = t->next, i++)
	{
	  switch( t->node_type ) {
	  case AR_TREE_STRING :
	    params[i] = 
	      arc_command_parameter_create_string(t->value.string_value);
	    break;
	  case AR_TREE_INTEGER :
	    params[i] =  arc_command_parameter_create_int(t->value.int_value);
	    break;
	  case AR_TREE_FLOAT :
	    params[i] =  arc_command_parameter_create_float(t->value.flt_value);
	    break;
	  };
	}

      if( ! error )
	{
	  void (*oldhdl)(int) = signal (SIGINT, SIG_DFL);
	  
	  result = arc_command_execute (cmd, ARC_PREFERENCES, nb_params,params,
					presult);
	  signal (SIGINT, oldhdl);
	}

      for(i = 0; i < nb_params; i++)
	{
	  if( params[i] != NULL )
	    arc_command_parameter_delete(params[i]);
	}
      ccl_delete(params);
    }

  return result;
}

			/* --------------- */

static int
s_interp_and_display_command(ccl_parse_tree *top)
{
  int stop = 0;
  arc_command_parameter result;
  

  if( ! s_interp_command(top,&result) )
    return 0;

  if( result == NULL )
    return 1;

  switch( result->type ) {
  case ARC_CMDP_INTEGER :
    ccl_log(CCL_LOG_DISPLAY,"%d", result->u.int_value);
    break;
  case ARC_CMDP_FLOAT :
    ccl_log(CCL_LOG_DISPLAY,"%g", result->u.flt_value);
    break;
  case ARC_CMDP_STRING :
  case ARC_CMDP_IDENTIFIER :
    ccl_log(CCL_LOG_DISPLAY,"%d", result->u.string_value);
    break;
  case ARC_CMDP_CUSTOM :
    /*
    {
      shell_custom_parameter p = 
	(shell_custom_parameter)result->u.custom;
      if( p->methods->type == SHELL_TERMINATE )
	stop = 1;
      else if( p->methods->print != NULL )
	p->methods->print("$$",p->value);
    }
    */
    break;
  };
  arc_command_parameter_delete(result);

  return stop;
}

			/* --------------- */

static int
s_interp_cmd (ccl_parse_tree *t)
{
  ccl_pre (t->node_type == AR_TREE_SHELL_CMD);

  return s_interp_and_display_command(t);
}

			/* --------------- */

static void
s_log_proc_for_FILE(ccl_log_type type, const char  *msg, void *data)
{
  FILE *stream = (FILE*)data;
  fprintf(stream,"%s",msg);
  fflush(stream);
}

			/* --------------- */

void
arc_shell_interp(ccl_parse_tree *t)
{
  int stop = 0;

  while( t != NULL && ! stop )
    {
      ccl_assert( t->node_type == AR_TREE_SHELL_CMD || 
		  t->node_type == AR_TREE_SHELL_REDIR_APPEND ||
		  t->node_type == AR_TREE_SHELL_REDIR_CREATE ||
		  t->node_type == AR_TREE_SHELL_REDIR_PIPE );
      
      if( t->node_type == AR_TREE_SHELL_REDIR_APPEND ||
	  t->node_type == AR_TREE_SHELL_REDIR_CREATE )
	{
	  const char *mode = (t->node_type == AR_TREE_SHELL_REDIR_APPEND)?"a":"w";
	  FILE          *f = fopen(t->child->value.string_value,mode);

	  if( f == NULL ) 
	    {
	      ccl_log(CCL_LOG_ERROR,"%s:%d: can't redirect to '%s'.\n",
		     t->filename,t->line,t->child->value.string_value);
	    }
	  else
	    {
	      ccl_log_push_redirection (CCL_LOG_DISPLAY,s_log_proc_for_FILE,f);
	      stop = ! s_interp_cmd(t->child->next);
	      ccl_log_pop_redirection (CCL_LOG_DISPLAY);
	      fflush(f);
	      fclose(f);
	    }
	  
	}
      else if( t->node_type == AR_TREE_SHELL_REDIR_PIPE )
	{
	  FILE *pipemore;
  
	  if( (pipemore = popen (t->child->value.string_value,"w")) == NULL )
	    {
	      ccl_log(CCL_LOG_ERROR,"can't create the 'more' process\n");
	      stop = 1;
	    }
	  else
	    {
	      ccl_log_push_redirection (CCL_LOG_DISPLAY, 
					s_log_proc_for_FILE, pipemore);
	      stop = ! s_interp_cmd(t->child->next);
	      pclose (pipemore);
	      ccl_log_pop_redirection (CCL_LOG_DISPLAY);
	    }
	}
      else
	{
	  stop = ! s_interp_cmd(t);
	}
      if (ARC_SHELL_CURRENT_CONTEXT && ARC_SHELL_CURRENT_CONTEXT->cr_mode)
	{
	  if (! stop)
	    ccl_display ("EOC\n");
	  else
	    ccl_display ("EOCWE\n");
	}
      t = t->next;
    }
}

			/* --------------- */

