/*
 * boolean-formula.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdint.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-list.h>
#include "boolean-formula.h"

typedef enum { BF_CST, BF_OR, BF_AND, BF_NOT, BF_VAR } bf_kind;

struct boolean_formula_st
{
  int refcount;
  bf_kind kind;
  char *comment;
  bf_index index;  
  uint32_t size;
  bf_manager *man;
  bf_formula *operands[2];  
  bf_literal cnf;
};

struct boolean_formula_manager_st
{
  ccl_hash *ut;
  bf_formula tmp;
  bf_fresh_index_allocator *fia;
  void *fia_data;
};

#define ENABLE_REWRITING_RULES 1

#define IS_T(f) ((f)->kind == BF_CST && (f)->index == 1)
#define IS_F(f) ((f)->kind == BF_CST && (f)->index == 0)

			/* --------------- */

static bf_index 
s_alloc_index (bf_manager *bm);

static bf_formula *
s_alloc_formula (bf_manager *bm, bf_kind kind, int index, 
		 bf_formula *f1, bf_formula *f2);

static void
s_delete_formula (bf_formula *f);

static void
s_check_formula (bf_formula *f);

static ccl_hash *
s_make_sharing_table (void);

			/* --------------- */

bf_manager *
bf_crt_manager (bf_fresh_index_allocator *fia, void *clientdata)
{
  bf_manager *result = ccl_new (bf_manager);

  ccl_pre (fia != NULL);
  result->ut = s_make_sharing_table ();
  result->fia = fia;
  result->fia_data = clientdata;

  return result;
}

void 
bf_manager_destroy (bf_manager *bm)
{
  ccl_pre (bm != NULL);

  ccl_assert (ccl_hash_get_size (bm->ut) == 0);
  ccl_hash_delete (bm->ut);
  ccl_delete (bm);
}

bf_formula *
bf_crt_true (bf_manager *bm)
{
  ccl_pre (bm != NULL);

  return s_alloc_formula (bm, BF_CST, 1, NULL, NULL);
}

bf_formula *
bf_crt_false (bf_manager *bm)
{
  ccl_pre (bm != NULL);

  return s_alloc_formula (bm, BF_CST, 0, NULL, NULL);
}

static int
s_are_opposite (const bf_formula *f1, const bf_formula *f2) 
{
  return ((f1->kind == BF_NOT && f1->operands[0] == f2) ||
	  (f2->kind == BF_NOT && f2->operands[0] == f1));
}

/* AND(AND(a,b),a) => AND (a,b) */
#define RS1(a_,b_)	      \
  do {							      \
    if ((a_)->kind == BF_AND &&				      \
	((a_)->operands[0] == (b_) || (a_)->operands[1] == (b_)))	\
      {							\
	bf_unref (b_);						\
	return a_;						\
      }								\
  } while (0)
    
/* AND(AND(a,b),-a) => 0 */
#define RS2(a_,b_)				\
  do {					\
    if ((a_)->kind == BF_AND &&				      \
	(s_are_opposite ((a_)->operands[0],(b_)) ||	      \
	 s_are_opposite ((a_)->operands[1],(b_))))	      \
      {							      \
	bf_unref (a_);					      \
	bf_unref (b_);					      \
	return bf_crt_false (bm);			      \
      }							      \
  } while (0)

/* AND(a,-AND(-a,b)) => a */
#define RS3(a_,b_)						\
  do {								\
  if ((b_)->kind == BF_NOT &&					\
      (b_)->operands[0]->kind == BF_AND &&			\
      (s_are_opposite (a_, (b_)->operands[0]->operands[0]) ||	\
       s_are_opposite (a_, (b_)->operands[0]->operands[1])))	\
    {								\
      bf_unref (b_);						\
      return (a_);						\
    }								\
  } while (0)

/* AND(AND(a,b),AND(a,c)) => AND(AND(a,b),c) */
#define RS4a(a_,b_)						\
  do {								\
  if ((a_)->kind == BF_AND && (b_)->kind == BF_AND &&           \
      (a_)->operands[0] == (b_)->operands[0]) \
    {								\
      bf_formula *bf = bf_crt_and (bm, (a_), bf_ref ((b_)->operands[1])); \
      bf_unref (b_); \
      return bf; \
    }								\
  } while (0)

#define RS4b(a_,b_)						\
  do {								\
  if ((a_)->kind == BF_AND && (b_)->kind == BF_AND &&           \
      (a_)->operands[0] == (b_)->operands[1]) \
    {								\
      bf_formula *bf = bf_crt_and (bm, (a_), bf_ref ((b_)->operands[0])); \
      bf_unref (b_); \
      return bf; \
    }								\
  } while (0)

#define RS4c(a_,b_)						\
  do {								\
  if ((a_)->kind == BF_AND && (b_)->kind == BF_AND &&           \
      (a_)->operands[1] == (b_)->operands[0]) \
    {								\
      bf_formula *bf = bf_crt_and (bm, (a_), bf_ref ((b_)->operands[1])); \
      bf_unref (b_); \
      return bf; \
    }								\
  } while (0)

#define RS4d(a_,b_)						\
  do {								\
  if ((a_)->kind == BF_AND && (b_)->kind == BF_AND &&           \
      (a_)->operands[1] == (b_)->operands[1]) \
    {								\
      bf_formula *bf = bf_crt_and (bm, (a_), bf_ref ((b_)->operands[0])); \
      bf_unref (b_); \
      return bf; \
    }								\
  } while (0)

#define RS4(a_,b_) \
 do { RS4a(a_,b_); RS4b(a_,b_); RS4c(a_,b_); RS4d(a_,b_); } while (0)

/* AND(AND(a,b),AND(-a,c)) => 0 */
#define RS5(a_,b_)						\
  do {								\
  if ((a_)->kind == BF_AND && (b_)->kind == BF_AND &&           \
      (s_are_opposite ((a_)->operands[0], (b_)->operands[0]) ||	\
       s_are_opposite ((a_)->operands[0], (b_)->operands[1]) || \
       s_are_opposite ((a_)->operands[1], (b_)->operands[0]) ||	 \
       s_are_opposite ((a_)->operands[1], (b_)->operands[1])))	 \
    {								\
      bf_unref (a_); bf_unref (b_); \
      return bf_crt_false (bm); \
    }								\
  } while (0)

/* AND(-AND(a,b),-AND(-a,b)) => -b */
#define RS6a(a_,b_)						\
  do {								\
    if ((a_)->kind == BF_NOT && (b_)->kind == BF_NOT &&		\
	(a_)->operands[0]->kind == BF_AND &&			\
	(b_)->operands[0]->kind == BF_AND &&			\
	(s_are_opposite ((a_)->operands[0]->operands[0],	\
			 (b_)->operands[0]->operands[0]) &&		\
	 (a_)->operands[0]->operands[1] == (b_)->operands[0]->operands[1])) \
      {									\
	bf_formula *r =							\
	  bf_crt_not (bm, bf_ref ((b_)->operands[0]->operands[1]));	\
	bf_unref (a_); bf_unref (b_);					\
	return r;							\
      }									\
  } while (0)

/* AND(-AND(a,b),-AND(b,-a)) => -b */
#define RS6b(a_,b_)						\
  do {								\
    if ((a_)->kind == BF_NOT && (b_)->kind == BF_NOT &&		\
	(a_)->operands[0]->kind == BF_AND &&			\
	(b_)->operands[0]->kind == BF_AND &&			\
	(s_are_opposite ((a_)->operands[0]->operands[0],	\
			 (b_)->operands[0]->operands[1]) &&		\
	 (a_)->operands[0]->operands[1] == (b_)->operands[0]->operands[0])) \
      {									\
	bf_formula *r =							\
	  bf_crt_not (bm, bf_ref ((b_)->operands[0]->operands[0]));	\
	bf_unref (a_); bf_unref (b_);					\
	return r;							\
      }									\
  } while (0)

#define RS6(a_,b_) do { RS6a(a_,b_); RS6b(a_,b_); } while (0)

/* AND(-AND(a,b),AND(-a,d)) => AND(-a,d) */
#define RS7(a_,b_) \
  do {								\
    if ((a_)->kind == BF_NOT && (a_)->operands[0]->kind == BF_AND && \
	(b_)->kind == BF_AND &&			\
	(s_are_opposite ((a_)->operands[0]->operands[0], (b_)->operands[0])|| \
	 s_are_opposite ((a_)->operands[0]->operands[0], (b_)->operands[1])|| \
	 s_are_opposite ((a_)->operands[0]->operands[1], (b_)->operands[0])|| \
	 s_are_opposite ((a_)->operands[0]->operands[1], (b_)->operands[1]))) \
      {									\
	bf_unref (a_);							\
        return (b_);							\
      }									\
  } while (0)

/* AND(-AND(a,b),AND(b,d)) => AND(-a,AND(b,d))- */
#define RS8a(a_,b_) \
  do {								\
    if ((a_)->kind == BF_NOT && (a_)->operands[0]->kind == BF_AND && \
	(b_)->kind == BF_AND &&			\
	((a_)->operands[0]->operands[1] == (b_)->operands[0] || \
         (a_)->operands[0]->operands[1] == (b_)->operands[1])) \
      {									\
	bf_formula *tmp = \
	  bf_crt_not (bm, bf_ref ((a_)->operands[0]->operands[0])); \
	bf_unref (a_);							\
	a_ = tmp;							\
        return bf_crt_and (bm, a_, b_);					\
      }									\
  } while (0)

#define RS8b(a_,b_) \
  do {								\
    if ((a_)->kind == BF_NOT && (a_)->operands[0]->kind == BF_AND && \
	(b_)->kind == BF_AND &&			\
	((a_)->operands[0]->operands[0] == (b_)->operands[0] || \
         (a_)->operands[0]->operands[0] == (b_)->operands[1])) \
      {									\
	bf_formula *tmp = \
	  bf_crt_not (bm, bf_ref ((a_)->operands[0]->operands[1])); \
	bf_unref (a_);							\
	a_ = tmp;							\
        return bf_crt_and (bm, a_, b_);					\
      }									\
  } while (0)

#define RS8(a_,b_) do { RS8a(a_,b_); RS8b(a_,b_); } while (0)

/* AND(-AND(a,b),a) => AND(a,-b) */
#define RS9a(x_,a_) \
do {								\
    if ((x_)->kind == BF_NOT && (x_)->operands[0]->kind == BF_AND && \
	(x_)->operands[0]->operands[0] == (a_))			\
      {									\
        bf_formula *b = bf_ref ((x_)->operands[0]->operands[1]);	\
        bf_formula *tmp = bf_crt_and (bm, a_, bf_crt_not (bm, b)); \
	bf_unref (x_);							\
        return tmp; \
      }									\
  } while (0)

#define RS9b(x_,a_) \
do {								\
    if ((x_)->kind == BF_NOT && (x_)->operands[0]->kind == BF_AND && \
	(x_)->operands[0]->operands[1] == (a_))			\
      {									\
        bf_formula *b = bf_ref ((x_)->operands[0]->operands[0]);	\
        bf_formula *tmp = bf_crt_and (bm, a_, bf_crt_not (bm, b)); \
	bf_unref (x_);							\
        return tmp; \
      }									\
  } while (0)

#define RS9(a_,b_) do { RS9a(a_,b_); RS9b(a_,b_); } while (0)

/* AND(-AND(a,b),-a) => -AND(a,b) */
#define RS10(a_,b_)						     \
    do {							     \
      if ((a_)->kind == BF_NOT && (a_)->operands[0]->kind == BF_AND &&	\
	  (s_are_opposite ((a_)->operands[0]->operands[0], (b_)) || \
           s_are_opposite ((a_)->operands[0]->operands[1], (b_)))) \
	{							   \
	  bf_unref (b_);						\
	  return a_;							\
	}								\
    } while (0)

bf_formula *
bf_crt_and (bf_manager *bm, bf_formula *f1, bf_formula *f2) 
{
  if (f1 == f2)
    {
      bf_unref (f2);
      return f1;
    }

  if (s_are_opposite (f1, f2))
    {
      bf_unref (f1);
      bf_unref (f2);
      return bf_crt_false (bm);
    }

  if (IS_T (f1))
    {
      bf_unref (f1);
      return f2;
    }
  if (IS_T (f2))
    {
      bf_unref (f2);
      return f1;
    }
  if (IS_F (f1) || IS_F (f2))
    { 
      bf_unref (f1); 
      bf_unref (f2); 
      return bf_crt_false (bm);
    }
  if (ENABLE_REWRITING_RULES)
    {
      RS1(f1,f2); RS1(f2,f1);
      RS2(f1,f2); RS2(f2,f1);
      RS3(f1,f2); RS3(f2,f1);
      RS4(f1,f2); RS4(f2,f1);
      RS5(f1,f2); RS5(f2,f1);
      RS6(f1,f2); RS6(f2,f1);
      RS7(f1,f2); RS7(f2,f1);
      RS8(f1,f2); RS8(f2,f1);
      RS9(f1,f2); RS9(f2,f1);
      RS10(f1,f2); RS10(f2,f1);
    }
  return s_alloc_formula (bm, BF_AND, 0, f1, f2);
}

bf_formula *
bf_crt_or (bf_manager *bm, bf_formula *f1, bf_formula *f2)
{
  if (IS_F (f1))
    { 
      bf_unref (f1); 
      return f2; 
    }
  if (IS_F (f2))
    {
      bf_unref (f2);
      return f1;
    }
  if (IS_T (f1) || IS_T (f2))
    {
      bf_unref (f1); 
      bf_unref (f2);
      return bf_crt_true (bm);
    }

  return bf_crt_not (bm, bf_crt_and (bm, 
				     bf_crt_not (bm, f1), 
				     bf_crt_not (bm, f2)));
}

bf_formula *
bf_crt_not (bf_manager *bm, bf_formula *f)
{
  bf_formula *result;
  if (f->kind == BF_CST) 
    {
      if (f->index)
	result = bf_crt_false (bm);
      else
	result = bf_crt_true (bm);
      bf_unref (f);
    }
  else if (f->kind == BF_NOT)
    {
      result = bf_ref (f->operands[0]);
      bf_unref (f);
    }
  else 
    {
      result = s_alloc_formula (bm, BF_NOT, 0, f, NULL);
    }
  return result;
}

bf_formula *
bf_crt_variable (bf_manager *bm, const char *comment)
{
  bf_index id = bm->fia (bm->fia_data);
  bf_formula *result = s_alloc_formula (bm, BF_VAR, id, NULL, NULL);
  
  if (comment)
    result->comment = ccl_string_dup (comment);

  return result;
}

bf_formula *
bf_crt_equiv (bf_manager *bm, bf_formula *f1, bf_formula *f2)
{
  bf_formula *a1 = bf_crt_and (bm, bf_ref (f1), bf_ref (f2));
  bf_formula *a2 = bf_crt_and (bm, 
			       bf_crt_not (bm, bf_ref (f1)), 
			       bf_crt_not (bm, bf_ref (f2)));
  bf_formula *result = bf_crt_or (bm, a1, a2);
  bf_unref (f1);
  bf_unref (f2);

  return (result);
}

bf_formula *
bf_crt_not_equiv (bf_manager *bm, bf_formula *f1, bf_formula *f2)
{
  bf_formula *a1 = bf_crt_or (bm, bf_ref (f1), bf_ref (f2));
  bf_formula *a2 = bf_crt_or (bm, 
			       bf_crt_not (bm, bf_ref (f1)), 
			       bf_crt_not (bm, bf_ref (f2)));
  bf_formula *result = bf_crt_and (bm, a1, a2);
  bf_unref (f1);
  bf_unref (f2);

  return (result);
}

bf_formula *
bf_crt_imply (bf_manager *bm, bf_formula *f1, bf_formula *f2)
{
  bf_formula *result = bf_crt_or (bm, bf_crt_not (bm, f1), f2);

  return (result);
}

bf_formula *
bf_crt_ite (bf_manager *bm, bf_formula *f1, bf_formula *f2, bf_formula *f3)
{
  bf_formula *result =
    bf_crt_or (bm, 
	       bf_crt_and (bm, bf_ref (f1), f2),
	       bf_crt_and (bm, bf_crt_not (bm, bf_ref (f1)), f3));
  bf_unref (f1);

  return result;
}

bf_formula *
bf_crt_at_least_one (bf_manager *bm, bf_formula **F, int nbF)
{
  int i;
  bf_formula *result;

  ccl_pre (nbF >= 1);

  /* at least one element of F is true */
  result = F[0];
  for (i = 1; i < nbF; i++)
    result = bf_crt_or (bm, result, F[i]);

  return result;
}

bf_formula *
bf_crt_at_most_one (bf_manager *bm, bf_formula **F, int nbF)
{
  int i;
  bf_formula *result = bf_crt_true (bm);

  ccl_pre (nbF >= 1);

  /* at least one element of F is true */
  for (i = 0; i < nbF - 1; i++)
    {
      int j;
      
      for (j = i + 1; j < nbF; j++)
	{
	  bf_formula *tmp = bf_crt_or (bm, 
				       bf_crt_not (bm, bf_ref (F[i])),
				       bf_crt_not (bm, bf_ref (F[j])));
	  result = bf_crt_and (bm, result, tmp);
	}
      bf_unref (F[i]);
    }
  bf_unref (F[i]);

  return result;
}

bf_formula *
bf_crt_one_among (bf_manager *bm, bf_formula **F, int nbF)
{
  int i;

  ccl_pre (nbF >= 1);
  for (i = 0; i < nbF; i++)
    bf_ref (F[i]);
  bf_formula *result = bf_crt_and (bm, 
				   bf_crt_at_least_one (bm, F, nbF),
				   bf_crt_at_most_one (bm, F, nbF));

  return result;
}

bf_formula *
bf_crt_xor (bf_manager *bm, bf_formula *a, bf_formula *b)
{
  return bf_crt_not (bm, bf_crt_equiv (bm, a, b));
}

bf_formula *
bf_crt_xor3 (bf_manager *bm, bf_formula *a, bf_formula *b, bf_formula *c)
{
  return bf_crt_xor (bm, a, bf_crt_xor (bm, b, c));
}

bf_formula *
bf_crt_carry (bf_manager *bm, bf_formula *a, bf_formula *b, bf_formula *c)
{
  bf_formula *result = 
    bf_crt_or (bm, 
	       bf_crt_and (bm, bf_ref (a), bf_ref (b)),
	       bf_crt_or (bm, 
			  bf_crt_and (bm, bf_ref (a), bf_ref (c)),
			  bf_crt_and (bm, bf_ref (b), bf_ref (c))));
  bf_unref (a);
  bf_unref (b);
  bf_unref (c);

  return result;
}

bf_formula *
bf_crt_half_adder (bf_manager *bm, bf_formula *a, bf_formula *b, 
		   bf_formula **p_carry)
{
  bf_formula *result = bf_crt_xor (bm, bf_ref (a), bf_ref (b));
  *p_carry = bf_crt_and (bm, bf_ref (a), bf_ref (b));
  bf_unref (a);
  bf_unref (b);

  return result;
}

bf_formula *
bf_crt_full_adder (bf_manager *bm, bf_formula *a, bf_formula *b, bf_formula *c, 
		   bf_formula **p_carry)
{
  bf_formula *result = bf_crt_xor3 (bm, bf_ref (a), bf_ref (b), bf_ref (c));
  *p_carry = bf_crt_carry (bm, bf_ref (a), bf_ref (b), bf_ref (c));
  bf_unref (a);
  bf_unref (b);
  bf_unref (c);

  return result;
}

void
bf_log_formula (ccl_log_type log, bf_manager *bm, bf_formula *f)
{
  if (f->kind == BF_VAR)
    {
      if (f->comment)
	ccl_log (log, "%s_%d", f->comment, f->index);
      else 
	ccl_log (log, "%d", f->index);
    }
  else if (f->kind == BF_CST)
    ccl_log (log, "%s", f->index ? "t" : "f");
  else
    {
      const char *op;

      if (f->kind == BF_OR) op = "or";
      else if (f->kind == BF_AND) op = "and";
      else op = "not";

      ccl_log (log, "(%s ", op);
      ccl_assert (f->operands[0] != f);
      bf_log_formula (log, bm, f->operands[0]);
      if (f->kind != BF_NOT) 
	{
	  ccl_log (log, " ");
	  bf_log_formula (log, bm, f->operands[1]);
	}
      ccl_log (log, ")");
    }
}

/* f = -(-(-a.-b).-(a.b)) */

static int
s_is_or (bf_formula *f, bf_formula **p_a, bf_formula **p_b)
{
  if (f->kind != BF_NOT || f->operands[0]->kind != BF_AND) return 0;
  f = f->operands[0];
  if (f->operands[0]->kind != BF_NOT || f->operands[1]->kind != BF_NOT)
    return 0;

  *p_a = f->operands[0]->operands[0];
  *p_b = f->operands[1]->operands[0];

  return 1;
}

static int
s_is_equiv (bf_formula *f, bf_formula **p_a, bf_formula **p_b)
{
  bf_formula *a;
  bf_formula *b;
  if (! s_is_or (f, &a, &b))
    return 0;

  if (a->kind != BF_AND  || b->kind != BF_AND)
    return 0;

  if ((s_are_opposite (a->operands[0], b->operands[0]) && 
       s_are_opposite (a->operands[1], b->operands[1])) ||
      (s_are_opposite (a->operands[0], b->operands[1]) && 
       s_are_opposite (a->operands[1], b->operands[0])))
    {
      *p_a = a->operands[0];
      *p_b = a->operands[1];
      return 1;
    }

  return 0;
}

static bf_literal
s_to_cnf (bf_manager *bm, bf_formula *f, 
	  bf_literal (*cache)(bf_formula *f, bf_literal l, void *cachedata), 
	  void *cachedata, 
	  void (*add_clause) (const bf_literal *variables, int nb_variables, 
			      void *data),
	  void *data)
{
  bf_literal C;
  bf_literal variables[3];

  
  if ((C = cache (f, BF_UNDEF_INDEX, cachedata)) != BF_UNDEF_INDEX)
    return C;

  if (f->kind == BF_VAR)
    C = bf_var2lit (f->index, 0);
  else
    {
      bf_literal A, B;
      if (! (f->kind == BF_NOT && f->operands[0]->kind == BF_VAR))
	C = bf_var2lit (s_alloc_index (bm), 0);

      switch (f->kind)
	{	  
	case BF_CST:
	  variables[0] = ((f->index == 0)? bf_negate(C) : C);
	  add_clause (variables, 1, data);
	  break;

	case BF_NOT: /* C = -A */	  
	  {
	    bf_formula *a;
	    bf_formula *b;
	    if (s_is_equiv (f, &a, &b))
	      {
		A = s_to_cnf (bm, a, cache, cachedata, add_clause, data);
		B = s_to_cnf (bm, b, cache, cachedata, add_clause, data);
	  
		/* (-A + -B + C) */
		variables[0] = bf_negate (A); 
		variables[1] = bf_negate (B);
		variables[2] = C;
		add_clause (variables, 3, data);

		/* (A + B + C) */
		variables[0] = A; 
		variables[1] = B;
		variables[2] = C;
		add_clause (variables, 3, data);

		/* (-A + B + -C) */
		variables[0] = bf_negate (A); 
		variables[1] = B;
		variables[2] = bf_negate (C);
		add_clause (variables, 3, data);

		/* (A + -B + -C) */
		variables[0] = A; 
		variables[1] = bf_negate (B);
		variables[2] = bf_negate (C);
		add_clause (variables, 3, data);

	      }
	    else if (f->operands[0]->kind == BF_VAR)
	      C = bf_var2lit (f->operands[0]->index, 1);
	    else
	      {
		A = s_to_cnf (bm, f->operands[0], cache, cachedata, add_clause,
			      data);
		
		/* (-A + -C) */
		variables[0] = bf_negate (A); 
		variables[1] = bf_negate (C);
		add_clause (variables, 2, data);
		/* (A + C) */
		variables[0] = A; 
		variables[1] = C;
		add_clause (variables, 2, data);
	      }
	    }
	  break;

	case BF_OR:
	  A = s_to_cnf (bm, f->operands[0], cache, cachedata, add_clause, data);
	  B = s_to_cnf (bm, f->operands[1], cache, cachedata, add_clause, data);
	  
	  /* (A + B + -C) */
	  variables[0] = A; 
	  variables[1] = B;
	  variables[2] = bf_negate (C);
	  add_clause (variables, 3, data);
	  /* (-A + C) */
	  variables[0] = bf_negate (A); 
	  variables[1] = C;	  
	  add_clause (variables, 2, data);
	  /* (-B + C) */
	  variables[0] = bf_negate (B); 
	  variables[1] = C;
	  add_clause (variables, 2, data);
	  break;

	default:
	  ccl_assert (f->kind == BF_AND);
	  A = s_to_cnf (bm, f->operands[0], cache, cachedata, add_clause, data);
	  B = s_to_cnf (bm, f->operands[1], cache, cachedata, add_clause, data);
	  
	  /* (-A + -B + C) */
	  variables[0] = bf_negate (A); 
	  variables[1] = bf_negate (B);
	  variables[2] = C;
	  add_clause (variables, 3, data);
	  /* (A + -C) */
	  variables[0] = A; 
	  variables[1] = bf_negate (C);	  
	  add_clause (variables, 2, data);
	  /* (B + -C) */
	  variables[0] = B; 
	  variables[1] = bf_negate (C);
	  add_clause (variables, 2, data);
	  break;
	}
    }
  ccl_assert (C != BF_UNDEF_INDEX);

  return cache (f, C, cachedata);
}

static int
s_is_lit (const bf_formula *f, bf_literal *p_l)
{
  int result;
  if (f->kind == BF_VAR)
    {
      *p_l = bf_var2lit (f->index, 0);
      result = 1;
    }
  else if (f->kind == BF_NOT)
    {
      result = s_is_lit (f->operands[0], p_l);
      if (result)
	*p_l = bf_negate (*p_l);
    }
  else
    {
      result = 0;
    }

  return result;
}

bf_literal 
bf_to_cnf_with_cache (bf_manager *bm, bf_formula *f, 
		      bf_literal (*cache)(bf_formula *f, bf_literal l, 
					  void *cachedata), 
		      void *cachedata, 
		      void (*add_clause) (const bf_literal *variables, 
					  int nb_variables, 
					  void *data),
		      void *data, int add_def)
{
  bf_literal res = BF_UNDEF_INDEX;
  bf_literal l[2];

  if (add_def)
    {
      if (bf_is_false (f))
	add_clause (NULL, 0, data);
      if (f->kind == BF_CST)
	return res;
    }
  ccl_pre (f->kind != BF_CST || ! add_def);

  if (add_def && f->kind == BF_AND)
    {
      ccl_list *todo = ccl_list_create ();
      ccl_list_add (todo, f->operands[0]);
      ccl_list_add (todo, f->operands[1]);
      
      while (! ccl_list_is_empty (todo))
	{
	  bf_formula *tf = ccl_list_take_first (todo);
	  if (tf->kind == BF_AND)
	    {
	      ccl_list_add (todo, tf->operands[0]);
	      ccl_list_add (todo, tf->operands[1]);
	    }
	  else
	    {
	      bf_to_cnf_with_cache (bm, tf, cache, cachedata, add_clause, 
				    data, 1);
	    }
	}
      ccl_list_delete (todo);
    }
  else if (add_def && f->kind == BF_OR && s_is_lit (f->operands[0], l) && 
	   s_is_lit (f->operands[1], l+1))
    {      
      add_clause (l, 2, data);
    }
  else
    {
      l[0] = s_to_cnf (bm, f, cache, cachedata, add_clause, data);
      if (add_def)
	add_clause (l, 1, data);
      else
	res = l[0];
    }

  ccl_assert (!add_def || res == BF_UNDEF_INDEX);

  return res;
}

static bf_literal 
s_cache (bf_formula *f, bf_literal l, void *cachedata)
{
  if (l == BF_UNDEF_INDEX)
    return f->cnf;
  return (f->cnf = l);
}

bf_literal 
bf_to_cnf (bf_manager *bm, bf_formula *f, 
	   void (*add_clause) (const bf_literal *variables, 
			       int nb_variables, 
			       void *data),
	   void *data, int add_def)
{
  return bf_to_cnf_with_cache (bm, f, s_cache, NULL, add_clause, data, add_def);
}

int 
bf_is_true (bf_formula *f)
{
  ccl_pre (f != NULL);
  return f->kind == BF_CST && f->index != 0;
}

int 
bf_is_false (bf_formula *f)
{
  ccl_pre (f != NULL);
  return f->kind == BF_CST && f->index == 0;
}

bf_index 
bf_get_var_index (bf_formula *f)
{
  ccl_pre (f->kind == BF_VAR);

  return f->index;
}

const char *
bf_get_var_comment (bf_formula *f)
{
  ccl_pre (f->kind == BF_VAR);

  return f->comment;
}

static void
s_get_size_rec (bf_formula *f, ccl_hash *C) 
{
  if (ccl_hash_find (C, f) || f->kind == BF_VAR)
    return;
  ccl_assert (f->kind != BF_CST);

  ccl_hash_insert (C, f);
  if (f->operands[0] != NULL)
    s_get_size_rec (f->operands[0], C);
  if (f->operands[1] != NULL)
    s_get_size_rec (f->operands[1], C);
}

int 
bf_get_size (bf_formula *f) 
{
  int result;
  ccl_hash *C = ccl_hash_create (NULL, NULL, NULL, NULL);

  ccl_pre (f != NULL);

  s_get_size_rec (f, C);
  result = ccl_hash_get_size (C);
  ccl_hash_delete (C);

  return result;
}

bf_manager * 
bf_get_man (bf_formula *f)
{
  ccl_pre (f != NULL);

  return f->man;
}

bf_formula *
bf_ref (bf_formula *f)
{  
  ccl_pre (f != NULL);
  f->refcount++;

  return f;
}

static void
s_delete_formula_iter (bf_formula *f)
{
  ccl_list *todo = ccl_list_create ();
  ccl_list_add (todo, f);
  while (! ccl_list_is_empty (todo))
    {
      f = ccl_list_take_first (todo);
      f->refcount--;
      if (f->refcount == 0)
	{
	  switch (f->kind)
	    {
	    case BF_CST: case BF_VAR: 
	      
	      break;
	    case BF_AND: case BF_OR:
	      ccl_list_add (todo, f->operands[1]);
	    case BF_NOT:
	      ccl_list_add (todo, f->operands[0]);
	    }
	  s_delete_formula (f);
	}
    }
  ccl_list_delete (todo);
}

void
bf_unref (bf_formula *f)
{
  ccl_pre (f != NULL);
  ccl_pre (f->refcount > 0);

  if (f->refcount == 1)
    {
      if (f->kind == BF_CST || f->kind == BF_VAR)
	{
	  f->refcount--;
	  s_delete_formula (f);
	}
      else
	{
	  s_delete_formula_iter (f);
	}
    }
  else
    f->refcount--;
}

static bf_index 
s_alloc_index (bf_manager *bm)
{
  return bm->fia (bm->fia_data);
}

static bf_formula *
s_alloc_formula (bf_manager *bm, bf_kind kind, int index, 
		 bf_formula *f1, bf_formula *f2)
{
  bf_formula *bf = &bm->tmp;  

  if (f1 != NULL && f2 != NULL)
    {
      if (f2 < f1)
	{
	  bf_formula *x = f1;
	  f1 = f2;
	  f2 = x;
	}
    }
  bf->refcount = 1;
  bf->index = index;
  bf->kind = kind;
  bf->operands[0] = f1;
  bf->operands[1] = f2;
  bf->comment = NULL;
  
  bf->size = 1;
  if (f1) bf->size += f1->size;
  if (f2) bf->size += f2->size;
  bf->man = bm;
  bf->cnf = BF_UNDEF_INDEX;

  if (ccl_hash_find (bm->ut, bf))
    {
      bf = bf_ref (ccl_hash_get (bm->ut));
      ccl_zdelete (bf_unref, f1);
      ccl_zdelete (bf_unref, f2);
    }
  else
    {
      bf = ccl_new (bf_formula);
      ccl_memcpy (bf, &bm->tmp, sizeof (bf_formula));
      ccl_hash_find (bm->ut, bf);
      ccl_hash_insert (bm->ut, bf);
    }

  return (bf);
}

static void
s_delete_formula (bf_formula *f)
{
  ccl_pre (f->refcount == 0);
  ccl_pre (ccl_hash_find (f->man->ut, f));
  ccl_hash_find (f->man->ut, f);
  ccl_hash_remove (f->man->ut);

  ccl_zdelete (ccl_string_delete, f->comment);
  ccl_memset (f, 0xFF, sizeof (*f));
  ccl_delete (f);
}

static void
s_check_formula (bf_formula *f)
{
  ccl_pre (f != NULL);

  switch (f->kind)
    {
    case BF_VAR: 
      break;
    case BF_CST: 
      ccl_assert (f->index == 0 || f->index == 1); break;
    case BF_OR: case BF_AND:
      s_check_formula (f->operands[1]);
    case BF_NOT:
      s_check_formula (f->operands[0]);
      break;
    default:
      abort ();
    }
}

static unsigned int 
s_hash_bf (const void *ptr)
{
  const bf_formula *bf = ptr;
  unsigned int result = 13 * bf->kind;

  result += 117 * bf->index;
  result <<= 7;
  result += 917 * bf->size;
  result += 12345 * (uintptr_t) bf->operands[0];
  result <<= 7;
  result += 98765 * (uintptr_t) bf->operands[1];

  return result;
}

static int 
s_compare_bf (const void *ptr1, const void *ptr2)
{
  const bf_formula *bf1 = ptr1;
  const bf_formula *bf2 = ptr2;
  int result = (int) bf1->kind - (int) bf2->kind;

  if (result == 0)
    result = bf1->index - bf2->index;

  if (result == 0 && bf1->kind != BF_VAR && bf1->kind != BF_CST)
    {
      result = (intptr_t) bf1->operands[0] - (intptr_t) bf2->operands[0];
      if (result == 0)
	result = (intptr_t) bf1->operands[1] - (intptr_t) bf2->operands[1];
    }
  
  return result;
}

static ccl_hash *
s_make_sharing_table (void)
{
  ccl_hash *result = ccl_hash_create (s_hash_bf, s_compare_bf, NULL, NULL);

  return result;
}
