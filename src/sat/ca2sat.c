/*
 * ca2sat.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include "bitblaster.h"
#include "ca2sat.h"

struct ca2sat_translator_st
{
  ar_ca *ca;
  ar_ca_exprman *eman;
  bitblaster *expr2sat;
  ar_ca_expr *domain_constraint;
};

			/* --------------- */

static bf_formula *
s_translate_post_relation (ca2sat_translator *c2s, 
			   varset *vs, ar_ca_expr *e1, 
			   varset *vsp, ar_ca_expr *e2);

static ar_ca_expr *
s_build_domain_constraint (ca2sat_translator *c2s);

			/* --------------- */

ca2sat_translator *
ca2sat_translator_create (ar_ca *ca, bf_fresh_index_allocator *fia, 
			  void *fiadata)
{
  ca2sat_translator *result = ccl_new (ca2sat_translator);
  
  result->ca = ar_ca_add_reference (ca);
  result->eman = ar_ca_get_expression_manager (ca);
  result->expr2sat = bitblaster_create (result->eman, fia, fiadata);
  result->domain_constraint = s_build_domain_constraint (result);

  return result;
}

void
ca2sat_translator_destroy (ca2sat_translator *c2s)
{
  ar_ca_expr_del_reference (c2s->domain_constraint);
  bitblaster_delete (c2s->expr2sat);
  ar_ca_exprman_del_reference (c2s->eman);
  ar_ca_del_reference (c2s->ca);
  ccl_delete (c2s);
}

bf_manager * 
ca2sat_get_bf_manager (ca2sat_translator *c2s)
{
  ccl_pre (c2s != NULL);

  return bitblaster_get_bf_manager (c2s->expr2sat);
}

varset * 
ca2sat_get_varset (ca2sat_translator *c2s)
{
  ccl_pre (c2s != NULL);

  return varset_create ();
}

bf_formula *
ca2sat_get_domain_constraint (ca2sat_translator *c2s, varset *vs)
{
  bf_formula *result;

  ccl_pre (c2s != NULL);
  ccl_pre (vs != NULL);

  result = ca2sat_translate_boolean_expr (c2s, vs, c2s->domain_constraint);

  return result;
}

bf_formula *
ca2sat_get_init (ca2sat_translator *c2s, varset *vs, int with_assertions)
{
  bf_formula *result;
  const ccl_pair *p;
  const ccl_list *init;
  bf_manager *bm = ca2sat_get_bf_manager (c2s);

  ccl_pre (c2s != NULL);
  ccl_pre (vs != NULL);

  init = ar_ca_get_initial_assignments (c2s->ca);
  result = bf_crt_true (bm);

  for (p = FIRST (init); p; p = CDDR (p))
    {
      ar_ca_expr *v = CAR (p);
      ar_ca_expr *val = CADR (p);
      ar_ca_expr *tmp = ar_ca_expr_crt_eq (v, val);
      bf_formula *a = ca2sat_translate_boolean_expr (c2s, vs, tmp);
      ar_ca_expr_del_reference (tmp);
      result = bf_crt_and (bm, result, a);
    }

  init = ar_ca_get_initial_constraints (c2s->ca);

  for (p = FIRST (init); p; p = CDR (p))
    {
      bf_formula *a = ca2sat_translate_boolean_expr (c2s, vs, CAR (p));
      result = bf_crt_and (bm, result, a);
    }
  
  if (with_assertions)
    result = bf_crt_and (bm, result, ca2sat_get_assertions (c2s, vs));

  return result;
}

bf_formula *
ca2sat_get_assertions (ca2sat_translator *c2s, varset *vs)
{
  bf_formula *result;
  const ccl_pair *p;
  const ccl_list *assertions;
  bf_manager *bm = ca2sat_get_bf_manager (c2s);

  ccl_pre (c2s != NULL);
  ccl_pre (vs != NULL);

  assertions = ar_ca_get_assertions (c2s->ca);
  result = ca2sat_get_domain_constraint (c2s, vs);

  for (p = FIRST (assertions); p; p = CDR (p))
    {
      bf_formula *a = ca2sat_translate_boolean_expr (c2s, vs, CAR (p));
      result = bf_crt_and (bm, result, a);
      assertions++;
    }			   

  return result;
}

bf_formula *
ca2sat_translate_boolean_expr (ca2sat_translator *c2s, varset *vs, 
			       ar_ca_expr *e)
{
  return bitblaster_translate_boolean_expr (c2s->expr2sat, vs, e);
}

			/* --------------- */

static bf_formula *
s_translate_transition (ca2sat_translator *c2s, ar_ca_trans *t, 
			varset *vs, varset *vsp, int with_id)
{
  int i;
  ccl_hash *donevars = 
    with_id ? ccl_hash_create (NULL, NULL, NULL, NULL) : NULL;
  ar_ca_expr *G = ar_ca_trans_get_guard (t);
  bf_formula *result = ca2sat_translate_boolean_expr (c2s, vs, G);
  int nb_ass = ar_ca_trans_get_number_of_assignments (t);
  ar_ca_expr **a = ar_ca_trans_get_assignments (t);
  bf_manager *bm = ca2sat_get_bf_manager (c2s);

  ar_ca_expr_del_reference (G);

  for (i = 0; i < nb_ass; i++, a += 2)
    {
      ar_ca_expr *v = a[0];
      ar_ca_expr *val = a[1];
      bf_formula *rel = s_translate_post_relation (c2s, vs, val, vsp, v);
      result = bf_crt_and (bm, result, rel);
      if (with_id)
	{
	  ccl_assert (!ccl_hash_find (donevars, v));
	  ccl_hash_find (donevars, v);
	  ccl_hash_insert (donevars, v);      
	}
    }

  if (with_id)
    {
      const ccl_list *sv = ar_ca_get_state_variables (c2s->ca);
      const ccl_pair *p = FIRST (sv);
      
      while (p != NULL)
	{
	  int k;
	  bf_formula *tmpr = bf_crt_true (bm);

	  /* in order to increase sharing ratio, equalities are packed 
	   * by conjunction of N members.
	   */
	  for (k = 0; k < 96 && p != NULL; k++)
	    {
	      ar_ca_expr *v = CAR (p);
      
	      if (! ccl_hash_find (donevars, v))
		{
		  bf_formula *rel = 
		    s_translate_post_relation (c2s, vs, v, vsp, v);
		  tmpr = bf_crt_and (bm, tmpr, rel);
		}	  
	    }
	  result = bf_crt_and (bm, result, tmpr);
	}
      ccl_hash_delete (donevars);
    }


  return result;  
}

bf_formula *
ca2sat_translate_transition (ca2sat_translator *c2s, ar_ca_trans *t,
			     varset *vs, varset *vsp,
			     int with_identity)
{
  return s_translate_transition (c2s, t, vs, vsp, with_identity);
}

static ccl_hash *
s_var_trans_dependencies (ar_ca_trans **trans, int nb_t)
{
  ccl_hash *result = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *) ccl_list_delete);
  intptr_t t;

  for (t = 0; t < nb_t; t++)
    {
      int i;
      int nb_ass = ar_ca_trans_get_number_of_assignments (trans[t]);
      ar_ca_expr **a = ar_ca_trans_get_assignments (trans[t]);

      for (i = 0; i < nb_ass; i++, a += 2)
	{
	  ccl_list *tlist;	   
	  ar_ca_expr *v = a[0];
	  if (ccl_hash_find (result, v))
	    tlist = ccl_hash_get (result);
	  else
	    {
	      tlist = ccl_list_create ();
	      ccl_hash_insert (result, tlist);
	    }
	  ccl_list_add (tlist, (void *) t);
	}
    }

  return result;
}

bf_formula *
ca2sat_translate_transitions (ca2sat_translator *c2s, ar_ca_trans **trans,
			      int nb_t, varset *vs, varset *vsp,
			      int with_post_cond,
			      bf_formula **bt_vars)
{
  int i;
  bf_manager *bm = ca2sat_get_bf_manager (c2s);
  bf_formula *result = bf_crt_true (bm);
  bf_formula **bt;

  if (bt_vars != NULL)
    bt = bt_vars;
  else
    bt = ccl_new_array (bf_formula *, nb_t);

  /* generate /\ bt[i] <=> [[t_i]] */
  for (i = 0; i < nb_t; i++)
    {
      bf_formula *bf = s_translate_transition (c2s, trans[i], vs, vsp, 0);
      bt[i] = bf_crt_variable (bm, NULL);
      //bf = bf_crt_equiv (bm, bf_ref (bt[i]), bf);
      bf = bf_crt_imply (bm, bf_ref (bt[i]), bf);
      result = bf_crt_and (bm, result, bf);
    }

#if 1
  ccl_hash *vtdeps = s_var_trans_dependencies (trans, nb_t);
  const ccl_list *sv = ar_ca_get_state_variables (c2s->ca);
  const ccl_pair *p;

  CCL_LIST_FOREACH (p, sv)
    {
      ar_ca_expr *v = CAR (p);
      bf_formula *rel = 
	s_translate_post_relation (c2s, vs, v, vsp, v);

      if (ccl_hash_find (vtdeps, v))
	{
	  ccl_list *deps = ccl_hash_get (vtdeps);
	  ccl_pair *p = FIRST (deps);
	  int ti = (intptr_t) CAR (p);

	  ccl_assert (0 <= ti && ti < nb_t);
	  bf_formula *tmp = bf_ref (bt[ti]);

	  for (p = CDR (p); p; p = CDR (p))
	    {
	      int ti = (intptr_t) CAR (p);
	      ccl_assert (0 <= ti && ti < nb_t);
	      tmp = bf_crt_or (bm, tmp, bf_ref (bt[ti]));
	    }
	  rel = bf_crt_or (bm, rel, tmp);
	}
      result = bf_crt_and (bm, result, rel);
    }
#endif
  /* generate constraint that only one transition is enabled \/ bt[i] */

  bf_formula *tmp;
  for (i = 0; i < nb_t ; i++)
    bf_ref (bt[i]);
  //  tmp = bf_crt_at_least_one (bm, bt, nb_t);
  tmp = bf_crt_one_among (bm, bt, nb_t);
  result = bf_crt_and (bm, result, tmp);

  if (with_post_cond)
    result = bf_crt_and (bm, result, 
			 ca2sat_get_assertions (c2s, vsp));
  
  ccl_hash_delete (vtdeps);

  if (bt_vars == NULL)
    {
      for (i = 0; i < nb_t; i++)
	bf_unref (bt[i]);
      ccl_delete (bt);
    }

  return result;
}

bf_formula *
ca2sat_translate_post_relation (ca2sat_translator *c2s, 
				varset *vs, ar_ca_expr *e1,
				varset *vsp, ar_ca_expr *e2)
{
  return s_translate_post_relation (c2s, vs, e1, vsp, e2);
}

bf_formula *
ca2sat_translate_distinct_state (ca2sat_translator *c2s, 
				 varset *V0, varset *V1)
{
  bf_manager *bm = ca2sat_get_bf_manager (c2s);
  bf_formula *result = bf_crt_true (bm);
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_flow_variables (c2s->ca);
  int k = 2;

  while (k--)
    {
      CCL_LIST_FOREACH (p, vars)
	{	  
	  ar_ca_expr *v = CAR (p);
	  bf_formula *tmp0 = 
	    ca2sat_translate_post_relation (c2s, V0, v, V1, v); 
	  bf_formula * tmp1 = bf_crt_and (bm, result, tmp0);
	  result = tmp1;
	}
      vars = ar_ca_get_state_variables (c2s->ca);
    }

  result = bf_crt_not (bm, result);

  return result;
}

void
ca2sat_display_solution (ca2sat_translator *c2s, const char *model)
{
  abort();
}

static bf_formula *
s_translate_post_relation (ca2sat_translator *c2s, 
			   varset *vs, ar_ca_expr *e1,
			   varset *vsp, ar_ca_expr *e2)
{  
  return bitblaster_translate_equality (c2s->expr2sat, vs, e1, vsp, e2);
}

static ar_ca_expr *
s_build_domain_constraint (ca2sat_translator *c2s)
{
  int k = 2;
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_flow_variables (c2s->ca);
  ar_ca_expr *result = ar_ca_expr_crt_boolean_constant (c2s->eman, 1);

  while (k--)
    {
      for (p = FIRST (vars); p; p = CDR (p))
	{
	  if (! ar_ca_expr_is_boolean_expr (CAR (p)))
	    {
	      const ar_ca_domain *dom = 
		ar_ca_expr_variable_get_domain (CAR (p));
	      ar_ca_expr *C = ar_ca_expr_crt_in_domain (CAR (p), dom);
	      ar_ca_expr *tmp = ar_ca_expr_crt_and (result, C);
	      ar_ca_expr_del_reference (C);
	      ar_ca_expr_del_reference (result);
	      result = tmp;
	    }
	}
      vars = ar_ca_get_state_variables (c2s->ca);
    }

  return result;
}

