/*
 * bitvector.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef BITVECTOR_H
# define BITVECTOR_H

# include <ccl/ccl-array.h>
# include <sat/boolean-formula.h>

typedef CCL_ARRAY(bf_formula *) bitvector;

extern bitvector *
bitvector_create (void);

extern bitvector *
bitvector_clone (const bitvector *bv);

extern void
bitvector_copy (bitvector *dst, const bitvector *src);

extern void
bitvector_delete (bitvector *bv);

extern void
bitvector_clear (bitvector *bv);

extern int 
bitvector_get_size (const bitvector *bv);

extern bf_formula *
bitvector_get_bit (const bitvector *bv, int index);

extern bf_formula *
bitvector_get_extbit (const bitvector *bv, int index);

extern bf_formula *
bitvector_get_sign (const bitvector *bv);

extern void
bitvector_set_bit (const bitvector *bv, int index, bf_formula *bf);

extern void
bitvector_add (bitvector *bv, bf_formula *bf);

extern void
bitvector_log (ccl_log_type log, bf_manager *bm, bitvector *bv);

extern int
bitvector_is_true (const bitvector *bv);

extern int
bitvector_is_false (const bitvector *bv);

extern int
bitvector_is_equal_to (const bitvector *bv, const bitvector *other);

extern int
bitvector_is_constant (const bitvector *bv);

extern int
bitvector_to_integer (const bitvector *bv);

#endif /* ! BITVECTOR_H */
