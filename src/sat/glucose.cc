/*
 * glucose.cc -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-log.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include <cstdlib>
#include "glucose.h"
#include <core/Solver.h>

using namespace Glucose;

struct glucose_solver_st {
  Solver gsolver;
  bf_index group;
  vec<Lit> tmpclause;


  glucose_solver_st () 
    : gsolver (), group (BF_UNDEF_INDEX), tmpclause(10) {
    // gsolver.certifiedOutput = stderr;
    // gsolver.certifiedUNSAT = true;
    // gsolver.setIncrementalMode ();
    
  }

  ~glucose_solver_st () { 
  }

  inline Lit mk_lit (bf_literal l) {
    return mkLit (bf_lit2var (l), bf_sign (l) ? 1 : 0);
  }
};

glucose_solver *
glucose_solver_create (void)
{
  return new glucose_solver;
}

void
glucose_solver_set_group (glucose_solver *S, bf_index gid)
{
  ccl_pre (S->group == BF_UNDEF_INDEX);
  S->group = gid;
}

void
glucose_solver_clear_group (glucose_solver *S)
{
  ccl_pre (S->group != BF_UNDEF_INDEX);

  glucose_solver_add_clause_0 (S);
  S->group = BF_UNDEF_INDEX;
  glucose_solver_gc (S);
}

void
glucose_solver_destroy (glucose_solver *S)
{
  delete S;
}

void
glucose_solver_gc (glucose_solver *S)
{
  int nc = S->gsolver.nClauses ();
  
  S->gsolver.simplify ();
  if (ccl_debug_is_on && 0)
    {
      ccl_debug ("remove %d clauses\n", nc - S->gsolver.nClauses ());
    }
}

bf_index
glucose_solver_new_var (glucose_solver *S)
{
  return S->gsolver.newVar ();
}

void
glucose_solver_add_clause_0 (glucose_solver *S)
{
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 0));
  S->gsolver.addClause (S->tmpclause);
}

void
glucose_solver_add_clause_1 (glucose_solver *S, bf_literal l)
{
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 0));
  S->tmpclause.push (S->mk_lit (l));
  S->gsolver.addClause (S->tmpclause);
}

void
glucose_solver_add_clause_2 (glucose_solver *S, bf_literal l1, bf_literal l2)
{
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 0));
  S->tmpclause.push (S->mk_lit (l1));
  S->tmpclause.push (S->mk_lit (l2));
  S->gsolver.addClause (S->tmpclause);
}

void
glucose_solver_add_clause_3 (glucose_solver *S, bf_literal l1, bf_literal l2, 
			     bf_literal l3)
{
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 0));
  S->tmpclause.push (S->mk_lit (l1));
  S->tmpclause.push (S->mk_lit (l2));
  S->tmpclause.push (S->mk_lit (l3));
  S->gsolver.addClause (S->tmpclause);
}

void
glucose_solver_add_clause (glucose_solver *S, const bf_literal *lits, 
			   int nb_lits)
{
  int i;

  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 0));
  for (i = 0; i < nb_lits; i++) 
    S->tmpclause.push (S->mk_lit (lits[i]));
  S->gsolver.addClause (S->tmpclause);
}

void
glucose_add_clause_to_solver (const bf_literal *lits, int nb_lits, void *data)
{
  glucose_solver *S = (glucose_solver *) data;

  if (nb_lits == 0)
    glucose_solver_add_clause_0 (S);
  else if (nb_lits == 1)
    glucose_solver_add_clause_1 (S, lits[0]);
  else if (nb_lits == 2)
    glucose_solver_add_clause_2 (S, lits[0], lits[1]);
  else if (nb_lits == 3)
    glucose_solver_add_clause_3 (S, lits[0], lits[1], lits[2]);
  else 
    glucose_solver_add_clause (S, lits, nb_lits);  

  if (S->gsolver.nClauses () && S->gsolver.nClauses () % 1000000 == 0)
    ccl_debug ("%d Mclauses\n", S->gsolver.nClauses () / 1000000);
}

int
glucose_solver_solve (glucose_solver *S)
{  
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 1));
  return S->gsolver.solve (S->tmpclause);
}

int
glucose_solver_solve_under_assumptions_1 (glucose_solver *S, bf_literal l)
{
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 1));
  S->tmpclause.push (S->mk_lit (l));
  return S->gsolver.solve (S->tmpclause);
}

int
glucose_solver_solve_under_assumptions_2 (glucose_solver *S, bf_literal l1, 
					  bf_literal l2)
{
  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 1));
  S->tmpclause.push (S->mk_lit (l1));
  S->tmpclause.push (S->mk_lit (l2));
  return S->gsolver.solve (S->tmpclause);
}

int
glucose_solver_solve_under_assumptions (glucose_solver *S, ccl_list *lit)
{
  ccl_pair *p;

  S->tmpclause.clear ();
  if (S->group != BF_UNDEF_INDEX)
    S->tmpclause.push (mkLit (S->group, 1));
  for (p = FIRST (lit); p; p = CDR (p))
    S->tmpclause.push (S->mk_lit ((intptr_t) CAR (p)));
  return S->gsolver.solve (S->tmpclause);
}

char *
glucose_solver_get_model (glucose_solver *S, int *p_msize)
{  
  bf_index sz = S->gsolver.model.size ();
  char *result = ccl_new_array (char, sz + 1);

  ccl_assert (S->gsolver.okay ());

  for (bf_index i = 0; i < sz; i++) {
    result[i] = (S->gsolver.model[i] == l_True) ? 1 : 0;
  }

  *p_msize = sz;

  return result;
}

bf_literal *
glucose_solver_get_conflict (glucose_solver *S, int *p_csize)
{
  int csize = S->gsolver.conflict.size ();
  bf_literal *result = ccl_new_array (bf_literal, csize);
  int i;
  
  for (i = 0; i < csize; i++)
    {
      Lit l = S->gsolver.conflict[i];
      result[i] =  bf_var2lit (var(l), sign(l) ? 1 : 0);
    }

  if (p_csize != NULL)
    *p_csize = csize;

  return result;
}

int 
glucose_solver_get_nb_clauses (glucose_solver *S)
{
  return S->gsolver.nClauses ();
}

int 
glucose_solver_get_nb_learnts (glucose_solver *S)
{
  return S->gsolver.nLearnts ();
}

int
glucose_solver_is_okay (glucose_solver *S)
{
  return S->gsolver.okay ();
}
