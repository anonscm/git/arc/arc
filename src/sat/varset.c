/*
 * varset.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-util.h"
#include "boolean-formula.h"
#include "varset.h"


struct varset_st
{
  int refcount;
  ccl_hash *a2b;
};

#define m_nb_bits_for_symbols(eman) \
  (ar_bit_size (ar_ca_expr_get_number_of_enum_constants (eman) - 1) + 1)


varset *
varset_create (void)
{
  varset *result = ccl_new (varset);

  result->refcount = 1;
  result->a2b = ccl_hash_create (NULL, NULL, 
				 (ccl_delete_proc *) ar_ca_expr_del_reference,
				 (ccl_delete_proc *) bitvector_delete);

  return result;
}

			/* --------------- */

void
varset_unref (varset *vs)
{
  ccl_pre (vs != NULL);
  ccl_pre (vs->refcount > 0);

  vs->refcount--;
  if (vs->refcount == 0)
    {
      ccl_hash_delete (vs->a2b);
      ccl_delete (vs);
    }
}

varset * 
varset_ref (varset *vs)
{
  ccl_pre (vs != NULL);
  ccl_pre (vs->refcount > 0);

  vs->refcount++;

  return vs;
}

static int 
s_compute_value (const bitvector *bv, const char *M, int Msize)
{
  int i;
  int sz = bitvector_get_size (bv);
  bitvector *value = bitvector_create ();
  int result;

  for (i = 0; i < sz; i++)
    {
      bf_formula *v = bitvector_get_bit (bv, i);
      bf_index bvi = bf_get_var_index (v);

      ccl_assert (bvi < (unsigned) Msize);
      if (M[bvi])
	v = bf_crt_true (bf_get_man (v));
      else
	v = bf_crt_false (bf_get_man (v));
      bitvector_add (value, v);
    }

  result = bitvector_to_integer (value);
  bitvector_delete (value);

  return result;
}

ar_ca_expr * 
varset_get_value_of (varset *V, ar_ca_expr *var, const char *M, int Msize)
{
  ar_ca_expr *result;
  int value = varset_get_value_as_int (V, var, M, Msize);
  ar_ca_exprman *eman = ar_ca_expr_get_expression_manager (var);

  if (ar_ca_expr_is_boolean_expr (var))
    {
      result = ar_ca_expr_crt_boolean_constant (eman, value ? 1 : 0);
    }
  else if (ar_ca_expr_is_enum_expr (var))
    {
#if CCL_ENABLE_ASSERTIONS
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
      ccl_assert (ar_ca_domain_has_element (dom, value));
#endif /* CCL_ENABLE_ASSERTIONS */

      result = ar_ca_expr_get_enum_constant_by_index (eman, value);
      ccl_assert (result != NULL);
    }
  else
    {
      result = ar_ca_expr_crt_integer_constant (eman, value);
    }
  ar_ca_exprman_del_reference (eman);

  return result;
}

			/* --------------- */

int
varset_get_value_as_int (varset *V, ar_ca_expr *var, const char *M, int Msize)
{
  const bitvector *bv;
  int result;

  ccl_pre (varset_cache_find (V, var));

  varset_cache_find (V, var);
  bv = varset_cache_get (V);
  result = s_compute_value (bv, M, Msize);
  if (ar_ca_expr_is_boolean_expr (var))
    result = result ? 1 : 0;

  return result;
}

			/* --------------- */

void
varset_dump (ccl_log_type log, varset *V)
{
  ccl_pointer_iterator *i = ccl_hash_get_keys (V->a2b);
  while (ccl_iterator_has_more_elements (i))
    {
      ar_ca_expr *e = ccl_iterator_next_element (i);
      if (ar_ca_expr_get_kind (e) == AR_CA_VAR)
	{
	  ar_ca_expr_log (log, e);
	  ccl_log (log, " @ %p\n", e);
	}
    }
  ccl_iterator_delete (i);
}

			/* --------------- */

int
varset_cache_find (varset *V, ar_ca_expr *e)
{
  ccl_pre (V != NULL);
  ccl_pre (e != NULL);

  return ccl_hash_find (V->a2b, e);
}

			/* --------------- */

const bitvector *
varset_cache_get (varset *V)
{
  ccl_pre (V != NULL);

  return ccl_hash_get (V->a2b);
}

			/* --------------- */

void
varset_cache_insert (varset *V, ar_ca_expr *e, bitvector *bits)
{
  ccl_pre (V != NULL);
  ccl_pre (e != NULL);
  ccl_pre (bits != NULL);

  ccl_pre (! varset_cache_find (V, e));

  ccl_hash_find (V->a2b, ar_ca_expr_add_reference (e));
  ccl_hash_insert (V->a2b, bits);
}

