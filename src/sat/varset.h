/*
 * varset.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef VARSET_H
# define VARSET_H

# include <sat/bitvector.h>
# include "ar-ca-expression.h"

typedef struct varset_st varset;

extern varset *
varset_create (void);

extern void
varset_unref (varset *vs);

extern varset *
varset_ref (varset *vs);

extern ar_ca_expr * 
varset_get_value_of (varset *vs, ar_ca_expr *var, const char *M, int Msize);

extern int
varset_get_value_as_int (varset *vs, ar_ca_expr *var, const char *M, int Msize);

extern void
varset_dump (ccl_log_type log, varset *V);

extern int
varset_cache_find (varset *vs, ar_ca_expr *e);

extern const bitvector *
varset_cache_get (varset *vs);

extern void
varset_cache_insert (varset *vs, ar_ca_expr *e, bitvector *bv);

#endif /* ! VARSET_H */
