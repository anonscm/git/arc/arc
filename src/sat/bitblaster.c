/*
 * bitblaster.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdlib.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-util.h"
#include "bitblaster.h"

struct bitblaster_st
{
  ar_ca_exprman *eman;
  bf_manager *bm;
  int nb_bits_for_symbols;
};


			/* --------------- */

#define m_nb_bits_for_symbols(eman) \
  (ar_bit_size (ar_ca_expr_get_number_of_enum_constants (eman) - 1) + 1)

			/* --------------- */

static int 
s_max_size (const bitvector *a, const bitvector *b);

			/* --------------- */

bitblaster * 
bitblaster_create (ar_ca_exprman *eman, bf_fresh_index_allocator *fia, 
		   void *fiadata)
{
  bitblaster *result = ccl_new (bitblaster);

  result->eman = ar_ca_exprman_add_reference (eman);
  result->bm = bf_crt_manager (fia, fiadata);
  result->nb_bits_for_symbols = m_nb_bits_for_symbols (result->eman);

  return result;
}

void 
bitblaster_delete (bitblaster *t)
{
  ccl_pre (t != NULL);

  ar_ca_exprman_del_reference (t->eman);
  bf_manager_destroy (t->bm);
  ccl_delete (t);
}

bf_manager *
bitblaster_get_bf_manager (bitblaster *t)
{
  ccl_pre (t != NULL);

  return t->bm;
}

bf_formula *
bitblaster_translate_boolean_expr (bitblaster *t, varset *V,
				  ar_ca_expr *e)
{
  const bitvector *bexprs = bitblaster_translate_expr (t, V, e);

  ccl_assert (bitvector_get_size (bexprs) == 1);

  return bf_ref (bitvector_get_bit (bexprs, 0));
}

			/* --------------- */

const bitvector * 
bitblaster_translate_expr (bitblaster *t, varset *V, ar_ca_expr *e)
{
  bitvector *result = NULL;
  ar_ca_expression_kind ekind;

  if (varset_cache_find (V, e))
    return varset_cache_get (V);

  ekind = ar_ca_expr_get_kind (e);

  if (ekind == AR_CA_CST)
    {
      int val = ar_ca_expr_get_min (e);
      int sz;

      if (ar_ca_expr_is_enum_expr (e))
	sz = t->nb_bits_for_symbols;
      else if (ar_ca_expr_is_boolean_expr (e))
	sz = 1;
      else if (val < 0)	
	sz = ar_bit_size (-val) + 1;
      else 
	sz = ar_bit_size (val) + 1;
      result = bitblaster_mk_constant (t, sz, val);
    }
  else if (ekind == AR_CA_VAR)
    {
      result = bitblaster_mk_expr_variable (t, V, e);
    }
  else if (ekind == AR_CA_EQ)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      bf_formula *rF = 
	bitblaster_translate_equality (t, V, args[0], V, args[1]);
      result = bitvector_create ();
      bitvector_add (result, rF);
    }
  else if (ekind == AR_CA_LT)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      const bitvector *a0 = bitblaster_translate_expr (t, V, args[0]);
      const bitvector *a1 = bitblaster_translate_expr (t, V, args[1]);
      result = bitblaster_mk_lt (t, a0, a1);      
    }
  else if (ar_ca_expr_is_boolean_expr (e))
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      bf_formula *bfargs[] = { NULL, NULL, NULL };
      bf_formula *rF;

      bfargs[0] = bitblaster_translate_boolean_expr (t, V, args[0]);
      if (ekind == AR_CA_NOT)
	rF = bf_crt_not (t->bm, bf_ref (bfargs[0]));
      else 
	{
	  ccl_assert (ekind == AR_CA_OR || ekind == AR_CA_AND || 
		      ekind == AR_CA_ITE);

	  bfargs[1] = bitblaster_translate_boolean_expr (t, V, args[1]);
	  if (ekind != AR_CA_OR)
	    rF = bf_crt_and (t->bm, bf_ref (bfargs[0]), bf_ref (bfargs[1]));

	  if (ekind == AR_CA_ITE)
	    {
	      bfargs[2] = bitblaster_translate_boolean_expr (t, V, args[2]);
	      rF = bf_crt_or (t->bm, rF, 
			      bf_crt_and (t->bm, 
					  bf_crt_not (t->bm, 
						      bf_ref (bfargs[0])), 
					  bf_ref (bfargs[2])));
	      bf_unref (bfargs[2]);
	    }
	  else if (ekind == AR_CA_OR)
	    {
	      rF = bf_crt_or (t->bm, bf_ref (bfargs[0]), bf_ref (bfargs[1]));
	    }
	  bf_unref (bfargs[1]);
	}
      bf_unref (bfargs[0]);
      result = bitvector_create ();
      bitvector_add (result, rF);
    }
  else 
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      const bitvector *bvargs[] = { NULL, NULL, NULL };

      bvargs[0] = bitblaster_translate_expr (t, V, args[0]);
      if (ekind == AR_CA_NEG)
	result =  bitblaster_mk_neg (t, bvargs[0]);
      else
	{
	  bvargs[1] = bitblaster_translate_expr (t, V, args[1]);
	  if (ekind == AR_CA_ITE)
	    {
	      bf_formula *cond = bitvector_get_bit (bvargs[0], 0);
	      bvargs[2] = bitblaster_translate_expr (t, V, args[2]);
	      if (ar_ca_expr_is_integer_expr (args[1]))
		result = bitblaster_mk_signed_multiplexer (t, bf_ref (cond), 
							   bvargs[1],
							   bvargs[2]);
	      else
		result = bitblaster_mk_multiplexer (t, bf_ref (cond), bvargs[1],
						    bvargs[2]);
	    }
	  else if (ekind == AR_CA_ADD)
	    result = bitblaster_mk_add (t, bvargs[0], bvargs[1]);
	  else if (ekind == AR_CA_MUL)
	    result = bitblaster_mk_mul (t, bvargs[0], bvargs[1]);
	  else if (ekind == AR_CA_DIV)
	    result = bitblaster_mk_div (t, bvargs[0], bvargs[1]);
	  else if (ekind == AR_CA_MOD)
	    result = bitblaster_mk_mod (t, bvargs[0], bvargs[1]);
	  else if (ekind == AR_CA_MIN)
	    result = bitblaster_mk_min (t, bvargs[0], bvargs[1]);
	  else 
	    {
	      ccl_assert (ekind == AR_CA_MAX);
	      result = bitblaster_mk_max (t, bvargs[0], bvargs[1]);
	    }
	}
    }

  ccl_post (result != NULL);

  varset_cache_insert (V, e, result);

  return result;
}

			/* --------------- */

bf_formula *
bitblaster_translate_equality (bitblaster *t, 
			       varset *V1, ar_ca_expr *e1,
			       varset *V2, ar_ca_expr *e2)
{
  bf_formula *rF;

  if ((ar_ca_expr_is_enum_expr (e1) && ar_ca_expr_get_kind (e1) == AR_CA_ITE) ||
      (ar_ca_expr_is_enum_expr (e2) && ar_ca_expr_get_kind (e2) == AR_CA_ITE))
    {
      ar_ca_expr **iteargs;

      if (!(ar_ca_expr_is_enum_expr (e1) && 
	    ar_ca_expr_get_kind (e1) == AR_CA_ITE))
	{ 
	  void *tmp = e1;
	  e1 = e2;
	  e2 = tmp;
	  tmp = V1;
	  V1 = V2;
	  V2 = tmp;
	}

      iteargs = ar_ca_expr_get_args (e1);
      bf_formula *cond = 
	bitblaster_translate_boolean_expr (t, V1, iteargs[0]);
      bf_formula *then_f = 
	bitblaster_translate_equality (t, V1, iteargs[1], V2, e2);
      bf_formula *else_f = 
	bitblaster_translate_equality (t, V1, iteargs[2], V2, e2);
      rF = bf_crt_ite (t->bm, cond, then_f, else_f);
    }
  else
    {
      int i;
      const bitvector *el0 = bitblaster_translate_expr (t, V1, e1);
      const bitvector *el1 = bitblaster_translate_expr (t, V2, e2);
      int sz = s_max_size (el0, el1);

      rF = bf_crt_equiv (t->bm, 
			 bf_ref (bitvector_get_bit (el0, 0)),
			 bf_ref (bitvector_get_bit (el1, 0)));
      if (ar_ca_expr_is_integer_expr (e1))
	{
	  for (i = 1; i < sz; i++)
	    rF = bf_crt_and (t->bm, rF, 
			     bf_crt_equiv (t->bm, 
					   bf_ref (bitvector_get_extbit (el0,
									 i)),
					   bf_ref (bitvector_get_extbit (el1,
									 i))));
	}
      else
	{
	  ccl_assert (bitvector_get_size (el0) == bitvector_get_size (el1));
      
	  for (i = 1; i < sz; i++)
	    rF = bf_crt_and (t->bm, rF, 
			     bf_crt_equiv (t->bm, 
					   bf_ref (bitvector_get_bit (el0, i)),
					   bf_ref (bitvector_get_bit (el1, i))));
	}
    }

  return rF;
}

			/* --------------- */

const bitvector * 
bitblaster_mk_expr (bitblaster *t, varset *V, ar_ca_expr *e)
{
  return bitblaster_translate_expr (t, V, e);
}

bitvector * 
bitblaster_mk_constant (bitblaster *t, int sz, int value)
{
  bf_formula *bit;
  int is_zero = 1;
  bitvector *result = bitvector_create ();

  if (sz == 1)
    {
      bit = (value&0x1) ? bf_crt_true(t->bm) : bf_crt_false(t->bm);
      bitvector_add (result, bit);
    }
  else
    {
      while (sz--)
	{      
	  if (sz == 0 && is_zero) 
	    value = 0;
	  
	  bit = (value&0x1) ? bf_crt_true(t->bm) : bf_crt_false(t->bm);
	  is_zero = is_zero && ((value&0x1) == 0);
	  bitvector_add (result, bit);
	  value >>= 1;
	}
    }
  
  return result;
}

bitvector *
bitblaster_mk_integer (bitblaster *t, int value)
{
  int sz = ar_bit_size (value >= 0 ? value : -value) + 1;

  return bitblaster_mk_constant (t, sz, value);
}

bitvector * 
bitblaster_mk_excluded_zero (bitblaster *t, int sz)
{
  bitvector *result = bitvector_create ();

  sz--;
  while (sz--)
    bitvector_add (result, bf_crt_false(t->bm));
  bitvector_add (result, bf_crt_true(t->bm));

  return result;
}

bitvector *
bitblaster_mk_variable (bitblaster *t, int sz, const char *namepattern)
{
  int i;
  bitvector *result = bitvector_create ();
  char *tmpname = NULL;

  for (i = 0; i < sz; i++)
    {      
      bf_formula *v;

      if (namepattern != NULL)
	tmpname = ccl_string_format_new (namepattern, i);
      v = bf_crt_variable (t->bm, tmpname);
      ccl_zdelete (ccl_string_delete, tmpname);
      bitvector_add (result, v);
    }
  return result;
}

bitvector * 
bitblaster_mk_expr_variable (bitblaster *t, varset *V, ar_ca_expr *v)
{
  const ar_identifier *vid;
  char *vname;
  bitvector *result;

  ccl_pre (ar_ca_expr_get_kind (v) == AR_CA_VAR);
  ccl_pre (!varset_cache_find (V, v));

  vid = ar_ca_expr_variable_get_const_name (v);
  vname = ar_identifier_to_string (vid);

  if (ar_ca_expr_is_boolean_expr (v))
    result = bitblaster_mk_variable (t, 1, vname);
  else
    {	      
      int nb_bits;
      char *pattern = ccl_string_format_new ("%s_%%d", vname);
      if (ar_ca_expr_is_enum_expr (v))
	{
	  nb_bits = t->nb_bits_for_symbols;
	}
      else
	{
	  int max;
	  ar_bounds range;
	  const ar_ca_domain *D = ar_ca_expr_variable_get_domain (v);
	  ar_ca_domain_get_bounds (D, &range);
	  max = abs (range.min);
	  if (max < abs (range.max))
	    max = range.max;
	  nb_bits = ar_bit_size (max) + 1;
	}
      result = bitblaster_mk_variable (t, nb_bits, vname);
      ccl_string_delete (pattern);
    }
  ccl_string_delete (vname);

  return result;
}

			/* --------------- */

bitvector * 
bitblaster_sign_extend (bitblaster *t, int newsize, const bitvector *a)
{
  int i;
  bf_formula *ba;
  int asz = bitvector_get_size (a);
  bitvector *result = bitvector_clone (a);

  ccl_pre (1 <= asz && asz <= newsize);

  ba = bitvector_get_bit (a, asz - 1);
  for (i = asz; i < newsize; i++)
    bitvector_add (result, bf_ref (ba));

  return result;
}

bitvector *
bitblaster_mk_neg (bitblaster *t, const bitvector *a)
{
  int i;  
  bf_formula *val;
  int sz = bitvector_get_size (a);
  bitvector *result = bitvector_create ();
  bf_formula *carry_in = bf_crt_true (t->bm);

  for (i = 0; i < sz - 1; i++)
    {
      bf_formula *ba = bitvector_get_bit (a, i);
      bf_formula *tmp = bf_crt_not (t->bm, bf_ref (ba));

      val = bf_crt_half_adder (t->bm, tmp, carry_in, &carry_in);
      bitvector_add (result, val);
    }

  val = bitvector_get_bit (a, i);
  val = bf_crt_not (t->bm, bf_ref (val));
  val = bf_crt_xor (t->bm, val, carry_in);
  bitvector_add (result, val);

  return result;
}

bitvector *
bitblaster_mk_add (bitblaster *t, const bitvector *a, const bitvector *b)
{
  int i;
  bf_formula *ba;
  bf_formula *bb;
  bf_formula *val;
  int sz = s_max_size (a, b) + 1;
  bitvector *result = bitvector_create ();
  bf_formula *carry_in = bf_crt_false (t->bm);

  for (i = 0; i < sz; i++)
    {
      ba = bitvector_get_extbit (a, i);
      bb = bitvector_get_extbit (b, i);
      val = bf_crt_full_adder (t->bm, bf_ref (ba), bf_ref (bb), carry_in, 
			       &carry_in);
      bitvector_add (result, val);
    }  
  bf_unref (carry_in);

  return result;
}

static bitvector *
s_mk_sub (bitblaster *t, const bitvector *a, const bitvector *b, 
	  bf_formula **p_carry)
{
 int i;
  bf_formula *ba;
  bf_formula *bb;
  bf_formula *val;
  int sz = s_max_size (a, b);
  bitvector *result = bitvector_create ();
  bf_formula *carry_in = bf_crt_true (t->bm);

  for (i = 0; i < sz - 1; i++)
    {
      ba = bitvector_get_extbit (a, i);
      bb = bitvector_get_extbit (b, i);
      val = bf_crt_full_adder (t->bm, bf_ref (ba), 
			       bf_crt_not (t->bm, bf_ref (bb)), 
			       carry_in, &carry_in);
      bitvector_add (result, val);
    }
  ba = bitvector_get_extbit (a, i);
  bb = bitvector_get_extbit (b, i);
  if (p_carry)
    val = bf_crt_full_adder (t->bm, bf_ref (ba), 
			     bf_crt_not (t->bm, bf_ref (bb)), 
			     carry_in, p_carry);
  else
    val = bf_crt_xor3 (t->bm, bf_ref (ba), 
		       bf_crt_not(t->bm, bf_ref (bb)), 
		       carry_in);

  bitvector_add (result, val);

  return result;
}

bitvector *
bitblaster_mk_sub (bitblaster *t, const bitvector *a, const bitvector *b)
{
 int i;
  bf_formula *ba;
  bf_formula *bb;
  bf_formula *val;
  int sz = s_max_size (a, b) + 1;
  bitvector *result = bitvector_create ();
  bf_formula *carry_in = bf_crt_true (t->bm);

  for (i = 0; i < sz; i++)
    {
      ba = bitvector_get_extbit (a, i);
      bb = bitvector_get_extbit (b, i);
      val = bf_crt_full_adder (t->bm, bf_ref (ba), 
			       bf_crt_not (t->bm, bf_ref (bb)), 
			       carry_in, &carry_in);
      bitvector_add (result, val);
    }
  bf_unref (carry_in);

  return result;
}

bitvector *
bitblaster_mk_equ (bitblaster *t, const bitvector *a, const bitvector *b)
{
  int i;
  bitvector *result = bitvector_create ();
  int sz = s_max_size (a, b);
  bf_formula *val = bf_crt_true (t->bm);

  for (i = sz - 1; i > 0; i--)
    {
      bf_formula *ba = bitvector_get_extbit (a, i);
      bf_formula *bb = bitvector_get_extbit (b, i);

      val = bf_crt_and (t->bm, val, 
			bf_crt_equiv (t->bm, bf_ref (ba), bf_ref (bb)));
    }
  bitvector_add (result, val);

  return result;
}

bitvector *
bitblaster_mk_nequ (bitblaster *t, const bitvector *a, const bitvector *b)
{
  int i;
  bitvector *result = bitvector_create ();
  int sz = s_max_size (a, b);
  bf_formula *val = bf_crt_false (t->bm);

  for (i = 0; i < sz; i++)
    {
      bf_formula *ba = bitvector_get_extbit (a, i);
      bf_formula *bb = bitvector_get_extbit (b, i);

      val = bf_crt_or (t->bm, val, 
		       bf_crt_xor (t->bm, bf_ref (ba), bf_ref (bb)));
    }
  bitvector_add (result, val);

  return result;
}

bitvector *
bitblaster_mk_lt (bitblaster *t, const bitvector *a, const bitvector *b)
{
  bitvector *result = bitvector_create ();
  bitvector *a_minus_b = bitblaster_mk_sub (t, a, b);  
  bf_formula *s = bitvector_get_sign (a_minus_b);
  
  bitvector_add (result, bf_ref (s));
  bitvector_delete (a_minus_b);

  return result;
}

static bf_formula *
s_bit_mul (bitblaster *t, const bitvector *a, int i, const bitvector *b, int j)
{
  bf_formula *ba = bitvector_get_extbit (a, i);
  bf_formula *bb = bitvector_get_extbit (b, j);
  bf_formula *result = bf_crt_and (t->bm, bf_ref (ba), bf_ref (bb));

  return result;
}

bitvector *
bitblaster_mk_mul (bitblaster *t, const bitvector *a, const bitvector *b)
{
  int i;
  int sz = 2 * s_max_size (a, b);
  bitvector *result = bitvector_create ();
  bitvector *carry_in = NULL;
  bitvector *carry_out = NULL;
  bf_formula *in1; /* input 1 of adders */
  bf_formula *in2; /* input 2 of adders */
  bf_formula *cin; /* input carry of full-adders */
  bf_formula *cout; /* output carry */
  bf_formula *add; /* result of the adder */

  bitvector_add (result, s_bit_mul (t, a, 0, b, 0));

  for (i = 1; i < sz; i++)
    {
      int j;

      carry_out = bitvector_create ();
      in1 = s_bit_mul (t, a, 0, b, i);
      in2 = s_bit_mul (t, a, 1, b, i - 1);
      if (i < sz - 1)
	{
	  add = bf_crt_half_adder (t->bm, in1, in2, &cout);
	  bitvector_add (carry_out, cout);

	  for (j = 2; j <= i; j++)
	    {
	      in1 = add;
	      in2 = s_bit_mul (t, a, j, b, i - j);
	      cin = bitvector_get_bit (carry_in, j - 2);
	      add = bf_crt_full_adder (t->bm, in1, in2, bf_ref (cin), &cout);
	      bitvector_add (carry_out, cout);
	    }
	  bitvector_add (result, add);
	  ccl_zdelete (bitvector_delete, carry_in);
	  carry_in = carry_out;
	}
      else
	{
	  /* last step. don't generate/store couts. */
	  
	  add = bf_crt_xor (t->bm, in1, in2);

	  for (j = 2; j <= i; j++) 
	    {
	      in1 = add;
	      in2 = s_bit_mul (t, a, j, b, i - j);
	      cin = bitvector_get_bit (carry_in, j - 2);
	      add = bf_crt_xor3 (t->bm, in1, in2, bf_ref (cin));
	    }
	  bitvector_add (result, add);
	}
    }

  bitvector_delete (carry_in);
  bitvector_delete (carry_out);

  return result;
}

static bitvector *
s_mk_apply_binary (bitblaster *t, const bitvector *a, const bitvector *b,
		   bf_formula * (*binop) (bf_manager *, 
					     bf_formula *, bf_formula *))
{
  int i;
  bitvector *result = bitvector_create ();  
  int sz = bitvector_get_size (a);

  ccl_pre (sz == bitvector_get_size (b));
  
  for (i = 0; i < sz; i++)
    {
      bf_formula *ba = bitvector_get_bit (a, i);
      bf_formula *bb = bitvector_get_bit (b, i);
      bitvector_add (result, binop (t->bm, bf_ref (ba), bf_ref (bb)));
    }

  return result;
}

bitvector *
bitblaster_mk_and (bitblaster *t, const bitvector *a, const bitvector *b)
{
  return s_mk_apply_binary (t, a, b, bf_crt_and);
}

bitvector *
bitblaster_mk_imply (bitblaster *t, const bitvector *a, const bitvector *b)
{
  return s_mk_apply_binary (t, a, b, bf_crt_imply);
}

bitvector *
bitblaster_mk_xor (bitblaster *t, const bitvector *a, const bitvector *b)
{
  return s_mk_apply_binary (t, a, b, bf_crt_xor);
}

bitvector *
bitblaster_mk_not (bitblaster *t, const bitvector *a)
{
  int i;
  bitvector *result = bitvector_create ();  
  int sz = bitvector_get_size (a);
    
  for (i = 0; i < sz; i++)
    {
      bf_formula *ba = bitvector_get_bit (a, i);
      bitvector_add (result, bf_crt_not (t->bm, bf_ref (ba)));
    }

  return result;
}

bitvector *
bitblaster_mk_multiplexer (bitblaster *t, bf_formula *c, 
			   const bitvector *a, const bitvector *b)
{
  ccl_pre (bitvector_get_size (a) == bitvector_get_size (b));
  
  return bitblaster_mk_multiplexer_with_get_bit (t, c, a, b,
						 bitvector_get_bit);
}

bitvector *
bitblaster_mk_signed_multiplexer (bitblaster *t, bf_formula *c, 
				  const bitvector *a, const bitvector *b)
{
  return bitblaster_mk_multiplexer_with_get_bit (t, c, a, b,
						 bitvector_get_extbit);
}

bitvector *
bitblaster_mk_multiplexer_with_get_bit (bitblaster *t, bf_formula *c, 
					const bitvector *a, const bitvector *b,
					bf_formula *get_bit (const bitvector *,
							     int))
{
  int i;
  bitvector *result = bitvector_create ();  
  int sz = bitvector_get_size (a);
  
  for (i = 0; i < sz; i++)
    {
      bf_formula *ba = get_bit (a, i);
      bf_formula *bb = get_bit (b, i);
      bitvector_add (result, 
		     bf_crt_ite (t->bm, bf_ref (c), bf_ref (ba), bf_ref (bb)));
    }
  bf_unref (c);

  return result;
}

bitvector *
bitblaster_mk_abs (bitblaster *t, const bitvector *a)
{
  int sz = bitvector_get_size (a);
  bf_formula *sign = bitvector_get_bit (a, sz - 1);
  bitvector *result;

  if (bf_is_false (sign))
    result = bitvector_clone (a);
  else if (bf_is_true (sign))
    result = bitblaster_mk_neg (t, a);
  else
    {
      bitvector *neg_a = bitblaster_mk_neg (t, a);
      result = bitblaster_mk_multiplexer (t, bf_ref (sign), neg_a, a);
      bitvector_delete (neg_a);
    }

  return result;
}


static void
s_mk_udiv_urem (bitblaster *t, const bitvector *a, const bitvector *b,
		bitvector **p_quotient, bitvector **p_remainder)
{
  int i;
  int sz = s_max_size (a, b);
  ccl_list *qbits = ccl_list_create ();
  bitvector *rbits = bitvector_create ();

  bitvector_add (rbits, bf_ref (bitvector_get_extbit (a, sz - 1)));
  for (i = 1; i < sz; i++)
    bitvector_add (rbits, bf_crt_false (t->bm));

  for (i = 0; i < sz; i++)
    {
      bf_formula *q;
      bitvector *r_minus_b = s_mk_sub (t, rbits, b, &q);

      if (p_quotient)
	{
	  ccl_list_put_first (qbits, bf_ref (q));
	}

      if (i < sz - 1)
	{
	  int j;

	  for (j = sz - 1; j > 0; j--)
	    {
	      bf_formula *br = bitvector_get_extbit (rbits, j - 1);
	      bf_formula *bd = bitvector_get_extbit (r_minus_b, j - 1);
	      bf_formula *f = bf_crt_ite (t->bm, 
					  bf_ref (q), bf_ref (bd), bf_ref (br));
	      bitvector_set_bit (rbits, j, f);
	    }
	  bitvector_set_bit (rbits, 0, 
			     bf_ref (bitvector_get_extbit (a, sz - i - 2)));
	}
      else if (p_remainder)
	{
	  int j;
	  for (j = 0; j < sz; j++) 
	    {
	      bf_formula *br = bitvector_get_extbit (rbits, j);
	      bf_formula *bd = bitvector_get_extbit (r_minus_b, j);
	      bf_formula *f = bf_crt_ite (t->bm, 
					  bf_ref (q), bf_ref (bd), bf_ref (br));
	      bitvector_set_bit (rbits, j, f);
            }
	}
      bitvector_delete (r_minus_b);
      bf_unref (q);
    }
  
  if (p_quotient)
    {
      *p_quotient = bitvector_create ();
      while (! ccl_list_is_empty (qbits))
	{
	  bf_formula *qb = ccl_list_take_first (qbits);
	  bitvector_add (*p_quotient, qb);
	}
      ccl_list_delete (qbits);
    }
  else
    {
      ccl_list_clear_and_delete (qbits, (ccl_delete_proc *) bf_unref);
    }

  if (p_remainder)
    *p_remainder = rbits;
  else 
    bitvector_delete (rbits);
}

static void
s_neg_if_not_null (bitblaster *t, bitvector **p_bv)
{
  if (p_bv)
    {
      bitvector *aux = bitblaster_mk_neg (t, *p_bv);	  
      bitvector_delete (*p_bv);
      *p_bv = aux;
    }
}

static void
s_mk_sdiv_srem (bitblaster *t, const bitvector *a, const bitvector *b,
		bitvector **p_quotient, bitvector **p_remainder)
{
  bf_formula *asign = bitvector_get_sign (a);
  bf_formula *bsign = bitvector_get_sign (b);

  if (bf_is_false (asign) && bf_is_false (bsign))
    {
      s_mk_udiv_urem (t, a, b, p_quotient, p_remainder);
    }  
  else if (bf_is_false (asign) && bf_is_true (bsign))
    {
      bitvector *negb = bitblaster_mk_neg (t, b);
      s_mk_udiv_urem (t, a, negb, p_quotient, p_remainder);
      s_neg_if_not_null (t, p_quotient);
      bitvector_delete (negb);
    }
 else if (bf_is_true (asign) && bf_is_false (bsign))
   {
      bitvector *nega = bitblaster_mk_neg (t, a);
      s_mk_udiv_urem (t, nega, b, p_quotient, p_remainder);
      s_neg_if_not_null (t, p_quotient);
      s_neg_if_not_null (t, p_remainder);
      bitvector_delete (nega);
   }
 else if (bf_is_true (asign) && bf_is_true (bsign))
   {
     bitvector *nega = bitblaster_mk_neg (t, a);
     bitvector *negb = bitblaster_mk_neg (t, b);
     s_mk_udiv_urem (t, nega, negb, p_quotient, p_remainder);
     s_neg_if_not_null (t, p_remainder);
     bitvector_delete (nega);
     bitvector_delete (negb);
   }
 else 
   {
     bitvector *abs_a = bitblaster_mk_abs (t, a);
     bitvector *abs_b = bitblaster_mk_abs (t, b);
     bitvector *negq;
     bitvector *negr;

     s_mk_udiv_urem (t, abs_a, abs_b, p_quotient, p_remainder);
     bitvector_delete (abs_b);
     bitvector_delete (abs_a);

     negq = p_quotient ? bitblaster_mk_neg (t, *p_quotient) : NULL;
     negr = p_remainder ? bitblaster_mk_neg (t, *p_remainder) : NULL;

     if (p_quotient)
       {
	 bf_formula *nosign = 
	   bf_crt_iff (t->bm, bf_ref (asign), bf_ref (bsign));
	 bitvector *aux = 
	   bitblaster_mk_multiplexer (t, bf_ref (nosign), *p_quotient, negq);
	 bitvector_delete (*p_quotient);
	 bitvector_delete (negq);
	 *p_quotient = aux;
	 bf_unref (nosign);
       }
     if (p_remainder)
       {
	 bf_formula *nosign = bitvector_get_sign (a);
	 bitvector *aux = 
	   bitblaster_mk_multiplexer (t, bf_ref (nosign), negr, *p_remainder);
	 bitvector_delete (*p_remainder);
	 bitvector_delete (negr);
	 *p_remainder = aux;
       }
    }
}


bitvector *
bitblaster_mk_div (bitblaster *t, const bitvector *a, const bitvector *b)
{
  bitvector *result;
  s_mk_sdiv_srem (t, a, b, &result, NULL);
  return result;
}

bitvector *
bitblaster_mk_mod (bitblaster *t, const bitvector *a, const bitvector *b)
{
  bitvector *result;
  s_mk_sdiv_srem (t, a, b, NULL, &result);
  return result;
}

bitvector *
bitblaster_mk_min (bitblaster *t, const bitvector *a, const bitvector *b)
{  
  bitvector *result;
  bitvector *a_minus_b = bitblaster_mk_sub (t, a, b);  
  bf_formula *s = bitvector_get_sign (a_minus_b);

  if (bf_is_true (s))  
    result = bitvector_clone (a);
  else if (bf_is_false (s))
    result = bitvector_clone (b);
  else
    result = bitblaster_mk_multiplexer (t, bf_ref (s), a, b);
  bitvector_delete (a_minus_b);

  return result;
}

bitvector *
bitblaster_mk_max (bitblaster *t, const bitvector *a, const bitvector *b)
{
  bitvector *result;
  bitvector *a_minus_b = bitblaster_mk_sub (t, a, b);  
  bf_formula *s = bitvector_get_sign (a_minus_b);

  if (bf_is_true (s))  
    result = bitvector_clone (b);
  else if (bf_is_false (s))
    result = bitvector_clone (a);
  else
    result = bitblaster_mk_multiplexer (t, bf_ref (s), b, a);
  bitvector_delete (a_minus_b);

  return result;
}

static int 
s_max_size (const bitvector *a, const bitvector *b)
{
  int res = bitvector_get_size (a);

  if (res < bitvector_get_size (b))
    res = bitvector_get_size (b);
  return res;
}

