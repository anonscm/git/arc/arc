/*
 * ca2sat-ut.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-ca-semantics.h"
#include "parser/altarica-input.h"
#include <commands/allcmds-p.h>
#include "ar-interp-altarica.h"
#include "glucose.h"
#include "bitblaster.h"
#include "ca2sat.h"

struct tests
{
  const char *name;
  int (*testfunc) (int *p_testid, ccl_log_type log);
};

#define UT(func) { #func, s_ ## func ## _test }

static bf_index 
s_new_var (void *pS)
{
  glucose_solver *S = *((glucose_solver **)pS);

  return glucose_solver_new_var (S);
}

#define CONTEXT_NAME "CA2SAT_NODE"
#define CA_CONTEXT						\
  "node " CONTEXT_NAME "\n"					\
  "state f, f_ : [1,3]; \n"					\
  "state x : [-10,10]; \n"					\
  "state z : [-10,10]; \n"					\
  "state P,Q,R : bool; \n"					\
  "state c:[0,1][6]; \n"					\
  "state y : [0,9]; r : [0,9]; o : [0,9]; "			\
  " d : [0,9]; n : [0,9]; e : [0,9]; m : [0,9]; s : [0,9]; \n"	\
  " g : [0,9]; l : [0,9]; t : [0,9]; b : [0,9]; a : [0,9]; \n"	\
  "edon " 

static const char *TAUTOLOGIES[] = {
  "(e > 0) => ((d + g) mod e <= (e - 1))",
  "3 < 4",
  "((s != 0 & m != 0 & "
  "d+e = 10*c[0]+y &"
  "c[0]+n+r = 10*c[1]+e &"
  "c[1]+e+o = 10*c[2]+n &"
  "c[2]+s+m = 10*c[3]+o &"
  "c[3]     = m         &"
  "s != e & s != n & s != d & s != m & s != o & s != r & s != y &"
  "e != n & e != d & e != m & e != o & e != r & e != y &"
  "n != d & n != m & n != o & n != r & n != y &"
  "d != m & d != o & d != r & d != y &"
  "m != o & m != r & m != y &"
  "o != r & o != y &"
  "r != y) = (y = 2 & r = 8 & o = 0 & d = 7 & n = 6 & e = 5 & m = 1 & s = 9 & "
  "c[2] = 0 & c[0] = 1 & c[1] = 1 & c[3] = 1))",     

  "   ( d != 0 &"
  "          2*d = 10*c[0]+t &"
  "     c[0]+2*l = 10*c[1]+r &"
  "     c[1]+2*a = 10*c[2]+e &"
  "     c[2]+n+r = 10*c[3]+b &"
  "     c[3]+o+e = 10*c[4]+o &"
  "     c[4]+d+g = 10*c[5]+r &"
  "     c[5] = 0 &"
  "d != o & d != n & d != a & d != l & d != g & d != e & d != r & d != b & "
  "d != t &"
  "o != n & o != a & o != l & o != g & o != e & o != r & o != b & o != t &"
  "n != a & n != l & n != g & n != e & n != r & n != b & n != t &"
  "a != l & a != g & a != e & a != r & a != b & a != t &"
  "l != g & l != e & l != r & l != b & l != t &"
  "g != e & g != r & g != b & g != t &"
  "e != r & e != b & e != t &"
  "r != b & r != t &"
  "b != t) = (d = 5 & r = 7 & a = 4 & c[1] = 1 & o = 2 & b = 3 & n = 6 & "
  "c[5] = 0 & c[2] = 0 & c[3] = 1 & t = 0 & l = 8 & e = 9 & g = 1 & c[0] = 1 "
  "& c[4] = 1)", 
  "3 <= 4",
  "not (4 < 3)",
  "not (4 <= 3)",
  "not (3 < 3)",
  "3 <= 3",
  "(3*x = y) => (y = 0 | y = 3 | y = 6 | y = 9)", 
  "(3*x=y) => (y mod 3 = 0)",
  "(14*x=y) => (y mod 7 = 0 and y mod 2 = 0)",
  "(x*y=z) => (z mod x = 0 and z mod y = 0)",
  "not not (P or not P)", 
  "not(P or Q) => ( not P and not Q)",
  "P => (Q => (P and Q))",
  "(P or Q) => (Q or P)",
  "(P => R) => ((Q => R) => ((P or Q) => R))",
  "P => (P or Q)",
  "(P => Q) => ((not P => Q) => not not Q)", 
  "(P => not P) => not P",
  "not P => (P => Q)", 
  "false => (2 = 3)", 
  "(P => Q) => ((not Q) => not P)",
  "(P => Q) => ((Q => R) => (P => R))",
  "(P => (Q => R)) => ((P => Q) => (P => R))",
  "(((P=>Q)=>Q)=>Q)=> (P => Q)",
  "true", 
  "(max (x, y) > 5) = (x > 5 or y > 5)", 
  "(max (x, y) < 5) = (x < 5 and y < 5)", 
  "(min (x, y) > 5) = (x > 5 and y > 5)", 
  "(min (x, y) < 5) = (x < 5 or y < 5)", 
  "min(x,y) = (if x < y then x else y)",
  "(if x < y then y else x) = max (x, y)",
  "(x*x-2*x-15=0) = (x=-3 or x=5)", 
  "3 * 6 = 18",
  "4 mod 3 = 1",
  NULL
};

static ar_ca_expr *
s_build_expression (ar_ca *ca, const char *formula)
{
  ar_ca_exprman *man;
  ar_node *node;
  ar_context *ctx;
  ar_expr *goal;
  ar_expr *flat_goal;
  ar_ca_expr *result;
  char *tmp = ccl_string_format_new ("@%s@", formula);
  altarica_tree *t = altarica_read_string (tmp, "command line", AR_INPUT_AEXPR, 
					   NULL, NULL);
  ccl_string_delete (tmp);

  if (t == NULL)
    return NULL;

  man = ar_ca_get_expression_manager(ca);
  node = ar_ca_get_node (ca);
  ctx = ar_node_get_context (node);
  goal = NULL;
  flat_goal = NULL;
  result = NULL;

  ccl_try (exception)
    {


      goal = ar_interp_boolean_expression (t->child, ctx,
					   ar_interp_acheck_member_access);
      flat_goal = ar_expr_remove_composite_slots (goal, ctx);
      result = ar_model_expr_to_ca_expr (flat_goal, man, NULL);            
    }
  ccl_no_catch;

  ccl_zdelete (ar_expr_del_reference, flat_goal);
  ccl_zdelete (ar_expr_del_reference, goal);
  ar_context_del_reference (ctx);
  ar_node_del_reference (node);
  ar_ca_exprman_del_reference (man);
  ccl_parse_tree_delete_tree (t);

  return result;
}

static void
s_add_domain_constraint (ar_ca_expr **E, ccl_list *vars)
{
  ccl_pair *p;
  ar_ca_expr *result = ar_ca_expr_add_reference (*E);

  for (p = FIRST (vars); p; p = CDR (p))
    {
      ar_ca_expr *v = CAR (p);
      const ar_ca_domain *D = ar_ca_expr_variable_get_domain (v);
      ar_ca_expr *tmp1 = ar_ca_expr_crt_in_domain (v, D);
      ar_ca_expr *tmp2 = ar_ca_expr_crt_not (tmp1);
      ar_ca_expr_del_reference (tmp1);
      tmp1 = ar_ca_expr_crt_or (tmp2, result);
      ar_ca_expr_del_reference (result);
      result = tmp1;
    }
  ar_ca_expr_del_reference (*E);
  *E = result;
}

static int
s_tautology_checking_test (int *ptestid, ccl_log_type log)
{
  int result;
  altarica_tree *t = 
    altarica_read_string (CA_CONTEXT, "ca2sat unit tests", AR_INPUT_ALTARICA, 
			  NULL, NULL);
  if (t == NULL)    
    return 0;
  
  result = ar_interp_altarica_tree (t->child);
  ccl_parse_tree_delete_tree (t);

  if (result)
    {
      ar_ca *ca = arc_command_get_ca (CONTEXT_NAME);
      
      if (ca != NULL)
	{
	  glucose_solver *S;
	  ar_ca_exprman *eman = ar_ca_get_expression_manager (ca);
	  ca2sat_translator *c2s = ca2sat_translator_create (ca, s_new_var, &S);
	  bf_manager *bm = ca2sat_get_bf_manager (c2s);
	  const char **t;

	  for (t = TAUTOLOGIES; result && *t; t++)
	    {
	      bf_formula *bexpr;
	      bf_formula *not_expr;
	      varset *vs;
	      ccl_list *vars;
	      ar_ca_expr *expr;

	      (*ptestid)++;
	      ccl_log (log, "[%3d] checking tautology %s ... ", *ptestid, *t);
	      
	      if ((expr = s_build_expression (ca, *t)) == NULL)
		{
		  ccl_log (log, "failed\n");
		  result = 0;
		  continue;
		}

	      vars = ar_ca_expr_get_variables (expr, NULL);
	      s_add_domain_constraint (&expr, vars);

	      vs = varset_create ();

	      S = glucose_solver_create ();
	      bexpr = ca2sat_translate_boolean_expr (c2s, vs, expr);
	      not_expr = bf_crt_not (bm, bexpr);
	      bf_to_cnf (bm, not_expr, glucose_add_clause_to_solver, S, 1);
	      bf_unref (not_expr);
	      result = !glucose_solver_solve (S);
	      if (! result && ccl_debug_is_on)
		{
		  int msize;
		  char *model = glucose_solver_get_model (S, &msize);

		  ccl_debug ("counter example:");

		  while (! ccl_list_is_empty (vars))
		    {
		      ar_ca_expr *v = ccl_list_take_first (vars);
		      ar_ca_expr *val = 
			varset_get_value_of (vs, v, model, msize);
		      ccl_debug (" ");
		      ar_ca_expr_log (CCL_LOG_DEBUG, v);
		      ccl_debug (" := ");
		      ar_ca_expr_log (CCL_LOG_DEBUG, val);
		      ar_ca_expr_del_reference (val);
		    }
		  ccl_debug ("\n");
		}
	      glucose_solver_destroy (S);
	      ar_ca_expr_del_reference (expr);
	      varset_unref (vs);
	      ccl_log (log, "%s\n", result ? "ok" : "failed");
	      ccl_list_delete (vars);
	    }
	  
	  ca2sat_translator_destroy (c2s);
	  ar_ca_exprman_del_reference (eman);
	}
    }

  return result;
}

int 
ca2sat_unit_tests (ccl_log_type log)
{
  int global_result = 1;
  static int TESTID;

  struct tests T[] = {
    UT (tautology_checking), 
    { NULL, NULL }
  };
  struct tests *pT;

  ccl_log (log, "ca2sat test-suite\n");  
  srand (123456789);
  TESTID = 0;

  for (pT = T; pT->name != NULL; pT++)
    {
      int result = pT->testfunc (&TESTID, log);
      global_result = global_result && result;
    }


  return global_result;
}
