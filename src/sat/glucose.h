/*
 * glucose.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef GLUCOSE_H
# define GLUCOSE_H

# include "config.h"
# include <ccl/ccl-common.h>
# include <ccl/ccl-list.h>
# include <sat/boolean-formula.h>

BEGIN_C_DECLS

typedef struct glucose_solver_st glucose_solver;

extern glucose_solver *
glucose_solver_create (void);

extern void
glucose_solver_destroy (glucose_solver *S);

extern void
glucose_solver_set_group (glucose_solver *S, bf_index gid);

extern void
glucose_solver_clear_group (glucose_solver *S);

extern void
glucose_solver_gc (glucose_solver *S);

extern bf_index
glucose_solver_new_var (glucose_solver *S);

extern void
glucose_solver_add_clause_0 (glucose_solver *S);

extern void
glucose_solver_add_clause_1 (glucose_solver *S, bf_literal l);

extern void
glucose_solver_add_clause_2 (glucose_solver *S, bf_literal l1, bf_literal l2);

extern void
glucose_solver_add_clause_3 (glucose_solver *S, bf_literal l1, bf_literal l2, 
			     bf_literal l3);

extern void
glucose_solver_add_clause (glucose_solver *S, const bf_literal *lits, 
			   int nb_lits);

extern void
glucose_add_clause_to_solver (const bf_literal *lits, int nb_lits, void *data);

extern int
glucose_solver_solve (glucose_solver *S);

extern int
glucose_solver_solve_under_assumptions_1 (glucose_solver *S, bf_literal l);

extern int
glucose_solver_solve_under_assumptions_2 (glucose_solver *S, bf_literal l1, 
					  bf_literal l2);
extern int
glucose_solver_solve_under_assumptions (glucose_solver *S, ccl_list *lit);

extern char *
glucose_solver_get_model (glucose_solver *S, int *msize);

extern bf_literal *
glucose_solver_get_conflict (glucose_solver *S, int *p_csize);

extern int 
glucose_solver_get_nb_clauses (glucose_solver *S);

extern int 
glucose_solver_get_nb_learnts (glucose_solver *S);

extern int
glucose_solver_is_okay (glucose_solver *S);

END_C_DECLS

#endif /* ! GLUCOSE_H */
