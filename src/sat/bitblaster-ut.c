/*
 * bitblaster-ut.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdlib.h>
#include "bitblaster.h"
#include "glucose.h"

static bitvector *
s_is_not_exclzero (bitblaster *t, const bitvector *a)
{
  bitvector *badza = bitblaster_mk_excluded_zero (t, bitvector_get_size (a));
  bitvector *neqa = bitblaster_mk_nequ (t, a, badza);    
  bitvector_delete (badza);

  return neqa;
}

static bitvector *
s_are_not_exclzero (bitblaster *t, const bitvector *a, const bitvector *b)
{
  bitvector *ca = s_is_not_exclzero (t, a);
  bitvector *cb = s_is_not_exclzero (t, b);
  bitvector *res = bitblaster_mk_and (t, ca, cb);
  bitvector_delete (ca);
  bitvector_delete (cb);

  return res;
}

static bitvector *
s_mk_equ (bitblaster *t, const bitvector *dom, 
	  const bitvector *a, const bitvector *b)
{
  bitvector *eq = bitblaster_mk_equ (t, a, b);  
  bitvector *result = bitblaster_mk_imply (t, dom, eq);  
  bitvector_delete (eq);

  return result;
}  


static int
s_are_bv_equiv (bitblaster *t, glucose_solver *S, const bitvector *dom, 
		const bitvector *a, 
		const bitvector *b)
{
  bitvector *eq = s_mk_equ (t, dom, a, b);  
  bf_manager *bm = bitblaster_get_bf_manager (t);
  int result = bitvector_is_true (eq);
  if (! result && ! bitvector_is_constant (eq))
    {
#if 1
      bf_literal lF;
      bf_index q = glucose_solver_new_var (S);
      bf_formula *not_eq = bitvector_get_bit (eq, 0);
      not_eq = bf_crt_not (bm, bf_ref (not_eq));

      lF = bf_to_cnf (bm, not_eq, glucose_add_clause_to_solver, S, 0);
      bf_unref (not_eq);

      glucose_solver_add_clause_2 (S, bf_var2lit (q, 1), lF);
      result = !glucose_solver_solve_under_assumptions_1 (S, bf_var2lit (q, 0));
      glucose_solver_add_clause_1 (S, bf_var2lit (q, 1));
#else
      bf_formula *not_eq = bitvector_get_bit (eq, 0);
      not_eq = bf_crt_not (bm, bf_ref (not_eq));

      bf_to_cnf (bm, not_eq, glucose_add_clause_to_solver, S, 1);
      bf_unref (not_eq);

      result = !glucose_solver_solve (S);
#endif
    }
  bitvector_delete (eq);

  return result;
}

/* extend (neg (a)) = neg (extend (a)) */
static int
s_extend_neg_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		   bitvector *b)
{
  bitvector *neg_a = bitblaster_mk_neg (t, a);
  bitvector *extend_neg_a = 
    bitblaster_sign_extend (t, bitvector_get_size (a) + 2, neg_a);
  bitvector *extend_a = 
    bitblaster_sign_extend (t, bitvector_get_size (a) + 2, a);
  bitvector *neg_extend_a = bitblaster_mk_neg (t, extend_a);
  bitvector *C = s_are_not_exclzero (t, a, b);
  int result = s_are_bv_equiv (t, S, C, neg_extend_a, extend_neg_a);
  bitvector_delete (C);
  bitvector_delete (neg_a);
  bitvector_delete (extend_neg_a);
  bitvector_delete (extend_a);
  bitvector_delete (neg_extend_a);

  return result;
}

/* a - b = a + neg (b) */
static int 
s_minus_add_neg_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		      bitvector *b)
{
  bitvector *a_minus_b = bitblaster_mk_sub (t, a, b);
  bitvector *neg_b = bitblaster_mk_neg (t, b);
  bitvector *a_plus_neg_b = bitblaster_mk_add (t, a, neg_b);
  bitvector *C = s_are_not_exclzero (t, a, b);
  int result = s_are_bv_equiv (t, S, C, a_minus_b, a_plus_neg_b);
  bitvector_delete (C);
  bitvector_delete (a_minus_b);
  bitvector_delete (neg_b);
  bitvector_delete (a_plus_neg_b);

  return result;
}

/* a = neg (neg (a)) */
static int 
s_neg_neg_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		bitvector *dummy)
{
  bitvector *neg_a = bitblaster_mk_neg (t, a);
  bitvector *neg_neg_a = bitblaster_mk_neg (t, neg_a);
  bitvector *C = s_is_not_exclzero (t, a);
  int result = s_are_bv_equiv (t, S, C, a, neg_neg_a);
  bitvector_delete (C);
  bitvector_delete (neg_a);
  bitvector_delete (neg_neg_a);

  return result;
}

/* a - b = neg (b - a) */
static int 
s_minus_neg_test (bitblaster *t, glucose_solver *S, bitvector *a, bitvector *b)
{
  bitvector *a_minus_b = bitblaster_mk_sub (t, a, b);
  bitvector *b_minus_a = bitblaster_mk_sub (t, b, a);
  bitvector *neg_b_minus_a = bitblaster_mk_neg (t, b_minus_a);
  bitvector *C = s_are_not_exclzero (t, a, b);
  int result = s_are_bv_equiv (t, S, C, a_minus_b, neg_b_minus_a);
  bitvector_delete (C);
  bitvector_delete (a_minus_b);
  bitvector_delete (b_minus_a);
  bitvector_delete (neg_b_minus_a);

  return result;
}

/* a + b = a - neg b */
static int 
s_plus_eq_minus_neg_test (bitblaster *t, glucose_solver *S, bitvector *a, 
			  bitvector *b)
{
  bitvector *a_plus_b = bitblaster_mk_add (t, a, b);
  bitvector *neg_b = bitblaster_mk_neg (t, b);
  bitvector *a_minus_neg_b = bitblaster_mk_sub (t, a, neg_b);
  bitvector *C = s_are_not_exclzero (t, a, b);
  int result = s_are_bv_equiv (t, S, C, a_plus_b, a_minus_neg_b);
  bitvector_delete (C);
  bitvector_delete (a_plus_b);
  bitvector_delete (a_minus_neg_b);
  bitvector_delete (neg_b);

  return result;
}

/* a - a = 0 */
static int 
s_minus_zero_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		   bitvector *dummy)
{
  bitvector *a_minus_a = bitblaster_mk_sub (t, a, a);
  bitvector *zero = 
    bitblaster_mk_constant (t, bitvector_get_size (a), 0);
  bitvector *C = s_is_not_exclzero (t, a);
  int result = s_are_bv_equiv (t, S, C, a_minus_a, zero);
  bitvector_delete (C);
  bitvector_delete (a_minus_a);
  bitvector_delete (zero);

  return result;
}

/* a + a - a = a */
static int 
s_add_minus_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		  bitvector *dummy)
{
  bitvector *a_plus_a = bitblaster_mk_add (t, a, a);
  bitvector *a_plus_a_minus_a = bitblaster_mk_sub (t, a_plus_a, a);
  bitvector *C = s_is_not_exclzero (t, a);
  int result = s_are_bv_equiv (t, S, C, a_plus_a_minus_a, a);
  bitvector_delete (C);
  bitvector_delete (a_plus_a_minus_a);
  bitvector_delete (a_plus_a);

  return result;
}

/* a + 1 != a */
static int 
s_add_one_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		bitvector *dummy)
{
  bitvector *one = bitblaster_mk_constant (t, 2, 1);
  bitvector *a_plus_one = bitblaster_mk_add (t, a, one);
  bitvector *C = s_is_not_exclzero (t, a);
  int result = ! s_are_bv_equiv (t, S, C, a_plus_one, a);
  bitvector_delete (C);
  bitvector_delete (a_plus_one);
  bitvector_delete (one);

  return result;
}

/* a * b = a + ... + a if b is constant */

static int 
s_mul_eq_add_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		   bitvector *b)
{
  bitvector *aux;
  bitvector *a_mul_b;
  bitvector *a_b_times;
  bitvector *C;
  int bval;
  int sign;
  int i;
  int result;

  if (! bitvector_is_constant (b))
    {   
      if (! bitvector_is_constant (a))
	return 1;
      aux = b;
      b = a;
      a = aux;
    }

  bval = bitvector_to_integer (b);
  if (bval < 0)
    {
      bval = -bval;      
      sign = 1;
    }
  else
    sign = 0;

  if (bval > (1 << 15)-1)
    return 1;

  a_mul_b = bitblaster_mk_mul (t, a, b);
  a_b_times = bitblaster_mk_constant (t, bitvector_get_size (a_mul_b), 0);
  C = s_are_not_exclzero (t, a, b);  

  for (i = 0; i < bval; i++)
    {
      aux = bitblaster_mk_add (t, a_b_times, a);
      bitvector_delete (a_b_times);
      a_b_times = aux;
    }

  if (sign)
    {
      aux = bitblaster_mk_neg (t, a_b_times);
      bitvector_delete (a_b_times);
      a_b_times = aux;
    }
  result = s_are_bv_equiv (t, S, C, a_mul_b, a_b_times);
  bitvector_delete (C);
  bitvector_delete (a_mul_b);
  bitvector_delete (a_b_times);

  return result;
}

/* abs (a - b) = abs (b - a) */
static int 
s_abs_minus_test (bitblaster *t, glucose_solver *S, bitvector *a, bitvector *b)
{
  bitvector *a_minus_b = bitblaster_mk_sub (t, a, b);
  bitvector *abs_1 = bitblaster_mk_abs (t, a_minus_b);
  bitvector *b_minus_a = bitblaster_mk_sub (t, b, a);
  bitvector *abs_2 = bitblaster_mk_abs (t, b_minus_a);
  bitvector *C = s_are_not_exclzero (t, a, b);
  int result = s_are_bv_equiv (t, S, C, abs_1, abs_2);
  bitvector_delete (C);
  bitvector_delete (a_minus_b);
  bitvector_delete (abs_1);
  bitvector_delete (b_minus_a);
  bitvector_delete (abs_2);

  return result;
}

/* (a * b) / b = a */
static int
s_mul_div_test (bitblaster *t, glucose_solver *S, bitvector *a, bitvector *b)
{
  int result;

  if (bitvector_is_constant (b) && bitvector_to_integer (b) == 0)
    result = 1;
  else
    {
      bitvector *m = bitblaster_mk_mul (t, a, b);
      bitvector *d = bitblaster_mk_div (t, m, b);
      bitvector *tmp = s_are_not_exclzero (t, a, b);
      bitvector *zero = bitblaster_mk_constant (t, 2, 0);
      bitvector *bnotzero = bitblaster_mk_nequ (t, b, zero);
      bitvector *C = bitblaster_mk_and (t, tmp, bnotzero);

      result = s_are_bv_equiv (t, S, C, d, a);
      
      bitvector_delete (C);
      bitvector_delete (bnotzero);
      bitvector_delete (zero);
      bitvector_delete (tmp);
      bitvector_delete (m);
      bitvector_delete (d);
    }

  return result;
}

/* q = a / b, r = a % b, a = q * b + r */
static int
s_mul_div_mod_test (bitblaster *t, glucose_solver *S, bitvector *a, 
		    bitvector *b)
{
  bitvector *q = bitblaster_mk_div (t, a, b);
  bitvector *r = bitblaster_mk_mod (t, a, b);
  bitvector *q_mul_b = bitblaster_mk_mul (t, q, b);
  bitvector *q_mul_b_plus_r = bitblaster_mk_add (t, q_mul_b, r);
  bitvector *tmp = s_are_not_exclzero (t, a, b);
  bitvector *zero = bitblaster_mk_constant (t, 2, 0);
  bitvector *bnotzero = bitblaster_mk_nequ (t, b, zero);
  bitvector *C = bitblaster_mk_and (t, tmp, bnotzero);
  
  int result = s_are_bv_equiv (t, S, C, q_mul_b_plus_r, a);

  bitvector_delete (C);
  bitvector_delete (bnotzero);
  bitvector_delete (zero);
  bitvector_delete (tmp);
  bitvector_delete (q_mul_b_plus_r);
  bitvector_delete (q_mul_b);
  bitvector_delete (r);
  bitvector_delete (q);

  return result;
}

struct tests 
{
  const char *name;
  int (*test) (bitblaster *t, glucose_solver *S, 
	       bitvector *a, bitvector *b);
};

static bf_index 
s_new_var (void *pS)
{
  glucose_solver *S = *((glucose_solver **)pS);

  return glucose_solver_new_var (S);
}

static void
s_log_bitvector_value (ccl_log_type log, bitvector *bva, char *model, int msize)
{
  int i, sz;

  ccl_log (log, "[ ");
  sz = bitvector_get_size (bva);
  for (i = 0; i < sz; i++)
    {
      bf_formula *v = bitvector_get_bit (bva, i);
      int value;
      if (bf_is_true (v)) 
	value = 1;
      else if (bf_is_false (v)) 
	value = 0;
      else 
	value = model[bf_get_var_index (v)];
      ccl_log (log, "%d ", value);
    }
  ccl_log (log, "]");

}

static void
s_log_counter_example (int tid, 
		       glucose_solver *S, bitvector *bva, bitvector *bvb)
{
  int msize;
  char *model = glucose_solver_get_model (S, &msize);

  ccl_debug ("TESTID = %d\n", tid);
  ccl_debug ("A = ");
  s_log_bitvector_value (CCL_LOG_DEBUG, bva, model, msize);
  ccl_debug ("\nB = ");
  s_log_bitvector_value (CCL_LOG_DEBUG, bvb, model, msize);
  ccl_debug ("\n");
}

static void
s_call_test (int *testid, int (*test) (bitblaster *t, glucose_solver *S, 
					bitvector *a, bitvector *b),
	     bitblaster *t, glucose_solver *S, bitvector *a, bitvector *b,
	     int *p_result)
{
  if (!*p_result)
    return;
  *p_result = test (t, S, a, b);

  if (!*p_result)
    s_log_counter_example (*testid, S, a, b);
  (*testid)++;
}


#define UT(func) { #func, s_ ## func ## _test }

int
bitblaster_unit_tests (ccl_log_type log)
{
  const int N = 100;
  int global_result = 1;
  ar_ca_exprman *eman = ar_ca_exprman_create ();
  glucose_solver *S = glucose_solver_create ();
  bitblaster *bt = bitblaster_create (eman, s_new_var, &S);
  static int TESTID;

  struct tests T[] = {
    UT (mul_div), 
    UT (mul_div_mod), 
    UT (abs_minus),
    UT (extend_neg),
    UT (minus_add_neg),
    UT (neg_neg),
    UT (minus_neg),
    UT (minus_zero),
    UT (add_minus),
    UT (plus_eq_minus_neg), 
    UT (add_one), 
    UT (mul_eq_add),
    { NULL, NULL }
  };
  struct tests *pT;

  ccl_log (log, "bitblaster test-suite\n");  
  srand (123456789);
  TESTID = 0;
  for (pT = T; pT->name != NULL; pT++)
    {
      int j;
      int result = 1;
      ccl_log (log, " %s...", pT->name);
      for (j = 2; j < 15 && result; j++)
	{
	  int i;	  
	  int wa = (rand () % j) + 2;
	  int wb = (rand () % j) + 2;
	  int wx = (rand () % j) + 2;
	  int wy = (rand () % j) + 2;
	  //	  S = glucose_solver_create ();
	  bitvector *bvx = bitblaster_mk_variable (bt, wx, "x_%d");
	  bitvector *bvy = bitblaster_mk_variable (bt, wy, "y_%d");

	  /* cst / cst, var/cst, cst/var test */
	  for (i = 0; i < N && result; i++)
	    {
	      int a = rand () % 0x3FFFFFFF;
	      int b = rand () % 0x3FFFFFFF;
	      bitvector *bva = bitblaster_mk_constant (bt, wa, a);
	      bitvector *bvb = bitblaster_mk_constant (bt, wb, b); 
	      s_call_test (&TESTID, pT->test, bt, S, bva, bvb, &result);
	      s_call_test (&TESTID, pT->test, bt, S, bvb, bva, &result);
	      bitvector_delete (bva);
	      bitvector_delete (bvb);
	    }

	  /* var / var test */
	  CCL_DEBUG_START_TIMED_BLOCK (("generic test wa=%d wb=%d", wa, wb));
	  s_call_test (&TESTID, pT->test, bt, S, bvx, bvy, &result);
	  CCL_DEBUG_END_TIMED_BLOCK ();
	  bitvector_delete (bvx);
	  bitvector_delete (bvy);
	  //	  glucose_solver_destroy (S);
	}

      ccl_log (log, " %s\n", result ? "ok" : "nok");
      global_result = global_result && result;

      glucose_solver_gc (S);
    }

  ar_ca_exprman_del_reference (eman);
  bitblaster_delete (bt);

  return global_result;
}
