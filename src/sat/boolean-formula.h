/*
 * boolean-formula.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef BOOLEAN_FORMULA_H
# define BOOLEAN_FORMULA_H

# include <ccl/ccl-log.h>

BEGIN_C_DECLS

typedef struct boolean_formula_manager_st bf_manager;
typedef struct boolean_formula_st bf_formula;
typedef uint32_t bf_index;
typedef uint32_t bf_literal;
typedef bf_index bf_fresh_index_allocator (void *data);

# define BF_UNDEF_INDEX (0xFFFFFFFF)
/* a literal with sign bit set to 1 is a negative literal */

# define bf_var2lit(var,sgn) ((bf_literal)(((var)<<1)|((sgn)?1:0)))
# define bf_lit2var(lit) ((bf_index)((lit)>>1))
# define bf_sign(lit) ((int)((lit)&0x1))
# define bf_negate(lit) ((bf_literal)((lit)^0x1))

extern bf_manager *
bf_crt_manager (bf_fresh_index_allocator *fia, void *clientdata);

extern void 
bf_manager_destroy (bf_manager *bm);

extern bf_formula *
bf_crt_true (bf_manager *bm);

extern bf_formula *
bf_crt_false (bf_manager *bm);

extern bf_formula *
bf_crt_and (bf_manager *bm, bf_formula *f1, bf_formula *f2);

extern bf_formula *
bf_crt_or (bf_manager *bm, bf_formula *f1, bf_formula *f2);

extern bf_formula *
bf_crt_not (bf_manager *bm, bf_formula *f);

extern bf_formula *
bf_crt_variable (bf_manager *bm, const char *comment);

extern bf_formula *
bf_crt_equiv (bf_manager *bm, bf_formula *f1, bf_formula *f2);

# define bf_crt_iff bf_crt_equiv

extern bf_formula *
bf_crt_not_equiv (bf_manager *bm, bf_formula *f1, bf_formula *f2);

extern bf_formula *
bf_crt_imply (bf_manager *bm, bf_formula *f1, bf_formula *f2);

extern bf_formula *
bf_crt_ite (bf_manager *bm, bf_formula *f1, bf_formula *f2, bf_formula *f3);

extern bf_formula *
bf_crt_at_least_one (bf_manager *bm, bf_formula **F, int nbF);

extern bf_formula *
bf_crt_at_most_one (bf_manager *bm, bf_formula **F, int nbF);

extern bf_formula *
bf_crt_one_among (bf_manager *bm, bf_formula **F, int nbF);

extern bf_formula *
bf_crt_xor (bf_manager *bm, bf_formula *a, bf_formula *b);

extern bf_formula *
bf_crt_xor3 (bf_manager *bm, bf_formula *a, bf_formula *b, bf_formula *c);

extern bf_formula *
bf_crt_carry (bf_manager *bm, bf_formula *a, bf_formula *b, bf_formula *c);

extern bf_formula *
bf_crt_half_adder (bf_manager *bm, bf_formula *a, bf_formula *b, 
		   bf_formula **p_carry);

extern bf_formula *
bf_crt_full_adder (bf_manager *bm, bf_formula *a, bf_formula *b, bf_formula *c, 
		   bf_formula **p_carry);

extern void
bf_log_formula (ccl_log_type log, bf_manager *bm, bf_formula *f);

extern int 
bf_is_true (bf_formula *f);

extern int 
bf_is_false (bf_formula *f);

extern bf_index 
bf_get_var_index (bf_formula *f);

extern const char *
bf_get_var_comment (bf_formula *f);

extern int 
bf_get_size (bf_formula *f);

extern bf_manager * 
bf_get_man (bf_formula *f);

extern bf_formula *
bf_ref (bf_formula *f);

extern void
bf_unref (bf_formula *f);

extern bf_literal 
bf_to_cnf_with_cache (bf_manager *bm, bf_formula *f, 
		      bf_literal (*cache)(bf_formula *f, bf_literal l, 
					  void *cachedata), 
		      void *cachedata, 
		      void (*add_clause) (const bf_literal *variables, 
					  int nb_variables, 
					  void *data),
		      void *data, int add_def);

extern bf_literal 
bf_to_cnf (bf_manager *bm, bf_formula *f, 
	   void (*add_clause) (const bf_literal *variables, 
			       int nb_variables, 
			       void *data),
	   void *data, int add_def);

END_C_DECLS

#endif /* ! BOOLEAN_FORMULA_H */
