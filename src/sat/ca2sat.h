/*
 * ca2sat.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef CA2SAT_H
# define CA2SAT_H

# include "ar-constraint-automaton.h"
# include "sat/boolean-formula.h"
# include "sat/varset.h"

typedef struct ca2sat_translator_st ca2sat_translator;

extern ca2sat_translator *
ca2sat_translator_create (ar_ca *ca, bf_fresh_index_allocator *fia, 
			  void *fiadata);

extern void
ca2sat_translator_destroy (ca2sat_translator *c2s);

extern bf_manager * 
ca2sat_get_bf_manager (ca2sat_translator *c2s);

extern varset * 
ca2sat_get_varset (ca2sat_translator *c2s);

extern bf_formula *
ca2sat_get_domain_constraint (ca2sat_translator *c2s, varset *vs);

extern bf_formula *
ca2sat_get_init (ca2sat_translator *c2s, varset *vs, 
		 int with_assertions);

extern bf_formula *
ca2sat_get_assertions (ca2sat_translator *c2s, varset *vs);

extern bf_formula *
ca2sat_translate_boolean_expr (ca2sat_translator *c2s, varset *vs, 
			       ar_ca_expr *e);

extern bf_formula *
ca2sat_translate_transition (ca2sat_translator *c2s, ar_ca_trans *t,
			     varset *vs, varset *vsp, 
			     int with_identity);

extern bf_formula *
ca2sat_translate_transitions (ca2sat_translator *c2s, ar_ca_trans **t,
			      int nb_t,
			      varset *vs, varset *vsp, 
			      int with_post_cond,
			      bf_formula **bt_vars);

/*!
 * Translate equality of e1[vs] and e2[vsp]
 */
extern bf_formula *
ca2sat_translate_post_relation (ca2sat_translator *c2s, 
				varset *vs, ar_ca_expr *e1,
				varset *vsp, ar_ca_expr *e2);

/*!
 * Generate a formula specifying that assignment of V0 and V1 can not be equal
 * i.e; there exists at least one variable v for which v[V0] != v[V1].
 */
extern bf_formula *
ca2sat_translate_distinct_state (ca2sat_translator *c2s, 
				 varset *V0, varset *V1);

extern void
ca2sat_display_solution (ca2sat_translator *c2s, const char *model);

extern int
ca2sat_unit_tests (ccl_log_type log);

#endif /* ! CA2SAT_H */
