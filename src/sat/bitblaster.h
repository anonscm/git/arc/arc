/*
 * bitblaster.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef BITBLASTER_H
# define BITBLASTER_H

# include "ar-ca-expression.h"
# include "sat/boolean-formula.h"
# include "sat/varset.h"

typedef struct bitblaster_st bitblaster;

extern bitblaster * 
bitblaster_create (ar_ca_exprman *eman, bf_fresh_index_allocator *fia, 
		   void *fiadata);

extern void 
bitblaster_delete (bitblaster *t);

extern bf_manager *
bitblaster_get_bf_manager (bitblaster *t);

extern bf_formula *
bitblaster_translate_boolean_expr (bitblaster *t, varset *V, ar_ca_expr *e);

extern const bitvector * 
bitblaster_translate_expr (bitblaster *t, varset *V, ar_ca_expr *e);

extern bf_formula *
bitblaster_translate_equality (bitblaster *t, varset *V1, ar_ca_expr *e1,
			       varset *V2, ar_ca_expr *e2);

/*!
 * alias for bitblaster_translate_expr
 */
extern const bitvector * 
bitblaster_mk_expr (bitblaster *t, varset *V, ar_ca_expr *e);

extern bitvector * 
bitblaster_mk_excluded_zero (bitblaster *t, int sz);

extern bitvector *
bitblaster_mk_constant (bitblaster *t, int sz, int value);

extern bitvector *
bitblaster_mk_integer (bitblaster *t, int value);

extern bitvector *
bitblaster_mk_expr_variable (bitblaster *t, varset *V, ar_ca_expr *v);

extern bitvector *
bitblaster_mk_variable (bitblaster *t, int sz, const char *namepattern);

extern bitvector *
bitblaster_sign_extend (bitblaster *t, int newsize, const bitvector *a);

extern bitvector *
bitblaster_mk_neg (bitblaster *t, const bitvector *a);
extern bitvector *
bitblaster_mk_add (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_sub (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_mul (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_div (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_mod (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_min (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_max (bitblaster *t, const bitvector *a, const bitvector *b);


extern bitvector *
bitblaster_mk_equ (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_nequ (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_lt (bitblaster *t, const bitvector *a, const bitvector *b);

extern bitvector *
bitblaster_mk_and (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_imply (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_xor (bitblaster *t, const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_not (bitblaster *t, const bitvector *a);

extern bitvector *
bitblaster_mk_multiplexer (bitblaster *t, bf_formula *cond, 
			   const bitvector *a, const bitvector *b);
extern bitvector *
bitblaster_mk_signed_multiplexer (bitblaster *t, bf_formula *c, 
				  const bitvector *a, const bitvector *b);

extern bitvector *
bitblaster_mk_multiplexer_with_get_bit (bitblaster *t, bf_formula *cond, 
					const bitvector *a, const bitvector *b,
					bf_formula *get_bit (const bitvector *,
							     int));

extern bitvector *
bitblaster_mk_abs (bitblaster *t, const bitvector *a);

extern int
bitblaster_unit_tests (ccl_log_type log);

#endif /* ! BITBLASTER_H */
