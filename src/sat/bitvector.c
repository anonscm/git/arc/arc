/*
 * bitvector.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "bitvector.h"

bitvector *
bitvector_create (void)
{
  bitvector *bv = ccl_new (bitvector);

  ccl_array_init (*bv);

  return bv;
}

bitvector *
bitvector_clone (const bitvector *bv)
{
  bitvector *result = bitvector_create ();

  ccl_pre (bv != NULL);  
  
  bitvector_copy (result, bv);

  return result;
}

void
bitvector_copy (bitvector *dst, const bitvector *src)
{
  int i;

  for (i = 0; i < dst->size; i++)
    {
      bf_unref (dst->data[i]);
      dst->data[i] = bf_ref (src->data[i]);
    }
  for (; i < src->size; i++)
    bitvector_add (dst, bf_ref (src->data[i]));
}

void
bitvector_delete (bitvector *bv)
{
  int i;

  ccl_pre (bv != NULL);

  for (i = 0; i < bv->size; i++)
    bf_unref (bv->data[i]);
  ccl_array_delete (*bv);
  ccl_delete (bv);
}

void
bitvector_clear (bitvector *bv)
{
  int i;

  ccl_pre (bv != NULL);

  for (i = 0; i < bv->size; i++)
    {
      ccl_zdelete (bf_unref, bv->data[i]);
      bv->data[i] = NULL;
    }
}

int 
bitvector_get_size (const bitvector *bv)
{
  ccl_pre (bv != NULL);

  return bv->size;
}

bf_formula *
bitvector_get_bit (const bitvector *bv, int index)
{
  ccl_pre (0 <= index && index < bitvector_get_size (bv));

  return bv->data[index];
}

bf_formula *
bitvector_get_extbit (const bitvector *bv, int index)
{
  ccl_pre (bv != NULL);
  ccl_pre (bitvector_get_size (bv) > 0);

  if (index < bitvector_get_size (bv))
    return bv->data[index];
  return bv->data[bv->size - 1];    
}

bf_formula *
bitvector_get_sign (const bitvector *bv)
{
  return bitvector_get_bit (bv, bv->size - 1);
}

void
bitvector_set_bit (const bitvector *bv, int index, bf_formula *bf)
{
  ccl_pre (0 <= index && index < bitvector_get_size (bv));

  bf_unref (bv->data[index]);
  bv->data[index] = bf;  
}

void
bitvector_add (bitvector *bv, bf_formula *bf)
{
  ccl_pre (bv != NULL);
  ccl_pre (bf != NULL);

  ccl_array_add (*bv, bf);
}

void
bitvector_log (ccl_log_type log, bf_manager *bm, bitvector *bv)
{
  int i;

  ccl_log (log, "[");
  for (i = 0; i < bv->size; i++)
    {
      ccl_log (log, " ");
      bf_log_formula (log, bm, bv->data[i]);
    }
  ccl_log (log, " ]");
}

int
bitvector_is_true (const bitvector *bv)
{
  ccl_pre (bv != NULL);
  ccl_pre (bitvector_get_size (bv) == 1);
  return bf_is_true (bitvector_get_bit (bv, 0));
}

int
bitvector_is_false (const bitvector *bv)
{
  ccl_pre (bv != NULL);
  ccl_pre (bitvector_get_size (bv) == 1);
  return bf_is_false (bitvector_get_bit (bv, 0));
}

int
bitvector_is_equal_to (const bitvector *bv, const bitvector *other)
{
  int i;
  int sz = bitvector_get_size (bv);
  int result = (sz == bitvector_get_size (other));

  for (i = 0; i < sz && result; i++)
    result = (bitvector_get_bit (bv, i) == bitvector_get_bit (other, i));

  return result;
}

int
bitvector_is_constant (const bitvector *bv)
{
  int i;
  int result = 1;
  int sz = bitvector_get_size (bv);

  for (i = 0; i < sz && result; i++)
    {
      bf_formula *bit = bitvector_get_bit (bv, i);
      result = (bf_is_true (bit) || bf_is_false (bit));
    }

  return result;
}

int
bitvector_to_integer (const bitvector *bv)
{
  int i;
  int result = 0;
  int sz = sizeof (int) * 8;

  ccl_pre (bitvector_is_constant (bv));

  for (i = 0; i < sz; i++)
    { 
      bf_formula *b = bitvector_get_extbit (bv, i);
      if (bf_is_true (b))
	result |= (1 << i);
    }

  return result;
}
