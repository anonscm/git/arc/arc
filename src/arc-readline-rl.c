/*
 * arc-readline-rl.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#else
# define HAVE_LIBREADLINE 0
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-string.h>
#include "arc-readline.h"

#if HAVE_LIBREADLINE

#ifdef HAVE_READLINE_H
# include <readline.h>
#endif /* HAVE_READLINE_H */

#ifdef HAVE_READLINE_READLINE_H
# include <readline/readline.h>
#endif /* HAVE_READLINE_READLINE_H */

#ifdef HAVE_HISTORY_H
# include <history.h>
#endif /* HAVE_HISTORY_H */

#ifdef HAVE_READLINE_HISTORY_H
# include <readline/history.h>
#endif /* HAVE_READLINE_HISTORY_H */

static char *history_filename = NULL;

static void 
s_ctrl_c_handler (int sig)
{
  if (sig != SIGINT)
    return;
  rl_crlf ();
  rl_free_line_state();
  rl_cleanup_after_signal();
  rl_reset_after_signal();
  rl_replace_line ("", 1);
  rl_forced_update_display ();
}

			/* --------------- */

void
arc_readline_init_rl (const char *hfilename)
{
  rl_initialize ();
#if HAVE_READLINE_HISTORY
  if( hfilename != NULL )
    {
      history_filename = ccl_string_dup(hfilename);
      using_history();
      read_history(history_filename);
    }
#endif /* HAVE_READLINE_HISTORY */
}

			/* --------------- */

void
arc_readline_terminate_rl (void)
{
#if HAVE_READLINE_HISTORY
  if( history_filename != NULL )
    {
      write_history(history_filename);
      ccl_string_delete(history_filename);
      history_filename = NULL;
    }
#endif /* HAVE_READLINE_HISTORY */
}

			/* --------------- */

char *
arc_readline_get_line_rl (const char *prompt)
{
  char *result = NULL;
  char *line;

  signal (SIGINT, s_ctrl_c_handler); 

  line = readline (prompt);

  if( line != NULL )
    {
      char  *p;
      
      for(p = line; *p && isspace(*p); p++)
	/* do nothing */;
#if HAVE_READLINE_HISTORY
      if( *p != '\0' )
	add_history(p);
#endif /* HAVE_READLINE_HISTORY */
      
      result = ccl_string_format_new ("%s\n", line);
      free (line); /* line is not allocated with ccl */
    }
  
  return result;
}

#endif /* HAVE_LIBREADLINE */
