/*
 * ar-acheck-builtins.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_ACHECK_BUILTINS_H
# define AR_ACHECK_BUILTINS_H

enum ar_acheck_builtin {
# define AR_ACHECK_BUILTIN(_enumid, _strid) AR_ACHECK_ ## _enumid,
# include "ar-acheck-builtins.def"
# undef AR_ACHECK_BUILTIN
  LAST_AND_UNUSED_AR_ACHECK_BUILTIN
};

extern const char *AR_ACHECK_BUILTIN_NAMES[];

# define AR_ACHECK_BUILTIN(_enumid, _strid) \
  extern const char *ACHECK_BUILTIN_  ## _enumid ## _STR;
# include "ar-acheck-builtins.def"
# undef AR_ACHECK_BUILTIN

#endif /* ! AR_ACHECK_BUILTINS_H */
