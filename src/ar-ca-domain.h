/*
 * ar-ca-domain.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_DOMAIN_H
# define AR_CA_DOMAIN_H

# include <ccl/ccl-log.h>
# include "ar-bounds.h"

struct ar_ca_expression_manager_st;

typedef struct ar_ca_domain_st ar_ca_domain;

extern ar_ca_domain *
ar_ca_domain_create_range(int min, int max);

extern ar_ca_domain *
ar_ca_domain_create_enum(const int *values, int size);

extern ar_ca_domain *
ar_ca_domain_create_bool(void);

extern ar_ca_domain *
ar_ca_domain_add_reference(ar_ca_domain *domain);

extern void
ar_ca_domain_del_reference(ar_ca_domain *domain);

extern int
ar_ca_domain_get_cardinality(const ar_ca_domain *domain);

extern int
ar_ca_domain_get_ith_value (const ar_ca_domain *domain, int i);

extern int
ar_ca_domain_get_value_index (const ar_ca_domain *domain, int value);

extern void
ar_ca_domain_get_bounds(const ar_ca_domain *domain, ar_bounds *bounds);

extern const int *
ar_ca_domain_get_enum_values(const ar_ca_domain *domain, int *psize);

extern int
ar_ca_domain_has_element(const ar_ca_domain *domain, int element);

extern int
ar_ca_domain_is_boolean(const ar_ca_domain *domain);

extern int
ar_ca_domain_is_enum(const ar_ca_domain *domain);

extern int
ar_ca_domain_is_integer(const ar_ca_domain *domain);

extern uint32_t
ar_ca_domain_hash(const ar_ca_domain *domain);

extern int
ar_ca_domain_equals(const ar_ca_domain *domain, const ar_ca_domain *other);

extern void
ar_ca_domain_log (ccl_log_type log, const ar_ca_domain *domain,
		  struct ar_ca_expression_manager_st *man);

extern void
ar_ca_domain_log_gen (ccl_log_type log, const ar_ca_domain *domain,
		      struct ar_ca_expression_manager_st *man, 
		      const char *separator, const char *quotes);

extern char *
ar_ca_domain_value_to_string (const ar_ca_domain *dom, int value,
			      struct ar_ca_expression_manager_st *man);

extern void
ar_ca_domain_log_value (ccl_log_type log, const ar_ca_domain *dom, int value,
			struct ar_ca_expression_manager_st *man);

extern void
ar_ca_domain_log_value_gen (ccl_log_type log, const ar_ca_domain *dom, 
			    int value, struct ar_ca_expression_manager_st *man, 
			    const char *separator, const char *quotes);

#endif /* ! AR_CA_DOMAIN_H */
