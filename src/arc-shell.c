/*
 * arc-shell.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include "parser/altarica-input.h"
#include "ar-interp-altarica.h"
#include "ar-interp-acheck.h"
#include "mec5/mec5-interp-ast.h"
#include "lustre/translators.h"
#include "mec5/mec5-serialization.h"
#include "arc-readline.h"
#include "commands/allcmds.h"
#include "arc-shell.h"
#include "arc-shell-preferences.h"
#include "ar-util.h"
#include "arc.h"

arc_shell_context ARC_SHELL_CURRENT_CONTEXT;

			/* --------------- */

const char *SHELL_PREFS[] = {
  ARC_SHELL_VERBOSE,  ARC_SHELL_VERBOSE_DEFAULT,
  ARC_SHELL_PROMPT_0, ARC_SHELL_PROMPT_0_DEFAULT,
  ARC_SHELL_PROMPT_1, ARC_SHELL_PROMPT_1_DEFAULT,
  ARC_SHELL_PROMPT_EVAL, ARC_SHELL_PROMPT_EVAL_DEFAULT,
  ARC_SHELL_HISTORY_FILE, ARC_SHELL_HISTORY_FILE_DEFAULT,
  ARC_SHELL_CHECK_CARD_ABORT, ARC_SHELL_CHECK_CARD_ABORT_DEFAULT,
  ARC_SHELL_RUSAGE_AT_EXIT,   ARC_SHELL_RUSAGE_AT_EXIT_DEFAULT,
  NULL
};

			/* --------------- */

void
arc_shell_init (void)
{
  const char **pp;

  for(pp = SHELL_PREFS; *pp; pp += 2)
    if (pp[1] != NULL)
      ccl_config_table_set_if_null(ARC_PREFERENCES,pp[0],pp[1]);

  ARC_SHELL_CURRENT_CONTEXT = NULL;
  arc_command_init();
  arc_register_commands();
}

			/* --------------- */

void
arc_shell_terminate(void)
{
  arc_command_terminate();
}

			/* --------------- */

static const char *
s_get_extension(const char *filename)
{
  const char *s = filename+strlen(filename);

  while( s != filename )
    {
      if( *s == '.' )
	{
	  s++;
	  break;
	}
      s--;
    }

  return s;
}

			/* --------------- */

static int
s_file_exist(const char *filename)
{
  FILE *f = fopen(filename,"r");
  if( f == NULL )
    return 0;

  fclose(f);

  return 1;
}

			/* --------------- */

static int
s_interp_altarica_tree (altarica_tree *t)
{
  int result = 1;

  ccl_pre (t != NULL);

  for (; t != NULL && result; t = t->next)
    {
      switch (t->node_type) 
	{
	case AR_TREE_ALTARICA : 
	  result = ar_interp_altarica_tree (t->child);
	  break;
	case AR_TREE_MECV :
	  result = ar_interp_mec5_tree (t->child, ARC_PREFERENCES);
	  break;

	case AR_TREE_ACHECK :
	  result = ar_interp_acheck_tree (t->child, ARC_PREFERENCES);
	  break;

	case AR_TREE_ARC :
	  arc_shell_interp(t->child);
	  break;

	default:
	  ccl_throw_no_msg (internal_error);
	  break;
	}
    }

  return result;
}

			/* --------------- */

static int
s_is_cmd (const char *id, void *data)
{
  return (arc_command_find (id) != NULL);
}

			/* --------------- */

static const char *
s_look_for_preprocessor (const char *ext, const char **p_args)
{  
  char *pref = ccl_string_format_new (ARC_SHELL_PREPROCESSOR_COMMAND, ext);
  const char *cmd = ccl_config_table_get (ARC_PREFERENCES, pref);

  ccl_string_delete (pref);
  if (cmd == NULL)
    {
      ext = "default";
      pref = ccl_string_format_new (ARC_SHELL_PREPROCESSOR_COMMAND, ext);
      cmd = ccl_config_table_get (ARC_PREFERENCES, pref);
      ccl_string_delete (pref);
    }

  if (cmd != NULL)
    {
      pref = ccl_string_format_new (ARC_SHELL_PREPROCESSOR_ARGS, ext);
      *p_args = ccl_config_table_get (ARC_PREFERENCES, pref);
      ccl_string_delete (pref);      
    }

  return cmd;
}

			/* --------------- */

static void
s_exec_pp_command (const char *command, const char *tmpfilename)
{
  char *tmp = NULL;

  ccl_string_format_append (&tmp, "%s > %s", command, tmpfilename);
  system (tmp);
  ccl_string_delete (tmp);
}

altarica_tree *
arc_shell_load_preprocessed_file (const char *filename, int modes)
{
  const char *ext = s_get_extension (filename);
  altarica_tree *t = NULL;
  const char *pp_args = NULL;
  const char *pp_cmd = s_look_for_preprocessor (ext, &pp_args);
  
  if (pp_cmd == NULL)
    t = altarica_read_file (filename, modes, s_is_cmd, NULL);
  else
    {
      char *command;
      char *tmpl = ccl_string_dup ("arctmpfile.XXXXXX");
      char *tmpfilename = mktemp (tmpl);
      char *s = strrchr (pp_cmd, '%');

      if (s != NULL && s[1] == 's')
	command = ccl_string_format_new (pp_cmd, filename);
      else 
	command = ccl_string_format_new ("%s \"%s\"", pp_cmd, filename);
      
      if (pp_args != NULL)
	ccl_string_format_append (&command," %s", pp_args);
      
      s_exec_pp_command (command, tmpfilename);
      t = altarica_read_file (tmpfilename, modes, s_is_cmd, NULL);
      unlink (tmpfilename);
      ccl_string_delete (command);
      ccl_string_delete (tmpl);
    }

  return t;
}

			/* --------------- */

int
arc_shell_load_file (const char *fname, int modes)
{
  char *filename = arc_shell_find_file_in_path (fname);
  int result = (filename != NULL);

  if (result)
    {
      const char *ext = s_get_extension (filename);
      
      ccl_warning ("Loading file '%s'.\n", filename);

      if ((modes & AR_INPUT_ARC) != 0 && ccl_string_equals (ext, "arc"))
	result = arc_shell_load_script_file (filename);
      else if (ccl_string_equals (ext, "lus"))
	result = ar_lustre_to_altarica (filename, ARC_PREFERENCES);
      else if (ccl_string_equals (ext, "rel"))
	result = ar_mec5_load_relation (filename, ARC_PREFERENCES);
      else 
	{
	  altarica_tree *t =
	    arc_shell_load_preprocessed_file (filename, modes);
	  if( t != NULL ) 
	    {
	      result = s_interp_altarica_tree(t);
	      ccl_parse_tree_delete_tree(t);
	    }
	  else
	    {
	      result = 0;
	    }
	}
      ccl_string_delete (filename);
    }
  else
    {
      ccl_error("the file '%s' is not readable.\n", fname);
    }

  return result;
}

			/* --------------- */

static char * 
s_check_file_in_dir (const char *dirname, const char *filename)
{
  char *result = ccl_string_format_new ("%s/%s", dirname, filename);
  if (! s_file_exist (result))
    {
      ccl_string_delete (result);
      result = NULL;
    }
  return result;
}

			/* --------------- */

char *
arc_shell_find_file_in_path (const char *filename)
{
  char *result = NULL;
  char *path = getenv (ENV_ARCPATH);

  if (s_file_exist (filename))
    result = ccl_string_dup (filename);
  else if (path != NULL && strchr (filename, '/') == NULL)
    {
      int stop = 0;
      char *p = path = ccl_string_dup (path);

      while (! stop)
	{
	  char *pe = strchr (p, ENV_ARCPATH_SEPCHAR);

	  if (pe != NULL)
	    *pe = '\0';
	  result = s_check_file_in_dir (p, filename);
	  
	  if (pe == NULL || result != NULL)
	    stop = 1;
	  else
	    p = pe + 1;
	}
      ccl_string_delete (path);
    }

  return result;
}
