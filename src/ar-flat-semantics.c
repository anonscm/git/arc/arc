/*
 * ar-flat-semantics.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include "ar-interp-altarica.h"
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-semantics.h"
#include "ar-flat-semantics.h"

#define QVARS_PREFIX "$$"


#define DBG_IS_ON (ccl_debug_is_on && ar_flattening_debug_is_on)

#define DBG_FLATTENING(flag_) (DBG_IS_ON && ar_flattening_debug_ ## flag_)
#define DBG_START_TIMER(args_) \
  CCL_DEBUG_COND_START_TIMED_BLOCK (DBG_IS_ON, args_)
#define DBG_END_TIMER() \
  CCL_DEBUG_COND_END_TIMED_BLOCK (DBG_IS_ON)

			/* --------------- */

int ar_flattening_debug_is_on = 0;
int ar_flattening_debug_show_fd = 1;
int ar_flattening_debug_log_vectors = 1;

			/* --------------- */

static ar_expr * 
s_simplify (ar_expr *e, ar_context *ctx)
{  
  ar_expr *ne = ar_expr_remove_composite_slots (e, ctx);
  ar_expr *result;
  if (ar_expr_is_simplifiable (e))
    result = ar_expr_simplify (ne);
  else
    result = ar_expr_add_reference (ne);
  ar_expr_del_reference (ne);
  return result;
}

static ccl_list *
s_intersect_slot_lists(ccl_list *l1, ccl_list *l2)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create();

  for(p = FIRST(l1); p; p = CDR(p))
    if( ccl_list_has(l2,CAR(p)) )
      ccl_list_add(result,ar_context_slot_add_reference(CAR(p)));

  for(p = FIRST(l2); p; p = CDR(p))
    if( ccl_list_has(l1,CAR(p)) && ! ccl_list_has(result,CAR(p)) )
      ccl_list_add(result,ar_context_slot_add_reference(CAR(p)));

  return result;
}

			/* --------------- */

static ar_expr *
s_quantify_constraints(ar_context *ctx,
		       int index, int nb_constraints, ar_expr **constraints, 
		       ccl_list **varlists, int *ordering, ccl_list *qvars)
{
  ccl_pair *p;
  ar_expr *result;
  int c = ordering[index];
  ccl_list *vars = NULL;
  ar_context *qctx = NULL;

  if( ccl_list_get_size(varlists[c]) == 0 )
    {
      if( index == nb_constraints-1 ) 
	return ar_expr_crt_true();
      else
	return s_quantify_constraints(ctx,index+1,nb_constraints,constraints,
				      varlists,ordering,qvars);
    }

  if( ccl_list_get_size(qvars) > 0 )
    {
      vars = ccl_list_create();
      qctx = ar_context_create(ctx);

      for(p = FIRST(varlists[c]); p; p = CDR(p))
	{
	  ar_context_slot *sl = (ar_context_slot *)CAR(p);
	  
	  if( ccl_list_has(qvars,sl) )
	    {
	      ccl_list *sls = ar_context_slot_expand_slot (-1, sl, NULL);
	      
	      while (! ccl_list_is_empty (sls))
		{
		  ar_context_slot *csl = ccl_list_take_first (sls);
		  ar_identifier *id = ar_context_slot_get_name (csl);
		  
		  ar_context_add_slot (qctx, id, csl);

		  ar_identifier_del_reference (id);
		  ar_context_slot_del_reference (csl);
		}
	      ccl_list_delete (sls);
	      ccl_list_add (vars,sl);
	    }
	}
      
      for(p = FIRST(vars); p; p = CDR(p))
	{
	  ar_context_slot *sl = (ar_context_slot *)CAR(p);
	  CAR(p) = ar_expr_crt_variable(sl);
	  ccl_list_remove(qvars,sl);
	  ar_context_slot_del_reference(sl);
	}
    }
  else
    {
      qctx = ar_context_add_reference(ctx);      
    }

  if( index == nb_constraints-1 )
    result = ar_expr_add_reference(constraints[c]);
  else
    {
      ar_expr *tmp = 
	s_quantify_constraints(qctx,index+1,nb_constraints,constraints,
			       varlists,ordering,qvars);
      result = ar_expr_crt_binary(AR_EXPR_AND,constraints[c],tmp);
      ar_expr_del_reference(tmp);
    }

  if( vars != NULL )
    {
      if( ! ccl_list_is_empty(vars) ) 
	{
	  ar_expr *tmp = 
	    ar_expr_crt_quantifier(AR_EXPR_EXIST,qctx,vars,result);
	  ar_expr_del_reference(result);
	  result = tmp;
	}
      ccl_list_clear_and_delete(vars,(ccl_delete_proc *)ar_expr_del_reference);
    }
  ar_context_del_reference(qctx);

  return result;
}

			/* --------------- */

static ar_expr *
s_compute_enabling_condition(ar_context *ctx, ar_trans *t, ccl_list *assertions,
			     ccl_hash *subst, ccl_list *qfvars)
{
  ccl_pair *p;
  const ar_trans_assignment *a;
  ar_expr *result = ar_trans_get_guard(t);
  ccl_hash *s_tables[2];
  int i, nb_constraints; 
  ar_expr **constraints;
  ccl_list **varlists;
  int *ordering;

  s_tables[0] = subst;
  s_tables[1] = ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)
			       ar_expr_del_reference);

  for(a = ar_trans_get_assignments(t); a; a = a->next)
    {
      ar_context_slot *sl;
      ar_type *vtype = ar_expr_get_type(a->lvalue);
      ar_expr *c = ar_expr_crt_in_domain(a->value,vtype);
      ar_expr *tmp = s_simplify (c, ctx);

      if (ar_expr_is_true (tmp))
	{
	  ar_expr_del_reference (tmp);
	  tmp = ar_expr_add_reference (result);
	}
      else
	{
	  ar_expr_del_reference (tmp);
	  tmp = ar_expr_crt_binary (AR_EXPR_AND, result, c);
	}

      ar_type_del_reference(vtype);
      ar_expr_del_reference(result);
      ar_expr_del_reference(c);
      result = tmp;

      sl = ar_expr_get_slot (a->lvalue);
      ccl_assert( ! ccl_hash_find(s_tables[1], sl) );

      ccl_hash_find(s_tables[1], sl);
      ccl_hash_insert(s_tables[1],ar_expr_add_reference(a->value));
      ar_context_slot_del_reference (sl);
    }

  if( (nb_constraints = ccl_list_get_size(assertions)) == 0 )
    {
      ccl_hash_delete(s_tables[1]);
      goto simplify;
    }

  constraints = ccl_new_array(ar_expr *,nb_constraints);
  varlists = ccl_new_array(ccl_list *,nb_constraints);
  ordering = ccl_new_array(int,nb_constraints);

  {
    ccl_hash *cache = 
      ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
		       ar_expr_del_reference);
				       
    for(i = 0, p = FIRST(assertions); p; p = CDR(p), i++)
      {
	int j, k;
	ccl_list *tmp;
	int nbvars;
	ar_expr *a = (ar_expr *)CAR(p);

	constraints[i] =  ar_expr_replace_with_tables(a,ctx, s_tables,2, cache);

	tmp = ar_expr_get_variables(constraints[i],ctx,NULL);      
	varlists[i] = s_intersect_slot_lists(tmp,qfvars);
	ccl_list_clear_and_delete(tmp,(ccl_delete_proc *)
				  ar_context_slot_del_reference);
	nbvars = ccl_list_get_size(varlists[i]);

	if( nbvars == 0 )
	  {
	    ar_expr *tmp = ar_expr_crt_binary (AR_EXPR_AND, result, 
					       constraints[i]);
	    ar_expr_del_reference(result);
	    result = tmp;
	  }

	for(j = 0; j < i; j++)
	  {
	    if( nbvars < ccl_list_get_size(varlists[ordering[j]]) )
	      break;
	  }

	if( j < i )
	  {
	    for(k = i; k >= j+1; k--)
	      ordering[k] = ordering[k-1];
	  }
	ordering[j] = i;
      }
    ccl_hash_delete (cache);
  }
  ccl_hash_delete(s_tables[1]);
  {
    ccl_list *qvars = ccl_list_deep_dup(qfvars,(ccl_duplicate_func *)
				       ar_context_slot_add_reference);
    ar_expr *qf = s_quantify_constraints(ctx,
					 0,nb_constraints,constraints,varlists,
					 ordering,qvars);
    ar_expr *tmp = ar_expr_crt_binary(AR_EXPR_AND,result,qf);
    ar_expr_del_reference(qf);
    ar_expr_del_reference(result);
    ccl_assert( ccl_list_get_size(qvars) == 0 );
    ccl_list_delete(qvars);

    result = tmp;
  }

  for(i = 0; i < nb_constraints; i++)
    {
      ar_expr_del_reference(constraints[i]);
      ccl_list_clear_and_delete(varlists[i],(ccl_delete_proc *)
				ar_context_slot_del_reference);
    }
  ccl_delete(constraints);
  ccl_delete(varlists);
  ccl_delete(ordering);
 simplify:
  if(0)  {
    ar_expr *tmp = s_simplify (result, ctx);
    ar_expr_del_reference (result);
    result = tmp;
  }

  return result;
}

			/* --------------- */

static ccl_list *
s_compute_quantified_flow_vars(ar_context *ctx, ccl_list *assertions, 
			       ccl_hash **psubst)
{
  ccl_pair *p;
  ccl_list *vars = ccl_list_create();
  ccl_list *result = ccl_list_create();

  *psubst = ccl_hash_create(NULL,NULL,
			    (ccl_delete_proc *)ar_context_slot_del_reference,
			    (ccl_delete_proc *)ar_expr_del_reference);

  for(p = FIRST(assertions); p; p = CDR(p))
    vars = ar_expr_get_variables((ar_expr *)CAR(p),ctx,vars);

  while( ! ccl_list_is_empty(vars) )
    {
      ar_context_slot *slot = (ar_context_slot *)ccl_list_take_first(vars);

      if( (ar_context_slot_get_flags(slot) & AR_SLOT_FLAG_FLOW_VAR) != 0 )
	{
	  ar_identifier *id = ar_context_slot_get_name(slot);
	  ar_identifier *nid = ar_identifier_rename_with_prefix(id,
								QVARS_PREFIX);
	  ar_context_slot *nslot = ar_context_slot_rename(slot,nid);
	  ar_expr *nvar = ar_expr_crt_variable(nslot);

	  ccl_assert( ! ccl_hash_find(*psubst,slot) );

	  ccl_list_add(result,ar_context_slot_add_reference(nslot));
	  ccl_hash_find(*psubst,ar_context_slot_add_reference(slot));
	  ccl_hash_insert(*psubst,ar_expr_add_reference(nvar));

	  ar_identifier_del_reference(id);
	  ar_identifier_del_reference(nid);
	  ar_expr_del_reference(nvar);
	  ar_context_slot_del_reference(nslot);
	}

      ar_context_slot_del_reference(slot);
    }
  ccl_list_delete(vars);

  return result;
}

			/* --------------- */

#if 0
static ccl_hash *
s_compute_vt(ar_node *node, ar_event_poset *order)
{
  ccl_pair *p;
  ar_context *ctx = ar_node_get_context(node);
  ccl_list *trans = ar_node_get_transitions(node);
  ccl_list *assertions = ar_node_get_assertions(node);
  ccl_hash *subst = NULL;
  ccl_list *qfvars = s_compute_quantified_flow_vars(ctx,assertions,&subst);
  ccl_hash *result = ccl_hash_create(NULL,NULL,
				    (ccl_delete_proc *)ar_event_del_reference,
				    (ccl_delete_proc *)ar_expr_del_reference);
  
  for(p = FIRST(trans); p; p = CDR(p))
    {
      ar_trans *t = (ar_trans *)CAR(p);
      ar_event *ev = ar_trans_get_event(t);
      ar_expr *ect = 
	s_compute_enabling_condition(ctx,t,assertions,subst,qfvars);

      if( ccl_hash_find(result,ev) )
	{
	  ar_expr *current_vt = ccl_hash_get(result);
	  ar_expr *new_vt = ar_expr_crt_binary(AR_EXPR_OR,current_vt,ect);
	  ccl_hash_insert(result,new_vt);
	  ar_expr_del_reference(current_vt);
	}
      else
	{
	  ar_event_add_reference(ev);
	  ccl_hash_insert(result,ar_expr_add_reference(ect));
	}
      ar_expr_del_reference(ect);
      ar_event_del_reference(ev);
    }
  
  ar_context_del_reference(ctx);
  ccl_hash_delete(subst);
  ccl_list_clear_and_delete(qfvars,(ccl_delete_proc *)ar_expr_del_reference);

  return result;
}
#endif
/* 
 * check if the assertion "a" has the form f = val or val = f, where val any 
 * expression except a flow variable if allow_flow_var is 0. If this is the 
 * case it assigns f and val accordingly 
 * and, in the case where f and val have distinct domains, the function assigns
 * to *domain the constraint val\in dom(f).
 */
static int
s_is_f_eq_val (ar_context *ctx, ar_expr *a, ar_context_slot **f, ar_expr **val, 
	       ar_expr **domain, int allow_flow_var)
{
  int i;
  int result;
  ar_expr **args;

  if (ar_expr_get_op (a) != AR_EXPR_EQ)      
    return 0;

  result = 0;
  args = ar_expr_get_args (a, NULL);

  for (i = 0; i < 2 && !result; i++)
    {
      ccl_list *vars;

      if (ar_expr_get_op (args[i]) != AR_EXPR_VAR)
	continue;

      *f = ar_expr_get_slot (args[i]);

      if (! (ar_context_slot_get_flags (*f) & AR_SLOT_FLAG_FLOW_VAR))
	{
	  ar_context_slot_del_reference (*f);
	  continue;
	}

      *val = args[(i + 1) % 2];      
      if (! allow_flow_var && ar_expr_get_op (*val) == AR_EXPR_VAR)
	{ 
	  ar_context_slot *sl2 = ar_expr_get_slot (*val);
	  if ((ar_context_slot_get_flags (sl2) & AR_SLOT_FLAG_FLOW_VAR))
	    {
	      ar_context_slot_del_reference (sl2);
	      ar_context_slot_del_reference (*f);
	      continue;
	    }
	  ar_context_slot_del_reference (sl2);
	}

      *val = ar_expr_add_reference (*val);
      vars = ar_expr_get_slots (*val, ctx, NULL);
      result = ! ccl_list_has (vars, *f);
      if (result)
	{
	  ar_type *sltype = ar_context_slot_get_type (*f);
	  ar_type *valtype = ar_expr_get_type (*val);

	  *domain = NULL;
	  if (! ar_type_equals (sltype, valtype))
	    {
	      ar_expr *tmp = ar_expr_crt_in_domain (*val, sltype);
	      ar_expr *c = s_simplify (tmp, ctx);
	      ar_expr_del_reference (tmp);
	      
	      if (! ar_expr_is_true (c))
		*domain = c;
	      else
		ar_expr_del_reference (c);
	    }
	  ar_type_del_reference (sltype);
	  ar_type_del_reference (valtype);
	}
      else
	{
	  ar_context_slot_del_reference (*f);
	  ar_expr_del_reference (*val);
	}
      ccl_list_clear_and_delete (vars, (ccl_delete_proc *) 
				 ar_context_slot_del_reference);
    }

  return result;
}

			/* --------------- */

static ccl_list *
s_actual_assertions (ar_context *ctx, ccl_list *assertions)
{
  ccl_pair *p;
  ccl_list *A = ccl_list_create ();
  ccl_list *result;
  int allow_flow_var;

  if (DBG_IS_ON)
    ccl_debug_push_tab_level ();

  for (p = FIRST (assertions); p; p = CDR (p))
    A = ar_expr_expand_composite_types (CAR (p), ctx, A);

  for (allow_flow_var = 0; allow_flow_var < 2; allow_flow_var++)
    {
      int Asize = ccl_list_get_size (A);

      while (Asize)
	{
	  ar_expr *a = ccl_list_take_first (A);
	  ar_context_slot *sl;
	  ar_expr *tmp[2] = { NULL, NULL};
	  
	  if (DBG_FLATTENING (show_fd))
	    {
	      ccl_debug ("checking FD for ");
	      ar_expr_log (CCL_LOG_DEBUG, a);	  
	      ccl_debug ("\n");
	    }

	  if (s_is_f_eq_val (ctx, a, &sl, tmp, tmp + 1, allow_flow_var))
	    {
	      ccl_pair *p;
	      int changed = 0;
     
	      if (DBG_FLATTENING (show_fd))
		{
		  ar_identifier *id = ar_context_slot_get_name (sl);
		  ccl_debug ("FD found ");
		  ar_identifier_log (CCL_LOG_DEBUG, id);	  
		  ar_identifier_del_reference (id);
		  ccl_debug (" := ");
		  ar_expr_log (CCL_LOG_DEBUG, tmp[0]);
		  ccl_debug ("\n");
		}
	      
	      for (p = FIRST (A); p; p = CDR (p))
		{
		  ar_expr *aux = ar_expr_replace (CAR (p), sl, tmp[0]);
		  if (aux != CAR (p))
		    changed++;
		  ar_expr_del_reference (CAR (p));
		  CAR (p) = aux;
		}
	      ar_context_slot_del_reference (sl);
	      ar_expr_del_reference (tmp[0]);

	      if (tmp[1] != NULL)
		{
		  if (DBG_FLATTENING (show_fd))
		    {
		      ccl_debug ("adding constraint ");
		      ar_expr_log (CCL_LOG_DEBUG, tmp[1]);
		      ccl_debug ("\n");
		    }
	      
		  ccl_list_add (A, tmp[1]);
		  changed = 1;
		}
	      if (changed)
		{
		  if (DBG_FLATTENING (show_fd))
		    ccl_debug ("%d assertions have changed\n", changed);
		  Asize = ccl_list_get_size (A);
		}
	      else
		Asize--;
	    }
	  else
	    {
	      if (DBG_FLATTENING (show_fd))
		ccl_debug ("assertion postponed\n");
	      ccl_list_add (A, ar_expr_add_reference (a));
	      Asize--;
	    }
	  ar_expr_del_reference (a);
	}
    }
  if (DBG_IS_ON)
    ccl_debug_pop_tab_level ();

  result = A;

  return result;
}

			/* --------------- */

static ccl_list *
s_apply_transition_order(ar_context *ctx, ccl_list *trans, ar_poset **orders, 
			 int nb_orders, ccl_list *model_assertions)
{
  ccl_list *result;
  ccl_hash *new_guards = ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)
					ar_expr_del_reference);
  ccl_hash *ect = ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)
				 ar_expr_del_reference);
  ccl_list *assertions = s_actual_assertions (ctx, model_assertions);
  if (DBG_IS_ON)
    {
      ccl_debug ("# Actual assertions = %d / %d \n", 
		 ccl_list_get_size (assertions), 
		 ccl_list_get_size (model_assertions));
    }
  
  /* 1 - compute the table of enabling conditions (ect) */
  {
    ccl_pair *p;
    ccl_hash *subst;
    ccl_list *qfvars = s_compute_quantified_flow_vars(ctx,assertions,&subst);
      
    for(p = FIRST(trans); p; p = CDR(p))
      {
	ar_trans *T = (ar_trans *)CAR(p);
	ar_expr *g_and_post = 
	  s_compute_enabling_condition(ctx,T,assertions,subst,qfvars);
	  
	ccl_hash_find(ect,T);
	ccl_hash_insert(ect,g_and_post);
	  
	ccl_hash_find(new_guards,T);
	ccl_hash_insert(new_guards,ar_trans_get_guard(T));
      }
    ccl_hash_delete(subst);
    ccl_list_clear_and_delete(qfvars, (ccl_delete_proc *) 
			      ar_context_slot_del_reference);
  }

  /* 2 - apply orders */
  {
    int conds_size = 2;
    ar_expr **conds = ccl_new_array(ar_expr *,conds_size);

    while( nb_orders-- )
      {
	ccl_pair *p;
	ar_poset *order = *orders++;
      
	for(p = FIRST(trans); p; p = CDR(p))
	  {
	    ar_trans *T = (ar_trans *)CAR(p);
	    ccl_list *gt_T = ar_poset_get_greater_objects(order,T);
	    
	    if( ! ccl_list_is_empty(gt_T) )
	      {
		int i;
		ccl_pair *pT;
		int nb_conds = ccl_list_get_size(gt_T)+1;

		if( nb_conds >= conds_size )
		  {
		    conds = ccl_realloc(conds,sizeof(ar_expr *)*nb_conds);
		    conds_size = nb_conds;
		  }

		ccl_hash_find(new_guards,T);
		conds[0] = ar_expr_add_reference(ccl_hash_get(new_guards));
		for(i = 1, pT = FIRST(gt_T); pT; pT = CDR(pT), i++)
		  {
		    ccl_hash_find (ect, CAR(pT));
		    conds[i] = ccl_hash_get (ect);
		    conds[i] = ar_expr_crt_unary (AR_EXPR_NOT, conds[i]);
		  }
		
		ccl_hash_find(new_guards,T);
		ccl_hash_insert(new_guards, ar_expr_crt_and (nb_conds, conds));

		while (i--)
		  ar_expr_del_reference(conds[i]);
	      }
	    
	    ccl_list_delete(gt_T);
	  }
      }
    ccl_delete(conds);
    ccl_hash_delete(ect);
  }

  /* 3 - duplication of original transitions and replacement of guards by
     new ones. */
  {
    ccl_pair *p;

    result = ccl_list_create();
    for(p = FIRST(trans); p; p = CDR(p))
      {
	ar_trans *T = (ar_trans *)CAR(p);
	ar_trans *newT = ar_trans_dup(T);

	ccl_hash_find(new_guards,T);
	ar_trans_set_guard (newT, ccl_hash_get (new_guards));
	ccl_list_add(result,newT);
      }
    ccl_hash_delete(new_guards);
  }

  ccl_list_clear_and_delete (assertions, (ccl_delete_proc *)
			     ar_expr_del_reference);

  return result;
}

			/* --------------- */

static void
s_delete_transition_list(void *tl)
{
  ccl_list_clear_and_delete(tl,(ccl_delete_proc *)ar_trans_del_reference);
}

			/* --------------- */

static ar_node *
s_apply_event_order(ar_node *node)
{
  ccl_pair *p;
  ccl_list *new_trans;
  ar_event_poset *order = ar_node_get_event_poset(node);
  ar_node *result = ar_node_create_reduced(node);
  ccl_list *trans = ar_node_get_transitions(result);
  ccl_list *assertions = ar_node_get_assertions(result);
  ar_poset *torder = ar_poset_create (NULL, NULL, NULL, NULL);
  ar_context *ctx = ar_node_get_context(result);
  ccl_hash *ev2trans = 
    ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)ccl_list_delete);

  for(p = FIRST(trans); p; p = CDR(p))
    {
      ar_trans *T = (ar_trans *)CAR(p);
      ar_event *ev = ar_trans_get_event(T);
      ccl_list *ev_trans;

      if( ccl_hash_find(ev2trans,ev) )
	ev_trans = (ccl_list *)ccl_hash_get(ev2trans);
      else
	{
	  ev_trans = ccl_list_create();
	  ccl_hash_insert(ev2trans,ev_trans);
	}

      ccl_list_add(ev_trans,T);
      ar_event_del_reference(ev);
    }
	
  for(p = FIRST(trans); p; p = CDR(p))
    {
      ccl_pair *pev;
      ar_trans *T = (ar_trans *)CAR(p);
      ar_event *ev = ar_trans_get_event(T);
      ccl_list *gt_ev = ar_event_poset_get_greater_events(order,ev);
      
      ar_poset_add(torder,T);
      for(pev = FIRST(gt_ev); pev; pev = CDR(pev))
	{
	  if( ccl_hash_find(ev2trans,CAR(pev)) )
	    {
	      ccl_pair *p_gt;
	      ccl_list *tlist = (ccl_list *)ccl_hash_get(ev2trans);
	      
	      for(p_gt = FIRST(tlist); p_gt; p_gt = CDR(p_gt))
		ar_poset_add_pair_no_check (torder,CAR(p_gt),T);
	    }
	}
      ar_event_del_reference(ev);
      ccl_list_delete(gt_ev);
    }
  ccl_hash_delete(ev2trans);

  new_trans = s_apply_transition_order(ctx,trans,&torder,1,assertions);

  ccl_list_clear(trans,(ccl_delete_proc *)ar_trans_del_reference);
  ccl_list_append(trans,new_trans);
  ccl_list_delete(new_trans);
  ar_event_poset_del_reference(order);
  ar_poset_del_reference(torder);
  ar_context_del_reference(ctx);

  return result;
}

			/* --------------- */

struct cond_law_data
{
  ar_context *ctx;
  ccl_hash *sl2expr;
  ccl_hash *cache;
};

static void *
s_instantiate_cond_data (void *cond_data, ar_identifier *prefix, void *cbdata)
{
  struct cond_law_data *cld = cbdata;
  ar_expr *cond = cond_data;
  ar_expr *result;
  
  if (cld->sl2expr)
    result = ar_expr_replace_with_tables (cond, cld->ctx, &cld->sl2expr, 1,
					  cld->cache);
  else 
    result = ar_expr_replace_with_context (cond, cld->ctx);
  
  return result;
}

static ar_identifier *
s_instantiate_id (ar_identifier *id, ar_identifier *subname)
{
  if (subname != NULL)
    return ar_identifier_add_prefix (id, subname);
  else
    return ar_identifier_add_reference (id);
}

static void
s_flatten_law_param (ar_identifier *lpid, ar_node *submodel,
		     ar_identifier *subname, ar_identifier **p_newlpid,
		     sas_law_param **p_newlp)
{
  sas_law_param *lp = ar_node_get_law_parameter (submodel, lpid);

  *p_newlpid = s_instantiate_id (lpid, subname);
  if (subname != NULL)
    *p_newlp = sas_law_param_instantiate (lp, subname);
  else
    *p_newlp = sas_law_param_add_reference (lp);
  sas_law_param_del_reference (lp);
}

static void
s_add_law_parameters (ar_node *result, ar_identifier *subname,
		      ar_node *submodel)
{
  ccl_pair *p;    
  const ccl_list *law_parameters = ar_node_get_ids_of_law_parameters (submodel);
    
  for (p = FIRST (law_parameters); p; p = CDR (p))
    {
      sas_law_param *newlp;
      ar_identifier *lpid = CAR (p);
      sas_law_param *lp = ar_node_get_law_parameter (submodel, lpid);
      ar_identifier *newlpid;

      s_flatten_law_param (lpid, submodel, subname, &newlpid, &newlp);

      ar_node_add_law_parameter (result, newlpid, newlp);
      sas_law_param_del_reference (newlp);	    
      ar_identifier_del_reference (newlpid);
      sas_law_param_del_reference (lp);
    }
}

static void
s_add_laws (ar_node *result, ar_identifier *subname, ar_node *submodel,
	    ar_context *ctx, ccl_hash *sl2expr, ccl_hash *cache)
{
  ccl_pair *p;
  const ccl_list *laws = ar_node_get_ids_of_laws (submodel);
  struct cond_law_data cld = { ctx, sl2expr, cache };
  
  for (p = FIRST (laws); p; p = CDR (p))
    {
      ar_identifier *lid = CAR (p);
      sas_law *l = ar_node_get_law (submodel, lid);
      ar_identifier *newlid;
      sas_law *newl;

      newlid = s_instantiate_id (lid, subname);
      if (subname != NULL)
	newl = sas_law_instantiate (l, subname, s_instantiate_cond_data, &cld);
      else
	newl = sas_law_add_reference (l);

      ar_node_add_law (result, newlid, newl);
      sas_law_del_reference (newl);	    
      ar_identifier_del_reference (newlid);
      sas_law_del_reference (l);
    }
}

static void
s_add_observers (ar_node *result, ar_identifier *subname, ar_node *submodel,
		 ar_context *ctx, ccl_hash *sl2expr, ccl_hash *cache)
{
  ccl_pair *p;
  const ccl_list *observers = ar_node_get_ids_of_observers (submodel);
  
  for (p = FIRST (observers); p; p = CDR (p))
    {
      ar_identifier *obsid = CAR (p);
      ar_expr *obs = ar_node_get_observer (submodel, obsid);
      ar_identifier *newobsid = s_instantiate_id (obsid, subname);
      ar_expr *newobs;
      
      

      if (sl2expr)
	newobs = ar_expr_replace_with_tables (obs, ctx, &sl2expr, 1, cache);
      else 
	newobs= ar_expr_replace_with_context (obs, ctx);
      
      ar_node_add_observer (result, newobsid, newobs);
      ar_expr_del_reference (newobs);	    
      ar_identifier_del_reference (newobsid);
      ar_expr_del_reference (obs);
    }
}

static void
s_add_preemptibles (ar_node *result, ar_identifier *subname, ar_node *submodel)
{
  ccl_pointer_iterator *pi = ar_node_get_preemptibles (submodel);
  
  while (ccl_iterator_has_more_elements (pi))
    {
      ar_identifier *lid = ccl_iterator_next_element (pi);
      ar_identifier *newlid = s_instantiate_id (lid, subname);
      
      ar_node_add_preemptible (result, newlid);
      ar_identifier_del_reference (newlid);
    }
  ccl_iterator_delete (pi);
}

struct flatten_bucket_data
{
  ar_identifier *subname;
  ar_node *submodel;
  ar_node *result;
};

static void
s_add_bucket (ar_identifier *bid, ccl_list *events, ccl_list *probas,
	      void *data)
{
  ccl_pair *p;
  struct flatten_bucket_data *d = data;
  ccl_list *newevents = ccl_list_create ();
  ccl_list *newprobas = ccl_list_create ();
  ar_identifier *newbid = s_instantiate_id (bid, d->subname);
  
  for (p = FIRST (events); p; p = CDR (p))
    {
      ar_identifier *newlid = s_instantiate_id (CAR (p), d->subname);
      ccl_list_add (newevents, newlid);
    }
  
  for (p = FIRST (probas); p; p = CDR (p))
    {
      sas_law_param *newlp = sas_law_param_instantiate (CAR (p), d->subname);
      ccl_list_add (newprobas, newlp);
    }
  ar_node_add_bucket (d->result, newbid, newevents, newprobas);
  ar_identifier_del_reference (newbid);
  ccl_list_clear_and_delete (newevents, (ccl_delete_proc *)
			     ar_identifier_del_reference);
  ccl_list_clear_and_delete (newprobas, (ccl_delete_proc *)
			     sas_law_param_del_reference);
}
	      
static void
s_add_buckets (ar_node *result, ar_identifier *subname, ar_node *submodel)
{
  struct flatten_bucket_data data = { subname, submodel, result };
    
  ar_node_map_buckets (submodel, s_add_bucket, &data);
}

static void
s_add_priorities (ar_node *result, ar_identifier *subname, ar_node *submodel)
{
  ar_identifier_iterator *ii = ar_node_get_events_with_priorities (submodel);
  
  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      ar_identifier *newid = s_instantiate_id (id, subname);
      int pl = ar_node_get_priority (submodel, id);
      ar_node_add_priority (result, newid, pl);
      ar_identifier_del_reference (newid);
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);
}

static void
s_add_external_directives (ar_node *result, ar_identifier *subname,
			   ar_node *submodel, ar_context *ctx,
			   ccl_hash *sl2expr, ccl_hash *cache)
{
  s_add_law_parameters (result, subname, submodel);
  s_add_laws (result, subname, submodel, ctx, sl2expr, cache);
  s_add_observers (result, subname, submodel, ctx, sl2expr, cache);    
  s_add_preemptibles (result, subname, submodel);
  s_add_buckets (result, subname, submodel);
  s_add_priorities (result, subname, submodel);
}

static void
s_add_node(ar_node *result, ar_identifier *subname, ar_node *submodel,
	   ar_idtable *translists)
{
  ar_context *ctx = ar_node_get_context(result);
  ccl_hash *sl2expr = NULL;
  ccl_hash *sl2sl = NULL;
  ccl_hash *cache = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
		     ar_expr_del_reference);
  ccl_hash *eventcache =
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
		     ar_event_del_reference);
  
  ccl_pre( ccl_imply(subname != NULL,ar_node_is_leaf(submodel)) );

  if( subname != NULL )
    {
      sl2expr = ccl_hash_create(NULL,NULL,
				(ccl_delete_proc *)ar_context_slot_del_reference,
				(ccl_delete_proc *)ar_expr_del_reference);
      sl2sl = ccl_hash_create(NULL,NULL,
			      (ccl_delete_proc *)ar_context_slot_del_reference,
			      (ccl_delete_proc *)ar_context_slot_del_reference);
    }

  /* add parameters */
  {
    ar_identifier_iterator *i = ar_node_get_names_of_parameters (submodel);
    while (ccl_iterator_has_more_elements (i))
      {
	ar_identifier *pid = ccl_iterator_next_element (i);
	ar_type *type = ar_node_get_type_of_parameter (submodel, pid);
	ar_expr *value = ar_node_get_value_of_parameter (submodel, pid);

	if( subname != NULL )
	  {
	    ar_identifier *tmp = ar_identifier_add_prefix(pid,subname);
	    ar_identifier_del_reference(pid);
	    pid = tmp;
	  }	

	ar_node_add_local_parameter(result,pid,type,value);

	ar_type_del_reference(type);
	ccl_zdelete(ar_expr_del_reference,value);
	ar_identifier_del_reference(pid);
      }
    ccl_iterator_delete(i);
  }
  
  /* add variables */
  {
    ar_identifier_iterator *i = ar_node_get_names_of_variables (submodel);
    while( ccl_iterator_has_more_elements(i) )
      {
	ar_identifier *vid = ccl_iterator_next_element(i);
	uint32_t flags = ar_node_get_flags_of_variable (submodel, vid);
	ar_type *type = ar_node_get_type_of_variable (submodel, vid);
	ccl_list *attr = ar_node_get_attributes_of_variable (submodel, vid);

	if( subname != NULL )
	  {
	    ar_identifier *tmp = ar_identifier_add_prefix(vid,subname);
	    ar_identifier_del_reference(vid);
	    vid = tmp;
	  }	

	ar_node_add_variable(result,vid,type,flags,attr);

	if( attr != NULL )
	  ccl_list_clear_and_delete(attr,(ccl_delete_proc *)
				    ar_identifier_del_reference);
	ar_type_del_reference(type);
	ar_identifier_del_reference(vid);
      }
    ccl_iterator_delete(i);
  }

  if( sl2expr )
    {
      ccl_pair *p;
      ar_context *subctx = ar_node_get_context(submodel);
      ccl_list *slots = ar_context_get_slots(subctx,
					    AR_SLOT_FLAG_PARAMETER|
					    AR_SLOT_FLAG_STATE_VAR|
					    AR_SLOT_FLAG_FLOW_VAR);


      for(p = FIRST(slots); p; p = CDR(p))
	{
	  ar_identifier *sid = (ar_identifier *)CAR(p);
	  ar_context_slot *sl = ar_context_get_slot(subctx,sid);
	  ar_identifier *nid = ar_identifier_add_prefix(sid,subname);
	  ar_context_slot *nsl = ar_context_get_slot(ctx,nid);

	  ar_identifier_del_reference(sid);
	  ar_identifier_del_reference(nid);

	  if( nsl != NULL )
	    {
	      ar_expr *esl;

	      if( (ar_context_slot_get_flags(nsl) & AR_SLOT_FLAG_PARAMETER) )
		esl = ar_expr_crt_parameter(nsl);
	      else
		esl = ar_expr_crt_variable(nsl);

	      ccl_hash_find(sl2expr,ar_context_slot_add_reference(sl));
	      ccl_hash_insert(sl2expr,ar_expr_add_reference(esl));
	      ccl_hash_find(sl2sl,ar_context_slot_add_reference(sl));
	      ccl_hash_insert(sl2sl,ar_context_slot_add_reference(nsl));
	      ar_context_slot_del_reference(nsl);
	      ar_expr_del_reference(esl);
	    }

	  ar_context_slot_del_reference(sl);

	}

      ar_context_del_reference(subctx);
      ccl_list_delete(slots);
    }


  /* add assertions */
  {
    ccl_pair *p;
    ccl_list *assertions = ar_node_get_assertions(submodel);
    for(p = FIRST(assertions); p; p = CDR(p))
      {
	ar_expr *a = (ar_expr *)CAR(p);
	ar_expr *new_a;
	if( sl2expr )
	  new_a = ar_expr_replace_with_tables(a, ctx, &sl2expr,1, cache);
	else 
	  new_a = ar_expr_replace_with_context(a,ctx);
	ar_node_add_assertion(result,new_a);
	ar_expr_del_reference(new_a);
      }
  }

  /* add initial constraints */
  {
    ccl_pair *p;
    ccl_list *init_c = ar_node_get_initial_constraints (submodel);
    for(p = FIRST (init_c); p; p = CDR (p))
      {
	ar_expr *c = (ar_expr *) CAR (p);
	ar_expr *new_c;

	if (sl2expr)
	  new_c = ar_expr_replace_with_tables (c, ctx, &sl2expr,1, cache);
	else 
	  new_c = ar_expr_replace_with_context (c, ctx);
	ar_node_add_initial_constraint (result, new_c);
	ar_expr_del_reference (new_c);
      }
  }

  /* add initial states */
  {
    ccl_pair *p;
    ccl_list *init = ar_node_get_initialized_slots (submodel);
    
    for(p = FIRST(init); p; p = CDR(p))
      {
	ar_context_slot *sl = (ar_context_slot *)CAR(p);
	ar_expr *value = ar_node_get_initial_value (submodel, sl);
	ar_context_slot *nsl;
	ar_expr *nvalue;
	
	if( sl2expr )
	  {
	    nvalue = ar_expr_replace_with_tables(value,ctx, &sl2expr,1, cache);
	    ccl_assert( ccl_hash_find(sl2sl,sl) );

	    ccl_hash_find(sl2sl,sl);
	    nsl = ccl_hash_get(sl2sl);
	    nsl = ar_context_slot_add_reference(nsl);
	  }
	else
	  {
	    ar_identifier *id = ar_context_slot_get_name(sl);
	    nvalue = ar_expr_replace_with_context(value,ctx);
	    nsl = ar_context_get_slot(ctx,id);
	    ar_identifier_del_reference(id);
	  }

	ar_node_add_initial_state(result,nsl,nvalue);
	ar_expr_del_reference(nvalue);
	ar_expr_del_reference (value);
	ar_context_slot_del_reference(nsl);
      }
  }

  /* store transitions/event */
  {
    ccl_pair *p;
    ccl_list *trans = ar_node_get_transitions(submodel);

    for(p = FIRST(trans); p; p = CDR(p))
      {
	ar_trans *T = (ar_trans *)CAR(p);
	ar_expr *G = ar_trans_get_guard(T);
	ar_expr *newG = sl2expr
	  ?ar_expr_replace_with_tables(G,ctx, &sl2expr,1,cache)
	  :ar_expr_replace_with_context(G,ctx);
	ar_event *ev = ar_trans_get_event(T);
	ar_event *new_ev = subname
	  ?ar_event_add_prefix(ev,subname, eventcache)
	  :ar_event_add_reference(ev);
	ar_identifier *new_ev_id = ar_event_get_name(new_ev);
	ar_trans *newT = ar_trans_create(newG,new_ev);
	const ar_trans_assignment *a = ar_trans_get_assignments(T);
	ccl_list *tlist = ar_idtable_get(translists,new_ev_id);

	if( tlist == NULL )
	  {
	    tlist = ccl_list_create();
	    ar_idtable_put(translists,new_ev_id,tlist);
	  }
	ccl_list_add(tlist,newT);
	ar_identifier_del_reference(new_ev_id);
	ar_expr_del_reference(G);
	ar_expr_del_reference(newG);
	ar_event_del_reference(ev);
	ar_event_del_reference(new_ev);

	for(; a != NULL; a = a->next)
	  {
	    ar_context_slot *sl;
	    ar_expr *var;
	    ar_expr *value;

	    if( sl2expr )
	      {
		sl = ar_expr_get_slot (a->lvalue);
		value = ar_expr_replace_with_tables(a->value,ctx, &sl2expr,1,
						    cache);
		ccl_assert( ccl_hash_find(sl2sl,sl) );
		ccl_hash_find(sl2sl,sl);
		ar_context_slot_del_reference (sl);
		sl = ccl_hash_get(sl2sl);
		var = ar_expr_crt_variable (sl);
	      }
	    else
	      {
		ar_identifier *id = ar_expr_get_name (a->lvalue);
		sl = ar_context_get_slot(ctx,id);
		ar_identifier_del_reference(id);
		value = ar_expr_replace_with_context(a->value,ctx);
		var = ar_expr_crt_variable (sl);
		ar_context_slot_del_reference (sl);
	      }
	    ar_trans_add_assignment(newT,var,value);
	    ar_expr_del_reference(value);
	    ar_expr_del_reference(var);
	  }
      }
  }

  s_add_external_directives (result, subname, submodel, ctx, sl2expr, cache);

  ccl_hash_delete (eventcache);
  ccl_hash_delete (cache);
  ccl_zdelete(ccl_hash_delete,sl2expr);
  ccl_zdelete(ccl_hash_delete,sl2sl);
  ar_context_del_reference(ctx);
}

			/* --------------- */

static ar_node **
s_get_subnode_semantics(ar_node *node, ar_node *nresult, 
			ar_identifier ***pslots, int *pnb_sub,
			ar_idtable *translists)
  CCL_THROW(altarica_interpretation_exception)
{
  int i;
  ar_identifier_iterator *sli;
  ar_node **result;
  
  DBG_START_TIMER(("adding subnode semantics"));
  
  *pnb_sub = ar_node_get_nb_subnodes(node);
  *pslots = ccl_new_array (ar_identifier *,*pnb_sub);
  result = ccl_new_array(ar_node *,*pnb_sub);
  sli = ar_node_get_names_of_subnodes (node);
  i = 0;
  while( ccl_iterator_has_more_elements(sli) )
    {
      ar_identifier *id = ccl_iterator_next_element(sli);
      ar_node *model = ar_node_get_subnode_model (node, id);

      (*pslots)[i] = ar_identifier_add_reference (id);

      ccl_try (altarica_interpretation_exception)
	{
	  result[i] = (ar_node *)
	    ar_semantics_get (model, AR_SEMANTICS_FLATTENED);
	}
      ccl_catch
	{
	  for(i = 0; i < *pnb_sub; i++)
	    {
	      ccl_zdelete (ar_identifier_del_reference, (*pslots)[i]);
	      ccl_zdelete (ar_node_del_reference, result[i]);
	    }
	  ccl_delete (*pslots);
	  ccl_delete (result);
	  *pslots = NULL;
	  ar_node_del_reference(model);
	  ar_identifier_del_reference(id);
	  ccl_iterator_delete(sli);
	  DBG_END_TIMER ();
	  
	  ccl_rethrow();
	}
      ccl_end_try;

      ccl_assert( ar_node_get_nb_subnodes(result[i]) == 0 );
      s_add_node(nresult,id,result[i],translists);
      i++;
      ar_node_del_reference(model);
      ar_identifier_del_reference(id);
    }
  ccl_iterator_delete(sli);
  DBG_END_TIMER ();
  
  return result;
}

			/* --------------- */

static ccl_list *
s_nodes_with_epsilon (ar_node *node, int nb_sub, ar_identifier **subslots,
		      ar_node **subnodes)
{
  int i;
  ccl_list *result = ccl_list_create ();
  
  ccl_list_add (result, ar_identifier_add_reference (AR_EMPTY_ID));
  ccl_list_add (result, ar_identifier_add_reference (AR_EPSILON_ID));
  
  for (i = 0; i < nb_sub; i++)
    {
      if (ar_node_has_non_trivial_epsilon (subnodes[i]))
	{
	  ccl_list_add (result, ar_identifier_add_reference (subslots[i]));
	  ccl_list_add (result,
			ar_identifier_add_prefix (AR_EPSILON_ID, subslots[i]));
	}
    }
  
  return result;
}

static ccl_list *
s_complete_synchronization_vectors(ar_node *node, int nb_sub, 
				   ar_identifier **subslots,
				   ar_node **subnodes)
{
  ccl_pair *p;
  ccl_list *vectors = ar_node_get_broadcasts(node);
  ccl_list *result = ccl_list_create ();
  ccl_list *epsnodes = s_nodes_with_epsilon (node, nb_sub, subslots, subnodes);
    
  for(p = FIRST(vectors); p; p = CDR(p))
    {
      ccl_pair *pn;
      ar_broadcast *bv = (ar_broadcast *) CAR (p);
      ar_broadcast *cbv = ar_broadcast_clone (bv);

      for (pn = FIRST (epsnodes); pn; pn = CDDR (pn))
	{
	  ar_identifier *sub = CAR (pn);
	  if (! ar_broadcast_has_subnode (cbv, sub))
	    ar_broadcast_add_event (cbv, CADR (pn), 0);
	}
      ccl_list_add (result, cbv);
    }

  ccl_list_clear_and_delete (epsnodes, (ccl_delete_proc *)
			     ar_identifier_del_reference);
  
  return result;
}

			/* --------------- */

struct translist_cell
{
  ar_identifier *e;
  ccl_list *translist;
};

typedef struct transition_generator_data {
  int nb_sub;
  void **todo;
  ccl_list *node_ordering;
  ar_idtable *translists;
  struct translist_cell *translist_table;
  ar_trans **tvect;
  ar_expr **guards;
  ar_event **events;

  ccl_list *transitions;
  ar_idtable *ev2tlist;
  ccl_hash *evlist2tlist;
  ar_poset *trans_poset;
  ccl_list *gtT;
  ccl_list *vect;
} TGD;

			/* --------------- */

static void
s_build_transition_vector (int pos, TGD *data)
{
  int i;
  ccl_pair *p;
  ccl_list *tlist;
  ar_expr *G = ar_expr_crt_and(pos,data->guards);
  ar_identifier *label = ar_event_get_name (data->events[0]);
  ar_event *E = ar_event_create_vector (label, pos - 1, data->events + 1);
  ar_trans *newT = ar_trans_create(G,E);

  ccl_pre (ar_identifier_get_path_length (label) == 1);
  ar_identifier_del_reference (label);
  
  for(i = 0; i < pos; i++)
    {
      const ar_trans_assignment *a;

      if (data->tvect[i] == NULL)
	{
	  ccl_assert (i == 0 && data->events[i] == AR_EPSILON);
	  continue;
	}
      for(a = ar_trans_get_assignments(data->tvect[i]); a; a = a->next)
	ar_trans_add_assignment(newT,a->lvalue,a->value);
    }
  ccl_list_add(data->transitions,newT);

  if( ccl_hash_find(data->evlist2tlist,data->vect) )
    tlist = (ccl_list *)ccl_hash_get(data->evlist2tlist);
  else
    {
      tlist = ccl_list_create();
      ccl_hash_insert(data->evlist2tlist,tlist);
    }
  ccl_list_add(tlist,newT);
  ar_poset_add(data->trans_poset,newT);
  for(p = FIRST(data->gtT); p; p = CDR(p))
    ar_poset_add_pair_no_check (data->trans_poset,CAR(p),newT);

  {
    ar_identifier *base_name = ar_event_get_name(E);

    ccl_assert( ar_identifier_get_path_length(base_name) == 1 );
	
    if( (tlist=ar_idtable_get(data->ev2tlist,base_name)) == NULL )
      {
	tlist = ccl_list_create();
	ar_idtable_put(data->ev2tlist,base_name,tlist);
      }
    ccl_list_add(tlist,ar_trans_add_reference(newT));
    ar_identifier_del_reference(base_name);
  }
  ar_event_del_reference(E);
  ar_expr_del_reference(G);
}

static void
s_generate_transitions_for_vector_rec (ccl_pair *event, int pos, TGD *data)
{
  ccl_pair *p;
  ar_identifier *id;
  ccl_list *translist;
  
  if (event == NULL)
    {
      s_build_transition_vector (pos, data);
      return;
    }

  id = CAR (event);
  
  if (data->translist_table[pos].e != id)
    {
      data->translist_table[pos].e = id;
      data->translist_table[pos].translist =
	ar_idtable_get (data->translists, id);
    }

  translist = data->translist_table[pos].translist;

  if (translist == NULL)
    {
      ccl_assert (pos == 0 && id == AR_EPSILON_ID);
      data->tvect[pos] = NULL;
      data->guards[pos] = ar_expr_crt_true ();
      data->events[pos] = ar_event_add_reference (AR_EPSILON);
      s_generate_transitions_for_vector_rec (CDR (event), pos + 1, data);
      ar_expr_del_reference (data->guards[pos]);
      ar_event_del_reference (data->events[pos]);      
    }
  else
    {
      ccl_pre (! ccl_list_is_empty (translist)); 

      for (p = FIRST (translist); p; p = CDR (p))
	{
	  data->tvect[pos] = CAR (p);
	  data->guards[pos] = ar_trans_get_guard(data->tvect[pos]);
	  data->events[pos] = ar_trans_get_event(data->tvect[pos]);
	  s_generate_transitions_for_vector_rec (CDR (event), pos + 1, data);
	  ar_expr_del_reference (data->guards[pos]);
	  ar_event_del_reference (data->events[pos]);
	}
    }
}

static void
s_generate_transitions_for_vector (ccl_list *events, TGD *data)
{
  if (ar_identifier_get_path_length (CAR (FIRST (events))) > 1)
    ccl_list_put_first (events, ar_identifier_add_reference (AR_EPSILON_ID));

  s_generate_transitions_for_vector_rec (FIRST (events), 0, data);
}

static int
s_sort_subnodes (const ar_identifier *id1, const ar_identifier *id2)
{
  int res = (ar_identifier_get_path_length (id1) -
	     ar_identifier_get_path_length (id2));

  if (res == 0)
    res = ar_identifier_compare (id1, id2);
  return res;
}


static void
s_generate_transitions_for_std_vector (ar_broadcast *bv, TGD *data)
{
  data->evlist2tlist = 
    ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)ccl_list_delete);
  data->gtT = ccl_list_create();
  data->vect = ar_broadcast_get_instance_vector (bv);
  ccl_list_sort (data->vect, (ccl_compare_func *) s_sort_subnodes);
		 
  s_generate_transitions_for_vector (data->vect, data);
  ccl_list_delete (data->gtT);
  ccl_list_clear_and_delete (data->vect, (ccl_delete_proc *)
			     ar_identifier_del_reference);
  ccl_hash_delete(data->evlist2tlist);

}

static void
s_generate_transitions_for_broadcast_vector (ar_broadcast *bv, TGD *data)
{
  ccl_pair *p;
  ar_poset *poset = ar_broadcast_get_instance_vectors(bv);
  ccl_list *vectors = ar_poset_get_top_to_down_objects(poset);

  data->evlist2tlist = 
    ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)ccl_list_delete);

  for(p = FIRST(vectors); p; p = CDR(p))
    {
      ccl_pair *pgt;
      ccl_list *gt = ar_poset_get_greater_objects(poset,CAR(p));
      ccl_list *vect = (ccl_list *)CAR(p);
      
      data->gtT = ccl_list_create();
      data->vect = vect;
      ccl_list_sort (data->vect, (ccl_compare_func *)
		     s_sort_subnodes);
      
      for(pgt = FIRST(gt); pgt; pgt = CDR(pgt))
	{
	  ccl_list *gt_vect = (ccl_list *)CAR(pgt);
	  ccl_list *gt_trans;
	  
	  ccl_hash_find(data->evlist2tlist,gt_vect);
	  gt_trans = ccl_hash_get(data->evlist2tlist);
	  ccl_list_append(data->gtT,gt_trans);
	}
      ccl_list_delete(gt);
      
      s_generate_transitions_for_vector (vect,data);
      ccl_list_delete(data->gtT);
    }

  ccl_hash_delete(data->evlist2tlist);

  ccl_list_delete(vectors);
  ar_poset_del_reference(poset);
}

static void
s_generate_transitions_for_broadcast (ar_broadcast *bv, TGD *data)
{
  if (ar_broadcast_get_nb_marked (bv) == 0)
    s_generate_transitions_for_std_vector (bv, data);
  else
    s_generate_transitions_for_broadcast_vector (bv, data);
}

static void
s_apply_broadcasts(ar_node *result, ccl_list *sync, ar_idtable *translists,
		   int nb_sub, ar_event_poset *control,
		   ar_identifier **subslots)
{
  int i;
  int o = 0;
  ccl_pair *p;
  ar_poset *orders[2];
  TGD data;
  ar_context *ctx = ar_node_get_context(result);
  ccl_list *trans;

  data.node_ordering = ccl_list_create ();
  ccl_list_add (data.node_ordering, AR_EMPTY_ID);
  for (i = 0; i < nb_sub; i++)
    ccl_list_add (data.node_ordering, subslots[i]);

  data.todo = ccl_new_array (void *, 5 * (nb_sub + 2));

  data.translists = translists;
  data.translist_table = ccl_new_array (struct translist_cell, nb_sub + 1);
  data.nb_sub = nb_sub;
  data.tvect = ccl_new_array(ar_trans *,nb_sub+1);
  data.events = ccl_new_array(ar_event *,nb_sub+1);
  data.guards = ccl_new_array(ar_expr *,nb_sub+1);

  data.transitions = ccl_list_create();
  data.ev2tlist = 
    ar_idtable_create (0, NULL,
		       (ccl_delete_proc *) s_delete_transition_list);
  data.trans_poset = orders[0] = ar_poset_create (NULL, NULL, NULL, NULL);

  DBG_START_TIMER (("generate macro-transitions"));
  {
    int nb_trans = 0;
    for(p = FIRST(sync); p; p = CDR(p))
      {
	ar_broadcast *bv = (ar_broadcast *)CAR(p);

	s_generate_transitions_for_broadcast(bv,&data);
	if (DBG_FLATTENING (log_vectors))
	  {
	    ccl_debug ("generate %d transitions for ", 
		       ccl_list_get_size (data.transitions) - nb_trans);
	    nb_trans = ccl_list_get_size (data.transitions);
	    ar_broadcast_log (CCL_LOG_DEBUG, bv);
	    ccl_debug (".\n");
	  }
      }    
  }
  ccl_list_delete (data.node_ordering);
  
  if (DBG_IS_ON)
    {
      ccl_debug ("generate %d transitions by %d synchros.\n", 
		 ccl_list_get_size (data.transitions),
		 ccl_list_get_size (sync));
    }
  DBG_END_TIMER ();

  if( ! ar_poset_is_empty(data.trans_poset) )
    orders[o++] = ar_poset_add_reference(data.trans_poset);  
  ar_poset_del_reference(data.trans_poset);  

  orders[o] = ar_poset_create (NULL, NULL, NULL, NULL);

  if( ! ar_event_poset_is_empty(control) )
    {
      ar_event_iterator *ei;

      DBG_START_TIMER (
			     ("build ordering of macro-transitions for "
			      "controller"));

      ei = ar_event_poset_get_events(control);
      while( ccl_iterator_has_more_elements(ei) )
	{
	  ccl_pair *p_te;
	  ar_event *e = ccl_iterator_next_element(ei);
	  ar_identifier *eid = ar_event_get_name(e);
	  ccl_list *e_trans = ar_idtable_get(data.ev2tlist,eid);
	  ccl_list *gt_e = ar_event_poset_get_greater_events(control,e);
	  
	  ar_identifier_del_reference(eid);
	  ar_event_del_reference(e);
	  
	  for(p_te = FIRST(e_trans); p_te; p_te = CDR(p_te))
	    {
	      ccl_pair *pe;

	      ar_poset_add(orders[o],CAR(p_te));
	      for(pe = FIRST(gt_e); pe; pe = CDR(pe))
		{
		  ccl_pair *p_t_gt_e;
		  ar_identifier *gt_eid = ar_event_get_name(CAR(pe));
		  ccl_list *gt_e_trans = ar_idtable_get(data.ev2tlist,gt_eid);
		  ar_identifier_del_reference(gt_eid);
	      
		  for(p_t_gt_e = FIRST(gt_e_trans); p_t_gt_e; 
		      p_t_gt_e = CDR(p_t_gt_e))
		    ar_poset_add_pair_no_check (orders[o],CAR(p_t_gt_e),
						CAR(p_te));
		}
	    }

	  ccl_list_delete(gt_e);
	}
      ccl_iterator_delete(ei);
      DBG_END_TIMER ();
    }

  if( ar_poset_is_empty(orders[o]) )
    ar_poset_del_reference(orders[o]);  
  else
    o++;

  if( o > 0 )
    {
      ccl_list *assertions = ar_node_get_assertions(result);


      DBG_START_TIMER (("applying priorities"));
      trans = s_apply_transition_order(ctx,data.transitions,orders,o, 
				       assertions);
      s_delete_transition_list(data.transitions);
      DBG_END_TIMER ();
    }
  else
    {
      trans = data.transitions;
    }
  
  while( o-- )
    ar_poset_del_reference(orders[o]);

  ccl_delete (data.todo);
  ccl_delete (data.translist_table);
  ccl_delete(data.tvect);
  ccl_delete(data.events);
  ccl_delete(data.guards);
  ar_idtable_del_reference(data.ev2tlist);
  ar_context_del_reference(ctx);

  DBG_START_TIMER (("registering macro-events"));
  for(p = FIRST(trans); p; p = CDR(p))
    {
      ar_trans *t = (ar_trans *)CAR(p);
      ar_event *ev = ar_trans_get_event(t);
      ar_event *actual_event = ar_node_get_event_from_event(result,ev);

      if( actual_event == NULL )
	{
	  actual_event = ar_event_add_reference(ev);
	  ar_node_add_event(result,ev);
	}
      ar_node_add_transition(result,t);
      ar_trans_set_event(t,actual_event);

      ar_trans_del_reference(t);
      ar_event_del_reference(ev);
      ar_event_del_reference(actual_event);
    }
  DBG_END_TIMER ();

  ccl_list_delete(trans);
}

static ar_node *
s_synchronized_product(ar_node *node, ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception)
{
  int i, nb_sub;
  ar_identifier **subslots;
  ar_node **subnodes = NULL;
  ccl_list *sync;
  ar_idtable *translists;
  ar_node *result = ar_node_create(ctx);
  ar_event_poset *control;

  DBG_START_TIMER (("compute subnodes semantics"));
  subslots = NULL;
  translists = ar_idtable_create(0, NULL,s_delete_transition_list);
  ccl_try(altarica_interpretation_exception)
    {
      subnodes = 
	s_get_subnode_semantics(node,result,&subslots,&nb_sub,translists);
    }
  ccl_catch
    {
      ar_idtable_del_reference(translists);
      ar_node_del_reference(result);
      DBG_END_TIMER ();
      ccl_rethrow();
    }
  ccl_end_try;
  DBG_END_TIMER ();

  DBG_START_TIMER (("adding self components"));
  s_add_node (result, NULL, node, translists);  
  DBG_END_TIMER ();

  DBG_START_TIMER (("complete sync vectors"));
  sync = s_complete_synchronization_vectors (node, nb_sub, subslots, subnodes);
  DBG_END_TIMER ();

  control = ar_node_get_event_poset (node);
  s_apply_broadcasts (result, sync, translists, nb_sub, control, subslots);
  ar_event_poset_del_reference(control);
  ccl_list_clear_and_delete(sync,(ccl_delete_proc *)ar_broadcast_del_reference);
  
  for(i = 0; i < nb_sub; i++)
    {
      ar_identifier_del_reference(subslots[i]);
      ar_node_del_reference(subnodes[i]);
    }
  ccl_delete(subnodes);
  ccl_delete(subslots);
  ar_idtable_del_reference(translists);

  return result;
}

static void
s_add_epsilon_loop (ar_node *node)
{
  ar_expr *guard = ar_expr_crt_true();
  ar_trans *t = ar_trans_create (guard, AR_EPSILON);

  ar_node_add_event (node, AR_EPSILON);
  ar_node_add_transition (node, t);
  ar_expr_del_reference (guard);
  ar_trans_del_reference (t);  
}

static ar_node *
s_compute_flat_semantics (ar_node *node, ar_context *ctx)
  CCL_THROW (altarica_interpretation_exception)
{
  ar_node *result = NULL;
  
  ccl_pre (node != NULL); 
  ccl_pre (ar_node_get_template (node) == NULL);

  if (ar_node_get_nb_subnodes (node) == 0)
    {
      ar_event_poset *poset = ar_node_get_event_poset(node);

      if( ar_event_poset_is_empty(poset) )
	result = ar_node_add_reference(node);
      else
	result = s_apply_event_order(node);

      ar_event_poset_del_reference(poset);
    }
  else
    {
      ar_identifier *name = ar_node_get_name(node);
      
      ccl_try(altarica_interpretation_exception)
        {
	  result = s_synchronized_product(node, ctx);
        }
      ccl_no_catch;

      if( result != NULL )
	ar_node_set_name(result,name);

      ar_identifier_del_reference(name);

      if( result == NULL )
	ccl_throw(altarica_interpretation_exception,
		  ccl_exception_current_message);
    }
  
  if (ar_node_get_number_of_events (result) == 0)
    s_add_epsilon_loop (result);

  return result;
}

ar_node *
ar_compute_flat_semantics(ar_node *node, ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception)
{
  ar_node *n;
  ar_node *flat;

  if (DBG_IS_ON)
    {
      ar_identifier *nodename = ar_node_get_name (node);
      char *s = ar_identifier_to_string (nodename);      
      CCL_DEBUG_START_TIMED_BLOCK (("flat semantics for '%s'", s));
      ccl_string_delete (s);
      ar_identifier_del_reference (nodename);
    }

  if( ar_node_get_template(node) != NULL )
    {
      ar_identifier *nodename = ar_node_get_name(node);
           
      ccl_error ("error: '");
      ar_identifier_log  (CCL_LOG_ERROR, nodename);
      ccl_error ("' contains parameters that are not instanciated.\n");
      ar_identifier_del_reference(nodename);
      ccl_throw_no_msg (altarica_interpretation_exception);
    }
  else
    {
      n = ar_node_add_reference(node);
    }

  ccl_try(altarica_interpretation_exception)
    {
      flat = s_compute_flat_semantics(n, ctx);
    }
  ccl_catch
    {
      flat = NULL;
    }
  ccl_end_try;
  
  ar_node_del_reference(n);
  DBG_END_TIMER ();

  if (flat == NULL)
    ccl_rethrow ();
  
  return flat;
}
