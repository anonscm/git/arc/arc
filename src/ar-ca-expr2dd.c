/*
 * ar-ca-expr2dd.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-ca-expr2dd.h"

static ar_dd
s_solve_constraint(ar_ddm ddm, ar_ca_expr *expr, const varorder *order);

			/* --------------- */

static int
s_cmp (const void *p1, const void *p2)
{
  if (p1 < p2) return 1;
  if (p1 > p2) return -1;
  return 0;
}


static void
s_collect_kind (ar_ca_expr *e, ar_ca_expression_kind kind, ccl_list *l,
		ccl_hash *cache)
{
  if (ar_ca_expr_get_kind (e) == kind)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      s_collect_kind (args[0], kind, l, cache); 
      s_collect_kind (args[1], kind, l, cache); 
    }
  else if (! ccl_hash_find (cache, e))
    {
      ccl_hash_insert (cache, e);
      ccl_list_insert (l, e, s_cmp);
    }
}

ar_dd 
ar_ca_expr_compute_dd (ar_ca_expr *expr, ar_ddm ddm, const varorder *order, 
		       ccl_hash *cache)
{
  ar_dd result = NULL;
  ar_dd ops[3] = { NULL, NULL, NULL };
  ar_ca_expr **args = ar_ca_expr_get_args(expr);
  ar_ca_expression_kind k = ar_ca_expr_get_kind(expr);

  if (cache && ccl_hash_find (cache, expr))
    {
      result = ccl_hash_get (cache);

      return AR_DD_DUP (result);
    }

  switch( k ) {
  case AR_CA_CST:
    if( ar_ca_expr_get_min(expr) ) result = ar_dd_one(ddm);
    else result = ar_dd_zero(ddm);
    result = AR_DD_DUP(result);
    break;

  case AR_CA_VAR:
    {
      int arity = 2;
      ar_ddtable sons = ar_dd_allocate_dd_table(ddm,arity,EXHAUSTIVE);
      int index = varorder_get_dd_index (order, expr);

      ccl_assert (index >= 0);

      AR_DD_ENTHDD(sons,0) = AR_DD_DUP(ar_dd_zero(ddm));
      AR_DD_ENTHDD(sons,1) = AR_DD_DUP(ar_dd_one(ddm));
      result = ar_dd_find_or_add(ddm,index,arity,EXHAUSTIVE,sons);
      ar_dd_free_dd_table(ddm,arity,EXHAUSTIVE,sons);
    }
    break;

  case AR_CA_AND: case AR_CA_OR: 
    {
      ccl_hash *c = ccl_hash_create (NULL, NULL, NULL, NULL);
      ccl_list *ops = ccl_list_create ();
      ar_ca_expression_kind k = ar_ca_expr_get_kind (expr);
      ar_dd (*ddop) (ar_ddm, ar_dd, ar_dd);

      if (k == AR_CA_OR)
	ddop = ar_dd_compute_or;
      else 
	ddop = ar_dd_compute_and;

      s_collect_kind (expr, k, ops, c);

      ccl_assert (! ccl_list_is_empty (ops));

      result = 
	ar_ca_expr_compute_dd (ccl_list_take_first (ops), ddm, order, cache);
      while (! ccl_list_is_empty (ops))
	{
	  ar_dd tmp1 = 
	    ar_ca_expr_compute_dd (ccl_list_take_first (ops), ddm, order, 
				   cache);
	  ar_dd tmp2 = ddop (ddm, result, tmp1);
	  AR_DD_FREE (tmp1);
	  AR_DD_FREE (result);
	  result = tmp2;
	}

      ccl_list_delete (ops);
      ccl_hash_delete (c);
    }
    break;

  case AR_CA_NOT:
    ops[0] = ar_ca_expr_compute_dd (args[0], ddm, order, cache);
    result = AR_DD_NOT (ops[0]);
    break;

  case AR_CA_ITE: case AR_CA_EQ:
    if (ar_ca_expr_is_boolean_expr (args[1]))
      {
	ops[0] = ar_ca_expr_compute_dd (args[0], ddm, order, cache);
	ops[1] = ar_ca_expr_compute_dd (args[1], ddm, order, cache);

	if (k == AR_CA_ITE)  
	  {
	    ops[2] = ar_ca_expr_compute_dd (args[2], ddm, order, cache);
	    result = ar_dd_compute_ite(ddm,ops[0],ops[1],ops[2]);
	    ccl_zdelete(AR_DD_FREE,ops[2]); 
	  }
	else
	  {
	    result = ar_dd_compute_iff(ddm,ops[0],ops[1]);
	  }
	ccl_zdelete(AR_DD_FREE,ops[0]); 
	ccl_zdelete(AR_DD_FREE,ops[1]); 
	break;
      }

  case AR_CA_LT:
    result = s_solve_constraint (ddm, expr, order);
    break;

  case AR_CA_NEG: case AR_CA_ADD: case AR_CA_MUL: case AR_CA_DIV: 
  case AR_CA_MOD: case AR_CA_MIN: case AR_CA_MAX:
    ccl_throw(internal_error,"invalid switch value");
  }

  if (cache && ! ccl_hash_find (cache, expr))
    {
      expr = ar_ca_expr_add_reference (expr);

      ccl_hash_insert (cache, AR_DD_DUP (result));
    }

  return result;
}

			/* --------------- */

static int
s_compare_variable_indexes(const void *p1, const void *p2, void *data)
{
  const varorder *order = *(const varorder **)data;
  const ar_ca_expr *v1 = (const ar_ca_expr *) p1;
  int index1 = varorder_get_dd_index (order, v1);
  const ar_ca_expr *v2 = (const ar_ca_expr *) p2;
  int index2 = varorder_get_dd_index (order, v2);

  ccl_pre (index1 >= 0);
  ccl_pre (index2 >= 0);

  return index2 - index1;
}

			/* --------------- */
#if 1
#include "ar-solver.h"

struct solver_data
{
  ar_ddm ddm;
  int nb_vars;
  ccl_list *ordered_vars;
  ar_ca_expr **vars;
  const varorder *order;
  ar_dd result;
};

static int
s_new_solution (ar_solver *solver, void *clientdata)
{
  int i; 
  ar_dd tmp[2];
  struct solver_data *data = clientdata;
  ar_dd sol = ar_dd_dup_one (data->ddm);

  for (i = 0; i < data->nb_vars; i++)
    {
      ar_ca_expr *var = data->vars[i];
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
      int card = ar_ca_domain_get_cardinality (dom);
      ar_ddcode encoding = card <= 2 ? EXHAUSTIVE : COMPACTED;
      int dd_index = varorder_get_dd_index (data->order, var);
      int min = ar_ca_expr_get_min (var);
      int max = ar_ca_expr_get_max (var);

      min = ar_ca_domain_get_value_index (dom, min);
      max = ar_ca_domain_get_value_index (dom, max);

      tmp[0] = 
	ar_dd_var_in_range (data->ddm, dd_index, card, encoding, min, max);
      tmp[1] = ar_dd_compute_and (data->ddm, sol, tmp[0]);

      AR_DD_FREE (sol);
      AR_DD_FREE (tmp[0]);
      sol = tmp[1];
    }

  tmp[0] = ar_dd_compute_or (data->ddm, data->result, sol);
  AR_DD_FREE (sol);
  AR_DD_FREE (data->result);
  data->result = tmp[0];

  return 1;
}

static ar_dd
s_solve_constraint (ar_ddm ddm, ar_ca_expr *expr, const varorder *order)
{
  int i;
  ccl_pair *p;
  struct solver_data data;
  ar_solver *solver;    

  data.ordered_vars = ar_ca_expr_get_variables (expr, NULL);
  data.ddm = ddm;
  data.nb_vars = ccl_list_get_size (data.ordered_vars);
  data.vars = ccl_new_array (ar_ca_expr *, data.nb_vars);
  data.order = order;
  data.result = ar_dd_dup_zero (ddm);

  for (i = 0, p = FIRST (data.ordered_vars); p; p = CDR (p), i++)
    {
      data.vars[i] = CAR (p);
      ar_ca_expr_variable_reset_bounds (data.vars[i]);
    }
  ccl_list_sort_with_data (data.ordered_vars, s_compare_variable_indexes, 
			   &order);

  solver = ar_create_gp_solver (&expr, 1, data.vars, data.nb_vars, NULL,
				NULL, s_new_solution, NULL);
  ar_solver_solve (solver, &data);
  ar_solver_delete (solver);
  ccl_delete (data.vars);
  ccl_list_delete (data.ordered_vars);

  return data.result;
}
#else
static ar_dd
s_enumerate_assignments (ar_ddm ddm, ar_ca_expr *expr, varorder *order, 
			 ccl_pair *p, int pred_index, int pred_value)
{
  ar_dd result;

  if( p == NULL )
    {
      ar_ca_expr_evaluate(expr,1);
      if( ar_ca_expr_get_min(expr) )
	result = ar_dd_one(ddm);
      else 
	result = ar_dd_zero(ddm);
      result = AR_DD_DUP(result);
    }
  else
    {
      ar_bounds bounds;
      ar_ca_expr *var = (ar_ca_expr *)CAR(p);
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain(var);
      int index = varorder_get_dd_index (order, var);

      if (index == pred_index)
	{
	  ar_ca_expr_set_bounds_to_ith_domain_value (var, pred_value);
	  result = s_enumerate_assignments (ddm, expr, order, CDR (p),
					    pred_index, pred_value);
	}
      else
	{
	  int i, arity;
	  ar_ddcode encoding;
	  ar_ddtable sons;

	  ar_ca_domain_get_bounds(dom,&bounds);
	  arity = ar_ca_domain_get_cardinality (dom);
	  encoding = ((arity > 2)?COMPACTED:EXHAUSTIVE); 
	  sons = ar_dd_allocate_dd_table(ddm,arity,encoding);
      
	  if( encoding == COMPACTED ) 
	    {
	      for(i = 0; i < arity; i++)
		{
		  ar_ca_expr_set_bounds_to_ith_domain_value (var, i);
		  AR_DD_MIN_OFFSET_NTHDD(sons,i) = i;
		  AR_DD_MAX_OFFSET_NTHDD(sons,i) = i;
		  AR_DD_CNTHDD(sons,i) = 
		    s_enumerate_assignments (ddm, expr, order, CDR(p), index, 
					     i);
		}
	    }
	  else
	    {
	      for(i = 0; i < arity; i++)
		{
		  ar_ca_expr_set_bounds_to_ith_domain_value (var, i);
		  AR_DD_ENTHDD(sons,i) = 
		    s_enumerate_assignments (ddm, expr, order, CDR(p), index,
					     i);
		}
	    }
	  result = ar_dd_find_or_add(ddm,index,arity,encoding,sons);
	  ar_dd_free_dd_table(ddm,arity,encoding,sons);
	}
    }

  return result;
}

			/* --------------- */

static ar_dd
s_solve_constraint (ar_ddm ddm, ar_ca_expr *expr, varorder *order)
{
  ar_dd result;
  ccl_list *variables = ar_ca_expr_get_variables (expr, NULL);

  ccl_list_sort_with_data (variables, s_compare_variable_indexes, order);
  result = s_enumerate_assignments (ddm, expr, order, FIRST (variables), -1, 0);
  ccl_list_delete(variables);

  return result;
}
#endif
			/* --------------- */

static ar_ca_expr *
s_domain_get_value (ar_ca_exprman *eman, const ar_ca_domain *dom, int idx)
{
  int value = ar_ca_domain_get_ith_value (dom, idx);
  ar_ca_expr *result;

  ccl_assert (!ar_ca_domain_is_boolean (dom));
  
  if (ar_ca_domain_is_enum (dom))
    result = ar_ca_expr_get_enum_constant_by_index (eman, value);
  else
    result = ar_ca_expr_crt_integer_constant (eman, value);

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_compute_expr_from_dd (ar_ddm ddm, ar_ca_exprman *eman, const varorder *order, 
			ar_dd dd, ccl_hash *cache)
{
  ar_ca_expr *result = NULL;

  if (cache && ccl_hash_find (cache, dd))
    return ar_ca_expr_add_reference (ccl_hash_get (cache));

  if (ar_dd_is_zero (ddm, dd))
    result = ar_ca_expr_crt_boolean_constant (eman, 0);
  else if (ar_dd_is_one (ddm, dd))
    result = ar_ca_expr_crt_boolean_constant (eman, 1);
  else
    {
      int arity = AR_DD_ARITY (dd);
      int index = AR_DD_INDEX (dd);
      ar_ca_expr *var = varorder_get_variable_from_dd_index (order, index);
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
      var = ar_ca_expr_add_reference (var);

      if (ar_ca_domain_is_boolean (dom))
	{
	  ar_ca_expr *F0 = 
	    s_compute_expr_from_dd (ddm, eman, order, AR_DD_EPNTHSON (dd, 0),
				    cache);
	  ar_ca_expr *F1 = 
	    s_compute_expr_from_dd (ddm, eman, order, AR_DD_EPNTHSON (dd, 1),
				    cache);
	  ccl_assert (arity == 2);

	  result = ar_ca_expr_crt_ite (var, F1, F0);

	  ar_ca_expr_del_reference (F0);
	  ar_ca_expr_del_reference (F1);
	}
      else if (AR_DD_CODE (dd) == COMPACTED)
	{
	  int i;

	  if (ar_ca_domain_is_integer (dom))
	    {
	      for(i = 0; i < arity; i++) 
		{
		  ar_ca_expr *tmp[3];
		  int min = AR_DD_MIN_OFFSET_NTHSON (dd, i);
		  int max = AR_DD_MAX_OFFSET_NTHSON (dd, i);

		  if (min == max)
		    {
		      tmp[1] = s_domain_get_value (eman, dom, min);
		      tmp[0] = ar_ca_expr_crt_eq (var, tmp[1]);
		      ar_ca_expr_del_reference (tmp[1]);
		    }
		  else
		    {
		      tmp[0] = s_domain_get_value (eman, dom, min);
		      tmp[1] = ar_ca_expr_crt_leq (tmp[0], var);
		      ar_ca_expr_del_reference (tmp[0]);

		      tmp[0] = s_domain_get_value (eman, dom, max);
		      tmp[2] = ar_ca_expr_crt_leq (var, tmp[0]);
		      ar_ca_expr_del_reference (tmp[0]);

		      tmp[0] = ar_ca_expr_crt_and (tmp[1], tmp[2]);
		      ar_ca_expr_del_reference (tmp[1]);
		      ar_ca_expr_del_reference (tmp[2]);
		    }

		  tmp[1] = s_compute_expr_from_dd (ddm, eman, order, 
						   AR_DD_CPNTHSON (dd, i),
						   cache);
		  tmp[2] = ar_ca_expr_crt_and (tmp[0], tmp[1]);
		  ar_ca_expr_del_reference (tmp[0]);
		  ar_ca_expr_del_reference (tmp[1]);

		  if (i == 0) 
		    result = tmp[2];
		  else
		    {
		      tmp[0] = ar_ca_expr_crt_or (result, tmp[2]);
		      ar_ca_expr_del_reference (result);
		      ar_ca_expr_del_reference (tmp[2]);
		      result = tmp[0];
		    }
		}	  
	    }
	  else
	    {
	      for(i = 0; i < arity; i++) 
		{
		  int j;
		  ar_ca_expr *eq;
		  ar_ca_expr *aux2;
		  int min = AR_DD_MIN_OFFSET_NTHSON (dd, i);
		  int max = AR_DD_MAX_OFFSET_NTHSON (dd, i);
		  ar_ca_expr *aux = s_domain_get_value (eman, dom, min);
		  ar_ca_expr *tmp = ar_ca_expr_crt_eq (var, aux);
		  ar_ca_expr_del_reference (aux);

		  for (j = min + 1; j <= max; j++) 
		    {		  
		      aux = s_domain_get_value (eman, dom, j);
		      eq = ar_ca_expr_crt_eq (var, aux);
		      ar_ca_expr_del_reference (aux);
		      aux = ar_ca_expr_crt_or (tmp, eq);
		      ar_ca_expr_del_reference (tmp);
		      ar_ca_expr_del_reference (eq);
		      tmp = aux;
		    }

		  aux2 = s_compute_expr_from_dd (ddm, eman, order, 
						 AR_DD_CPNTHSON (dd, i),
						 cache);
		  aux = ar_ca_expr_crt_and (tmp, aux2);
		  ar_ca_expr_del_reference (aux2);
		  ar_ca_expr_del_reference (tmp);
		  tmp = aux;

		  if (i == 0) 
		    result = tmp;
		  else
		    {
		      aux = ar_ca_expr_crt_or (result, tmp);
		      ar_ca_expr_del_reference (result);
		      ar_ca_expr_del_reference (tmp);
		      result = aux;
		    }
		}	  
	    }
	}
      else
	{
	  int i;
	  for (i = 0; i < arity; i++) 
	    {
	      ar_ca_expr *aux2;
	      ar_ca_expr *aux = s_domain_get_value (eman, dom, i);
	      ar_ca_expr *tmp = ar_ca_expr_crt_eq (var, aux);

	      ar_ca_expr_del_reference (aux);
	      aux = s_compute_expr_from_dd (ddm, eman, order, 
					    AR_DD_EPNTHSON (dd, i),
					    cache);
	      aux2 = ar_ca_expr_crt_and (tmp, aux);
	      ar_ca_expr_del_reference (tmp);
	      ar_ca_expr_del_reference (aux);
	      tmp = aux2;

	      if (i == 0) 
		result = tmp;
	      else
		{
		  aux = ar_ca_expr_crt_or (result, tmp);
		  ar_ca_expr_del_reference (tmp);
		  ar_ca_expr_del_reference (result);
		  result = aux;
		}
	    }
	}

      ar_ca_expr_del_reference (var);
    }

  if (cache && ! ccl_hash_find (cache, dd))
    {
      AR_DD_DUP (dd);
      ccl_hash_insert (cache, ar_ca_expr_add_reference (result));
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_compute_expr_from_dd (ar_ddm ddm, ar_ca_exprman *eman, 
				 const varorder *order, ar_dd dd, 
				 ccl_hash *cache)
{
  ar_ca_expr *result = s_compute_expr_from_dd (ddm, eman, order, dd, cache);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_simplify_with_dd (ar_ddm ddm, ar_ca_exprman *eman, 
			     const varorder *order, 
			     ar_ca_expr *expr, ccl_hash *e2dd, ccl_hash *dd2e)
{
  ar_dd dd = ar_ca_expr_compute_dd (expr, ddm, order, e2dd);
  ar_ca_expr *result = 
    ar_ca_expr_compute_expr_from_dd (ddm, eman, order, dd, dd2e);
  AR_DD_FREE (dd);

  return result;
}

			/* --------------- */

static void
s_delete_dd (ar_dd dd)
{
  AR_DD_FREE (dd);
}

			/* --------------- */

void
ar_ca_expr_build_dd_caches (ccl_hash **p_e2dd, ccl_hash **p_dd2e)
{
  if (p_e2dd)
    *p_e2dd = 
      ccl_hash_create (NULL, NULL, (ccl_delete_proc *) ar_ca_expr_del_reference,
		       (ccl_delete_proc *) s_delete_dd);
  if (p_dd2e)
    *p_dd2e = 
      ccl_hash_create (NULL, NULL, (ccl_delete_proc *) s_delete_dd,
		       (ccl_delete_proc *) ar_ca_expr_del_reference);
}

			/* --------------- */

void
ar_ca_expr_clean_up_dd_caches (ccl_hash *e2dd, ccl_hash *dd2e)
{
  ccl_zdelete (ccl_hash_delete, e2dd);
  ccl_zdelete (ccl_hash_delete, dd2e);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_simplify (ar_ca_expr *expr)
{
  ar_ca_expr *R;
  ccl_list *vars = ar_ca_expr_get_variables (expr, NULL);
  varorder *vo = varorder_create (NULL, NULL, NULL);
  int index = 0;
  ar_ca_exprman *man = ar_ca_expr_get_manager (expr) ;
  ccl_hash *e2dd = NULL;
  ccl_hash *dd2e = NULL;

  ar_ca_expr_build_dd_caches (&e2dd, &dd2e);

  ccl_assert (ar_ca_expr_is_boolean_expr(expr));


  while (! ccl_list_is_empty (vars))
    {
      ar_ca_expr *v = ccl_list_take_first (vars);
      varorder_add_variable (vo, v);
      varorder_set_dd_index (vo, v, index++);
    }
  ccl_list_delete (vars);
  R = ar_ca_expr_simplify_with_dd (DDM, man, vo, expr, e2dd, dd2e);
  if (ar_ca_expr_get_size (R) > ar_ca_expr_get_size (expr))
    {
      ar_ca_expr_del_reference (R);
      R = ar_ca_expr_add_reference (expr);
    }
  varorder_del_reference (vo);
  ar_ca_exprman_del_reference (man);
  ar_ca_expr_clean_up_dd_caches (e2dd, dd2e);

  return R;
}

ar_ca_expr *
ar_ca_expr_rewrite_as_primes (ar_ca_expr *expr)
{
  ar_ca_exprman *eman = ar_ca_expr_get_expression_manager (expr); 
  ccl_list *primes = ar_ca_expr_get_primes_as_expr (expr);  
  ar_ca_expr *R = ar_ca_expr_crt_boolean_constant (eman, 0);
  ar_ca_exprman_del_reference (eman);
  
  while (! ccl_list_is_empty (primes))
    {
      ar_ca_expr *p = ccl_list_take_first (primes);
      ar_ca_expr *tmp = ar_ca_expr_crt_or (R, p);
      ar_ca_expr_del_reference (p);
      ar_ca_expr_del_reference (R);
      R = tmp;
    }
  ccl_list_delete (primes);

  return R;
}

ccl_list * 
ar_ca_expr_get_primes_as_expr (ar_ca_expr *expr)
{
  ar_dd F;
  ccl_list *R = ccl_list_create ();
  ccl_list *vars = ar_ca_expr_get_variables (expr, NULL);
  varorder *vo = varorder_create (NULL, NULL, NULL);
  int index = 0;
  ar_ca_exprman *man = ar_ca_expr_get_manager (expr) ;
  ccl_hash *e2dd = NULL;
  ccl_hash *dd2e = NULL;

  ar_ca_expr_build_dd_caches (&e2dd, &dd2e);

  ccl_assert (ar_ca_expr_is_boolean_expr(expr));

  while (! ccl_list_is_empty (vars))
    {
      ar_ca_expr *v = ccl_list_take_first (vars);
      varorder_add_variable (vo, v);
      varorder_set_dd_index (vo, v, index++);
    }
  ccl_list_delete (vars);

  F = ar_ca_expr_compute_dd (expr, DDM, vo, e2dd);
    
  while (! ar_dd_is_zero (DDM, F))
    {
      ar_dd p = ar_dd_pick_prime (DDM, &F);
      ar_ca_expr *pE = ar_ca_expr_compute_expr_from_dd (DDM, man, vo, p, dd2e);
      AR_DD_FREE (p);
      
      ccl_list_add (R, pE);
    }
  AR_DD_FREE (F);
  varorder_del_reference (vo);
  ar_ca_exprman_del_reference (man);
  ar_ca_expr_clean_up_dd_caches (e2dd, dd2e);

  return R;
}
