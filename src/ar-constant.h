/*
 * ar-constant.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CONSTANT_H
# define AR_CONSTANT_H

# include <ccl/ccl-log.h>
# include "ar-type.h"

typedef struct ar_constant_st ar_constant;
typedef enum ar_constant_kind_enum {
  AR_CST_SCALAR,
  AR_CST_STRUCTURE,
  AR_CST_ARRAY
} ar_constant_kind;

			/* --------------- */

extern void
ar_constant_init(void);
extern void
ar_constant_terminate(void);

			/* --------------- */

extern ar_constant *
ar_constant_crt_boolean(int value);
extern ar_constant *
ar_constant_crt_integer(int value);
extern ar_constant *
ar_constant_crt_symbol(int value, ar_type *type);
extern ar_constant *
ar_constant_crt_structure(ar_identifier **fields, 
			  ar_constant **values, 
			  int width,
			  ar_type *type);
extern ar_constant *
ar_constant_crt_array(ar_constant **values, int width, ar_type *type);

			/* --------------- */

extern int
ar_constant_get_boolean_value(ar_constant *cst);
extern int
ar_constant_get_integer_value(ar_constant *cst);
extern int
ar_constant_get_symbol_value(ar_constant *cst);
extern int
ar_constant_get_width(ar_constant *cst);
extern ar_constant *
ar_constant_get_at(ar_constant *cst, int pos);
extern ar_constant *
ar_constant_get_field(ar_constant *cst, ar_identifier *fname);
extern int
ar_constant_has_field(ar_constant *cst, ar_identifier *fname);

			/* --------------- */

extern ar_constant_kind
ar_constant_get_kind(ar_constant *cst);
extern int
ar_constant_has_base_type(ar_constant *cst, ar_type *type);
extern int
ar_constant_has_type(ar_constant *cst, ar_type *type);
extern ar_type *
ar_constant_get_type(ar_constant *cst);
extern void
ar_constant_set_type(ar_constant *cst, ar_type *type);

			/* --------------- */

extern ar_constant *
ar_constant_add_reference(ar_constant *cst);
extern void
ar_constant_del_reference(ar_constant *cst);

			/* --------------- */

extern int
ar_constant_are_equal(ar_constant *c1, ar_constant *c2);

			/* --------------- */
extern void
ar_constant_log(ccl_log_type log, ar_constant *cst);
extern char *
ar_constant_to_string(ar_constant *cst);

			/* --------------- */

extern int
ar_constant_is_in_domain(ar_constant *cst, ar_type *type);

#endif /* ! AR_CONSTANT_H */
