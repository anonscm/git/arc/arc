/*
 * ar-interp-altarica.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_INTERP_ALTARICA_H
# define AR_INTERP_ALTARICA_H

# include "parser/altarica-tree.h"
# include "ar-interp-altarica-exception.h"
# include "ar-type.h"
# include "ar-expr.h"
# include "ar-node.h"

typedef ar_expr *
ar_member_access_interp (altarica_tree *t, ar_type *type, ar_context *ctx)
  CCL_THROW (altarica_interpretation_exception);

			/* --------------- */

extern int
ar_interp_altarica_tree(altarica_tree *t);

extern void
ar_interp_altarica_constant_decl (altarica_tree *t, ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception);

extern void
ar_interp_altarica_domain_decl (altarica_tree *t, ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception);

extern ar_type *
ar_interp_domain(altarica_tree *t, ar_context *ctx,
		 ar_member_access_interp *mai)
  CCL_THROW(altarica_interpretation_exception);

			/* --------------- */

extern ar_expr *
ar_interp_expression(altarica_tree *t, ar_type *type, ar_context *ctx,
		     ar_member_access_interp *mai)
  CCL_THROW(altarica_interpretation_exception);

extern ar_expr *
ar_interp_constant_expression(altarica_tree *t, ar_type *type, ar_context *ctx,
			      ar_member_access_interp *mai)
  CCL_THROW(altarica_interpretation_exception);

extern int
ar_interp_integer_value(altarica_tree *t, ar_context *ctx,
			ar_member_access_interp *mai)
  CCL_THROW(altarica_interpretation_exception);

extern ar_expr *
ar_interp_boolean_expression (altarica_tree *t, ar_context *ctx,
			      ar_member_access_interp *mai)
  CCL_THROW(altarica_interpretation_exception);

extern ar_expr *
ar_interp_member_access (altarica_tree *t, ar_type *type, ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception);

extern ar_expr *
ar_interp_acheck_member_access (altarica_tree *t, ar_type *type, 
				ar_context *ctx)
  CCL_THROW(altarica_interpretation_exception);

extern ar_identifier *
ar_interp_member_access_as_identifier (altarica_tree *t, ar_context *ctx,
				       ar_member_access_interp *mai)
  CCL_THROW (altarica_interpretation_exception);

			/* --------------- */

extern ar_node *
ar_interp_node(altarica_tree *t, ar_context *ctx) 
  CCL_THROW(altarica_interpretation_exception);

extern ar_node *
ar_interp_node_template(altarica_tree *t, ar_context *ctx, 
			ar_idtable *setting) 
  CCL_THROW(altarica_interpretation_exception);

extern ar_identifier *
ar_interp_symbol_as_identifier(altarica_tree *t);

extern ar_identifier *
ar_interp_id_or_bang_id (altarica_tree *t, ar_context *ctx);

extern ccl_list *
ar_interp_id_list(altarica_tree *t, int unicity);

extern ar_identifier *
ar_interp_identifier_path(altarica_tree *t, ar_context *ctx);

#endif /* ! AR_INTERP_ALTARICA_H */
