/*
 * ar-solver-glucose.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <sat/bitblaster.h>
#include <sat/glucose.h>
#include "ar-util.h"

#include "ar-solver-p.h"

typedef struct ar_glucose_solver_st ar_glucose_solver;
struct ar_glucose_solver_st 
{
  ar_solver super;
  
  glucose_solver *S;
  ar_ca_exprman *eman;
  bf_manager *bm;
  bitblaster *bb;
  varset *vs;

  ar_bounds *obounds;
};

			/* --------------- */

static void
s_solve_constraints (ar_glucose_solver *solver);

static bf_index 
s_new_var (void *solver);

static bf_formula *
s_mk_in_range (bitblaster *bb, const bitvector *bv, 
	       ar_bounds *b, ar_bounds *ob);

			/* --------------- */

static void
s_glucose_solver_delete (ar_solver *S)
{
  ar_glucose_solver *solver = (ar_glucose_solver *) S;

  ccl_pre (solver != NULL);

  varset_unref (solver->vs); /* preceed deletion of bb to be sure that all
				bf have been deleted */
  bitblaster_delete (solver->bb);
  ccl_delete (solver->obounds);
  ar_ca_exprman_del_reference (solver->eman);
  glucose_solver_destroy (solver->S);
}

			/* --------------- */

static void
s_glucose_solver_solve (ar_solver *S)
{
  ar_glucose_solver *solver = (ar_glucose_solver *) S;
  
  ccl_pre (solver != NULL);

  s_solve_constraints (solver);
}

			/* --------------- */

ar_solver *
ar_create_glucose_solver (ar_ca_expr **constraints, int nb_constraints,
			  ar_ca_expr **variables, int nb_variables,
			  ar_backtracking_stack *bstack,
			  ar_solver_start_solution_se_proc *set_start,
			  ar_solver_new_solution_proc *set_add,
			  ar_solver_end_solution_set_proc *set_end)
{
  int i;
  ar_glucose_solver *result = (ar_glucose_solver *)
    ar_allocate_solver (sizeof (ar_glucose_solver), s_glucose_solver_delete,
			s_glucose_solver_solve, variables, nb_variables,
			bstack, set_start, set_add, set_end);
  
  result->S = glucose_solver_create ();
  if (nb_constraints)
    result->eman = ar_ca_expr_get_manager (constraints[0]);
  else if (nb_variables)
    result->eman = ar_ca_expr_get_manager (variables[0]);
  else 
    result->eman = ar_ca_exprman_create ();
  result->vs = varset_create ();
  result->bb = bitblaster_create (result->eman, s_new_var, result);
  result->bm = bitblaster_get_bf_manager (result->bb);

  for (i = 0; i < nb_constraints; i++)
    {
      bf_formula *bf = 
	bitblaster_translate_boolean_expr (result->bb, result->vs, 
					   constraints[i]);      
      if (bf_is_false (bf))
	glucose_solver_add_clause_0 (result->S);
      else
	bf_to_cnf (result->bm, bf, glucose_add_clause_to_solver, result->S, 1);
      bf_unref (bf);
    }
  result->obounds = ccl_new_array (ar_bounds, nb_variables);

  for (i = 0; i < nb_variables; i++)
    {
      ar_ca_expr *v = variables[i];
      const ar_ca_domain *D = ar_ca_expr_variable_get_domain (v);
      ar_ca_expr *indom = ar_ca_expr_crt_in_domain (v, D);
      bf_formula *bf = 
	bitblaster_translate_boolean_expr (result->bb, result->vs, indom);

      if (bf_is_false (bf))
	glucose_solver_add_clause_0 (result->S);
      else
	bf_to_cnf (result->bm, bf, glucose_add_clause_to_solver, result->S, 1);
      bf_unref (bf);
      ar_ca_domain_get_bounds (D, &result->obounds[i]);

      if (! varset_cache_find (result->vs, v))
	{
	  bitblaster_translate_expr (result->bb, result->vs, v);
	  ccl_assert (varset_cache_find (result->vs, v));
	}
      ar_ca_expr_del_reference (indom);
    }

  return (ar_solver *) result;
}

			/* --------------- */

static bf_literal 
s_cnfcache (bf_formula *f, bf_literal l, void *cachedata)
{
  bf_literal result = l;
  ccl_hash *cache = cachedata;

  if (l == BF_UNDEF_INDEX)
    {
      if (ccl_hash_find (cache, f))
	result = (bf_literal) (intptr_t) ccl_hash_get (cache);
    }
  else
    {
      ccl_assert (! ccl_hash_find (cache, f));
      ccl_hash_find (cache, f);
      bf_ref (f);
      ccl_hash_insert (cache, (void *) (intptr_t) l);
    }

  return result;
}

static void 
s_initialize_variables (ar_glucose_solver *solver, ccl_hash *cnfcache)
{
  int i;
  
  for (i = 0; i < NB_VARIABLES (solver); i++)
    {
      ar_ca_expr *v = VARIABLES (solver, i);

      if (varset_cache_find (solver->vs, v))
	{
	  ar_bounds *b = BOUNDS (solver, i);	  
	  ar_bounds *ob = &solver->obounds[i];	  
	  bf_formula *bf = NULL;

	  if (! ar_ca_expr_is_boolean_expr (v))
	    {
	      if (ob->min != b->min || ob->max != b->max)
		{
		  const bitvector *bv = varset_cache_get (solver->vs);
		  bf = s_mk_in_range (solver->bb, bv, b, ob);
		}
	    }
	  else if (b->min == b->max)
	    {
	      const bitvector *bv = varset_cache_get (solver->vs);
	      bf_formula *var = bitvector_get_bit (bv, 0);
	      bf = bf_ref (var);
	      if (! b->min)
		bf = bf_crt_not (solver->bm, bf);
	    }

	  if (bf != NULL)
	    {
	      bf_to_cnf_with_cache (solver->bm, bf, 
				    s_cnfcache, cnfcache, 
				    glucose_add_clause_to_solver, 
				    solver->S, 1);
	      bf_unref (bf);	  
	    }
	}
      else
	{
	  ccl_assert (varset_cache_find (solver->vs, v));
	}
    }
}

static void
s_solve_constraints (ar_glucose_solver *solver)
{
  int i;
  int terminated = 0;
  bf_index gid;
  ccl_hash *cnfcache;

  if (! glucose_solver_solve (solver->S))
    return;

  gid = glucose_solver_new_var (solver->S);
  glucose_solver_set_group (solver->S, gid);
  cnfcache = ccl_hash_create (NULL, NULL, (ccl_delete_proc *) bf_unref, NULL);

  s_initialize_variables (solver, cnfcache);

  while (! terminated)
    {
      terminated = ! glucose_solver_solve (solver->S);

      if (!terminated)
	{
	  int Msize;
	  char *M = glucose_solver_get_model (solver->S, &Msize);

	  ar_backtracking_stack_choice_point (BSTACK (solver));
	  for (i = 0; i < NB_VARIABLES (solver); i++)
	    {
	      ar_ca_expr *v = VARIABLES (solver, i);
	      ar_bounds *b = BOUNDS (solver, i);
	      int value = varset_get_value_as_int (solver->vs, v, M, Msize);
	      ar_bounds_save (b, BSTACK (solver));
	      ar_bounds_set (b, value, value);	      
	    }

	  terminated = ! SET_ADD (solver);

	  if (!terminated)
	    {
	      bf_formula *bC;
	      ar_ca_expr *C = ar_ca_expr_crt_boolean_constant (solver->eman, 0);
	      for (i = 0; i < NB_VARIABLES (solver); i++)
		{
		  ar_ca_expr *v = VARIABLES (solver, i);
		  ar_ca_expr *val = 
		    varset_get_value_of (solver->vs, v, M, Msize);
		  ar_ca_expr *neq = ar_ca_expr_crt_neq (v, val);
		  ar_ca_expr *aux = ar_ca_expr_crt_or (C, neq);
		  ar_ca_expr_del_reference (C);
		  ar_ca_expr_del_reference (val);
		  ar_ca_expr_del_reference (neq);
		  C = aux;
		}	      
	      bC = bitblaster_translate_boolean_expr (solver->bb, solver->vs, 
						      C);
	      ar_ca_expr_del_reference (C);

	      if (bf_is_false (bC))
		terminated = 1;
	      else
		{
		  bf_to_cnf_with_cache (solver->bm, bC, 
					s_cnfcache, cnfcache, 
					glucose_add_clause_to_solver,
					solver->S, 1);
		}
	      bf_unref (bC);
	    }
	  ccl_delete (M);
	  ar_backtracking_stack_restore_last (BSTACK (solver));
	}
    }
  glucose_solver_clear_group (solver->S);
  if (0 && ccl_debug_is_on)
    {
      ccl_debug ("nb clauses %d G=%d\n", 
		 glucose_solver_get_nb_clauses (solver->S)
		 + glucose_solver_get_nb_learnts (solver->S), gid);
    }
  glucose_solver_gc (solver->S);  
  ccl_hash_delete (cnfcache);
}

			/* --------------- */

static bf_index 
s_new_var (void *data)
{
  ar_glucose_solver *S = data;
  bf_index res = glucose_solver_new_var (S->S);

  return res;
}

static bf_formula *
s_mk_in_range (bitblaster *bb, const bitvector *bv, 
	       ar_bounds *b, ar_bounds *ob)
{
  bitvector *r = NULL;
  bf_formula *result;

  if (b->min != ob->min)
    {
      bitvector *bvmin = bitblaster_mk_integer (bb, b->min - 1);
      r = bitblaster_mk_lt (bb, bvmin, bv);
      bitvector_delete (bvmin);
    }

  if (b->max != ob->max)
    {
      bitvector *tmp = bitblaster_mk_integer (bb, b->max + 1);
      bitvector *lt1 = bitblaster_mk_lt (bb, bv, tmp);
      bitvector_delete (tmp);
      if (r == NULL)
	r = lt1;
      else
	{
	  tmp = bitblaster_mk_and (bb, r, lt1);
	  bitvector_delete (r);
	  bitvector_delete (lt1);
	  r = tmp;
	}
    }
  result = bf_ref (bitvector_get_bit (r, 0));

  bitvector_delete (r);

  return result;
}
