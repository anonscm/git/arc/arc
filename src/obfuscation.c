/*
 * obfuscation.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-event.h"
#include "ar-bang-ids.h"
#include "ar-acheck-builtins.h"
#include "sas/sas-laws.h"
#include "obfuscation-p.h"

static void
s_prefill_translation_table (obfuscation *o);

static void 
s_translate (ar_identifier_translator *it, ccl_log_type log, 
	     ar_identifier_context ctx, const char *id);

			/* --------------- */

obfuscation *
obfuscation_create (size_t size, 
		    char * (*translate) (obfuscation *o, 
					 ar_identifier_context ctx, 
					 const char *id),
		    void (*destroy) (obfuscation *o))
{
  obfuscation *result;

  ccl_pre (size >= sizeof (obfuscation));

  result = (obfuscation *) ccl_calloc (size, 1);
  result->super.translate = s_translate;
  result->translate = translate;
  result->H = ccl_hash_create (ccl_string_hash,
			       ccl_string_compare,
			       ccl_string_delete,
			       ccl_string_delete);
  result->destroy = destroy;
  s_prefill_translation_table (result);

  return result;
}

			/* --------------- */

void
obfuscation_delete (obfuscation *o)
{
  ccl_pre (o != NULL);
  
  ccl_hash_delete (o->H);
  if (o->destroy != NULL)
    o->destroy (o);
  ccl_delete (o); 
}

			/* --------------- */

ar_identifier_translator *
obfuscation_get_identifier_translator (obfuscation *o)
{
  return (ar_identifier_translator *) o;
}


			/* --------------- */

void
obfuscation_dump_translation_table (const char *filename, obfuscation *o, 
				    int reverse)
{
  FILE *output = fopen (filename, "w");

  if (output != NULL)
    {
      char *s[2];
      int i1 = reverse ? 0 : 1;
      int i2 = (i1 + 1) % 2;
      char *epsilon = ar_identifier_to_string (AR_EPSILON_ID);
      ccl_hash_entry_iterator *ei = ccl_hash_get_entries (o->H);

      while (ccl_iterator_has_more_elements (ei))
	{
	  ccl_hash_entry e = ccl_iterator_next_element (ei);
	  
	  if (strcmp ((char *) e.object, epsilon) == 0)
	    continue;
	  s[i1]= (char *) e.object; 
	  s[i2] = (char *) e.key; 
	  fprintf (output, "s/\\<%s\\>/%s/g\n", s[0], s[1]);
	}
      fflush (output);
      fclose (output);
      ccl_string_delete (epsilon);
    }
  else
    {
      ccl_error ("can't create file '%s'\n", filename);
    }
}

			/* --------------- */

static char *
s_translate_actually (obfuscation *o, ar_identifier_context ctx, const char *id)
{
  char *translated_id = ccl_hash_get_with_key (o->H, id);

  if (translated_id == NULL)
    {
      char *newid = ccl_string_dup (id);
      translated_id = o->translate (o, ctx, newid);
      ccl_assert (! ccl_hash_find (o->H, newid));
      ccl_hash_find (o->H, newid);
      ccl_hash_insert (o->H, translated_id);
    }

  return translated_id;
}

			/* --------------- */

static int
s_must_add_quotes (const char *s)
{
  if (!(('a' <= *s && *s <= 'z') || ('A' <= *s && *s <= 'Z') || *s == '_'))
    return 1;

  while (*(++s))
    {
      if (!(('a' <= *s && *s <= 'z') ||
	    ('A' <= *s && *s <= 'Z') ||
	    ('0' <= *s && *s <= '9') || *s == '_'))
	return 1;
    }

  return 0;
}

			/* --------------- */

static void 
s_translate (ar_identifier_translator *it, ccl_log_type log, 
	     ar_identifier_context ctx, const char *id)
{
  const char *fmt = "%s";
  const char *translated_id = NULL;
  obfuscation *o = (obfuscation *) it;

  switch (ctx)
    {
    case AR_IC_BANG_TYPE:      
      {
	ar_bang_t bang;
	
	if (ar_bang_str_to_bang_t (id, &bang))
	  translated_id = id;
	else
	  translated_id = s_translate_actually (o, ctx, id);
      }
      break;

    case AR_IC_NODE_ATTR:
      break;

    case AR_IC_STATE_VAR_ATTR:
    case AR_IC_FLOW_VAR_ATTR:
    case AR_IC_EVENT_ATTR:
      if (strcmp (id, "public") == 0 || strcmp (id, "private") == 0 ||
	  strcmp (id, "parent") == 0 || 
	  (ctx == AR_IC_FLOW_VAR_ATTR && 
	   (strcmp (id, "in") == 0 ||strcmp (id, "out") == 0 ||
	    strcmp (id, "counter") == 0)))
	translated_id = id;
      break;

    case AR_IC_ACHECK_SET:
      {
	int i;
	for (i = 0; i < LAST_AND_UNUSED_AR_ACHECK_BUILTIN; i++)
	  {
	    if (strcmp (id, AR_ACHECK_BUILTIN_NAMES[i]) == 0)
	      {
		translated_id = id;
		break;
	      }
	  }
      }
      break;

    case AR_IC_ACHECK_FILENAME:
    case AR_IC_ACHECK_PROJECT_TGT:
    case AR_IC_ARC_OUT_FILENAME:
      translated_id = id;
      break;

    case AR_IC_MEC5_COMMAND:
      {
	char *sp = strchr (id, ' ');
	*sp = '\0';
	ccl_log (log, "%s ", id);
	*sp = ' ';
	translated_id = s_translate_actually (o, ctx, sp + 1);
      }
      break;

    case AR_IC_ARC_STRING:
      translated_id  = ccl_hash_get_with_key (o->H, id);
      if (translated_id == NULL)
	translated_id = id;
      break;

    case AR_IC_EXTERN_DECL:
    case AR_IC_EXTERN_FUNCTION:
      {
	const char **p;
	static const char *known_ids[] = {
	  "preemptible", "parameter", "law", "predicate", "property",
	  "bucket", "priority",
	  "uniform", "normal", "lognormal", 
#define SAS_LAW(l) #l,
	  SAS_LAWS
#undef SAS_LAW
	  NULL };

	for (p = known_ids; *p && translated_id == NULL; p++)
	  if (strcmp (*p, id) == 0)
	    translated_id = id;
      }
      break;
      
    default:	   
      break;
    }

  if (translated_id == NULL)
    translated_id = s_translate_actually (o, ctx, id);
  if (ctx != AR_IC_MEC5_COMMAND && s_must_add_quotes (translated_id))
    {
      if (ctx == AR_IC_ARC_STRING)
	fmt = "\"%s\"";
      else
	fmt = "'%s'";
    }

  ccl_log (log, fmt, translated_id);
}

			/* --------------- */

static void
s_prefill_translation_table (obfuscation *o)
{
  char *epsilon = ar_identifier_to_string (AR_EPSILON_ID);
  char *epsilon2 = ccl_string_dup (epsilon);

  ccl_hash_find (o->H, epsilon);
  ccl_hash_insert (o->H, epsilon2);
}

