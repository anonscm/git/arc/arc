/*
 * ar-semantics.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_SEMANTICS_H
# define AR_SEMANTICS_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

# include <ccl/ccl-config-table.h>
# include "ar-interp-altarica-exception.h"
# include "ar-node.h"

typedef enum {
  AR_SEMANTICS_FLATTENED = 0,
  AR_SEMANTICS_CONSTRAINT_AUTOMATON,
  AR_SEMANTICS_TRANSITION_SYSTEM,
  AR_SEMANTICS_RELATIONS,
} ar_semantics_type;

CCL_DECLARE_EXCEPTION (domain_cardinality_exception, exception);
CCL_DECLARE_EXCEPTION (abstract_type_exception, exception);

extern void
ar_semantics_init (ccl_config_table *table);

extern void
ar_semantics_terminate (void);

extern void *
ar_semantics_get (ar_node *node, ar_semantics_type type)
  CCL_THROW ((altarica_interpretation_exception, domain_cardinality_exception,
	      abstract_type_exception));

extern void
ar_semantics_cleanup (ar_node *node, ar_semantics_type type);

#endif /* ! AR_SEMANTICS_H */
