/*
 * ar-interp-acheck.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_INTERP_ACHECK_H
# define AR_INTERP_ACHECK_H

# include <ccl/ccl-config-table.h>
# include "parser/altarica-tree.h"
# include "ctlstar.h"
# include "ar-state-graph-semantics.h"

extern int
ar_interp_acheck_tree (altarica_tree *t, ccl_config_table *conf);

extern ctlstar_formula * 
ar_interp_ctlstar_formula (altarica_tree *t, ar_sgs *sgs, int allow_paths,
			   ar_idtable *tmpvars);

extern ar_sgs_set *
ar_interp_eval_ctlstar_formula (ctlstar_formula *spec, ar_sgs *sgs, 
				ccl_config_table *conf);

#endif /* ! AR_INTERP_ACHECK_H */
