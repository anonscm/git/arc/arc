/*
 * ar-interp-altarica-domain.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "ar-bang-ids.h"
#include "mec5/mec5.h"
#include "mec5/mec5-interp-ast.h"
#include "ar-interp-altarica-p.h"

static ar_type *
s_interp_array_domain (altarica_tree *t, ar_context *ctx,
		       ar_member_access_interp *mai)
{
  ar_type *result = NULL;

  ccl_pre (t->node_type == AR_TREE_ARRAY_DOMAIN);

  result = ar_interp_domain (t->child,ctx,mai);

  ccl_try(altarica_interpretation_exception)
    {
      for (t = t->child->next; t; t = t->next)
	{
	  int asize = ar_interp_integer_value (t, ctx, mai);
	  ar_type *tmp = ar_type_crt_array (result, asize);
	  ar_type_del_reference (result);
	  result = tmp;
	}
    }  
  ccl_catch
    {      
      ar_type_del_reference (result);
      ccl_rethrow();
    }  
  ccl_end_try;

  return result;
}

			/* --------------- */

static ar_type *
s_interp_domain_by_name(altarica_tree *t, ar_context *ctx)
{
  ar_identifier *name = ar_interp_symbol_as_identifier(t);
  ar_type *result = ar_context_get_type(ctx,name);

  ar_identifier_del_reference(name);

  if( result == NULL )    
    INTERP_EXCEPTION_MSG ((t, "undefined domain '%s'\n", t->value.id_value));

  return result;
}

			/* --------------- */

static ar_type *
s_interp_range(altarica_tree *t, ar_context *ctx, ar_member_access_interp *mai)
{
  int min;
  int max;
  ar_type *result;


  ccl_pre( t->node_type == AR_TREE_RANGE );

  min = ar_interp_integer_value(t->child,ctx,mai);
  max = ar_interp_integer_value(t->child->next,ctx,mai);
  if( max < min )
    INTERP_EXCEPTION_MSG((t, "inverted range bounds [%d,%d].\n", min, max));
  result = ar_type_crt_range(min,max);

  return result;
}

			/* --------------- */

static ar_type *
s_interp_symbol_set (altarica_tree *t, ar_context *ctx)
{
  ar_type *result;

  ccl_pre (IS_LABELLED_BY (t, SYMBOL_SET));

  result = ar_type_crt_symbol_set ();

  ccl_try (altarica_interpretation_exception)
    {
      for (t = t->child; t != NULL; t = t->next)
	{
	  ar_identifier *sym = ar_interp_symbol_as_identifier (t);

	  if (ar_context_has_slot (ctx, sym))
	    {
	      ar_error (t, "identifier '");
	      ar_identifier_log (CCL_LOG_ERROR, sym);
	      ccl_error ("' already exists as a different object.\n");
	      ar_identifier_del_reference (sym);
	      INTERP_EXCEPTION ();
	    }
	  ar_type_symbol_set_add_value (result, sym);
	  ar_identifier_del_reference (sym);
	}
    }
  ccl_catch
    {
      ar_type_del_reference (result);
      ccl_rethrow ();
    }
  ccl_end_try;

  return result;
}

			/* --------------- */

static void
s_interp_structure_field(altarica_tree *t, ar_type *structure, ar_context *ctx,
			 ar_member_access_interp *mai)
{
  ar_type *ftype;

  ccl_pre( t->node_type == AR_TREE_STRUCTURE_FIELDS );

  t = t->child; ccl_assert( t->node_type == AR_TREE_ID_LIST );

  ftype = ar_interp_domain(t->next,ctx,mai);

  ccl_try(altarica_interpretation_exception)
    {
      for(t = t->child; t != NULL; t = t->next)
	{
	  ar_identifier *fname = ar_interp_symbol_as_identifier(t);

	  if( ar_type_struct_has_field(structure,fname) )
	    {
	      ar_identifier_del_reference(fname);
	      INTERP_EXCEPTION_MSG((t, "duplicated field '%s'\n",
				    t->value.id_value));
	    }

	  ar_type_struct_add_field(structure,fname,ftype);
	  ar_identifier_del_reference(fname);
	}
    }
  ccl_catch
    {
      ar_type_del_reference(ftype);
      ccl_rethrow();
    }
  ccl_end_try;

  ar_type_del_reference(ftype);
}

			/* --------------- */

static ar_type *
s_interp_structure(altarica_tree *t, ar_context *ctx,
		   ar_member_access_interp *mai)
{
  ar_type *result;

  ccl_pre( t->node_type == AR_TREE_STRUCTURE );
  
  result = ar_type_crt_structure();

  ccl_try(altarica_interpretation_exception)
    {
      for(t = t->child; t != NULL; t = t->next)
	s_interp_structure_field(t,result,ctx,mai);
    }
  ccl_catch
    {
      ar_type_del_reference(result);
      ccl_rethrow();
    }
  ccl_end_try;
  
  return result;
}

			/* --------------- */

static ar_type *
s_interp_bang_domain(altarica_tree *t, ar_context *ctx, 
		     ar_member_access_interp *mai)
{
  ar_type *result = NULL;
  ar_identifier *nodename;

  ccl_pre (IS_LABELLED_BY (t, BANG_ID));

  nodename = ar_interp_symbol_as_identifier (t->child);

  if (ar_model_has_node (nodename))
    {
      ar_type * (*creator) (ar_node *) = NULL;
      ar_node *node = ar_model_get_node (nodename);
      ccl_ustring id = t->child->next->value.id_value;
      ar_bang_t bang;

      if (ar_bang_str_to_bang_t (id, &bang))
	{
	  switch (bang)
	    {
	    case AR_BANG_EVENTS:
	      creator = ar_node_get_event_type;
	      break;
	    case AR_BANG_CONFIGURATIONS:
	      creator = ar_node_get_configuration_type;
	      break;
	    case AR_BANG_SUPER_CONFIGURATIONS:
	      creator = ar_node_get_configuration_base_type;
	      break;
	    default:
	      break;
	    }
	}

      if (creator)
	{
	  ccl_try (altarica_interpretation_exception)
	    {
	      ar_mec5_build_bang_relation (node, bang);
	      result = creator (node);
	    }
	  ccl_no_catch;
	}
      else
	{
	  ccl_error ("%s:%d: error: undefined bang type '%s'.\n", t->filename, 
		     t->line, id);      
	}
      ar_node_del_reference (node);
    }
  else
    {
      ccl_error ("%s:%d: error: undefined node type '%s'.\n", 
		 t->filename, t->line, t->child->value.id_value);      
    }
  ar_identifier_del_reference (nodename);
  if (result == NULL)
    INTERP_EXCEPTION ();
  
  return result;
}

			/* --------------- */

ar_type *
ar_interp_domain (altarica_tree *t, ar_context *ctx, 
		  ar_member_access_interp *mai)
  CCL_THROW (altarica_interp_exception) 
{
  ar_type *result = NULL;

  switch (t->node_type) 
    {
    case AR_TREE_ARRAY_DOMAIN : 
      result = s_interp_array_domain (t, ctx, mai); 
      break;

    case AR_TREE_IDENTIFIER : 
      result = s_interp_domain_by_name (t, ctx); 
      break;

    case AR_TREE_RANGE : 
      result = s_interp_range (t, ctx, mai); 
      break;

    case AR_TREE_SYMBOL_SET : 
      result = s_interp_symbol_set (t, ctx); 
      break;

    case AR_TREE_BOOLEANS : 
      result = ar_type_add_reference (AR_BOOLEANS); 
      break;

    case AR_TREE_INTEGERS : 
      result = ar_type_add_reference (AR_INTEGERS); 
      break;

    case AR_TREE_STRUCTURE : 
      result = s_interp_structure (t, ctx, mai); 
      break;

    case AR_TREE_BANG_ID : 
      result = s_interp_bang_domain (t, ctx, mai); 
      break;

    default : 
      INVALID_NODE_TYPE (); 
      break;
    }

  ccl_post (result != NULL);

  return result;
}
