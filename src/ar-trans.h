/*
 * ar-trans.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_TRANS_H
# define AR_TRANS_H

# include "ar-expr.h"
# include "ar-event.h"

typedef struct ar_trans_st ar_trans;
typedef struct ar_trans_assignment_st {
  ar_expr *lvalue;
  ar_expr *value;
  struct ar_trans_assignment_st *next;
} ar_trans_assignment;

extern ar_trans *
ar_trans_create(ar_expr *g, ar_event *e);

extern ar_trans *
ar_trans_add_reference(ar_trans *t);

extern void
ar_trans_del_reference(ar_trans *t);

extern ar_expr * 
ar_trans_get_guard(ar_trans *t);

extern void
ar_trans_set_guard(ar_trans *t, ar_expr *g);

extern ar_event *
ar_trans_get_event(ar_trans *t);

extern int
ar_trans_has_epsilon_event (ar_trans *t);

extern int
ar_trans_has_event(ar_trans *t, ar_event *ev);

extern void
ar_trans_set_event(ar_trans *t, ar_event *e);

extern void
ar_trans_add_assignment(ar_trans *t, ar_expr *lvalue, ar_expr *value);

extern ar_expr * 
ar_trans_get_assignment(ar_trans *t, ar_expr *lvalue);

extern int
ar_trans_is_assigned(ar_trans *t, ar_expr *lvalue);

extern const ar_trans_assignment *
ar_trans_get_assignments(ar_trans *t);

extern void
ar_trans_log(ccl_log_type log, ar_trans *t);

extern void
ar_trans_log_gen(ccl_log_type log, ar_trans *t, 
		 void (*idlog)(ccl_log_type log, const ar_identifier *id));

extern ar_trans *
ar_trans_dup(ar_trans *t);

extern int
ar_trans_is_epsilon_loop (ar_trans *t);

#endif /* ! AR_TRANS_H */
