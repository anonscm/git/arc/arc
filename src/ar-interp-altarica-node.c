/*
 * ar-interp-altarica-node.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-interp-altarica-p.h"

			/* --------------- */
static int
s_get_slot_depth(ar_context *ctx, ar_context_slot *slot)
{
  int result;
  ar_identifier *tmp;  
  ar_identifier *id = ar_context_slot_get_name(slot);

  while( ar_identifier_get_path_length(id) > 0 )
    {
      uint32_t flags;
      ar_context_slot *sl = ar_context_get_slot(ctx,id);

      ccl_assert( sl != NULL );
      flags = ar_context_slot_get_flags(sl);
      ar_context_slot_del_reference(sl);

      if( (flags&AR_SLOT_FLAG_NODE) != 0 )
	break;
      
      tmp = ar_identifier_get_prefix(id);
      ar_identifier_del_reference(id);
      id = tmp;
    }

  result = ar_identifier_get_path_length(id);
  ar_identifier_del_reference(id);

  return result;
}

			/* --------------- */

static void
s_check_visibility_of_variables (altarica_tree *t, ar_context *ctx, ar_expr *a)
{
  int error = 0;
  ccl_pair *p;
  ccl_list *slots = ar_expr_get_variables (a, ctx, NULL);

  for (p = FIRST (slots); p; p = CDR (p))
    {
      ar_context_slot *sl = (ar_context_slot *) CAR (p);
      ar_identifier *name = ar_context_slot_get_name (sl);
      int depth = s_get_slot_depth (ctx, sl);
      uint32_t flags = ar_context_slot_get_flags (sl);
     
      if (((flags & AR_SLOT_FLAG_PRIVATE) != 0 && depth > 0) ||
	  ((flags & AR_SLOT_FLAG_PARENT) != 0 && depth > 1))
	{
	  const char *s = "private";
	  const char *obj = "variable";
	  error = 1;
	  ccl_error ("%s:%d: error: '", t->filename, t->line);
	  ar_identifier_log (CCL_LOG_ERROR, name);
	  if ((flags & AR_SLOT_FLAG_PARENT) != 0)
	    s = "parent-only";
	  if ((flags & AR_SLOT_FLAG_PARAMETER) != 0)
	    obj = "parameter";
	  ccl_error ("' is a %s %s.\n", s, obj);
	}
      
      ar_identifier_del_reference (name);
      ar_context_slot_del_reference (sl);
    }
  ccl_list_delete (slots);

  if (error)
    INTERP_EXCEPTION ();
}

			/* --------------- */

static ccl_list *
s_interp_attributes(altarica_tree *t)
{
  ccl_list *result;

  ccl_pre( IS_LABELLED_BY(t,ATTRIBUTES) );

  if( t->child == NULL ) 
    result = ccl_list_create();
  else
    result = ar_interp_id_list(t->child,0);

  return result;
}

			/* --------------- */

static void
s_interp_node_attributes(altarica_tree *t, ar_node *node)
{
  ccl_list *attr = s_interp_attributes(t);

  while( ! ccl_list_is_empty(attr) )
    {
      ar_identifier *id = (ar_identifier *)ccl_list_take_first(attr);
      ar_node_add_attribute(node,id);
      ar_identifier_del_reference(id);
    }
  ccl_list_delete(attr);
}

			/* --------------- */

static const char *
s_look_for_symbol(ar_context *ctx, ar_identifier *id)
{
  const char *result = NULL;
  ar_context_slot *slot = ar_context_get_slot(ctx,id);

  if( slot != NULL )
    {
      uint32_t flags = ar_context_slot_get_flags(slot);
      if( flags & AR_SLOT_FLAG_STATE_VAR )
	result = "state variable";
      else if( flags & AR_SLOT_FLAG_FLOW_VAR )
	result = "flow variable";
      else if( flags & AR_SLOT_FLAG_NODE )
	result = "node";
      else if( flags & AR_SLOT_FLAG_PARAMETER )
	result = "parameter";
      else
	ccl_throw_no_msg (internal_error);
      ar_context_slot_del_reference(slot);
    }

  return result;
}

			/* --------------- */

static ccl_list *
s_interp_flags(altarica_tree *t, uint32_t *current_flags)
{
  int vis = 0;
  int dir = 0;
  ccl_pair *pa;
  ccl_list *attr = s_interp_attributes(t);
  uint32_t flags = *current_flags;

  for(pa = FIRST(attr); pa; pa = CDR(pa))
    {
      ar_identifier *a = (ar_identifier *)CAR(pa);

      if( a == AR_ATTR_PUBLIC ) 
	{ 
	  if( ! (flags & AR_SLOT_FLAG_PUBLIC) )
	    vis++;
	  flags |= AR_SLOT_FLAG_PUBLIC; 
	}
      else if( a == AR_ATTR_PARENT ) 
	{
	  if( ! (flags & AR_SLOT_FLAG_PARENT) )
	    vis++;
	  flags |= AR_SLOT_FLAG_PARENT;
	}
      else if( a == AR_ATTR_PRIVATE ) 
	{
	  if( ! (flags & AR_SLOT_FLAG_PRIVATE) )
	    vis++;

	  flags |= AR_SLOT_FLAG_PRIVATE;
	}
      else if( a == AR_ATTR_IN ) 
	{
	  if( ! (flags & AR_SLOT_FLAG_IN) )
	    dir++;
	  flags |= AR_SLOT_FLAG_IN;
	}
      else if( a == AR_ATTR_OUT ) 
	{
	  if( ! (flags & AR_SLOT_FLAG_OUT) )
	    dir++;
	  flags |= AR_SLOT_FLAG_OUT;
	}
    }


  if( vis > 1 )
    {
      ccl_list_clear_and_delete(attr,
				(ccl_delete_proc *)
				ar_identifier_del_reference);
      INTERP_EXCEPTION_MSG((t, "several visibility attributes are "
			    "defined here.\n"));
    }

   if( dir > 1 )
    {
      ccl_list_clear_and_delete(attr,
				(ccl_delete_proc *)
				ar_identifier_del_reference);
      INTERP_EXCEPTION_MSG((t, "several orientation attributes are "
			    "defined here.\n"));
    }

   if(!(flags&(AR_SLOT_FLAG_PUBLIC|AR_SLOT_FLAG_PARENT|AR_SLOT_FLAG_PRIVATE)))
    {
      if( flags & AR_SLOT_FLAG_STATE_VAR ) 
	flags |= AR_SLOT_FLAG_DFLT_STATE_VIS;
      else if( flags & AR_SLOT_FLAG_FLOW_VAR )
	flags |= AR_SLOT_FLAG_DFLT_FLOW_VIS;
      else if( flags & AR_SLOT_FLAG_EVENT )
	flags |= AR_SLOT_FLAG_DFLT_EVENT_VIS;
      else
	ccl_throw_no_msg (internal_error);
    }

   *current_flags = flags;

   return attr;
}

			/* --------------- */

static void
s_interp_node_variables(altarica_tree *nf, ar_node *node)
{
  int error = 0;
  int is_flow;
  altarica_tree *t;
  ar_context *ctx;

  ccl_pre( IS_LABELLED_BY(nf,VARIABLES_DECL) );

  is_flow = IS_LABELLED_BY(nf->child,FLOW);
  ctx = ar_node_get_context(node);

  for(t = nf->child->next; t && ! error; t = t->next)
    {
      ccl_list * volatile varnames = NULL;
      ar_type * volatile type = NULL;
      ccl_list * volatile attr = NULL;

      ccl_pre( IS_LABELLED_BY(t,VAR_DECL) );

      ccl_try(altarica_interpretation_exception)
	{
	  uint32_t flags = 0;
	  ccl_pair *pv;

	  varnames = ar_interp_id_list(t->child,0);
	  type = ar_interp_domain(t->child->next, ctx,ar_interp_member_access);

	  if( is_flow ) 
	    flags |= AR_SLOT_FLAG_FLOW_VAR;
	  else 
	    flags |= AR_SLOT_FLAG_STATE_VAR;

	  attr = s_interp_flags(t->child->next->next,&flags);

	  for(pv = FIRST(varnames); pv; pv = CDR(pv))
	    {
	      ar_identifier *v = (ar_identifier *)CAR(pv);
	      const char *symbol_type = s_look_for_symbol (ctx,v);

	      if( symbol_type == NULL && ar_type_symbol_set_get_value(v) >= 0 )
		symbol_type = "enum value";

	      if( symbol_type != NULL )
		{		  
		  ccl_error("%s:%d: error: symbol name '",t->filename,t->line);
		  ar_identifier_log(CCL_LOG_ERROR,v);
		  ccl_error("' clashes with the %s with the same name.\n",
			    symbol_type);
		  INTERP_EXCEPTION();
		}

	      ar_node_add_variable(node,v,type,flags,attr);	      
	    }
	}
      ccl_catch 
	{
	  error = 1;
	}
      ccl_end_try;

      if (varnames != NULL)
	ccl_list_clear_and_delete (varnames, (ccl_delete_proc *) 
				   ar_identifier_del_reference);
      if (attr != NULL)
	ccl_list_clear_and_delete (attr, (ccl_delete_proc *) 
				   ar_identifier_del_reference);

      if (type != NULL)
	ar_type_del_reference (type);
    }  
  ar_context_del_reference(ctx);

  if (error)
    INTERP_EXCEPTION ();
}

			/* --------------- */

static ccl_list *
s_generate_array_identifiers(ar_identifier *prefix, int width, int *sizes)
{
  ccl_list *result;
  ccl_list *strresult = ccl_list_create();


  ccl_list_add(strresult,ar_identifier_to_string(prefix));

  if( sizes != NULL )
    {
      int i;

      ccl_assert( width >= 1 );

      for(i = 0; i < width; i++)
	{
	  int j;
	  ccl_list *tmp = ccl_list_create();

	  for(j = 0; j < sizes[i]; j++)
	    {
	      ccl_pair *p;

	      for(p = FIRST(strresult); p; p = CDR(p))
		{
		  char *s = (char *)CAR(p);
		  char *aux = ccl_string_format_new("%s[%d]",s,j);
		  ccl_list_add(tmp,aux);
		}
	    }

	  ccl_list_clear_and_delete(strresult,ccl_string_delete);
	  strresult = tmp;
	}
    }

  {
    ccl_pair *p;

    result = ccl_list_create();
    for(p = FIRST(strresult); p; p = CDR(p))
      {
	ar_identifier *id = ar_identifier_create((char *)CAR(p));
	ccl_list_add(result,id);	
      }
    ccl_list_clear_and_delete(strresult,ccl_string_delete);
  }

  return result;
}

			/* --------------- */

static ar_type *
s_interp_subnode_type(altarica_tree *t, ar_node *node, ar_context *ctx, 
		      ar_node **pmodel, ar_identifier *subname)
{
  ar_type *result = NULL;

  if( IS_LABELLED_BY(t,IDENTIFIER) )
    {
      ar_identifier *model_name = ar_interp_symbol_as_identifier(t);
      ar_node *model = ar_model_get_node(model_name);
      ar_identifier_del_reference(model_name);

      if (model == NULL)	
	INTERP_EXCEPTION_MSG((t, "undefined node type '%s'.\n",
			      t->value.id_value));

      if( ar_node_get_template(model) != NULL )
	{
	  ar_context *modelctx = ar_node_get_context(model);
	  altarica_tree *modeltmpl = ar_node_get_template(model);
	  ar_idtable *params = 
	    ar_node_get_subnode_parameter_setting(node,subname);

	  ccl_try(altarica_interpretation_exception)
	    {
	      *pmodel = ar_interp_node_template(modeltmpl,modelctx,params);
	    }
	  ccl_catch
	    {
	      ccl_error("%s:%d: error: these errors occur while instanciating "
			"the field '",t->filename,t->line);
	      ar_identifier_log(CCL_LOG_ERROR,subname);
	      ccl_error("'.\n");
	      ccl_zdelete(ar_idtable_del_reference,params);
	      ar_context_del_reference(modelctx);
	      ar_node_del_reference(model);
	      ccl_rethrow();
	    }
	  ccl_end_try;

	  ccl_zdelete(ar_idtable_del_reference,params);
	  ar_context_del_reference(modelctx);
	}
      else
	{
	  *pmodel = ar_node_add_reference(model);
	}

      result = ar_node_get_configuration_type(*pmodel);
      ar_node_del_reference(model);
    }
  else if( IS_LABELLED_BY(t,SUBNODE_ARRAY) )
    {
      ar_type *subtype;
      int array_size = ar_interp_integer_value(t->child->next,ctx,
					       ar_interp_member_access);
      
      if( array_size <= 0 )
	INTERP_EXCEPTION_MSG((t, "invalid array size '%d'.\n", array_size));
      subtype = s_interp_subnode_type(t->child,node,ctx,pmodel,subname);
      result = ar_type_crt_array(subtype,array_size);
      ar_type_del_reference(subtype);
    }
  else
    {
      INVALID_NODE_TYPE();
    }

  return result;
}

			/* --------------- */

static void
s_interp_node_subnodes(altarica_tree *nf, ar_node *node)
{
  int error = 0;
  altarica_tree *t;
  ar_context *ctx;

  ccl_pre( IS_LABELLED_BY(nf,SUBNODES_DECL) );
  
  ctx = ar_node_get_context(node);

  for(t = nf->child; t && ! error; t = t->next)
    {
      ccl_list * volatile subnames = NULL;
      ar_node *model = NULL;
      ar_type * volatile node_type = NULL;

      ccl_pre( IS_LABELLED_BY(t,SUBNODES) );

      ccl_try(altarica_interpretation_exception)
	{
	  ccl_pair *p;

	  subnames = ar_interp_id_list (t->child,0 );

	  for(p = FIRST(subnames); p; p = CDR(p))
	    {
	      ar_identifier *n = (ar_identifier *)CAR(p);
	      const char *symbol_type = s_look_for_symbol(ctx,n);

	      if (symbol_type == NULL && ar_type_symbol_set_get_value (n) >= 0)
		symbol_type = "enum value";

	      if (symbol_type != NULL)
		{		  
		  ar_error (t, "subnode name '");
		  ar_identifier_log (CCL_LOG_ERROR, n);
		  ccl_error ("' clashes with the %s with the same name.\n",
			     symbol_type);
		  INTERP_EXCEPTION ();
		}

	      node_type = s_interp_subnode_type (t->child->next, node, ctx,
						 &model, n);
	      ar_node_add_subnode(node,n,model,node_type);
	      ar_type_del_reference(node_type); 
	      node_type = NULL;
	      ar_node_del_reference(model);
	      model = NULL;
	    }
	}
      ccl_catch
	{
	  error = 1;
	}
      ccl_end_try;

      if( subnames != NULL )
	ccl_list_clear_and_delete(subnames,(ccl_delete_proc *)
				  ar_identifier_del_reference);
      ccl_zdelete(ar_node_del_reference,model);
      ccl_zdelete(ar_type_del_reference,node_type);
    }

  ar_context_del_reference(ctx);

  if( error )
    INTERP_EXCEPTION();
}

			/* --------------- */

static void
s_interp_node_assertions(altarica_tree *nf, ar_node *node)
{
  ar_expr *a = NULL;
  ar_context *ctx = ar_node_get_context(node);

  ccl_pre( nf->node_type == AR_TREE_ASSERTIONS_DEF );

  ccl_try(altarica_interpretation_exception)
    {
      nf = nf->child;
      while( nf != NULL )
	{
	  a = ar_interp_boolean_expression (nf, ctx, ar_interp_member_access);

	  s_check_visibility_of_variables (nf, ctx, a);
	  ar_node_add_assertion (node, a);	  
	  ar_expr_del_reference(a);
	  a = NULL;

	  nf = nf->next;
	}  
      ar_context_del_reference(ctx);
    }
  ccl_catch
    {
      if( a != NULL )
	ar_expr_del_reference(a);
      ar_context_del_reference(ctx);
      ccl_rethrow();
    }
  ccl_end_try;
}

			/* --------------- */

static ccl_list *
s_interp_assignment_list (altarica_tree *t, ar_node *node, 
			  ccl_list *constraints)
{
  int error = 0;
  ccl_list *result = ccl_list_create ();
  ar_context *ctx = ar_node_get_context (node);

  while (t != NULL && !error)
    {
      ar_type *type = NULL;
      ar_expr *slot_expr = NULL;
      ar_expr *value = NULL;

      ccl_assert (ccl_imply (constraints == NULL, 
			     IS_LABELLED_BY (t, ASSIGNMENT)));

      ccl_try (altarica_interpretation_exception)
	{
	  if (IS_LABELLED_BY (t, ASSIGNMENT))
	    {
	      slot_expr = ar_interp_member_access (t->child, NULL, ctx); 
	      type = ar_expr_get_type(slot_expr); 
	      
	      value = ar_interp_expression (t->child->next, type, ctx,
					    ar_interp_member_access);
	      s_check_visibility_of_variables (t->child, ctx, value);

	      ccl_list_add (result, ar_expr_add_reference (slot_expr));
	      ccl_list_add (result, ar_expr_add_reference (value));
	    }
	  else
	    {
	      ccl_assert (constraints != NULL);
	      value = 
		ar_interp_boolean_expression (t, ctx, ar_interp_member_access);
	      ccl_list_add (constraints, ar_expr_add_reference (value));
	    }

	  t = t->next;
	}
      ccl_catch
	{
	  error = 1;
	}
      ccl_end_try;
      ccl_zdelete (ar_expr_del_reference, value);
      ccl_zdelete (ar_type_del_reference, type);
      ccl_zdelete (ar_expr_del_reference, slot_expr);
    }

  ar_context_del_reference (ctx);

  if (error)
    {
      ccl_list_clear_and_delete (result, 
				 (ccl_delete_proc *) ar_expr_del_reference);
      INTERP_EXCEPTION ();
    }

  return result;
}

			/* --------------- */

static ar_symbol
s_interp_element_in_array_as_symbol(altarica_tree *t, ar_context *ctx)
{
  ar_symbol result = NULL;
  char *pos_str = NULL;
  size_t pos_str_size = 0;

  ccl_pre( IS_LABELLED_BY(t,ELEMENT_IN_ARRAY) );

  t = t->child;
  ccl_string_format(&pos_str,&pos_str_size,"%s",t->value.id_value);
  t = t->next;

  ccl_try(altarica_interpretation_exception)
    {
      while( t != NULL )
	{
	  int pos = ar_interp_integer_value(t,ctx,ar_interp_member_access);
	  char *tmp = ccl_string_dup(pos_str);

	  ccl_string_format(&pos_str,&pos_str_size,"%s[%d]",tmp,pos);
	  ccl_string_delete(tmp);

	  t = t->next;
	}

      result = ccl_string_make_unique(pos_str);
    }
  ccl_no_catch;

  ccl_string_delete(pos_str);

  if( result == NULL )
    ccl_rethrow();

  return result;
}

			/* --------------- */
#if 0
static ar_identifier *
s_interp_element_in_array_as_identifier(altarica_tree *t, ar_context *ctx)
{
  ccl_list *path = ccl_list_create();
  ar_identifier *result = NULL;
  char *pos_str = NULL;
  size_t pos_str_size = 0;

  ccl_pre( IS_LABELLED_BY(t,ELEMENT_IN_ARRAY) );

  t = t->child;
  ccl_list_add(path,t->value.id_value);
  t = t->next;

  ccl_try(altarica_interpretation_exception)
    {
      while( t != NULL )
	{
	  int pos = ar_interp_integer_value(t,ctx);
	  ccl_string_format(&pos_str,&pos_str_size,"[%d]",pos);

	  ccl_list_add(path,ccl_string_make_unique(pos_str));

	  t = t->next;
	}

      result = ar_identifier_create_from_path(path);
    }
  ccl_no_catch;

  if( pos_str != NULL )
    ccl_string_delete(pos_str);

  if( path != NULL )
    ccl_list_delete(path);

  if( result == NULL )
    ccl_rethrow();

  return result;
}
#endif
			/* --------------- */

static ar_identifier *
s_interp_declared_event_name(altarica_tree *nf, ar_node *node)
{
  ar_identifier *result = NULL;
  ccl_list *path = ccl_list_create();
  ar_context *ctx = ar_node_get_context(node);

  ccl_try(altarica_interpretation_exception)
    {
      if( IS_LABELLED_BY(nf,IDENTIFIER) )
	ccl_list_add(path,nf->value.id_value);
      else if( IS_LABELLED_BY(nf,ELEMENT_IN_ARRAY) )
	{
	  ar_symbol id = s_interp_element_in_array_as_symbol(nf,ctx);
	  ccl_list_add(path,id);
	}
      else
	{
	  INVALID_NODE_TYPE();
	}
    }
  ccl_catch
    {
      if( path != NULL )
	{
	  ccl_list_delete(path);
	  path = NULL;
	}
    }
  ccl_end_try;

  ar_context_del_reference(ctx);

  if( path != NULL )
    {
      result = ar_identifier_create_from_path(path);
      ccl_list_delete(path);
    }
  else
    {
      ccl_rethrow();
    }

  return result;
}

			/* --------------- */

static void
s_add_loop_on_event(ar_node *node, ar_event *event)
{
  ar_expr *guard = ar_expr_crt_true();
  ar_trans *t = ar_trans_create(guard,event);

  ar_node_add_event(node,event);
  ar_node_add_transition(node,t);
  ar_expr_del_reference(guard);
  ar_trans_del_reference(t);
}

			/* --------------- */

static void
s_add_epsilon_event(ar_node *node)
{
  if( ! ar_node_has_event(node,AR_EPSILON_ID) )
    s_add_loop_on_event(node,AR_EPSILON);
}

			/* --------------- */

static ccl_list *
s_interp_transition_labels (altarica_tree *nf, ar_node *node)
{
  ccl_list *result = ccl_list_create ();

  ccl_try (altarica_interpretation_exception)
    {
      for (; nf != NULL; nf = nf->next)
	{
	  ar_identifier *event_name = s_interp_declared_event_name (nf, node);
	  ar_event *event = ar_node_get_event (node, event_name);
	  
	  if (event == NULL)
	    {
	      if (event_name == AR_EPSILON_ID)
		{
		  s_add_epsilon_event (node);
		  event = ar_node_get_event (node, event_name);
		  ccl_assert (event != NULL);
		}
	      else
		{
		  ccl_error ("%s:%d: error: undefined event '", nf->filename, 
			     nf->line);
		  ar_identifier_log (CCL_LOG_ERROR, event_name);
		  ccl_error ("'.\n");
		  ar_identifier_del_reference (event_name);
		  INTERP_EXCEPTION ();
		}
	    }
      
	  ar_identifier_del_reference (event_name);
	  ccl_list_add (result, event);
	}
    }
  ccl_catch
    {
      ccl_list_clear_and_delete (result, 
				 (ccl_delete_proc *) ar_event_del_reference);
      ccl_rethrow ();
    }
  ccl_end_try;
  
  return result;
}

			/* --------------- */

struct index
{
  int value;
  ar_expr *expr_index;
  struct index *next;
};

static void
s_generate_transitions (altarica_tree *nf, ar_node *node, ar_expr *G, 
			ccl_list *events, ccl_list *assignment, 
			ccl_pair *indexes, struct index *stack)
{
  if (indexes == NULL)
    {
      ccl_pair *p;
      ar_context *ctx = ar_node_get_context (node);
      ar_expr *newG = ar_expr_add_reference (G);
      ccl_list *a = ccl_list_deep_dup (assignment, (ccl_duplicate_func *)
				       ar_expr_add_reference);

      while (stack != NULL)
	{ 	  
	  ar_expr *value = ar_expr_crt_integer (stack->value);
	  ar_expr *cond = ar_expr_crt_binary (AR_EXPR_EQ, stack->expr_index,
					      value);
	  ar_expr *tmp = ar_expr_crt_binary (AR_EXPR_AND, newG, cond);
	  ar_expr_del_reference (newG);
	  ar_expr_del_reference (cond);
	  newG = tmp;

	  for (p = FIRST (a); p; p = CDR (p))
	    {
	      tmp = ar_expr_subst (CAR (p), ctx, stack->expr_index, value);
	      ar_expr_del_reference (CAR (p));
	      CAR (p) = tmp;
	    }
	  ar_expr_del_reference (value);

	  stack = stack->next;
	}
      
      for (p = FIRST (events); p; p = CDR (p))
	{
	  ccl_pair *pa;
	  ar_trans *T = ar_trans_create (newG, CAR (p));
	  
	  for (pa = FIRST (a); pa; pa = CDDR (pa))
	    {
	      ar_expr *var = CAR (pa);
	      ar_expr *value = CADR (pa);
	      ar_context_slot *sl = ar_expr_eval_slot (var, ctx);

	      if( !(ar_context_slot_get_flags(sl) & AR_SLOT_FLAG_STATE_VAR) )
		{
		  ar_identifier *id = ar_context_slot_get_name(sl);
		  ccl_error("%s:%d: error: '",nf->child->next->filename,
			    nf->child->next->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ar_identifier_del_reference(id);
		  ccl_error("' is not a state variable.\n");
		  ar_expr_del_reference (newG);
		  ccl_list_clear_and_delete (a, (ccl_delete_proc *)
					     ar_expr_del_reference);
		  ar_context_del_reference (ctx);
		  ar_trans_del_reference (T);
		  ar_context_slot_del_reference (sl);
		  INTERP_EXCEPTION();
		}

	      if( s_get_slot_depth(ctx,sl) > 0 )
		{
		  ar_identifier *id = ar_context_slot_get_name(sl);
		  ccl_error("%s:%d: error: '",nf->child->next->filename,
			    nf->child->next->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ar_identifier_del_reference(id);
		  ccl_error("' is not a valid state variable.\n");
		  ar_expr_del_reference (newG);
		  ccl_list_clear_and_delete (a, (ccl_delete_proc *)
					     ar_expr_del_reference);
		  ar_context_del_reference (ctx);
		  ar_trans_del_reference (T);
		  ar_context_slot_del_reference (sl);
		  INTERP_EXCEPTION();
		}

	      var = ar_expr_crt_variable (sl);
	      ar_context_slot_del_reference (sl);
	      ar_trans_add_assignment(T, var, value);
	      ar_expr_del_reference (var);
	    }
	  ar_node_add_transition (node, T);
	  ar_trans_del_reference (T);
	}
      ar_expr_del_reference (newG);
      ccl_list_clear_and_delete (a, (ccl_delete_proc *) ar_expr_del_reference);
      ar_context_del_reference (ctx);
    }
  else
    {
      struct index i;

      i.value = (intptr_t) CAR (indexes);
      i.expr_index = CADR (indexes);
      i.next = stack;

      while (i.value--)
	s_generate_transitions (nf, node, G, events, assignment, 
				CDDR (indexes), &i);
    }
}


static void
s_build_transitions (altarica_tree *nf, ar_node *node, ar_expr *G, 
		     ccl_list *events, ccl_list *assignment)
{
  int error = 0;
  ccl_pair *p;
  ccl_list *indexes = ccl_list_create ();

  for (p = FIRST (assignment); p; p = CDR (p))
    {
      ar_expr *e = CAR (p);
      indexes = ar_expr_get_array_indexes (e, indexes);
    }

  ccl_try (exception)
    {
      s_generate_transitions (nf, node, G, events, assignment, FIRST (indexes), 
			      NULL);
    }
  ccl_catch
    {
      error = 1;
    }
  ccl_end_try;

  while (! ccl_list_is_empty (indexes))
    {
      ccl_list_take_first (indexes);
      ar_expr_del_reference (ccl_list_take_first (indexes));
    }
  ccl_list_delete (indexes);
  if (error) 
    ccl_rethrow ();
}

			/* --------------- */

static void
s_interp_transition_tgt(altarica_tree *nf, ar_node *node, ar_expr *G)
{
  int error = 0;
  ccl_list *assignments = NULL;
  ccl_list *events = NULL;
  ar_context *ctx = ar_node_get_context(node);

  ccl_pre( IS_LABELLED_BY(nf,TRANSITION_TGT) );
  
  ccl_try(altarica_interpretation_exception)
    {
      assignments = s_interp_assignment_list (nf->child->next, node, NULL);
      events = s_interp_transition_labels (nf->child->child, node);
      s_build_transitions (nf, node, G, events, assignments);
    }
  ccl_catch
    {
      error = 1;
    }
  ccl_end_try;

  if( assignments != NULL )
    {
      ccl_list_clear_and_delete (assignments, 
				 (ccl_delete_proc *) ar_expr_del_reference);
    }
  if (events != NULL)
    {
      ccl_list_clear_and_delete (events,
				 (ccl_delete_proc *) ar_event_del_reference);
    }

  ar_context_del_reference(ctx);
  if( error ) 
    INTERP_EXCEPTION();
}

			/* --------------- */

static void
s_interp_node_transitions(altarica_tree *nf, ar_node *node)
{
  int error = 0;
  altarica_tree *t;
  ar_context *ctx = ar_node_get_context(node);

  ccl_pre( IS_LABELLED_BY(nf,TRANSITIONS_DEF) );

  for(t = nf->child; t; t = t->next)
    {
      ar_expr *G = NULL;

      ccl_try(altarica_interpretation_exception)
	{ 
	  altarica_tree *tgt;

	  G = ar_interp_boolean_expression (t->child, ctx,
					    ar_interp_member_access);
	  s_check_visibility_of_variables (t->child, ctx, G);

	  for (tgt = t->child->next; tgt; tgt = tgt->next)
	    s_interp_transition_tgt (tgt, node, G); 
	}
      ccl_catch
	{ 
	  error = 1; 
	}
      ccl_end_try;

      ccl_zdelete (ar_expr_del_reference, G);
    }

  ar_context_del_reference(ctx);

  if( error )
    INTERP_EXCEPTION();
}

			/* --------------- */

static int
s_eval_sync_kind(altarica_tree *nf)
{
  int result = 0;

  if( IS_LABELLED_BY(nf,SYNC_MAX) )
    result = 1;
  else if( ! IS_LABELLED_BY(nf,SYNC_MIN) )
    INVALID_NODE_TYPE();

  return result;
}

			/* --------------- */

static void
s_interp_card_constraint(altarica_tree *t, ar_context *ctx, int nb_marked, 
			 int *pmin, int *pmax)
{
  int min, max, val = 0;

  ccl_pre( t != NULL );

  min = 0;
  val = max = nb_marked;

  if( t->child != NULL )
    val = ar_interp_integer_value(t->child,ctx,ar_interp_member_access);

  switch( t->node_type ) {
  case AR_TREE_SYNC_CONSTRAINT_EQ: min = max = val; break;
  case AR_TREE_SYNC_CONSTRAINT_LEQ: max = val; break;
  case AR_TREE_SYNC_CONSTRAINT_GEQ: min = val; break;
  case AR_TREE_SYNC_CONSTRAINT_LT: max = val-1; break;
  case AR_TREE_SYNC_CONSTRAINT_GT: min = val+1; break;
  case AR_TREE_SYNC_CONSTRAINT_NONE: break;
  default : INVALID_NODE_TYPE();
  };

  if( min < 0 || min > nb_marked || min > max || max > nb_marked ) 
    {
      ccl_error("%s:%d: error: cardinality constraint is out of range. \n",
		t->filename,t->line);
      ccl_error("%s:%d: error: there is %d marked events.\n",
		t->filename,t->line,nb_marked);
      INTERP_EXCEPTION();
    }
  *pmin = min; 
  *pmax = max;
}

			/* --------------- */

static ar_identifier *
s_interp_identifier_path_rec(altarica_tree *t, ar_context *ctx)
{
  ar_identifier *result;

  if( IS_LABELLED_BY(t,IDENTIFIER) )
    {
      result = ar_identifier_create_simple(t->value.id_value);
    }
  else
    {     
      ar_symbol id;

      ccl_assert( IS_LABELLED_BY(t,ELEMENT_IN_ARRAY) );

      id = s_interp_element_in_array_as_symbol(t,ctx);
      result = ar_identifier_create_simple(id);	  
    }

  if( t->next != NULL )
    {
      ar_identifier *tail = s_interp_identifier_path_rec(t->next,ctx);
      ar_identifier *tmp = ar_identifier_add_prefix(tail,result);
      ar_identifier_del_reference(result);
      ar_identifier_del_reference(tail);
      result = tmp;
    }

  return result;
}

			/* --------------- */

ar_identifier *
ar_interp_identifier_path(altarica_tree *t, ar_context *ctx)
{
  ccl_pre( IS_LABELLED_BY(t,IDENTIFIER_PATH) );

  return s_interp_identifier_path_rec(t->child,ctx);
}

			/* --------------- */

static ar_event *
s_look_for_event(altarica_tree *t, ar_node *m, ar_identifier *id)
{
  ar_event *result = NULL;
  ar_identifier *mname = ar_node_get_name(m);

  if( ar_identifier_is_simple(id) )
    {
      if( ar_node_has_event(m,id) )
	result = ar_node_get_event(m,id);
      else
	{
	  ccl_error("%s:%d: error: undefined event '",t->filename,t->line);
	  ar_identifier_log(CCL_LOG_ERROR,id);
	  ccl_error("' in model '");
	  ar_identifier_log(CCL_LOG_ERROR,mname);
	  ccl_error("'.\n");
	}
    }
  else
    {
      ar_identifier *suffix;
      ar_identifier *sub = ar_identifier_get_first(id,&suffix);
      ar_node *subnode = ar_node_get_subnode_model(m,sub);

      if(  subnode == NULL )
	{
	  ccl_error("%s:%d: error: undefined subnode '",t->filename,t->line);
	  ar_identifier_log(CCL_LOG_ERROR,sub);
	  ccl_error("' in model '");
	  ar_identifier_log(CCL_LOG_ERROR,mname);
	  ccl_error("'.\n");
	}
      else
	{
	  result = s_look_for_event(t,subnode,suffix);
	  ar_node_del_reference(subnode);
	}

      ar_identifier_del_reference(sub);
      ar_identifier_del_reference(suffix);
    }

  ar_identifier_del_reference(mname);
  
  return result;
}

			/* --------------- */

static void
s_interp_broadcast_list(altarica_tree *bl, ar_node *node, ar_broadcast *bv,
			ar_context *ctx)
{
  altarica_tree *ei;

  ccl_pre( IS_LABELLED_BY(bl,BROADCAST_LIST) );

  for(ei = bl->child; ei; ei = ei->next)
    {
      int flags;
      ar_event *ev = NULL;
      ar_identifier *eid;
      ar_identifier *tmp = ar_interp_identifier_path(ei->child,ctx);

      if( ar_identifier_get_path_length(tmp) > 1 )
	eid = ar_identifier_collapse_suffix(tmp);
      else
	eid = ar_identifier_add_reference(tmp);
      ar_identifier_del_reference(tmp);

      if( (ev = s_look_for_event(ei,node,eid)) == NULL )
	{
	  ar_identifier_del_reference(eid);
	  INTERP_EXCEPTION();
	}

      flags = ar_event_get_flags(ev);
      ar_event_del_reference(ev);

      if( ! ar_identifier_is_simple(eid) && 
	  (flags & AR_SLOT_FLAG_PRIVATE) != 0 )
	{
	  ccl_error("%s:%d: error: can't synchronize the private event '",
		    ei->filename,ei->line);
	  ar_identifier_log(CCL_LOG_ERROR,eid);
	  ccl_error("'\n");
	  ar_identifier_del_reference(eid);
	  INTERP_EXCEPTION();
	}

      /* check if the subnode is already synchronized in 'node' */
      {
	ar_identifier *sub = ar_identifier_get_prefix(eid);
		
	if( ar_broadcast_has_subnode(bv,sub) )
	  {
	    ccl_error("%s:%d: error: subnode ",ei->filename,ei->line);
	    ar_identifier_log(CCL_LOG_ERROR,sub);
	    ccl_error(" is already synchronized in this vector.\n");
	    ar_identifier_del_reference(eid);
	    ar_identifier_del_reference(sub);
	    INTERP_EXCEPTION();
	  }
	ar_identifier_del_reference(sub);
      }

      ar_broadcast_add_event(bv,eid,ei->value.int_value?1:0);
      ar_identifier_del_reference(eid);
    }
}

			/* --------------- */

static void
s_interp_sync_vector(altarica_tree *sv, ar_node *node, ar_context *ctx)
{
  int maximize = s_eval_sync_kind(sv->child->next->next);
  ar_broadcast *bv = ar_broadcast_create(maximize);

  ccl_pre( IS_LABELLED_BY(sv,SYNC_VECTOR) );

  ccl_try(altarica_interpretation_exception)
    {
      int min, max, nb_marked;

      s_interp_broadcast_list(sv->child,node,bv,ctx);
      min = max = nb_marked = ar_broadcast_get_nb_marked(bv);
      s_interp_card_constraint(sv->child->next,ctx,nb_marked,&min,&max);
      ar_broadcast_set_bounds(bv,min,max);
      ar_node_add_broadcast(node,bv);
      ar_broadcast_del_reference(bv);
    }
  ccl_catch
    {
      ar_broadcast_del_reference(bv);
      ccl_rethrow();
    }
  ccl_end_try;
}

			/* --------------- */

static void
s_interp_node_sync(altarica_tree *nf, ar_node *node)
{
  int error = 0;
  ar_context *ctx = ar_node_get_context(node);

  ccl_pre( IS_LABELLED_BY(nf,SYNCHRONIZATION_DEF) );

  for(nf = nf->child; nf; nf = nf->next)
    {
      ccl_pre( IS_LABELLED_BY(nf,SYNC_VECTOR) );

      ccl_try(altarica_interpretation_exception)
	{ 
	  s_interp_sync_vector(nf,node,ctx); 
	}
      ccl_catch
	{ 
	  error = 1; 
	}
      ccl_end_try;
    }
  ar_context_del_reference(ctx);

  if( error )
    INTERP_EXCEPTION();
}

			/* --------------- */

static ccl_list *
s_assignment_list_to_slot_assignement_list (altarica_tree *t, ccl_list *a, 
					    ar_node *node)
{
  int error = 0;
  ccl_pair *p;
  ccl_list *result = ccl_list_create ();
  ar_context *ctx = ar_node_get_context(node);

  for (p = FIRST (a); p && !error; p = CDDR (p))
    {
      ccl_pair *paux;
      ar_expr *slot_expr = CAR (p);
      ar_context_slot *slot = ar_expr_eval_slot (slot_expr, ctx);
      ar_identifier *slot_name = ar_context_slot_get_name(slot);
      ar_identifier *assigned_twice = NULL;

      for(paux = FIRST(result); paux && assigned_twice == NULL; 
	  paux = CDDR (paux))
	{
	  ar_context_slot *other = (ar_context_slot *) CAR (paux);
	  ar_identifier *other_name = ar_context_slot_get_name (other);

	  if (ar_identifier_is_container_of (slot_name, other_name))
	    assigned_twice = ar_identifier_add_reference (slot_name);
	  else if (ar_identifier_is_container_of (other_name, slot_name))
	    assigned_twice = ar_identifier_add_reference (other_name);
	  ar_identifier_del_reference (other_name);
	}

      ar_identifier_del_reference (slot_name);

      if (assigned_twice != NULL)
	{
	  ccl_error ("%s:%d: error: variable '", t->filename, t->line);
	  ar_identifier_log (CCL_LOG_ERROR, assigned_twice);
	  ar_identifier_del_reference (assigned_twice);
	  ccl_error ("' is assigned twice.\n");
	  error = 1;
	}
      else
	{
	  ccl_list_add (result, ar_context_slot_add_reference (slot)); 
	  ccl_list_add (result, ar_expr_add_reference (CADR (p))); 
	}
      ar_context_slot_del_reference (slot);
    }
  ar_context_del_reference(ctx);

  if (error)
    {
      ccl_pair *p;

      for (p = FIRST (result); p; p = CDDR (p))
	{
	  ar_context_slot_del_reference (CAR (p));
	  ar_expr_del_reference (CADR (p));
	}
      ccl_list_delete (result);
      INTERP_EXCEPTION ();
    }

  return result;
}

			/* --------------- */


static ccl_list *
s_interp_slot_assignment_list (altarica_tree *t, ar_node *node, 
			       ccl_list *constraints)
{
  ccl_list *result = NULL;
  ccl_list *a = s_interp_assignment_list (t, node, constraints);


  ccl_try (altarica_interpretation_exception)
    {
      result = s_assignment_list_to_slot_assignement_list (t, a, node);
    }
  ccl_no_catch;

  ccl_list_clear_and_delete (a, (ccl_delete_proc *) ar_expr_del_reference);
  if (result == NULL)
    INTERP_EXCEPTION ();

  return result;
}

			/* --------------- */


static void
s_interp_node_init(altarica_tree *nf, ar_node *node)
{
  int error = 0;
  ccl_list *init = NULL;
  ccl_list *constraints = ccl_list_create ();
  ccl_pair *p;
  ar_context *ctx;

  ccl_pre (IS_LABELLED_BY (nf, INIT_DECL));
  
  ccl_try (altarica_interpretation_exception)
    {
      init = s_interp_slot_assignment_list (nf->child, node, constraints);
    }
  ccl_catch
    {
      ccl_list_clear_and_delete (constraints, (ccl_delete_proc *)
				 ar_expr_del_reference);
      ccl_rethrow ();
    }
  ccl_end_try;

  ctx = ar_node_get_context(node);
  for(p = FIRST(init); p; p = CDDR(p))
    {
      ar_context_slot *slot = (ar_context_slot *)CAR(p);
      ar_expr *val = (ar_expr *)CADR(p);
      
      if( !(ar_context_slot_get_flags(slot) & AR_SLOT_FLAG_STATE_VAR) )
	{
	  ar_identifier *id = ar_context_slot_get_name(slot);
	  ccl_error("%s:%d: error: '",nf->filename,nf->line);
	  ar_identifier_log(CCL_LOG_ERROR,id);
	  ar_identifier_del_reference(id);
	  ccl_error("' is not a state variable.\n");
	  error = 1;
	}
      else if( ! ar_expr_is_constant(val,ctx) )
	{
	  ccl_error("%s:%d: error: '",nf->filename,nf->line);
	  ar_expr_log(CCL_LOG_ERROR,val);
	  ccl_error("' is not a constant expression.\n");
	  error = 1;
	}
      else
	{
	  if (ar_expr_is_computable (val))
	    {
	      ar_constant *c = ar_expr_eval (val, ctx);	  
	      ar_type *type = ar_context_slot_get_type (slot);

	      if (! ar_constant_is_in_domain(c, type))
		{
		  ar_error(nf,"'");
		  ar_expr_log (CCL_LOG_ERROR, val);
		  ccl_error("' is not in domain '");
		  ar_type_log (CCL_LOG_ERROR, type);
		  ccl_error("'.\n");
		  error = 1;
		}
	      ar_type_del_reference (type);
	      ar_constant_del_reference (c);
	    }
	  if (!error)
	    ar_node_add_initial_state (node, slot, val);
	}
      ar_expr_del_reference(val);
      ar_context_slot_del_reference(slot);
    }
  
  if (!error)
    {
      ccl_try(altarica_interpretation_exception)
        {
	  for (p = FIRST (constraints); p; p = CDR (p)) 
	    {
	      ar_expr *ic = CAR (p);
	      s_check_visibility_of_variables (nf, ctx, ic);
	      ar_node_add_initial_constraint (node, ic);
	    }
	}
      ccl_catch
	{
	  error = 1;
	}
      ccl_end_try;
    }
  ccl_list_clear_and_delete (constraints, (ccl_delete_proc *)
			     ar_expr_del_reference);
  
  ccl_list_delete(init);
  ar_context_del_reference(ctx);

  if (error)
    INTERP_EXCEPTION();
}

			/* --------------- */

static void
s_interp_node_parameter_setting(altarica_tree *nf, ar_node *node)
{
  ar_context *ctx = ar_node_get_context(node);
  ar_identifier *pname = NULL;
  ar_expr *value = NULL;

  ccl_pre( IS_LABELLED_BY(nf,PARAM_SET_DECL) );
  
  ccl_try(altarica_interpretation_exception)
    {
      altarica_tree *t;

      for(t = nf->child; t != NULL; t = t->next)
	{
	  ar_identifier *param;
	  ar_identifier *subname;

	  ccl_assert( IS_LABELLED_BY(t,ASSIGNMENT) );

	  pname = 
	    ar_interp_member_access_as_identifier(t->child,ctx,
						  ar_interp_member_access);
	  value = ar_interp_constant_expression (t->child->next, NULL, ctx,
						ar_interp_member_access);

	  if( ar_identifier_get_path_length(pname) != 2 )
	    {
	      ccl_error("%s:%d: error: invalid parameter name '",
			t->child->filename,t->child->line);
	      ar_identifier_log(CCL_LOG_ERROR,pname);
	      ccl_error("'.\n");
	      INTERP_EXCEPTION();
	    }

	  subname = ar_identifier_get_first(pname,&param);

	  if( ar_node_has_subnode_parameter_setting(node,subname,param) )
	    {
	      ccl_error("%s:%d: error: parameter '",t->child->filename,
			t->child->line);
	      ar_identifier_log(CCL_LOG_ERROR,pname);
	      ccl_error("' is already assigned.\n");
	      ar_identifier_del_reference(param);
	      ar_identifier_del_reference(subname);
	      INTERP_EXCEPTION();
	    }

	  ar_node_set_subnode_parameter(node,subname,param,value);
	  ar_identifier_del_reference(param);
	  ar_identifier_del_reference(subname);
	  ar_identifier_del_reference(pname);
	  ar_expr_del_reference(value);
	  pname = NULL;
	  value = NULL;
	}
    }
  ccl_catch
    {
      ccl_zdelete(ar_expr_del_reference,value);
      ccl_zdelete(ar_identifier_del_reference,pname);
      ar_context_del_reference(ctx);
      ccl_rethrow();
    }
  ccl_end_try;

  ar_context_del_reference(ctx);
}

			/* --------------- */

static ccl_list *
s_interp_array_of_events(altarica_tree *nf, ar_context *ctx)
{
  ccl_list *result = NULL;
  int len = ccl_parse_tree_count_siblings(nf->child->next);
  int *dims = ccl_new_array(int,len);
  int  i;

  ccl_try(altarica_interpretation_exception)
    {
      altarica_tree *t;
      for(t = nf->child->next, i = 0; i < len; i++, t = t->next)
	{
	  dims[i] = ar_interp_integer_value(t,ctx,ar_interp_member_access);
	  if( dims[i] <= 0 )
	    INTERP_EXCEPTION_MSG((t, "invalid array size (%d).\n", dims[i]));
	}
    }
  ccl_catch
    {
      ccl_delete(dims);
      ccl_rethrow();
    }
  ccl_end_try;

  {
    ar_identifier *id = ar_interp_symbol_as_identifier(nf->child);
    result = s_generate_array_identifiers(id,len,dims);
    ar_identifier_del_reference(id);
  }
	       
  ccl_delete(dims);

  return result;
}

			/* --------------- */

static ar_event *
s_add_event(ar_node *node, ar_identifier *ename, int flags, ccl_list *attr)
{
  ar_event *ev;

  if( ar_node_has_event(node,ename) )
    ev = ar_node_get_event(node,ename);
  else
    {
      ev = ar_event_create(ename);
      ar_event_set_flags(ev,flags);
      ar_event_set_attr(ev,attr);
      ar_node_add_event(node,ev);
    }

  return ev;
}

			/* --------------- */

static ar_event_poset *
s_interp_dag_list(altarica_tree *t, ar_node *node, int flags, ar_context *ctx,
		  ccl_list *attr)
{
  int is_dag = 1;
  altarica_tree *nf = t;
  ar_event_poset *result = NULL;

  switch( nf->node_type ) {
  case AR_TREE_IDENTIFIER :
    {
      ar_event *ev;
      ar_identifier *id = ar_interp_symbol_as_identifier(nf);

      result = ar_event_poset_create();
      ev = s_add_event(node,id,flags,attr);
      ar_event_poset_add_event(result,ev);
      ar_event_del_reference(ev);
      ar_identifier_del_reference(id);
    }
    break;

  case AR_TREE_ELEMENT_IN_ARRAY :
    {
      ccl_pair *p;
      ccl_list *ids = s_interp_array_of_events(nf,ctx);

      result = ar_event_poset_create();
      for(p = FIRST(ids); p; p = CDR(p))
	{
	  ar_identifier *id = (ar_identifier *)CAR(p);
	  ar_event *ev = s_add_event(node,id,flags,attr);
	  ar_event_poset_add_event(result,ev);
	  ar_event_del_reference(ev);
	  ar_identifier_del_reference(id);
	}
      ccl_list_delete(ids);
    }
    break;

  case AR_TREE_EVENT_LT : case AR_TREE_EVENT_GT : 
    {
      ar_event_poset *ep1 = NULL;
      ar_event_poset *ep2 = NULL;

      ccl_try(altarica_interpretation_exception)
	{
	  ep1 = s_interp_dag_list(nf->child,node,flags,ctx,attr);
	  ep2 = s_interp_dag_list(nf->child->next,node,flags,ctx,attr);
	  result = ar_event_poset_create();
	  if( nf->node_type == AR_TREE_EVENT_LT )
	    is_dag = ar_event_poset_add_lt(result,ep1,ep2);
	  else
	    is_dag = ar_event_poset_add_lt(result,ep2,ep1);
	}
      ccl_catch
	{
	  if (result != NULL)
	    {
	      ar_event_poset_del_reference (result);
	      result = NULL;
	    }
	}
      ccl_end_try;

      if( ep1 != NULL )
	ar_event_poset_del_reference(ep1);
      if( ep2 != NULL )
	ar_event_poset_del_reference(ep2);

      if( result == NULL )
	INTERP_EXCEPTION();
    }
    break;
    
  case AR_TREE_EVENT_DAG_LIST :
    result = ar_event_poset_create();
    ccl_try(altarica_interpretation_exception)
      {
	for(nf = nf->child; nf && result && is_dag; nf = nf->next)
	  {
	    ar_event_poset *aux = s_interp_dag_list(nf,node,flags,ctx,attr);

	    is_dag = ar_event_poset_add_poset(result,aux);
	    ar_event_poset_del_reference(aux);
	  }
      }
    ccl_catch
      {
	ar_event_poset_del_reference(result);
	result = NULL;
      }
    ccl_end_try;

    break;

  default : 
    INVALID_NODE_TYPE();
    break;
  }

  if( ! is_dag )
    {
      if( result != NULL )
	ar_event_poset_del_reference(result);
      INTERP_EXCEPTION_MSG((t, "cycle creation in event poset.\n"));
    }

  if( result == NULL )
    INTERP_EXCEPTION();

  return result;
}

			/* --------------- */

static void
s_interp_node_events(altarica_tree *nf, ar_node *node)
{
  int error = 0;
  ar_event_poset *node_ep = ar_node_get_event_poset(node);
  ar_context *ctx = ar_node_get_context(node);

  ccl_pre( nf->node_type == AR_TREE_EVENTS_DECL );
  
  for(nf = nf->child; nf && !error; nf = nf->next)
    {
      ar_event_poset *ep;
      uint32_t flags = AR_SLOT_FLAG_EVENT;
      ccl_list *attr = NULL;

      ccl_assert( nf->node_type == AR_TREE_EVENT_POSET );

      ccl_try(altarica_interpretation_exception)
	{
	  int is_dag;

	  attr = s_interp_flags(nf->child->next,&flags);
	  ep = s_interp_dag_list(nf->child,node,flags,ctx,attr);
	  is_dag = ar_event_poset_add_poset(node_ep,ep);
	  ar_event_poset_del_reference(ep);

	  if( ! is_dag )
	    INTERP_EXCEPTION_MSG((nf, "cycle creation in event poset.\n"));
	}
      ccl_catch
	{
	  error = 1;
	}
      ccl_end_try;

      if( attr != NULL )
	ccl_list_clear_and_delete(attr,(ccl_delete_proc *)
				  ar_identifier_del_reference);
    }

  ar_context_del_reference(ctx);
  ar_event_poset_del_reference(node_ep);
  if( error )
    INTERP_EXCEPTION();
}

			/* --------------- */

static altarica_tree *
s_look_for_parameters(altarica_tree *t)
{
  altarica_tree *nf;

  for(nf = t->child->next->next; nf; nf = nf->next)
    {
      if( IS_LABELLED_BY(nf,PARAMETERS_DECL) )
	return nf;
    }

  return nf;
}

			/* --------------- */

static void
s_interp_parameters(altarica_tree *t, ar_node *node, ar_idtable *setting)
{
  int error = 0;
  ccl_list *idlist = NULL;
  ar_type *type = NULL;
  ar_context *ctx = ar_node_get_context(node);

  ccl_pre( IS_LABELLED_BY(t,PARAMETERS_DECL) );

  ccl_try(altarica_interpretation_exception)
    {
      for(t = t->child; t; t = t->next)
	{
	  ccl_pair *p;

	  ccl_pre( IS_LABELLED_BY(t,PARAMETER_DECL) );

	  idlist = ar_interp_id_list(t->child,1);
	  type = ar_interp_domain(t->child->next,ctx,ar_interp_member_access);

	  for(p = FIRST(idlist); p; p = CDR(p))
	    {
	      ar_identifier *id = (ar_identifier *)CAR(p);
	      ar_expr *value = NULL;
	      const char *symbol_type = s_look_for_symbol(ctx,id);

	      if( symbol_type != NULL )
		{		  
		  ccl_error("%s:%d: error: symbol name '",t->filename,t->line);
		  ar_identifier_log(CCL_LOG_ERROR,id);
		  ccl_error("' clashes with the %s with the same name.\n",
			    symbol_type);
		  INTERP_EXCEPTION();
		}

	      if( setting ) 
		{
		  value = ar_idtable_get(setting,id);
		  if( value != NULL && ar_expr_is_computable(value) )
		    {
		      ar_constant *cst = ar_expr_eval(value, ctx);
		      if( ! ar_constant_is_in_domain(cst,type) )
			{
			  ccl_error("%s:%d: error: '",t->filename,t->line);
			  ar_expr_log(CCL_LOG_ERROR,value);
			  ccl_error("' is not in the domain of the "
				    "parameter '");
			  ar_identifier_log(CCL_LOG_ERROR,id);
			  ccl_error("'.\n");
			  ar_constant_del_reference(cst);
			  ar_expr_del_reference(value);
			  INTERP_EXCEPTION();
			}
		      ar_constant_del_reference(cst);
		    }
		  else
		    {
		      ccl_warning("%s:%d: warning: the value of parameter '",
				  t->child->filename,t->child->line);
		      ar_identifier_log(CCL_LOG_WARNING,id);
		      ccl_warning("' is not computable.\n");
		    }		  
		}
	      
	      ar_node_add_local_parameter(node,id,type,value);
	      ccl_zdelete(ar_expr_del_reference,value);
	    }

	  ccl_list_clear_and_delete(idlist,(ccl_delete_proc *)
				    ar_identifier_del_reference);
	  idlist = NULL;
	  ar_type_del_reference(type);
	  type = NULL;
	}
    }
  ccl_catch
    {
      error = 1;
    }
  ccl_end_try;

  if( idlist != NULL )
    ccl_list_clear_and_delete(idlist,(ccl_delete_proc *)
			      ar_identifier_del_reference);
  if( type != NULL )
    ar_type_del_reference(type);

  ar_context_del_reference(ctx);
  if( error )
    ccl_rethrow();  
}

			/* --------------- */

static ar_node *
s_interp_parametrized_node(altarica_tree *t, altarica_tree *params,
			   ar_context *parent_context) 
{
  ar_identifier *nodename = ar_interp_symbol_as_identifier(t->child);
  ar_node *node = ar_node_create_parametrized(t,parent_context);

  ar_node_set_name(node,nodename);
  ar_identifier_del_reference(nodename);
  
  return node;
}

			/* --------------- */

static ar_identifier *
s_build_template_name(ar_identifier *modelname, ar_idtable *params)
{
  ar_identifier *result;
  char *newname = ar_identifier_to_string(modelname);

  ccl_string_format_append(&newname,"<");
  if( params != NULL )
    {
      char *fmt = "%s=%s,";
      ar_identifier_iterator *i = ar_idtable_get_keys(params);
      while( ccl_iterator_has_more_elements(i) )
	{
	  ar_identifier *pname = ccl_iterator_next_element(i);
	  char *pname_str = ar_identifier_to_string(pname);
	  ar_expr *value = (ar_expr *)ar_idtable_get(params,pname);
	  char *value_str = ar_expr_to_string(value);

	  ar_identifier_del_reference(pname);
	  ar_expr_del_reference(value);

	  if( ! ccl_iterator_has_more_elements(i) )
	    fmt = "%s=%s";
	  ccl_string_format_append(&newname,fmt,pname_str,value_str);
	  ccl_string_delete(pname_str);
	  ccl_string_delete(value_str);
	}
      ccl_iterator_delete(i);
    }
  ccl_string_format_append(&newname,">");
  result = ar_identifier_create(newname);
  ccl_string_delete(newname);

  return result;
}

			/* --------------- */

static void
s_register_new_public_event (ar_node *node, ar_identifier *ievid, int flags,
			     const ccl_list *attrs)
{
  ar_identifier *nevid = ar_identifier_collapse (ievid);
  if (! ar_node_has_event (node, nevid))
    {
      ar_broadcast *bv = ar_broadcast_create (1);
      ar_event *nev = ar_event_create (nevid);
  
      ar_event_set_flags (nev, flags);
      ar_event_set_attr (nev, attrs);
      s_add_loop_on_event (node, nev);
      
      ar_broadcast_add_event (bv, nevid, 0);
      ar_broadcast_add_event (bv, ievid, 0);
      ar_node_add_broadcast (node, bv);
      
      ar_broadcast_del_reference (bv);
      ar_event_del_reference (nev);
    }
  ar_identifier_del_reference (nevid);
}

static void
s_add_public_events(ar_node *node)
{
  ccl_list *vectors;
  ar_context_slot_iterator *sli;
  ar_idtable *synchronized_events;

  if( ar_node_is_leaf(node) )
    return;

  vectors = ar_node_get_broadcasts(node);
  synchronized_events = ar_idtable_create (0, NULL, NULL);

  {
    ccl_pair *p;

    for(p = FIRST(vectors); p; p = CDR(p))
      {
	int i;
	ar_broadcast *bv = (ar_broadcast *)CAR(p);
	ar_identifier_array subnodes = ar_broadcast_get_marked_subnodes (bv);

	for (i = 0; i < 2; i++)
	  {
	    int j;

	    for (j = 0; j < subnodes.size; j++)
	      {
		ar_identifier *ev =
		  ar_broadcast_get_event (bv, subnodes.data[j]);
		ar_idtable_put (synchronized_events, ev, ev);
		ar_identifier_del_reference (ev);
		ar_identifier_del_reference (subnodes.data[j]);
	      }

	    ccl_array_delete (subnodes);
	    if (i == 0)
	      subnodes = ar_broadcast_get_not_marked_subnodes (bv); 
	  }
      }
  }

  sli = ar_node_get_slots_for_subnodes(node);
  while( ccl_iterator_has_more_elements(sli) )
    {
      ar_context_slot *subnode = ccl_iterator_next_element(sli);
      ar_identifier *subname = ar_context_slot_get_name(subnode);
      ar_type *type = ar_context_slot_get_type(subnode);
      ar_node *model = ar_type_bang_get_node(type);
      ar_event_iterator *ei = ar_node_get_events(model);

      while( ccl_iterator_has_more_elements(ei) )
	{
	  ar_event *ev = ccl_iterator_next_element(ei);
	  
	  if( (ar_event_get_flags(ev) & AR_SLOT_FLAG_PUBLIC) != 0 )
	    {
	      ar_identifier *evid = ar_event_get_name(ev);
	      ar_identifier *ievid = ar_identifier_add_prefix(evid,subname);
	      ar_identifier_del_reference(evid);
	      int synchronized = ar_idtable_has (synchronized_events, ievid);

	      if( ! synchronized )
		s_register_new_public_event (node, ievid,
					     ar_event_get_flags(ev),
					     ar_event_get_attr(ev));
	      ar_identifier_del_reference(ievid);
	    }
	  ar_event_del_reference(ev);
	}
      ccl_iterator_delete(ei);

      ar_context_slot_del_reference(subnode);
      ar_type_del_reference(type);
      ar_node_del_reference(model);
      ar_identifier_del_reference(subname);
    }
  ccl_iterator_delete(sli);
  ar_idtable_del_reference (synchronized_events);
}

			/* --------------- */

#if 0
static void
s_add_idle_state(ar_node *node)
{
  if( ar_node_get_nb_variables(node) == 0 )
    {
      ar_type *dom = ar_type_crt_range (0, 0);

      ar_node_add_variable (node, AR_IDLE_VAR, dom,
			    AR_SLOT_FLAG_STATE_VAR|AR_SLOT_FLAG_PRIVATE,NULL);
      ar_type_del_reference (dom);

    }
}
#endif
			/* --------------- */

static void
s_add_epsilon_vector(ar_node *node)
{
  if( ! ar_node_is_leaf(node) )
    {
      ar_broadcast *epsilon_vect = ar_broadcast_create(1);
      ar_node_add_broadcast(node,epsilon_vect);
      ar_broadcast_del_reference(epsilon_vect);
    }
}

			/* --------------- */

static ccl_set *
s_collect_synchronized_events (ar_node *node)
{
  ccl_pair *p;
  ar_identifier_iterator *ii;
  ccl_list *vectors = ar_node_get_broadcasts (node);
  ccl_set *result = ccl_set_create ();

  for (p = FIRST (vectors); p; p = CDR (p))
    {
      ar_broadcast *vect = CAR (p);
      ii = ar_broadcast_get_subnodes (vect);
      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *sn = ccl_iterator_next_element (ii);
	  ar_identifier *ev = ar_broadcast_get_event (vect, sn);
	  ccl_set_add (result, ev);
	  ar_identifier_del_reference (sn);
	  ar_identifier_del_reference (ev);
	}
      ccl_iterator_delete (ii);
    }
  
  return result;
}

static void
s_add_asynchronous_events(ar_node *node)
{
  ccl_set *sync_events;
  ccl_list *async_events;
  ar_context_slot_iterator *sli;

  if( ar_node_is_leaf(node) )
    return;
  
  sli = ar_node_get_slots_for_subnodes(node);
  sync_events = s_collect_synchronized_events (node);
  async_events = ccl_list_create();

  while( ccl_iterator_has_more_elements(sli) )
    {
      ar_context_slot *sl = ccl_iterator_next_element(sli);
      ar_type *type = ar_context_slot_get_type(sl);
      ar_identifier *subname = ar_context_slot_get_name(sl);
      ar_node *submodel = ar_type_bang_get_node(type);
      ar_identifier_iterator *ei = ar_node_get_event_identifiers(submodel);
            
      ar_node_del_reference(submodel);
      ar_type_del_reference(type);
      ar_context_slot_del_reference(sl);

      while( ccl_iterator_has_more_elements(ei) )
	{
	  int sync = 0;
	  ar_identifier *tmp = ccl_iterator_next_element(ei);

	  if( tmp != AR_EPSILON_ID )
	    {
	      ar_identifier *eid = ar_identifier_add_prefix (tmp, subname);
	      sync = (ccl_set_has (sync_events, eid));
	      if( ! sync )
		{
		  ar_broadcast *bv = ar_broadcast_create (1);
		  ar_broadcast_add_event (bv, eid,0);
		  ccl_list_add (async_events, bv);
		}
	      ar_identifier_del_reference (eid);
	    }
	  ar_identifier_del_reference(tmp);
	}

      ar_identifier_del_reference(subname);
      ccl_iterator_delete(ei);
    }
  ccl_iterator_delete(sli);

  {
    ar_identifier_iterator *ei = ar_node_get_event_identifiers(node);

    while( ccl_iterator_has_more_elements(ei) )
      {
	int sync = 0;
	ar_identifier *tmp = ccl_iterator_next_element(ei);

	if( tmp != AR_EPSILON_ID )
	  {
	    sync = ccl_set_has (sync_events, tmp);
	    if( ! sync )
	      {
		ar_broadcast *bv = ar_broadcast_create(1);
		
		ar_broadcast_add_event(bv,tmp,0);
		ccl_list_add(async_events,bv);
	      }
	  }
	ar_identifier_del_reference(tmp);
      }
    ccl_iterator_delete(ei);
  }
  ccl_set_delete (sync_events);
  
  while( ! ccl_list_is_empty(async_events) )
    {
      ar_broadcast *bv = (ar_broadcast *)ccl_list_take_first(async_events);
      ar_node_add_broadcast(node,bv);
      ar_broadcast_del_reference(bv);
    }
  ccl_list_delete(async_events);
}

			/* --------------- */

static void
s_complete_node(ar_node *node)
{
  s_add_public_events(node);
  s_add_epsilon_event(node);
  /* s_add_idle_state(node); */
  s_add_asynchronous_events(node);
  s_add_epsilon_vector(node);
}

			/* --------------- */
static void
s_add_unable_transition (ar_node *node, ar_event *e)
{
  ar_expr *false_guard = ar_expr_crt_false ();
  ar_trans *t = ar_trans_create(false_guard, e);
  ar_node_add_transition (node, t);
  ar_expr_del_reference (false_guard);
  ar_trans_del_reference (t);
}

			/* --------------- */

static ccl_set *
s_collect_trans_labels (ar_node *node)
{
  ccl_pair *p;
  ccl_list *trans = ar_node_get_transitions(node);
  ccl_set *result = ccl_set_create ();

  for (p = FIRST (trans); p; p = CDR (p))
    {
      ar_event *te = ar_trans_get_event (CAR (p));
      ccl_set_add (result, te);
      ar_event_del_reference (te);
    }
  
  return result;
}

static void
s_check_node(altarica_tree *t, ar_node *node)
{
  /*
   *  Look for events without transitions
   */
    ccl_set *labels = s_collect_trans_labels (node);
    ar_event_iterator *ei = ar_node_get_events(node);
    
    while( ccl_iterator_has_more_elements(ei) )
      {
	int found = 0;
	ar_event *e = ccl_iterator_next_element(ei);

	found = ccl_set_has (labels, e);

	if( ! found )
	  {
	    ccl_warning("%s:%d: warning: no transition exists for the event '",
		      t->filename,t->line);
	    ar_event_log(CCL_LOG_WARNING,e);
	    ccl_warning ("'.\n");
	    
	    s_add_unable_transition (node, e);
	    /*
	    ar_event_del_reference(e);
	    ccl_iterator_delete(ei);
	    INTERP_EXCEPTION();
	    */
	  }
	ar_event_del_reference(e);
      }
    ccl_iterator_delete(ei);
    ccl_set_delete (labels);
}

			/* --------------- */

static int
s_cmp_labels (const void *t1, const void *t2)
{
  return ((altarica_tree *)t1)->node_type - ((altarica_tree *)t2)->node_type;
}

			/* --------------- */

static ar_node *
s_interp_node (altarica_tree *t, ar_context *parent_context, int as_template,
	       ar_idtable *setting)
  CCL_THROW (altarica_interp_exception)
{
  int error = 0;
  altarica_tree *nf;
  ar_identifier *nodename;
  ar_node *node;

  ccl_pre (t != NULL && t->node_type == AR_TREE_NODE);

  if (as_template)
    {
      if ((nf = s_look_for_parameters (t)) != NULL)
	return s_interp_parametrized_node (t, nf, parent_context);
    }

  nodename = ar_interp_symbol_as_identifier (t->child);
  if (! as_template)
    {
      ar_identifier *newname = s_build_template_name (nodename, setting);
      ar_identifier_del_reference (nodename);
      nodename = newname;
    }

  node = ar_node_create (parent_context);
  ar_node_set_name (node, nodename);
  ar_identifier_del_reference (nodename);

  s_interp_node_attributes (t->child->next,node);

  ccl_try (altarica_interpretation_exception)
    {
      ccl_pair *p;
      ccl_list *nodes = ccl_list_create ();

      for(nf = t->child->next->next; nf; nf = nf->next)
	ccl_list_insert (nodes, nf, s_cmp_labels);

      for (p = FIRST (nodes); p; p = CDR (p))
	{
	  nf = CAR (p);
	  ccl_try (altarica_interpretation_exception)
	    {
	      switch(nf->node_type) 
		{
		case AR_TREE_PARAMETERS_DECL :
		  s_interp_parameters (nf, node, setting);
		  break;
		case AR_TREE_VARIABLES_DECL :
		  s_interp_node_variables (nf, node);
		  break;
		case AR_TREE_EVENTS_DECL :
		  s_interp_node_events (nf, node);
		  break;
		case AR_TREE_SUBNODES_DECL :
		  s_interp_node_subnodes (nf, node);
		  break;
		case AR_TREE_ASSERTIONS_DEF :
		  s_interp_node_assertions (nf, node);
		  break;
		case AR_TREE_TRANSITIONS_DEF :
		  s_interp_node_transitions (nf, node);
		  break;
		case AR_TREE_SYNCHRONIZATION_DEF :
		  if (ar_node_is_leaf (node))
		    {
		      ar_error (nf, "synchronization vectors are not "
				"allowed in leaf nodes.\n");
		      INTERP_EXCEPTION ();
		    }
		  s_interp_node_sync (nf, node);
		  break;
		case AR_TREE_INIT_DECL :
		  s_interp_node_init (nf, node);
		  break;
		case AR_TREE_PARAM_SET_DECL :	
		  s_interp_node_parameter_setting (nf, node);
		  break;
		case AR_TREE_EXTERN_DECL :
		  ar_interp_extern_decl (nf, node);
		  break;
		default : 
		  INVALID_NODE_TYPE ();
		  break;
		}
	    }
	  ccl_catch
	    {
	      error = 1;
	    }
	  ccl_end_try;
	}
      ccl_list_delete (nodes);
      if (error)
	INTERP_EXCEPTION ();
      s_complete_node (node);
      s_check_node (t, node);
    }
  ccl_catch
    {
      ar_node_del_reference (node);
      ccl_rethrow ();
    }
  ccl_end_try;

  return node;
}

			/* --------------- */

ar_node *
ar_interp_node(altarica_tree *t, ar_context *parent_context) 
  CCL_THROW(altarica_interp_exception)
{
  return s_interp_node (t, parent_context, 1, NULL); 
}

			/* --------------- */

ar_node *
ar_interp_node_template(altarica_tree *t, ar_context *ctx, ar_idtable *setting)
  CCL_THROW(altarica_interp_exception)
{
  return s_interp_node(t,ctx,0,setting);
}
