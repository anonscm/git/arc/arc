/*
 * ar-rldd.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_RLDD_H
# define AR_RLDD_H

# include <ccl/ccl-protos.h>
# include <ccl/ccl-log.h>

typedef struct ar_word_diagram_st ar_rldd;
typedef struct ar_word_diagram_manager_st ar_rlddm;
typedef void ar_rldd_log_label (ccl_log_type log, const void *label, void *data);
typedef void ar_rldd_map_node_proc (ar_rlddm *man, ar_rldd *node, void *data);


extern ar_rlddm *
ar_rldd_create_manager(int node_table_min_size,
		      int node_table_fill_degree,
		      int op_table_size,
		      int page_size,
		      ccl_duplicate_func *label_dup,   
		      ccl_delete_proc *label_delete,
		      ccl_compare_func *label_compare);

extern void
ar_rldd_destroy_manager (ar_rlddm *manager);

extern ar_rldd *
ar_rldd_epsilon (ar_rlddm *manager);

extern ar_rldd *
ar_rldd_empty (ar_rlddm *manager);

extern ar_rldd *
ar_rldd_letter (ar_rlddm *manager, void *label);

extern int
ar_rldd_is_nil (ar_rlddm *man, ar_rldd *node);

extern int
ar_rldd_is_epsilon (ar_rlddm *man, ar_rldd *node);

extern ar_rldd *
ar_rldd_dup (ar_rlddm *man, ar_rldd *node);

extern void
ar_rldd_delete (ar_rlddm *man, ar_rldd *node);

extern ar_rldd *
ar_rldd_union (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_div (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_intersection (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_difference (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_concatenation (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_concat_letter (ar_rlddm *man, void *label, ar_rldd *n);

extern ar_rldd *
ar_rldd_extract (ar_rlddm *man, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_min (ar_rlddm *man, ar_rldd *n);

extern ar_rldd *
ar_rldd_power (ar_rlddm *man, ar_rldd *E, int N);

extern ar_rldd *
ar_rldd_find_operation (ar_rlddm *man, void *op, ar_rldd *n1, ar_rldd *n2);

extern ar_rldd *
ar_rldd_add_operation (ar_rlddm *man, void *op, ar_rldd *n1, ar_rldd *n2, 
		      ar_rldd *r);

extern void
ar_rldd_display_node_structure (ar_rlddm *man, ar_rldd *n, ccl_log_type log,
			       ar_rldd_log_label *log_label, void *data);

extern void
ar_rldd_display_node_sequences (ar_rlddm *man, ar_rldd *n, ccl_log_type log,
			       ar_rldd_log_label *log_label, void *data);

extern void
ar_rldd_display_node_table (ar_rlddm *man, ccl_log_type log);

extern void *
ar_rldd_get_label (ar_rldd *n);

extern ar_rldd *
ar_rldd_get_left (ar_rldd *n);

extern ar_rldd *
ar_rldd_get_right (ar_rldd *n);

extern int 
ar_rldd_get_size (ar_rldd *n);

extern int 
ar_rldd_get_height (ar_rldd *n);

extern int
ar_rldd_number_of_sequences (ar_rlddm *man, ar_rldd *n);

extern void
ar_rldd_map_nodes (ar_rlddm *man, ar_rldd *n, ar_rldd_map_node_proc *map, 
		  void *data);

#endif /* ! AR_RLDD_H */
