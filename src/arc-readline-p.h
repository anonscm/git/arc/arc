/*
 * arc-readline-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_READLINE_P_H
# define ARC_READLINE_P_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#else
# define HAVE_LIBREADLINE 0
#endif /* HAVE_CONFIG_H */

#if HAVE_LIBREADLINE
extern void
arc_readline_init_rl (const char *hfilename);
extern void
arc_readline_terminate_rl (void);
extern char *
arc_readline_get_line_rl (const char *prompt);
#endif

extern char *
arc_readline_get_line_no_rl (const char *prompt);

#endif /* ! ARC_READLINE_P_H */
