/*
 * depgraph.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-graph.h>
#include <ccl/ccl-bittable.h>
#include "depgraph.h"

struct depgraph_st
{
  ccl_graph *G;
  ccl_hash *vkind;
};

#define set_vkind(t, v, k)			\
  do {						\
    if (!ccl_hash_find (t, v))			\
      ccl_hash_insert (t, (void *) k);		\
  } while (0) 

			/* --------------- */

static depgraph *
s_new_depgraph (void);

depgraph *
depgraph_compute (ar_ca *ca, int add_fdset)
{
  depgraph *result = s_new_depgraph ();
  ccl_graph *G = result->G;
  ccl_hash *vkind = result->vkind;

  CCL_DEBUG_START_TIMED_BLOCK (("compute dependency graph"));
  {
    int j;
    const ccl_pair *p;
    const ccl_list *constraints = ar_ca_get_assertions (ca);
    depgraph_object_kind kind = DG_CONSTRAINT;

    for (j = 0; j < 2; j++)
      {
	for (p = FIRST (constraints); p; p = CDR (p))
	  {
	    ar_ca_expr *C = CAR (p);
	    ccl_list *vars = ar_ca_expr_get_variables (C, NULL);
	    
	    set_vkind (vkind, C, kind);
	    
	    while (!ccl_list_is_empty (vars))
	      {
		ar_ca_expr *var = ccl_list_take_first (vars);
		
		ccl_graph_add_data_edge (G, C, var, NULL);
		ccl_graph_add_data_edge (G, var, C, NULL);
	      }
	    ccl_list_delete (vars);
	  }
	constraints = ar_ca_get_initial_constraints (ca);
	kind = DG_INIT_CONSTRAINT;
      }
  }

  if (add_fdset)
    {
      const fdset *fds = ar_ca_get_fdset (ca);
      ccl_list *vars = fdset_get_ordered_variables (fds, 0);
      while (! ccl_list_is_empty (vars))
	{
	  ccl_pair *p;
	  ar_ca_expr *v = ccl_list_take_first (vars);
	  funcdep *F= fdset_get_funcdep (fds, v);
	  const ccl_list *Fvars = funcdep_get_inputs (F);

	  set_vkind (vkind, F, DG_FUNC);
	  ccl_graph_add_data_edge (G, v, F, NULL);
	  for (p = FIRST (Fvars); p; p = CDR (p))
	    {
	      ar_ca_expr *Fv = CAR  (p);
	      ccl_assert (Fv != v);
	      ccl_graph_add_data_edge (G, F, Fv, NULL);
	    }
	}
      ccl_list_delete (vars);
    }

  if(0) {
    const ccl_pair *p;
    const ccl_list *l_ass = ar_ca_get_initial_assignments (ca);
    
    for (p = FIRST (l_ass); p; p = CDDR (p))
      {
	ar_ca_expr *var = CAR (p);
	ar_ca_expr *value = CADR (p);

	ccl_graph_add_data_edge (G, var, value, NULL); 
	set_vkind (vkind, value, DG_INIT);
      }
  }

  {
    const ccl_pair *p;
    const ccl_list **vars;
    const ccl_list *varslist[] = {
      ar_ca_get_flow_variables (ca),
      ar_ca_get_state_variables (ca),
      NULL };

    for (vars = varslist; *vars; vars++)
      {
	for (p = FIRST (*vars); p; p = CDR (p))
	  {
	    ccl_graph_find_or_add_vertex (G, CAR (p));
	    set_vkind (vkind, CAR (p), DG_VARIABLE);
	  }
      }
  }

  {
    int i;
    int nb_trans = ar_ca_get_number_of_transitions (ca);
    ar_ca_trans **trans = ar_ca_get_transitions (ca);

    for (i = 0; i < nb_trans; i++)
      {
	int j;
	int nb_ass = ar_ca_trans_get_number_of_assignments (trans[i]);
	ar_ca_expr **a = ar_ca_trans_get_assignments (trans[i]);
	ar_ca_expr *g = ar_ca_trans_get_guard (trans[i]);

	if (! ar_ca_expr_is_false (g))
	  {
	    ccl_list *vars = ar_ca_expr_get_variables (g, NULL);
	    set_vkind (vkind, trans[i], DG_TRANSITION);
	    {
	      ar_ca_event *e = ar_ca_trans_get_event (trans[i]);
	      set_vkind (vkind, e, DG_EVENT);
	      ccl_graph_add_data_edge (G, trans[i], e, NULL);
	      ccl_graph_add_data_edge (G, e, trans[i], NULL);
	      ar_ca_event_del_reference (e);
	    }
	    
	    while (!ccl_list_is_empty (vars))
	      {
		ar_ca_expr *var = ccl_list_take_first (vars);
		ccl_graph_add_data_edge (G, trans[i], var, NULL);
	      }
	    ccl_list_delete (vars);

	    for (j = 0; j < nb_ass; j++)
	      {
		ccl_graph_add_data_edge (G, a[2 * j], trans[i], NULL);
		ccl_graph_add_data_edge (G, trans[i], a[2 * j], NULL);
		vars = ar_ca_expr_get_variables (a[2 * j + 1], NULL);
		while (!ccl_list_is_empty (vars))
		  {
		    ar_ca_expr *var = ccl_list_take_first (vars);
		    ccl_graph_add_data_edge (G, trans[i], var, NULL);
		  }
		ccl_list_delete (vars);
	      }
	  }

	ar_ca_expr_del_reference (g);
      }
  }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

depgraph * 
depgraph_clone (depgraph *dg)
{
  depgraph *result = ccl_new (depgraph);
  ccl_hash_entry_iterator *ei = ccl_hash_get_entries (dg->vkind);

  result->G = ccl_graph_dup (dg->G, NULL, NULL);
  result->vkind = ccl_hash_create (NULL, NULL, NULL, NULL);
  while (ccl_iterator_has_more_elements (ei))
    {
      ccl_hash_entry e = ccl_iterator_next_element (ei);
      ccl_assert (! ccl_hash_find (result->vkind, e.key));
      ccl_hash_find (result->vkind, e.key);
      ccl_hash_insert (result->vkind, e.object);
    }
  ccl_iterator_delete (ei);

  return result;
}

void
depgraph_delete (depgraph *dg)
{
  ccl_graph_del_reference (dg->G);
  ccl_hash_delete (dg->vkind);
  ccl_delete (dg);
}

			/* --------------- */

static void 
s_dot_vertex_data (ccl_log_type log, ccl_vertex *v, void *cbdata)
{
  void *data = ccl_vertex_get_data (v);
  depgraph_object_kind kind;
  ccl_hash_find (cbdata, data);

  ccl_log (log, "label=\"");
  kind = (intptr_t) ccl_hash_get (cbdata);
  switch (kind) 
    {
    case DG_INIT: 
    case DG_INIT_CONSTRAINT: 
      ccl_log (log, "init\\n"); 
      goto dispexpr;

    case DG_CONSTRAINT: 
      ccl_log (log, "assert\\n"); 
      goto dispexpr;

    dispexpr:
      ar_ca_expr_log (log, data);
      break;

    case DG_VARIABLE: 
      ar_ca_expr_log (log, data);
      break;

    case DG_TRANSITION: 
      ar_ca_trans_log (log, data);
      break;

    case DG_EVENT: 
      ccl_log (log, "event\\n");
      ar_ca_event_log (log, data);
      break;

    case DG_FUNC: 
      ccl_log (log, "function\\n");
      funcdep_log (log, data);
      break;

    default:
      ccl_unreachable ();
    };
  ccl_log (log, "\"");  
}

			/* --------------- */

void
depgraph_log (ccl_log_type log, const depgraph *dg)
{
  ccl_graph_log_as_dot (log, dg->G, NULL, NULL, s_dot_vertex_data, dg->vkind,
			NULL, NULL);
}

			/* --------------- */

ccl_graph *
depgraph_get_graph (const depgraph *dg)
{
  ccl_pre (dg != NULL);

  return dg->G;
}

depgraph_object_kind 
depgraph_get_vertex_kind (depgraph *dg, ccl_vertex *v)
{
  void *obj = ccl_vertex_get_data (v);

  if (!ccl_hash_find (dg->vkind, obj))
    return DG_UNKNOWN;
  return (depgraph_object_kind) (intptr_t) ccl_hash_get (dg->vkind);
}

depgraph_object_kind 
depgraph_get_kind (depgraph *dg, void *obj)
{
  if (! ccl_hash_find (dg->vkind, obj))
    return DG_UNKNOWN;
  
  return (depgraph_object_kind) (intptr_t) ccl_hash_get (dg->vkind);
}

void * 
depgraph_get_vertex_object (depgraph *dg, ccl_vertex *v)
{
  return ccl_vertex_get_data (v);
}

			/* --------------- */

static int
s_reach_search (ccl_vertex *v, void *data)
{
  void *element = ccl_vertex_get_data (v);
  if (!ccl_hash_find (data, element))
    ccl_hash_insert (data, element);

  return 0;
}

			/* --------------- */

void
depgraph_induced_subgraph (depgraph *dg, ccl_hash *objects)
{
  CCL_DEBUG_START_TIMED_BLOCK (("trim dependency graph"));
  {
    ccl_vertex_iterator *vi = ccl_graph_get_vertices (dg->G);
    ccl_list *vertices = 
      ccl_list_create_from_iterator ((ccl_pointer_iterator *) vi);
    ccl_iterator_delete (vi);
    
    while (! ccl_list_is_empty (vertices))
      {
	ccl_vertex *v = ccl_list_take_first (vertices);
	void *data = ccl_vertex_get_data (v);
	if (! ccl_hash_find (objects, data))
	  ccl_graph_remove_vertex (dg->G, v, 0);
      }
    ccl_list_delete (vertices);
  }
  CCL_DEBUG_END_TIMED_BLOCK ();
}

			/* --------------- */

ccl_hash *
depgraph_get_reachables_from_formula (depgraph *dg, ar_ca_expr *formula)
{
  ccl_list *tmp = ccl_list_create ();
  ccl_hash *result;

  ccl_list_add (tmp, formula);
  result = depgraph_get_reachables_from_formulas (dg, tmp);
  ccl_list_delete (tmp);

  return result;
}

			/* --------------- */

ccl_hash *
depgraph_get_reachables_from_formulas (depgraph *dg, ccl_list *formulas)
{
  ccl_hash *result = NULL;

  CCL_DEBUG_START_TIMED_BLOCK (("compute reachables in dep graph"));
  {
    ccl_pair *p;
    ccl_hash *variables = NULL;
    
    for (p = FIRST (formulas); p; p = CDR (p))
      variables = ar_ca_expr_get_variables_in_table (CAR (p), variables);

    result = depgraph_get_reachables_from_variables (dg, variables);
    ccl_hash_delete (variables);
  }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */

ccl_hash *
depgraph_get_reachables_from_variables (depgraph *dg, ccl_hash *variables)
{
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, NULL);

  CCL_DEBUG_START_TIMED_BLOCK (("compute reachables in dep graph"));
  {
    ccl_pointer_iterator *pi = ccl_hash_get_keys (variables);
    
    while (ccl_iterator_has_more_elements (pi))
      {
	ar_ca_expr *var = ccl_iterator_next_element (pi);

	if (! ccl_hash_find (result, var))
	  {
	    ccl_vertex *v = ccl_graph_get_vertex (dg->G, var);
	    ccl_assert (v != NULL);
	    ccl_graph_dfs (dg->G, v, 0, s_reach_search, result);
	  }
      }
    ccl_iterator_delete (pi);
  }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

			/* --------------- */

static void
s_mk_successors_with_kind (depgraph *dg, depgraph_object_kind k, ccl_vertex *v)
{
  ccl_vertex_iterator *si = ccl_vertex_get_successors (v);
  ccl_list *todo = ccl_list_create_from_iterator ((ccl_pointer_iterator *) si);
  ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_iterator_delete (si);

  while (! ccl_list_is_empty (todo))
    {
      ccl_vertex *n = ccl_list_take_first (todo);
      
      if (depgraph_get_vertex_kind (dg, n) == k && 
	  ! ccl_vertex_has_successor (v, n))
	ccl_graph_add_edge (dg->G, v, n, NULL);
      else 
	{
	  si = ccl_vertex_get_successors (n);
	  while (ccl_iterator_has_more_elements (si))
	    {
	      ccl_vertex *p = ccl_iterator_next_element (si);
	      if (! ccl_hash_find (done, p))
		{
		  ccl_hash_insert (done, p);
		  ccl_list_add (todo, p);
		}
	    }
	  ccl_iterator_delete (si);
	}
    }
  ccl_list_delete (todo);
  ccl_hash_delete (done);
}

void
depgraph_restrict_to_kind (depgraph *dg, depgraph_object_kind k)
{
  ccl_vertex_iterator *vi = ccl_graph_get_vertices (dg->G);
  ccl_pair *p;
  ccl_list *vertices = 
    ccl_list_create_from_iterator ((ccl_pointer_iterator *) vi);
  ccl_iterator_delete (vi);
    
  for (p = FIRST (vertices); p; p = CDR (p))
    {
      ccl_vertex *v = CAR (p);
      if (depgraph_get_vertex_kind (dg, v) == k)
	s_mk_successors_with_kind (dg, k, v);
    }

  while (! ccl_list_is_empty (vertices))
    {
      ccl_vertex *v = ccl_list_take_first (vertices);
      if (depgraph_get_vertex_kind (dg, v) != k)
	ccl_graph_remove_vertex (dg->G, v, 0);
    }
  ccl_list_delete (vertices);
}

static depgraph *
s_new_depgraph (void)
{
  static ccl_vertex_data_methods DG_VERTEX_METHODS = { NULL, NULL, NULL };
  static ccl_vertex_data_methods DG_EDGE_METHODS = { NULL, NULL, NULL };
  depgraph *result = ccl_new (depgraph);

  result->G = ccl_graph_create (&DG_VERTEX_METHODS, &DG_EDGE_METHODS);
  result->vkind = ccl_hash_create (NULL, NULL, NULL, NULL);

  return result;
}
