/*
 * depgraph.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef DEPGRAPH_H
# define DEPGRAPH_H

# include <ar-constraint-automaton.h>

/*In these graphs v -> u means v depends on u */

typedef enum ar_ca_object_kind_enum {
  DG_INIT,
  DG_INIT_CONSTRAINT,
  DG_CONSTRAINT,
  DG_TRANSITION,
  DG_VARIABLE,
  DG_EVENT,
  DG_FUNC,
  DG_UNKNOWN
} depgraph_object_kind;

typedef struct depgraph_st depgraph;

extern depgraph *
depgraph_compute (ar_ca *ca, int add_fdset);

extern depgraph *
depgraph_clone (depgraph *dg);

extern void
depgraph_delete (depgraph *dg);

extern void
depgraph_log (ccl_log_type log, const depgraph *dg);

extern ccl_graph *
depgraph_get_graph (const depgraph *dg);

extern depgraph_object_kind 
depgraph_get_vertex_kind (depgraph *dg, ccl_vertex *v);

extern depgraph_object_kind 
depgraph_get_kind (depgraph *dg, void *obj);

extern void * 
depgraph_get_vertex_object (depgraph *dg, ccl_vertex *v);

extern void
depgraph_induced_subgraph (depgraph *dg, ccl_hash *objects);

extern ccl_hash *
depgraph_get_reachables_from_formula (depgraph *dg, ar_ca_expr *formula);

extern ccl_hash *
depgraph_get_reachables_from_formulas (depgraph *dg, ccl_list *formulas);

extern ccl_hash *
depgraph_get_reachables_from_variables (depgraph *dg, ccl_hash *variables);

extern void
depgraph_restrict_to_kind (depgraph *dg, depgraph_object_kind k);

#endif /* ! DEPGRAPH_H */
