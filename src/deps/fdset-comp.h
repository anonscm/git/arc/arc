/*
 * fdset-comp.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef FDSET_COMP_H
# define FDSET_COMP_H

# include <deps/fdset.h>
# include <ar-constraint-automaton.h>

extern int fdset_comp_debug_is_on;
extern int fdset_comp_debug_show_fd;
extern int fdset_comp_debug_fd_checking;
extern int fdset_comp_debug_postponed_fd;
extern int fdset_comp_debug_generated_assertions;
extern int fdset_comp_debug_useless_assertions;
extern int fdset_comp_debug_irrelevant_variables;
extern int fdset_comp_debug_tautologies;

extern fdset *
fdset_compute (ar_ca *ca, int const_states, int states);

#endif /* ! FDSET_COMP_H */
