/*
 * fdset-comp.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include "varorder.h"
#include "ar-rel-semantics.h"
#include "ar-ca-expr2dd.h"
#include "ar-constraint-automaton.h"
#include "ar-solver.h"
#include "ar-ca-rewriters.h"
#include "fdset-comp.h"

#define STATE_FDS_THRESHOLD 5
#define CONST_STATE_THRESHOLD 50
#define CONST_STATE_REWRITE_DEPTH 2

/*!
 * debug flags
 */
#define DBG_IS_ON (fdset_comp_debug_is_on && ccl_debug_is_on)
#define DBG_FD(f_) (DBG_IS_ON && fdset_comp_debug_ ## f_)

#define DBG_CHECKING_FD(a)						\
  do {									\
    if (DBG_FD (fd_checking))						\
      {									\
	static int k = 0; ccl_debug ("checking FD for %p ", a); k++;	\
	ar_ca_expr_log (CCL_LOG_DEBUG, (a));				\
	ccl_debug ("\n");						\
      }									\
  } while (0)

#define DBG_ASSERTION_POSTPONED(a)				\
  do {								\
    if (DBG_FD (postponed_fd))					\
      {								\
	ccl_debug (" assertion postponed ");			\
	ar_ca_expr_log (CCL_LOG_DEBUG, (a));			\
	ccl_debug ("\n");					\
      }								\
  } while (0)

#define _DBG_FD_FOUND(_msg,var,func)			\
  do {							\
    if (DBG_FD (show_fd))				\
      {							\
	ccl_debug (_msg "FD found ");			\
	ar_ca_expr_log (CCL_LOG_DEBUG, (var));		\
	ccl_debug (" := ");				\
	ar_ca_expr_log (CCL_LOG_DEBUG, (func));		\
	ccl_debug ("\n");				\
      }							\
  } while (0)

#define DBG_FD_FOUND(var,func) _DBG_FD_FOUND ("", var, func)
#define DBG_STATE_FD_FOUND(var,func) \
  _DBG_FD_FOUND ("state ", var, func)
#define DBG_STATIC_STATE_FD_FOUND(var,func) \
  _DBG_FD_FOUND ("static state ", var, func)

#define DBG_START_TIMER(args_) \
  CCL_DEBUG_COND_START_TIMED_BLOCK (DBG_IS_ON, args_)
#define DBG_END_TIMER() \
  CCL_DEBUG_COND_END_TIMED_BLOCK (DBG_IS_ON)

#define DBG_F_START_TIMER(f_, args_) \
  CCL_DEBUG_COND_START_TIMED_BLOCK (DBG_FD(f_), args_)
#define DBG_F_END_TIMER(f_) \
  CCL_DEBUG_COND_END_TIMED_BLOCK (DBG_FD(f_))

int fdset_comp_debug_is_on = 0;
int fdset_comp_debug_show_fd = 1;
int fdset_comp_debug_fd_checking = 1;
int fdset_comp_debug_postponed_fd = 1;
int fdset_comp_debug_generated_assertions = 1;
int fdset_comp_debug_useless_assertions = 1;
int fdset_comp_debug_irrelevant_variables = 1;
int fdset_comp_debug_tautologies = 1;

#if 0
static int
s_look_for_fd (ar_ddm ddm, ar_dd rel)
{
  int j;
  int arity = AR_DD_ARITY (rel);
  int fd_found = 1;

  if (AR_DD_CODE (rel) == EXHAUSTIVE)
    {

      for (j = 0; j < arity && fd_found; j++)
	{
	  int k;
	  ar_dd son_j = (AR_DD_POLARITY (rel)
			 ? AR_DD_NOT(AR_DD_ENTHSON (rel, j))
			 : AR_DD_ENTHSON (rel, j));
	  for (k = j + 1; k < arity && fd_found; k++)
	    {
	      ar_dd son_k = (AR_DD_POLARITY (rel)
			     ? AR_DD_NOT(AR_DD_ENTHSON (rel, k))
			     : AR_DD_ENTHSON (rel, k));
	      ar_dd common = ar_dd_compute_and (ddm, son_j, son_k);
	      fd_found = ar_dd_is_zero (ddm, common);
	      AR_DD_FREE (common);
	    }
	}
    }
  else
    {
      for (j = 0; j < arity && fd_found; j++)
	{
	  int k;
	  int size_j = (AR_DD_MAX_OFFSET_NTHSON (rel, j)
			-AR_DD_MIN_OFFSET_NTHSON (rel, j) + 1);
	  
	  ar_dd son_j = (AR_DD_POLARITY (rel)
			 ? AR_DD_NOT(AR_DD_CNTHSON (rel, j))
			 : AR_DD_CNTHSON (rel, j));
	  
	  if (size_j > 1)
	    {
	      fd_found = ar_dd_is_zero (ddm, son_j);
	      continue;
	    }

	  for (k = j + 1; k < arity && fd_found; k++)
	    {
	      int size_k = (AR_DD_MAX_OFFSET_NTHSON (rel, k)
			    -AR_DD_MIN_OFFSET_NTHSON (rel, k) + 1);
	      ar_dd son_k = (AR_DD_POLARITY (rel)
			     ? AR_DD_NOT(AR_DD_CNTHSON (rel, k))
			     : AR_DD_CNTHSON (rel, k));
	      if (size_k > 1)
		{
		  fd_found = ar_dd_is_zero (ddm, son_k);
		  continue;
		}
	      else
		{
		  ar_dd common = ar_dd_compute_and (DDM, son_j, son_k);
		  fd_found = ar_dd_is_zero (ddm, common);
		  AR_DD_FREE (common);
		}
	    }
	}
    }

  return fd_found;
}
#endif
			/* --------------- */

static ar_ca_expr *
s_domain_get_value (ar_ca_exprman *eman, const ar_ca_domain *dom, int idx)
{
  int value = ar_ca_domain_get_ith_value (dom, idx);
  ar_ca_expr *result;

  if (ar_ca_domain_is_boolean (dom))
    result = ar_ca_expr_crt_boolean_constant (eman, idx);
  else if (ar_ca_domain_is_enum (dom))
    result = ar_ca_expr_get_enum_constant_by_index (eman, value);
  else
    result = ar_ca_expr_crt_integer_constant (eman, value);

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_crt_func (ar_ca_exprman *eman, ccl_list *assignments, 
	    ar_ca_expr **p_assertion)
{
  ar_ca_expr *tmp;  
  ar_ca_expr *new_a = ccl_list_take_first (assignments);
  ar_ca_expr *result = ccl_list_take_first (assignments);

  while (! ccl_list_is_empty (assignments))
    {
      ar_ca_expr *cond = ccl_list_take_first (assignments);
      ar_ca_expr *value = ccl_list_take_first (assignments);
      tmp = ar_ca_expr_crt_ite (cond, value, result);

      ar_ca_expr_del_reference (value);
      ar_ca_expr_del_reference (result);
      result = tmp;
      
      tmp = ar_ca_expr_crt_or (new_a, cond);
      ar_ca_expr_del_reference (cond);
      ar_ca_expr_del_reference (new_a);
      new_a = tmp;
    }

  tmp = ar_ca_expr_crt_not (new_a);
  if (ar_ca_expr_is_satisfiable (tmp))
    {
      *p_assertion = new_a;
    }
  else
    ar_ca_expr_del_reference (new_a);
  ar_ca_expr_del_reference (tmp);

  ccl_assert (ccl_list_is_empty (assignments));

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_compute_formula_from_fd_syntax (ar_ca_exprman *eman, ar_ca_expr *var, 
				  ar_ca_expr *A, ar_ca_expr **p_assertion)
{
  int i;
  ar_ca_expr *result;
  ccl_list *assignments = ccl_list_create ();
  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
  int card = ar_ca_domain_get_cardinality (dom);

  for (i = 0; i < card; i++)
    {
      ar_ca_expr *val = s_domain_get_value (eman, dom, i);
      ar_ca_expr *cond = ar_ca_expr_replace (A, var, val);
      ccl_list_add (assignments, cond);
      ccl_list_add (assignments, val);
    }
  result = s_crt_func (eman, assignments, p_assertion);
  ccl_list_delete (assignments);

  return result;
}

			/* --------------- */

static void
s_add_assertion_if_necessary (ar_ca_expr *var, ar_ca_expr *F, 
			      ar_ca_expr **p_assertion)
{
  ar_ca_expr *c;
  ar_ca_expr *nc;

  if (ar_ca_expr_variable_is_boolean (var))
    return;

  c = ar_ca_expr_crt_in_domain_of (F, var);
  nc = ar_ca_expr_crt_not (c);
      
  if (ar_ca_expr_is_satisfiable (nc))
    {
      if (DBG_FD (generated_assertions))
	{
	  ccl_debug ("new assertion :");
	  ar_ca_expr_log (CCL_LOG_DEBUG, c);
	  ccl_debug ("\n");
	}      
      *p_assertion = c;
    }
  else
    ar_ca_expr_del_reference (c);
  ar_ca_expr_del_reference (nc);	      
}

/**
 ** 
 ** FUNCTIONAL DEPENDENCIES FOR FLOW VARIABLES
 **
 **/

static int
s_exists_loop (fdset *fds, ar_ca_expr *var, ccl_list *Avars)
{
  ccl_pair *p;
  int result = 0;

  for (p = FIRST (Avars); p && !result; p = CDR (p))
    result = (CAR (p) != var) && fdset_is_circular (fds, var, CAR (p));

  return result;
}

#define FD_FROM_SEM s_fd_from_semantics
#if 0
static int
s_fd_from_dd (ar_ca_exprman *eman, ar_ca_expr *A,
	      fdset *fds, ccl_hash *states, ar_ca_expr **p_assertion)
{
  int i;
  ccl_pair *p;
  ar_dd rel;
  int fd_found = 0;
  ar_ca_expr *pred = NULL;
  varorder *order = varorder_create (NULL, NULL, NULL);
  ccl_list *Avars = ar_ca_expr_get_variables (A, NULL);

  for (i = 0, p = FIRST (Avars); p; p = CDR (p), i++)
    {
      varorder_add_variable (order, CAR (p));
      varorder_set_dd_index (order, CAR (p), i);
    }

  for (p = FIRST (Avars); p && !fd_found; p = CDR (p))
    {
      ar_ca_expr *var = CAR (p);

      if (pred != NULL)
	varorder_swap_dd_index (order, pred, var);

      if (ccl_hash_get_with_key (states, var) ||
	  fdset_has_dependency_for (fds, var) ||
	  s_exists_loop (fds, var, Avars))
	{
	  pred = var;
	  continue;
	}
	      
      rel = ar_ca_expr_compute_dd (A, DDM, order, NULL);

      if (ar_dd_is_one (DDM, rel) || ar_dd_is_zero (DDM, rel))
	{
	  if (DBG_FD (useless_assertions))
	    {
	      ccl_debug ("useless assertion: ");
	      ar_ca_expr_log (CCL_LOG_DEBUG, A);
	      ccl_debug ("\n");
	    }
	  AR_DD_FREE (rel);
	  break;
	}

      if (AR_DD_INDEX (rel) == varorder_get_dd_index (order, var))
	{	      
	  fd_found = s_look_for_fd (DDM, rel);
	  if (fd_found)
	    {	      
	      ar_ca_expr *F = 
		s_compute_formula_from_fd_syntax (eman, var, A, p_assertion);
	      fdset_add_dependency (fds, var, F);
	      DBG_FD_FOUND (var, F);
	      ar_ca_expr_del_reference (F);
	    }
	}
      else if (DBG_FD (irrelevant_variables))
	{
	  /* the variable is not relevant in this assertion */
	  ccl_debug ("not relevant variable '");
	  ar_ca_expr_log (CCL_LOG_DEBUG, var);
	  ccl_debug ("' in assertion ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, A);
	  ccl_debug ("\n");
	}
      AR_DD_FREE (rel);
      pred = var;
    }
  varorder_del_reference (order);
  ccl_list_delete (Avars);

  return fd_found;
}
#endif

static int
s_fd_from_semantics (ar_ca_exprman *eman, ar_ca_expr *A, fdset *fds, 
		     ccl_hash *states, ar_ca_expr **p_assertion)
{
  ccl_pair *p;
  int fd_found = 0;
  ccl_list *Avars;
  ar_ca_expr *tmp = ar_ca_expr_crt_not (A);

  if (! ar_ca_expr_is_satisfiable (tmp))
    {
      if (DBG_FD (tautologies))
	{
	  ccl_debug ("tautology detected: ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, A);
	  ccl_debug ("\n");
	}
      ar_ca_expr_del_reference (tmp);
      return 0;
    }
  ar_ca_expr_del_reference (tmp);

  Avars = ar_ca_expr_get_variables (A, NULL);
  
  for (p = FIRST (Avars); p && !fd_found; p = CDR (p))
    {
      int i;
      int Dsize;
      ar_ca_expr *C[2];
      ar_ca_expr *var = CAR (p);
      const ar_ca_domain *D;

      if (ccl_hash_get_with_key (states, var) ||
	  fdset_has_dependency_for (fds, var) ||
	  s_exists_loop (fds, var, Avars))
	{
	  continue;
	}

      D = ar_ca_expr_variable_get_domain (var);
      Dsize = ar_ca_domain_get_cardinality (D);
      fd_found = 1;
      for (i = 0; i < Dsize && fd_found; i++)
	{
	  int j;
	  ar_ca_expr *v1 = ar_ca_expr_variable_get_ith_value (var, i);

	  C[0] = ar_ca_expr_replace (A, var, v1);
	  ar_ca_expr_del_reference (v1);

	  for (j = i + 1; j < Dsize && fd_found; j++)
	    {
	      ar_ca_expr *v2 = ar_ca_expr_variable_get_ith_value (var, j);
	      
	      C[1] = ar_ca_expr_replace (A, var, v2);
	      ar_ca_expr_del_reference (v2);
	      
	      if (1) {
		ar_ca_expr *aux = ar_ca_expr_crt_and (C[0], C[1]);
		fd_found = ! ar_ca_expr_is_satisfiable (aux);
		ar_ca_expr_del_reference (aux);
	      }
	      ar_ca_expr_del_reference (C[1]);
	    }
	  ar_ca_expr_del_reference (C[0]);
	}
      if (fd_found)
	{	      
	  ar_ca_expr *F = 
	    s_compute_formula_from_fd_syntax (eman, var, A, p_assertion);

	  fdset_add_dependency (fds, var, F, A);
	  DBG_FD_FOUND (var, F);
	  ar_ca_expr_del_reference (F);
	}
    }
  ccl_list_delete (Avars);

  return fd_found;
}

static int
s_is_cst (ar_ca_expr *F, fdset *fds, ccl_hash *states)
{
  return (ar_ca_expr_get_kind (F) == AR_CA_CST);
}

static int
s_is_state_var (ar_ca_expr *F, fdset *fds, ccl_hash *states)
{
#if 1
  int res;
  ccl_list *l = ar_ca_expr_get_variables (F, NULL);

  if (ccl_list_get_size (l) != 1)
    res = 0;
  else
    {
      res = (ccl_hash_find (states, CAR(FIRST(l))));
    }
  ccl_list_delete (l);
  return res;
#else
  return (ccl_hash_find (states, F));
#endif 
}

static int
s_is_not_flow_var (ar_ca_expr *F, fdset *fds, ccl_hash *states)
{
  if (ar_ca_expr_get_kind (F) == AR_CA_VAR &&
      ! ccl_hash_find (states, F) &&
      ! fdset_has_dependency_for (fds, F))
    return 0;
  return 1;
}

static int
s_is_any_expr (ar_ca_expr *F, fdset *fds, ccl_hash *states)
{
  return 1;
}

static int
s_may_add_dependency (ar_ca_expr *var, ar_ca_expr *func, ar_ca_expr *A, 
		      fdset *fds, ccl_hash *states, ar_ca_expr **p_assertion,
		      int (*filter) (ar_ca_expr *F, fdset *fds, 
				     ccl_hash *states))
{
  int result = 0;

  if (ar_ca_expr_get_kind (var) == AR_CA_VAR &&
      filter (func, fds, states) && 
      ! ar_ca_expr_has_variable (func, var) &&
      ! ccl_hash_get_with_key (states, var) &&
      fdset_may_add_dependency (fds, var, func))
    {
      fdset_add_dependency (fds, var, func, A);
      DBG_FD_FOUND (var, func);
      s_add_assertion_if_necessary (var, func, p_assertion);
      result = 1;
    }
  return result;
}

static int
s_fd_from_syntax (ar_ca_expr *A, fdset *fds, ccl_hash *states, 
		  ar_ca_expr **p_assertion, 
		  int (*filter) (ar_ca_expr *F, fdset *fds, 
				 ccl_hash *states))
{
  int i;
  ar_ca_expr **args = ar_ca_expr_get_args (A);
  int result;

  *p_assertion = NULL;

  if ((ar_ca_expr_get_kind (A) != AR_CA_EQ ||
      (ar_ca_expr_get_kind (args[0]) != AR_CA_VAR &&
       ar_ca_expr_get_kind (args[1]) != AR_CA_VAR)) &&
      !(ar_ca_expr_get_kind (A) == AR_CA_NOT && 
	ar_ca_expr_get_kind (args[0]) == AR_CA_VAR) &&
      ar_ca_expr_get_kind (A) != AR_CA_VAR)
    return 0;

  if (ar_ca_expr_get_kind (A) == AR_CA_VAR)
    {
      ar_ca_exprman *eman = ar_ca_expr_get_manager (A);
      ar_ca_expr *f = ar_ca_expr_crt_boolean_constant (eman, 1);
      result = s_may_add_dependency (A, f, A, fds, states, p_assertion, filter);
      ar_ca_expr_del_reference (f);
      ar_ca_exprman_del_reference (eman);
    }
  else if (ar_ca_expr_get_kind (A) == AR_CA_NOT)
    {
      ar_ca_exprman *eman = ar_ca_expr_get_manager (A);
      ar_ca_expr *f = ar_ca_expr_crt_boolean_constant (eman, 0);
      result = s_may_add_dependency (args[0], f, A, fds, states, p_assertion,
				     filter);
      ar_ca_expr_del_reference (f);
      ar_ca_exprman_del_reference (eman);
    }
  else
    {
      result = 0;
      for (i = 0; i < 2 && ! result; i++)
	{
	  ar_ca_expr *var = args[i];
	  ar_ca_expr *func = args[(i + 1) % 2];

	  result = s_may_add_dependency (var, func, A, fds, states, p_assertion,
					 filter);
	}
    }

  return result;
}

static int 
s_apply_assertion_filter (ccl_list *C, fdset *fds, ccl_hash *states,
			  int (*filter)(ar_ca_expr *, fdset *, ccl_hash *))
{
  int change = 0;
  int nb_assertions = ccl_list_get_size (C);

  while (nb_assertions)
    {	  
      ar_ca_expr *a = ccl_list_take_first (C);
      ar_ca_expr *newa = NULL;

      DBG_CHECKING_FD (a);
      if (! s_fd_from_syntax (a, fds, states, &newa, filter))
	{
	  ccl_assert (newa == NULL);
	  DBG_ASSERTION_POSTPONED (a);
	  ccl_list_add (C, a);
	  nb_assertions--;
	}
      else
	{
	  ar_ca_expr_del_reference (a);
	  if (newa != NULL) 
	    ccl_list_add (C, newa);
	  else
	    nb_assertions--;
	  change = 1;
	}
    }  
  return (change);
}

static int 
s_compute_fd_for_flow_variables (ar_ca *ca, fdset *fds, ccl_hash *states)
{
  int i;
  int new_fds = 0;
  ar_ca_exprman *eman = ar_ca_get_expression_manager (ca);
  int nb_assertions = ar_ca_get_number_of_assertions (ca);
  ccl_list *C;
  int (*filters[])(ar_ca_expr *, fdset *, ccl_hash *) = {
    s_is_cst, 
    s_is_state_var, 
    s_is_not_flow_var,
    s_is_any_expr,
    NULL
  };

  DBG_START_TIMER (("initial constraint list"));
  C = ccl_list_deep_dup (ar_ca_get_assertions (ca), (ccl_duplicate_func *)
			 ar_ca_expr_add_reference);
  DBG_END_TIMER ();
  
  DBG_START_TIMER (("identify constant and state variables"));
  for (i = 0; i < 2; i++)
    {
      while (s_apply_assertion_filter (C, fds, states, filters[i]))
	{
	  ccl_pair *p;
	  new_fds = 1;
	  for (p = FIRST (C); p; p = CDR (p))
	    {
	      ar_ca_expr *a = CAR (p);
	      CAR (p) = ar_ca_rewrite_expr_with_fd (fds, a, 0);
	      ar_ca_expr_del_reference (a);
	    }
	}
    }
  DBG_END_TIMER ();

  DBG_START_TIMER (("apply filters"));
  for (i = 2; filters[i] != NULL; i++)
    {
      nb_assertions = ccl_list_get_size (C);

      while (nb_assertions)
	{	  
	  ar_ca_expr *a = ccl_list_take_first (C);
	  ar_ca_expr *newa = NULL;

	  DBG_CHECKING_FD (a);
	  if (! (s_fd_from_syntax (a, fds, states, &newa, filters[i])) 
	      && ! (FD_FROM_SEM (eman, a, fds, states, &newa))
	      )
	    {
	      ccl_assert (newa == NULL);
	      DBG_ASSERTION_POSTPONED (a);
	      ccl_list_add (C, a);
	      nb_assertions--;
	    }
	  else
	    {
	      new_fds = 1;
	      ar_ca_expr_del_reference (a);

	      if (newa != NULL)	
		ccl_list_add (C, newa);
	      nb_assertions = ccl_list_get_size (C);
	    }
	}
    }
  DBG_END_TIMER ();

  DBG_START_TIMER (("apply semantic"));
  while (! ccl_list_is_empty (C))
    {
      int fd = 0;
      ar_ca_expr *a = ccl_list_take_first (C);
      ar_ca_expr *newa = NULL;
      
      DBG_CHECKING_FD (a);
      fd = s_fd_from_syntax (a, fds, states, &newa, s_is_any_expr);
      if (! fd)
	fd = FD_FROM_SEM (eman, a, fds, states, &newa);
      if (fd)
	new_fds = 1;
      ar_ca_expr_del_reference (a);
      if (newa != NULL)
	ccl_list_add (C, newa);
    }
  DBG_END_TIMER ();
  
  ccl_list_delete (C);
  ar_ca_exprman_del_reference (eman);
  
  return new_fds;
}

struct scc_data
{
  ccl_hash *scc2svars;
  ccl_hash *ca_vars;
};

static void 
s_attach_to_scc(ccl_graph *G, void *data, int comp, void *cbdata)
{
  struct scc_data *sd = cbdata;
  ccl_hash *svars;

  /* label are either transitions and variables. ignore transitions. */
  if (! ccl_hash_find (sd->ca_vars, data))
    return;

  if (ccl_hash_find (sd->scc2svars, (void *) (intptr_t) comp))
    svars = ccl_hash_get (sd->scc2svars);
  else
    {
      svars = ccl_hash_create (NULL, NULL, NULL, NULL);
      ccl_hash_insert (sd->scc2svars, svars);
    }

  ccl_assert (! ccl_hash_find (svars, data));
  ccl_hash_find (svars, data);
  ccl_hash_insert (svars, data);
}

static ccl_list * 
s_compute_set_of_state_variables (ar_ca *ca, ccl_hash *vars)
{
  int i;
  struct scc_data sd;
  ccl_pointer_iterator *pi;
  ccl_list *result;
  static ccl_vertex_data_methods VM = { NULL, NULL, NULL };
  static ccl_edge_data_methods EM = { NULL, NULL, NULL };
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  ccl_graph *TG = ccl_graph_create (&VM, &EM);

  for (i = 0; i < nb_trans; i++)
    {
      ar_ca_trans *t = trans[i];
      int j;
      int nb_a = ar_ca_trans_get_number_of_assignments (t);
      ar_ca_expr **a = ar_ca_trans_get_assignments (t);

      for (j = 0; j < nb_a; j++)
	{
	  ccl_graph_add_data_edge (TG, t, a[2 * j], NULL);
	  ccl_graph_add_data_edge (TG, a[2 * j], t, NULL);	  
	}
    }
  sd.scc2svars = ccl_hash_create (NULL, NULL, NULL, NULL);
  sd.ca_vars = vars;
  ccl_graph_compute_scc (TG, s_attach_to_scc, &sd);
  ccl_graph_del_reference (TG);

  pi = ccl_hash_get_elements (sd.scc2svars);
  result = ccl_list_create_from_iterator (pi);
  ccl_iterator_delete (pi);
  ccl_hash_delete (sd.scc2svars);

  return result;
}

static void 
s_order_variables (ar_ca *ca, varorder *order, void *clientdata)
{
  int k;
  int o = 0;
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_state_variables (ca);
  
  for (k = 0; k < 2; k++)
    {
      for (p = FIRST (vars); p; p = CDR (p))
	varorder_set_dd_index (order, CAR (p), o++);
      vars = ar_ca_get_flow_variables (ca);
    }
}

static ar_dd 
s_reachables_for_set (ar_ca *ca, ccl_hash *svars, ar_relsem **p_rs)
{
  ar_dd result;
  /*  ar_ca *rca = ar_ca_restrict_to_variables (ca, svars, NULL); */
  ar_ca *rca = ar_ca_add_reference (ca);
  ccl_pointer_iterator *pi = ccl_hash_get_keys (svars);
  const varorder *order;
  ar_ddm ddm;
  ar_dd wvars;
  ar_dd tmp[2];
  int fixpoint;
  int nb_a;
  ar_dd *a;
  
  *p_rs = ar_compute_relational_semantics (rca, s_order_variables, NULL);
  ddm = ar_relsem_get_decision_diagrams_manager (*p_rs);
  order = ar_relsem_get_order (*p_rs);
  wvars = ar_dd_create_projection_list (ddm);
  a = ar_relsem_get_assertions (*p_rs, &nb_a);

  while (ccl_iterator_has_more_elements (pi))
    {
      ar_ca_expr *var = ccl_iterator_next_element (pi);
      int ddidx = varorder_get_dd_index (order, var);
      ar_dd_projection_list_add (ddm, &wvars, ddidx, ddidx);
    }
  ccl_iterator_delete (pi);
  tmp[0] = ar_relsem_get_initial (*p_rs);
  result = ar_dd_project_on_variables (ddm, tmp[0], wvars);
  AR_DD_FREE (tmp[0]);

  fixpoint = 0;
  while (! fixpoint)
    {
      tmp[0] = ar_relsem_compute_post (*p_rs, result, nb_a, a);
      tmp[1] = ar_dd_project_on_variables (ddm, tmp[0], wvars);
      AR_DD_FREE (tmp[0]);
      tmp[0] = ar_dd_compute_or (ddm, result, tmp[1]);
      fixpoint = (tmp[0] == result);
      AR_DD_FREE (tmp[1]);
      AR_DD_FREE (result);
      result = tmp[0];
    }
  AR_DD_FREE (wvars);

  ar_ca_del_reference (rca);

  return result;
}

static int 
s_cmp_domain_sizes (const void *p1, const void *p2)
{
  const ar_ca_domain *d1 = ar_ca_expr_variable_get_domain (p1);
  const ar_ca_domain *d2 = ar_ca_expr_variable_get_domain (p2);

  return ar_ca_domain_get_cardinality (d1) - ar_ca_domain_get_cardinality (d2);
}

#if 0
static int
s_var_has_name (const char *s, ar_ca_expr *v)
{
  char *vs = ar_ca_expr_to_string (v);
  int res = ccl_string_equals (s, vs);
  ccl_string_delete (vs);
  return res;
}
#endif

static int 
s_compute_dependencies_for_set (ar_ca *ca, ccl_hash *svars, fdset *fds)
{
  int new_fds = 0;
  ar_relsem *rs;  
  ar_dd reach = s_reachables_for_set (ca, svars, &rs);
  ar_ca_exprman *eman = ar_relsem_get_expression_manager (rs);
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  const varorder *order = ar_relsem_get_order (rs);
  ccl_pointer_iterator *pi = ccl_hash_get_keys (svars);
  ccl_list *vars = ccl_list_create_from_iterator (pi);
  ccl_hash *dd2e = NULL;

  ccl_list_sort (vars, s_cmp_domain_sizes);
  ar_ca_expr_build_dd_caches (NULL, &dd2e);
  ccl_iterator_delete (pi);

  while (! ccl_list_is_empty (vars))
    {
      ar_ca_expr *v = ccl_list_take_first (vars);
      int ddindex = varorder_get_dd_index (order, v);
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (v);
      int i, max = ar_ca_domain_get_cardinality (dom);
      ar_dd *cof = ccl_new_array (ar_dd, max + 1);
      int fd = 1;

      ccl_assert (! fdset_has_dependency_for (fds, v));
      ccl_assert (ddindex >= 0);

      for (i = 0; i < max; i++)
	{
	  ar_ca_expr *c = ar_ca_expr_variable_get_ith_value (v, i);
	  ar_ca_expr *eq = ar_ca_expr_crt_eq (v, c);
	  ar_dd tmp = ar_relsem_compute_simple_set (rs, eq);
	  ar_dd aux = ar_dd_compute_and (ddm, tmp, reach);

	  cof[i] = ar_dd_project_variable (ddm, aux, ddindex);
	  AR_DD_FREE (tmp);
	  AR_DD_FREE (aux);
	  ar_ca_expr_del_reference (c);
	  ar_ca_expr_del_reference (eq);
	}

      cof[max] = ar_dd_dup_zero (ddm);

      for (i = 0; i < max && fd; i++)
	{
	  ar_dd tmp;
	      
	  fd = ! ar_dd_intersect (ddm, cof[i], cof[max]);
	  tmp = ar_dd_compute_or (ddm, cof[max], cof[i]);
	  AR_DD_FREE (cof[max]);
	  cof[max] = tmp;
	}

      if (fd)
	{
	  int i;
	  ar_ca_expr *val = ar_ca_expr_variable_get_ith_value (v, max - 1);
	  ar_dd tmp;

	  for (i = max - 2; i >= 0; i--)
	    {
	      ar_ca_expr *cond = 
		ar_ca_expr_compute_expr_from_dd (ddm, eman, order, cof[i],
						 dd2e);
	      ar_ca_expr *c = ar_ca_expr_variable_get_ith_value (v, i);
	      ar_ca_expr *ite = 
		ar_ca_expr_crt_ite (cond, c, val);
	      ar_ca_expr_del_reference (cond);
	      ar_ca_expr_del_reference (c);
	      ar_ca_expr_del_reference (val);
	      val = ite;
	    }
	  if (fdset_may_add_dependency (fds, v, val))
	    {	      
	      DBG_STATE_FD_FOUND (v, val);
	      fdset_add_dependency (fds, v, val, NULL);
	      new_fds = 1;
	    }

	  ar_ca_expr_del_reference (val);
	  
	  tmp = ar_dd_project_variable (ddm, reach, ddindex);
	  AR_DD_FREE (reach);
	  reach = tmp;
	}

      for (i = 0; i < max + 1; i++)
	AR_DD_FREE (cof[i]);
      ccl_delete (cof);
    }
  
  ar_ca_expr_clean_up_dd_caches (NULL, dd2e);
  ccl_list_delete (vars);
  AR_DD_FREE (reach);
  ar_ca_exprman_del_reference (eman);

  ar_relsem_del_reference (rs);

  return new_fds;
}

static int  
s_compute_fd_for_const_state_variables (ar_ca *ca, fdset *fds)
{
  int i;
  int result = 1;
  int change;
  ccl_hash *vars = ar_ca_get_variables (ca, AR_CA_STATE_VARS);
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  ccl_hash *subst = ccl_hash_create (NULL, NULL, NULL, NULL);
  const ccl_pair *p;
  ccl_hash *init = ccl_hash_create (NULL, NULL, NULL, NULL);
  const ccl_list *exprs = ar_ca_get_initial_assignments (ca);
  ccl_set *constants = NULL;
  
  for (p = FIRST (exprs); p; p = CDDR (p))
    {
      ar_ca_expr *v = CAR (p);
      ar_ca_expr *val = CADR (p);
      
      if (! fdset_has_dependency_for (fds, v))
	{
	  ccl_hash_find (init, v);
	  ccl_hash_insert (init, val);
	}
    }

  do {
    change = 0;
    for (i = 0; i < nb_trans && ccl_hash_get_size (vars) > 0; i++)
      {
	int j;
	int nb_a = ar_ca_trans_get_number_of_assignments (trans[i]);
	ar_ca_expr **a = ar_ca_trans_get_assignments (trans[i]);
	ar_ca_expr *g = ar_ca_trans_get_guard (trans[i]);
	ar_ca_expr *ng = NULL;
	
	if (constants == NULL ||
	    ar_ca_expr_has_variable_in_table (g, constants))
	  {
	    ng = ar_ca_rewrite_expr_with_bounded_fd (fds, g, 0,
						     CONST_STATE_REWRITE_DEPTH);
	    if( ar_ca_expr_get_size (ng) > CONST_STATE_THRESHOLD)
	      {
		ar_ca_expr_del_reference (ng);
		ng = NULL;
	      }
	  }
	
	if (ng == NULL || ar_ca_expr_is_satisfiable (ng))
	  {
	    for (j = 0; j < nb_a && ccl_hash_get_size (vars) > 0; j++, a += 2)
	      {
		if (ccl_hash_find (vars, *a))
		  ccl_hash_remove (vars);
	      }
	  }
	ar_ca_expr_del_reference (g);
	ccl_zdelete (ar_ca_expr_del_reference, ng);
      }
    ccl_zdelete (ccl_set_delete, constants);
    constants = NULL;
    
    if (ccl_hash_get_size (vars) > 0 && ccl_hash_get_size (init) > 0)
      {
	ccl_pointer_iterator *vi = ccl_hash_get_keys (vars);
	while (ccl_iterator_has_more_elements (vi))
	  {
	    ar_ca_expr *var = ccl_iterator_next_element (vi);
	    if (ccl_hash_find (init, var))
	      {
		ar_ca_expr *val = ccl_hash_get (init);

		if (constants == NULL)
		  constants = ccl_set_create ();

		ccl_set_add (constants, var);
		ccl_hash_remove (init);
		
		ccl_assert (ar_ca_expr_get_kind (val) == AR_CA_CST);
		ccl_assert (fdset_may_add_dependency (fds, var, val));
		DBG_STATIC_STATE_FD_FOUND (var, val);
		fdset_add_dependency (fds, var, val, NULL);
		change = 1;		
	      }
	  }
	ccl_iterator_delete (vi);
	if (change)
	  result = 1;
      }
  } while (change);
  
  ccl_hash_delete (vars);
  ccl_hash_delete (subst);
  ccl_hash_delete (init);
  
  return result;
}

static int
s_cmp_tset (const void *p1, const void *p2)
{
  const ccl_hash *sv1 = p1;
  const ccl_hash *sv2 = p2;

  return (ccl_hash_get_size (sv2) - ccl_hash_get_size (sv1));
}

static int 
s_compute_fd_for_state_variables (ar_ca *srcca, fdset *result, ccl_hash *vars)
{
  int new_fds = 0;
  ar_ca *ca = ar_ca_remove_functional_dependencies (srcca, result);
  ccl_list *sets = s_compute_set_of_state_variables (ca, vars);
  ccl_hash *not_visited = ar_ca_get_variables (ca, AR_CA_STATE_VARS);
  ccl_list_sort (sets, s_cmp_tset);

  while (! ccl_list_is_empty (sets))
    {
      ccl_hash *svars = ccl_list_take_first (sets);
      ar_ca *rca = ar_ca_restrict_to_variables (ca, svars, NULL, 0);
      ccl_pointer_iterator *pi = ccl_hash_get_keys (svars);
	  
      while (ccl_iterator_has_more_elements (pi))
	{
	  ar_ca_expr *var = ccl_iterator_next_element (pi);
	  ccl_assert (ccl_hash_find (not_visited, var));
	  ccl_hash_find (not_visited, var);
	  ccl_hash_remove (not_visited);
	}
      ccl_iterator_delete (pi);
      
      if (ar_ca_get_number_of_variables (rca) < STATE_FDS_THRESHOLD &&
	  s_compute_dependencies_for_set (rca, svars, result))
	new_fds = 1;

      ccl_hash_delete (svars);
      ar_ca_del_reference (rca);
    }
  ccl_hash_delete (not_visited);
  ccl_list_delete (sets);
  ar_ca_del_reference (ca);

  return new_fds;
}

fdset *
fdset_compute (ar_ca *ca, int const_states, int with_states)
{
  DBG_START_TIMER (("compute FDs"));
  fdset *result = fdset_create ();
  ccl_hash *states = ar_ca_get_variables (ca, AR_CA_STATE_VARS);

  DBG_START_TIMER (("compute flow FDs"));
  s_compute_fd_for_flow_variables (ca, result, states);
  DBG_END_TIMER ();
  
  if (const_states)
    {
      DBG_START_TIMER (("compute const state FDs"));
      s_compute_fd_for_const_state_variables (ca, result);
      DBG_END_TIMER ();
    }

  if (with_states)
    {
      DBG_START_TIMER (("compute state FDs"));
      while (s_compute_fd_for_state_variables (ca, result, states))
	s_compute_fd_for_const_state_variables (ca, result);
      DBG_END_TIMER ();
    }
  
  if (DBG_IS_ON)
    ccl_debug ("found %d / %d FDs.\n", 
	       fdset_get_number_of_functions (result),
	       ar_ca_get_number_of_variables (ca));
  ccl_hash_delete (states);
  DBG_END_TIMER ();
  
  return result;
}
