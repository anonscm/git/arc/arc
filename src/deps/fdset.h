/*
 * fdset.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef FDSET_H
# define FDSET_H

# include <ccl/ccl-graph.h>
# include "ar-ca-expression.h"

typedef struct fdset_st fdset;
typedef struct funcdep_st funcdep;

extern fdset * 
fdset_create (void);

extern fdset * 
fdset_add_reference (fdset *fds);

extern void 
fdset_del_reference (fdset *fds);

extern int 
fdset_get_number_of_functions (const fdset *fds);

extern int
fdset_is_circular (const fdset *fds, ar_ca_expr *var, ar_ca_expr *F);

extern int
fdset_may_add_dependency (const fdset *fds, ar_ca_expr *var, ar_ca_expr *F);

extern void
fdset_add_dependency (fdset *fds, ar_ca_expr *var, ar_ca_expr *F,
		      ar_ca_expr *from);

extern int
fdset_has_dependency_for (const fdset *fds, ar_ca_expr *var);

extern const ccl_graph *
fdset_get_dependency_graph (const fdset *fds);

extern ccl_list *
fdset_get_ordered_variables (const fdset *fds, int all);

extern int
fdset_compare_variables (const void *v1, const void *v2, void *data);

extern void
fdset_log (ccl_log_type log, const fdset *fds);

extern void
fdset_remove_dependency_for (fdset *fds, ar_ca_expr *var);

extern fdset *
fdset_get_constants_and_variables (const fdset *fds);

extern funcdep * 
fdset_get_funcdep (const fdset *fds, ar_ca_expr *var);

extern int
fdset_implies (const fdset *fds, ar_ca_expr *e);

extern ar_ca_expr *
funcdep_get_output (const funcdep *fd);

extern const ccl_list *
funcdep_get_inputs (const funcdep *fd);

extern ar_ca_expr *
funcdep_get_function (const funcdep *fd);

extern void
funcdep_log (ccl_log_type log, const funcdep *fd);

#endif /* ! FDSET_H */
