/*
 * fdset.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-ca-expr2dd.h"
#include "ar-constraint-automaton.h"
#include "fdset.h"

struct funcdep_st
{
  ccl_list *inputs;
  ar_ca_expr *output;
  ar_ca_expr *function;
  ar_ca_expr *from;
};

struct fdset_st 
{
  int refcount;
  ccl_hash *fdeps;
  ccl_graph *depgraph;
  ccl_list *ordering;
  ccl_hash *order;
  ccl_hash *from;
  ccl_hash *tclosure;
};

static funcdep *
s_create_funcdep (ar_ca_expr *output, ar_ca_expr *function, ar_ca_expr *from);

static void
s_delete_funcdep (funcdep *fd);

static int
s_has_at_most_one_variable (ar_ca_expr *e);

fdset * 
fdset_create (void)
{
  static ccl_vertex_data_methods VM = { NULL, NULL, NULL };
  static ccl_edge_data_methods EM = { NULL, NULL, NULL };
  fdset *result = ccl_new (fdset);

  result->refcount = 1;
  result->fdeps = 
    ccl_hash_create (NULL, NULL, (ccl_delete_proc *) ar_ca_expr_del_reference,
		     (ccl_delete_proc *) s_delete_funcdep);

  result->depgraph = ccl_graph_create (&VM, &EM);
  result->ordering = ccl_list_create ();
  result->order = ccl_hash_create (NULL, NULL, NULL, NULL);
  result->from = ccl_hash_create (NULL, NULL, NULL, NULL);
  
  return result;
}

fdset * 
fdset_add_reference (fdset *fds)
{
  ccl_pre (fds != NULL && fds->refcount > 0);

  fds->refcount++;
  
  return fds;
}

void 
fdset_del_reference (fdset *fds)
{
  ccl_pre (fds != NULL && fds->refcount > 0);

  fds->refcount--;
  if (fds->refcount == 0)
    {
      ccl_hash_delete (fds->from);
      ccl_hash_delete (fds->fdeps);
      ccl_hash_delete (fds->order);
      ccl_graph_del_reference (fds->depgraph);
      ccl_zdelete (ccl_list_delete, fds->ordering);
      ccl_delete (fds);
    }
}

int 
fdset_get_number_of_functions (const fdset *fds)
{
  ccl_pre (fds != NULL);

  return ccl_hash_get_size (fds->fdeps);
}

struct circular_test_data
{
  int found;
  ccl_hash *vars;
};

static int
s_circular_dependency_test (ccl_vertex *v, void *data)
{
  struct circular_test_data *ctd = data;
  
  if (ccl_hash_find (ctd->vars, ccl_vertex_get_data (v)))
    ctd->found = 1;

  return ctd->found;
}

int
fdset_is_circular (const fdset *fds, ar_ca_expr *var, ar_ca_expr *F)
{
  int result;

  ccl_pre (fds != NULL); 
  ccl_pre (var != NULL); 
  ccl_pre (F != NULL);

  if (ar_ca_expr_get_kind (F) == AR_CA_VAR)
    {
      if (F == var)
	result = 1;
      else
	{
	  ccl_vertex *src = ccl_graph_get_vertex (fds->depgraph, var);
	  ccl_vertex *tgt = ccl_graph_get_vertex (fds->depgraph, F);

	  if (src != NULL && tgt != NULL)
	    result = ccl_graph_is_reachable_from (fds->depgraph, tgt, src);
	  else
	    result = 0;
	}
    }
  else 
    {
      struct circular_test_data ctd; 

      ctd.found = 0;
      ctd.vars = ar_ca_expr_get_variables_in_table (F, NULL);

      if (ccl_hash_get_size (ctd.vars) != 0)
	{
	  ccl_vertex *v = ccl_graph_get_vertex (fds->depgraph, var);
	  
	  if (v == NULL)
	    ctd.found = ccl_hash_find (ctd.vars, var);
	  else
	    ccl_graph_dfs (fds->depgraph, v, 1, s_circular_dependency_test, 
			   &ctd);
	}
      ccl_hash_delete (ctd.vars);
      result = ctd.found;
    }

  return result;
}

int
fdset_may_add_dependency (const fdset *fds, ar_ca_expr *var, 
				ar_ca_expr *F)
{
  return (! fdset_has_dependency_for (fds, var) &&
	  ! fdset_is_circular (fds, var, F));
}

static void
s_propagate_const_or_var (fdset *fds, ar_ca_expr *var, ar_ca_expr *F)
{
  ccl_list *todo = ccl_list_create ();

  ccl_list_add (todo, var);
  ccl_list_add (todo, F);

  while (! ccl_list_is_empty (todo))
    {
      ar_ca_expr *tmp;
      ar_ca_expr *var = ccl_list_take_first (todo);
      ar_ca_expr *F = ccl_list_take_first (todo);
      ccl_vertex *v = ccl_graph_get_vertex (fds->depgraph, var);
      ccl_edge_iterator *ei = ccl_vertex_get_in_edges (v);
      ccl_list *epreds =
	ccl_list_create_from_iterator ((ccl_pointer_iterator *) ei);
      ccl_iterator_delete (ei);						       

      while (! ccl_list_is_empty (epreds))
	{
	  ccl_edge *epred = ccl_list_take_first (epreds);
	  ccl_vertex *pred = ccl_edge_get_src (epred);
	  ar_ca_expr *p = ccl_vertex_get_data (pred);
	  funcdep *fdp = fdset_get_funcdep (fds, p);
	  ccl_list *ninputs;
	  
	  /* remove dependency p -> var */
	  ccl_list_remove (fdp->inputs, var);
	  ccl_graph_remove_edge (fds->depgraph, epred);
	  tmp = ar_ca_expr_replace (fdp->function, var, F);	  
	  ar_ca_expr_del_reference (fdp->function);
	  fdp->function = tmp;

	  ninputs = ar_ca_expr_get_variables (F, NULL);

	  /* create dependency p -> F if  F is a variable */
	  if (! ccl_list_is_empty (ninputs))
	    {
	      ar_ca_expr *vv = CAR (FIRST (ninputs));
	      ccl_assert (ccl_list_get_size (ninputs) == 1);

	      if (! ccl_list_has (fdp->inputs, vv))
		{
		  ccl_list_add (fdp->inputs, vv);
		  ccl_graph_add_data_edge (fds->depgraph, p, vv, NULL);
		}
	    }
	  else if (ccl_list_is_empty (fdp->inputs))
	    {
	      /* now p is assigned a constant */
	      if (ar_ca_expr_get_kind (fdp->function) != AR_CA_CST &&
		  ar_ca_expr_is_boolean_expr (fdp->function))
		{
		  tmp = ar_ca_expr_simplify (fdp->function);
		  ar_ca_expr_del_reference (fdp->function);
		  fdp->function = tmp;
		}
	      ccl_assert (ar_ca_expr_get_kind (fdp->function) == AR_CA_CST);
	      ccl_list_add (todo, p);
	      ccl_list_add (todo, fdp->function);
	    }
	  ccl_list_delete (ninputs);
	}
      ccl_list_delete (epreds);
    }
  ccl_list_delete (todo);
}


void
fdset_add_dependency (fdset *fds, ar_ca_expr *var, ar_ca_expr *F,
		      ar_ca_expr *from)
{
  ccl_pair *p;
  ccl_vertex *src = ccl_graph_get_vertex (fds->depgraph, var);
  funcdep *fd = s_create_funcdep (var, F, from);

  ccl_pre (! fdset_is_circular (fds, var, F)); 
  ccl_pre (from == NULL || ! ccl_hash_find (fds->from, from));

  if (from != NULL)
    {
      ccl_hash_find (fds->from, from);
      ccl_hash_insert (fds->from, fds);
    }
  
  if (src == NULL)
    src = ccl_graph_find_or_add_vertex (fds->depgraph, var);
  else if (s_has_at_most_one_variable (F))
    s_propagate_const_or_var (fds, var, F);
      
  for (p = FIRST (fd->inputs); p; p = CDR (p))
    {
      ar_ca_expr *fvar = CAR (p);
      ccl_vertex *tgt = ccl_graph_find_or_add_vertex (fds->depgraph, fvar);
      
      if (! ccl_vertex_has_successor (src, tgt))
	ccl_graph_add_edge (fds->depgraph, src, tgt, NULL);
    }
      
  ccl_hash_find (fds->fdeps, ar_ca_expr_add_reference (var));
  ccl_hash_insert (fds->fdeps, fd);
  
  if (! ccl_list_is_empty (fds->ordering))
    {
      ccl_list_clear (fds->ordering, NULL);
      ccl_hash_delete (fds->order);
      fds->order = ccl_hash_create (NULL, NULL, NULL, NULL);
    }
}

int
fdset_has_dependency_for (const fdset *fds, ar_ca_expr *var)
{
  ccl_pre (fds != NULL && var != NULL);

  return ccl_hash_find (fds->fdeps, var);
}

/*
ar_ca_expr * 
fdset_get_dependency_for (const fdset *fds, ar_ca_expr *var)
{
  ar_ca_expr *result;

  ccl_pre (fds != NULL && var != NULL);

  if (ccl_hash_find (fds->fdeps, var)) 
    {
      result = ccl_hash_get (fds->fdeps);
    }
  else
    {
      result = NULL;
    }

  return result;
}
*/

const ccl_graph *
fdset_get_dependency_graph (const fdset *fds)
{
  ccl_pre (fds != NULL);

  return fds->depgraph;
}

static void
s_compute_ordering (const fdset *fds, ccl_list *dest, int all, int foreval)
{
  intptr_t i;
  ccl_pair *p;
  ccl_list *to = ccl_graph_topological_order (fds->depgraph);
  void (*adder) (ccl_list *, void *);

  if (foreval)
    adder = ccl_list_put_first;
  else
    adder = ccl_list_add;

  while (! ccl_list_is_empty (to))
    {
      ccl_vertex *vs = ccl_list_take_first (to);
      ar_ca_expr *v = ccl_vertex_get_data (vs);
      if (all || ccl_hash_find (fds->fdeps, v))	
	adder (dest, v);
    }
  ccl_list_delete (to);

  for (i = 0, p = FIRST (fds->ordering); p; p = CDR (p), i++)
    {
      ccl_hash_find (fds->order, CAR (p));
      ccl_hash_insert (fds->order, (void *) i);
    }
}

ccl_list *
fdset_get_ordered_variables (const fdset *fds, int all)
{
  ccl_list *result = ccl_list_create ();

  s_compute_ordering (fds, result, all, 1);

  return result;
}

int
fdset_compare_variables (const void *v1, const void *v2, void *data)
{
  intptr_t i1, i2;
  const fdset *fds = data;

  if (ccl_list_is_empty (fds->ordering))
    s_compute_ordering (fds, fds->ordering, 0, 0);  

  i1 = (intptr_t) ccl_hash_get_with_key (fds->order, v1);
  i2 = (intptr_t) ccl_hash_get_with_key (fds->order, v2);

  return i1 - i2;
}

void
fdset_log (ccl_log_type log, const fdset *fds)
{
  ccl_pair *p;

  if (ccl_list_is_empty (fds->ordering))
    s_compute_ordering (fds, fds->ordering, 0, 0);  

  for (p = FIRST (fds->ordering); p; p = CDR (p))
    {
      ar_ca_expr *var = CAR (p);
      funcdep *fd = ccl_hash_get_with_key (fds->fdeps, var);

      ar_ca_expr_log (log, var);
      ccl_log (log, " <-- ");
      ar_ca_expr_log (log, fd->function);
      ccl_log (log, " \n");
    }
}

void
fdset_remove_dependency_for (fdset *fds, ar_ca_expr *var)
{
  ccl_vertex *v;
  ccl_list *succ;
  ccl_vertex_iterator *vi;
  
  ccl_pre (ccl_hash_find (fds->fdeps, var));

  ccl_hash_find (fds->fdeps, var);
  ccl_hash_remove (fds->fdeps);
  v = ccl_graph_get_vertex (fds->depgraph, var);
  
  vi = ccl_vertex_get_successors (v);
  succ = ccl_list_create_from_iterator ((ccl_pointer_iterator *) vi);
  ccl_iterator_delete (vi);  
  ccl_graph_remove_vertex (fds->depgraph, v, 0);

  while (! ccl_list_is_empty (succ))
    {
      ccl_vertex *s = ccl_list_take_first (succ);
      ccl_assert (s != v);
      if (ccl_vertex_get_degree (s) == 0)
	ccl_graph_remove_vertex (fds->depgraph, s, 0);
    }
  ccl_list_delete (succ);
  ccl_list_clear (fds->ordering, NULL);
  ccl_hash_delete (fds->order);
  fds->order = ccl_hash_create (NULL, NULL, NULL, NULL);
}

static int
s_has_at_most_one_variable (ar_ca_expr *e)
{
  int result = (ar_ca_expr_get_kind (e) == AR_CA_VAR ||
		ar_ca_expr_get_kind (e) == AR_CA_CST);
  if (! result)
    {
      ccl_list *vars = ar_ca_expr_get_variables (e, NULL);
      result = ccl_list_get_size (vars) == 1;
      ccl_list_delete (vars);
    }
  
  return result;
}

fdset *
fdset_get_constants_and_variables (const fdset *fds)
{
  fdset *result = fdset_create ();
  ccl_pointer_iterator *i = ccl_hash_get_elements (fds->fdeps);

  while (ccl_iterator_has_more_elements (i))
    {
      funcdep *fd = ccl_iterator_next_element (i);
      if (s_has_at_most_one_variable (fd->function))
	fdset_add_dependency (result, fd->output, fd->function, fd->from);
    }
  ccl_iterator_delete (i);
  
  return result;
}

funcdep * 
fdset_get_funcdep (const fdset *fds, ar_ca_expr *var)
{
  funcdep *result;
  
  ccl_pre (fds != NULL);
  ccl_pre (var != NULL);

  if (ccl_hash_find (fds->fdeps, var))
    result = ccl_hash_get (fds->fdeps);
  else
    result = NULL;
  return result;
}

static int
s_and_is_sat (ar_ca_expr *e1, ar_ca_expr *e2)
{
  ar_ca_expr *c = ar_ca_expr_crt_and (e1, e2);
  int result = ar_ca_expr_is_satisfiable (c);
  ar_ca_expr_del_reference (c);
  
  return result;
}

int
fdset_implies (const fdset *fds, ar_ca_expr *e)
{
  if (ccl_hash_find (fds->from, e))
    return 1;
  else
    {
      int is_sat = 1;
      ar_ca_expr *not_e = ar_ca_expr_crt_not (e);
      ar_ca_expr *global = ar_ca_expr_add_reference (not_e);
      ccl_list *vars = ar_ca_expr_get_variables (e, NULL);
      ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);

      while (! ccl_list_is_empty (vars) && is_sat)
	{
	  ar_ca_expr *v = ccl_list_take_first (vars);
      
	  if (ccl_hash_find (done, v))
	    continue;
	  ccl_hash_insert (done, v);

	  if (fdset_has_dependency_for (fds, v))
	    {
	      funcdep *fd = fdset_get_funcdep (fds, v);
	      ar_ca_expr *func = funcdep_get_function (fd);
	      ar_ca_expr *eq = ar_ca_expr_crt_eq (v, func);	  
	  
	      if ((is_sat = s_and_is_sat (not_e, eq)))
		{
		  ar_ca_expr *tmp = ar_ca_expr_crt_and (global, eq);
		  ar_ca_expr_del_reference (global);
		  global = tmp;
		  ccl_list_append (vars, funcdep_get_inputs (fd));
		}	  
	      ar_ca_expr_del_reference (eq);
	    }
	  is_sat = is_sat && ar_ca_expr_is_satisfiable (global);
	}
      ccl_list_delete (vars);
      ccl_hash_delete (done);
      ar_ca_expr_del_reference (not_e);
      ar_ca_expr_del_reference (global);
      return ! is_sat;
    }  
}

ar_ca_expr *
funcdep_get_output (const funcdep *fd)
{
  ccl_pre (fd != NULL);

  return fd->output;
}

const ccl_list *
funcdep_get_inputs (const funcdep *fd)
{
  ccl_pre (fd != NULL);
  
  return fd->inputs;
}

ar_ca_expr *
funcdep_get_function (const funcdep *fd)
{
  ccl_pre (fd != NULL);
  
  return fd->function;
}

void
funcdep_log (ccl_log_type log, const funcdep *fd)
{
  ccl_pre (fd != NULL);
  ar_ca_expr_log (log, funcdep_get_output (fd));
  ccl_log (log, " <- ");
  ar_ca_expr_log (log, funcdep_get_function (fd));
}

static funcdep *
s_create_funcdep (ar_ca_expr *output, ar_ca_expr *function,
		  ar_ca_expr *from)
{
  funcdep *res = ccl_new (funcdep);
  res->output = ar_ca_expr_add_reference (output);
  res->function = ar_ca_expr_add_reference (function);
  res->inputs = ar_ca_expr_get_variables (function, NULL);
  res->from = from ? ar_ca_expr_add_reference (from) : NULL;
  
  return res;
}

static void
s_delete_funcdep (funcdep *fd)
{
  ccl_pre (fd != NULL);

  ccl_zdelete (ar_ca_expr_del_reference, fd->from);
  ar_ca_expr_del_reference (fd->output);
  ar_ca_expr_del_reference (fd->function);
  ccl_list_delete (fd->inputs);
  ccl_delete (fd);
}

