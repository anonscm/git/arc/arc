/*
 * ar-state-graph-semantics.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-acheck-builtins.h"
#include "ar-state-graph-semantics-p.h"

ar_sgs *
ar_sgs_create (ar_ca *ca, size_t size, const ar_sgs_methods *methods)
{
  const ar_sgs_predef_set *ps;
  ar_sgs *result = ccl_calloc (size, 1);

  ccl_pre (size >= sizeof (ar_sgs));
  ccl_pre (methods != NULL);

  result->refcount = 1;
  result->methods = methods;
  result->ca = ar_ca_add_reference (ca);
  result->state_sets = 
    ar_idtable_create (1, (ccl_duplicate_func *) ar_sgs_set_add_reference,
		       (ccl_delete_proc *) ar_sgs_set_del_reference);
  result->predefined_state_sets =  ar_idtable_create (1, NULL, NULL);
  for (ps = methods->predefined_state_sets; ps && ps->id != NULL; ps++)
    {
      if (ps->compute != NULL)
	{
	  ar_identifier *id = ar_identifier_create (ps->id);
	  ar_idtable_put (result->predefined_state_sets, id, ps->compute);
	  ar_identifier_del_reference (id);
	}
    }

  result->trans_sets = 
    ar_idtable_create (1, (ccl_duplicate_func *) ar_sgs_set_add_reference,
		       (ccl_delete_proc *) ar_sgs_set_del_reference);

  result->predefined_trans_sets = ar_idtable_create (1, NULL, NULL);
  for (ps = methods->predefined_trans_sets; ps && ps->id != NULL; ps++)
    {
      if (ps->compute != NULL)
	{
	  ar_identifier *id = ar_identifier_create (ps->id);
	  ar_idtable_put (result->predefined_trans_sets, id, ps->compute);
	  ar_identifier_del_reference (id);
	}
    }

  result->order = ar_ca_build_varorder (ca);

  return result;
}

			/* --------------- */

ar_sgs *
ar_sgs_add_reference (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  sgs->refcount++;

  return sgs;
}

			/* --------------- */

void
ar_sgs_del_reference (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);
  ccl_pre (sgs->refcount > 0);

  sgs->refcount--;
  if (sgs->refcount == 0)
    {
      ar_idtable_del_reference (sgs->state_sets);
      ar_idtable_del_reference (sgs->predefined_state_sets);
      ar_idtable_del_reference (sgs->trans_sets);
      ar_idtable_del_reference (sgs->predefined_trans_sets);
      ccl_zdelete (sgs->methods->destroy, sgs);
      varorder_del_reference (sgs->order);
      ar_ca_del_reference (sgs->ca);
      ccl_delete (sgs);
    }
}

			/* --------------- */

ar_ca *
ar_sgs_get_constraint_automaton (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  return ar_ca_add_reference (sgs->ca);
}

			/* --------------- */

const varorder *
ar_sgs_get_varorder (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  return sgs->order;
}
			/* --------------- */

ar_node *
ar_sgs_get_node (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  return ar_ca_get_node (sgs->ca);
}

			/* --------------- */

ar_ca_exprman *
ar_sgs_get_expr_manager (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  return ar_ca_get_expression_manager (sgs->ca);
}

			/* --------------- */

ar_identifier *
ar_sgs_get_name (ar_sgs *sgs)
{
  ar_ca *ca = ar_sgs_get_constraint_automaton (sgs);
  ar_node *node = ar_ca_get_node (ca);
  ar_identifier *result = ar_node_get_name (node);
  ar_node_del_reference (node);
  ar_ca_del_reference (ca);

  return result;
}

			/* --------------- */

int
ar_sgs_has_set (ar_sgs *sgs, ar_identifier *name, int *is_state)
{
  int result = 1;

  ccl_pre (sgs != NULL); 
  ccl_pre (name != NULL);

  if (ar_idtable_has (sgs->state_sets, name) || 
      ar_idtable_has (sgs->predefined_state_sets, name)) 
    *is_state = 1;
  else if (ar_idtable_has (sgs->trans_sets, name) ||
	   ar_idtable_has (sgs->predefined_trans_sets, name)) 
    *is_state = 0;
  else 
    result = 0;

  return result;
}

			/* --------------- */

int
ar_sgs_has_state_set (ar_sgs *sgs, ar_identifier *name)
{
  int is_state = 0;

  return ar_sgs_has_set (sgs, name, &is_state) && is_state;
}

			/* --------------- */

void
ar_sgs_remove_state_set (ar_sgs *sgs, ar_identifier *name)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (name != NULL);

  if (sgs->methods->pre_remove_set != NULL)
    sgs->methods->pre_remove_set (sgs, name);
  ar_idtable_remove (sgs->state_sets, name);
}

			/* --------------- */

int
ar_sgs_has_trans_set (ar_sgs *sgs, ar_identifier *name)
{
  int is_state = 0;

  return ar_sgs_has_set (sgs, name, &is_state) && !is_state;
}

			/* --------------- */

void
ar_sgs_remove_trans_set (ar_sgs *sgs, ar_identifier *name)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (name != NULL);

  ar_idtable_remove (sgs->trans_sets, name);
  if (sgs->methods->pre_remove_set != NULL)
    sgs->methods->pre_remove_set (sgs, name);
}

			/* --------------- */

void
ar_sgs_add_state_set (ar_sgs *sgs, ar_identifier *name, ar_sgs_set *set)
{
  ccl_pre (sgs != NULL); ccl_pre (set != NULL);

  ar_idtable_put (sgs->state_sets, name, set);
  if (sgs->methods->post_add_set != NULL)
    sgs->methods->post_add_set (sgs, name, set);
}

			/* --------------- */

void
ar_sgs_add_trans_set (ar_sgs *sgs, ar_identifier *name, ar_sgs_set *set)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (set != NULL);

  ar_idtable_put (sgs->trans_sets, name, set);
  if (sgs->methods->post_add_set != NULL)
    sgs->methods->post_add_set (sgs, name, set);
}

			/* --------------- */

ar_sgs_set *
ar_sgs_get_state_set (ar_sgs *sgs, ar_identifier *name)
{
  ar_sgs_set *result;

  ccl_pre (sgs != NULL); 
  ccl_pre (name != NULL);

  result = (ar_sgs_set *) ar_idtable_get (sgs->state_sets, name);
  if (result == NULL && ar_idtable_has (sgs->predefined_state_sets, name))
    {
      ar_sgs_set *(*compute) (ar_sgs *sgs, const ar_identifier *id) =
	ar_idtable_get (sgs->predefined_state_sets, name);

      if (ccl_debug_is_on)
	{
	  ccl_debug ("compute predefined property '");
	  ar_identifier_log (CCL_LOG_DEBUG, name);
	  ccl_debug ("'\n");
	}

      result = compute (sgs, name);
      ar_sgs_add_state_set (sgs, name, result);
    }

  return result;
}

			/* --------------- */

ar_sgs_set *
ar_sgs_get_trans_set (ar_sgs *sgs, ar_identifier *name)
{
  ar_sgs_set *result;

  ccl_pre (sgs != NULL); 
  ccl_pre (name != NULL);

  result = (ar_sgs_set *) ar_idtable_get (sgs->trans_sets, name);
  if (result == NULL && ar_idtable_has (sgs->predefined_trans_sets, name))
    {
      ar_sgs_set *(*compute) (ar_sgs *sgs, const ar_identifier *id);

      compute = ar_idtable_get (sgs->predefined_trans_sets, name);
      if (ccl_debug_is_on)
	{
	  ccl_debug ("compute predefined property '");
	  ar_identifier_log (CCL_LOG_DEBUG, name);
	  ccl_debug ("'\n");
	}
      result = compute (sgs, name);
      ar_sgs_add_trans_set (sgs, name, result);
    }

  return result;
}

			/* --------------- */

ar_identifier_iterator *
ar_sgs_get_state_sets (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  return ar_idtable_get_keys (sgs->state_sets);
}

			/* --------------- */

ar_identifier_iterator *
ar_sgs_get_trans_sets (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL);

  return ar_idtable_get_keys (sgs->trans_sets);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_proj (ar_sgs *sgs, ar_sgs_set *X, int on_state)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (X != NULL); 

  return sgs->methods->proj (sgs, X, on_state);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_pick (ar_sgs *sgs, ar_sgs_set *X)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (X != NULL); 

  return sgs->methods->pick (sgs, X);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_assert (ar_sgs *sgs, ar_sgs_set *S)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->assert_config (sgs, S);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_src (ar_sgs *sgs, ar_sgs_set *T)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 

  return sgs->methods->src (sgs, T);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_tgt (ar_sgs *sgs, ar_sgs_set *T)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 

  return sgs->methods->tgt (sgs, T);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_rsrc (ar_sgs *sgs, ar_sgs_set *S)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->rsrc (sgs, S);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_rtgt (ar_sgs *sgs, ar_sgs_set *S)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->rtgt (sgs, S);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_loop (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (R1 != NULL); 
  ccl_pre (R2 != NULL); 

  return sgs->methods->loop (sgs, R1, R2);
}
 
			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_trace (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, 
		      ar_sgs_set *S2)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S1 != NULL); 
  ccl_pre (S2 != NULL); 
  ccl_pre (T != NULL); 

  return sgs->methods->trace (sgs, S1, T, S2);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_unav (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->unav (sgs, T, S);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_reach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->reach (sgs, S, T);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_post (ar_sgs *sgs, ar_sgs_set *S)
{
  ar_sgs_set *result;

  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  if (sgs->methods->post == NULL)
    {
      ar_sgs_set *tmp = ar_sgs_compute_rsrc (sgs, S);
      result = ar_sgs_compute_tgt (sgs, tmp);
      ar_sgs_set_del_reference (tmp);
    }
  else
    {
      result = sgs->methods->post (sgs, S);
    }

  return result;
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_pre (ar_sgs *sgs, ar_sgs_set *S)
{
  ar_sgs_set *result;

  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  if (sgs->methods->pre == NULL)
    {
      ar_sgs_set *tmp = ar_sgs_compute_rtgt (sgs, S);
      result = ar_sgs_compute_src (sgs, tmp);
      ar_sgs_set_del_reference (tmp);
    }
  else
    {
      result = sgs->methods->pre (sgs, S);
    }

  return result;
}

			/* --------------- */


ar_sgs_set * 
ar_sgs_compute_post_star (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *I)
{
  ar_sgs_set *result = ar_sgs_compute_intersection (sgs, S, I);
  int card = ar_sgs_get_set_cardinality (sgs, result);
  int fixpoint = 0;

  while (!fixpoint)
    {
      int newcard;
      ar_sgs_set *tmp0 = ar_sgs_compute_post (sgs, result);
      ar_sgs_set *tmp1 = ar_sgs_compute_intersection (sgs, tmp0, I);
      ar_sgs_set_del_reference (tmp0);
      tmp0 = ar_sgs_compute_union (sgs, tmp1, result);
      ar_sgs_set_del_reference (result);
      ar_sgs_set_del_reference (tmp1);
      result = tmp0;

      newcard = ar_sgs_get_set_cardinality (sgs, result);
      fixpoint = (card == newcard);
      card = newcard;
    }

  return result;
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_pre_star (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *I)
{
  ar_sgs_set *result = ar_sgs_compute_intersection (sgs, S, I);
  int card = ar_sgs_get_set_cardinality (sgs, result);
  int fixpoint = 0;

  while (!fixpoint)
    {
      int newcard;
      ar_sgs_set *tmp0 = ar_sgs_compute_pre (sgs, result);
      ar_sgs_set *tmp1 = ar_sgs_compute_intersection (sgs, tmp0, I);
      ar_sgs_set_del_reference (tmp0);
      tmp0 = ar_sgs_compute_union (sgs, tmp1, result);
      ar_sgs_set_del_reference (result);
      ar_sgs_set_del_reference (tmp1);
      result = tmp0;

      newcard = ar_sgs_get_set_cardinality (sgs, result);
      fixpoint = (card == newcard);
      card = newcard;
    }

  return result;
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_coreach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->coreach (sgs, S, T);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_state_set (ar_sgs *sgs, ar_ca_expr *cond)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (cond != NULL); 

  return sgs->methods->state_set (sgs, cond);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_label (ar_sgs *sgs, ar_identifier *label)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (label != NULL); 

  return sgs->methods->label (sgs, label);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_attribute (ar_sgs *sgs, ar_identifier *attr)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (attr != NULL); 

  return sgs->methods->event_attribute (sgs, attr);
}

			/* --------------- */

ccl_list *
ar_sgs_compute_classes (ar_sgs *sgs)
{
  ccl_pre (sgs != NULL); 

  return sgs->methods->classes (sgs);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_initial (ar_sgs *sgs)
{
  ar_identifier *initid = \
    ar_identifier_create (ACHECK_BUILTIN_INITIAL_STATES_STR);
  ar_sgs_set *result = ar_sgs_get_state_set (sgs, initid);
  
  ar_identifier_del_reference (initid);

  return result;
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_lfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F)
{
  ccl_pre (sgs != NULL); 

  return sgs->methods->lfp (sgs, var, F);
}

			/* --------------- */

ar_sgs_set * 
ar_sgs_compute_gfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F)
{
  ccl_pre (sgs != NULL); 

  return sgs->methods->gfp (sgs, var, F);
}

			/* --------------- */

ar_sgs_set *
ar_sgs_compute_complement (ar_sgs *sgs, ar_sgs_set *S)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->set_complement (sgs, S);
}

			/* --------------- */

ar_sgs_set *
ar_sgs_compute_union (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S1 != NULL); 
  ccl_pre (S2 != NULL); 

  return sgs->methods->set_union (sgs, S1, S2);
}

			/* --------------- */

ar_sgs_set *
ar_sgs_compute_intersection (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S1 != NULL); 
  ccl_pre (S2 != NULL); 

  return sgs->methods->set_intersection (sgs, S1, S2);
}

			/* --------------- */

ar_sgs_set *
ar_sgs_compute_difference (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S1 != NULL); 
  ccl_pre (S2 != NULL); 

  return sgs->methods->set_difference (sgs, S1, S2);
}


			/* --------------- */

ar_sgs_set *
ar_sgs_compute_imply (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  ar_sgs_set *nS1 = ar_sgs_compute_complement (sgs, S1);
  ar_sgs_set *result = ar_sgs_compute_union (sgs, nS1, S2);
  ar_sgs_set_del_reference (nS1);

  return result;
}

			/* --------------- */

double
ar_sgs_get_set_cardinality (ar_sgs *sgs, ar_sgs_set *set)
{
  ccl_pre (sgs != NULL);
  ccl_pre (set != NULL);

  return sgs->methods->card (sgs, set);
}

			/* --------------- */

int
ar_sgs_is_set_empty (ar_sgs *sgs, ar_sgs_set *S)
{
  int result;

  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  if (sgs->methods->is_empty == NULL)
    result = ar_sgs_get_set_cardinality (sgs, S) == 0.0;
  else
    result = sgs->methods->is_empty (sgs, S);

  return result;
}

			/* --------------- */

void
ar_sgs_dot_trace (ccl_log_type log, ar_sgs *sgs, 
		  ar_sgs_set *initial, ar_sgs_set *trans, ar_sgs_set *target,
		  ccl_config_table *conf)
{  
  ar_sgs_set *T = ar_sgs_compute_trace (sgs, initial, trans, target);
  ar_sgs_set *from = ar_sgs_compute_src (sgs, T);
  ar_sgs_set *to = ar_sgs_compute_tgt (sgs, T);
  ar_sgs_set *S = ar_sgs_compute_union (sgs, from, to);
  ar_sgs_to_dot (log, sgs, S, T, conf);
  ar_sgs_set_del_reference (S);
  ar_sgs_set_del_reference (to);
  ar_sgs_set_del_reference (from);
  ar_sgs_set_del_reference (T);
}

			/* --------------- */

void
ar_sgs_to_mec4 (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
		ccl_config_table *conf)
{
  sgs->methods->to_mec4 (sgs, log, S, T, conf);
}

			/* --------------- */

void
ar_sgs_to_dot (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
	       ccl_config_table *conf)
{
  sgs->methods->to_dot (sgs, log, S, T, conf);
}

			/* --------------- */

void
ar_sgs_to_gml (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
	       ccl_config_table *conf)
{
  sgs->methods->to_gml (sgs, log, S, T, conf);
}

			/* --------------- */

void
ar_sgs_display_set (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *set,
		    ar_sgs_display_format df)
{
  sgs->methods->display_set (sgs, log, set, df);
}

			/* --------------- */

int
ar_sgs_sets_are_equal (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  return sgs->methods->sets_are_equal (sgs, S1, S2);
}

ar_sgs_set *
ar_sgs_set_create (int is_state, size_t size, const ar_sgs_set_methods *methods)
{
  ar_sgs_set *result = ccl_calloc (size, 1);

  ccl_pre (size >= sizeof (ar_sgs_set));
  ccl_pre (methods != NULL);

  result->refcount = 1;
  result->methods = methods;
  result->is_state_set = is_state;

  return result;
}

			/* --------------- */

ar_sgs_set *
ar_sgs_set_add_reference (ar_sgs_set *set)
{
  ccl_pre (set != NULL);

  set->refcount++;

  return set;
}

			/* --------------- */

void
ar_sgs_set_del_reference (ar_sgs_set *set)
{
  ccl_pre (set != NULL);
  ccl_pre (set->refcount > 0 );

  set->refcount--;
  if (set->refcount == 0)
    {
      ccl_zdelete (set->methods->destroy, set);      
      ccl_delete (set);
    }
}

			/* --------------- */

int
ar_sgs_set_is_state_set (ar_sgs_set *set)
{
  ccl_pre (set != NULL);

  return set->is_state_set;
}

			/* --------------- */

int
ar_sgs_set_is_empty (ar_sgs *sgs, ar_sgs_set *S)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 

  return sgs->methods->is_empty (sgs, S);
}

			/* --------------- */

ar_expr *
ar_sgs_set_states_to_formula (ar_sgs *sgs, ar_sgs_set *S, ccl_list *pvars,
			      ccl_list *vars, int exist)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 
  ccl_pre (S->is_state_set);
  ccl_pre (sgs->methods->states_to_formula != NULL);
  
  return sgs->methods->states_to_formula (sgs, S, pvars, vars, exist);
}


ar_ca_expr *
ar_sgs_set_states_to_ca_expr (ar_sgs *sgs, ar_sgs_set *S, ccl_hash *vars)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (S != NULL); 
  ccl_pre (S->is_state_set);
  ccl_pre (sgs->methods->states_to_ca_expr != NULL);
  
  return sgs->methods->states_to_ca_expr (sgs, S, vars);
}

			/* --------------- */

ccl_hash *
ar_sgs_set_get_events (ar_sgs *sgs, ar_sgs_set *T)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 
  ccl_pre (! T->is_state_set);
  ccl_pre (sgs->methods->get_events != NULL);

  return sgs->methods->get_events (sgs, T);
}

			/* --------------- */

ar_sgs_set *
ar_sgs_set_get_rel_for_event (ar_sgs *sgs, ar_sgs_set *T, ar_ca_event *e)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (T != NULL); 
  ccl_pre (! T->is_state_set);
  ccl_pre (sgs->methods->get_rel_for_event != NULL);

  return sgs->methods->get_rel_for_event (sgs, T, e);
}

ar_expr *
ar_sgs_get_ith_value_for_type (ar_type *type, int i, ar_ca_exprman *man)
{
  ar_expr *result = NULL;
  ar_type_kind kind = ar_type_get_kind (type);
  switch (kind)
    {
    case AR_TYPE_BOOLEANS:
      if (i == 0) 
	result = ar_expr_crt_false ();
      else 
	result = ar_expr_crt_true ();
      break;

    case AR_TYPE_RANGE:
      result = ar_expr_crt_integer (i);
      break;

    case AR_TYPE_SYMBOL_SET:
    case AR_TYPE_ENUMERATION:
      {
	ar_ca_expr *val = ar_ca_expr_get_enum_constant_by_index(man,i);
	ar_identifier *valname = ar_ca_expr_enum_constant_get_name(val);
	int ival = ((kind == AR_TYPE_ENUMERATION)
		    ? i
		    : ar_type_symbol_set_get_value (valname));
	ar_constant *cst = ar_constant_crt_symbol(ival, type);
	result = ar_expr_crt_constant(cst);
	ar_constant_del_reference(cst);
	ar_identifier_del_reference(valname);
	ar_ca_expr_del_reference(val);
      }
      break;

    default:
      ccl_throw_no_msg (internal_error);
    }

  return result;
}

			/* --------------- */

ar_sgs_set *
ar_sgs_formula_compute_assignment (ar_sgs *sgs, ar_ca_expr **assignments, 
				   int nb_assignments)
{
  ccl_pre (sgs != NULL); 
  ccl_pre (sgs->methods->assignment != NULL);
  
  return sgs->methods->assignment (sgs, assignments, nb_assignments);
}
