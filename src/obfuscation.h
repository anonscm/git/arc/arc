/*
 * obfuscation.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef OBFUSCATION_H
# define OBFUSCATION_H

# include <ccl/ccl-log.h>
# include "parser/altarica-tree.h"

typedef struct obfuscation_st obfuscation;

extern obfuscation *
obfuscation_crt_none (void);

extern obfuscation *
obfuscation_crt_sequential (void);

extern void
obfuscation_delete (obfuscation *o);

			/* --------------- */

extern ar_identifier_translator *
obfuscation_get_identifier_translator (obfuscation *o);

extern void
obfuscation_dump_translation_table (const char *filename, obfuscation *o,
				    int reverse);

#endif /* ! OBFUSCATION_H */
