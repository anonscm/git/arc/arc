/*
 * ar-backtracking-stack.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_BACKTRACKING_STACK_H
# define AR_BACKTRACKING_STACK_H

typedef struct ar_backtracking_stack_st ar_backtracking_stack;

extern ar_backtracking_stack *
ar_backtracking_stack_create (void);

extern ar_backtracking_stack *
ar_backtracking_stack_add_reference (ar_backtracking_stack *bstack);

extern void
ar_backtracking_stack_del_reference (ar_backtracking_stack *bstack);

extern void
ar_backtracking_stack_save (ar_backtracking_stack *bstack, int *pdata);

extern void
ar_backtracking_stack_restore (ar_backtracking_stack *bstack, 
			       void *choice_point);

extern void
ar_backtracking_stack_restore_last (ar_backtracking_stack *bstack);

extern void *
ar_backtracking_stack_choice_point (ar_backtracking_stack *bstack);

extern int
ar_backtracking_stack_is_empty (ar_backtracking_stack *bstack);

#endif /* ! AR_BACKTRACKING_STACK_H */
