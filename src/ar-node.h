/*
 * ar-node.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_NODE_H
# define AR_NODE_H

# include "parser/altarica-tree.h"
# include "ar-identifier.h"
# include "ar-event-poset.h"
# include "ar-broadcast.h"
# include "ar-trans.h"
# include <sas/sas-params.h>
# include <sas/sas-laws.h>

typedef struct ar_node_st ar_node;

extern int 
ar_node_init (void);

extern void
ar_node_terminate (void);

extern ar_node *
ar_node_create(ar_context *ctx);

extern ar_node *
ar_node_create_parametrized(altarica_tree *tmpl, ar_context *ctx);

extern ar_node *
ar_node_create_reduced(ar_node *node);

extern int 
ar_node_get_uid(ar_node *node);

extern ar_identifier *
ar_node_get_name(ar_node *node);

extern void
ar_node_set_name(ar_node *node, ar_identifier *id);

extern ar_node *
ar_node_add_reference(ar_node *node);
extern void
ar_node_del_reference(ar_node *node);

extern ar_context *
ar_node_get_context (const ar_node *node);

extern ar_context *
ar_node_get_parent_context(ar_node *node);

extern ar_type *
ar_node_get_configuration_type(ar_node *node);

extern ar_type *
ar_node_get_configuration_base_type(ar_node *node);

extern ar_type *
ar_node_get_event_type(ar_node *node);

extern void
ar_node_log(ccl_log_type log, ar_node *node, int with_epsilon);

extern altarica_tree *
ar_node_get_template(ar_node *node);

/*                                                                parameters */
extern void
ar_node_add_local_parameter(ar_node *node, ar_identifier *pname, 
			    ar_type *type, ar_expr *value);

/*                                                             param_setting */
extern void
ar_node_set_subnode_parameter(ar_node *node, ar_identifier *subnode,
			      ar_identifier *pname, ar_expr *value);
extern int
ar_node_has_subnode_parameter_setting(ar_node *node, ar_identifier *subnode,
				      ar_identifier *pname);
extern ar_idtable *
ar_node_get_subnode_parameter_setting(ar_node *node, ar_identifier *subnode);

extern ar_idtable *
ar_node_get_subnode_parameter_settings(ar_node *node);

extern ar_context_slot_iterator *
ar_node_get_slots_for_parameters (const ar_node *node);

extern ar_identifier_iterator *
ar_node_get_names_of_parameters (ar_node *node);

extern ar_type *
ar_node_get_type_of_parameter (ar_node *node, ar_identifier *id);

extern ar_expr *
ar_node_get_value_of_parameter (ar_node *node, ar_identifier *id);

/*                                                                attributes */
extern void
ar_node_add_attribute(ar_node *node, ar_identifier *id);

extern int
ar_node_has_attribute(ar_node *node, ar_identifier *id);

extern ccl_list *
ar_node_get_attributes (const ar_node *node);

/*                                                                 variables */
extern ar_identifier *AR_IDLE_VAR;

extern void
ar_node_add_variable(ar_node *node, ar_identifier *id, ar_type *type, 
		     uint32_t flags, ccl_list *attr);

extern ar_context_slot_iterator *
ar_node_get_slots_for_variables (const ar_node *node);

extern int
ar_node_get_nb_variables (const ar_node *node);

extern ar_context_slot *
ar_node_get_variable_slot (ar_node *node, ar_identifier *id);

extern ar_expr *
ar_node_get_variable (ar_node *node, ar_identifier *id);

extern int
ar_node_has_flow_variable(ar_node *node, ar_identifier *id);

extern int
ar_node_has_state_variable(ar_node *node, ar_identifier *id);

extern ar_identifier_iterator *
ar_node_get_names_of_variables (const ar_node *node);

extern uint32_t 
ar_node_get_flags_of_variable (ar_node *node, ar_identifier *id);

extern ar_type *
ar_node_get_type_of_variable (ar_node *node, ar_identifier *id);

extern ccl_list * 
ar_node_get_attributes_of_variable (ar_node *node, ar_identifier *id);

/*                                                                  subnodes */
extern void
ar_node_add_subnode(ar_node *node, ar_identifier *n, ar_node *model,
		    ar_type *type);
extern int
ar_node_has_subnode (const ar_node *node, const ar_identifier *n);

extern ar_node *
ar_node_get_subnode_model (const ar_node *node, const ar_identifier *n);

extern int
ar_node_get_nb_subnodes (const ar_node *node);

# define ar_node_is_leaf(_n) (ar_node_get_nb_subnodes(_n)==0)

extern int
ar_node_get_nb_subnode_fields(ar_node *node);

extern ar_context_slot_iterator *
ar_node_get_slots_for_subnode_fields(ar_node *node);

extern ar_context_slot_iterator *
ar_node_get_slots_for_subnodes (const ar_node *node);

extern ar_identifier_iterator *
ar_node_get_names_of_subnodes (const ar_node *node);

extern void
ar_node_display_subnode_type (ccl_log_type log, ar_type *type);

/*!
 * return the list of subnodes identifier paths starting from the empty prefix.
 */
extern ccl_list *
ar_node_get_hierarchy_identifiers (ar_node *n);

/*                                                                    events */
extern void
ar_node_add_event(ar_node *node, ar_event *ev);
extern int
ar_node_has_event(ar_node *node, ar_identifier *ev);
extern ar_event *
ar_node_get_event(ar_node *node, ar_identifier *ev);
extern ar_event *
ar_node_get_event_from_event(ar_node *node, ar_event *ev);
extern int
ar_node_get_number_of_events(ar_node *node);
extern ar_event_poset *
ar_node_get_event_poset(ar_node *node);
extern ar_event_iterator *
ar_node_get_events(ar_node *node);
extern ar_identifier_iterator *
ar_node_get_event_identifiers(ar_node *node);
extern ccl_list *
ar_node_get_events_for_identifier(ar_node *node, ar_identifier *ev);

/*                                                               transitions */
extern void
ar_node_add_transition(ar_node *node, ar_trans *t);

extern ccl_list *
ar_node_get_transitions(ar_node *node);

extern int 
ar_node_get_number_of_transitions (ar_node *node);

extern void
ar_node_remove_transition(ar_node *node, ar_trans *t);

extern int
ar_node_has_non_trivial_epsilon (ar_node *node);

/*                                                                assertions */
extern void
ar_node_add_assertion(ar_node *node, ar_expr *a);

extern ccl_list *
ar_node_get_assertions(ar_node *node);

/*                                                                      init */
extern void
ar_node_add_initial_state(ar_node *node, ar_context_slot *slot, 
			  ar_expr *value);
extern ccl_list *
ar_node_get_initialized_slots (ar_node *node);

extern const ar_expr *
ar_node_get_initial_value_cst (ar_node *node, ar_context_slot *sl);

extern ar_expr *
ar_node_get_initial_value (ar_node *node, ar_context_slot *sl);

extern void
ar_node_add_initial_constraint (ar_node *node, ar_expr *value);

extern ccl_list *
ar_node_get_initial_constraints (ar_node *node);

/*                                                                   synchro */
extern void
ar_node_add_broadcast(ar_node *node, ar_broadcast *v);
extern ccl_list *
ar_node_get_broadcasts(ar_node *node);

			/* --------------- */

extern ar_node *
ar_node_remove_composite_types(ar_node *node);

			/* --------------- */

extern void
ar_node_dump_node(ccl_log_type log, ar_node *node, int with_epsilon);

/*                                           probability laws and parameters */
extern int
ar_node_has_law_parameter (ar_node *node, ar_identifier *id);

extern sas_law_param *
ar_node_get_law_parameter (ar_node *node, ar_identifier *id);

extern void
ar_node_add_law_parameter (ar_node *node, ar_identifier *paramid,
			   sas_law_param *param);

extern const ccl_list *
ar_node_get_ids_of_law_parameters (ar_node *node);

extern ccl_list *
ar_node_get_ordered_ids_of_law_parameters (ar_node *node);

extern int
ar_node_has_law (ar_node *node, ar_identifier *eventid);

extern void
ar_node_add_law (ar_node *node, ar_identifier *eventid, sas_law *law);

extern const ccl_list *
ar_node_get_ids_of_laws (ar_node *node);

extern sas_law *
ar_node_get_law (ar_node *node, ar_identifier *id);

extern void
ar_node_add_observer (ar_node *node, ar_identifier *id, ar_expr *e);

extern ar_expr * 
ar_node_get_observer (ar_node *node, ar_identifier *id);

extern const ccl_list *
ar_node_get_ids_of_observers (ar_node *node);

extern void
ar_node_add_preemptible (ar_node *node, ar_identifier *eventid);

extern ccl_pointer_iterator *
ar_node_get_preemptibles (ar_node *node);

extern void
ar_node_add_bucket (ar_node *node, ar_identifier *id, ccl_list *events,
		    ccl_list *probas);

extern int 
ar_node_is_in_bucket (ar_node *node, ar_identifier *id);

extern void
ar_node_map_buckets (ar_node *node,
		     void (*map)(ar_identifier *id, ccl_list *events,
				 ccl_list *probas,
				 void *data),
		     void *clientdata);

extern void
ar_node_add_priority (ar_node *node, ar_identifier *eventid, int pl);

extern int
ar_node_get_priority (ar_node *node, ar_identifier *eventid);

extern ar_identifier_iterator *
ar_node_get_events_with_priorities (ar_node *node);

#endif /* ! AR_NODE_H */
