/*
 * ar-state-graph-semantics-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_STATE_GRAPH_SEMANTICS_P_H
# define AR_STATE_GRAPH_SEMANTICS_P_H

# include "ar-state-graph-semantics.h"

struct ar_state_graph_semantics_st 
{
  int refcount;
  const ar_sgs_methods *methods;
  ar_ca *ca;
  varorder *order;
  ar_idtable *state_sets;
  ar_idtable *predefined_state_sets;  
  ar_idtable *trans_sets;  
  ar_idtable *predefined_trans_sets;  
};

typedef struct ar_state_graph_semantics_methods_predef_set_st 
{
  const char *id;
  ar_sgs_set *(*compute) (ar_sgs *sgs, const ar_identifier *id);
} ar_sgs_predef_set;

struct ar_state_graph_semantics_methods_st 
{
  const ar_sgs_predef_set *predefined_state_sets;
  const ar_sgs_predef_set *predefined_trans_sets;

  int (*sets_are_equal) (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
  ccl_hash *(*get_events) (ar_sgs *sgs, ar_sgs_set *T);
  ar_sgs_set * (*get_rel_for_event) (ar_sgs *sgs, ar_sgs_set *T, 
				     ar_ca_event *e);
  ar_expr *(*states_to_formula) (ar_sgs *sgs, ar_sgs_set *S, ccl_list *pvars, 
				 ccl_list *vars, int exist);

  ar_ca_expr *(*states_to_ca_expr) (ar_sgs *sgs, ar_sgs_set *S, ccl_hash *vars);

  void (*post_add_set) (ar_sgs *sgs, ar_identifier *id, ar_sgs_set *set);
  void (*pre_remove_set) (ar_sgs *sgs, ar_identifier *id);

  void (*display_set) (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *set,
		       ar_sgs_display_format df);
  void (*destroy) (ar_sgs *sgs);
  double (*card) (ar_sgs *sgs, ar_sgs_set *S);
  void (*to_mec4) (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
		   ccl_config_table *conf);
  void (*to_dot) (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
		  ccl_config_table *conf);
  void (*to_gml) (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
		  ccl_config_table *conf);
  ar_sgs_set * (*assignment) (ar_sgs *sgs, ar_ca_expr **assignments, 
			      int nb_assignments);
  int  (*is_empty) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*post) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*pre) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*proj) (ar_sgs *sgs, ar_sgs_set *X, int on_state);
  ar_sgs_set * (*pick) (ar_sgs *sgs, ar_sgs_set *X);
  ar_sgs_set * (*assert_config) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*src) (ar_sgs *sgs, ar_sgs_set *T);
  ar_sgs_set * (*tgt) (ar_sgs *sgs, ar_sgs_set *T);
  ar_sgs_set * (*rsrc) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*rtgt) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*loop) (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2); 
  ar_sgs_set * (*trace) (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, 
			 ar_sgs_set *S2);
  ar_sgs_set * (*unav) (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S);
  ar_sgs_set * (*reach) (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);
  ar_sgs_set * (*coreach) (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);
  ar_sgs_set * (*state_set) (ar_sgs *sgs, ar_ca_expr *cond);
  ar_sgs_set * (*label) (ar_sgs *sgs, ar_identifier *label);
  ar_sgs_set * (*event_attribute) (ar_sgs *sgs, ar_identifier *label);
  ccl_list * (*classes) (ar_sgs *sgs);
  ar_sgs_set * (*lfp) (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);
  ar_sgs_set * (*gfp) (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);
  ar_sgs_set * (*set_complement) (ar_sgs *sgs, ar_sgs_set *S);
  ar_sgs_set * (*set_union) (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
  ar_sgs_set * (*set_intersection) (ar_sgs *sgs, ar_sgs_set *S1, 
				    ar_sgs_set *S2);
  ar_sgs_set * (*set_difference) (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
};

			/* --------------- */

#define AR_SGS_SET_IS_STATE 1
#define AR_SGS_SET_IS_TRANS 0

struct ar_state_graph_set_st 
{
  int refcount;
  const ar_sgs_set_methods *methods;
  int is_state_set;
};

			/* --------------- */

struct ar_state_graph_set_methods_st
{
  void (*destroy) (ar_sgs_set *set);
};

			/* --------------- */

typedef enum {
  AR_SGS_F_CST, AR_SGS_F_VAR, AR_SGS_F_SRC, AR_SGS_F_TGT,
  AR_SGS_F_RSRC, AR_SGS_F_RTGT, AR_SGS_F_OR, AR_SGS_F_AND, AR_SGS_F_NOT,
  AR_SGS_F_ASSERT
} ar_sgs_formula_type;

struct ar_state_graph_formula_st
{
  int refcount;
  int index;
  ar_sgs_formula_type type;
  ar_sgs_set *set;
  int is_state;
  ar_identifier *ident;
  ar_sgs_formula *args[2];
};

			/* --------------- */

extern ar_sgs *
ar_sgs_create (ar_ca *ca, size_t size, const ar_sgs_methods *methods);

extern ar_sgs_set *
ar_sgs_set_create (int is_state, size_t size, 
		   const ar_sgs_set_methods *methods);

extern void
ar_sgs_formula_clean_sets (ar_sgs_formula *F);

extern void
ar_sgs_formula_clean_indexes (ar_sgs_formula *F);

extern void
ar_sgs_formula_set_indexes (ar_sgs_formula *F, int *p_last_index);

extern ar_expr *
ar_sgs_get_ith_value_for_type (ar_type *type, int i, ar_ca_exprman *man);

#endif /* ! AR_STATE_GRAPH_SEMANTICS_P_H */
