/*
 * ar-ca-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_P_H
# define AR_CA_P_H

# include <deps/fdset.h>
# include <ccl/ccl-set.h>
# include "ar-constraint-automaton.h"

struct ar_ca_st {
  int refcount;
  ar_node *node;
  ccl_list *hierarchy;
  ar_ca_exprman *man;
  ccl_list *flow_variables;
  ccl_list *state_variables;
  
  CCL_ARRAY(ar_ca_event *) events;
  ccl_list *assertions;
  CCL_ARRAY(ar_ca_trans *) transitions;
  ccl_list *initial_assignments;
  ccl_list *initial_constraints;
  ar_ca *reduced;
  fdset *fds;
  ar_ca_event *epsilon;
  ccl_list *law_params;
  ar_idtable *laws;
  ar_idtable *observers;
  ccl_list *obsids;
  ccl_set *preemptibles;
  
  ar_idtable *bucket_event_probas;
  ar_idtable *event_to_bucket_table;
  ccl_list *buckets;
  
  ar_idtable *priorities;
};

#define CA_DBG_IS_ON (ccl_debug_is_on && ar_ca_debug_is_on)
#define CA_DBG(f_) (CA_DBG_IS_ON && ar_ca_debug_ ## f_)
#define CA_DBG_START_TIMER(args_) \
  CCL_DEBUG_COND_START_TIMED_BLOCK (CA_DBG_IS_ON, args_)
#define CA_DBG_END_TIMER() \
  CCL_DEBUG_COND_END_TIMED_BLOCK (CA_DBG_IS_ON)
#define CA_DBG_F_START_TIMER(f_, args_)			\
  CCL_DEBUG_COND_START_TIMED_BLOCK (CA_DBG (f_), args_)
#define CA_DBG_F_END_TIMER(f_) \
  CCL_DEBUG_COND_END_TIMED_BLOCK (CA_DBG (f_))

extern ar_ca *
ar_ca_allocate_ca (ar_node *node, ar_ca_exprman *man);
  
#endif /* ! AR_CA_P_H */
