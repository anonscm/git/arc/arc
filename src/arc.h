/*
 * arc.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_H
# define ARC_H

# include <ccl/ccl-common.h>
# include <ccl/ccl-config-table.h>

BEGIN_C_DECLS

# define ENV_ARCRC "ARCRC"
# define ENV_ARCPATH "ARCPATH"
# define ENV_ARCPATH_SEPCHAR ':'

# define ARCRC_FILE  "~/.arcrc"
# define ARC_DEFAULT_PREFERENCES "default-preferences"

extern ccl_config_table *ARC_PREFERENCES;

extern void
arc_init (void);

extern void
arc_terminate (void);

extern void
arc_load_preferences (void);

extern void
arc_save_preferences (void);

extern void
arc_set_logfile (FILE *file);

END_C_DECLS

#endif /* ! ARC_H */
