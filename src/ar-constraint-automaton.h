/*
 * ar-constraint-automaton.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CONSTRAINT_AUTOMATON_H
# define AR_CONSTRAINT_AUTOMATON_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-graph.h>
# include <ccl/ccl-iterator.h>
# include <deps/fdset.h>

# include "ar-node.h"
# include "varorder.h"
# include "ar-ca-expression.h"

typedef struct ar_ca_st ar_ca;

typedef struct ar_ca_event_st ar_ca_event;
typedef struct ar_ca_trans_st ar_ca_trans;
  
			/* --------------- */

typedef struct ar_ca_depgraph_st ar_ca_depgraph;

			/* --------------- */

extern int ar_ca_debug_is_on;
extern int ar_ca_debug_ordering;

			/* --------------- */

extern ar_ca *
ar_ca_create(ar_node *n);
extern ar_ca *
ar_ca_add_reference(ar_ca *ca);
extern void
ar_ca_del_reference(ar_ca *ca);

extern void
ar_ca_log_info (ccl_log_type log, const ar_ca *ca, int comments);
extern void
ar_ca_log (ccl_log_type log, const ar_ca *ca);
extern void
ar_ca_log_gen (ccl_log_type log, const ar_ca *ca, int show_epsilon, 
	       const char *separator, const char *quotes);

extern ar_node *
ar_ca_get_node (const ar_ca *ca);
extern const ccl_list *
ar_ca_get_hierarchy_identifiers(const ar_ca *ca);
extern ar_node *
ar_ca_get_hierarchical_node (const ar_ca *ca);
extern ar_ca_exprman *
ar_ca_get_expression_manager(ar_ca *ca);
extern int
ar_ca_get_max_cardinality (const ar_ca *ca);

extern int
ar_ca_get_number_of_variables(const ar_ca *ca);
extern int
ar_ca_get_number_of_flow_variables(const ar_ca *ca);
extern void
ar_ca_add_flow_variable(ar_ca *ca, ar_ca_expr *var);
extern const ccl_list * 
ar_ca_get_flow_variables (const ar_ca *ca);
extern ccl_pointer_iterator *
ar_ca_get_flow_variables_iterator (ar_ca *ca);

extern int
ar_ca_get_number_of_state_variables (const ar_ca *ca);
extern void
ar_ca_add_state_variable (ar_ca *ca, ar_ca_expr *var);
extern const ccl_list *
ar_ca_get_state_variables (const ar_ca *ca);
extern ccl_pointer_iterator *
ar_ca_get_state_variables_iterator (ar_ca *ca);

/*!
 * Return variables of ca in an associative array. Reference counters are not
 * increased. Keys of the table are variables and associated value is a Boolean
 * that indicates if the variable is a state variable or not.
 */
# define AR_CA_STATE_VARS 0x1
# define AR_CA_FLOW_VARS 0x2
# define AR_CA_ALL_VARS (AR_CA_STATE_VARS|AR_CA_FLOW_VARS)

extern ccl_hash *
ar_ca_get_variables (const ar_ca *ca, int which);

extern void
ar_ca_add_event(ar_ca *ca, ar_ca_event *ev);
extern int
ar_ca_get_number_of_events(const ar_ca *ca);
extern ar_ca_event **
ar_ca_get_events(ar_ca *ca);
extern ccl_pointer_iterator *
ar_ca_get_events_iterator (ar_ca *ca);
extern int 
ar_ca_has_subevent(ar_ca *ca, ar_identifier *id);
extern ar_ca_event *
ar_ca_get_epsilon_event (ar_ca *ca);
extern ar_ca_trans *
ar_ca_get_epsilon_transition (ar_ca *ca);

extern void
ar_ca_add_initial_assignment(ar_ca *ca, ar_ca_expr *var, ar_ca_expr *value);
extern int
ar_ca_get_number_of_initial_assignments (const ar_ca *ca);
extern const ccl_list *
ar_ca_get_initial_assignments (const ar_ca *ca);

extern void
ar_ca_add_initial_constraint (ar_ca *ca, ar_ca_expr *ic);
extern int
ar_ca_get_number_of_initial_constraints (const ar_ca *ca);
extern const ccl_list *
ar_ca_get_initial_constraints (const ar_ca *ca);

extern void
ar_ca_add_assertion(ar_ca *ca, ar_ca_expr *constraint);
extern int
ar_ca_get_number_of_assertions(const ar_ca *ca);
extern const ccl_list * 
ar_ca_get_assertions (const ar_ca *ca);


extern void
ar_ca_add_transition(ar_ca *ca, ar_ca_trans *t);
extern int
ar_ca_get_number_of_transitions(const ar_ca *ca);
extern ar_ca_trans **
ar_ca_get_transitions (const ar_ca *ca);
extern ccl_pointer_iterator *
ar_ca_get_transitions_iterator (ar_ca *ca);

			/* --------------- */


extern ar_ca_event *
ar_ca_event_create(const ccl_list *hierarchy, ccl_list *ids);
extern ar_ca_event *
ar_ca_event_add_reference(ar_ca_event *ev);
extern void
ar_ca_event_del_reference(ar_ca_event *ev);
extern void
ar_ca_event_log (ccl_log_type log, ar_ca_event *ev);
extern void
ar_ca_event_log_gen (ccl_log_type log, ar_ca_event *ev, int show_epsilon, 
		 const char *separator, const char *quotes);
extern int
ar_ca_event_contains(ar_ca_event *ev, ar_identifier *id);
extern int
ar_ca_event_equals(ar_ca_event *ev, ar_ca_event *other);
extern int
ar_ca_event_is_epsilon(ar_ca_event *ev);
extern ar_identifier_iterator *
ar_ca_event_get_ids (const ar_ca_event *ev);

/*!
 * IDs are not ordered according to hierarchy.
 */
extern ar_identifier_iterator *
ar_ca_event_get_non_epsilon_ids (const ar_ca_event *ev);
extern void
ar_ca_event_add_attribute (ar_ca_event *ev, ar_identifier *id);
extern int
ar_ca_event_has_attribute (ar_ca_event *ev, ar_identifier *id);
extern ccl_list *
ar_ca_event_get_attributes (ar_ca_event *ev);

			/* --------------- */

extern ar_ca_trans *
ar_ca_trans_create(ar_ca_expr *guard, ar_ca_event *ev);
extern ar_ca_trans *
ar_ca_trans_add_reference(ar_ca_trans *t);
extern void
ar_ca_trans_del_reference(ar_ca_trans *t);
extern ar_ca_expr *
ar_ca_trans_get_guard (const ar_ca_trans *t);
extern ar_ca_expr *
ar_ca_trans_get_guard_with_domains (const ar_ca_trans *t);
extern void
ar_ca_trans_set_guard(ar_ca_trans *t, ar_ca_expr *G);
extern int 
ar_ca_trans_evaluate_guard(ar_ca_trans *t);
extern ar_ca_event *
ar_ca_trans_get_event(const ar_ca_trans *t);
extern const ar_ca_event *
ar_ca_trans_get_const_event(const ar_ca_trans *t);
extern void
ar_ca_trans_add_assignment(ar_ca_trans *t, ar_ca_expr *var, ar_ca_expr *val);
extern int
ar_ca_trans_get_number_of_assignments (const ar_ca_trans *t);
extern ar_ca_expr **
ar_ca_trans_get_assignments (const ar_ca_trans *t);
extern void
ar_ca_trans_log (ccl_log_type log, ar_ca_trans *t);
extern void
ar_ca_trans_log_gen (ccl_log_type log, ar_ca_trans *t, int show_epsilon, 
		     const char *separator, const char *quotes);

# define AR_CA_R_VARS  0x1
# define AR_CA_W_VARS  0x2
# define AR_CA_RW_VARS (AR_CA_R_VARS|AR_CA_W_VARS)
extern ccl_list * 
ar_ca_trans_get_variables (ar_ca_trans *t, ccl_list *result, int flags);

			/* --------------- */

extern void
ar_ca_add_law_parameter (ar_ca *ca, sas_law_param *p);

extern void
ar_ca_add_law (ar_ca *ca, ar_identifier *id, sas_law *law);

extern int
ar_ca_has_law (ar_ca *ca, ar_identifier *id);

extern sas_law *
ar_ca_get_law (ar_ca *ca, ar_identifier *id);

extern int 
ar_ca_get_number_of_laws (ar_ca *ca);

extern void
ar_ca_add_observer (ar_ca *ca, ar_identifier *id, ar_ca_expr *obs);

extern ar_ca_expr *
ar_ca_get_observer (ar_ca *ca, ar_identifier *id);

extern ar_identifier_iterator *
ar_ca_get_observer_ids (ar_ca *ca);

extern int 
ar_ca_get_number_of_observers (ar_ca *ca);

extern void 
ar_ca_add_preemptible (ar_ca *ca, ar_identifier *id);

extern ccl_pointer_iterator *
ar_ca_get_preemptibles (ar_ca *ca);

extern int 
ar_ca_is_preemptible (ar_ca *ca, ar_identifier *id);

extern void
ar_ca_add_bucket (ar_ca *ca, ar_identifier *id, ccl_list *events,
		  ccl_list *probas);

extern const ccl_list *
ar_ca_get_buckets (ar_ca *ca);

extern sas_law_param *
ar_ca_get_bucket_proba (ar_ca *ca, const ar_identifier *id);

extern ccl_list *
ar_ca_get_bucket (ar_ca *ca, const ar_identifier *id);

extern void
ar_ca_add_priority (ar_ca *ca, ar_identifier *id, int pl);

extern int
ar_ca_has_priority (ar_ca *ca, ar_identifier *id);

extern int
ar_ca_get_priority (ar_ca *ca, ar_identifier *id);

			/* --------------- */

extern const ar_ca *
ar_ca_get_reduced (ar_ca *ca);

extern ar_ca *
ar_ca_clone (ar_ca *ca);

			/* --------------- */

extern varorder * 
ar_ca_build_varorder (ar_ca *ca);

extern void
ar_ca_order_variables (ar_ca *ca, varorder *order);

extern const fdset *
ar_ca_get_fdset (ar_ca *ca);

extern int 
ar_ca_check (const ar_ca *ca);

#endif /* ! AR_CONSTRAINT_AUTOMATON_H */
