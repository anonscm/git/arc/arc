/*
 * ar-translators.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-translators.h"

CCL_DEFINE_EXCEPTION(translation_exception,exception);

			/* --------------- */

void
ar_translators_init(ccl_config_table *conf)
{
  ar_ts_translators_init(conf);
  ar_lustre_translators_init(conf);
}

			/* --------------- */

void
ar_translators_terminate(void)
{
  ar_lustre_translators_terminate();
  ar_ts_translators_terminate();
}
