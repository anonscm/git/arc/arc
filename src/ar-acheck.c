/*
 * ar-acheck.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-acheck.h"

# define AR_ACHECK_BUILTIN(_enumid, _strid) 	\
  const char *ACHECK_BUILTIN_ ## _enumid ## _STR = _strid;
# include "ar-acheck-builtins.def"
# undef AR_ACHECK_BUILTIN

const char *AR_ACHECK_BUILTIN_NAMES[] = {
#define AR_ACHECK_BUILTIN(_enumid, _strid) _strid,
#include "ar-acheck-builtins.def"
#undef AR_ACHECK_BUILTIN
  NULL
};

			/* --------------- */

const char *AR_ACHECK_PREF_NAMES[] = {
#define AR_ACHECK_PREF(_enumid, _strid) "acheck." _strid,
#include "ar-acheck-preferences.def"
#undef AR_ACHECK_PREF
};

# define AR_ACHECK_PREF(_enumid, _strid) 	\
  const char *ACHECK_PREF_ ## _enumid ## _STR = "acheck." _strid;
# include "ar-acheck-preferences.def"
# undef AR_ACHECK_PREF

			/* --------------- */

int
ar_acheck_init (ccl_config_table *conf)
{
  return 1;
}

			/* --------------- */

void
ar_acheck_terminate (void)
{
}

