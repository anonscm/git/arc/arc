/*
 * ar-help.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include "ar-help.h"

#define READ_BUFFER_SIZE 100

#ifndef ARC_DATADIR
# define ARC_DATADIR getenv ("ARC_DATADIR")
#endif /* ARC_DATADIR */

#ifndef ARC_MANPAGES_INDEX
# define  ARC_MANPAGES_INDEX "pages.idx"
#endif /* ARC_MANPAGES_INDEX */

typedef struct help_st {
  char *manpage;
  char *shortdesc;
  char *description;  
} HELP;

static ccl_hash *MAN_PAGES = NULL;
static int PAGES_ARE_LOADED = 0;

			/* --------------- */

static int
s_read_manpages (const char *dir);

static void
s_register_page (const char *manpage, const char *shortdesc, 
		 const char *description);

static void
s_delete_page (HELP *h);

static void
s_load_pages (void);

			/* --------------- */

int
ar_help_init(void)
{
  int result;

  MAN_PAGES = ccl_hash_create (ccl_string_hash,
			       ccl_string_compare,
			       NULL,
			       (ccl_delete_proc *) s_delete_page);
  PAGES_ARE_LOADED = 0;
  result = (MAN_PAGES != NULL);

  return result;
}

			/* --------------- */

void
ar_help_terminate(void)
{
  ccl_hash_delete (MAN_PAGES);
  MAN_PAGES = NULL;
}


			/* --------------- */

const ar_help *
ar_help_manpage (const char *mp)
{
  ar_help *result = NULL;

  ccl_pre (MAN_PAGES != NULL);

  if (! PAGES_ARE_LOADED)
    s_load_pages ();
  if (ccl_hash_find (MAN_PAGES, (char *) mp))
    result = ccl_hash_get (MAN_PAGES);

  return result;
}

			/* --------------- */

ccl_list *
ar_help_apropos (const char *kw)
{
  ccl_pointer_iterator *manpages;
  ccl_list *result;

  ccl_pre (MAN_PAGES != NULL);

  if (! PAGES_ARE_LOADED)
    s_load_pages ();
  
  result = ccl_list_create ();
  manpages = ccl_hash_get_elements (MAN_PAGES);
  while (ccl_iterator_has_more_elements (manpages))
    {
      HELP *help = ccl_iterator_next_element (manpages);
      if (strstr (help->manpage, kw) != NULL ||
	  strstr (help->shortdesc, kw) != NULL ||
	  strstr (help->description, kw) != NULL)
	ccl_list_add (result, help);
    }
  ccl_iterator_delete (manpages);

  return result;
}

			/* --------------- */

void
ar_help_log_manpage (ccl_log_type log, const char *mp)
{
  const ar_help *hlp = ar_help_manpage (mp);

  if (hlp == NULL)
    ccl_warning ("warning: can't find '%s' manual page.\n", mp);
  else
    ccl_log (log, "%s", hlp->description);
}

			/* --------------- */

static char *
s_read_manpage (const char *filename)
{
  char *tmp;
  char *result = NULL;
  FILE *page = fopen (filename, "r");

  if (page == NULL)
    {
      ccl_warning ("warning: can not open file '%s'.\n", filename);
      return NULL;
    }
  
  tmp = ccl_new_array (char, READ_BUFFER_SIZE);

  while (fgets (tmp, READ_BUFFER_SIZE, page) != NULL)
    ccl_string_format_append (&result, "%s", tmp);

  ccl_delete (tmp);
  fclose (page);

  if (result == NULL)
    ccl_warning ("warning: error while reading '%s'.\n", filename);

  return result;
}

			/* --------------- */

static char *
s_readline (FILE *f)
{
  char *tmp = ccl_new_array (char, READ_BUFFER_SIZE);
  char *result = NULL;

  while (fgets (tmp, READ_BUFFER_SIZE, f) != NULL)
    {
      char *newline = strrchr (tmp, '\n');
      if (newline != NULL)
	*newline = '\0';
      ccl_string_format_append (&result, "%s", tmp);
      if (newline)
	break;
    }
  ccl_delete (tmp);

  return result;
}

			/* --------------- */

static int
s_read_manpages (const char *dirname)  
{
  int result;
  char *indexfilename = 
    ccl_string_format_new ("%s/%s", dirname, ARC_MANPAGES_INDEX);
					       
  FILE *index = fopen (indexfilename, "r");

  if (index == NULL)
    {
      result = 0;
    }
  else
    {
      char *line;
      result = 1;

      while (result && (line = s_readline (index)) != NULL)
	{
	  char *doc;
	  char *shortdesc;
	  char *filename = strchr (line, ' ');

	  if (filename == NULL || 
	      (shortdesc = strchr (filename + 1, ' ')) == NULL)
	    {
	      ccl_error ("warning: malformed manpage index file.\n");
	      ccl_string_delete (line);
	      
	      result = 0;
	    }
	  else
	    {
	      char *manpagefilename;

	      *filename = '\0';
	      filename++;
	      *shortdesc = '\0';
	      shortdesc++;

	      manpagefilename = 
		ccl_string_format_new ("%s/%s", dirname, filename);

	      doc = s_read_manpage (manpagefilename);
	      ccl_string_delete (manpagefilename);
	      
	      if (doc == NULL)
		result = 0;
	      else
		{
		  s_register_page (line, shortdesc, doc);
		  ccl_string_delete (doc);
		}
	      ccl_string_delete (line);
	    }
	}
      fclose (index);
    }
  ccl_string_delete (indexfilename);

  return result;
}

			/* --------------- */

static void
s_register_page (const char *manpage, const char *shortdesc, 
		 const char *description)
{
  HELP *page = ccl_new (HELP);

  page->manpage = ccl_string_dup (manpage);

  if (ccl_hash_find (MAN_PAGES, page->manpage))
    {
      ccl_warning ("warning: there exists two manpages with the same id "
		   "'%s'.\n", manpage);
      ccl_warning ("warning: this problem is non-critical but only one page "
		   "will be available.\n");
      ccl_warning ("warning: please contact maintainers at: %s\n",
		 PACKAGE_BUGREPORT);
      ccl_hash_remove (MAN_PAGES);
    }

  page->shortdesc = ccl_string_dup (shortdesc);
  page->description = ccl_string_dup (description);
  ccl_hash_find (MAN_PAGES, page->manpage);
  ccl_hash_insert (MAN_PAGES, page);
}

			/* --------------- */

static void
s_delete_page (struct help_st *h)
{
  ccl_string_delete (h->manpage);
  ccl_string_delete (h->shortdesc);
  ccl_string_delete (h->description);
  ccl_delete (h);
}

			/* --------------- */

static void
s_load_pages (void)
{  
  char *datadir;
      
  ccl_pre (MAN_PAGES != NULL);
  
  if (PAGES_ARE_LOADED)
    return;
  
  datadir = getenv ("ARC_DATADIR");
#ifdef ARC_DATADIR
  if (datadir == NULL)
    datadir = ARC_DATADIR;
#endif
  if (datadir != NULL)
      PAGES_ARE_LOADED = s_read_manpages (datadir);

  if (! PAGES_ARE_LOADED)
    ccl_warning ("warning: can not find location for ARC man pages.\n"
		 "try to set environment variable ARC_DATADIR "
		 "(current value=%s).\n", datadir ? datadir : "");
}
