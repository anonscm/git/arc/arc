/*
 * ar-poset.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_POSET_H
# define AR_POSET_H

# include <ccl/ccl-list.h>
# include <ccl/ccl-iterator.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-protos.h>

typedef struct ar_poset_st ar_poset;

extern ar_poset *
ar_poset_create (ccl_hash_func *hash, ccl_duplicate_func *dup, 
		 ccl_compare_func *cmp, ccl_delete_proc *del);

extern ar_poset *
ar_poset_add_reference(ar_poset *poset);

extern void
ar_poset_del_reference(ar_poset *poset);

extern int
ar_poset_get_set_size(ar_poset *poset);

extern void
ar_poset_add(ar_poset *poset, void *ev);

extern int
ar_poset_has(ar_poset *poset, void *ev);

extern int
ar_poset_has_path(ar_poset *poset, void *ev1, void *ev2);

extern void*
ar_poset_get_object(ar_poset *poset, void *obj);

extern int
ar_poset_add_pair(ar_poset *poset,  void *greater, void *least);

extern void
ar_poset_add_pair_no_check (ar_poset *poset,  void *greater, void *least);

extern int
ar_poset_add_poset(ar_poset *poset, ar_poset *other);

extern int
ar_poset_add_lt(ar_poset *poset, ar_poset *poset1, ar_poset *poset2);

extern void
ar_poset_reverse(ar_poset *poset);

extern ccl_list *
ar_poset_get_greater_objects(ar_poset *poset, void *ev);

extern ccl_pointer_iterator *
ar_poset_get_objects(ar_poset *poset);

extern ccl_list *
ar_poset_get_top_to_down_objects(ar_poset *poset);

extern ccl_list *
ar_poset_get_down_to_top_objects(ar_poset *poset);

extern int
ar_poset_is_empty(ar_poset *poset);

extern void
ar_poset_log(ccl_log_type log, ar_poset *poset, 
	     void (*logproc)(ccl_log_type log, void *obj));

extern ar_poset *
ar_poset_dup(ar_poset *poset);

extern ccl_list *
ar_poset_get_topological_order (ar_poset *poset);

#endif /* ! AR_POSET_H */
