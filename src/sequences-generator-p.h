/*
 * sequences-generator-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SEQUENCES_GENERATOR_P_H
# define SEQUENCES_GENERATOR_P_H

# include "ar-rldd.h"

extern ccl_list *
sgen_list_of_counters_identifiers (ar_ca *ca);

extern void
ar_rldd_to_aralia (ar_rlddm *man, ar_rldd *rldd, ccl_log_type log, 
		   ar_rldd_log_label *ll, void *ll_data, const char *prefix);

#endif /* ! SEQUENCES_GENERATOR_P_H */
