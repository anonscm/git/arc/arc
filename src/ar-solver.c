/*
 * ar-solver.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-solver-p.h"

typedef enum {
  SATISFIABLE,
  MODIFIED,
  INCONSISTENCY
} solver_status;

typedef struct ar_gp_solver_st ar_gp_solver;

struct ar_gp_solver_st {
  ar_solver super;
  ar_ca_expr **constraints;
  int *solved;
  int nb_to_solve;
  int nb_constraints;
};

			/* --------------- */

static solver_status
s_solve_constraints (ar_gp_solver *solver);

static void
s_enumerate_solutions (ar_gp_solver *solver);

static void
s_propagate (ar_gp_solver *solver, ar_ca_expr *expr, int value,
	     solver_status *rstatus);

			/* --------------- */

static void
s_gp_solver_delete (ar_solver *S)
{
  int i;
  ar_gp_solver *solver = (ar_gp_solver *) S;
  
  ccl_pre( solver != NULL );

  for(i = 0; i < solver->nb_constraints; i++)
    ar_ca_expr_del_reference(solver->constraints[i]);
  ccl_delete(solver->constraints);
  ccl_delete(solver->solved);
}

			/* --------------- */

static void
s_gp_solver_solve (ar_solver *S)
{
  solver_status status; 
  ar_gp_solver *solver = (ar_gp_solver *) S;
  
  ccl_pre (solver != NULL);

  ccl_memzero (solver->solved, sizeof (uint32_t) * solver->nb_constraints);
  solver->nb_to_solve = solver->nb_constraints;

  status = s_solve_constraints (solver);
  if (status == SATISFIABLE)
    {
      if (solver->nb_to_solve > 0)
	s_enumerate_solutions (solver);
      else
	SET_ADD (solver);
    }
}

			/* --------------- */

ar_solver *
ar_create_gp_solver(ar_ca_expr **constraints, int nb_constraints,
		    ar_ca_expr **variables, int nb_variables,
		    ar_backtracking_stack *bstack,
		    ar_solver_start_solution_se_proc *set_start,
		    ar_solver_new_solution_proc *set_add,
		    ar_solver_end_solution_set_proc *set_end)
{
  int i;
  ar_gp_solver *result = (ar_gp_solver *)
    ar_allocate_solver (sizeof (ar_gp_solver), s_gp_solver_delete,
			s_gp_solver_solve, variables, nb_variables,
			bstack, set_start, set_add, set_end);

  result->constraints = ccl_new_array(ar_ca_expr *,nb_constraints);
  result->solved = ccl_new_array(int,nb_constraints);
  result->nb_to_solve = 0;
  result->nb_constraints = nb_constraints;  
  for(i = 0; i < nb_constraints; i++)
    result->constraints[i] = ar_ca_expr_add_reference(constraints[i]);

  return (ar_solver *) result;
}

			/* --------------- */

ar_solver *
ar_allocate_solver (size_t size,
		    void (*delete_solver) (ar_solver *s),
		    void (*solve) (ar_solver *s),
		    ar_ca_expr **variables, int nb_variables,
		    ar_backtracking_stack *bstack,
		    ar_solver_start_solution_se_proc *set_start,
		    ar_solver_new_solution_proc *set_add,
		    ar_solver_end_solution_set_proc *set_end)
{
  int i;
  ar_solver *result = ccl_calloc (1, size);

  result->variables = ccl_new_array (ar_ca_expr *, nb_variables);
  result->bounds = ccl_new_array (ar_bounds *, nb_variables);
  result->nb_variables = nb_variables;
  for (i = 0; i < nb_variables; i++)
    {
      result->variables[i] = ar_ca_expr_add_reference(variables[i]);
      result->bounds[i] = ar_ca_expr_get_bounds_address(variables[i]);
    }

  if( bstack == NULL )
    result->bstack = ar_backtracking_stack_create();
  else
    result->bstack = ar_backtracking_stack_add_reference(bstack);

  result->delete_solver = delete_solver;
  result->solve = solve;
  result->set_start = set_start;
  result->set_add = set_add;
  result->set_end = set_end;

  return result;
}

void
ar_solver_delete(ar_solver *solver)
{
  int i;

  ccl_pre( solver != NULL );

  solver->delete_solver (solver);
 
  for(i = 0; i < solver->nb_variables; i++)
    ar_ca_expr_del_reference(solver->variables[i]);
  ccl_delete(solver->variables);
  ccl_delete(solver->bounds);

  ar_backtracking_stack_del_reference(solver->bstack);
  ccl_delete(solver);
}

			/* --------------- */

void
ar_solver_solve(ar_solver *solver, void *clientdata)
{
  ccl_pre (solver != NULL);

  ar_backtracking_stack_choice_point(solver->bstack);

  solver->set_data = clientdata;
  if( solver->set_start != NULL )
    solver->set_start(solver,solver->set_data);

  solver->solve (solver);

  if( solver->set_end != NULL )
    solver->set_end(solver,solver->set_data);

  ar_backtracking_stack_restore_last(solver->bstack);
  
}

			/* --------------- */

ar_ca_expr **
ar_solver_get_variables(ar_solver *solver, int *psize)
{
  ccl_pre( solver != NULL );   ccl_pre( psize != NULL ); 

  *psize = solver->nb_variables;

  return solver->variables;
}

			/* --------------- */

static solver_status
s_solve_constraints (ar_gp_solver *solver)
{
  int i;
  ar_ca_expr *A;
  ar_ca_expr **constraints = solver->constraints;
  int nb_constraints = solver->nb_constraints;
  int *solved = solver->solved;
  solver_status status;
  solver_status aux;

  do 
    {
      status = SATISFIABLE;

      for(i = 0; i < nb_constraints && status != INCONSISTENCY; i++)
	{
	  if( solved[i] ) 
	    continue;

	  ccl_assert( solver->nb_to_solve > 0 );

	  A = constraints[i];

	  ar_ca_expr_evaluate(A,1);
	  aux = SATISFIABLE;
	  s_propagate(solver,A,1,&aux);

	  if( ar_ca_expr_get_min(A) == ar_ca_expr_get_max(A) )
	    {
	      ar_backtracking_stack_save (BSTACK (solver), &solved[i]);
	      solved[i] = 1;
	      solver->nb_to_solve--;
	    }
	    
	  if( aux != SATISFIABLE )
	    status = aux;
	}
    } 
  while( status == MODIFIED );

  return status;
}

			/* --------------- */

static int
s_enumerate_solutions_rec (ar_gp_solver *solver, int varindex)
{
  int terminated = 0;
  int min, max, val;
  solver_status status;
  ar_ca_expr *var;
  ar_bounds *bounds;
  const ar_ca_domain *dom;

  ccl_pre (0 <= varindex && varindex < NB_VARIABLES (solver));

  var = VARIABLES (solver, varindex);
  bounds = BOUNDS (solver, varindex);
  min = bounds->min;
  max = bounds->max;
  dom = ar_ca_expr_variable_get_domain(var);

  for(val = min; val <= max && ! terminated; val++)
    {
      if( ! ar_ca_domain_has_element(dom,val) )
	continue;
      
      ar_backtracking_stack_choice_point (BSTACK (solver));
      
      ar_bounds_save (bounds, BSTACK (solver));
      ar_bounds_set(bounds,val,val);
      ar_backtracking_stack_save (BSTACK (solver), &solver->nb_to_solve);
      
#if 0
      ar_ca_expr_log(CCL_LOG_DISPLAY,var);
      ccl_display("@%d <- %d, to_solve = %d\n",varindex,val,
		  solver->nb_to_solve);
#endif
      
      status = s_solve_constraints(solver);
      
      if( status == SATISFIABLE ) 
	{	  
	  if( solver->nb_to_solve == 0 ) 
	    {
	      ccl_assert( solver->nb_to_solve == 0 );
	      terminated = ! SET_ADD (solver);
	    }
	  else
	    terminated = s_enumerate_solutions_rec (solver,varindex-1);
	}
      ar_backtracking_stack_restore_last (BSTACK (solver));
    }

  ccl_assert (bounds->min == min && bounds->max == max);

  return terminated;
}

			/* --------------- */

static void
s_enumerate_solutions (ar_gp_solver *solver)
{
  (void) s_enumerate_solutions_rec (solver, NB_VARIABLES (solver) - 1);
}

			/* --------------- */

static void
s_propagate(ar_gp_solver *solver, ar_ca_expr *expr, int value,
	    solver_status *status)
{
  ar_ca_expression_kind kind;
  int lmin, lmax, lcmp, lval, rmin, rmax, rcmp, rval;
  ar_bounds *bounds;
  ar_ca_expr **args;
  ar_ca_expr *lop;
  ar_ca_expr *rop;

  if( *status == INCONSISTENCY ) 
    return;
  
  kind = ar_ca_expr_get_kind(expr);
  bounds = ar_ca_expr_get_bounds_address(expr);

  if( ! ar_bounds_contains(bounds,value) )
    {
      *status = INCONSISTENCY;
      return;
    }

  if( ar_bounds_is_singleton(bounds) )
    return;

  ccl_assert( kind != AR_CA_CST );

  if( kind == AR_CA_VAR )
    {
      const ar_ca_domain *dom = ar_ca_expr_variable_get_domain(expr);

      if( ! ar_ca_domain_has_element(dom,value) )
	*status = INCONSISTENCY;
      else
	{
	  ar_bounds_save (bounds, BSTACK (solver));
	  ar_bounds_set(bounds,value,value);
	  *status = MODIFIED;
	}
      return;
    }

  args = ar_ca_expr_get_args(expr);

  if( kind == AR_CA_NOT )
    {
      s_propagate(solver,args[0],(value?0:1),status);
      return;
    }

  if( kind == AR_CA_NEG )
    {
      s_propagate(solver,args[0],-value,status);
      return;
    }
  
  lop    = args[0];
  lmin   = ar_ca_expr_get_min(lop);
  lmax   = ar_ca_expr_get_max(lop);
  lcmp   = (lmin==lmax);
  lval   = lmin;

  rop    = args[1];
  rmin   = ar_ca_expr_get_min(rop);
  rmax   = ar_ca_expr_get_max(rop);
  rcmp   = (rmin==rmax);
  rval   = rmin;
  
  switch( kind ) {
  case AR_CA_ADD:
    if( lcmp ) s_propagate(solver,rop,value-lval,status);
    else if( rcmp ) s_propagate(solver,lop,value-rval,status);
    break;

  case AR_CA_MUL:
    if( lcmp )
      {
	if( lval == 0 ) 
	  return;
	else if( value % lval != 0 ) 
	  *status = INCONSISTENCY;
	else 
	  s_propagate(solver,rop,value/lval,status);
      }
    else if ( rcmp )
      {
	if( rval == 0 ) 
	  return;
	else if( value % rval != 0 ) 
	  *status = INCONSISTENCY;
	else 
	  s_propagate(solver,lop,value/rval,status);
      }
    break;

  case AR_CA_DIV:
    ccl_pre( rcmp && rval != 0 );
    if( lcmp )
      {
	if( lval / rval != value )
	  *status = INCONSISTENCY;
      }
    break;

  case AR_CA_MOD:
    ccl_pre( rcmp && rval != 0 );

    if( lcmp )
      {
	int v = (lval % rval);

	if( v < 0 ) v = -v;
	if( v != value )
	  *status = INCONSISTENCY;
      }
    break;

  case AR_CA_AND:
    if( value )
      {
	s_propagate(solver,lop,1,status);
	s_propagate(solver,rop,1,status);
      }
    else if( rcmp && rval )
      s_propagate(solver,lop,0,status);
    else if( lcmp && lval )
      s_propagate(solver,rop,0,status);
    break;

  case AR_CA_OR:
    if( ! value )
      {
	s_propagate(solver,lop,0,status);
	s_propagate(solver,rop,0,status);
      }
    else if( rcmp && ! rval )
      s_propagate(solver,lop,1,status);
    else if( lcmp && ! lval )
      s_propagate(solver,rop,1,status);
    break;

  case AR_CA_EQ:
    if( value )
      {
	if( lmin>rmax || lmax < rmin )
	  *status = INCONSISTENCY;
	else if( lmin == rmax )
	  {
	    s_propagate(solver,lop,lmin,status);
	    s_propagate(solver,rop,rmax,status);
	  }
	else if( lmax == rmin )
	  {
	    s_propagate(solver,lop,lmax,status);
	    s_propagate(solver,rop,rmin,status);
	  }
	else if( lcmp )
	  s_propagate(solver,rop,lval,status);
	else if( rcmp )
	  s_propagate(solver,lop,rval,status);
      }
    else
      {
	if( lcmp && rcmp && lval == rval )
	  *status = INCONSISTENCY;
      }
    break;

  case AR_CA_LT:
    if( value )
      {
        if( lmin >= rmax ) 
          *status = INCONSISTENCY;
        else if( lmin+1 == rmax) 
	  {
	    s_propagate(solver,lop,lmin,status);
	    s_propagate(solver,rop,rmax,status);
	  }
      }
    else 
      {
        if( lmax < rmin )
          *status = INCONSISTENCY;
        else if( lmax == rmin ) 
	  {
	    s_propagate(solver,lop,lmax,status);
	    s_propagate(solver,rop,rmin,status);
	  }
      }
    break;

  case AR_CA_ITE:
    if( lcmp ) 
      {
	if( lval ) s_propagate(solver,rop,value,status);
	else s_propagate(solver,args[2],value,status);
      }
    break;

  case AR_CA_MIN:
    if (lcmp && lval < value) 
      s_propagate (solver, rop, value, status);
    else if (rcmp && rval < value) 
      s_propagate (solver, lop, value, status);
    break;

  case AR_CA_MAX:
    if (lcmp && lval > value) 
      s_propagate (solver, rop, value, status);
    else if (rcmp && rval > value) 
      s_propagate (solver, lop, value, status);
    break;

  case AR_CA_CST:  case AR_CA_VAR: case AR_CA_NOT: case AR_CA_NEG:
    ccl_throw_no_msg (internal_error);
  };
}

struct counter 
{
  int result;
  int limit;
};

static int
s_count_solutions (ar_solver *solver, void *clientdata)
{
  struct counter *c = clientdata;
  c->result++;

  if (c->limit < 0)
    return 1;

  return (c->result <= c->limit);
}

#if 1
# define CRT_SOLVER ar_create_glucose_solver
#else
# define CRT_SOLVER ar_create_gp_solver
#endif

int
ar_solver_count_solutions (ar_ca_expr **constraints, int nb_constraints,
			   int limit)
{
  int i, nb_vars;
  struct counter data;
  ar_solver *solver;  
  ar_ca_expr **vars;
  ccl_list *variables = ccl_list_create ();

  for (i = 0; i < nb_constraints; i++)
    ar_ca_expr_get_variables (constraints[i], variables);

  nb_vars = ccl_list_get_size (variables);
  vars = ccl_new_array (ar_ca_expr *, nb_vars);
  i = 0;
  while (!ccl_list_is_empty (variables))
    {
      vars[i] = ccl_list_take_first (variables);
      ar_ca_expr_variable_reset_bounds (vars[i]);
      i++;
    }
  ccl_list_delete (variables);

  data.result = 0;
  data.limit = limit;
  solver = CRT_SOLVER (constraints, nb_constraints, vars, nb_vars, 
		       NULL, NULL, s_count_solutions, NULL);
  ar_solver_solve (solver, &data);
  ar_solver_delete (solver);
  ccl_delete (vars);

  return data.result;
}
