/*
 * varorder.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-list.h>
#include "varorder.h"

enum table_id {
  VAR_KEYS = 0,
  INDEX_KEYS,
  DD_INDEX_KEYS,
  NB_TABLES
};

typedef struct variable_st variable;
struct variable_st {
  void *var;
  int index;
  int dd_index;
  variable *parent;
};

struct varorder_st {  
  int refcount;
  ccl_list *varlist;
  ccl_hash *tables[NB_TABLES];
  ccl_delete_proc *vardel;
  varorder_log_proc *logvar;
  void *logvardata;
};

struct varlist_iterator {
  ccl_pointer_iterator super;
  ccl_pair *p;
};

			/* --------------- */

#define INT2PTR(i) ((void *) (intptr_t ) (i))
#define PTR2INT(p) ((int) (intptr_t ) (i))

static variable *
s_get_variable (const varorder *vo, enum table_id table, const void *var);

static variable *
s_representative (variable *v);

static variable *
s_representative_variable (const varorder *vo, const void *var);

			/* --------------- */

varorder * 
varorder_create (ccl_delete_proc *vardel, varorder_log_proc *logvar, 
		 void *logvardata)
{
  int i;
  varorder *result = ccl_new (varorder);

  result->refcount = 1;
  result->varlist = ccl_list_create ();
  for (i = 0; i < NB_TABLES; i++)
    result->tables[i] = ccl_hash_create (NULL, NULL, NULL, NULL);
  result->vardel = vardel;
  result->logvar = logvar;
  result->logvardata = logvardata;

  return result;
}

			/* --------------- */

varorder *
varorder_add_reference (varorder *vo)
{
  ccl_pre (vo != NULL);
  ccl_pre (vo->refcount > 0);

  vo->refcount++;

  return vo;
}

			/* --------------- */

void
varorder_del_reference (varorder *vo)
{
  ccl_pre (vo != NULL);
  ccl_pre (vo->refcount > 0);

  vo->refcount--;
  if (vo->refcount == 0)
    {
      int i;

      while (! ccl_list_is_empty (vo->varlist))
	{
	  variable *ov = ccl_list_take_first (vo->varlist);
	  if (vo->vardel)
	    vo->vardel (ov->var);
	  ccl_delete (ov);
	}
      ccl_list_delete (vo->varlist);

      for (i = 0; i < NB_TABLES; i++)
	ccl_hash_delete (vo->tables[i]);
      ccl_delete (vo);
    }
}

			/* --------------- */

int
varorder_get_nb_dd_indexes (const varorder *vo)
{
  ccl_pre (vo != NULL);
  
  return ccl_hash_get_size (vo->tables[DD_INDEX_KEYS]);
}

			/* --------------- */

int
varorder_get_dd_indexes (const varorder *vo, int **p_indexes)
{
  int result;

  ccl_pre (vo != NULL);

  result = ccl_list_get_size (vo->varlist);

  if (p_indexes != NULL)
    {
      ccl_pair *p;
      int *indexes = *p_indexes = ccl_new_array (int, result);

      for (p = FIRST (vo->varlist); p; p = CDR (p))
	{
	  variable *v = CAR (p);
	  variable *r = s_representative (v);

	  ccl_assert (0<= v->index && v->index < result);

	  indexes[v->index] = r->dd_index;
	}
    }

  return result;
}

			/* --------------- */

int
varorder_get_max_dd_index (const varorder *vo)
{
  ccl_pair *p;
  int max = -1;

  ccl_pre (vo != NULL);

  for (p = FIRST (vo->varlist); p; p = CDR (p))
    {
      variable *ov = CAR (p);
      if (max < ov->dd_index)
	max = ov->dd_index;
    }

  return max;
}

			/* --------------- */

int 
varorder_add_variable (const varorder *vo, void *var)
{
  variable *ov;

  ccl_pre (vo != NULL);
  ccl_pre (var != NULL);
  ccl_pre (! ccl_hash_find (vo->tables[VAR_KEYS], var));

  ov = ccl_new (variable);
  ov->var = var;
  ov->index = ccl_hash_get_size (vo->tables[VAR_KEYS]);
  ov->dd_index = -1;
  ov->parent = ov;

  ccl_list_add (vo->varlist, ov);
  ccl_hash_find (vo->tables[VAR_KEYS], var);
  ccl_hash_insert (vo->tables[VAR_KEYS], ov);
  ccl_hash_find (vo->tables[INDEX_KEYS], INT2PTR (ov->index));
  ccl_hash_insert (vo->tables[INDEX_KEYS], ov);

  return ov->index;
}

			/* --------------- */

void
varorder_reset_dd_indexes (const varorder *vo)
{
  ccl_pair *p;

  ccl_pre (vo != NULL);

  for (p = FIRST (vo->varlist); p; p = CDR (p))
    {
      variable *var = CAR (p);
      if (ccl_hash_find (vo->tables[DD_INDEX_KEYS], INT2PTR (var->dd_index)))
	ccl_hash_remove(vo->tables[DD_INDEX_KEYS]);
      ((variable *) CAR (p))->dd_index = -1;
    }
  ccl_post (ccl_hash_get_size (vo->tables[DD_INDEX_KEYS]) == 0);
}

			/* --------------- */

void
varorder_unset_dd_index (const varorder *vo, const void *var)
{
  variable *ov = s_representative_variable (vo, var);

  ccl_assert (ov->parent == ov);

  if (ov->dd_index < 0)
    return;

  ccl_hash_find (vo->tables[DD_INDEX_KEYS], INT2PTR (ov->dd_index));
  ccl_hash_remove (vo->tables[DD_INDEX_KEYS]);
  ov->dd_index = -1;
}

			/* --------------- */

void
varorder_set_dd_index (const varorder *vo, const void *var, int dd_index)
{
  variable *ov = s_representative_variable (vo, var);

  ccl_assert (ov->parent == ov);
  ccl_pre (! ccl_hash_find (vo->tables[DD_INDEX_KEYS], INT2PTR (dd_index)));

  if (ov->dd_index >= 0 &&
      ccl_hash_find (vo->tables[DD_INDEX_KEYS], INT2PTR (ov->dd_index)))
    ccl_hash_remove (vo->tables[DD_INDEX_KEYS]);

  ccl_hash_find (vo->tables[DD_INDEX_KEYS], INT2PTR (dd_index));
  ccl_hash_insert (vo->tables[DD_INDEX_KEYS], ov);
  ov->dd_index = dd_index;

}

			/* --------------- */

void
varorder_swap_dd_index (const varorder *vo, const void *v1, const void *v2)
{
  int dd_index_1 = varorder_get_dd_index (vo, v1);
  int dd_index_2 = varorder_get_dd_index (vo, v2);

  varorder_unset_dd_index (vo, v1);
  varorder_unset_dd_index (vo, v2);
  varorder_set_dd_index (vo, v1, dd_index_2);
  varorder_set_dd_index (vo, v2, dd_index_1);
}

			/* --------------- */

int
varorder_get_index (const varorder *vo, const void *var)
{
  variable *ov = s_get_variable (vo, VAR_KEYS, var);

  return ov->index;
}

			/* --------------- */

int
varorder_get_dd_index (const varorder *vo, const void *var)
{
  variable *ov = s_representative_variable (vo, var);
  
  return ov->dd_index;
}

			/* --------------- */

void *
varorder_get_representative (const varorder *vo, const void *var)
{
  variable *ov = s_representative_variable (vo, var);

  return ov->var;
}

			/* --------------- */

void 
varorder_merge (const varorder *vo, const void *rep, const void *var)
{
  variable *ov_rep = s_representative_variable (vo, rep);
  variable *ov_var = s_representative_variable (vo, var);

  ccl_pre (ov_rep->dd_index == -1 || ov_var->dd_index == -1);

  ov_var->parent = ov_rep;
  if (ov_rep->dd_index == -1)
    {
      ov_rep->dd_index = ov_var->dd_index;
      ov_var->dd_index = -1;
    }
}

			/* --------------- */

void *
varorder_get_variable_from_index (const varorder *vo, int index)
{
  variable *ov = s_get_variable (vo, INDEX_KEYS, INT2PTR (index));

  return ov->var;
}

			/* --------------- */

void *
varorder_get_variable_from_dd_index (const varorder *vo, int dd_index)
{
  variable *ov = s_get_variable (vo, DD_INDEX_KEYS, INT2PTR (dd_index));

  return ov->var;
}

			/* --------------- */

int
varorder_has_dd_index (const varorder *vo, const int dd_index)
{
  ccl_pre (vo != NULL);

  return ccl_hash_find (vo->tables[DD_INDEX_KEYS], INT2PTR (dd_index));
}

			/* --------------- */

static void
s_log_varlist (ccl_log_type log, const varorder *vo, ccl_list *varlist,
	       void (*deps)(ccl_log_type log, void *var, void *data),
	       void *data)
{
  ccl_pair *p;

  for (p = FIRST (varlist); p; p = CDR (p))
    {
      variable *v = CAR (p);
      variable *r = s_representative (v);

      ccl_log (log, "%3d -dd-> %3d : ", v->index, r->dd_index);
      if (vo->logvar)
	vo->logvar (log, v->var, vo->logvardata);
      else 
	ccl_log (log, "%p", v->var);
      if (v != r)
	{
	  ccl_log (log, " (= ");
	  if (vo->logvar)
	    vo->logvar (log, r->var, vo->logvardata);
	  else 
	    ccl_log (log, " %p", r->var);
	  ccl_log (log, ")");
	}
      if (deps != NULL)
	deps (log, v->var, data);
      ccl_log (log, "\n");
    }
}

			/* --------------- */

void
varorder_log (ccl_log_type log, const varorder *vo)
{
  varorder_log_with_deps (log, vo, NULL, NULL);
}

			/* --------------- */
void
varorder_log_with_deps (ccl_log_type log, const varorder *vo,
			void (*deps)(ccl_log_type log, void *var, void *data),
			void *data)
{
  s_log_varlist (log, vo, vo->varlist, deps, data);
}

			/* --------------- */

static int
s_cmp_dd_index (const void *v1, const void *v2)
{
  const variable *ov1 = s_representative (v1);
  const variable *ov2 = s_representative (v2);
  
  return ov1->dd_index - ov2->dd_index;
}

			/* --------------- */

void
varorder_log_sorted_by_dd_index (const ccl_log_type log, const varorder *vo)  
{
  varorder_log_sorted_by_dd_index_with_deps (log, vo, NULL, NULL);
}

			/* --------------- */

void
varorder_log_sorted_by_dd_index_with_deps (const ccl_log_type log,
					   const varorder *vo,
					   void (*deps)(ccl_log_type log,
							void *var,
							void *data),
					   void *data)
{
  ccl_list *vars = ccl_list_dup (vo->varlist);

  ccl_list_sort (vars, s_cmp_dd_index);
  s_log_varlist (log, vo, vars, deps, data);
  ccl_list_delete (vars);
}

			/* --------------- */

static int
s_varlist_iterator_has_more_elements (const ccl_pointer_iterator *i)
{
  return ((struct varlist_iterator *)i)->p != NULL;
}

			/* --------------- */

static void *
s_varlist_iterator_next_element (ccl_pointer_iterator *i)
{
  struct varlist_iterator *vli = (struct varlist_iterator *) i;
  variable *result = CAR (vli->p);

  vli->p = CDR (vli->p);

  return result->var;
}

			/* --------------- */

static void 
s_varlist_iterator_delete_iterator (ccl_pointer_iterator *i)
{
  ccl_delete (i);
}

			/* --------------- */

ccl_pointer_iterator *
varorder_get_variables (const varorder *vo)
{
  struct varlist_iterator *result = ccl_new (struct varlist_iterator);

  result->super.has_more_elements = s_varlist_iterator_has_more_elements; 
  result->super.next_element = s_varlist_iterator_next_element; 
  result->super.delete_iterator = s_varlist_iterator_delete_iterator; 
  result->p = FIRST (vo->varlist);

  return (ccl_pointer_iterator *) result;
}

			/* --------------- */

static variable *
s_get_variable (const varorder *vo, enum table_id table, const void *var)
{
  variable *result;

  ccl_pre (vo != NULL);

  result = ccl_hash_get_with_key (vo->tables[table], var);

  ccl_post (result != NULL);

  return result;
}

			/* --------------- */

static variable *
s_representative (variable *v)
{
  if (v->parent != v)
    v->parent = s_representative (v->parent);

  return v->parent;
}

			/* --------------- */

static variable *
s_representative_variable (const varorder *vo, const void *var)
{
  variable *ov = s_get_variable (vo, VAR_KEYS, var);

  return s_representative (ov);
}

			/* --------------- */
