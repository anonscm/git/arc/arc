/*
 * ar-broadcast.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_BROADCAST_H
# define AR_BROADCAST_H

# include "ar-identifier.h"
# include "ar-poset.h"

typedef struct ar_broadcast_st ar_broadcast;

extern ar_broadcast *
ar_broadcast_create(int maximize);

extern ar_broadcast *
ar_broadcast_clone(ar_broadcast *bv);

extern int
ar_broadcast_get_size (const ar_broadcast *bv);

extern void
ar_broadcast_set_maximize (ar_broadcast *bv, int maximize);

extern int
ar_broadcast_get_maximize (ar_broadcast *bv);

extern void
ar_broadcast_add_event(ar_broadcast *bv, ar_identifier *event, int marked);

extern ar_broadcast *
ar_broadcast_add_reference(ar_broadcast *bv);

extern void
ar_broadcast_del_reference(ar_broadcast *bv);

extern int
ar_broadcast_has_subnode(ar_broadcast *bv, const ar_identifier *subnode);

extern int
ar_broadcast_has_event(ar_broadcast *bv, ar_identifier *event);

extern int 
ar_broadcast_get_nb_marked(ar_broadcast *bv);

extern int 
ar_broadcast_get_nb_not_marked(ar_broadcast *bv);

extern void
ar_broadcast_set_bounds(ar_broadcast *bv, int min, int max);

extern void
ar_broadcast_get_bounds(ar_broadcast *bv, int *pmin, int *pmax);

extern ar_identifier_iterator *
ar_broadcast_get_subnodes (ar_broadcast *bv);

extern int
ar_broadcast_is_marked (ar_broadcast *bv, ar_identifier *subnode);

extern ar_identifier_array 
ar_broadcast_get_marked_subnodes (ar_broadcast *bv);

extern ar_identifier_array 
ar_broadcast_get_not_marked_subnodes (ar_broadcast *bv);

extern ar_identifier *
ar_broadcast_get_event(ar_broadcast *bv, ar_identifier *subnode);

extern ar_poset *
ar_broadcast_get_instance_vectors (ar_broadcast *bv);

extern ccl_list * 
ar_broadcast_get_instance_vector (ar_broadcast *bv);

extern void
ar_broadcast_log(ccl_log_type log, ar_broadcast *bv);

#endif /* ! AR_BROADCAST_H */
