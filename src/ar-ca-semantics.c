/*
 * ar-ca-semantics.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-stack.h>
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-semantics.h"
#include "ar-ca-p.h"
#include "ar-ca-semantics.h"


static ar_ca_domain *
s_type_to_domain (ar_type *t, ar_ca_exprman *man)
  CCL_THROW (abtract_type_exception)
{
  ar_ca_domain *result = NULL;

  switch (ar_type_get_kind (t)) 
    {
    case AR_TYPE_BOOLEANS:
      result = ar_ca_domain_create_bool ();
      break;
      
    case AR_TYPE_INTEGERS:
      ccl_warning("warning: Infinite integer sets are not supported by "
		  "contraint automata. The\ndomain is replaced by the range "
		  "[%d,%d].\n", INT_MIN, INT_MAX);
      result = ar_ca_domain_create_range (INT_MIN, INT_MAX);
      break;

    case AR_TYPE_RANGE:
      result = ar_ca_domain_create_range (ar_type_range_get_min (t),
					  ar_type_range_get_max (t));
      break;

    case AR_TYPE_SYMBOL_SET:
      {
	int k = 0;
	ccl_int_iterator *ii = ar_type_symbol_set_get_values (t);
	int *values = ccl_new_array (int, ar_type_get_cardinality (t));
	
	while (ccl_iterator_has_more_elements(ii)) 
	  {
	    int index = ccl_iterator_next_element (ii);
	    ar_identifier *id = ar_type_symbol_set_get_value_name (index);
	    ar_ca_expr *cst = ar_ca_expr_crt_enum_constant (man, id);
	    ar_identifier_del_reference (id);
	    
	    values[k++] = ar_ca_expr_enum_constant_get_index (cst);	  
	    ar_ca_expr_del_reference (cst);
	  }
	ccl_iterator_delete (ii);
	
	ccl_assert (k == ar_type_get_cardinality (t));
	
	result = ar_ca_domain_create_enum (values, k);
	ccl_delete (values);
      }
      break;

    case AR_TYPE_ABSTRACT:
      ccl_throw_no_msg (abstract_type_exception);      
      break;
      
    case AR_TYPE_SYMBOLS: 
    case AR_TYPE_ARRAY: case AR_TYPE_STRUCTURE: case AR_TYPE_BANG:
    case AR_TYPE_ENUMERATION:
      ccl_unreachable ();
      break;
    }
  
  return result;
}

			/* --------------- */

ar_ca_domain *
ar_model_type_to_ca_domain(ar_type *type, ar_ca_exprman *man,
			   ccl_hash *domaincache)
  CCL_THROW(abtract_type_exception)
{
  ar_ca_domain *result = NULL;

  if( domaincache == NULL )
    result = s_type_to_domain(type,man);
  else if( ccl_hash_find(domaincache,type) )
    {
      result = (ar_ca_domain *)ccl_hash_get(domaincache);
      result = ar_ca_domain_add_reference(result);
    }
  else 
    {
      result = s_type_to_domain(type,man);
      ccl_hash_insert(domaincache,result);
    }

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_expr_to_ca_expr (ar_expr *e, ar_ca *ca, ar_ca_exprman *man, 
		   ccl_hash *domains, ccl_hash *cache)
  CCL_THROW(abtract_type_exception)
{
  int i, arity;
  ar_ca_expr *result = NULL;
  ar_expr_op op;
  ar_expr **args;
  ar_ca_expr **ops;

  if (ccl_hash_find (cache, e))
    {
      result = ccl_hash_get (cache);
      result = ar_ca_expr_add_reference (result);
      return result;
    }

  op = ar_expr_get_op (e);
  args = ar_expr_get_args (e, &arity);
  ops = arity ? ccl_new_array (ar_ca_expr *, arity) : NULL;

  ccl_try (abstract_type_exception)
    {
      for (i = 0; i < arity; i++) 
	ops[i] = s_expr_to_ca_expr (args[i], ca, man, domains, cache);
    }
  ccl_catch
    {
      goto end;
    }
  ccl_end_try;
  
  ccl_assert (ccl_imply ((op != AR_EXPR_OR && op != AR_EXPR_AND && 
			  op != AR_EXPR_MIN && op != AR_EXPR_MAX && 
			  op != AR_EXPR_ITE), arity <= 2));

  switch (op) 
    {
    case AR_EXPR_CST: 
      {
	int val;
	ar_constant *cst = ar_expr_eval (e, NULL);

	switch (ar_expr_get_type_kind (e)) 
	  {
	  case AR_TYPE_BOOLEANS:
	    if (ar_expr_is_true (e)) 
	      result = ar_ca_expr_crt_boolean_constant (man, 1);
	    else
	      result = ar_ca_expr_crt_boolean_constant (man, 0);
	    break;
	    
	  case AR_TYPE_RANGE: 
	  case AR_TYPE_INTEGERS:
	    val = ar_constant_get_integer_value (cst);
	    result = ar_ca_expr_crt_integer_constant (man, val);
	    break;

	  case AR_TYPE_SYMBOL_SET: 
	  case AR_TYPE_SYMBOLS: 
	    {
	      ar_identifier *id;
	      
	      val = ar_constant_get_symbol_value (cst);

	      id = ar_type_symbol_set_get_value_name (val);
	      result = ar_ca_expr_crt_enum_constant (man, id);
	      ar_identifier_del_reference (id);
	    }
	    break;

	  case AR_TYPE_ABSTRACT:
	    ar_constant_del_reference (cst);
	    ccl_throw_no_msg (abstract_type_exception);
	    break;
	    
	  case AR_TYPE_ARRAY: case AR_TYPE_STRUCTURE: 
	  case AR_TYPE_BANG: 
	  case AR_TYPE_ENUMERATION:
	    ccl_unreachable ();
	    break;
	  }
	ar_constant_del_reference (cst);
      }
      break;
    
  case AR_EXPR_VAR: case AR_EXPR_PARAM:
    {
      ar_context_slot *sl = ar_expr_get_slot (e);
      ar_identifier *id = ar_expr_get_name (e);
      
      if ((result = ar_ca_expr_get_variable_by_name (man, id)) == NULL)
	{
	  ar_expr *value = ar_context_slot_get_value (sl);
	  if (value == NULL)
	    {
	      ccl_list *attrs = ar_expr_get_attributes (e);
	      ar_type *type = ar_expr_get_type (e);
	      ar_ca_domain *dom = NULL;
	      ccl_try (abstract_type_exception)
	        {
		  dom = ar_model_type_to_ca_domain (type, man, domains);
		  result = ar_ca_expr_crt_variable (man, id, dom, attrs);

		  ccl_assert ((ar_context_slot_get_flags (sl) & 
			       AR_SLOT_FLAG_PARAMETER));
	  
		  if (ca != NULL)
		    ar_ca_add_state_variable (ca, result);
		  else
		    ar_ca_expr_reset_bounds (result);
		  
		  ar_ca_domain_del_reference (dom);
		}
	      ccl_no_catch;
	      ar_type_del_reference (type);
	      if (attrs != NULL)
		ccl_list_clear_and_delete (attrs, (ccl_delete_proc *)
					   ar_identifier_del_reference);
	    }
	  else
	    {
	      ccl_try (abstract_type_exception)
	        {	      
		  result = s_expr_to_ca_expr (value, ca, man, domains, cache);
		}
	      ccl_no_catch;
	      ar_expr_del_reference (value);
	    }	  
	}
      ar_identifier_del_reference (id);
      ar_context_slot_del_reference (sl);
    }
    break;

    case AR_EXPR_MIN: case AR_EXPR_MAX: case AR_EXPR_OR: case AR_EXPR_AND:
      {      
	ar_ca_expr *(*crt)(ar_ca_expr *,ar_ca_expr *);
	if (op == AR_EXPR_OR) crt = ar_ca_expr_crt_or;
	else if (op == AR_EXPR_AND) crt = ar_ca_expr_crt_and;
	else if (op == AR_EXPR_MIN) crt = ar_ca_expr_crt_min;
	else crt = ar_ca_expr_crt_max;

	result = crt (ops[0],ops[1]); 
	for(i = 2; i < arity; i++)
	  {
	    ar_ca_expr *tmp = crt (result, ops[i]);
	    ar_ca_expr_del_reference(result);
	    result = tmp;
	  }
      }
    break;
    
  case AR_EXPR_ITE: result = ar_ca_expr_crt_ite(ops[0],ops[1],ops[2]); break;
  case AR_EXPR_ADD: result = ar_ca_expr_crt_add(ops[0],ops[1]); break;
  case AR_EXPR_SUB: result = ar_ca_expr_crt_sub(ops[0],ops[1]); break;
  case AR_EXPR_MUL: result = ar_ca_expr_crt_mul(ops[0],ops[1]); break;
  case AR_EXPR_DIV: result = ar_ca_expr_crt_div(ops[0],ops[1]); break;
  case AR_EXPR_MOD: result = ar_ca_expr_crt_mod(ops[0],ops[1]); break;
  case AR_EXPR_NOT: result = ar_ca_expr_crt_not(ops[0]); break;
  case AR_EXPR_NEG: result = ar_ca_expr_crt_neg(ops[0]); break;
  case AR_EXPR_EQ : result = ar_ca_expr_crt_eq(ops[0],ops[1]); break;
  case AR_EXPR_NEQ: result = ar_ca_expr_crt_neq(ops[0],ops[1]); break;
  case AR_EXPR_LT : result = ar_ca_expr_crt_lt(ops[0],ops[1]); break;
  case AR_EXPR_GT : result = ar_ca_expr_crt_gt(ops[0],ops[1]); break;
  case AR_EXPR_LEQ: result = ar_ca_expr_crt_leq(ops[0],ops[1]); break;
  case AR_EXPR_GEQ: result = ar_ca_expr_crt_geq(ops[0],ops[1]); break;

  case AR_EXPR_EXIST: case AR_EXPR_FORALL: 
    {
      ccl_pair *p;
      ar_context *ctx = ar_expr_get_quantifier_context (e);
      ccl_list *qvars = ar_expr_get_quantified_variables(e);

      result = ar_ca_expr_add_reference(ops[0]);

      for(p = FIRST(qvars); p; p = CDR(p))
	{
	  ar_ca_expr *tmp;	  
	  ccl_list *slots = 
	    ar_expr_expand_composite_types (CAR (p), ctx, NULL);

	  while (!ccl_list_is_empty (slots))
	    {
	      ar_expr *var = (ar_expr *) ccl_list_take_first (slots);
	      ar_ca_expr *ca_var = NULL;

	      
	      ccl_try (abstract_type_exception)
	        {
		  ca_var =
		    s_expr_to_ca_expr (var,ca,man,domains, cache);
	      
		  if (op == AR_EXPR_EXIST) 
		    tmp = ar_ca_expr_crt_exist (ca_var, result);	      
		  else 
		    tmp = ar_ca_expr_crt_forall (ca_var, result);
		  ar_ca_expr_del_reference(ca_var);
		  ar_ca_expr_del_reference(result);
		  result = tmp;
		}
	      ccl_no_catch;
	      ar_expr_del_reference (var);
	      if (ca_var == NULL)
		break;
	    }
	  ccl_list_clear_and_delete (slots, (ccl_delete_proc *)
				     ar_expr_del_reference);
	}
      ar_context_del_reference (ctx);
    }
    break;


  case AR_EXPR_STRUCT_MEMBER: case AR_EXPR_ARRAY_MEMBER:
  case AR_EXPR_CALL: case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:
    ccl_throw_no_msg (internal_error);
    break;
  }
 end:
  if (ops != NULL)
    {
    
      for(i = 0; i < arity; i++) 
	ccl_zdelete (ar_ca_expr_del_reference, ops[i]);
      ccl_delete (ops);
    }
  
  if (result == NULL)
    ccl_rethrow ();

  ccl_post( result != NULL );

  ccl_hash_find (cache, e);
  ccl_hash_insert (cache, ar_ca_expr_add_reference (result));

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_model_expr_to_ca_expr(ar_expr *expr, ar_ca_exprman *man, 
			 ccl_hash *domaincache)
    CCL_THROW(abstract_type_exception)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				     ar_ca_expr_del_reference);
  ar_ca_expr *result = NULL;

  ccl_try (abstract_type_exception)
    {
      result = s_expr_to_ca_expr(expr,NULL,man,domaincache,cache);
    }
  ccl_no_catch;

  ccl_hash_delete (cache);
  if (result == NULL)
    ccl_rethrow ();
  
  return result;
}

			/* --------------- */

static void
s_make_variables(ar_node *n, ar_ca *ca, ccl_hash *domains, ccl_hash *cache)
    CCL_THROW(abtract_type_exception)
{
  int err = 0;
  
  CA_DBG_START_TIMER (("translate variables"));
  {
    ar_ca_exprman *man = ar_ca_get_expression_manager(ca);
    ar_context_slot_iterator *i = ar_node_get_slots_for_variables(n);

    while( ccl_iterator_has_more_elements(i) && ! err)
      {
	ar_context_slot *sl = ccl_iterator_next_element(i);
	ccl_list *attrs = ar_context_slot_get_attributes (sl);
	ar_identifier *vname = ar_context_slot_get_name(sl);
	ar_type *type = ar_context_slot_get_type(sl);
	ccl_try (abstract_type_exception)
	  {
	    ar_ca_domain *domain = ar_model_type_to_ca_domain(type,man,domains);
	    ar_ca_expr *var;
	
	    ccl_assert( ar_ca_expr_get_variable_by_name(man,vname) == NULL );
	    
	    var = ar_ca_expr_crt_variable (man, vname, domain, attrs);
	    ar_ca_domain_del_reference(domain);
      
	    if( (ar_context_slot_get_flags(sl) & AR_SLOT_FLAG_FLOW_VAR ) != 0 )
	      ar_ca_add_flow_variable(ca,var);
	    else
	      {
		ccl_assert( (ar_context_slot_get_flags(sl) & 
			     AR_SLOT_FLAG_STATE_VAR ) != 0 );
		ar_ca_add_state_variable(ca,var);
	      }
	    ar_ca_expr_del_reference(var);
	  }
	ccl_catch
	  {
	    err = 1;
	  }
	ccl_end_try;
	
	ar_type_del_reference(type);
	ar_identifier_del_reference(vname);
	if (attrs != NULL)
	  ccl_list_clear_and_delete (attrs, (ccl_delete_proc *)
				     ar_identifier_del_reference);
	ar_context_slot_del_reference(sl);
      }
    ccl_iterator_delete(i);

    if (! err)
      {
	i = ar_node_get_slots_for_parameters(n);
	while( ccl_iterator_has_more_elements(i) && ! err)
	  {
	    ar_context_slot *sl = ccl_iterator_next_element(i);
	    ar_expr *value = ar_context_slot_get_value(sl);
	    ar_identifier *pname = ar_context_slot_get_name(sl);
	    ar_type *type = ar_context_slot_get_type(sl);

	    ccl_try (abstract_type_exception)
	    {
	      ar_ca_domain *domain =
		ar_model_type_to_ca_domain (type, man, domains);
	      ar_ca_expr *var = ar_ca_expr_get_variable_by_name (man, pname);
	
	      if (var == NULL)
		{
		  var = ar_ca_expr_crt_variable (man, pname, domain, NULL);
		  ar_ca_add_state_variable(ca,var);
		}
	    
	      ar_ca_domain_del_reference(domain);

	      if (value == NULL)
		{
		  ccl_warning ("warning: parameter '");
		  ar_identifier_log (CCL_LOG_WARNING, pname);
		  ccl_warning ("' is encoded by an uninitialized state "
			       "variable.\n");
		}
	      else
		{
		  ar_ca_expr *ival =
		    s_expr_to_ca_expr (value, ca, man, domains, cache);
		  ar_ca_add_initial_assignment (ca, var, ival);
		  ar_expr_del_reference (value);
		  ar_ca_expr_del_reference (ival);
		}
	      ar_ca_expr_del_reference(var);    
	    }
	    ccl_catch
	      {
		err = 1;
	      }
	    ccl_end_try;
	
	    ar_type_del_reference(type);
	    ar_identifier_del_reference(pname);

	    ar_context_slot_del_reference(sl);
	  }
	ccl_iterator_delete(i);
      }
    ar_ca_exprman_del_reference(man);
  }
  CA_DBG_END_TIMER ();

  if (err)
    ccl_rethrow ();
}

			/* --------------- */

static void 
s_event_to_id_list_rec (ar_event *ev, ccl_list *result)
{
  int i;
  ar_event **comp = ar_event_get_components(ev);
  ar_identifier *id = ar_event_get_name (ev);
  int width = ar_event_get_width (ev);

  ccl_list_add (result, id);
      
  for (i = 0; i < width; i++)
    s_event_to_id_list_rec (comp[i], result);
}

static ccl_list *
s_event_to_id_list (ar_event *ev)
{
  ccl_list *result = ccl_list_create();
  
  s_event_to_id_list_rec (ev, result);
  
  return result;
}

			/* --------------- */

static ccl_hash *
s_make_events(ar_node *n, ar_ca *ca)
{
  ccl_hash *result = ccl_hash_create(NULL,NULL,NULL,NULL);
  const ccl_list *H = ar_ca_get_hierarchy_identifiers (ca);
    
  CA_DBG_START_TIMER (("translate events"));
  {
    ar_event_iterator *ei = ar_node_get_events(n);
    ar_ca_event **events = ar_ca_get_events (ca);

    while( ccl_iterator_has_more_elements(ei) )
      {
	ccl_pair *p;
	ar_event *ev = ccl_iterator_next_element(ei);
	const ccl_list *attrs = ar_event_get_attr (ev);
	ccl_list *ids = s_event_to_id_list (ev);
	ar_ca_event *ca_ev = ar_ca_event_create (H, ids);

	if (attrs)
	  {
	    for (p = FIRST (attrs); p; p = CDR (p))
	      ar_ca_event_add_attribute (ca_ev, CAR (p));
	  }
	*events = ar_ca_event_add_reference (ca_ev);

	ccl_hash_find(result,ev);
	ccl_hash_insert(result,ca_ev);
	ar_ca_event_del_reference(ca_ev);      
	ar_event_del_reference(ev);
	events++;
      }
    ccl_iterator_delete(ei);
  }
  CA_DBG_END_TIMER ();
  
  return result;
}

			/* --------------- */

static void
s_translate_assertions(ar_node *n, ar_ca *ca, ccl_hash *domains, 
		       ccl_hash *cache)
{
  CA_DBG_START_TIMER (("translate assertions"));
  {
    ccl_pair *p;
    ar_ca_exprman *man = ar_ca_get_expression_manager(ca);
    ccl_list *assertions = ar_node_get_assertions(n);
    
    
    for(p = FIRST(assertions); p; p = CDR(p))
      {
	ar_expr *a = (ar_expr *)CAR(p);
	ar_ca_expr *ca_a = s_expr_to_ca_expr(a,ca,man,domains, cache);
	ar_ca_add_assertion(ca,ca_a);
	ar_ca_expr_del_reference(ca_a);
      }
    ar_ca_exprman_del_reference(man);
  }
  CA_DBG_END_TIMER ();
}

			/* --------------- */

static void
s_translate_initial_states(ar_node *n, ar_ca *ca, ccl_hash *domains, 
			   ccl_hash *cache)
{
  CA_DBG_START_TIMER (("translate initial states & constraints"));
  {
    ccl_pair *p;
    ar_ca_exprman *man = ar_ca_get_expression_manager(ca);
    ccl_list *init = ar_node_get_initialized_slots(n);

    for(p = FIRST(init); p; p = CDR(p))
      {
	ar_context_slot *slot = (ar_context_slot *)CAR(p);
	ar_expr *value = ar_node_get_initial_value (n, slot);
	ar_identifier *name = ar_context_slot_get_name(slot);
	ar_ca_expr *ca_var = ar_ca_expr_get_variable_by_name(man,name);
	ar_ca_expr *ca_value = s_expr_to_ca_expr(value,ca,man,domains, cache);

	ccl_assert( ca_var != NULL );

	ar_ca_add_initial_assignment(ca,ca_var,ca_value);

	ar_expr_del_reference (value);
	ar_identifier_del_reference(name);
	ar_ca_expr_del_reference(ca_var);
	ar_ca_expr_del_reference(ca_value);
      }

    init = ar_node_get_initial_constraints (n);

    for(p = FIRST (init); p; p = CDR(p))
      {
	ar_expr *value = CAR (p);
	ar_ca_expr *ca_value = s_expr_to_ca_expr (value, ca, man, domains, 
						  cache);

	ar_ca_add_initial_constraint (ca, ca_value);

	ar_ca_expr_del_reference (ca_value);
      }
    ar_ca_exprman_del_reference(man);
  }
  CA_DBG_END_TIMER ();
}

			/* --------------- */

static ar_expr *
s_get_simplified_guard (ar_trans *t, ccl_hash *cache)
{
#if 1
  ar_expr *g = ar_trans_get_guard(t);
  ar_expr *tmpg = ar_expr_simplify_with_cache (g, cache);
  ar_expr_del_reference(g);

  return tmpg;
#else
  return ar_trans_get_guard(t);
#endif
}

			/* --------------- */
#if 0
static int
s_is_epsilon_trans (ar_ca_trans *t)
{
  int result = (ar_ca_trans_get_number_of_assignments (t) == 0);
  
  if (result)
    {
      ar_ca_expr *G = ar_ca_trans_get_guard (t);
      ar_ca_event *e = ar_ca_trans_get_event (t);

      result = (ar_ca_event_is_epsilon (e) && 
		ar_ca_expr_get_kind (G) == AR_CA_CST && 
		ar_ca_expr_get_min (G));
      ar_ca_expr_del_reference (G);
      ar_ca_event_del_reference (e);
    }
  return result;
}
#endif
			/* --------------- */

static void
s_translate_transitions(ar_node *n, ar_ca *ca, ccl_hash *eventtable, 
			ccl_hash *domains, ccl_hash *cache)
{
  CA_DBG_START_TIMER (("translate transitions"));
  {
    ccl_pair *p;
    ar_ca_exprman *man = ar_ca_get_expression_manager(ca);
    ccl_list *trans = ar_node_get_transitions(n);
    ar_ca_trans **ca_trans = ar_ca_get_transitions (ca);
    ccl_hash *sc = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				    ar_expr_del_reference);

    for(p = FIRST(trans); p; p = CDR(p))
      {
	ar_trans *t = (ar_trans *)CAR(p);
	ar_expr *g = s_get_simplified_guard(t,sc);
	ar_ca_expr *ca_g = s_expr_to_ca_expr(g,ca,man,domains, cache);
	ar_event *ev = ar_trans_get_event(t);
	ar_ca_event *ca_ev = (ccl_hash_find(eventtable,ev),
			      ccl_hash_get(eventtable));
	ar_ca_trans *ca_t = ar_ca_trans_create(ca_g,ca_ev);
	const ar_trans_assignment *a = ar_trans_get_assignments(t);

	ar_event_del_reference(ev);
	ar_expr_del_reference(g);
	ar_ca_expr_del_reference(ca_g);

	for(; a != NULL; a = a->next)
	  {
	    ar_identifier *name = ar_expr_get_name (a->lvalue);
	    ar_ca_expr *ca_var = ar_ca_expr_get_variable_by_name(man,name);
	    ar_ca_expr *ca_value = s_expr_to_ca_expr(a->value,ca,man,domains,
						     cache);

	    ar_ca_trans_add_assignment(ca_t,ca_var,ca_value);

	    ar_identifier_del_reference(name);
	    ar_ca_expr_del_reference(ca_value);
	    ar_ca_expr_del_reference(ca_var);
	  }
#if 0
	if (s_is_epsilon_trans (ca_t))
	  {
	    ca_g = ar_ca_expr_crt_boolean_constant (man, 0);
	    ar_ca_trans_set_guard (ca_t, ca_g);
	  }
#endif
	*ca_trans = ca_t;
	ca_trans++;
      }
    ccl_hash_delete (sc);
    ar_ca_exprman_del_reference(man);
  }
  CA_DBG_END_TIMER ();
}


static sas_law_param *
s_translate_law_parameter_value (ar_node *n, ar_idtable *params,
				 sas_law_param *p)
{
  sas_law_param *result;
  sas_law_param_kind kp = sas_law_param_get_kind (p);
  
  switch (kp)
    {
    case SAS_LAW_PARAM_CONSTANT:
      result = sas_law_param_add_reference (p);
      break;
    case SAS_LAW_PARAM_VARIABLE:
      {
	ar_identifier *id = sas_law_param_get_name (p);
	ccl_assert (sas_law_param_get_definition (p) == NULL);
	result = ar_idtable_get (params, id);
	if (result == NULL)
	  result = ar_model_get_law_parameter (id);
	ccl_post (result != NULL);
	ar_identifier_del_reference (id);
      }
      break;
      
    default:
      {
	sas_law_param *p1 = sas_law_param_get_arg (p, 0);
	sas_law_param *p2 = sas_law_param_get_arg (p, 1);
	sas_law_param *tp1 = s_translate_law_parameter_value (n, params, p1);
	sas_law_param *tp2 = s_translate_law_parameter_value (n, params, p2);
	result = sas_law_param_crt_binary (kp, tp1, tp2);
	sas_law_param_del_reference (p1);
	sas_law_param_del_reference (tp1);
	sas_law_param_del_reference (p2);
	sas_law_param_del_reference (tp2);
      }
      break;
    }
  
  return result;
}

struct update_params_data
{
  ar_idtable *params;
  ar_node *node;
};

static sas_law_param *
s_update_params (sas_law_param *p, void *data)
{
  struct update_params_data *d = data;
  
  return s_translate_law_parameter_value (d->node, d->params, p);
}

struct update_cond_data
{
  ar_ca *ca;
  ar_ca_exprman *man;
  ccl_hash *domains;
  ccl_hash *cache;
};

static sas_conditional_law *
s_update_cond (void *cond, sas_law *law, void *data)
{
  struct update_cond_data *d = data;
  ar_expr *c = cond;
  ar_ca_expr *cacond =
    s_expr_to_ca_expr (c, d->ca, d->man, d->domains, d->cache);
  sas_conditional_law *result =
    sas_conditional_law_crt (law, NULL, 
			     (ccl_to_string_func *) ar_ca_expr_to_string,
			     (ccl_delete_proc *) ar_ca_expr_del_reference,
			     cacond);
  
  return result;
}

static void
s_translate_parameters_and_laws (ar_node *n, ar_ca *result, ccl_hash *domains, 
				 ccl_hash *cache)
{
  ccl_pair *p;
  struct update_params_data data;
  struct update_cond_data cond_data;
  ccl_list *ids = ar_node_get_ordered_ids_of_law_parameters (n);
  const ccl_list *lids;

  data.params =
    ar_idtable_create (0, (ccl_duplicate_func *) sas_law_param_add_reference,
		       (ccl_delete_proc *) sas_law_param_del_reference);
  data.node = n;
  cond_data.ca = result;
  cond_data.man = ar_ca_get_expression_manager (result);
  cond_data.domains = domains;
  cond_data.cache = cache;
  
  while (! ccl_list_is_empty (ids))
    {
      sas_law_param *tp;
      ar_identifier *id = ccl_list_take_first (ids);
      sas_law_param *lp = ar_node_get_law_parameter (n, id);

      ccl_pre (lp != NULL);
      tp = s_translate_law_parameter_value (n, data.params, lp);
      sas_law_param_del_reference (lp);
      lp = sas_law_param_crt_variable (id);
      sas_law_param_set_definition (lp, tp);
      sas_law_param_del_reference (tp);
      
      ar_idtable_put (data.params, id, lp);
      ar_ca_add_law_parameter (result, lp);
      sas_law_param_del_reference (lp);
    }
  ccl_list_delete (ids);

  lids = ar_node_get_ids_of_laws (n);  
  for (p = FIRST (lids); p; p = CDR (p))
    {
      sas_law *nl;
      ar_identifier *eid = CAR (p);
      sas_law *l = ar_node_get_law (n, eid);
      
      ccl_assert (l != NULL);
      nl = sas_law_substitute_parameters (l, s_update_params, &data,
					  s_update_cond, &cond_data);
      ar_ca_add_law (result, eid, nl);
      
      sas_law_del_reference (l);
      sas_law_del_reference (nl);
    }
  
  ar_ca_exprman_del_reference (cond_data.man);  
  ar_idtable_del_reference (data.params);
}

static void
s_translate_observers (ar_node *n, ar_ca *ca, ccl_hash *domains, 
		       ccl_hash *cache)
{
  CA_DBG_START_TIMER (("translate observers"));
  {
    ccl_pair *p;
    ar_ca_exprman *man = ar_ca_get_expression_manager (ca);
    const ccl_list *observers = ar_node_get_ids_of_observers (n);
    
    
    for(p = FIRST (observers); p; p = CDR (p))
      {
	ar_identifier *oid = (ar_identifier *) CAR (p);
	ar_expr *obs = ar_node_get_observer (n, oid);
	ar_ca_expr *ca_obs = s_expr_to_ca_expr (obs, ca, man, domains, cache);
	
	ar_ca_add_observer (ca, oid, ca_obs);
	
	ar_ca_expr_del_reference (ca_obs);
	ar_expr_del_reference (obs);
      }
    ar_ca_exprman_del_reference (man);
  }
  CA_DBG_END_TIMER ();
}

static void
s_translate_preemptibles (ar_node *n, ar_ca *ca)
{
  CA_DBG_START_TIMER (("translate preemptibles"));
  {
    ccl_pointer_iterator *pi = ar_node_get_preemptibles (n);

    while (ccl_iterator_has_more_elements (pi))
      {
	ar_identifier *id = (ar_identifier *) ccl_iterator_next_element (pi);
	ar_ca_add_preemptible (ca, id);
      }
    ccl_iterator_delete (pi);
  }
  CA_DBG_END_TIMER ();
}

static void
s_translate_bucket (ar_identifier *id, ccl_list *events, ccl_list *probas,
		    void *clientdata)
{
  ar_ca *ca = clientdata;
  
  ar_ca_add_bucket (ca, id, events, probas);
}

static void
s_translate_buckets (ar_node *n, ar_ca *ca)
{
  CA_DBG_START_TIMER (("translate buckets"));
  ar_node_map_buckets (n, s_translate_bucket, ca);
  CA_DBG_END_TIMER ();
}

static void
s_translate_priorities (ar_node *n, ar_ca *ca)
{
  ar_identifier_iterator *ii = ar_node_get_events_with_priorities (n);
  
  CA_DBG_START_TIMER (("translate priorities"));  
  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      int pl = ar_node_get_priority (n, id);
      ar_ca_add_priority (ca, id, pl);
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);
  CA_DBG_END_TIMER ();
}

ar_ca *
ar_compute_constraint_automaton_semantics (ar_node *node)
{
  int err = 0;
  ar_node *n;
  ar_ca *result;
  ccl_hash *eventtable;
  ccl_hash *domains = ccl_hash_create(NULL,NULL,NULL,NULL);
  ccl_hash *cache = ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *) 
				      ar_ca_expr_del_reference);
  
  if (CA_DBG_IS_ON)
    {
      ar_identifier *nodename = ar_node_get_name (node);
      char *s = ar_identifier_to_string (nodename);
      CA_DBG_START_TIMER (("constraint automaton for '%s'", s));
      ccl_string_delete (s);
      ar_identifier_del_reference (nodename);
    }

  CA_DBG_START_TIMER (("remove composite types"));  
  n = ar_node_remove_composite_types(node);
  CA_DBG_END_TIMER ();
  result = ar_ca_create(n);
  eventtable = s_make_events(n,result);
  ccl_try (abstract_type_exception)
    {
      s_make_variables(n,result,domains, cache);
      s_translate_assertions(n,result,domains, cache);
      s_translate_initial_states(n,result,domains, cache);
      s_translate_transitions(n,result,eventtable,domains, cache);
      s_translate_parameters_and_laws (n, result, domains, cache);
      s_translate_observers (n, result, domains, cache);
      s_translate_preemptibles (n, result);
      s_translate_buckets (n, result);
      s_translate_priorities (n, result);
    }
  ccl_catch
    {
      err = 1;
    }
  ccl_end_try;
  
  ccl_hash_delete(eventtable);
  ccl_hash_delete(domains);
  ccl_hash_delete(cache);
  ar_node_del_reference(n);

  CA_DBG_END_TIMER ();

  if (err)
    {
      ar_ca_del_reference (result);
      ccl_rethrow ();
    }
  
  return result;
}
