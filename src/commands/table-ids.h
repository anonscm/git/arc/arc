/*
 * table-ids.h -- Shared identifiers for tables of objects.
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TABLE_IDS_H
# define TABLE_IDS_H

# define TABLE_CONSTANTS "constants"
# define TABLE_DOMAINS "domains"
# define TABLE_SIGNATURES "signatures"
# define TABLE_NODES "nodes"
# define TABLE_RELATIONS "relations"
# define TABLE_COMMANDS "commands"

#endif /* ! TABLE_IDS_H */
