/*
 * target-reduction.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <deps/depgraph.h>
#include "ar-semantics.h"
#include "ar-ca-semantics.h"
#include "ar-ca-rewriters.h"
#include "ar-interp-altarica.h"
#include "parser/altarica-input.h"
#include "ar-model.h"
#include "ar-constraint-automaton.h"
#include "allcmds.h"
#include "allcmds-p.h"
#include "validate.h"

struct option {
  enum { DG_BEFORE = 0, 
	 GOAL_BEFORE,
	 FDEP_BEFORE, 
	 FDEP_DOT_BEFORE, 

	 DG_AFTER, 
	 GOAL_AFTER,
	 FDEP_AFTER, 
	 FDEP_DOT_AFTER, 

	 VALIDATE, 
	 LAST_OPTION } id;
  char *optname;
  char *value;
};

			/* --------------- */

static int
s_parse_arguments (struct option *options, int argc, 
		   arc_command_parameter *argv, 
		   char **p_nodename, char **p_formula);

static ar_ca *
s_compute_ca_and_target (ar_ca *ca, const char *formula, ar_ca_expr **p_target);

static void
s_log_listener_to_file (ccl_log_type type, const char  *msg, void *data);

static FILE *  
s_push_redirect_to_file (const char *filename);

static void
s_pop_redirect (FILE *out);

static void
s_display_functional_dependencies (ccl_log_type log, ar_ca *ca, int as_dot);

#define REDIRECT_WITH_OPT(opt, action)					\
  do {									\
    if (OPTIONS[opt].value != NULL)					\
      {									\
	FILE *out_ = s_push_redirect_to_file (OPTIONS[opt].value);	\
	action;								\
	s_pop_redirect (out_);						\
      }									\
  } while (0)

			/* --------------- */

ARC_DEFINE_COMMAND (target_reduction_command)
{
  struct option OPTIONS[] = { 
    { DG_BEFORE, "depgraph-before", NULL },
    { GOAL_BEFORE, "goal-before", NULL },
    { FDEP_BEFORE, "fdep-before", NULL },
    { FDEP_DOT_BEFORE, "fdep-dot-before", NULL },

    { DG_AFTER, "depgraph-after", NULL },
    { GOAL_AFTER, "goal-after", NULL },
    { FDEP_AFTER, "fdep-after", NULL },
    { FDEP_DOT_AFTER, "fdep-dot-after", NULL },
    { VALIDATE, "validate", NULL },

    { LAST_OPTION, NULL, NULL }
  };
  char *nodename = NULL; 
  char *formula = NULL; 
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  if (! s_parse_arguments (OPTIONS, argc, argv, &nodename, &formula))
    retval = ARC_CMD_ERROR;
  else
    {
      ar_ca *src_ca = arc_command_get_ca (nodename);

      if (src_ca == NULL)
	{
	  ccl_error ("unknown node %s\n", nodename);
	  retval = ARC_CMD_ERROR;
	}
      else
	{
	  const fdset *fds = ar_ca_get_fdset (src_ca);
	  ar_ca_expr *target = NULL;
	  ar_ca *ca =  s_compute_ca_and_target (src_ca, formula, &target);

	  if (ca != NULL)
	    {
	      depgraph *G;

	      REDIRECT_WITH_OPT (DG_BEFORE, 
				 {
				   G = depgraph_compute (src_ca, 0);
				   depgraph_log (CCL_LOG_DISPLAY, G);
				   depgraph_delete (G);
				 });
	      
	      REDIRECT_WITH_OPT (GOAL_BEFORE, 
				 ar_ca_expr_log_gen (CCL_LOG_DISPLAY, target, 
						     ".", "''"));

	      REDIRECT_WITH_OPT (FDEP_BEFORE, 
				 s_display_functional_dependencies 
				 (CCL_LOG_DISPLAY, ca, 0));

	      REDIRECT_WITH_OPT (FDEP_DOT_BEFORE, 
				 s_display_functional_dependencies 
				 (CCL_LOG_DISPLAY, ca, 1));

	      {
		ar_ca_expr *tmp = ar_ca_rewrite_expr_with_fd (fds, target, 1);
		ar_ca_expr_del_reference (target);
		target = tmp;
	      }
	      ar_ca_trim_to_reachables_from_formula (ca, target);
	      REDIRECT_WITH_OPT (DG_AFTER, 
				 do 
				   {
				     G = depgraph_compute (ca, 0);
				     depgraph_log (CCL_LOG_DISPLAY, G);
				     depgraph_delete (G);
				   } while (0));

	      REDIRECT_WITH_OPT (GOAL_AFTER, 
				 ar_ca_expr_log_gen (CCL_LOG_DISPLAY, target, 
						     ".", "''"));

	      REDIRECT_WITH_OPT (FDEP_AFTER, 
				 s_display_functional_dependencies 
				 (CCL_LOG_DISPLAY, ca, 0));

	      REDIRECT_WITH_OPT (FDEP_DOT_AFTER, 
				 s_display_functional_dependencies 
				 (CCL_LOG_DISPLAY, ca, 1));


	      ar_ca_log (CCL_LOG_DISPLAY, ca);

	      REDIRECT_WITH_OPT (VALIDATE,
				 ar_validate_ca (CCL_LOG_DISPLAY, ca));
	      ar_ca_expr_del_reference (target);
	      ar_ca_del_reference (ca);
	    }
	  ccl_zdelete (ar_ca_del_reference, src_ca);
	}
    }

  return retval;
}

			/* --------------- */

static int
s_parse_arguments (struct option *options, int argc, 
		   arc_command_parameter *argv, 
		   char **p_nodename, char **p_formula)
{
  int i;

  if (argc < 2)
    {
      ccl_error ("error: wrong # of arguments.\n");
      return 0;
    }

  for (i = 0; i < argc; i++)
    {      
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument (#%d) type.\n", i + 1);
	  return 0;
	}

      if (*arg == '-')
	{
	  struct option *o;

	  if (arg[1] == '-')
	    {
	      for (o = options; o->optname; o++)
		{
		  if (strncmp (arg + 2, o->optname,strlen (o->optname)) == 0)
		    {
		      if ((o->value = strrchr (argv[i]->u.string_value, '=')))
			o->value++;
		      
		      break;
		    }
		}
	      if (o->optname == NULL)
		{
		  ccl_error ("error: unknown option '%s'.\n", arg);
		  return 0;
		}
	    }
	  else
	    {
	      ccl_error ("error: unknown option '%s'.\n", arg);
	      return 0;
	    }
	}
      else if (*p_nodename == NULL) 
	{
	  *p_nodename = arg;
	}
      else if (*p_formula == NULL)
	{
	  *p_formula =  arg;
	}
      else 
	{
	  ccl_error ("error: wrong argument '%s'. "
		     "Don't you forget double-quotes ?\n",
		     arg);
	  return 0;
	}
    }

  if (*p_nodename == NULL)
    {
      ccl_error ("error: no node name specified\n");
      return 0;
    }

  if (*p_formula == NULL)
    {
      ccl_error ("error: no target formula specified\n");
      return 0;
    }

  return 1;
}

			/* --------------- */

static ar_ca *
s_compute_ca_and_target (ar_ca *src_ca, const char *formula, 
			 ar_ca_expr **p_target)
{
  ar_ca_exprman *man;
  ar_node *node;
  ar_context *ctx;
  ar_expr *goal;
  ar_expr *flat_goal;
  ar_ca *result;
  char *tmp = ccl_string_format_new ("@%s@", formula);
  altarica_tree *t = altarica_read_string (tmp, "command line", AR_INPUT_AEXPR, 
					   NULL, NULL);
  ccl_string_delete (tmp);
  
  if (t == NULL)
    return NULL;

  result = ar_ca_duplicate (ar_ca_get_reduced (src_ca));
  node = ar_ca_get_node (result);
  man = ar_ca_get_expression_manager (result);
  ctx = ar_node_get_context (node);
  goal = NULL;
  flat_goal = NULL;
  
  ccl_try (exception)
    {
      ar_expr *flat_goal;
      
      goal = ar_interp_boolean_expression (t->child, ctx,
					   ar_interp_acheck_member_access);
      flat_goal = ar_expr_remove_composite_slots (goal, ctx);
      *p_target = ar_model_expr_to_ca_expr (flat_goal, man, NULL);
    }
  ccl_no_catch;
  ccl_zdelete (ar_expr_del_reference, goal);
  ccl_zdelete (ar_expr_del_reference, flat_goal);
  ar_context_del_reference (ctx);
  ar_node_del_reference (node);
  ar_ca_exprman_del_reference (man);
  ccl_parse_tree_delete_tree (t);

  if (*p_target == NULL)
    {
      ar_ca_del_reference (result);
      result = NULL;
    }

  return result;
}

			/* --------------- */

static void
s_log_listener_to_file (ccl_log_type type, const char  *msg, void *data)
{
  FILE *out = data;

  fprintf (out, "%s", msg);
}

			/* --------------- */

static FILE * 
s_push_redirect_to_file (const char *filename)
{
  FILE *out;

  if (strlen (filename) == 0)
    out = stdout;
  else
    out = fopen (filename, "w");
  
  if (out != NULL)
    ccl_log_push_redirection (CCL_LOG_DISPLAY, s_log_listener_to_file, out);
  else
    {
      ccl_error ("error: can't open file '%s'.\n", filename);
    }

  return out;
}

			/* --------------- */

static void
s_pop_redirect (FILE *out)
{
  if (out != NULL)
    {
      fflush (out);
      ccl_log_pop_redirection (CCL_LOG_DISPLAY);
      if (out != stdout)
	fclose (out);
    }
}

			/* --------------- */

static void
s_display_functional_dependencies (ccl_log_type log, ar_ca *ca, int as_dot)
{
  const fdset *fds = ar_ca_get_fdset (ca);
  ccl_list *fvars = fdset_get_ordered_variables (fds, 0);
  int nb_c = ar_ca_get_number_of_assertions (ca);
  int nb_l_ass = fdset_get_number_of_functions (fds);

  if (as_dot)
    ccl_log (log, "digraph funct_dep {\n");

  if (nb_l_ass > 0)
    {
      ccl_log (log, "/* \n * FUNCTIONAL DEPENDENCIES\n */\n");
      while (!ccl_list_is_empty (fvars))
	{
	  ar_ca_expr *var = ccl_list_take_first (fvars);
	  ccl_list *deps = ar_ca_expr_get_variables (var, NULL);

	  if (as_dot)
	    {
	      ccl_log (log, "N%p[label=\"", var);
	      ar_ca_expr_log (log, var);
	      ccl_log (log, "\"];\n");
	    }

	  while (! ccl_list_is_empty (deps))
	    {
	      ar_ca_expr *v = ccl_list_take_first (deps);

	      if (as_dot)
		{
		  ccl_log (log, "N%p[label=\"", v);
		  ar_ca_expr_log (log, v);
		  ccl_log (log, "\"];\n");
		  ccl_log (log, "N%p -> N%p;\n", var, v);
		}
	      else
		{
		  ar_ca_expr_log (log, var);
		  ccl_log (log, " -> ");
		  ar_ca_expr_log (log, v);
		  ccl_log (log, "\n");
		}
	    }
	  ccl_list_delete (deps);
	}
    }
  ccl_list_delete (fvars);

  if (nb_c > 0)
    { 
      const ccl_pair *p;
      const ccl_list *assertions = ar_ca_get_assertions (ca);

      ccl_log (log, "/* \n * REMAINING CONSTRAINTS\n */\n");
      for (p = FIRST (assertions); p; p = CDR (p))
	{
	  ccl_pair *p1;
	  ar_ca_expr *c = CAR (p);
	  ar_ca_expr *a = ar_ca_rewrite_expr_with_fd (fds, c, 1);

	  if (! ar_ca_expr_is_boolean_constant (a))
	    {
	      ccl_list *vars = ar_ca_expr_get_variables (a, NULL);
	  
	      if (as_dot)
		{
		  for (p1 = FIRST (vars); p1; p1 = CDR (p1))
		    {
		      ccl_log (log, "N%p[label=\"", CAR (p1));
		      ar_ca_expr_log (log, CAR (p1));
		      ccl_log (log, "\"];\n");
		    }
		}
	      
	      for (p1 = FIRST (vars); p1; p1 = CDR (p1))
		{
		  ccl_pair *p2;
		  
		  for (p2 = CDR (p1); p2; p2 = CDR (p2))
		    {
		      if (as_dot)
			{
			  ccl_log (log, "N%p -> N%p;\n", CAR (p1), CAR (p2));
			  ccl_log (log, "N%p -> N%p;\n", CAR (p2), CAR (p1));
			}
		      else
			{
			  ar_ca_expr_log (log, CAR (p1));
			  ccl_log (log, " <-> ");
			  ar_ca_expr_log (log, CAR (p2));
			  ccl_log (log, "\n");
			}		  
		    }
		}
	    }
	  ar_ca_expr_del_reference (a);
	}
    }

  if (as_dot)
    ccl_log (log, "}\n");
}
