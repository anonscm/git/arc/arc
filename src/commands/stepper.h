/*
 * stepper.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef STEPPER_H
# define STEPPER_H

# include "arc.h"

extern int 
stepper_cmd_init (ccl_config_table *prefs);

extern int 
stepper_cmd_terminate (void);

#endif /* ! STEPPER_H */
