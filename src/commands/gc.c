/*
 * gc.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-pool.h>
#include "ar-dd.h"
#include "allcmds.h"
#include "allcmds-p.h"


ARC_DEFINE_COMMAND (gc_command)
{
  *result = NULL;
  
  ar_dd_garbage_collection (DDM, 1);
  ar_dd_display_statistics_about_decision_diagrams(DDM, CCL_LOG_DISPLAY);
  ccl_pools_display_info (CCL_LOG_DEBUG);

  return ARC_CMD_SUCCESS;
}
