/*
 * dircmd.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <sys/param.h>
#include <unistd.h>
#include "arc.h"
#include "arc-shell-preferences.h"
#include "allcmds.h"

static int 
s_display_current_directory (void)
{
  int result = 1;
  char *path = ccl_new_array (char, MAXPATHLEN + 1);
  
  if (path == NULL)
    return 0;

  if (getwd (path) == NULL)
    {
      ccl_error ("can't current working directory.\n %s.\n", path);
      result = 0;
    }
  else
    {
      ccl_display ("%s\n", path);
    }
  ccl_delete (path);

  return result;
}

ARC_DEFINE_COMMAND (cd_command)
{
  arc_command_retval retval = ARC_CMD_ERROR;

    
  if (argc != 1)
    ccl_error ("wrong # of arguments (%d)\n", argc);
  else if (argv[0]->type != ARC_CMDP_STRING)
    ccl_error ("wrong argument type\n");
  else if (chdir (argv[0]->u.string_value) < 0)
    ccl_error ("can't change directory to '%s'\n", argv[0]->u.string_value);
  else 
    {
      if (ccl_config_table_get_boolean (ARC_PREFERENCES, ARC_SHELL_VERBOSE))
	{
	  ccl_display ("current directory changed to: ");
	  s_display_current_directory ();	  
	}
      retval = ARC_CMD_SUCCESS;
    }

  return retval;
}

ARC_DEFINE_COMMAND (pwd_command)
{
  arc_command_retval retval = ARC_CMD_SUCCESS;
  
  if (s_display_current_directory ())
    retval = ARC_CMD_SUCCESS;
  else
    retval = ARC_CMD_ERROR;

  return retval;
}
