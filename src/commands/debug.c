/*
 * debug.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <ccl/ccl-memory.h>
#include <ccl/ccl-pool.h>
#include "allcmds.h"

typedef void debug_info_proc (void);

struct debug_sub_command
{
  const char *name;
  debug_info_proc *command;
};

static void
s_debug_memory (void);

static void
s_debug_pools (void);


static const struct debug_sub_command DEBUG_SUB_COMMANDS[] = {
  { "memory", s_debug_memory },
  { "pools", s_debug_pools },
  { NULL, NULL }
};

ARC_DEFINE_COMMAND (debug_command)
{
  int i;
  const struct debug_sub_command *sc;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  if (argc == 0)
    {
      ccl_warning ("no argument is given. try:");
      for (sc = DEBUG_SUB_COMMANDS; sc->name != NULL; sc++)
	ccl_warning (" %s", sc->name);
      ccl_warning ("\n");
    }
  else 
    {
      *result = NULL;  
      for (i = 0; i < argc; i++)
	{
	  if (argv[i]->type == ARC_CMDP_STRING)
	    {
	      const char *subc = argv[i]->u.string_value;
	      for (sc = DEBUG_SUB_COMMANDS; sc->name != NULL; sc++)
		{
		  if (ccl_string_equals (subc, sc->name))
		    {
		      sc->command ();
		      break;
		    }
		}
	      if (sc == NULL)
		ccl_warning ("unknown sub-command '%s'.\n", subc);
	    }
	  else
	    {
	      ccl_error ("wrong argument type (#%d).\n", i);
	    }
	}
    }
  return retval;
}

static void
s_debug_memory (void)
{
  ccl_debug_log_statistics_per_module ();
}

static void
s_debug_pools (void)
{
  ccl_pools_display_info (CCL_LOG_DISPLAY);
}
