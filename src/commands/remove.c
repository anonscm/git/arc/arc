/*
 * remove.c -- Delete resources allocated to some objects.
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "table-ids.h"

struct table
{
  const char *name;
  int (*has) (ar_identifier *id);
  void (*remove) (ar_identifier *id);
  ccl_list *(*get_all) (void);
};

			/* --------------- */


static const struct table TABLES[] =
  {
    { TABLE_CONSTANTS, ar_model_has_parameter, ar_model_remove_parameter,
      ar_model_get_parameters },
    { TABLE_DOMAINS, ar_model_has_type, ar_model_remove_type,
      ar_model_get_types },
    { TABLE_NODES, ar_model_has_node, ar_model_remove_node,
      ar_model_get_nodes },
    { TABLE_SIGNATURES, ar_model_has_signature, ar_model_remove_signature,
      ar_model_get_signatures },
    { TABLE_RELATIONS, ar_mec5_has_relation, ar_mec5_remove_relation,
      ar_mec5_get_relations },
    { NULL, NULL, NULL }
  };

			/* --------------- */

static void
s_remove_all (const struct table *table)
{
  ccl_list *ids = table->get_all ();

  while (! ccl_list_is_empty (ids))
    {
      ar_identifier *id = ccl_list_take_first (ids);
      table->remove (id);
      ar_identifier_del_reference (id);
    }
  ccl_list_delete (ids);
}

			/* --------------- */

ARC_DEFINE_COMMAND (remove_command)
{
  int i;
  int dict_index;
  const char *dict;
  const struct table *T = NULL;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (argc < 1)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  for (i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument (%d) type.\n", i);
	  
	  return ARC_CMD_ERROR;
	}
    }

  dict = argv[0]->u.string_value;
  if (ccl_string_equals (dict, "all"))
    {
      for (T = TABLES; T->name != NULL; T++)
	s_remove_all (T);
      return ARC_CMD_SUCCESS;
    }

  dict_index = -1;
  for (i = 0, T = TABLES; T->name != NULL; T++, i++)
    {
      if (ccl_string_equals (T->name, dict))
	{
	  dict_index = i;
	  break;
	}
      if (ccl_string_has_prefix (dict, T->name) != NULL)
	{
	  if (dict_index >= 0)
	    {
	      ccl_error ("error: ambiguous dictionary '%s'.\n", dict);
	      return ARC_CMD_ERROR;
	    }
	  dict_index = i;
	}	
    }
  
  if (dict_index < 0)
    {
      ccl_error ("error: unknown dictionary '%s'.\n", dict);

      return ARC_CMD_ERROR;
    }
  
  T = &TABLES[dict_index];
  for (i = 1; i < argc; i++)
    {
      char *arg = argv[i]->u.string_value;
      if (ccl_string_equals (arg, "all"))
	{
	  s_remove_all (T);
	  return ARC_CMD_SUCCESS;
	}
    }

  for (i = 1; i < argc; i++)
    {
      char *arg = argv[i]->u.string_value;
      ar_identifier *id = ar_identifier_create (arg);
      
      if (T->has != NULL && T->has (id))
	{
	  ccl_assert (T->remove != NULL);
	  T->remove (id);
	}
      else
	{
	  ccl_error ("error: unknown identifier '%s'.\n", arg);
	  retval = ARC_CMD_ERROR;
	}
      
      ar_identifier_del_reference (id);
    }

  return retval;
}
