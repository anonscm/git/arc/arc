/*
 * ts-marks.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ts/ts.h>
#include "allcmds-p.h"
#include "allcmds.h"

static void
s_display_properties (const char *title, const char *emptymessage,
		      ar_ts *ts, ar_identifier_iterator *(*get_marks) (ar_ts *),
		      ar_ts_mark *(*get_mark) (ar_ts *, ar_identifier *))
{
  ar_identifier_iterator *ii = get_marks (ts);
  if (ccl_iterator_has_more_elements (ii)) 
    {
      ccl_display (title);
      while (ccl_iterator_has_more_elements (ii)) 
	{
	  ar_identifier *name = ccl_iterator_next_element (ii);
	  ar_ts_mark *m = get_mark (ts, name);
	  
	  ccl_display (" * ");
	  ar_identifier_log (CCL_LOG_DISPLAY, name);
	  ccl_display (" = %d\n", ar_ts_mark_get_size (m));
	  
	  ar_ts_mark_del_reference (m);
	  ar_identifier_del_reference (name);
	}      
    }
  else
    {
      ccl_display (emptymessage);
    }
  ccl_iterator_delete (ii);
}

			/* --------------- */

static void 
s_display_ts_properties (ar_ts *ts)
{
  ar_identifier *name = ar_ts_get_name (ts);

  ccl_display ("/**\n"
	       " * Cardinality of the properties computed for the transition "
	       "system\n"
	       " * of node '");
  ar_identifier_log (CCL_LOG_DISPLAY, name);
  ccl_display ("'\n\n");

  s_display_properties (" * State properties :\n"
			" * ------------------\n",
			" * No property for states.\n",
			ts, ar_ts_get_state_marks, ar_ts_get_state_mark);
  ccl_display (" *\n");      
  s_display_properties (" * Transition properties :\n"
			" * -----------------------\n",
			" * No property for transitions.\n",
			ts, ar_ts_get_trans_marks, ar_ts_get_trans_mark);
  ccl_display (" */\n");      
  
  ar_identifier_del_reference (name);
}

			/* --------------- */

ARC_DEFINE_COMMAND (ts_marks_command)
{
  arc_command_retval retval = ARC_CMD_ERROR;

  *result = NULL;
  if (argc != 1)
    ccl_error ("error: wrong # of arguments (%d).\n", argc);
  else if (argv[0]->type != ARC_CMDP_STRING)
    ccl_error ("error: bad argument type.\n");
  else
    {
      ar_ts *ts = arc_command_get_ts (argv[0]->u.string_value);
      if (ts != NULL)
	{
	  s_display_ts_properties (ts);
	  ar_ts_del_reference (ts);
	  retval = ARC_CMD_SUCCESS;
	}
    }

  return retval;
}

