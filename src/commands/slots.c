/*
 * slots.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-attributes.h"
#include "ar-model.h"
#include "allcmds.h"

static ccl_list *
s_flags_to_string (uint32_t flags);

ARC_DEFINE_COMMAND (slots_command)
{
  int i;
  ar_identifier *id;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  for (i = 0; i < argc; i++)
    {
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type != ARC_CMDP_STRING)
	continue;
      
      id = ar_identifier_create (arg);

      if (ar_model_has_node (id))
	{
	  ar_node *n = ar_model_get_node (id);
	  ccl_display ("slots for node context '%s' :\n", arg);

	  if (ar_node_get_template (n) != NULL)
	    {
	      ccl_display (" this node is a template.\n");
	    }
	  else
	    {
	      ar_context *ctx = ar_node_get_context (n);
	      ar_context_log_slots (ctx, 0xFFFFFFFF, s_flags_to_string);
	      ar_context_del_reference (ctx);
	      ccl_display ("\n");
	    }
	  ar_node_del_reference (n);
	}
      else
	{
	  ccl_warning ("unknown node '%s'.\n",arg);
	}
      ar_identifier_del_reference (id);
    }

  return retval;
}

			/* --------------- */

static ccl_list *
s_flags_to_string (uint32_t flags)
{
  ccl_list *result = ccl_list_create ();
  
  if ((flags & AR_SLOT_FLAG_STATE_VAR))
    ccl_list_add (result, "state");

  if ((flags & AR_SLOT_FLAG_FLOW_VAR))
    ccl_list_add (result, "flow");

  if ((flags & AR_SLOT_FLAG_NODE))
    ccl_list_add (result, "node");

  if ((flags & AR_SLOT_FLAG_EVENT))
    ccl_list_add (result, "event");

  if ((flags & AR_SLOT_FLAG_PARAMETER))
    ccl_list_add (result, "parameter");

  if ((flags & AR_SLOT_FLAG_IN))
    ccl_list_add (result, "in");

  if ((flags & AR_SLOT_FLAG_OUT))
    ccl_list_add (result, "out");

  if ((flags & AR_SLOT_FLAG_PUBLIC))
    ccl_list_add (result, "public");

  if ((flags & AR_SLOT_FLAG_PARENT))
    ccl_list_add (result, "parent");

  if ((flags & AR_SLOT_FLAG_PRIVATE))
    ccl_list_add (result, "private");

  return result;
}

			/* --------------- */

