/*
 * sat.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "parser/altarica-input.h"
#include "ar-ca-semantics.h"
#include "ar-ca-rewriters.h"
#include "ar-interp-altarica.h"
#include "ar-interp-altarica-exception.h"
#include "ar-node.h"
#include "sat/ca2sat.h"
#include "allcmds-p.h"
#include "allcmds.h"
#include "sat/glucose.h"

static int
s_formula_to_cnf (ar_ca *ca, const char *F);

static int
s_solve_formula (ar_ca *ca, const char *F);

static int
s_solve_assertion (ar_ca *ca);

static int
s_solve_steps (ar_ca *ca, int N, ar_ca_expr *G);

static int
s_reach (ar_ca *ca, const char *strgoal, int N);

ARC_DEFINE_COMMAND (sat_command)
{
  int i;
  ar_ca *ca;
  arc_command_retval retval = ARC_CMD_ERROR;

  *result = NULL;
  if (argc < 2)
    {
      ccl_error ("error: wrong # of arguments (%d).\n", argc);
      goto end;
    }

  for (i = 0; i < argc; i++)
    if (argv[i]->type != ARC_CMDP_STRING )
      {
	ccl_error ("error: bad argument type (%d).\n", i);
	goto end;
      }

  ca = arc_command_get_ca (argv[0]->u.string_value);
  if (ca != NULL)
    {
      const char *sc = argv[1]->u.string_value;
      if (ccl_string_has_prefix (sc, "cnf") != NULL)
	{
	  if (argc != 3)
	    ccl_error ("error: wrong # of arguments (%d).\n", argc);
	  else
	    {
	      const char *sF = argv[2]->u.string_value;
	      if (s_formula_to_cnf (ca, sF))
		retval = ARC_CMD_SUCCESS;
	    }
	}
      else if (ccl_string_has_prefix (sc, "solve") != NULL)
	{
	  if (argc != 3)
	    ccl_error ("error: wrong # of arguments (%d).\n", argc);
	  else
	    {
	      const char *sF = argv[2]->u.string_value;
	      if (s_solve_formula (ca, sF))
		retval = ARC_CMD_SUCCESS;
	    }
	}
      else if (ccl_string_has_prefix (sc, "assertions") != NULL)
	{
	  if (s_solve_assertion (ca))
	    retval = ARC_CMD_SUCCESS;
	}
      else if (ccl_string_has_prefix (sc, "steps") != NULL)
	{
	  if (argc != 3)
	    ccl_error ("error: wrong # of arguments (%d).\n", argc);
	  else
	    {
	      int N = atoi (argv[2]->u.string_value);
	      if (N < 0)
		ccl_error ("error: wrong argument (%s).\n", 
			   argv[2]->u.string_value);
	      if (s_solve_steps (ca, N, NULL))
		retval = ARC_CMD_SUCCESS;
	    }
	}
      else if (ccl_string_has_prefix (sc, "reachables") != NULL)
	{
	  if (argc != 4)
	    ccl_error ("error: wrong # of arguments (%d).\n", argc);
	  else
	    {
	      char *strgoal = argv[2]->u.string_value;
	      int N = atoi (argv[3]->u.string_value);
	      
	      if (N < 0)
		ccl_error ("error: wrong argument (%s).\n", 
			   argv[3]->u.string_value);
	      
	      if (s_reach (ca, strgoal, N))
		retval = ARC_CMD_SUCCESS;
	    }
	}
      else
	{
	  ccl_error ("error: unknown sub-command '%s'.\n", sc);
	  retval = ARC_CMD_ERROR;
	}
      
      ar_ca_del_reference (ca);
    }

 end:
  
  return retval;
}

static ar_ca_expr *
s_get_formula (ar_ca *ca, const char *F)
{
  ar_ca_expr *result = NULL;
  char *tmp = ccl_string_format_new ("@%s@", F);
  altarica_tree *t = altarica_read_string (tmp, "command line", 
					   AR_INPUT_AEXPR, NULL, NULL);
  ccl_string_delete (tmp);
	  
  if (t != NULL)
    {
      ar_node *node = ar_ca_get_node (ca);
      ar_context *ctx = ar_node_get_context (node);
      ar_ca_exprman *man = ar_ca_get_expression_manager (ca);
      ar_expr *aux1 = NULL;
      ar_expr *aux2 = NULL;
      
      ccl_try (exception)
        {
	  aux1 = ar_interp_boolean_expression (t->child, ctx,
					       ar_interp_acheck_member_access);
	  aux2 = ar_expr_remove_composite_slots (aux1, ctx);


	  result = ar_model_expr_to_ca_expr (aux2, man, NULL);
        }
      ccl_no_catch;

      ccl_zdelete (ar_expr_del_reference, aux1);	  
      ccl_zdelete (ar_expr_del_reference, aux2);
      ar_context_del_reference (ctx);
      ar_node_del_reference (node);
      ar_ca_exprman_del_reference (man);
      ccl_parse_tree_delete_tree (t);
    }
  return result;
}

static void
s_display_add_clause (const bf_literal *lits, int nb_lits, void *data)
{
  int i;

  for (i = 0; i < nb_lits; i++)
    ccl_display (bf_sign (lits[i])? "-%d " : "%d ", 
		 bf_lit2var (lits[i]));
  ccl_display ("\n");
}

static bf_index 
s_fresh_variable_allocator (void *data)
{
  bf_index *p_last_index = data;

  return (*p_last_index)++;
}

static int
s_formula_to_cnf (ar_ca *ca, const char *formula)
{
  static bf_index last_index = 1;
  bf_formula *bf;  
  bf_manager *bm;
  ca2sat_translator *T;
  varset *vs;
  ar_ca_expr *F = s_get_formula (ca, formula);

  if (F == NULL)
    return 0;

  T = ca2sat_translator_create (ca, s_fresh_variable_allocator, &last_index);
  vs = ca2sat_get_varset (T);
  bf = ca2sat_translate_boolean_expr (T, vs, F);
  bm = ca2sat_get_bf_manager (T);
  
  bf_log_formula (CCL_LOG_DISPLAY, bm, bf);
  ccl_display ("\n");
  bf_to_cnf (bm, bf, s_display_add_clause, NULL, 1);
  ccl_display ("\n");
  bf_unref (bf);
  ar_ca_expr_del_reference (F);
  varset_unref (vs);
  ca2sat_translator_destroy (T);

  return 1;
}

static size_t nb_clauses = 0;

static void
s_add_clause (const bf_literal *variables, int nb_variables, void *data)
{
  glucose_solver *S = data;

  if (nb_variables == 0)
    glucose_solver_add_clause_0 (S);
  else if (nb_variables == 1)
    glucose_solver_add_clause_1 (S, variables[0]);
  else if (nb_variables == 2)
    glucose_solver_add_clause_2 (S, variables[0], variables[1]);
  else if (nb_variables == 3)
    glucose_solver_add_clause_3 (S, variables[0], variables[1], variables[2]);
  else 
    glucose_solver_add_clause (S, variables, nb_variables);  
  //s_display_add_clause (variables, nb_variables, data);
  nb_clauses++;
  if (nb_clauses % 1000000 == 0)
    ccl_display ("%f Mclauses...\n", (double) nb_clauses / 1e6);
}

static bf_index 
s_glucose_var_allocator (void *data)
{
  return glucose_solver_new_var (data);
}


static int
s_solve_ca_exprs (ar_ca *ca, ar_ca_expr **formulas, int nb_formulas)
{
  int i;
  glucose_solver *S = glucose_solver_create ();
  ca2sat_translator *T = 
    ca2sat_translator_create (ca, s_glucose_var_allocator, S);
  varset *vs = ca2sat_get_varset (T);
  const char *outres = "sat";
  bf_manager *bm = ca2sat_get_bf_manager (T);

  for (i = 0; i < nb_formulas; i++) 
    {      
      bf_formula *bf = ca2sat_translate_boolean_expr (T, vs, formulas[i]);

      if (ccl_debug_is_on)
	{
	  bf_log_formula (CCL_LOG_DEBUG, bm, bf);
	  ccl_debug ("\n");
	}

      bf_to_cnf (bm, bf, s_add_clause, S, 1);
      bf_unref (bf);
    }

  {
    bf_formula *bf = ca2sat_get_domain_constraint (T, vs);
    if (ccl_debug_is_on)
      {
	bf_log_formula (CCL_LOG_DEBUG, bm, bf);
	ccl_debug ("\n");
      }

    bf_to_cnf (bm, bf, s_add_clause, S, 1);
    bf_unref (bf);
  }

  if (! glucose_solver_solve (S))
    outres = "unsat";
  ccl_display ("%s\n", outres);

  glucose_solver_destroy (S);
  varset_unref (vs);
  ca2sat_translator_destroy (T);

  return 1;
}

static int
s_solve_formula (ar_ca *ca, const char *formula)
{
  int result;
  ar_ca_expr *F = s_get_formula (ca, formula);

  if (F == NULL)
    result = 0;
  else 
    {
      result = s_solve_ca_exprs (ca, &F, 1);
      ar_ca_expr_del_reference (F);
    }

  return result;
}

static int
s_solve_assertion (ar_ca *ca)
{
  int nb_assertions;
  ar_ca_expr **assertions = (ar_ca_expr **)
    ccl_list_to_array (ar_ca_get_assertions (ca), &nb_assertions);
  int result = s_solve_ca_exprs (ca, assertions, nb_assertions);
  ccl_delete (assertions);
  
  return result;
}

#define WITH_ASSERTIONS 1


static int
s_solve_steps (ar_ca *ca, int N, ar_ca_expr *G)
{
  int i;
  glucose_solver *S = glucose_solver_create ();
  ca2sat_translator *T = 
    ca2sat_translator_create (ca, s_glucose_var_allocator, S);
  bf_manager *bm = ca2sat_get_bf_manager (T);
  varset **vs = ccl_new_array (varset *, N + 1);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  bf_formula *F;
  int result;

  for (i = 0; i <= N; i++)
    vs[i] = ca2sat_get_varset (T);

  nb_clauses = 0;
  F = ca2sat_get_init (T, vs[0], WITH_ASSERTIONS);
  bf_to_cnf (bm, F, s_add_clause, S, 1);
  bf_unref (F);
  for (i = 0; i < N; i++)
    {
      int j;
      
      ccl_debug ("step %d\n", i);

      F = 
	ca2sat_translate_transitions (T, trans, nb_trans, vs[i], vs[i + 1], 
				      WITH_ASSERTIONS, NULL);
      bf_to_cnf (bm, F, s_add_clause, S, 1);
      bf_unref (F);

      for (j = 0; j <= i; j++)
	{
	  F = ca2sat_translate_distinct_state (T, vs[j], vs[i+1]);
	  bf_to_cnf (bm, F, s_add_clause, S, 1);
	  bf_unref (F);
	}
    }

  if (G != NULL)
    {
      F = ca2sat_translate_boolean_expr (T, vs[N], G);
      bf_to_cnf (bm, F, s_add_clause, S, 1);
      bf_unref (F);
    }

  ccl_display ("%d clauses\n", nb_clauses);
  result = glucose_solver_solve (S);
  if (result)
    ccl_display ("sat\n");
  else
    ccl_display ("unsat\n");

  for (i = 0; i <= N; i++)
    varset_unref (vs[i]);
  ccl_delete (vs);
  glucose_solver_destroy (S);
  ca2sat_translator_destroy (T);

  return result;
}

static int 
s_bmc (ar_ca *ca, ar_ca_expr *G, int maxN)
{
  int i;
  int result = 0;

  for (i = 0; i < maxN && !result; i++)
    result = s_solve_steps (ca, i, G);

  return result;
}

static int
s_reach (ar_ca *srcca, const char *formula, int N)
{
  int result = 0;
  char *tmp = ccl_string_format_new ("@%s@", formula);
  altarica_tree *t = altarica_read_string (tmp, "command line", AR_INPUT_AEXPR, 
					   NULL, NULL);
  ccl_string_delete (tmp);

  if (t == NULL)
    return 0;

  CCL_DEBUG_START_TIMED_BLOCK (("compute reachability of %s", formula));
  {
    const fdset *fds = ar_ca_get_fdset (srcca);
    ar_ca *ca = ar_ca_duplicate (ar_ca_get_reduced (srcca));  
    ar_ca_exprman *man = ar_ca_get_expression_manager (ca);
    ar_node *node = ar_ca_get_node (ca);
    ar_context *ctx = ar_node_get_context (node);
    ar_expr *goal = NULL;
    ar_expr *flat_goal = NULL;
    
    ccl_try (exception)
      { 

	ar_ca_expr *tmp;
	ar_ca_expr *ca_goal;
	ccl_hash *Tdep = NULL;

	goal = ar_interp_boolean_expression (t->child, ctx,
					     ar_interp_acheck_member_access);
	flat_goal = ar_expr_remove_composite_slots (goal, ctx);
	tmp = ar_model_expr_to_ca_expr (flat_goal, man, NULL);
	
	ca_goal = ar_ca_rewrite_expr_with_fd (fds, tmp, 0);
      
	ar_expr_del_reference (flat_goal);
	ar_ca_expr_del_reference (tmp);
	
	ar_ca_trim_to_reachables_from_formula (ca, ca_goal);
  
	CCL_DEBUG_START_TIMED_BLOCK (("apply algorithm on reduced CA"));
	if (ccl_debug_is_on && Tdep)
	  ccl_debug ("nb trans = %d\n", ar_ca_get_number_of_transitions (ca));

	result = s_bmc (ca, ca_goal, N);
	if (result)
	  ccl_display("sat\n");
	else
	  ccl_display("unsat\n");
	CCL_DEBUG_END_TIMED_BLOCK ();
	ccl_zdelete (ccl_hash_delete, Tdep);
	ar_ca_expr_del_reference (ca_goal);
      }
    ccl_no_catch;

    ccl_zdelete (ar_expr_del_reference, flat_goal);
    ccl_zdelete (ar_expr_del_reference, goal);
    ar_context_del_reference (ctx);
    ar_node_del_reference (node);
    ar_ca_exprman_del_reference (man);
    ar_ca_del_reference (ca);
    
    ccl_parse_tree_delete_tree (t);
  }
  CCL_DEBUG_END_TIMED_BLOCK ();

  return result;
}

