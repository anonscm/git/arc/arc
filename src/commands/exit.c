/*
 * exit.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "arc-shell.h"
#include "arc-commands.h"

/*
ARC_DEFINE_COMMAND_HELP (exit_command) = {
  "exit",
  "arc, script, termination, quit",
  "Quit the ARC program.",
  "This command requests ARC for normal termination. Preferences are not saved when the program terminates. If you want keep your settings for next sessions use the 'set -save' commands.\n"
  "\n"
  "Syntax:\n"
  " exit\n"
  "  Terminates the ARC session.\n",
  NULL, NULL
};
*/
			/* --------------- */


ARC_DEFINE_COMMAND (exit_command)
{
  *result = NULL;
  arc_shell_exit ();

  return ARC_CMD_ERROR;
}

			/* --------------- */

