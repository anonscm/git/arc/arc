/*
 * validate.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "mec5/mec5.h"
#include "ar-semantics.h"
#include "allcmds.h"
#include "allcmds-p.h"
#include "validate.h"

#define CHECK_PROC_PROTO(_procname_) \
  void _procname_ (ccl_log_type log, ar_relsem *rs, ar_ca *ca, int use_reach,  \
		   ar_ca_exprman *eman, ar_ddm ddm)

typedef struct basic_checking_st
{
  const char *name;
  CHECK_PROC_PROTO ((*check));
} basic_checking;

			/* --------------- */

#define TAB " "

static CHECK_PROC_PROTO (s_check_usage_of_variables);

static CHECK_PROC_PROTO (s_check_initial_states);

static CHECK_PROC_PROTO (s_check_domains_in_configurations);

static CHECK_PROC_PROTO (s_check_domains_in_reachables);

static CHECK_PROC_PROTO (s_check_macro_transitions);

static const basic_checking CHECKLIST[] = {
  { "usage of variables", s_check_usage_of_variables },
  { "uniqueness of initial configuration", s_check_initial_states },
  { "coverage of domains / configurations", s_check_domains_in_configurations },
  { "coverage of domains / reachables", s_check_domains_in_reachables }, 
  { "usage of macro-transitions", s_check_macro_transitions }, 
  { NULL }
};

			/* --------------- */

static void
s_validate_relsem (ccl_log_type log, ar_relsem *rs, int reach);

			/* --------------- */

ar_relsem *
arc_command_get_relsem (const char *name)
{
  ar_relsem *rs = NULL;
  ar_node *n = arc_command_get_node (name);

  if (n != NULL)
    {
      ccl_try (altarica_interpretation_exception)
	rs = (ar_relsem *) ar_semantics_get (n, AR_SEMANTICS_RELATIONS);
      ccl_no_catch;
      ar_node_del_reference (n);
    }

  return rs;
}

			/* --------------- */

ARC_DEFINE_COMMAND (validate_command)
{
  int i;
  int use_reach = 1;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  if (argc <  1)
    {
      ccl_error("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  for (i = 0; i < argc && retval == ARC_CMD_SUCCESS; i++)
    {
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type == ARC_CMDP_STRING)
	{
	  if (strcmp (arg, "--no-reach") == 0)
	    use_reach = 0;
	  else if (strcmp (arg, "--reach") == 0)
	    use_reach = 1;
	  else 
	    {
	      ar_relsem *rs = arc_command_get_relsem (arg);

	      if (rs == NULL)
		retval = ARC_CMD_ERROR;
	      else
		{
		  s_validate_relsem (CCL_LOG_DISPLAY, rs, use_reach);
		  ar_relsem_del_reference (rs);
		}
	    }
	}
      else
	{
	  ccl_error ("error: wrong arg type (#%d).\n",i + 1);
	  retval = ARC_CMD_ERROR;
	}
    }

  return retval;
}

			/* --------------- */


void
ar_validate_ca (ccl_log_type log, ar_ca *ca)
  CCL_THROW (domain_cardinality_exception)
{
  ar_relsem *rs = ar_compute_relational_semantics (ca, NULL, NULL);
  s_validate_relsem (log, rs, 1);
  ar_relsem_del_reference (rs);
}

			/* --------------- */

static ar_dd
s_domain_of_variable (ar_relsem *rs, ar_ddm ddm, ar_dd F, ar_ca_expr *v)
{
  ar_identifier *vid = ar_ca_expr_variable_get_name (v);
  int ddindex = ar_relsem_get_var_dd_index (rs, vid);
  ar_dd dom = ar_dd_project_on_variable (ddm, F, ddindex);

  ar_identifier_del_reference (vid);

  return dom;
}

			/* --------------- */

static int
s_var_card (ar_ddm ddm, ar_dd D, const ar_ca_domain *dom)
{  
  int result;

  if (ar_dd_is_zero (ddm, D))
    result = 0;
  else if (ar_dd_is_one (ddm, D))
    result = ar_ca_domain_get_cardinality (dom);
  else
    {
      int i;
      int arity = AR_DD_ARITY (D);

      result = 0;
      for (i = 0; i < arity; i++)
	{
	  if (AR_DD_CODE (D) == EXHAUSTIVE)
	    {
	      if (! ar_dd_is_zero (ddm, AR_DD_EPNTHSON (D, i)))
		result++;
	    }
	  else
	    {
	      if (! ar_dd_is_zero (ddm, AR_DD_CPNTHSON (D, i)))
		result += (AR_DD_MAX_OFFSET_NTHSON (D, i) - 
			   AR_DD_MIN_OFFSET_NTHSON (D, i) + 1);
	    }
	}
    }

  return result;
}

			/* --------------- */

static int
s_is_singleton (ar_ddm ddm, ar_dd D, const ar_ca_domain *dom)
{  
  return s_var_card (ddm, D, dom) == 1;
}

			/* --------------- */

static void
s_display_domain (ccl_log_type log, ar_relsem *rs, ar_ca_exprman *eman, 
		  ar_ca_expr *V, ar_dd dom)
{
  ar_ca_expr *iexpr = ar_relsem_dd_to_expr (rs, dom);
  ar_relsem_add_eq_representative (rs, &iexpr, V);
  ar_ca_expr_log (log, iexpr);
  ar_ca_expr_del_reference (iexpr);
}

			/* --------------- */

static CHECK_PROC_PROTO (s_check_initial_states)
{  
  ar_dd init = ar_relsem_get_initial (rs);

  if (ar_dd_is_zero (ddm, init))
    ccl_log (log, TAB TAB "The system has no initial configuration.\n");
  else if (ar_relsem_get_cardinality (rs, init, 0) == 1.0)
    ccl_log (log, TAB TAB "The system has only one initial configuration\n");
  else 
    {
      int vs; 

      for (vs = 0; vs < 2; vs++)
	{
	  const ccl_pair *p;
	  const ccl_list *vars;
	  ccl_list *not_initialized;
	  const char *varfamily;

	  if (vs == 0)
	    {
	      vars = ar_ca_get_state_variables (ca);
	      varfamily = "state";
	    }
	  else 
	    {
	      vars = ar_ca_get_flow_variables (ca);
	      varfamily = "flow";
	    }

	  if (ccl_list_is_empty (vars))
	    continue;

	  not_initialized = ccl_list_create ();
	  
	  CCL_LIST_FOREACH (p, vars)
	    {
	      ar_ca_expr *v = CAR (p);
	      ar_dd values = s_domain_of_variable (rs, ddm, init, v);
	      const ar_ca_domain *dom = 
		ar_ca_expr_variable_get_domain (v);
	  
	      if (! s_is_singleton (ddm, values, dom))
		{
		  ccl_list_add (not_initialized, v);
		  ccl_list_add (not_initialized, values);
		}
	      else
		{
		  AR_DD_FREE (values);
		}
	    }

	  if (ccl_list_is_empty (not_initialized))
	    {
	      ccl_list_delete (not_initialized);
	      continue;
	    }

	  ccl_log (log, TAB TAB 
		   "%d %s variables have several initial values:\n",
		   ccl_list_get_size (not_initialized) / 2, varfamily);

	  while (! ccl_list_is_empty (not_initialized))
	    {	  
	      ar_ca_expr *V = ccl_list_take_first (not_initialized);
	      ar_dd ddom = ccl_list_take_first (not_initialized);
	  
	      if (ar_dd_is_one (ddm, ddom))
		{
		  ccl_log (log, TAB TAB TAB);
		  ar_ca_expr_log (log, V);
		  ccl_log (log, " is not initially constrained.\n");
		}
	      else
		{
		  ccl_log (log, TAB TAB TAB "initial condition for variable '");
		  ar_ca_expr_log (log, V);
		  ccl_log (log, "' verify:\n");
		  ccl_log (log, TAB TAB TAB TAB);
		  s_display_domain (log, rs, eman, V, ddom);
		  ccl_log (log, "\n");
		}
	      AR_DD_FREE (ddom);
	    } 
	  ccl_list_delete (not_initialized);
	}
    }

  AR_DD_FREE (init);
}

			/* --------------- */

static void
s_check_domains_against_dd (ccl_log_type log, ar_relsem *rs, ar_ca *ca, 
			    ar_ca_exprman *eman, ar_ddm ddm, ar_dd F, 
			    const char *Fname, ar_dd ref, const char *refname)
{
  int vs;
  int domains_are_covered = 1;

  for (vs = 0; vs < 2; vs++)
    {
      const ccl_list *vars;
      const ccl_pair *p;
      const char *varfamily;

      if (vs == 0)
	{
	  vars = ar_ca_get_state_variables (ca);
	  varfamily = "State";
	}
      else 
	{
	  vars = ar_ca_get_flow_variables (ca);
	  varfamily = "Flow";
	}

      if (ccl_list_is_empty (vars))
	continue;
      
      CCL_LIST_FOREACH (p, vars)
	{
	  ar_ca_expr *v = CAR (p);
	  ar_dd values = s_domain_of_variable (rs, ddm, F, v);
	  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (v);
	  int card = s_var_card (ddm, values, dom);

	  if (card != ar_ca_domain_get_cardinality (dom))
	    {
	      ar_dd refvalues = s_domain_of_variable (rs, ddm, ref, v);
	      ar_dd diff = ar_dd_compute_diff (ddm, refvalues, values);

	      domains_are_covered = 0;
	      ccl_log (log, TAB TAB TAB "%s variable '", varfamily);
	      ar_ca_expr_log (log, v);
	      ccl_log (log, "' does not cover its domain.\n");
	      if (ar_dd_is_zero (ddm, diff))
		ccl_log (log, TAB TAB TAB 
			 "However it covers its restriction to %s.\n", 
			 refname);
	      else
		{	
		  ccl_log (log, TAB TAB TAB 
			   "Missing values (restricted to %s) verify:\n",
			   refname);
		  ccl_log (log, TAB TAB TAB TAB);
		  s_display_domain (log, rs, eman, v, diff);
		  ccl_log (log, "\n");
		}

	      AR_DD_FREE (diff);
	      AR_DD_FREE (refvalues);
	    }
	  AR_DD_FREE (values);
	}
    } 
 
  if (domains_are_covered)
    ccl_log (log, TAB TAB 
	     "Domains of variables are covered by the set of %s.\n",
	     Fname);
}

			/* --------------- */

static CHECK_PROC_PROTO (s_check_domains_in_configurations)
{
  ar_dd any_c;
  
  if (! use_reach)
    return;

  any_c = ar_relsem_get_assertion (rs);
      
  if (ar_dd_is_zero (ddm, any_c))
    ccl_log (log, TAB TAB "set of configurations is empty.\n");
  else if (ar_dd_is_one (ddm, any_c))
    ccl_log (log, TAB TAB "any assignment is a configuration.\n");
  else    
    s_check_domains_against_dd (log, rs, ca, eman, ddm, any_c, 
				"configurations",
				ar_dd_one (ddm), "any assigments");
  AR_DD_FREE (any_c);
}

			/* --------------- */

static CHECK_PROC_PROTO (s_check_domains_in_reachables)
{  
  ar_dd reach;

  if (! use_reach)
    return;

  reach = ar_relsem_get_reachables (rs);

  if (ar_dd_is_zero (ddm, reach))
    ccl_log (log, TAB TAB "set of reachables is empty.\n");
  else if (ar_dd_is_one (ddm, reach))
    ccl_log (log, TAB TAB "any assignment is reachable.\n");
  else
    {
      ar_dd any_c = ar_relsem_get_assertion (rs);
      s_check_domains_against_dd (log, rs, ca, eman, ddm, reach, "reachables",
				  any_c, "configurations");
      AR_DD_FREE (any_c);
    }
  AR_DD_FREE (reach);
}

			/* --------------- */

static CHECK_PROC_PROTO (s_check_macro_transitions)
{
  if (use_reach)
    {
      int i;
      int all_are_triggered = 1;
      ar_dd_array rels = ar_relsem_get_transition_relations (rs, 0, 0);
      int nb_assertions = 0;
      ar_dd *assertions = ar_relsem_get_assertions (rs, &nb_assertions);
      ar_dd reach = ar_relsem_get_reachables (rs);
      ar_ca_trans **trans = ar_ca_get_transitions(ca);
  
      for (i = 0; i < rels.size; i++)    
	{
	  ar_dd p = 
	    ar_relsem_compute_post_by_T (rs, reach, rels.data[i], nb_assertions,
					 assertions);
	  if (ar_dd_is_zero (ddm, p))
	    {
	      all_are_triggered = 0;
	      ccl_log (log, TAB TAB);
	      ar_ca_trans_log (log, trans[i]);
	      ccl_log (log, " is never triggered.\n");
	    }
	  AR_DD_FREE (p);
	}
      AR_DD_DELETE_ARRAY (rels);
      AR_DD_FREE (reach);

      if (all_are_triggered)
	ccl_log (log, TAB TAB "All macro-transitions are triggered.\n");
    }
}

			/* --------------- */

static int
s_variable_is_used (ar_ca *ca, ar_ca_expr *v)
{
  int result = 0;

  if (!result)
    {
      const ccl_pair *p;
      const ccl_list *assertions = ar_ca_get_assertions (ca);
      
      for (p = FIRST (assertions); p && !result; p = CDR (p))
	result = ar_ca_expr_has_variable (CAR (p), v);
    }

  if (!result)
    {
      int i, nb_transitions = ar_ca_get_number_of_transitions (ca);
      ar_ca_trans **t = ar_ca_get_transitions (ca);
      
      for (i = 0; i < nb_transitions && !result; i++)
	{
	  ar_ca_expr *g = ar_ca_trans_get_guard (t[i]);

	  result = ar_ca_expr_has_variable (g, v);
	  ar_ca_expr_del_reference (g);
	  if (! result)
	    {
	      int j;
	      int nb_assignments = ar_ca_trans_get_number_of_assignments (t[i]);
	      ar_ca_expr **a = ar_ca_trans_get_assignments (t[i]);

	      for (j = 0; j < nb_assignments && !result; j++, a += 2)
		result = (ar_ca_expr_has_variable (a[0], v) ||
			  ar_ca_expr_has_variable (a[1], v));
	    }	  
	}
    }  

  return result;
}

			/* --------------- */

static int
s_variable_is_assigned (ar_ca *ca, ar_ca_expr *v)
{
  int result = 0;
  int i, nb_transitions = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **t = ar_ca_get_transitions (ca);
      
  for (i = 0; i < nb_transitions && !result; i++)
    {
      int j;
      int nb_assignments = ar_ca_trans_get_number_of_assignments (t[i]);
      ar_ca_expr **a = ar_ca_trans_get_assignments (t[i]);

      for (j = 0; j < nb_assignments && !result; j++, a += 2)
	result = ar_ca_expr_has_variable (a[0], v);
    }  

  return result;
}

			/* --------------- */

static CHECK_PROC_PROTO (s_check_usage_of_variables)
{  
  int i;
  int no_unused = 1;
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_flow_variables (ca);
  const char *varkind = "flow";

  for (i = 0; i < 2; i++)
    {
      CCL_LIST_FOREACH (p, vars)
	{
	  ar_ca_expr *v = CAR (p);
	  ar_identifier *varname = ar_ca_expr_variable_get_name (v);

	  if (! s_variable_is_used (ca, v))
	    {
	      ar_identifier *suffixname = 
		ar_identifier_get_name (varname);
	      if (suffixname != AR_IDLE_VAR)
		{
		  ccl_log (log, TAB TAB "%s variable '", varkind);
		  ar_identifier_log (log, varname);
		  ccl_log (log, "' is never used (asserts and trans). \n");
		  no_unused = 0;
		}
	      ar_identifier_del_reference (suffixname);
	    }

	  if (i == 1 && !s_variable_is_assigned (ca, v))
	    {
	      ccl_log (log, TAB TAB "%s variable '", varkind);
	      ar_identifier_log (log, varname);
	      ccl_log (log, "' is not assigned by a transition. \n");
	    }
	  ar_identifier_del_reference (varname);
	}
      vars = ar_ca_get_state_variables (ca);
      varkind = "state";
    }

  if (no_unused)
    {
      ccl_log (log, TAB TAB "All variables are referenced at least once in "
	       "assertions or transitions.\n");
    }
}

			/* --------------- */

static void
s_validate_relsem (ccl_log_type log, ar_relsem *rs, int use_reach)
{
  const basic_checking *bs;
  ar_ca *ca = ar_relsem_get_automaton (rs);
  ar_ca_exprman *eman = ar_ca_get_expression_manager (ca);
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_node *node = ar_ca_get_node (ca);
  ar_identifier *name = ar_node_get_name (node);


  ccl_log (log, "basic properties checking for node '");   
  ar_identifier_log (log, name);
  ccl_log (log, "'\n");
  ar_identifier_del_reference (name);

  if (use_reach)
    {
      ar_mec5_relation *reachrel;
      ar_dd reach;
      ar_identifier *reachid = ar_bang_id_for_node (node, AR_BANG_REACHABLES);
      if (! ar_mec5_has_relation (reachid))
	ar_mec5_build_bang_relation (node, AR_BANG_REACHABLES);
      reachrel = ar_mec5_get_relation (reachid);
      reach = ar_mec5_relation_get_dd (reachrel);

      ccl_log (log, TAB "there is %.0f configurations.\n", 
	       ar_relsem_get_cardinality (rs, reach, 0));
      AR_DD_FREE (reach);
      ar_mec5_relation_del_reference (reachrel);
      ar_identifier_del_reference (reachid);
    }

  for (bs = CHECKLIST; bs->check != NULL; bs++)
    {
      ccl_log (log, TAB "%s\n", bs->name);
      bs->check (log, rs, ca, use_reach, eman, ddm);
      ccl_log (log, "\n");
    }

  ar_ca_exprman_del_reference (eman);
  ar_ca_del_reference (ca);

  ar_node_del_reference (node);
}
