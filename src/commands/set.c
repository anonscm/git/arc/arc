/*
 * set.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "arc.h"
#include "arc-debug.h"
#include "allcmds.h"

ARC_DEFINE_COMMAND (set_command)
{  
  *result = NULL;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  if (argc == 0)
    {
      retval = ARC_CMD_ERROR;
      ccl_config_table_display (ARC_PREFERENCES);
      goto end;
    }

  if (argv[0]->type != ARC_CMDP_STRING)
    {
      retval = ARC_CMD_ERROR;
      ccl_error ("error: bad argument (#%d) type.\n", 1);
      goto end;
    }

  if (argv[0]->u.string_value[0] == '-')
    {
      if (strcmp (argv[0]->u.string_value, "-load") == 0)
	{
	  arc_load_preferences ();
	  arc_debug_update_flags (ARC_PREFERENCES);
	}
      else if (strcmp (argv[0]->u.string_value, "-save") == 0)
	arc_save_preferences ();
      else
	{
	  ccl_error ("error: unknown option '%s' to command set.\n",  
		     argv[0]->u.string_value);
	  retval = ARC_CMD_ERROR;
	}
    }  
  else 
    {
      char *propname = argv[0]->u.string_value;

      if (argc == 1)
	{
	  if (ccl_config_table_has (ARC_PREFERENCES, propname))
	    ccl_display ("%s = %s\n", propname, 
			 ccl_config_table_get (ARC_PREFERENCES, propname));
	  else
	    ccl_display ("unknown preference '%s'.\n", propname);
	}
      else if (argv[1]->type == ARC_CMDP_STRING)
	{
	  ccl_config_table_set (ARC_PREFERENCES, propname, 
				argv[1]->u.string_value);
	}
      else if (argv[1]->type == ARC_CMDP_INTEGER)
	{
	  char *tmp = ccl_string_format_new ("%d", argv[1]->u.int_value);
	  ccl_config_table_set (ARC_PREFERENCES, propname, tmp);
	  ccl_string_delete (tmp);
	}
      else
	{
	  ccl_error ("error: bad argument (#%d) type.\n", 1);
	  retval = ARC_CMD_ERROR;
	}

      if (retval == ARC_CMD_SUCCESS && 
	  strncmp (propname, arc_debug_preferences_prefix, 
		   strlen (arc_debug_preferences_prefix)) == 0)
	arc_debug_update_flags (ARC_PREFERENCES);	
    }
 end:
  return retval;
}

			/* --------------- */
