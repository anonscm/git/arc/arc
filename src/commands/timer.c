/*
 * timer.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-time.h>
#include "allcmds.h"

struct timer
{
  int is_started;
  ar_identifier *name;
  uint32_t t0;
  uint32_t accu;
};

			/* --------------- */

static void
s_create_or_start_timer (const char *name);

static struct timer *
s_find_timer (const char *name);

static void
s_print_timer (struct timer *t, int accu);

static void
s_delete_timer (struct timer *t);

			/* --------------- */

static ar_idtable *timers;

			/* --------------- */

int
timer_cmd_init (ccl_config_table *prefs)
{
  timers = ar_idtable_create (1, NULL, (ccl_delete_proc *) s_delete_timer);

  return 1;
}

			/* --------------- */

int
timer_cmd_terminate (void)
{
  ar_idtable_del_reference (timers);

  return 1;
}

			/* --------------- */

ARC_DEFINE_COMMAND (timer_command)
{
  int i;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  if (argc != 0 && argc != 2)
    {
      ccl_error ("error: wrong # argument (%d).\n", argc);
      retval = ARC_CMD_ERROR;
    }

  for (i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument (#%d) type\n", 0);
	  retval = ARC_CMD_ERROR;
	}
    }
  if (retval == ARC_CMD_ERROR)
    goto end;

  if (argc == 0)
    {
      ar_identifier_iterator *ti = ar_idtable_get_keys (timers);

      while (ccl_iterator_has_more_elements (ti))
	{
	  ar_identifier *id = ccl_iterator_next_element (ti);
	  ar_identifier_log (CCL_LOG_DISPLAY, id);
	  ccl_display ("\n");
	  ar_identifier_del_reference (id);
	}
      ccl_iterator_delete (ti);
    }
  else if (ccl_string_equals (argv[0]->u.string_value, "start"))
    s_create_or_start_timer (argv[1]->u.string_value);
  else
    {
      struct timer *t = s_find_timer (argv[1]->u.string_value);
      if (t == NULL)
	{
	  ccl_error ("error: unknown timer '%s'\n", argv[1]->u.string_value);
	  retval = ARC_CMD_ERROR;
	}
      else if (strcmp (argv[0]->u.string_value, "reset") == 0)
	{
	  t->is_started = 0;
	  t->accu = 0;
	  t->t0 = ccl_time_get_cpu_time_in_milliseconds ();
	}
      else if (strcmp (argv[0]->u.string_value, "remove") == 0)
	ar_idtable_remove (timers, t->name);
      else if (strcmp (argv[0]->u.string_value, "stop") == 0)
	{
	  if (t->is_started)
	    {
	      uint32_t t1 = ccl_time_get_cpu_time_in_milliseconds ();
	      t->is_started = 0;
	      t->accu += t1 - t->t0;
	      t->t0 = t1;
	    }
	  else
	    {
	      ccl_warning ("timer '");
	      ar_identifier_log (CCL_LOG_WARNING, t->name);
	      ccl_warning ("' is already stopped.\n", t->name);
	    }
	}
      else if (strcmp (argv[0]->u.string_value, "elapsed") == 0)
	{
	  if (t->is_started)
	    s_print_timer (t, 0);
	  else
	    {
	      ccl_warning ("start timer '");
	      ar_identifier_log (CCL_LOG_WARNING, t->name);
	      ccl_warning ("' first.\n");
	    }
	}
      else if (strcmp (argv[0]->u.string_value, "print") == 0)
	{
	  if (t->is_started)
	    {
	      ccl_warning ("stop timer '");
	      ar_identifier_log (CCL_LOG_WARNING, t->name);
	      ccl_warning ("' first.\n");
	    }
	  else
	    s_print_timer (t, 1);
	}
      else
	{
	  ccl_error ("error: unknown subcommand '%s'.\n",
		     argv[0]->u.string_value);
	  retval = ARC_CMD_ERROR;
	}
    }
 end:

  return retval;
}

			/* --------------- */

static void
s_create_or_start_timer (const char *name)
{
  struct timer *t = s_find_timer (name);

  if (t == NULL)
    {
      t = ccl_new (struct timer);
      t->is_started = 0;
      t->name = ar_identifier_create (name);
      t->accu = 0;
      ar_idtable_put (timers, t->name, t);
    }

  if (t->is_started)
    ccl_warning ("timer '%s' is already started.\n", name);
  else
    {
      t->is_started = 1;
      t->t0 = ccl_time_get_cpu_time_in_milliseconds ();
    }
}

			/* --------------- */

static struct timer *
s_find_timer (const char *name)
{
  ar_identifier *id = ar_identifier_create (name);
  struct timer *result = ar_idtable_get (timers, id);
  ar_identifier_del_reference (id);

  return result;
}

			/* --------------- */

static void
s_print_timer (struct timer *t, int accu)
{
  const char *f = accu ? "total_time" : "elapsed";
  uint32_t value =
    accu ? t->accu : ccl_time_get_cpu_time_in_milliseconds () - t->t0;
  
  ccl_display ("%s (", f);
  ar_identifier_log (CCL_LOG_DISPLAY, t->name);
  ccl_display (") = %.3f\n", (float) value  / 1000.0);
}

			/* --------------- */

static void
s_delete_timer (struct timer *t)
{
  ccl_pre (t != NULL);

  ar_identifier_del_reference (t->name);
  ccl_delete (t);
}
