/*
 * flatten.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-interp-altarica-exception.h"
#include "ar-model.h"
#include "ar-semantics.h"
#include "allcmds.h"
#include "allcmds-p.h"

ARC_DEFINE_COMMAND (flatten_command)
{
  int i;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  for(i = 0; i < argc; i++)
    {
      char *arg = argv[i]->u.string_value;

      if( argv[i]->type == ARC_CMDP_STRING )
	{
	  ar_node *n = arc_command_get_flat (arg);

	  if (n != NULL)
	    {
	      ccl_display ("// flat semantics of node %s \n", arg);
	      ar_node_log (CCL_LOG_DISPLAY, n, 1);
	      ccl_display ("\n");
	      ar_node_del_reference (n);
	    }
	}
      else
	{
	  ccl_error ("error: wrong arg type (#%d).\n", i + 1);
	  retval = ARC_CMD_ERROR;
	}
    }

  return retval;
}

			/* --------------- */

ar_node *
arc_command_get_flat (const char *name)
{
  ar_node *result = NULL;
  ar_node *n = arc_command_get_node (name);

  if (n == NULL)
    goto end;

  ccl_try (altarica_interpretation_exception)
    result = (ar_node *) ar_semantics_get (n, AR_SEMANTICS_FLATTENED);
  ccl_no_catch;
  ar_node_del_reference (n);

end:
  return result;
}
