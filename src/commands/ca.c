/*
 * ca.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-interp-altarica-exception.h"
#include "ar-model.h"
#include "ar-semantics.h"
#include "ar-constraint-automaton.h"
#include "allcmds.h"
#include "allcmds-p.h"

ARC_DEFINE_COMMAND (ca_command)
{
  int i;
  int mec5mode = 0;

  *result = NULL;

  for (i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: wrong argument (#0) type.\n");
	  
	  return ARC_CMD_ERROR;
	}
    }

  for (i = 0; i < argc; i++)
    {
      if (ccl_string_equals (argv[0]->u.string_value, "--mec"))
	{
	  mec5mode = 1;
	  i++;
	}
      else if (argv[0]->u.string_value[0] == '-')
	{
	  ccl_error ("error: unknown option '%s'.\n", argv[0]->u.string_value);
	  
 	  return ARC_CMD_ERROR;
	}
      else
	{
	  ar_ca *ca = arc_command_get_ca (argv[i]->u.string_value);
	  
	  if (ca == NULL)
	    return ARC_CMD_ERROR;

	  ccl_display ("// Constraint automaton of node '%s'\n", 
		       argv[i]->u.string_value);
	  ar_ca_log_gen (CCL_LOG_DISPLAY, ca, 0, mec5mode ? "^" : ".", "''");
	  ar_ca_del_reference (ca);
	}
    }

  return ARC_CMD_SUCCESS;
}

			/* --------------- */

ar_node *
arc_command_get_node (const char *name)
{
  ar_node *n = NULL;
  ar_identifier *id = ar_identifier_create (name);

  if (ar_model_has_node (id))
    n = ar_model_get_node (id);
  else
    ccl_error ("error: unknown node '%s'.\n", name);
  ar_identifier_del_reference (id);

  return n;
}


			/* --------------- */

ar_ca *
arc_command_get_ca (const char *name)
{
  ar_ca *ca = NULL;
  ar_node *n = arc_command_get_node (name);

  if (n == NULL)
    goto end;

  ccl_try (exception)
    ca = (ar_ca *) ar_semantics_get (n, AR_SEMANTICS_CONSTRAINT_AUTOMATON);
  ccl_catch
    {
      if (ccl_exception_is_raised (abstract_type_exception))
	ccl_error ("abstract type are not supported for this command.\n");
    }
  ccl_end_try;
  ar_node_del_reference (n);
 end:
  return ca;
}
