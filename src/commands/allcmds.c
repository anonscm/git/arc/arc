/*
 * allcmds.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "allcmds.h"

struct command
{
  const char *cmdid;
  arc_command_proc *cmd;
};

#define NB_COMMANDS (sizeof (COMMANDS)/COMMANDS[0])

#define ARC_COMMAND(_name,_cmd)			      \
  arc_command_register(_name, _cmd, NULL, NULL); 


void
arc_register_commands(void)
{
#include "allcmds.def"
}
