/*
 * arch.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "allcmds-p.h"

/*
ARC_DEFINE_COMMAND_HELP (arch_command) = 
{
  "arch",
  "architecture",
  "",
  "Syntax:\n"
  " arch id1 id2 ...\n"
  "  Display objects identified by id1 id2 ...\n"
  ,
  NULL, NULL
};
*/
			/* --------------- */

static void
s_architecture_to_dot (int tab, ar_identifier *name, ar_node *node);

			/* --------------- */

ARC_DEFINE_COMMAND (arch_command)
{
  int i;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  for (i = 0; i < argc; i++)
    {
      ar_identifier *name;
      ar_node *node;
      char *arg = argv[i]->u.string_value;


      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument (#%d) type\n", i);
	  continue;
	}
      node = arc_command_get_node (arg);
      if (node != NULL)
	{
	  name = ar_node_get_name (node);
	  s_architecture_to_dot (0, name, node);
	  ar_identifier_del_reference (name);
	  ar_node_del_reference (node);
	}
    }

  return retval;
}

			/* --------------- */

#define TAB() ccl_display ("%*s", tab, "")
#define TAB_DISPLAY(args)			\
  do {						\
    TAB ();					\
    ccl_display args;				\
  } while (0)

static void
s_architecture_to_dot (int tab, ar_identifier *name, ar_node *node)
{  
  ar_identifier *tmp;
  const char *cluster = "";
  const char *graph_type = "strict graph";

  if (tab > 0)
    {
      cluster = "cluster";
      graph_type = "subgraph";
    }
  TAB_DISPLAY (("%s \"%s", graph_type, cluster));
  ar_identifier_log (CCL_LOG_DISPLAY, name);
  ccl_display ("\" {\n");
  tab += 2;
  TAB_DISPLAY (("label=\""));
  tmp = ar_identifier_get_name (name);
  ar_identifier_log (CCL_LOG_DISPLAY, tmp);
  ar_identifier_del_reference (tmp);
  ccl_display ("\";\n");

  /* declare variables */
  {
    ar_context_slot_iterator *iv = ar_node_get_slots_for_variables (node);
    while (ccl_iterator_has_more_elements (iv))
      {
	ar_context_slot *sl = ccl_iterator_next_element (iv);
	ar_identifier *vname = ar_context_slot_get_name (sl);

	TAB_DISPLAY(("\""));
	ar_identifier_log (CCL_LOG_DISPLAY, name);
	ccl_display (".");
	ar_identifier_log (CCL_LOG_DISPLAY, vname);
	ccl_display ("\" [label=\"");
	ar_identifier_log (CCL_LOG_DISPLAY, vname);
	ccl_display ("\"];\n");
	ar_identifier_del_reference (vname);
	ar_context_slot_del_reference (sl);
      }
    ccl_iterator_delete (iv);
  }

  /* create sub boxes */
  {
    ar_context_slot_iterator *isub = ar_node_get_slots_for_subnodes (node);
    while (ccl_iterator_has_more_elements (isub))
      {
	ar_context_slot *sl = ccl_iterator_next_element (isub);
	ar_identifier *subname = ar_context_slot_get_name (sl);
	ar_type *type = ar_context_slot_get_type (sl);
	ar_node *subnode = ar_type_bang_get_node (type);
	
	tmp = ar_identifier_add_prefix (subname, name);
	s_architecture_to_dot (tab, tmp, subnode);
	ar_identifier_del_reference (tmp);
	ar_type_del_reference (type);
	ar_node_del_reference (subnode);
	ar_identifier_del_reference (subname);
	ar_context_slot_del_reference (sl);
      }
    ccl_iterator_delete (isub);
  }

  /* create link for assertions */
  {
    ccl_pair *p;
    ccl_list *assertions = ar_node_get_assertions (node);
    ar_context *ctx = ar_node_get_context (node);

    for (p = FIRST (assertions); p; p = CDR (p))
      {
	ccl_pair *psl;
	ar_expr *A = CAR (p);
	ccl_list *vars = ar_expr_get_variables (A, ctx, NULL);

	for (psl = FIRST (vars); psl; psl = CDR (psl))
	  {
	    ar_context_slot *sl = CAR (psl);
	    ar_identifier *slname = ar_context_slot_get_name (sl);

	    /* output the assertion node */
	    TAB_DISPLAY (("\""));
	    ar_identifier_log (CCL_LOG_DISPLAY, name);
	    ccl_display (".%p\" [label=\"\"];", A);
	    TAB_DISPLAY (("\""));
	    ar_identifier_log (CCL_LOG_DISPLAY, name);
	    ccl_display (".%p\" -- \"", A);
	    ar_identifier_log (CCL_LOG_DISPLAY, name);
	    ccl_display (".");
	    ar_identifier_log (CCL_LOG_DISPLAY, slname);
	    ccl_display ("\";\n");

	    ar_identifier_del_reference (slname);
	    ar_context_slot_del_reference (sl);
	  }
	ccl_list_delete (vars);
      }
    ar_context_del_reference (ctx);
  }
  tab -= 2;
  TAB_DISPLAY (("}\n"));  
}
