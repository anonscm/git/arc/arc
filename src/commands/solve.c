/*
 * solve.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-interp-altarica.h"
#include "parser/altarica-input.h"
#include "ar-constraint-automaton.h"
#include "ar-ca-semantics.h"
#include "ar-interp-altarica-exception.h"
#include "ar-model.h"
#include "ar-solver.h"
#include "ar-semantics.h"
#include "allcmds.h"

typedef ar_solver *
solver_constructor (ar_ca_expr **constraints, int nb_constraints,
		    ar_ca_expr **variables, int nb_variables,
		    ar_backtracking_stack *bstack,
		    ar_solver_start_solution_se_proc *set_start,
		    ar_solver_new_solution_proc *set_add,
		    ar_solver_end_solution_set_proc *set_end);

#define DEFAULT_SOLVER_CONSTRUCTOR ar_create_gp_solver

struct solver
{
  const char *id;
  solver_constructor *create;
};


static const struct solver SOLVERS[] = {
  { "solve", ar_create_gp_solver},
  { "glucose", ar_create_glucose_solver},
  { NULL, NULL }
};

static void
s_solve_ca_constraint (solver_constructor *crt, ar_ca *ca, int max);

static void
s_solve_expr (solver_constructor *crt, ar_ca *ca, int max, const char *formula);

ARC_DEFINE_COMMAND (solve_command)
{
  ar_identifier *id;
  arc_command_retval retval = ARC_CMD_ERROR;
  solver_constructor *create;
  const struct solver *s;
  
  for (s = SOLVERS; s->id != NULL; s++)
    {
      if (ccl_string_equals (s->id, cmd))
	{
	  create = s->create;
	  break;
	}
    }
  
  ccl_assert (s->id != NULL);
  *result = NULL;
  if (! (1 <= argc && argc <= 3))
    ccl_error ("error: wrong # of arguments (%d).\n", argc);
  else if (argv[0]->type != ARC_CMDP_STRING)
    ccl_error ("error: bad argument (#1) type.\n");
  else if (argc >= 2 && argv[1]->type != ARC_CMDP_INTEGER)
    ccl_error ("error: bad argument (#2) type.\n");
  else if (argc == 3  && argv[2]->type != ARC_CMDP_STRING)
    ccl_error ("error: bad argument (#3) type.\n");
  else
    {
      id = ar_identifier_create (argv[0]->u.string_value);
      
      if (! ar_model_has_node(id))
	ccl_error ("error: unknown node '%s'.\n", argv[0]->u.string_value);
      else
	{
	  ar_node *n = ar_model_get_node (id);
	  int max = -1;

	  if (argc >= 2)
	    max = argv[1]->u.int_value;

	  ccl_try (exception)
	    {
	      ar_ca *ca = (ar_ca *)
		ar_semantics_get (n, AR_SEMANTICS_CONSTRAINT_AUTOMATON);
	    
	      ccl_display ("// solutions to constraints of node '");
	      ar_identifier_log (CCL_LOG_DISPLAY, id);
	      ccl_display ("' (solver=%s)\n", s->id);
	      if (argc <= 2)
		s_solve_ca_constraint (create, ca, max);
	      else
		s_solve_expr (create, ca, max, argv[2]->u.string_value);
	      
	      ar_ca_del_reference (ca);
	      retval = ARC_CMD_SUCCESS;
	    }
	  ccl_no_catch;
      
	  ar_node_del_reference (n);
	}
      ar_identifier_del_reference (id);
    }

  return retval;
}

			/* --------------- */

static int
s_display_current_assignment (ar_solver *solver, void *data)
{
  int i, nb_vars;
  int *counters = data;
  ar_ca_expr **vars = ar_solver_get_variables (solver, &nb_vars);

  if (counters[1] >= 0 && counters[0] >= counters[1])
    return 0;

  counters[0]++;

  for (i = 0; i < nb_vars; i++)
    {
      ar_identifier *name = ar_ca_expr_variable_get_name (vars[i]);
      ar_bounds *b = ar_ca_expr_get_bounds_address (vars[i]);

      ar_identifier_log (CCL_LOG_DISPLAY, name);
      ccl_display (" ");
      if (ar_ca_expr_variable_is_boolean(vars[i]))
	{
	  const char *msg;

	  if (!ar_bounds_is_singleton (b)) 
	    msg = "in bool";
	  else if (b->min) 
	    msg = "= true";
	  else 
	    msg = "= false";
	  ccl_display ("%s", msg);
	}
      else if (ar_ca_expr_variable_is_integer (vars[i]))
	{
	  const char *fmt;
	  if (ar_bounds_is_singleton (b)) 
	    fmt = "= %d";
	  else 
	    fmt = "in [%d,%d]";
	  ccl_display (fmt, b->min, b->max);
	}
      else
	{
	  int v;
	  ar_ca_exprman *man = ar_ca_expr_get_expression_manager (vars[i]);
	  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (vars[i]);

	  if (!ar_bounds_is_singleton (b))
	    ccl_display ("in { ");
	  else 
	    ccl_display ("= ");

	  for (v = b->min; v <= b->max; v++)
	    {
	      ar_ca_expr *cst = ar_ca_expr_get_enum_constant_by_index (man, v);

	      if (ar_ca_domain_has_element (dom, v))
		{

		  ar_ca_expr_log (CCL_LOG_DISPLAY, cst);
		  if (v != b->max)
		    ccl_display (", ");
		}
	      ar_ca_expr_del_reference (cst);
	    }

	  if (!ar_bounds_is_singleton (b))
	    ccl_display (" }");

	  ar_ca_exprman_del_reference (man);
	}
      ar_identifier_del_reference (name);

      if (i != nb_vars - 1)
	ccl_display (", ");
    }
  ccl_display ("\n");

  return 1;
}

			/* --------------- */

static void
s_solve_ca_constraint (solver_constructor *crt, ar_ca *ca, int max)
{
  int i;
  ar_solver *solver;
  int nb_constraints;
  ar_ca_expr **constraints = (ar_ca_expr **)
    ccl_list_to_array (ar_ca_get_assertions (ca), &nb_constraints);
  int nb_variables = ar_ca_get_number_of_variables (ca);
  ar_ca_expr **variables = ccl_new_array (ar_ca_expr *, nb_variables);
  ar_backtracking_stack *bstack = ar_backtracking_stack_create ();
  int counters[2];
  ar_ca_expr **v;
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_state_variables (ca);
  
  for (v = variables, p = FIRST (vars); p; p = CDR (p), v++)
    *v = CAR (p);
  vars = ar_ca_get_flow_variables (ca);
  for (p = FIRST (vars); p; p = CDR (p), v++)
    *v = CAR (p);
  for (i = 0; i < nb_variables; i++)
    ar_ca_expr_variable_reset_bounds (variables[i]);
  counters[0] = 0;
  counters[1] = max;
  solver = crt (constraints, nb_constraints, variables, nb_variables, bstack,
		NULL, s_display_current_assignment, NULL);
  ar_solver_solve (solver, counters);
  ccl_assert (ar_backtracking_stack_is_empty (bstack));
  ar_backtracking_stack_del_reference (bstack);
  ccl_delete (variables);
  ccl_delete (constraints);
  ar_solver_delete (solver);
}

			/* --------------- */

ar_ca_expr *
arc_command_get_expr (ar_ca *ca, const char *formula)
{
  ar_ca_expr *result = NULL;
  char *tmp = ccl_string_format_new ("@%s@", formula);
  altarica_tree *t =
    altarica_read_string (tmp, "command line", AR_INPUT_AEXPR, NULL, NULL);
  ccl_string_delete (tmp);

  if (t != NULL)
    {
      ar_expr *flat_expr = NULL;
      ar_ca_exprman *man = ar_ca_get_expression_manager(ca);
      ar_node *node = ar_ca_get_node (ca);
      ar_context *ctx = ar_node_get_context (node);

      ccl_try (exception)
        {
	  ar_expr *nexpr =
	    ar_interp_boolean_expression (t->child, ctx,
					  ar_interp_acheck_member_access);
	  flat_expr = ar_expr_remove_composite_slots (nexpr, ctx);
	  ar_expr_del_reference (nexpr);
	  
	  result = ar_model_expr_to_ca_expr (flat_expr, man, NULL);	  
	}
      ccl_no_catch;
      ccl_zdelete (ar_expr_del_reference, flat_expr);
      ar_context_del_reference (ctx);
      ar_node_del_reference (node);
      ar_ca_exprman_del_reference (man);
      ccl_parse_tree_delete_tree (t);
    }
  return result;
}

static void
s_solve_expr (solver_constructor *crt, ar_ca *ca, int max, const char *formula)
{
  int i;
  ar_solver *solver;
  int nb_variables;
  ar_ca_expr **variables;
  ar_backtracking_stack *bstack;
  int counters[2];
  ccl_list *vars;
  ar_ca_expr *F = arc_command_get_expr (ca, formula);
  
  if (F == NULL)
    return;
  
  vars = ar_ca_expr_get_variables (F, NULL);
  variables = (ar_ca_expr **) ccl_list_to_array (vars, &nb_variables);
  bstack = ar_backtracking_stack_create ();
  ccl_list_delete (vars);
  
  for (i = 0; i < nb_variables; i++)
    ar_ca_expr_variable_reset_bounds (variables[i]);
  counters[0] = 0;
  counters[1] = max;
  solver = crt (&F, 1, variables, nb_variables, bstack, NULL, 
		s_display_current_assignment, NULL);
  ar_solver_solve (solver, counters);
  ccl_assert (ar_backtracking_stack_is_empty (bstack));
  ar_backtracking_stack_del_reference (bstack);
  ccl_delete (variables);
  ar_solver_delete (solver);
  ar_ca_expr_del_reference (F);
}
