/*
 * validate.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef VALIDATE_H
# define VALIDATE_H

extern void
ar_validate_ca (ccl_log_type log, ar_ca *ca)
  CCL_THROW(domain_cardinality_exception);

#endif /* ! VALIDATE_H */
