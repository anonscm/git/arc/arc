/*
 * stepper.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-interp-altarica.h"
#include "parser/altarica-input.h"
#include "ar-ca-semantics.h"
#include "ar-model.h"
#include "ar-constraint-automaton.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "allcmds-p.h"
#include "ar-identifier.h"
#include "ar-stepper.h"
#include "arc-shell.h"
#include "stepper.h"

typedef struct stepper_st stepper;
struct stepper_st 
{
  ar_ca *ca;
  varorder *order;
  ar_stepper *stepper;
};
  
/*
 * Sub-commands 
 */
#define SUBCOMMAND_PROTO(cmdname) \
  void cmdname (stepper *S, int argc, arc_command_parameter *argv)

#define SUBCOMMAND_NAME(cmdname) s_sc_ ## cmdname

#define DEFINE_SUBCOMMAND(cmdname) \
  static SUBCOMMAND_PROTO(SUBCOMMAND_NAME(cmdname)) 

struct subcommand 
{
  const char *name;
  SUBCOMMAND_PROTO((*subcmd)) ;
};

#define SUBCOMMANDS_LIST \
  SUBCOMMAND_DECL("start", start)	      \
  SUBCOMMAND_DECL("valid-trans", valid_trans) \
  SUBCOMMAND_DECL("trigger", trigger) \
  SUBCOMMAND_DECL("vars", vars)	      
  
#undef SUBCOMMAND_DECL
#define SUBCOMMAND_DECL(name, cmdname) DEFINE_SUBCOMMAND(cmdname);
SUBCOMMANDS_LIST

#undef SUBCOMMAND_DECL
#define SUBCOMMAND_DECL(name, cmdname) { name, SUBCOMMAND_NAME(cmdname) },

static struct subcommand SUBCOMMANDS[] = {
  SUBCOMMANDS_LIST
  { NULL, NULL }
};

			/* --------------- */

static int
s_integer_arg (int index, arc_command_parameter *argv);

static const char *
s_string_arg (int index, arc_command_parameter *argv);

static ar_ca_expr *
s_ca_formula_arg (ar_ca *ca, int index, arc_command_parameter *argv);

static ar_stepper_state_array 
s_states_arg (stepper *S, int from, int to, arc_command_parameter *argv);

static stepper *
s_find_or_add_stepper (const char *nodename);

static void
s_delete_stepper (stepper *S);

static void
s_display_state_array (stepper *S, ccl_log_type log, ar_stepper_state_array a);

			/* --------------- */


static ar_idtable *STEPPERS = NULL;

			/* --------------- */

int 
stepper_cmd_init (ccl_config_table *prefs)
{
  if (STEPPERS == NULL)
    STEPPERS = ar_idtable_create (1, NULL,
				  (ccl_delete_proc *) s_delete_stepper);

  return 1;
}

			/* --------------- */

int 
stepper_cmd_terminate (void)
{
  if (STEPPERS != NULL)
    {
      ar_idtable_del_reference (STEPPERS);
      STEPPERS = NULL;
    }
  return 1;
}

			/* --------------- */


ARC_DEFINE_COMMAND (stepper_command)
{
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (argc < 2)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  ccl_try (exception)
    {
      const char *nodeid;
      const char *cmd;
      struct subcommand *sc;
      stepper *S;

      nodeid = s_string_arg (0, argv);
      cmd = s_string_arg (1, argv);

      S = s_find_or_add_stepper (nodeid);
      if (S == NULL)
	ccl_throw_no_msg (exception);

      for (sc = SUBCOMMANDS; sc->name && strcmp (cmd, sc->name) != 0; sc++)
	CCL_NOP ();

      if (sc->subcmd)
	sc->subcmd (S, argc - 2, argv + 2);      
    }
  ccl_catch
    {
      if (ccl_exception_is_raised (memory_exhausted_exception))
	ccl_rethrow ();
      retval = ARC_CMD_ERROR;
    }
  ccl_end_try;

  return retval;
}


			/* --------------- */

static int
s_integer_arg (int index, arc_command_parameter *argv)
{
  int result;

  if (argv[index]->type != ARC_CMDP_INTEGER)
    {
      ccl_error ("error: bad argument (%d) type.\n", index);
      ccl_throw_no_msg (exception);
    }  
  else
    {
      result = argv[index]->u.int_value;
    }

  return result;
}

			/* --------------- */

static const char *
s_string_arg (int index, arc_command_parameter *argv)
{
  const char *result = NULL;

  if (argv[index]->type != ARC_CMDP_STRING)
    {
      ccl_error ("error: bad argument (%d) type.\n", index);
      ccl_throw_no_msg (exception);
    }  
  else
    {
      result = argv[index]->u.string_value;
    }

  return result;
 }

			/* --------------- */

static ar_ca_expr *
s_ca_formula_arg (ar_ca *ca, int index, arc_command_parameter *argv)
{
  ar_ca_expr *result = NULL;
  const char *formula = s_string_arg (index, argv);
  char *tmp = ccl_string_format_new ("@%s@", formula);
  altarica_tree *t = 
    altarica_read_string (tmp, "command line", AR_INPUT_AEXPR, NULL, NULL);
  ar_node *node = ar_ca_get_node (ca);
  ar_ca_exprman *man = ar_ca_get_expression_manager (ca);
  ar_context *ctx = ar_node_get_context (node);
  ar_expr *fe = NULL;
  
  ar_node_del_reference (node);
  ccl_string_delete (tmp);

  if (t == NULL)
    ccl_throw_no_msg (exception);

  ccl_try (exception)
    {
      ar_expr *e = NULL;
      

      ccl_exception_local_variable (ccl_parse_tree_delete_tree, &t);
      ccl_exception_local_variable (ar_context_del_reference, &ctx);
      ccl_exception_local_variable (ar_ca_exprman_del_reference, &man);
      ccl_exception_local_variable (ar_ca_expr_del_reference, &fe);

      e = ar_interp_boolean_expression (t->child, ctx,
					ar_interp_acheck_member_access);
      fe = ar_expr_remove_composite_slots (e, ctx);
      ar_expr_del_reference (e);
      
      result = ar_model_expr_to_ca_expr (fe, man, NULL);
    }
  ccl_catch
    {
      ccl_rethrow ();
    }
  ccl_end_try;
  
  ccl_parse_tree_delete_tree (t);
  ar_context_del_reference (ctx);
  ar_ca_exprman_del_reference (man);
 
  return result;
}

			/* --------------- */

static ar_stepper_state_array 
s_states_arg (stepper *S, int from, int to, arc_command_parameter *argv)
{
  ar_stepper_state_array result;
  ar_ca_exprman *eman = ar_ca_get_expression_manager (S->ca);
  ar_ca_expr *a = ar_ca_expr_crt_boolean_constant (eman, 1);

  ccl_try (exception)
    {
      int i;

      for (i = from; i <= to; i++)
	{
	  const char *formula = s_string_arg (i, argv);
	  if (strlen (formula) > 0)
	    {
	      ar_ca_expr *cond = s_ca_formula_arg (S->ca, i, argv);
	      ar_ca_expr *tmp = ar_ca_expr_crt_and (a, cond);
	      ar_ca_expr_del_reference (cond);
	      ar_ca_expr_del_reference (a);
	      a = tmp;
	    }
	}
    }
  ccl_catch
    {
      ar_ca_expr_del_reference (a);
      ar_ca_exprman_del_reference (eman);
      ccl_rethrow ();
    }
  ccl_end_try;

  result = ar_stepper_get_states_for_expr (S->stepper, a, AR_STEPPER_ALL);  
  ar_ca_expr_del_reference (a);
  ar_ca_exprman_del_reference (eman);

  return result;
}

			/* --------------- */

static stepper *
s_find_or_add_stepper (const char *nodename)
{
  ar_identifier *id = ar_identifier_create (nodename);
  stepper *result = ar_idtable_get (STEPPERS, id);

  if (result == NULL)
    {
      ar_ca *ca = arc_command_get_ca (nodename);
      if (ca != NULL)
	{
	  result = ccl_new (stepper);
	  result->ca = ca;
	  result->order = ar_ca_build_varorder (ca);
	  result->stepper = ar_stepper_create (ca, result->order);
	  ar_idtable_put (STEPPERS, id, result);
	}
    }
  ar_identifier_del_reference (id);

  return result;
}

			/* --------------- */

static void
s_delete_stepper (stepper *S)
{
  ar_ca_del_reference (S->ca);
  varorder_del_reference (S->order);
  ar_stepper_del_reference (S->stepper);
  ccl_delete (S);
}

			/* --------------- */

static void
s_display_state_array (stepper *S, ccl_log_type log, ar_stepper_state_array a)
{
  int i;
  int nb_vars = 0;
  ar_ca_expr **vars = ar_stepper_get_variables (S->stepper, &nb_vars);
  ar_ca_exprman *eman = ar_ca_get_expression_manager (S->ca);

  for (i = 0; i < a.size; i++)
    {
      int v;
      ar_stepper_state *s = a.data[i];
      
      for (v = 0; v < nb_vars; v++)
	{
	  const ar_ca_domain *dom = 
	    ar_ca_expr_variable_get_domain (vars[v]);
	  int index = varorder_get_index (S->order, vars[v]);
	  int min, max;

	  ar_ca_expr_log (log, vars[v]);
	  ccl_log (log, " = ");
	  ar_stepper_state_get_range (s, index, &min, &max);

	  ccl_assert (min == max);

	  ar_ca_domain_log_value (log, dom, min, eman);
	  if (v < nb_vars - 1)
	    ccl_log (log, ", ");
	}
      ccl_log (log, "\n");
    }
  ar_ca_exprman_del_reference (eman);
}

			/* --------------- */

DEFINE_SUBCOMMAND (start)
{
  ar_stepper_state_array states;

  if (argc == 0)
    states = ar_stepper_get_initial_states (S->stepper, AR_STEPPER_ALL);
  else
    states = s_states_arg (S, 0, argc - 1, argv);
  s_display_state_array (S, CCL_LOG_DISPLAY, states);
  ar_stepper_state_array_clean (states);
}

			/* --------------- */


DEFINE_SUBCOMMAND (valid_trans)
{
  int i;
  ar_stepper_state_array states = s_states_arg (S, 0, argc - 1, argv);
  int cr_mode = ARC_SHELL_CURRENT_CONTEXT && ARC_SHELL_CURRENT_CONTEXT->cr_mode;
  ar_stepper_index_array Ti;
  
  if (states.size != 1)
    {
      if (states.size == 0)
	ccl_error ("error: not a valid configuration\n");
      else
	ccl_error ("error: not enough precise assignment\n");
      ar_stepper_state_array_clean (states);
      ccl_throw_no_msg (exception);
    }

  ar_stepper_set_state (S->stepper, states.data[0], 0);
  ar_stepper_state_array_clean (states);

  Ti = ar_stepper_get_index_of_valid_transitions (S->stepper, 1);

  if (cr_mode)
    {  
      for (i = 0; i < Ti.size; i++)
	ccl_display ("%d\n", Ti.data[i]);
    }
  else
    {
      ar_stepper_trans_array T = 
	ar_stepper_get_valid_transitions (S->stepper, 1);

      for (i = 0; i < T.size; i++)
	{
	  ccl_display ("[%d] ", Ti.data[i]);
	  ar_ca_trans_log (CCL_LOG_DISPLAY, T.data[i]);
	  ccl_display ("\n");
	  ar_ca_trans_del_reference (T.data[i]);
	}
      ccl_array_delete (T);
    }
  ccl_array_delete (Ti);
}


DEFINE_SUBCOMMAND (trigger)
{
  int Ti;
  int nbT = ar_ca_get_number_of_transitions (S->ca);
  ar_ca_trans **T = ar_ca_get_transitions (S->ca);
  ar_stepper_state_array states;
  ar_stepper_state_array succs;

  if (argc == 0)
    {
      ccl_error ("error: stepper trigger: wrong # of arguments.\n");
      ccl_throw_no_msg (exception);
    }

  Ti = s_integer_arg (0, argv);
  if (! (0 <= Ti && Ti < nbT))
    {
      ccl_error ("error: stepper trigger: wrong transition index %d.\n", Ti);
      ccl_throw_no_msg (exception);
    }

  states = s_states_arg (S, 1, argc - 1, argv);
  if (states.size != 1)
    {
      if (states.size == 0)
	ccl_error ("error: not a valid configuration\n");
      else
	ccl_error ("error: not enough precise assignment\n");
      ar_stepper_state_array_clean (states);
      ccl_throw_no_msg (exception);
    }

  ar_stepper_set_state (S->stepper, states.data[0], 0);
  ar_stepper_state_array_clean (states);

  succs = ar_stepper_trigger_transition (S->stepper, T[Ti], AR_STEPPER_ALL);
  s_display_state_array (S, CCL_LOG_DISPLAY, succs);
  ar_stepper_state_array_clean (succs);
}

			/* --------------- */

DEFINE_SUBCOMMAND (vars)
{
  int i, nb_vars = 0;
  ar_ca_expr **vars = ar_stepper_get_variables (S->stepper, &nb_vars);

  if (argc != 0)
    {
      ccl_error ("error: stepper vars: wrong # of arguments.\n");
      ccl_throw_no_msg (exception);
    }

  for (i = 0; i < nb_vars; i++)
    {
      ar_ca_expr_log (CCL_LOG_DISPLAY, vars[i]);
      ccl_display ("\n");
    }
}

