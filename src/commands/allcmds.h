/*
 * allcmds.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ALLCMDS_H
# define ALLCMDS_H

# include "arc-commands.h"

# define ARC_COMMAND(name, proc) ARC_DECLARE_COMMAND (proc);
# include "allcmds.def"
# undef ARC_COMMAND

extern void
arc_register_commands(void);

#endif /* ! ALLCMDS_H */
