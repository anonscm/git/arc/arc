/*
 * unit-test.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "allcmds-p.h"
#include "allcmds.h"
#include "sat/ca2sat.h"
#include "sat/bitblaster.h"
#include "sas/sas.h"

struct unit_test 
{
  const char *test_name;
  int (*test)(ccl_log_type log);
};


static int all_tests (ccl_log_type log);

static struct unit_test UNIT_TESTS[] = {
  
  { "ca2sat", ca2sat_unit_tests },
  { "bitblaster", bitblaster_unit_tests },
  { "sas", sas_unit_tests },
  { "all", all_tests },
  { NULL, NULL }
};


ARC_DEFINE_COMMAND (unit_test_command)
{
  struct unit_test *ut;
  arc_command_retval retval = ARC_CMD_ERROR;

  *result = NULL;
  if (argc != 1)
    ccl_error ("error: wrong # of arguments (%d).\n", argc);
  else if (argv[0]->type != ARC_CMDP_STRING )
    ccl_error ("error: bad argument type (%d).\n", 0);
  else
    {
      for (ut = UNIT_TESTS; ut->test_name; ut++)
	{
	  if (strcmp (ut->test_name, argv[0]->u.string_value) == 0)
	    break;
	}
      
      if (ut->test == NULL)
	ccl_error ("error: wrong # of arguments (%d).\n", argc);
      else if (ut->test (CCL_LOG_DISPLAY))
	retval = ARC_CMD_SUCCESS;
    }
  
  return retval;
}

static int 
all_tests (ccl_log_type log)
{
  int result = 1;
  struct unit_test *ut;

  for (ut = UNIT_TESTS; ut->test_name != NULL; ut++)
    {
      if (ut->test != all_tests)
	{
	  int res = ut->test (log);
	  result = result && res;
	  ccl_log (log, "%s ... \t%s\n", ut->test_name, 
		   res ? "SUCCESS" : "FAIL");
		       
	}      
    }

  return result;
}
