/*
 * sas.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <signal.h>
#include <ctype.h>
#include <ccl/ccl-string.h>
#include "arc-readline.h"
#include "ar-ca-to-sas.h"
#include <sas/sas.h>
#include <sas/sas-rnd.h>
#include "allcmds.h"
#include "allcmds-p.h"

#define field_offset(type,field) (&(((type *)(NULL))->field))
#define param_offset(field) field_offset(sas_simulation_parameters, field)
#define field_addr(ptr,offset)   (((void *)(ptr))+offset)

struct optionpref
{
  const char *id;
  void *addr;
  const char *defvalue;
  void (*parser)(const char *strval, void *addr);
};

#define DEFAULT_PRNG  "ed"
#define DEFAULT_SCHEDULER  "dlink"

static ar_ca_expr *
s_found_undefined_flow_variable (ar_ca *ca);

static int
s_stop_simulation_callback (void *data);

static void
s_ctrl_c_handler (int sig);

static void 
s_parse_scheduler_type (const char *str, void *addr);

static void 
s_parse_prng (const char *str, void *addr);

static void 
s_parse_int (const char *str, void *addr);

static void 
s_parse_bool (const char *str, void *addr);

static void 
s_parse_double (const char *str, void *addr);

static void 
s_parse_threads (const char *str, void *addr);

static void
s_init_preferences (ccl_config_table *config, struct optionpref *prefs);

static int
s_parse_pref_option (const char *opt, struct optionpref *prefs);

static int stop_simulation = 0;

ARC_DEFINE_COMMAND (sas_command)
{
  int i;
  ar_ca *ca;
  ar_ca_expr *fv;  
  sas_simulation_parameters params;
  const char *modelname = NULL;
  int dont_catch_interrupt = 0;
  int debug = ccl_debug_is_on && sas_debug_is_on;
  struct optionpref PREFS[] = {
    { "scheduler", &params.scheduler, DEFAULT_SCHEDULER,
      s_parse_scheduler_type },
    { "prng", &params.prng, DEFAULT_PRNG, s_parse_prng },
    { "nb-threads", &params.nb_threads, "1", s_parse_threads },
    { "ignore-sigint", &dont_catch_interrupt, "false", s_parse_bool },
    { "timeout", &params.timeout, "-1", s_parse_int },
    { "seed", &params.seed, "123456781", s_parse_int },
    { "loop-length", &params.loop_length, "100", s_parse_int },
    { "nb-stories", &params.number_of_stories, "1", s_parse_int },
    { "duration", &params.duration, "0.0", s_parse_double },
    { "nb-tries", &params.number_of_tries, "1", s_parse_int },    
    { NULL }
  };
  
  *result = NULL;

  params.stop_simulation = NULL;
  params.stop_data = NULL;
  ccl_try (exception)
    {
      s_init_preferences (config, PREFS);
      
      for (i = 0; i < argc; i++)
	{
	  const char *opt = argv[i]->u.string_value;

	  if (argv[i]->type != ARC_CMDP_STRING)
	    {
	      ccl_error ("error: wrong argument (#0) type.\n");
	      ccl_throw_no_msg (exception);
	    }
	  if (s_parse_pref_option (opt, PREFS))
	    continue;
	  
	  if (*opt == '-')
	    {
	      ccl_error ("error: unknown option '%s'.\n", opt);
	      ccl_throw_no_msg (exception);
	    }
	  else if (modelname != NULL)
	    {
	      ccl_error ("error: only one model is allowed.\n", opt);
	      ccl_throw_no_msg (exception);
	    }
	  else
	    {
	      modelname = opt;
	    }
	}  
      ccl_assert (modelname != NULL);

      if (modelname == NULL)
	{
	  ccl_error ("error: no model is specified.\n");
	  ccl_throw_no_msg (exception);
	}
    }
  ccl_catch
    {
      return ARC_CMD_ERROR;
    }
  ccl_end_try;

  CCL_DEBUG_COND_START_TIMED_BLOCK (debug, ("compile model for simulation"));
  
  ca = arc_command_get_ca (modelname);
  if (ca == NULL)
    {
      CCL_DEBUG_COND_END_TIMED_BLOCK (debug);
      return ARC_CMD_ERROR;
    }
  else if ((fv = s_found_undefined_flow_variable (ca)) != NULL)
    {
      ccl_error ("The model is not data-flow. Flow variable '");
      ar_ca_expr_log (CCL_LOG_ERROR, fv);
      ccl_error ("' has been marked as a free input variable.\n");
      ar_ca_del_reference (ca);
      CCL_DEBUG_COND_END_TIMED_BLOCK (debug);
      
      return ARC_CMD_ERROR;
    }
  else
    {
      sas_model *M = ar_ca_to_sas_model (ca);
      sas_simulator *sim = sas_simulator_crt (M);
      
      CCL_DEBUG_COND_END_TIMED_BLOCK (debug);
  
      params.stop_simulation = s_stop_simulation_callback;
      params.stop_data = NULL;
      if (! dont_catch_interrupt)
	signal (SIGINT, s_ctrl_c_handler);
      sas_simulator_run (sim, &params);
      if (! dont_catch_interrupt)
	signal (SIGINT, SIG_DFL);
      sas_simulator_display_results (sim, CCL_LOG_DISPLAY);
      sas_simulator_destroy (sim);
      ar_ca_del_reference (ca);
      ar_ca_destroy_sas_model (M);
    }
  
  return ARC_CMD_SUCCESS;
}



static ar_ca_expr *
s_found_undefined_flow_variable (ar_ca *ca)
{
  const ccl_list *vars = ar_ca_get_flow_variables (ca);
  const ccl_pair *v;
  const fdset *fds = ar_ca_get_fdset (ca);

  for (v = FIRST (vars); v; v = CDR (v))
    {
      if (! fdset_has_dependency_for (fds, CAR (v)))
	return CAR (v);
    }
  
  return NULL;
}

static int
s_stop_simulation_callback (void *data)
{
  return stop_simulation;
}

static void
s_ctrl_c_handler (int sig)
{
  char *s;
  char c;
  static const char *prompt = "should I interrupt the simulation ? [y/N]\n";
  do
    {
      s = arc_readline_get_line_no_rl (prompt);
      c = tolower (*s);
    }
  while (c != 'y' && c != 'n');
  stop_simulation = (c == 'y');
}

static void 
s_parse_scheduler_type (const char *str, void *addr)
{
  enum sas_scheduler_type *pt = addr;
  
  if (ccl_string_cmp (str, "dlink") == 0)
    *pt = SAS_SCH_DLINK;
  else if (ccl_string_cmp (str, "cq") == 0)
    *pt = SAS_SCH_CQ;
  else
    {
      ccl_error ("unknown scheduler type '%s'.\n", str);
      ccl_throw_no_msg (exception);
    }
}

static void 
s_parse_prng (const char *str, void *addr)
{
  sas_prng_kind *pp = addr;
  
  if (ccl_string_equals (str, "ed"))
    *pp = SAS_PRNG_ERARD_DEGUENON;
  else if (ccl_string_equals (str, "mks"))
    *pp = SAS_PRNG_MARSAGLIA_KISS_SWB;
  else if (ccl_string_equals (str, "mkl"))
    *pp = SAS_PRNG_MARSAGLIA_KISS_LFIB4;
  else
    {
      ccl_error ("error: unknown prng '%s'.\n", str);
      ccl_throw_no_msg (exception);
    }
}

static void 
s_parse_int (const char *str, void *addr)
{
  *((int *)addr) = ccl_string_parse_int (str);
}

static void 
s_parse_bool (const char *str, void *addr)
{
  *((int *)addr) = ccl_string_parse_boolean (str);
}

static void 
s_parse_double (const char *str, void *addr)
{
  *((sas_double *)addr) = atof (str);
}

static void 
s_parse_threads (const char *str, void *addr)
{
  int *pnbt = addr;
  
  s_parse_int (str, pnbt);
  
  if (*pnbt <= 0)
    {
      ccl_error ("bad number of threads '%s'.\n", str);
      ccl_throw_no_msg (exception);
    }
}

static void
s_init_preferences (ccl_config_table *config, struct optionpref *prefs)
{
  int err = 0;
  char *pref = NULL;
  size_t prefsize = 0;

  ccl_try (exception)
    {
      for (; prefs->id; prefs++)
	{
	  const char *val;
	  ccl_string_format (&pref, &prefsize, "sas.%s", prefs->id);
	  val = ccl_config_table_get (config, pref);
	  if (val == NULL)
	    val = prefs->defvalue;
	  prefs->parser (val, prefs->addr);
	}
    }
  ccl_catch
    {
      err = 1;
    }
  ccl_end_try;

  ccl_zdelete (ccl_delete, pref);
  if (err)
    ccl_rethrow ();
}

static int
s_parse_pref_option (const char *opt, struct optionpref *prefs)
{
  const char *arg;
  
  if ((opt = ccl_string_has_prefix ("--", opt)) == NULL)
    return 0;
  for (; prefs->id != NULL; prefs++)
    {
      if (prefs->parser == s_parse_bool && ccl_string_equals (prefs->id, opt))
	{
	  prefs->parser (prefs->defvalue, prefs->addr);
	  *((int *)prefs->addr) = ! *((int *)prefs->addr);
	  return 1;
	}
      else if ((arg = ccl_string_has_prefix (prefs->id, opt)) != NULL)
	{

	  if (arg[0] == '=')
	    {
	      prefs->parser (arg + 1, prefs->addr);
	      return 1;
	    }
	  else
	    {
	      ccl_error ("missing argument to option '--%s'.\n", opt);
	      ccl_throw_no_msg (exception);
	    }
	}
    }
  return 0;
}
