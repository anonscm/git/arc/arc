/*
 * check-card.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-config-table.h>
#include "mec5/mec5.h"
#include "arc-shell-preferences.h"
#include "arc-commands.h"

ARC_DEFINE_COMMAND (check_card_command)
{
  int i = 0;  
  int check_ok = 1;
  int abort_on_failure = 
    ccl_config_table_get_boolean (config, ARC_SHELL_CHECK_CARD_ABORT);
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  while (i < argc)
    {
      double expected_card = -1.0;
      ar_identifier *propid;
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: wrong arg type (#%d).\n",1);
	  return ARC_CMD_ERROR;
	}
      i++;

      if (i < argc)
	{
	  if (argv[i]->type == ARC_CMDP_INTEGER)
	    {
	      expected_card = (double) argv[i]->u.int_value;
	      i++;
	    }
	  else if (argv[i]->type == ARC_CMDP_FLOAT)
	    {
	      expected_card = argv[i]->u.flt_value;
	      i++;
	    }
	}
      
      propid = ar_identifier_create (arg);
      if (ar_mec5_has_relation (propid))
	{
	  int lcheck;
	  ar_mec5_relation *R = ar_mec5_get_relation (propid);
	  double card = ar_mec5_relation_get_cardinality (R);
	  ar_mec5_relation_del_reference (R);

	  ccl_display ("check card (");
	  ar_identifier_log (CCL_LOG_DISPLAY, propid);
	  ccl_display (")");
	  if (expected_card < 0.0)
	    {
	      ccl_display (" != 0");
	      lcheck = (card != 0.0);
	    }
	  else
	    {
	      ccl_display (" = %0.f", expected_card);
	      lcheck = (card == expected_card);
	    }	  	  
	  if (lcheck) 
	    ccl_display (" passed\n");
	  else
	    ccl_display (" failed (%.0f)\n", card);
	  check_ok = check_ok && lcheck;
	}
      else
	{
	  ccl_error("error: undefined set '");
	  ar_identifier_log (CCL_LOG_ERROR, propid);
	  ccl_error("'.\n");
	  retval = ARC_CMD_ERROR;
	}
      ar_identifier_del_reference(propid);
    }

  if ((! check_ok || retval == ARC_CMD_ERROR) && abort_on_failure)
    exit (EXIT_FAILURE);

  return retval;
}
