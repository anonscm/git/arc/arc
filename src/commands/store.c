/*
 * store.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "allcmds.h"
#include "allcmds-p.h"
#include "mec5/mec5.h"
#include "mec5/mec5-serialization.h"

ARC_DEFINE_COMMAND (store_command)
{
  int i;
  const char *filename; 
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  for (i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument type (#%d).\n", i);
	  return ARC_CMD_ERROR;
	}
    }

  if (argc == 0)
    {
      ccl_error ("error: wrong # of arguments\n");
      return ARC_CMD_ERROR;
    }

  filename = argv[0]->u.string_value;
  for (i = 1; i < argc && retval == ARC_CMD_SUCCESS; i++)
    {
      ar_identifier *newname = NULL;
      ar_identifier *id = ar_identifier_create (argv[i]->u.string_value);
      
      if (! ar_mec5_has_relation (id))
	{
	  ccl_error ("error: unknown relation '%s'.\n", 
		     argv[i]->u.string_value);
	  ar_identifier_del_reference (id);
	  return ARC_CMD_ERROR;
	}
      
      if (i + 1 < argc && 
	  ccl_string_cmp (argv[i + 1]->u.string_value, "as") == 0)
	{
	  if (i + 2 < argc)
	    newname = ar_identifier_create (argv[i + 2]->u.string_value);
	  else
	    {
	      ccl_error ("error: wrong # of arguments\n");
	      ar_identifier_del_reference (id);
	      return ARC_CMD_ERROR;
	    }
	  i += 2;
	}

      if (! ar_mec5_store_relation (id, newname, filename, config))
	{
	  retval = ARC_CMD_ERROR;
	  ccl_error ("error: IO error while writing in '%s'\n", filename);
	}

      ar_identifier_del_reference (id);
      ccl_zdelete (ar_identifier_del_reference, id);
    }

  return retval;
}

			/* --------------- */
