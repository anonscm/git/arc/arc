/*
 * dump.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "allcmds.h"

static void
s_dump_node (ar_identifier *id)
{
  ar_node *node = ar_model_get_node (id);
  altarica_tree *t = ar_node_get_template(node);
  if (t != NULL)
    {
      ccl_display ("// this node has not been interpreted and is displayed "
		   " according to its\n"
		   "// syntactic tree (taken from %s:%d).\n",
		   t->filename, t->line);
      ar_node_log (CCL_LOG_DISPLAY, node, 0);
      ccl_display ("\n");
    }
  else
    {
      ar_node_dump_node (CCL_LOG_DISPLAY, node, 0);
      ccl_display ("\n");
    }

  ar_node_del_reference (node);
}

			/* --------------- */

ARC_DEFINE_COMMAND (dump_command)
{
  *result = NULL;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  if (argc !=  1)
    {
      ccl_error ("error: wrong # of arguments\n");
      return ARC_CMD_ERROR;
    }

  {
    char *arg = argv[0]->u.string_value;

    if (argv[0]->type == ARC_CMDP_STRING)
      {

	ar_identifier *id = ar_identifier_create (arg);

	if (ar_model_has_node (id)) 
	  { 
	    s_dump_node (id); 
	  }
	else
	  {
	    ccl_error ("error: unknown node '%s'\n", arg);
	    retval = ARC_CMD_ERROR;
	  }
	ar_identifier_del_reference (id);
      }
    else
      {
	ccl_error ("error: wrong arg type (#0).\n");
	retval = ARC_CMD_ERROR;
      }
    }

  return retval;
}

			/* --------------- */

