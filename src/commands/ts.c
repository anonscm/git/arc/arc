/*
 * ts.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-interp-altarica-exception.h"
#include "ar-model.h"
#include "ar-semantics.h"
#include "allcmds.h"
#include "allcmds-p.h"

ARC_DEFINE_COMMAND (ts_command)
{
  int i;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  for (i = 0; i < argc && retval == ARC_CMD_SUCCESS; i++)
    {
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type == ARC_CMDP_STRING)
	{
	  ar_ts *ts = arc_command_get_ts (arg);

	  if (ts != NULL)
	    {
	      ar_identifier *id = ar_identifier_create (arg);
	      ccl_display ("// transition system of node '");
	      ar_identifier_log (CCL_LOG_DISPLAY,id);
	      ccl_display ("'\n");	      
	      ar_ts_log (CCL_LOG_DISPLAY, ts, id);
	      ar_ts_del_reference (ts);
	      ar_identifier_del_reference (id);
	    }
	}
      else
	{
	  ccl_error ("error: wrong arg type (#%d).\n", i + 1);
	  retval = ARC_CMD_ERROR;
	}
    }

  return retval;
}

			/* --------------- */

ar_ts *
arc_command_get_ts (const char *name)
{
  ar_ts *result = NULL;
  ar_node *n = arc_command_get_node (name);

  if (n == NULL)
    goto end;

  ccl_try (exception)
    result = (ar_ts *) ar_semantics_get (n, AR_SEMANTICS_TRANSITION_SYSTEM);
  ccl_catch
    {
      if (ccl_exception_is_raised (domain_cardinality_exception))
	ccl_error ("error: Can't compute TS. Some variables have a domain "
		   "with a cardinality exceeding %d.\n", AR_TS_MAX_STATE_CARD); 
    } 
  ccl_end_try;
  ar_node_del_reference (n);

end:
  return result;
}

			/* --------------- */

