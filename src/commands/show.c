/*
 * show.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "table-ids.h"

struct table
{
  const char *name;
  int (*has) (ar_identifier *id);
  void (*show_object) (ar_identifier *id);
};

			/* --------------- */

static void
s_show_parameter (ar_identifier *id);

static void
s_show_type (ar_identifier *id);

static void
s_show_node (ar_identifier *id);

static void
s_show_signature (ar_identifier *id);

static void
s_show_relation (ar_identifier *id);

			/* --------------- */

static const struct table TABLES[] =
  {
    { TABLE_CONSTANTS, ar_model_has_parameter, s_show_parameter },
    { TABLE_DOMAINS, ar_model_has_type, s_show_type },
    { TABLE_NODES, ar_model_has_node, s_show_node },
    { TABLE_SIGNATURES, ar_model_has_signature, s_show_signature },
    { TABLE_RELATIONS, ar_mec5_has_relation, s_show_relation },
    { NULL, NULL, NULL }
  };

			/* --------------- */


ARC_DEFINE_COMMAND (show_command)
{
  int i;
  const struct table *t;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  for (i = 0; i < argc && retval == ARC_CMD_SUCCESS; i++)
    {
      int shown = 0;
      ar_identifier *id;
	  
      if (argv[i]->type != ARC_CMDP_STRING)
	ccl_error ("error: bad argument type #%d.\n", i);
      
      id = ar_identifier_create (argv[i]->u.string_value);
      
      for (t = TABLES; t->name != NULL; t++)
	{
	  if (t->has != NULL && t->has (id))
	    {
	      shown = 1;
	      if (t->show_object != NULL)
		t->show_object (id);
	    }
	}
	  
      if (! shown)
	{
	  ccl_error ("error: unknown identifier '%s'.\n",
		     argv[i]->u.string_value);
	  retval = ARC_CMD_ERROR;
	}

      ar_identifier_del_reference (id);
    }

  return retval;
}

			/* --------------- */

static void
s_show_parameter (ar_identifier *id)
{
  ar_context_slot *sl = ar_model_get_parameter (id);
  ar_type *type = ar_context_slot_get_type (sl);
  ar_expr *value = ar_context_slot_get_value (sl);

  ccl_display ("const ");
  ar_identifier_log (CCL_LOG_DISPLAY, id);
  ccl_display (" : ");
  ar_type_log (CCL_LOG_DISPLAY, type);
  ar_type_del_reference (type);
  if (value != NULL)
    {
      ccl_display (" = ");
      ar_expr_log (CCL_LOG_DISPLAY, value);
      ar_expr_del_reference (value);
    }
  ccl_display (";\n");
  ar_context_slot_del_reference (sl);
}

			/* --------------- */

static void
s_show_type (ar_identifier *id)
{
  ar_type *type = ar_model_get_type (id);
	      
  if (ar_type_get_kind (type) == AR_TYPE_ABSTRACT)
    {
      ccl_display ("sort ");
    }
  else
    {
      ccl_display ("domain ");
      ar_identifier_log (CCL_LOG_DISPLAY, id);
      ccl_display (" = ");
    }
  ar_type_log (CCL_LOG_DISPLAY, type);
  ccl_display ("\n");
  ar_type_del_reference (type);
}

			/* --------------- */

static void
s_show_node (ar_identifier *id)
{
  ar_node *node = ar_model_get_node (id);
  altarica_tree *t = ar_node_get_template (node);
  if (t != NULL)
    {
      ccl_display ("// this node has not been interpreted and is displayed "
		   " according to its\n"
		   "// syntactic tree (taken from %s:%d).\n",
		   t->filename, t->line);
    }

  ar_node_log (CCL_LOG_DISPLAY, node, 0);
  ccl_display ("\n");

  ar_node_del_reference (node);
}

			/* --------------- */

static void
s_show_signature (ar_identifier *id)
{
  ar_signature *sig = ar_model_get_signature (id);

  ar_signature_log (CCL_LOG_DISPLAY, sig);
  ccl_display ("\n");

  ar_signature_del_reference (sig);
}

			/* --------------- */

static void
s_show_relation (ar_identifier *id)
{
  ar_mec5_relation *rel = ar_mec5_get_relation (id);

  ar_identifier_log (CCL_LOG_DISPLAY, id);
  ccl_display(" contains : \n");
  ar_mec5_relation_log (CCL_LOG_DISPLAY, rel);
  ccl_display("\n");

  ar_mec5_relation_del_reference (rel);
}

			/* --------------- */

