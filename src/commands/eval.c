/*
 * eval.c -- Eval AltaRica text, Acheck or MEC 5 specification from ARC shell.
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-config-table.h>
#include "parser/altarica-input.h"
#include "parser/altarica-tree.h"
#include "ar-interp-altarica-exception.h"
#include "ar-interp-altarica.h"
#include "mec5/mec5-interp-ast.h"
#include "ar-interp-acheck.h"
#include "arc-commands.h"
#include "arc-shell.h"
#include "arc-shell-preferences.h"
#include "arc-readline.h"

static int
s_interp_altarica_tree (altarica_tree *t, ccl_config_table *config)
{
  int result = 1;

  ccl_pre (t != NULL);

  for (; t != NULL && result; t = t->next)
    {
      switch (t->node_type) 
	{
	case AR_TREE_ALTARICA : 
	  result = ar_interp_altarica_tree (t->child);
	  break;
	case AR_TREE_MECV :
	  result = ar_interp_mec5_tree (t->child, config);
	  break;
	case AR_TREE_ACHECK :
	  result = ar_interp_acheck_tree (t->child, config);
	  break;

	default:
	  ccl_unreachable ();
	}
    }

  return result;
}

			/* --------------- */

static int
s_is_cmd (const char *id, void *data)
{
  return (arc_command_find (id) != NULL);
}

			/* --------------- */

ARC_DEFINE_COMMAND (eval_command)
{
  altarica_tree *t;
  const int any_spec =  (AR_INPUT_ALTARICA|AR_INPUT_MECV| AR_INPUT_ACHECK);
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (ARC_SHELL_CURRENT_CONTEXT && ARC_SHELL_CURRENT_CONTEXT->is_interactive)
    {
      int cr_mode = ARC_SHELL_CURRENT_CONTEXT->cr_mode;
      int end_of_input_len;
      const char *end_of_input = "EOF";
      int stop = 0;
      char *str = NULL;

      const char *prompt = 
	(cr_mode ? NULL : ccl_config_table_get (config, ARC_SHELL_PROMPT_EVAL));

      if (argc > 1) 
	{
	  ccl_error ("error: wrong # of arguments\n");
	  
	  return ARC_CMD_ERROR;
	}

      if (argc == 1)
	{
	  if (argv[0]->type != ARC_CMDP_STRING)
	    {
	      ccl_error ("error: bad argument type.\n", 1);

	      return ARC_CMD_ERROR;
	    }
	  end_of_input = argv[0]->u.string_value;
	}


      if (cr_mode)
	ccl_display("EOC\n");
      end_of_input_len = strlen (end_of_input);

      while (!stop)
	{
	  char *line = arc_readline_get_line (!cr_mode, prompt);
	  if (line == NULL)
	    break;

	  if (strncmp (line, end_of_input, end_of_input_len) == 0 &&
	      line[end_of_input_len] == '\n')
	    stop = 1;
	  else
	    ccl_string_format_append (&str, "%s", line);
	  
	  ccl_string_delete (line);
	  if (cr_mode && !stop)
	    ccl_display("EOC\n");
	} 

      if (str != NULL)
	{
	  t = altarica_read_string (str, "<eval>", any_spec, s_is_cmd, NULL);
	  if (t != NULL)
	    {
	      if (! s_interp_altarica_tree (t, config))
		retval = ARC_CMD_ERROR;
	      ccl_parse_tree_delete_tree(t);
	    }
	  else
	    {
	      retval = ARC_CMD_ERROR;
	    }
	  ccl_string_delete (str);
	}
    }
  else
    {
      int i;

      for (i = 0; i < argc && retval == ARC_CMD_SUCCESS; i++)
	{
	  if (argv[i]->type != ARC_CMDP_STRING)
	    {
	      ccl_error ("error: bad argument (#%d) type.\n", i);
	      retval = ARC_CMD_ERROR;
	    }
	  else
	    {
	      t = altarica_read_string (argv[i]->u.string_value, "", any_spec, 
					NULL, NULL);
	      if (t == NULL || ! s_interp_altarica_tree (t, config))
		retval = ARC_CMD_ERROR;
	      ccl_zdelete (ccl_parse_tree_delete_tree, t);
	    }
	}
    }

  return retval;
}

			/* --------------- */


