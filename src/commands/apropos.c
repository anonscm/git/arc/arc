/*
 * apropos.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-help.h"
#include "allcmds.h"

ARC_DEFINE_COMMAND (apropos_command)
{
  char *word;
  ccl_list *topics;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (argc == 0)
    goto end;

  word = argv[0]->u.string_value;
  topics = ar_help_apropos (word);
        
  if (ccl_list_is_empty (topics))
    {
      ccl_error ("error: can't find info related to '%s'\n", word);
      retval = ARC_CMD_ERROR;
    }
  else 
    {
      ccl_pair *p;
      
      ccl_display ("Topics related to '%s':\n", word);
      for (p = FIRST (topics); p; p = CDR (p))
	{
	  ar_help *hlp = CAR (p);
	  ccl_display ("%s - %s\n", hlp->manpage, hlp->shortdesc);
	}  
    }
  ccl_list_delete (topics);

 end:
  return retval;
}
