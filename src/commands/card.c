/*
 * card.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "mec5/mec5.h"
#include "allcmds.h"
#include "arc-shell.h"


ARC_DEFINE_COMMAND (card_command)
{
  int i;
  arc_command_retval retval = ARC_CMD_SUCCESS;
  int cr_mode = ARC_SHELL_CURRENT_CONTEXT && ARC_SHELL_CURRENT_CONTEXT->cr_mode;

  *result = NULL;
  if (argc <  1)
    {
      ccl_error("error: wrong # of arguments\n");
      return ARC_CMD_ERROR;
    }

  for(i = 0; i < argc && retval == ARC_CMD_SUCCESS; i++)
    {
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type == ARC_CMDP_STRING)
	{
	  ar_identifier *id = ar_identifier_create (arg);

	  if (ar_mec5_has_relation (id))
	    {
	      ar_mec5_relation *R = ar_mec5_get_relation (id);
	      if (!cr_mode)
		{
		  ccl_display ("card (");
		  ar_identifier_log (CCL_LOG_DISPLAY, id);
		  ccl_display (") = ");
		}
	      ccl_display ("%.10g\n", ar_mec5_relation_get_cardinality (R));
	      ar_mec5_relation_del_reference (R);
	    }
	  else
	    {
	      ccl_error ("error: undefined relation '");
	      ar_identifier_log (CCL_LOG_ERROR, id);
	      ccl_error ("'.\n");
	      retval = ARC_CMD_ERROR;
	    }
	  ar_identifier_del_reference (id);
	}
      else
	{
	  ccl_error ("error: wrong arg type (#%d).\n",i + 1);
	  retval = ARC_CMD_ERROR;
	}
    }

  return retval;
}

			/* --------------- */

