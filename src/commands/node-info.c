/*
 * node-info.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "ar-constraint-automaton.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "allcmds-p.h"

enum ca_field 
{
  CA_FLOW_VARS,
  CA_STATE_VARS,
  CA_EVENTS,
  CA_TRANS
};

struct subcommand 
{
  const char *name;
  arc_command_retval 
  (*cmd) (const char *nodeid, ar_node *node, int argc, 
	  arc_command_parameter *argv);
};

			/* --------------- */

static arc_command_retval
s_children_sc (const char *nodeid, ar_node *node, int argc, 
	       arc_command_parameter *argv);

static arc_command_retval
s_max_conf_card_sc (const char *nodeid, ar_node *node, int argc,  
		     arc_command_parameter *argv);

static arc_command_retval
s_flow_variables_sc (const char *nodeid, ar_node *node, int argc,  
		     arc_command_parameter *argv);

static arc_command_retval
s_state_variables_sc (const char *nodeid, ar_node *node, int argc,  
		      arc_command_parameter *argv);

static arc_command_retval
s_events_sc (const char *nodeid, ar_node *node, int argc,  
	     arc_command_parameter *argv);

static arc_command_retval
s_transitions_sc (const char *nodeid, ar_node *node, int argc,  
		  arc_command_parameter *argv);

static arc_command_retval
s_dd_order_sc (const char *nodeid, ar_node *node, int argc,  
	       arc_command_parameter *argv);

static arc_command_retval 
s_log_ca_field (enum ca_field field, const char *nodeid, ar_node *node, 
		int argc, arc_command_parameter *argv);

static void
s_check_string_arg (int index, arc_command_parameter *argv, const char **parg);

			/* --------------- */

static struct subcommand SUBCOMMANDS[] = {
  { "children", s_children_sc },
  { "max-conf-card", s_max_conf_card_sc },
  { "flow-variables", s_flow_variables_sc },
  { "state-variables", s_state_variables_sc },
  { "events", s_events_sc },
  { "transitions", s_transitions_sc },
  { "varorder", s_dd_order_sc },
  { NULL, NULL }
};

			/* --------------- */

ARC_DEFINE_COMMAND (node_info_command)
{
  ar_node *node;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (argc < 2)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  ccl_try (exception)
    {
      int i;
      int scindex;
      const char *nodeid;
      const char *cmd;
      struct subcommand *sc;

      s_check_string_arg (0, argv, &nodeid);
      s_check_string_arg (1, argv, &cmd);

      node = arc_command_get_node (nodeid);
      if (node == NULL)
	ccl_throw_no_msg (exception);

      scindex = -1;
      for (i = 0, sc = SUBCOMMANDS; sc->name && retval == ARC_CMD_SUCCESS;
	   sc++, i++)
	{
	  if (ccl_string_equals (cmd, sc->name))
	    {
	      scindex = i;
	      break;
	    }
	  else if (ccl_string_has_prefix (cmd, sc->name) != NULL)
	    {
	      if (scindex >= 0)
		{
		  ccl_error ("error: ambiguous dictionary name '%s'.\n", cmd);
		  retval = ARC_CMD_ERROR;
		}
	      else
		{
		  scindex = i;
		}
	    }
	}
      if (retval == ARC_CMD_SUCCESS)
	{
	  if (scindex < 0)
	    {
	      ccl_error ("error: unknown command '%s'.\n", cmd);
	      retval = ARC_CMD_ERROR;
	    }
	  else
	    {
	      retval = SUBCOMMANDS[scindex].cmd (nodeid, node, argc, argv);
	    }
	}
      ar_node_del_reference (node);
    }
  ccl_catch
    {
      retval = ARC_CMD_ERROR;
    }
  ccl_end_try;

  return retval;
}

			/* --------------- */

static arc_command_retval
s_children_sc (const char *nodeid, ar_node *node, int argc, 
	     arc_command_parameter *argv)
{
  ar_context_slot_iterator *sli = ar_node_get_slots_for_subnodes (node);

  while (ccl_iterator_has_more_elements (sli))
    {
      ar_context_slot *sl = ccl_iterator_next_element (sli);
      ar_identifier *id = ar_context_slot_get_name (sl);
      ar_type *type = ar_context_slot_get_type (sl);
      ar_node *subtype = ar_type_bang_get_node (type);
      ar_identifier *subtypename = ar_node_get_name (subtype);

      ar_identifier_log (CCL_LOG_DISPLAY, id);
      ccl_display (" : ");
      ar_identifier_log_quote (CCL_LOG_DISPLAY, subtypename);
      ccl_display ("\n");

      ar_type_del_reference (type);
      ar_node_del_reference (subtype);
      ar_identifier_del_reference (id);
      ar_identifier_del_reference (subtypename);
      ar_context_slot_del_reference (sl);
    }
  ccl_iterator_delete (sli);

  return ARC_CMD_SUCCESS;
}

			/* --------------- */

static arc_command_retval
s_max_conf_card_sc (const char *nodeid, ar_node *node, 
		    int argc,  arc_command_parameter *argv)
{
  double result = 1.0;
  ar_node *fl = arc_command_get_flat (nodeid);
  ar_context_slot_iterator *sli = ar_node_get_slots_for_variables (fl);

  while (ccl_iterator_has_more_elements (sli))
    {
      ar_context_slot *sl = ccl_iterator_next_element (sli);
      ar_type *type = ar_context_slot_get_type (sl);

      result *= ar_type_get_cardinality (type);
      
      ar_type_del_reference (type);
      ar_context_slot_del_reference (sl);      
    }  
  ccl_iterator_delete (sli);
  ccl_display ("%g\n", result);
  ar_node_del_reference (fl);

  return ARC_CMD_SUCCESS;
}

			/* --------------- */

static arc_command_retval
s_flow_variables_sc (const char *nodeid, ar_node *node, int argc,  
		     arc_command_parameter *argv)
{
  return s_log_ca_field (CA_FLOW_VARS, nodeid, node, argc, argv);
}

			/* --------------- */

static arc_command_retval
s_state_variables_sc (const char *nodeid, ar_node *node, int argc,  
		      arc_command_parameter *argv)
{
  return s_log_ca_field (CA_STATE_VARS, nodeid, node, argc, argv);
}

			/* --------------- */

static arc_command_retval
s_events_sc (const char *nodeid, ar_node *node, int argc,  
	     arc_command_parameter *argv)
{
  return s_log_ca_field (CA_EVENTS, nodeid, node, argc, argv);
}

			/* --------------- */

static arc_command_retval
s_transitions_sc (const char *nodeid, ar_node *node, int argc,  
		  arc_command_parameter *argv)
{
  return s_log_ca_field (CA_TRANS, nodeid, node, argc, argv);
}

			/* --------------- */

static arc_command_retval 
s_log_ca_field (enum ca_field field, const char *nodeid, ar_node *node, 
		int argc, arc_command_parameter *argv)
{
  ar_ca *ca;
  arc_command_retval retval = ARC_CMD_SUCCESS;
  ccl_pointer_iterator *(*get_list) (ar_ca *ca);
  typedef void log_obj_proc(ccl_log_type log, void *obj, const char *sep, 
			       const char *quotes);
  log_obj_proc *log_obj;

  switch (field)
    {
    case CA_FLOW_VARS: 
      get_list = ar_ca_get_flow_variables_iterator;
      log_obj = (log_obj_proc *) ar_ca_expr_log;
      break;
    case CA_STATE_VARS: 
      get_list = ar_ca_get_state_variables_iterator;
      log_obj = (log_obj_proc *) ar_ca_expr_log;
      break;
    case CA_EVENTS:
      get_list = ar_ca_get_events_iterator;
      log_obj = (log_obj_proc *) ar_ca_event_log;
      break;
    case CA_TRANS:
      get_list = ar_ca_get_transitions_iterator;
      log_obj = (log_obj_proc *) ar_ca_trans_log;
      break;
    default:
      get_list = NULL;
      log_obj = NULL;
      break;
    }

  ccl_assert (get_list != NULL && log_obj != NULL);

  ca = arc_command_get_ca (nodeid);
  if (ca == NULL)
    retval = ARC_CMD_ERROR;
  else
    {
      ccl_pointer_iterator *it = get_list (ca);
      while (ccl_iterator_has_more_elements (it))
	{
	  void *obj = ccl_iterator_next_element (it);
	  log_obj (CCL_LOG_DISPLAY, obj, ".", NULL);
	  ccl_display ("\n");
	}
      ccl_iterator_delete (it);
      ar_ca_del_reference (ca);
    }

  return retval;
}

			/* --------------- */

static void
s_check_string_arg (int index, arc_command_parameter *argv, const char **parg)
{
 if (argv[index]->type != ARC_CMDP_STRING)
   {
     ccl_error ("error: bad argument (%d) type.\n", index);
     ccl_throw_no_msg (exception);
   }
 else
   {
     *parg = argv[index]->u.string_value;
   }
 }

			/* --------------- */

static arc_command_retval
s_dd_order_sc (const char *nodeid, ar_node *node, int argc,  
	       arc_command_parameter *argv)
{

  arc_command_retval retval = ARC_CMD_SUCCESS;
  ar_relsem *rs = arc_command_get_relsem (nodeid);

  if (rs == NULL)
    retval = ARC_CMD_ERROR;
  else
    {
      const varorder *order = ar_relsem_get_order (rs);
      varorder_log (CCL_LOG_DISPLAY, order);
      ar_relsem_del_reference (rs);
    }

  return retval;
}
