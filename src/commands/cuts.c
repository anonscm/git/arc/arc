/*
 * cuts.c -- Commands for set of failure sequences computation
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <deps/depgraph.h>
#include "ar-semantics.h"
#include "ar-ca-semantics.h"
#include "ar-interp-altarica.h"
#include "parser/altarica-input.h"
#include "ar-model.h"
#include "ar-constraint-automaton.h"
#include "sequences-generator.h"
#include "diagnosis/diagnosis.h"
#include "ar-ca-semantics.h"
#include "ar-ca-rewriters.h"
#include "allcmds.h"
#include "allcmds-p.h"
#include "ar-util.h"

enum generator
  {
    CUTS,
    SEQUENCES,
    FAULT_TREE
  };

static int
s_compute_sequences (ar_ca *ca, const char *formula, enum generator gen, 
		     ccl_set *visible_tags, ccl_set *disabled_tags, int bound,
		     int mincuts, int output_boolean_formula,
		     const char *prefix);

			/* --------------- */

ARC_DEFINE_COMMAND (cuts_command)
{
  arc_command_retval retval = ARC_CMD_SUCCESS;
  int i;
  const char *prefix = "";
  ccl_set *visible_tags;
  ccl_set *disabled_tags;
  int mincuts = 0;
  int bound = 0;
  int bf = 1;
  enum generator gen = CUTS;

  *result = NULL;

  if (argc < 2)
    {
      ccl_error ("error: wrong # of arguments.\n");
      return ARC_CMD_ERROR;
    }
  
  for (i = 0; retval == ARC_CMD_SUCCESS && i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument (#%d) type.\n", i + 1);
	  return ARC_CMD_ERROR;
	}
    }
      
  visible_tags =
    ccl_set_create_for_objects (NULL, NULL, (ccl_delete_proc *)
				ar_identifier_del_reference);
  disabled_tags =
    ccl_set_create_for_objects (NULL, NULL, (ccl_delete_proc *)
				ar_identifier_del_reference);
  if (ccl_string_equals (cmd, "sequences"))
    gen = SEQUENCES;
  else if (ccl_string_equals (cmd, "diag"))
    gen = FAULT_TREE;
  else
    ccl_assert (ccl_string_equals (cmd, "cuts"));

  while (retval == ARC_CMD_SUCCESS && 
	 argc && argv[0]->u.string_value[0] == '-')
    {
      const char *arg = argv[0]->u.string_value;
      const char *val;

      if (ccl_string_equals (arg, "--min"))
	{
	  if (gen == FAULT_TREE)
	    {
	      ccl_error ("error: invalid option '%s'.\n", 
			 argv[0]->u.string_value);
	      retval = ARC_CMD_ERROR;
	    }
	  else
	    mincuts = 1;
	}
      else if (ccl_string_equals (arg, "--enum"))
	{
	  if (gen == FAULT_TREE)
	    {
	      ccl_error ("error: invalid option '%s'.\n", 
			 argv[0]->u.string_value);
	      retval = ARC_CMD_ERROR;
	    }
	  else
	    bf = 0;
	}
      else if ((val = ccl_string_has_prefix ("--order=", arg)))
	{
	  if (gen == SEQUENCES)
	    bound = atoi (val);
	  else
	    {
	      ccl_error ("error: invalid option '%s'.\n", 
			 argv[0]->u.string_value);
	      retval = ARC_CMD_ERROR;
	    }
	}
      else if ((val = ccl_string_has_prefix ("--prefix=", arg)))
	prefix = val;
      else if ((val = ccl_string_has_prefix ("--visible-tags=", arg)))
	visible_tags = ar_parse_id_list (val, visible_tags);
      else if ((val = ccl_string_has_prefix ("--disabled-tags=", arg)))
	disabled_tags = ar_parse_id_list (val, disabled_tags);
      else
	{
	  ccl_error ("error: unknown option '%s'.\n", 
		     argv[0]->u.string_value);
	  retval = ARC_CMD_ERROR;
	}
      argv++;
      argc--;
    }

  if (retval != ARC_CMD_SUCCESS && argc != 2)
    {
      ccl_error ("error: wrong # of arguments.\n");
      retval = ARC_CMD_ERROR;
    }
  
  if (retval != ARC_CMD_SUCCESS && gen == SEQUENCES && bound <= 0)
    {
      ccl_error ("invalid order %d. it should be > 0.\n", bound);
      retval = ARC_CMD_ERROR;
    }

  if (retval == ARC_CMD_SUCCESS)
    {
      const char *nodename = argv[0]->u.string_value;
      const char *formula = argv[1]->u.string_value;
      ar_ca *ca = arc_command_get_ca (nodename);

      if (ca == NULL)
	{
	  ccl_error ("error: unknown node '%s'.\n", nodename);	  
	  retval = ARC_CMD_ERROR;
	}
      else
	{
	  CCL_DEBUG_START_TIMED_BLOCK (("compute %s", cmd));
	  if (! s_compute_sequences (ca, formula, gen, visible_tags, 
				     disabled_tags, bound, mincuts, bf,
				     prefix))
	    retval = ARC_CMD_ERROR;
	  CCL_DEBUG_END_TIMED_BLOCK ();
	  ar_ca_del_reference (ca);
	}
    }
  ccl_set_delete (visible_tags);
  ccl_set_delete (disabled_tags);
  
  return retval;
}

			/* --------------- */

static int
s_compute_sequences (ar_ca *srcca, const char *formula, enum generator gen,
		     ccl_set *visible_tags, ccl_set *disabled_tags, 
		     int bound, int mincuts, int output_boolean_formula,
		     const char *prefix)
{
  ar_ca_exprman *man;
  ar_node *node;
  ar_context *ctx;
  ar_expr *goal;
  ar_ca *ca;
  ar_expr *flat_goal = NULL;
  ar_ca_expr *ca_goal = NULL;

  char *tmp = ccl_string_format_new ("@%s@", formula);
  altarica_tree *t = altarica_read_string (tmp, "command line", AR_INPUT_AEXPR, 
					   NULL, NULL);

  ccl_string_delete (tmp);

  if (t == NULL)
    return 0;

  if (gen == FAULT_TREE)
    ca = ar_ca_duplicate (srcca);
  else
    ca = ar_ca_duplicate (ar_ca_get_reduced (srcca));

  man = ar_ca_get_expression_manager(ca);
  node = ar_ca_get_node (ca);
  ctx = ar_node_get_context (node);
  goal = NULL;
  
  ccl_try (exception)
    {
  
      goal = ar_interp_boolean_expression (t->child, ctx,
					   ar_interp_acheck_member_access);
      flat_goal = ar_expr_remove_composite_slots (goal, ctx);      
      ca_goal = ar_model_expr_to_ca_expr (flat_goal, man, NULL);
      
      if (gen != FAULT_TREE)
	{
	  const fdset *fds = ar_ca_get_fdset (srcca);
	  ar_ca_expr *tmp = ar_ca_rewrite_expr_with_fd (fds, ca_goal, 0);
	  ar_ca_expr_del_reference (ca_goal);
	  ca_goal = tmp;
	  ar_ca_trim_to_reachables_from_formula (ca, ca_goal);
	}            
  
      CCL_DEBUG_START_TIMED_BLOCK (("apply algorithm on reduced CA"));

      switch (gen)
	{
	case SEQUENCES:
	  ar_orderered_cuts_generator (ca, ca_goal, visible_tags, disabled_tags,
				       bound, mincuts, output_boolean_formula,
				       CCL_LOG_DISPLAY, prefix);
	  break;
	case CUTS:
	  ar_cuts_generator (ca, ca_goal, visible_tags, disabled_tags, 
			     mincuts, output_boolean_formula, CCL_LOG_DISPLAY,
			     prefix);
	  break;
	default:
	  ccl_assert (gen == FAULT_TREE);
	  diagnosis_generator (ca, ca_goal, visible_tags, disabled_tags, 
			       prefix, CCL_LOG_DISPLAY);
	  break;
	}
      CCL_DEBUG_END_TIMED_BLOCK ();
    }
  ccl_no_catch;
  ccl_zdelete (ar_expr_del_reference, flat_goal);
  ccl_zdelete (ar_ca_expr_del_reference, ca_goal);
  ccl_zdelete (ar_expr_del_reference, goal);
  ar_context_del_reference (ctx);
  ar_node_del_reference (node);
  ar_ca_exprman_del_reference (man);
  ar_ca_del_reference (ca);
  ccl_parse_tree_delete_tree (t);

  return 1;
}

			/* --------------- */

