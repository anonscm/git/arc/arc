/*
 * show-ts-marks.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "allcmds-p.h"
#include "allcmds.h"

static void
s_display_property_content (ar_ts *ts, ar_ts_mark *mark, int is_state)
{
  int i;

  ccl_display ("/**\n"
	       " * Content of the %s property:\n",
	       is_state ? "state" : "transition");

  for (i = ar_ts_mark_get_first (mark); i >= 0; 
       i = ar_ts_mark_get_next (mark, i))
    {
      ccl_display (" * ");
      if (is_state)
	ar_ts_state_log (CCL_LOG_DISPLAY, ts, i);
      else
	ar_ts_trans_log (CCL_LOG_DISPLAY, ts, i);
      ccl_display ("\n");
    }
  ccl_display (" */\n");
}

			/* --------------- */

ARC_DEFINE_COMMAND (show_ts_marks_command)
{
  int i;
  ar_ts *ts;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  if (argc <  2)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  if (argv[0]->type != ARC_CMDP_STRING)
    {
      ccl_error ("error: wrong arg type (#%d).\n", 1);

      return ARC_CMD_ERROR;
    }

  ts = arc_command_get_ts (argv[0]->u.string_value);
  if (ts == NULL)
    return ARC_CMD_ERROR;
  
  for (i = 1; i < argc && retval == ARC_CMD_SUCCESS; i++)
    {
      char *arg = argv[i]->u.string_value;

      if (argv[i]->type == ARC_CMDP_STRING)
	{
	  ar_identifier *id = ar_identifier_create (arg);
	  int is_state = 1;
	  ar_ts_mark *mark = ar_ts_get_state_mark (ts, id);

	  if (mark == NULL)
	    {
	      mark = ar_ts_get_trans_mark (ts, id);
	      is_state = 0;
	    }
	  
	  if (mark == NULL)
	    {
	      ccl_error ("error: the property '");
	      ar_identifier_log (CCL_LOG_ERROR, id);
	      ccl_error ("' is not defined for this TS.\n");
	      retval = ARC_CMD_ERROR;
	    }
	  else
	    {
	      s_display_property_content (ts, mark, is_state);
	      ar_ts_mark_del_reference (mark);
	    }

	  ar_identifier_del_reference (id);
	}
      else
	{
	  ccl_error ("error: wrong arg type (#%d).\n", i + 1);
	  retval = ARC_CMD_ERROR;
	}
    }

  ar_ts_del_reference (ts);

  return retval;
}

			/* --------------- */

