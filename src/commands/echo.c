/*
 * echo.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "allcmds.h"

ARC_DEFINE_COMMAND (echo_command)
{
  int i;
  ar_identifier *id;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  for (i = 0; i < argc; i++)
    {
      switch (argv[i]->type) 
	{
	case ARC_CMDP_INTEGER : 
	  ccl_display ("%d", argv[i]->u.int_value); 
	  break;

	case ARC_CMDP_FLOAT : 
	  ccl_display ("%g",argv[i]->u.flt_value); 
	  break;

	case ARC_CMDP_IDENTIFIER :
	  break;

	case ARC_CMDP_STRING : 
	  if (argv[i]->u.string_value[0] == '$')
	    {
	      id = ar_identifier_create (argv[i]->u.string_value + 1);
	      if (ar_model_has_parameter (id))
		{
		  ar_expr *value = ar_model_get_parameter_value (id);
		  
		  if (value == NULL)
		    ar_identifier_log (CCL_LOG_DISPLAY, id);
		  else 
		    {
		      ar_expr_log (CCL_LOG_DISPLAY, value);
		      ar_expr_del_reference (value);
		    }
		}
	      else if (ar_model_has_type (id))
		{
		  ar_type *d = ar_model_get_type (id);
		  ar_type_log (CCL_LOG_DISPLAY, d);
		  ar_type_del_reference (d);
		}
	      else if (ar_model_has_node (id))
		{
		  ar_node *n = ar_model_get_node (id);
		  ar_node_log (CCL_LOG_DISPLAY, n, 0);
		  ar_node_del_reference (n);
		}
	      else
		{
		  ar_identifier_log (CCL_LOG_DISPLAY, id);
		}
	      ar_identifier_del_reference (id);
	    }
	  else
	    {
	      ccl_display ("%s", argv[i]->u.string_value);
	    }
	  break;

	case ARC_CMDP_CUSTOM :
	  ccl_display ("<obj@%p>\n", argv[i]->u.custom);
	  break;
	}
      if (i + 1 < argc)
	ccl_display (" ");
    }
  ccl_display ("\n");

  return retval;
}

