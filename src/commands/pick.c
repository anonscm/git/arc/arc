/*
 * pick.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "mec5/mec5.h"
#include "allcmds.h"

ARC_DEFINE_COMMAND (pick_command)
{
  int i;
  ar_identifier *relid;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;
  if (! (1 <= argc && argc <= 3))
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  for (i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: wrong argument type\n");
	  
	  return ARC_CMD_ERROR;
	}
    }

  relid = ar_identifier_create (argv[0]->u.string_value);

  if (ar_mec5_has_relation (relid))
    {
      ar_mec5_relation *mrel = ar_mec5_get_relation (relid);
      ar_mec5_relation *newrel = ar_mec5_relation_pick_one_element (mrel);

      ccl_assert (mrel != NULL);

      ar_mec5_relation_del_reference (mrel);

      ar_mec5_relation_log (CCL_LOG_DISPLAY, newrel);
      ccl_display("\n");
      if (argc >= 2)
	{
	  ar_identifier *newid = ar_identifier_create (argv[1]->u.string_value);
	  ar_signature *sig = ar_context_get_signature (MEC5_CONTEXT, relid);
	  ar_signature *newsig = ar_signature_rename (sig, newid);

	  ccl_assert (sig != NULL);
	  ar_context_add_signature (MEC5_CONTEXT, newid, newsig);
	  ar_mec5_add_relation (newid, newrel);
	  ar_signature_log (CCL_LOG_WARNING, newsig);
	  ccl_warning ("\n");
	  ccl_warning ("%.0g elements/%.0g nodes\n",
		       ar_mec5_relation_get_cardinality (newrel),
		       ar_mec5_relation_get_data_structure_size (newrel));

	  ar_signature_del_reference (sig);
	  ar_signature_del_reference (newsig);
	  ar_identifier_del_reference (newid);
	}
      ar_mec5_relation_del_reference (newrel);
    }
  else
    {
      ccl_error ("error: undefined relation '");
      ar_identifier_log (CCL_LOG_ERROR, relid);
      ccl_error ("'.\n");
      retval = ARC_CMD_ERROR;
    }
  ar_identifier_del_reference (relid);


  return retval;
}

			/* --------------- */

