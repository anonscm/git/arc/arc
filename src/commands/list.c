/*
 * list.c -- Display lists of AltaRica objects and other things
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "mec5/mec5.h"
#include "arc-shell.h"
#include "table-ids.h"
#include "allcmds.h"

#define IN_ALL 1
#define NOT_IN_ALL (!IN_ALL)

struct table
{
  const char *name;
  void (*log_ids) (ccl_log_type log, int cr_mode);
  ccl_list *(*get_ids) (void);
  int in_all;
};

			/* --------------- */

static void
s_list_objects (ccl_log_type log, int cr_mode, const struct table *table);

static void
s_log_nodes (ccl_log_type log, int cr_mode);

static void
s_log_root_nodes (ccl_log_type log, int cr_mode);

static void
s_log_commands (ccl_log_type log, int cr_mode);

			/* --------------- */

static const struct table TABLES[] =
  {
    { TABLE_CONSTANTS, NULL, ar_model_get_parameters, IN_ALL },
    { TABLE_DOMAINS, NULL, ar_model_get_types, IN_ALL },
    { TABLE_NODES, s_log_nodes, NULL, IN_ALL },
    { "root-nodes", s_log_root_nodes, NULL, NOT_IN_ALL },
    { TABLE_SIGNATURES, NULL, ar_model_get_signatures, IN_ALL },
    { TABLE_RELATIONS, NULL, ar_mec5_get_relations, IN_ALL },
    { TABLE_COMMANDS, s_log_commands, NULL, NOT_IN_ALL },
    { NULL, NULL }
  };


			/* --------------- */

ARC_DEFINE_COMMAND (list_command)
{
  char *arg;
  const struct table *t;
  arc_command_retval retval = ARC_CMD_SUCCESS;
  int cr_mode = (ARC_SHELL_CURRENT_CONTEXT && 
		 ARC_SHELL_CURRENT_CONTEXT->cr_mode);

  *result = NULL;
  if (argc != 1)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  if (argv[0]->type != ARC_CMDP_STRING)
    {
      ccl_error ("error: bad argument (#0)\n");

      return ARC_CMD_ERROR;
    }
  
  arg = argv[0]->u.string_value;
  if (ccl_string_equals (arg, "all"))
    {
      for (t = TABLES; t->name != NULL; t++)
	if (t->in_all)
	  s_list_objects (CCL_LOG_DISPLAY, cr_mode, t);
    }
  else if (ccl_string_equals (arg, "kinds"))
    {
      const char *sep = cr_mode ? "\n" : ", ";

      for (t = TABLES; t->name != NULL; t++)
	{
	  ccl_display ("%s", t->name);
	  if (t[1].name != NULL)
	    ccl_display (sep);
	}
      ccl_display ("\n");
    }
  else 
    {
      int i;
      int tindex = -1;
      for (i = 0, t = TABLES; t->name != NULL; t++, i++)
	{
	  if (ccl_string_equals (arg, t->name)) 
	    {
	      s_list_objects (CCL_LOG_DISPLAY, cr_mode, t);
	      return ARC_CMD_SUCCESS;
	    }
	  else if (ccl_string_has_prefix (arg, t->name) != NULL)
	    {
	      if (tindex >= 0)
		{
		  ccl_error ("error: ambiguous kind '%s'\n", arg);
		  return ARC_CMD_ERROR;
		}
	      tindex = i;	      
	    }
	}
      if (tindex >= 0)
	{
	  s_list_objects (CCL_LOG_DISPLAY, cr_mode, &TABLES[tindex]);
	}
      else
	{
	  ccl_error ("error: unknown kind '%s'\n", arg);
	  retval = ARC_CMD_ERROR;
	}
    }

  return retval;
}

			/* --------------- */

static void
s_list_objects (ccl_log_type log, int cr_mode, const struct table *t)
{
  if (t->log_ids != NULL)
    t->log_ids (log, cr_mode);
  else
    {
      int first = 1;
      ccl_list *ids = t->get_ids ();
      const char *sep = cr_mode ? "\n" : ", ";

      while (! ccl_list_is_empty (ids))
	{
	  ar_identifier *id = ccl_list_take_first (ids);
	  
	  if (first)
	    {
	      if (! cr_mode) 
		ccl_log (log, "defined %s : ", t->name);
	      first = 0;
	    }
	  ar_identifier_log (log, id);
	  if (! ccl_list_is_empty (ids) || cr_mode)
	    ccl_log (log, sep);
	  ar_identifier_del_reference (id);
	}
      ccl_list_delete (ids);
      if (! cr_mode && !first) 
	ccl_log (log, "\n");	
    }
}

			/* --------------- */

static void
s_log_nodes_gen (ccl_log_type log, int only_roots, int cr_mode)
{
  ccl_pair *p;
  ar_idtable *roots;
  ccl_list *nodes = ar_model_get_nodes ();
  int nb_nodes;
  const char *sep = cr_mode ? "\n" : ", ";

  if (ccl_list_is_empty (nodes))
    {
      ccl_list_delete (nodes);
      return;
    }

  ccl_list_sort (nodes, (ccl_compare_func *)ar_identifier_lex_compare);
  nb_nodes = ccl_list_get_size (nodes);
  roots = ar_idtable_create (0, NULL, NULL);
  for (p = FIRST (nodes); p; p = CDR (p))
    ar_idtable_put (roots, CAR (p), CAR (p));

  for (p = FIRST (nodes); p; p = CDR (p))
    {
      ar_node *node = ar_model_get_node (CAR (p));
      
      if (! ar_node_is_leaf(node))
	{
	  ar_context_slot_iterator *subi = 
	    ar_node_get_slots_for_subnodes (node);
	  while (ccl_iterator_has_more_elements (subi))
	    {
	      ar_context_slot *sl = ccl_iterator_next_element (subi);
	      ar_type *type = ar_context_slot_get_type (sl);
	      ar_node *subtype = ar_type_bang_get_node (type);
	      ar_identifier *subnodeid = ar_node_get_name (subtype);
	      ar_idtable_remove (roots, subnodeid);
	      ar_identifier_del_reference (subnodeid);
	      ar_node_del_reference (subtype);
	      ar_type_del_reference (type);
	      ar_context_slot_del_reference (sl);
	    }
	  ccl_iterator_delete (subi);
	}
	
      ar_node_del_reference (node);
    }

  if (only_roots)
    {
      while (nb_nodes--)
	{
	  ar_identifier *nodename = ccl_list_take_first (nodes);
	  int is_root = ar_idtable_has (roots, nodename);
	  
	  if (only_roots && is_root)
	    ccl_list_add (nodes, nodename);
	  else
	    ar_identifier_del_reference (nodename);
	}
    }

  if (!ccl_list_is_empty (nodes))
    {
      if (!cr_mode)
	{
	  if (only_roots)
	    ccl_log (log, "defined root-nodes : ");
	  else
	    ccl_log (log, "defined nodes : ");
	}
      while (!ccl_list_is_empty (nodes))
	{
	  ar_identifier *nodename = ccl_list_take_first (nodes);
      
	  ar_identifier_log (log, nodename);
	  if (!only_roots && ar_idtable_has (roots, nodename))
	    ccl_log (log, "*");
      
	  if (! ccl_list_is_empty (nodes) || cr_mode)
	    ccl_log (log, sep);
	  ar_identifier_del_reference (nodename);
	}
      if (!cr_mode)
	ccl_log (log, "\n");
    }
  ccl_list_delete (nodes);
  ar_idtable_del_reference (roots);  
}

			/* --------------- */

static void
s_log_nodes (ccl_log_type log, int cr_mode)
{
  s_log_nodes_gen (log, 0, cr_mode);
}

static void
s_log_root_nodes (ccl_log_type log, int cr_mode)
{
  s_log_nodes_gen (log, 1, cr_mode);
}

			/* --------------- */

static void
s_log_commands (ccl_log_type log, int cr_mode)
{
  arc_command c = arc_command_get_all_commands ();

  while (c != NULL)
    {
      ccl_log (log, "%s", arc_command_get_name (c));
      c = arc_command_get_next_command (c);
      if (c != NULL)
	ccl_log (log, " ");
    }
  ccl_log (log, "\n");
}
