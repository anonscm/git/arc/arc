/*
 * allcmds-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ALLCMDS_P_H
# define ALLCMDS_P_H

# include <ts/ts.h>
# include "ar-constraint-automaton.h"
# include "ar-rel-semantics.h"

extern ar_node *
arc_command_get_node (const char *name);

extern ar_node *
arc_command_get_flat (const char *name);

extern ar_ca *
arc_command_get_ca (const char *name);

extern ar_ts *
arc_command_get_ts (const char *name);

extern ar_relsem *
arc_command_get_relsem (const char *name);

extern ar_ca_expr *
arc_command_get_ca_expr (ar_ca *ca, const char *formula);

#endif /* ! ALLCMDS_P_H */
