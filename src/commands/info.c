/*
 * info.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-model.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "allcmds-p.h"
#include "table-ids.h"

struct table
{
  const char *name;
  int (*has) (ar_identifier *id);
  void (*info) (ar_identifier *id);
  ccl_list *(*list)(void);
};

			/* --------------- */

#define s_info_for_parameter NULL
#define s_info_for_type NULL

static void 
s_info_for_node (ar_identifier *id);

#define s_info_for_signature NULL

static void 
s_info_for_relation (ar_identifier *id);

static void 
s_info_for_dd (ar_identifier *id);

static void 
s_info_for_dd_all_paths (ar_identifier *id);

static void 
s_info_for_order (ar_identifier *id);

			/* --------------- */


static const struct table TABLES[] =
  {
    { TABLE_CONSTANTS, ar_model_has_parameter, s_info_for_parameter,
      ar_model_get_parameters },
    { TABLE_DOMAINS, ar_model_has_type, s_info_for_type,
      ar_model_get_types },
    { TABLE_NODES, ar_model_has_node, s_info_for_node,
      ar_model_get_nodes },
    { TABLE_SIGNATURES, ar_model_has_signature, s_info_for_signature,
      ar_model_get_signatures },
    { TABLE_RELATIONS, ar_mec5_has_relation, s_info_for_relation,
      ar_mec5_get_relations },
    { "dd-assignments", ar_mec5_has_relation, s_info_for_dd,
      ar_mec5_get_relations },
    { "dd-all-paths", ar_mec5_has_relation, s_info_for_dd_all_paths,
      ar_mec5_get_relations },
    { "order", ar_mec5_has_relation, s_info_for_order,
      ar_mec5_get_relations },
    { NULL, NULL, NULL }
  };

			/* --------------- */

ARC_DEFINE_COMMAND (info_command)
{
  int i;
  int dict_index;
  const char *dict;
  const struct table *T = NULL;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (argc == 0)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  for (i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: bad argument (%d) type.\n", i);
	  
	  return ARC_CMD_ERROR;
	}
    }

  dict = argv[0]->u.string_value;
  dict_index = -1;
  for (i = 0, T = TABLES; T->name != NULL; T++, i++)
    {
      if (ccl_string_cmp(dict, T->name) == 0)
	{
	  dict_index = i;
	  break;
	}
      else if (ccl_string_has_prefix (dict, T->name) != NULL)
	{
	  if (dict_index >= 0)
	    {
	      ccl_error ("error: ambiguous dictionary name '%s'.\n", dict);
	      return ARC_CMD_ERROR;
	    }
	  dict_index = i;
	}
    }
  
  if (dict_index == -1)
    {
      ccl_error ("error: unknown dictionary '%s'.\n", dict);

      return ARC_CMD_ERROR;
    }
  
  T = &(TABLES[dict_index]);
  
  if (T->info == NULL)
    ccl_display ("nothing to say about this kind of objects\n");
  else if (argc == 1)
    {
      ccl_list *identifiers = T->list ();
      if (identifiers != NULL)
	{
	  int is_empty = ccl_list_is_empty (identifiers);
	  while (!is_empty)
	    {
	      ar_identifier *id = ccl_list_take_first (identifiers);
	      
	      T->info (id);
	      is_empty = ccl_list_is_empty (identifiers);
	      if (!is_empty)
		ccl_display ("\n");
	      ar_identifier_del_reference (id);
	    }
	  ccl_list_delete (identifiers);
	}
    }
  else 
    {    
      for (i = 1; i < argc; i++)
	{
	  char *arg = argv[i]->u.string_value;
	  ar_identifier *id = ar_identifier_create (arg);
      
	  if (T->has != NULL && T->has (id))
	    {
	      ccl_assert (T->info != NULL);
	      T->info (id);
	    }
	  else
	    {
	      ccl_warning ("unknown identifier '%s'.\n", arg);
	    }
	  if (i < argc - 1)
	    ccl_display ("\n");
	  ar_identifier_del_reference (id);
	}
    }

  return retval;
}

			/* --------------- */

static void 
s_info_for_relation (ar_identifier *id)
{
  ar_mec5_relation *mrel = ar_mec5_get_relation (id);
  ar_signature *sig = ar_context_get_signature (MEC5_CONTEXT, id);

  ccl_assert (sig != NULL);
  ar_signature_log (CCL_LOG_DISPLAY, sig);
  ccl_display ("\n");
  ar_signature_del_reference (sig);

  ccl_display ("cardinality         : %0.f\n",
	       ar_mec5_relation_get_cardinality (mrel));
  ccl_display ("data structure size : %0.f\n",
	       ar_mec5_relation_get_data_structure_size (mrel));
  ar_mec5_relation_del_reference (mrel);
}


			/* --------------- */

static void
s_log_node_structure (int tab, ar_node *node)
{
  if (ar_node_get_template (node) != NULL)
    {
      ar_identifier *id = ar_node_get_name (node);
      ar_identifier_log (CCL_LOG_DISPLAY, id);
      ccl_display (" is a template node. No info available.\n");
      ar_identifier_del_reference (id);
    }
  else
    {
      ar_context_slot_iterator *si = ar_node_get_slots_for_subnodes (node);
      while (ccl_iterator_has_more_elements (si))
	{
	  ar_context_slot *sl = ccl_iterator_next_element (si);
	  ar_identifier *sname = ar_context_slot_get_name (sl);
	  ar_node *sub = ar_node_get_subnode_model (node, sname);
	  ar_identifier *subname = ar_node_get_name (sub);

	  ccl_display ("%*s", tab, "");
	  ar_identifier_log (CCL_LOG_DISPLAY, sname);
	  ccl_display (" : ");
	  ar_identifier_log (CCL_LOG_DISPLAY, subname);
	  ccl_display (" \n");

	  ar_identifier_del_reference (sname);
	  ar_identifier_del_reference (subname);
	  ar_context_slot_del_reference (sl);

	  s_log_node_structure (tab + 2, sub);
	  ar_node_del_reference (sub);
	}

      ccl_iterator_delete (si);
    }
}

			/* --------------- */

static void 
s_info_for_node (ar_identifier *id)
{
  ar_node *node = ar_model_get_node (id);

  if (ar_node_get_template (node) != NULL)
    {
      ar_identifier_log (CCL_LOG_DISPLAY, id);
      ccl_display (" is a template node. No info available.\n");
    }
  else
    {
      char *sid = ar_identifier_to_string (id);
      ar_ca *ca = arc_command_get_ca (sid);
      
      ccl_display ("=== %s ===\n", sid);
      ccl_string_delete (sid);

      ar_ca_log_info (CCL_LOG_DISPLAY, ca, 0);
      ar_ca_del_reference (ca);
      ccl_display ("hierarchy: \n");
      s_log_node_structure (1, node);
    }

  ar_node_del_reference (node);
}

static void 
s_info_for_order (ar_identifier *id)
{
  int i;
  ar_mec5_relation *mrel = ar_mec5_get_relation (id);
  int arity = ar_mec5_relation_get_arity (mrel);
  int *cols = ccl_new_array (int, arity);

  ar_mec5_relation_get_ordered_columns (mrel, cols);
  ccl_display ("DD variable order for relation '");
  ar_identifier_log (CCL_LOG_DISPLAY, id);
  ccl_display ("'\n");

  for (i = 0; i < arity; i++)
    {
      int c = cols[i];
      ar_identifier *id = ar_mec5_relation_get_ith_arg_name (mrel, c);
      int dd_index = ar_mec5_relation_get_ith_dd_index (mrel, c);

      ccl_display ("%5d - ", dd_index);
      ar_identifier_log (CCL_LOG_DISPLAY, id);
      ccl_display ("\n");
    }

  ccl_delete (cols);
  ar_mec5_relation_del_reference (mrel);
}

static void 
s_dd (ar_identifier *id, int all_paths)
{
  ar_mec5_relation *mrel = ar_mec5_get_relation (id);
  ar_dd DD = ar_mec5_relation_get_dd (mrel);

  ccl_display ("/* DD of relation  '");
  ar_identifier_log (CCL_LOG_DISPLAY, id);
  ccl_display ("' */ \n");
  ar_dd_display_decision_diagram_nodes_as_dot2 (MEC5_DDM, DD, CCL_LOG_DISPLAY,
						! all_paths);
  AR_DD_FREE (DD);

  ar_mec5_relation_del_reference (mrel);
}

static void 
s_info_for_dd (ar_identifier *id)
{
  s_dd (id, 0);
}

static void 
s_info_for_dd_all_paths (ar_identifier *id)
{
  s_dd (id, 1);
}
