/*
 * load.c -- Command for loading files into ARC.
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "arc-shell.h"
#include "allcmds.h"

struct parser_mode
{
  const char *name;
  int mask;
};

static struct parser_mode PARSER_MODES[] = {
  { "none", AR_INPUT_NONE }, 
  { "altarica", AR_INPUT_ALTARICA },
  { "mecv", AR_INPUT_MECV },
  { "acheck", AR_INPUT_ACHECK },
  { "arc", AR_INPUT_ARC },
  { "any", AR_INPUT_ANY },
  { NULL, 0 }
};

static void
s_set_modes (const char *arg, int *modes)
{
  struct parser_mode *pm;
  char *s;
  char *t;
  char *strmodes;
  
  *modes = AR_INPUT_NONE;
  t = strmodes = ccl_string_dup (arg);
  while (*t)
    {
      s = strchr (t, ',');
      if (s != NULL)
	*s = '\0';
      for (pm = PARSER_MODES; pm->name != NULL; pm++)
	{
	  if (ccl_string_equals (t, pm->name))
	    {
	      *modes |= pm->mask;
	      break;
	    }
	}
      if (pm->name == NULL)
	ccl_error ("unknown parser mode '%s'.\n", t);
      if (s == NULL)
	break;
      else
	t = s + 1;
    }      
}

ARC_DEFINE_COMMAND (load_command)
{
  arc_command_retval retval = ARC_CMD_SUCCESS;
  int i;
  int modes = AR_INPUT_ANY;
  
  *result = NULL;

  for (i = 0; i < argc; i++)
    {
      int cr_mode = 0;
      const char *args;
      
      if (argv[i]->type != ARC_CMDP_STRING)
	continue;

      if ((args= ccl_string_has_prefix("--modes=",
				       argv[i]->u.string_value)) != NULL)
	{
	  s_set_modes (args, &modes);
	  continue;
	}
      
      if (ARC_SHELL_CURRENT_CONTEXT)
	{
	  cr_mode = ARC_SHELL_CURRENT_CONTEXT->cr_mode;
	  ARC_SHELL_CURRENT_CONTEXT->cr_mode = 0;
	}
      
      if (!arc_shell_load_file(argv[i]->u.string_value, modes))
	{
	  arc_shell_error("error while reading the file '%s'.\n",
			  argv[i]->u.string_value);
	  retval = ARC_CMD_ERROR;
	}
      if (ARC_SHELL_CURRENT_CONTEXT)
	ARC_SHELL_CURRENT_CONTEXT->cr_mode = cr_mode;
    }

  return retval;
}

