/*
 * depgraph-cmd.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-interp-altarica-exception.h"
#include "ar-ca-rewriters.h"
#include <deps/fdset.h>
#include <deps/depgraph.h>
#include "ar-model.h"
#include "ar-semantics.h"
#include "ar-constraint-automaton.h"
#include "allcmds.h"
#include "allcmds-p.h"

struct depgraph_options
{
  const char *name;
  int flag_index;
  size_t len;
};

#define SECTION_HEADER(log, header) ccl_log ((log), "==== %s ====\n", header)

#define DEPGRAPH_OPTIONS(_enum, _name, _flag, _desc)
#define OPTIONS								\
  DEPGRAPH_OPTIONS (DEP_TRANS_ASSERT, "trans-assert", 0,		\
		    "display assertions from which transitions depend on.") \
									\
  DEPGRAPH_OPTIONS (DEP_TRANS_CLASSES, "trans-classes", 1,		\
		    "display classes of dependant transitions.")	\
									\
									\
  DEPGRAPH_OPTIONS (DEP_NO_GRAPH, "no-dep-graph", 2,			\
		    "do not display the dependency graph.")		\
									\
  DEPGRAPH_OPTIONS (DEP_FUNC_DEPS, "func-deps", 3,			\
		    "display functional dependencies for flow variables.") \
									\
  DEPGRAPH_OPTIONS (DEP_CONSTRAINTS, "constraints", 4,			\
		    "display remaining constraints")                    \
  DEPGRAPH_OPTIONS (DEP_DOT_FUNC_DEPS, "dot-func-deps", 5,		\
		    "display in DOT format functional dependencies for " \
		    "flow variables.")					\
  DEPGRAPH_OPTIONS (DEP_WITH_FUNC_DEPS, "with-func-deps", 6,		\
		    "compute functional dependencies.") \
  DEPGRAPH_OPTIONS (DEP_WITH_EVENTS, "with-events", 7,		\
		    "display nodes for events.") \



			/* --------------- */

#undef DEPGRAPH_OPTIONS
#define DEPGRAPH_OPTIONS(_enum, _name, _flag, _desc)	\
  _enum = (1 << _flag) , 

enum degraph_options_enum {
  OPTIONS
  LAST_OPTION
};

#undef DEPGRAPH_OPTIONS
#define DEPGRAPH_OPTIONS(_enum, _name, _flag, _desc)		\
  { _name, _flag, sizeof (_name) / sizeof(_name[0]) }, 

static const struct depgraph_options DG_OPTIONS[] = {
  OPTIONS
  { NULL, 0 }
};

			/* --------------- */

static void
s_functional_dependencies (ccl_log_type log, ar_ca *ca, int dot);

static void
s_constraints (ccl_log_type log, ar_ca *ca);

static void
s_transition_assertions_dependencies (ccl_log_type log, ar_ca *ca, depgraph *G);

static void
s_transition_groups (ccl_log_type log, ar_ca *ca, depgraph *G);


			/* --------------- */

ARC_DEFINE_COMMAND (depgraph_command)
{
  depgraph *G;
  ar_ca *ca = NULL;
  arc_command_retval retval = ARC_CMD_SUCCESS;
  int flags = 0;

  *result = NULL;

  if (argc == 0)
    return ARC_CMD_SUCCESS;
  
  ccl_try (exception)
    {
      int i;
      int compute_fdset;
      
      for (i = 0; i < argc; i++)
	{
	  char *arg = argv[i]->u.string_value;
	  
	  if (argv[i]->type != ARC_CMDP_STRING)
	    {
	      ccl_error ("error: wrong argument (#0) type.");
	      ccl_throw_no_msg (exception);
	    }
      
	  if (arg[0] == '-' && arg[1] == '-')
	    {
	      const struct depgraph_options *opt;

	      for (opt = DG_OPTIONS; opt->name; opt++)
		{
		  if (strncmp(opt->name, arg + 2, opt->len) == 0)
		    {
		      flags |= 1 << opt->flag_index;
		      break;
		    }
		}
	      if (opt->name == NULL)
		{
		  ccl_error ("error: unknown option '%s'.\n", arg);
		  ccl_throw_no_msg (exception);		  
		}

	    }
	  else if (ca == NULL)
	    {
	      ca = arc_command_get_ca (arg);
	    }
	  else
	    {
	      ccl_error ("error: unexpected argument '%s'.\n", arg);
	      ccl_throw_no_msg (exception);
	    }
	}
   
      if (ca == NULL)
	ccl_throw_no_msg (exception);
      compute_fdset = ((flags & DEP_WITH_FUNC_DEPS) != 0);
      if (compute_fdset)
	ar_ca_reduce_assertions (ca);

      G = depgraph_compute (ca, compute_fdset);

      if (! (flags & DEP_WITH_EVENTS))
	{
	  ccl_graph *g = depgraph_get_graph (G);
	  ccl_vertex_iterator *pi = ccl_graph_get_vertices (g);
	  ccl_hash *objects = ccl_hash_create (NULL, NULL, NULL, NULL);
	  while (ccl_iterator_has_more_elements (pi))
	    {
	      ccl_vertex *v = ccl_iterator_next_element (pi);
	      void *data = ccl_vertex_get_data (v);
	      if (depgraph_get_kind (G, data) != DG_EVENT)
		{
		  ccl_hash_find (objects, data);
		  ccl_hash_insert (objects, data);
		}
	    }
	  ccl_iterator_delete (pi);
	  depgraph_induced_subgraph (G, objects);
	  ccl_hash_delete (objects);
	}
      
      if (! (flags & DEP_NO_GRAPH))
	depgraph_log (CCL_LOG_DISPLAY, G);

      if ((flags & DEP_TRANS_ASSERT))
	s_transition_assertions_dependencies (CCL_LOG_DISPLAY, ca, G);

      if ((flags & DEP_TRANS_CLASSES))
	s_transition_groups (CCL_LOG_DISPLAY, ca, G);

      if ((flags & (DEP_FUNC_DEPS|DEP_DOT_FUNC_DEPS)))
	s_functional_dependencies (CCL_LOG_DISPLAY, ca, 
				   flags & DEP_DOT_FUNC_DEPS);

      if ((flags & DEP_CONSTRAINTS))
	s_constraints (CCL_LOG_DISPLAY, ca);

      depgraph_delete (G);
    } 
  ccl_catch
    {
      retval = ARC_CMD_ERROR;
    }
  ccl_end_try;

  ccl_zdelete (ar_ca_del_reference, ca);

  return retval;
}

			/* --------------- */

struct collect_assertions 
{
  depgraph *dg;
  ccl_list *result;
};

static int
s_collect_assertions (ccl_vertex *v, void *data)
{
  struct collect_assertions *coldata = data;
  depgraph_object_kind kind = depgraph_get_vertex_kind (coldata->dg, v);

  if (kind == DG_CONSTRAINT)
    {
      void *object = depgraph_get_vertex_object (coldata->dg, v);
      ccl_list_add (coldata->result, object);
    }

  return 0;
}

			/* --------------- */

static void
s_transition_assertions_dependencies (ccl_log_type log, ar_ca *ca, depgraph *dg)
{
  int i;
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  struct collect_assertions data;
  ccl_graph *G = depgraph_get_graph (dg);
  data.dg = dg;
  data.result = ccl_list_create ();

  SECTION_HEADER (log, "dependencies of transitions / assertions");

  for (i = 0; i < nb_trans; i++)
    {
      ccl_vertex *v = ccl_graph_get_vertex (G, trans[i]);

      ccl_assert (v != NULL);
      ccl_graph_dfs (G, v, 0, s_collect_assertions, &data);
      ccl_log (log, "=== ");
      ar_ca_trans_log (log, trans[i]);
      ccl_log (log, "=== \n");
      while (!ccl_list_is_empty (data.result))
	{
	  const char *skind;

	  void *obj = ccl_list_take_first (data.result);
	  skind = "[C]";
	  ccl_log (log, "  %s ", skind);
	  ar_ca_expr_log (log, obj);
	  ccl_log (log, "\n");
	}
    }
  ccl_list_delete (data.result);
}

			/* --------------- */

struct Tdep_data {
  depgraph *dg;
  ccl_hash *Tdep;
};

static void
s_attach(ccl_graph *G, void *data, int comp, void *cbdata)
{
  ccl_list *scc;
  struct Tdep_data *tdepdata = cbdata;
  ccl_vertex *v = ccl_graph_get_vertex (G, data);

  if (depgraph_get_vertex_kind (tdepdata->dg, v) != DG_TRANSITION)
    return;

  if (ccl_hash_find (tdepdata->Tdep, (void *) (intptr_t) comp))
    scc = ccl_hash_get (tdepdata->Tdep);
  else
    {
      scc = ccl_list_create ();
      ccl_hash_insert (tdepdata->Tdep, scc);
    }
  ccl_list_add (scc, data);
}


static void
s_transition_groups (ccl_log_type log, ar_ca *ca, depgraph *G)
{
  int nb_scc;
  ccl_pointer_iterator *pi;
  
  struct Tdep_data data = { G, ccl_hash_create (NULL, NULL, NULL, NULL) };
  
  ccl_graph_compute_scc (depgraph_get_graph (G), s_attach, &data);

  nb_scc = ccl_hash_get_size (data.Tdep);

  SECTION_HEADER (log, "Classes of dependant transitions");

  ccl_log (log, ". there is %d classes. \n", nb_scc);
  ccl_log (log, ". there is %d transitions. \n", 
	   ar_ca_get_number_of_transitions (ca));

  pi = ccl_hash_get_elements (data.Tdep);
  while (ccl_iterator_has_more_elements (pi))
    {
      ccl_list *depclass = ccl_iterator_next_element (pi);
      ccl_log (log, "---- \n");
      while (!ccl_list_is_empty (depclass))
	{
	  ar_ca_trans *t = ccl_list_take_first (depclass);
	  ar_ca_trans_log (log, t);
	  ccl_log (log," \n");
	}
      ccl_list_delete (depclass);
    }
  ccl_iterator_delete (pi);  
  ccl_hash_delete (data.Tdep);  
}

			/* --------------- */

static void
s_functional_dependencies (ccl_log_type log, ar_ca *ca, int dot)
{
  const fdset *fds = ar_ca_get_fdset (ca);
  ccl_list *vars = fdset_get_ordered_variables (fds, 0);

  if (dot)
    {
      const ccl_pair *p;
      const ccl_list *vars = ar_ca_get_state_variables (ca);

      ccl_log (log, "digraph G {\n");  
      for (p = FIRST (vars); p; p = CDR (p))
	{
	  ccl_log (log, "\"");
	  ar_ca_expr_log (log, CAR (p));
	  ccl_log (log, "\"[style=filled, fillcolor=red];\n");
	}
    }
  else
    {
      SECTION_HEADER (log, "functional dependencies");
    }

  if (ccl_list_is_empty (vars))
    ccl_log (log, " there is no functional dependencies.\n");
  else
    {
      while (! ccl_list_is_empty (vars))
	{
	  ccl_pair *p;
	  ar_ca_expr *var = ccl_list_take_first (vars);
	  funcdep *fd = fdset_get_funcdep (fds, var);
	  const ccl_list *deps = funcdep_get_inputs (fd);

	  if (!dot)
	    {
	      ar_ca_expr_log (log, var);
	      ccl_log (log, " : \n");
	    }
	  for (p = FIRST (deps); p; p = CDR (p))
	    {
	      ar_ca_expr *v = CAR (p);

	      if (dot)
		{
		  ccl_log (log, "\"");
		  ar_ca_expr_log (log, var);
		  ccl_log (log, "\" -> \"");
		  ar_ca_expr_log (log, v);
		  ccl_log (log, "\";");
		}
	      else
		{
		  ccl_log (log, "  ");
		  ar_ca_expr_log (log, v);
		}
	      ccl_log (log, "\n");
	    }
	}
    }
  ccl_list_delete (vars);
  if (dot)
    ccl_log (log, "}");
  ccl_log (log, "\n");
}

			/* --------------- */

static void
s_constraints (ccl_log_type log, ar_ca *ca)
{
  const ccl_list *assertions = ar_ca_get_assertions (ca);

  SECTION_HEADER (log, "Constraints");

  if (ccl_list_is_empty (assertions))
    ccl_log (log, " there is no constraints.\n");
  else
    {
      const ccl_pair *p;
      for (p = FIRST (assertions); p; p = CDR (p))
	{
	  ccl_log (log, "  ");
	  ar_ca_expr_log (log, CAR (p));
	  ccl_log (log, "\n");
	}
    }
  ccl_log (log, "\n");
}
