/*
 * timer.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TIMER_H
# define TIMER_H

# include <ccl/ccl-config-table.h>

extern int
timer_cmd_init (ccl_config_table *prefs);

extern int
timer_cmd_terminate (void);

#endif /* ! TIMER_H */
