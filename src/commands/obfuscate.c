/*
 * obfuscate.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "arc-shell.h"
#include "obfuscation.h"
#include "allcmds.h"
#include "allcmds-p.h"

ARC_DEFINE_COMMAND (obfuscate_command)
{
  int i;
  const char *dump_table_filename = NULL;
  const char *dump_rev_table_filename = NULL;
  arc_command_retval retval = ARC_CMD_ERROR;
  obfuscation *o = NULL;

  *result = NULL;
  
  for(i = 0; i < argc; i++)
    {
      const char *val;
      char *opt = argv[i]->u.string_value;

      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: wrong arg type (#%d).\n", i + 1);
	  goto end;
	}

      if (*opt == '-')
	{
	  if ((val = ccl_string_has_prefix ("--algo=", opt)) != NULL)
	    {
	      if (ccl_string_equals (val, "none"))
		{
		  ccl_zdelete (obfuscation_delete, o);
		  o = obfuscation_crt_none ();
		}
	      else if (ccl_string_equals (val, "sequential"))
		{
		  ccl_zdelete (obfuscation_delete, o);
		  o = obfuscation_crt_sequential ();
		}
	      else
		{
		  ccl_error ("error: unknown algorithm '%s'.\n", val);
		  goto end;
		}
	    }
	  else if ((val = ccl_string_has_prefix ("--dump-table=", opt)) != NULL)
	    {
	      dump_table_filename = val;
	    }
	  else if ((val = ccl_string_has_prefix ("--dump-rev-table=", opt))
		   != NULL)
	    {
	      dump_rev_table_filename = val;
	    }
	  else
	    {
	      ccl_error ("error: unknown option '%s'.\n", opt);

	      goto end;
	    }
	}
      else
	{
	  break;
	}
    }

  if (o == NULL)
    o = obfuscation_crt_sequential ();

  for(; i < argc; i++)
    {
      if (argv[i]->type == ARC_CMDP_STRING)
	{
	  altarica_tree *file = NULL;
	  char *filename = 
	    arc_shell_find_file_in_path (argv[i]->u.string_value);

	  if (filename != NULL)
	    {
	      file = arc_shell_load_preprocessed_file (filename, AR_INPUT_ANY);
	      ccl_string_delete (filename);
	    }
	  
	  if (file == NULL)
	    {
	      ccl_error ("error: can't read file '%s'\n", 
			 argv[i]->u.string_value);
	      goto end;
	    }
	  else
	    {
	      ar_identifier_translator *it = 
		obfuscation_get_identifier_translator (o);
	      ar_tree_log (CCL_LOG_DISPLAY, file, it);
	      ccl_parse_tree_delete_tree (file);
	    }
	}
      else
	{
	  ccl_error ("error: wrong arg type (#%d).\n", i + 1);
	  goto end;
	}
    }

  if (dump_rev_table_filename != NULL) 
    obfuscation_dump_translation_table (dump_rev_table_filename, o, 1);
  if (dump_table_filename != NULL) 
    obfuscation_dump_translation_table (dump_table_filename, o, 0);
  retval = ARC_CMD_SUCCESS;
 end:
  ccl_zdelete (obfuscation_delete, o);

  return retval;
}
