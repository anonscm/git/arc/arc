/*
 * help.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-help.h"
#include "allcmds.h"

ARC_DEFINE_COMMAND (help_command)
{
  char *manpage = NULL;
  *result = NULL;

  if (argc == 0)
    manpage = "help";
  else if (argc == 1)
    {
      if (argv[0]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: wrong arg type (#%d).\n", 1);

	  return ARC_CMD_ERROR;
	}

      manpage = argv[0]->u.string_value;
    }
  else
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }
  
  ar_help_log_manpage (CCL_LOG_DISPLAY, manpage);

  return ARC_CMD_SUCCESS;
}
