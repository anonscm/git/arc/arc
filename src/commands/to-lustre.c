/*
 * to-lustre.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "parser/altarica-input.h"
#include "lustre/translators.h"
#include "ar-interp-altarica.h"
#include "ar-model.h"
#include "allcmds.h"

static void
s_to_lustre_node (ar_identifier *id, ccl_config_table *config)
{
  ar_node *node = ar_model_get_node (id);
  altarica_tree *t = ar_node_get_template (node);
  if (t == NULL)
    {
      ar_node_to_lustre (node, CCL_LOG_DISPLAY, config);
      ccl_display("\n");
    }
  else
    {
      ar_context *ctx = ar_node_get_context (node);

      ccl_try (altarica_interpretation_exception)
	{
	  ar_node *n = ar_interp_node_template (t, ctx, NULL);
	  ar_node_to_lustre (n, CCL_LOG_DISPLAY, config);
	  ar_node_del_reference (n);
	}
      ccl_no_catch;
      ar_context_del_reference (ctx);
    }
  ar_node_del_reference (node);
}

			/* --------------- */

ARC_DEFINE_COMMAND (to_lustre_command)
{
  char *arg = argv[0]->u.string_value;
  arc_command_retval retval = ARC_CMD_SUCCESS;

  *result = NULL;

  if (argc !=  1)
    {
      ccl_error ("error: wrong # of arguments\n");
      retval = ARC_CMD_ERROR;
    }
  else if (argv[0]->type == ARC_CMDP_STRING)
    {      
      ar_identifier *id = ar_identifier_create (arg);

      if (ar_model_has_node (id)) 
	s_to_lustre_node (id, config); 
      else
	ccl_error ("error: unknown node '%s'\n", arg);
      ar_identifier_del_reference (id);
    }
  else
    {
      ccl_error ("error: wrong arg type (#0).\n");
      retval = ARC_CMD_ERROR;
    }

  return retval;
}

			/* --------------- */
