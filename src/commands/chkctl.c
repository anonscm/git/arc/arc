/*
 * chkctl.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "parser/altarica-input.h"
#include "ar-interp-acheck.h"
#include "ctlstar.h"
#include "mec5/mec5.h"
#include "allcmds.h"
#include "allcmds-p.h"
#include "arc-shell.h"

#define TO_DOT_OPTION "--to-dot="
#define TAB_INC 2

static ar_sgs_set * 
s_show_s_in_F (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
	       ctlstar_formula *F, ar_sgs_set *Fsem, ar_idtable *tmpvars, 
	       ccl_config_table *conf);

static altarica_tree *
s_parse_ctlstar_formula (ar_sgs *sgs, const char *formula_text, 
			 ctlstar_formula **pF, ar_idtable **tmpvars)
			 
{
  char *f = ccl_string_format_new ("ctlspec %s", formula_text);
  altarica_tree *ftree = altarica_read_string (f, f, AR_INPUT_CTLSTAR,
					       NULL, NULL);
  if (ftree != NULL)
    {
      *tmpvars = ar_idtable_create (0, NULL, NULL);
      ccl_try (altarica_interpretation_exception)
        {
	  *pF = ar_interp_ctlstar_formula (ftree->child, sgs, 0, *tmpvars);
	}
      ccl_catch
	{
	  ccl_parse_tree_delete_tree (ftree);
	  ftree = NULL;
	}
      ccl_end_try;
    }

  ccl_string_delete (f);

  return ftree;
}

ARC_DEFINE_COMMAND (chkctl_command)
{
  int i;
  arc_command_retval retval = ARC_CMD_SUCCESS;
  ar_sgs *sgs = NULL;
  altarica_tree *ftree = NULL;
  ctlstar_formula *spec = NULL;
  ar_idtable *tmpvars = NULL;
  const char *dotfile;
  const char *nodename;
  const char *formula;

  *result = NULL;
  if (argc != 2 && argc != 3)
    {
      ccl_error ("error: wrong # of arguments\n");

      return ARC_CMD_ERROR;
    }

  for(i = 0; i < argc; i++)
    {
      if (argv[i]->type != ARC_CMDP_STRING)
	{
	  ccl_error ("error: wrong argument (%d) type\n", i + 1);

	  return ARC_CMD_ERROR;
	}
    }

  if (argc == 3)
    {
      int optlen = strlen (TO_DOT_OPTION);
      if (strncmp (argv[0]->u.string_value, TO_DOT_OPTION, optlen) != 0)
	{
	  ccl_error("error: invalid option '%s'.\n", argv[0]->u.string_value);
	  return ARC_CMD_ERROR;	  
	}
      dotfile = argv[0]->u.string_value + optlen;
      nodename = argv[1]->u.string_value;
      formula = argv[2]->u.string_value;
    }
  else
    {
      dotfile = NULL;
      nodename = argv[0]->u.string_value;
      formula = argv[1]->u.string_value;
    }
  ccl_try (altarica_interpretation_exception)
    {
      ar_sgs_set *set;

      sgs = (ar_sgs *) arc_command_get_relsem (nodename);
      ftree = 
	s_parse_ctlstar_formula (sgs, formula, &spec, &tmpvars);
      if (ftree)
	{
	  ar_sgs_set *dotS = NULL;
	  ar_sgs_set *tmp[2];	  
	  ar_sgs_set *init = ar_sgs_compute_initial (sgs);
	  
	  set = ar_interp_eval_ctlstar_formula (spec, sgs, config);

	  tmp[0] = ar_sgs_compute_difference (sgs, init, set);
	  if (ar_sgs_set_is_empty (sgs, tmp[0]))
	    {
	      ccl_display ("Property is satisfied by initial "
			   "configurations.\n");
	      ar_sgs_set_del_reference (tmp[0]);

	      ccl_display ("A witness of the property is:\n");	      
	      tmp[1] = ar_sgs_compute_pick (sgs, init);
	      ar_sgs_display_set (CCL_LOG_DISPLAY, sgs, tmp[1], 
				  AR_SGS_DISPLAY_ASIS);
	      ccl_display ("\n");	      
	      dotS = s_show_s_in_F (CCL_LOG_DISPLAY, 0, sgs, tmp[1], spec, set, 
				    tmpvars, config);
	      ar_sgs_set_del_reference (tmp[1]);
	    }
	  else
	    {
	      ctlstar_formula *not_spec = ctlstar_formula_crt_not (spec);

	      ccl_display ("Property is not satisfied by initial "
			   "configurations.\n"
			   "A counterexample of the property is:\n");	      
	      tmp[1] = ar_sgs_compute_pick (sgs, tmp[0]);
	      ar_sgs_set_del_reference (tmp[0]);

	      ar_sgs_display_set (CCL_LOG_DISPLAY, sgs, tmp[1],
				  AR_SGS_DISPLAY_ASIS);
	      ccl_display ("\n");	      
	      dotS = s_show_s_in_F (CCL_LOG_DISPLAY, 0, sgs, tmp[1], not_spec, 
				    NULL, tmpvars, config);
	      ar_sgs_set_del_reference (tmp[1]);
	      ctlstar_formula_del_reference (not_spec);
	    }
	  if (dotfile != NULL)
	    {
	      FILE *output = fopen (dotfile, "w");
	      if (output == NULL)
		ccl_error ("error: can't open file '%s'.\n", dotfile);
	      else
		{
		  ar_sgs_set *rsrc = ar_sgs_compute_rsrc (sgs, dotS);
		  ar_sgs_set *rtgt = ar_sgs_compute_rtgt (sgs, dotS);
		  ar_sgs_set *T = ar_sgs_compute_intersection (sgs, rsrc, rtgt);
		  ccl_log_redirect_to_FILE (CCL_LOG_DISPLAY, output);
		  ar_sgs_to_dot (CCL_LOG_DISPLAY, sgs, dotS, T, config);
		  ccl_log_pop_redirection (CCL_LOG_DISPLAY);
		  fflush (output);
		  fclose (output);
		  ar_sgs_set_del_reference (rsrc);
		  ar_sgs_set_del_reference (rtgt);
		  ar_sgs_set_del_reference (T);
		}
	    }
	  ar_sgs_set_del_reference (dotS);
	  ar_sgs_set_del_reference (set);
	  ar_sgs_set_del_reference (init);
	}
    }
  ccl_catch
    {
      retval = ARC_CMD_ERROR;
    }
  ccl_end_try;

  ccl_zdelete (ar_sgs_del_reference, sgs);
  ccl_zdelete (ctlstar_formula_del_reference, spec);
  ccl_zdelete (ccl_parse_tree_delete_tree, ftree);

  if (tmpvars != NULL)
    {
      ar_identifier_iterator *i = ar_idtable_get_keys (tmpvars);
      while (ccl_iterator_has_more_elements (i))
	{
	  ar_identifier *id = ccl_iterator_next_element (i);
	  ar_sgs_remove_state_set (sgs, id);
	  ar_identifier_del_reference (id);
	}
      ccl_iterator_delete (i);
      ar_idtable_del_reference (tmpvars);
    }

  return retval;
}

			/* --------------- */

static void
s_log_identifiers (ccl_log_type log, ar_identifier *id, void *data)
{
  if (ar_idtable_has (data, id))
    {
      altarica_tree *t = ar_idtable_get (data, id);
      ar_tree_log_ctlstar_expr (log, t, AR_NO_ID_TRANSLATION);
    }
  else
    {
      ar_identifier_log (log, id);
    }      
}

			/* --------------- */

static ar_sgs_set * 
s_show_s_in_atom (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
		  ctlstar_formula *F, ar_idtable *tmpvars)
{
  ccl_log (log, "%*s", tab, "");
  ar_sgs_display_set (log, sgs, s, AR_SGS_DISPLAY_ASIS);
  ccl_log (log, " belongs to '");

  if (ctlstar_formula_get_kind (F) == CTLSTAR_TRUE)
    ccl_log (log, "true");
  else if (ctlstar_formula_get_kind (F) == CTLSTAR_VAR)
    {
      ar_identifier *vname = ctlstar_formula_get_name (F);
      if (ar_idtable_has (tmpvars, vname))
	{
	  altarica_tree *text = ar_idtable_get (tmpvars, vname);
	  ar_tree_log_ctlstar_expr (log, text, AR_NO_ID_TRANSLATION);
	}
      else
	{
	  ar_identifier_log (log, vname);
	}
      ar_identifier_del_reference (vname);
    }      
  else
    {
      ctlstar_formula_log (log, F, s_log_identifiers, tmpvars);
    }

  ccl_log (log, "'\n");

  return ar_sgs_set_add_reference (s);
}

			/* --------------- */

static ar_sgs_set *
s_show_s_in_AND (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
		 ctlstar_formula *F, ar_idtable *tmpvars, 
		 ccl_config_table *conf)
{
  int i;
  int arity = ctlstar_formula_get_arity (F);
  ctlstar_formula **operands = ctlstar_formula_get_operands (F);
  ar_sgs_set *result = NULL;

  for (i = 0; i < arity; i++)
    {
      ar_sgs_set *tmp =
	s_show_s_in_F (log, tab + TAB_INC, sgs, s, operands[i], NULL, tmpvars, 
		       conf); 

      if (result == NULL)
	result = tmp;
      else
	{
	  ar_sgs_set *aux = ar_sgs_compute_union (sgs, result, tmp);
	  ar_sgs_set_del_reference (result);
	  ar_sgs_set_del_reference (tmp);
	  result = aux;
	}

      if (i < arity - 1)
	ccl_log (log, "%*sand\n", tab, "");
    }
  ccl_assert (result != NULL);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_show_s_in_OR (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
		ctlstar_formula *F, ar_idtable *tmpvars, ccl_config_table *conf)
{
  int i;
  int empty_clause = 1;
  int arity = ctlstar_formula_get_arity (F);
  ctlstar_formula **operands = ctlstar_formula_get_operands (F);
  ar_sgs_set *result = NULL;

  for (i = 0; i < arity && empty_clause; i++)
    {
      ar_sgs_set *set = ar_interp_eval_ctlstar_formula (operands[i], sgs, conf);
      ar_sgs_set *inter = ar_sgs_compute_intersection (sgs, s, set);
      empty_clause = ar_sgs_set_is_empty (sgs, inter);
      if (! empty_clause)
	result = s_show_s_in_F (log, tab + TAB_INC, sgs, s, operands[i], NULL, 
				tmpvars, conf);
      ar_sgs_set_del_reference (set);
      ar_sgs_set_del_reference (inter);
    }
  ccl_assert (result != NULL);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_compute_post (ar_sgs *sgs, ar_sgs_set *S)
{
  ar_identifier *seps_id = ar_identifier_create ("self_epsilon");
  ar_sgs_set *seps = ar_sgs_get_trans_set (sgs, seps_id);
  ar_sgs_set *tmp0 = ar_sgs_compute_rsrc (sgs, S);
  ar_sgs_set *tmp1 = ar_sgs_compute_difference (sgs, tmp0, seps);
  ar_sgs_set *result = ar_sgs_compute_tgt (sgs, tmp1);
  ar_sgs_set_del_reference (tmp1);
  ar_sgs_set_del_reference (tmp0);
  ar_sgs_set_del_reference (seps);
  ar_identifier_del_reference (seps_id);

  return result;
}


static ar_sgs_set *
s_show_transition_into (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
			ar_sgs_set *dst)
{
  ccl_hash *events;
  ar_sgs_set *tmp0 = s_compute_post (sgs, s);
  ar_sgs_set *tmp1;
  ar_sgs_set *tmp2 = ar_sgs_compute_intersection (sgs, tmp0, dst);
  ar_sgs_set *succ = ar_sgs_compute_pick (sgs, tmp2);

  ccl_assert (! ar_sgs_set_is_empty (sgs, tmp0));
  ccl_assert (! ar_sgs_set_is_empty (sgs, tmp2));

  ar_sgs_set_del_reference (tmp0);
  ar_sgs_set_del_reference (tmp2);

  ccl_assert (! ar_sgs_set_is_empty (sgs, succ));
  ccl_assert (ar_sgs_get_set_cardinality (sgs, succ) == 1);

  tmp0 = ar_sgs_compute_rsrc (sgs, s);
  tmp1 = ar_sgs_compute_rtgt (sgs, succ);
  tmp2 = ar_sgs_compute_intersection (sgs, tmp0, tmp1);
  ar_sgs_set_del_reference (tmp0);
  ar_sgs_set_del_reference (tmp1);

  ccl_assert (! ar_sgs_set_is_empty (sgs, tmp2));
  
  events = ar_sgs_set_get_events (sgs, tmp2);
  ar_sgs_set_del_reference (tmp2);

  ccl_log (log, "%*s", tab, "");
  ar_sgs_display_set (log, sgs, s, AR_SGS_DISPLAY_ASIS);
  ccl_log (log, " |- ");
  {
    ccl_pointer_iterator *ei = ccl_hash_get_elements (events);
    while (ccl_iterator_has_more_elements (ei))
      {
	ar_ca_event *e = ccl_iterator_next_element (ei);
	ar_ca_event_log (log, e);
	if (ccl_iterator_has_more_elements (ei))
	  ccl_log (log, ", ");
      }
    ccl_iterator_delete (ei);
  }
  ccl_hash_delete (events);

  ccl_log (log, " -> ");
  ar_sgs_display_set (log, sgs, succ, AR_SGS_DISPLAY_ASIS);
  ccl_log (log, "\n");

  return succ;
}

			/* --------------- */

static ar_sgs_set *
s_show_s_in_EG (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
		ctlstar_formula *F, ar_sgs_set *Fsem, ar_idtable *tmpvars, 
		ccl_config_table *conf)
{
  int loop = 0;
  ar_sgs_set *tmp[2];
  ctlstar_formula *not_until = *ctlstar_formula_get_operands (F);
  ctlstar_formula **operands = ctlstar_formula_get_operands (not_until);
  ctlstar_formula *g = ctlstar_formula_crt_not  (operands[1]);
  ar_sgs_set *gsem = ar_interp_eval_ctlstar_formula (g, sgs, conf);
  ar_sgs_set *stack = ar_sgs_set_add_reference (s);
  ar_sgs_set *result = ar_sgs_set_add_reference (s);

  s = ar_sgs_set_add_reference (s);
  if (Fsem == NULL)
    Fsem = ar_interp_eval_ctlstar_formula (F, sgs, conf);
  else
    Fsem = ar_sgs_set_add_reference (Fsem);

  tmp[0] = ar_sgs_compute_difference (sgs, Fsem, s);
  ar_sgs_set_del_reference (Fsem);
  Fsem = tmp[0];

  ccl_log (log, "%*sthe state starts the following path:\n", tab, "");
  tab += TAB_INC;
  while (! loop)
    {
      ar_sgs_set *aux;

      ccl_log (log, "%*sthe invariant holds:\n", tab, "");
      aux = s_show_s_in_F (log, tab + TAB_INC, sgs, s, g, gsem, tmpvars, conf);	
      tmp[0] = ar_sgs_compute_union (sgs, result, aux);
      ar_sgs_set_del_reference (result);
      ar_sgs_set_del_reference (aux);
      result = tmp[0];

      ccl_log (log, "%*sand\n", tab, "");
  
      /* check if there is a successor on the stack */
      tmp[0] = s_compute_post (sgs, s);
      tmp[1] = ar_sgs_compute_intersection (sgs, stack, tmp[0]);
      ar_sgs_set_del_reference (tmp[0]);
      if (! ar_sgs_set_is_empty (sgs, tmp[1]))
	{
	  tmp[0] = ar_sgs_compute_pick (sgs, tmp[1]);
	  ar_sgs_set_del_reference (tmp[1]);
	  tmp[1] = s_show_transition_into (log, tab + TAB_INC, sgs, s, tmp[0]);	
	  ar_sgs_set_del_reference (tmp[0]);
	  tmp[0] = ar_sgs_compute_union (sgs, result, tmp[1]);
	  ar_sgs_set_del_reference (result);
	  ar_sgs_set_del_reference (tmp[1]);
	  result = tmp[0];

	  ccl_log (log, "%*swhich closes the loop.\n", tab + TAB_INC, "");
	  loop = 1;
	}
      else
	{
	  ar_sgs_set_del_reference (tmp[1]);
	  tmp[0] = s_show_transition_into (log, tab + TAB_INC, sgs, s, Fsem);

	  tmp[1] = ar_sgs_compute_union (sgs, result, tmp[0]);
	  ar_sgs_set_del_reference (result);
	  result = tmp[1];

	  tmp[1] = ar_sgs_compute_union (sgs, stack, tmp[0]);
	  ar_sgs_set_del_reference (stack);
	  stack = tmp[1];

	  tmp[1] = ar_sgs_compute_difference (sgs, Fsem, tmp[0]);
	  ar_sgs_set_del_reference (Fsem);
	  Fsem = tmp[1];

	  ar_sgs_set_del_reference (s);
	  s = tmp[0];

	}	    
    }

  ar_sgs_set_del_reference (s);
  ar_sgs_set_del_reference (stack);
  ar_sgs_set_del_reference (Fsem);
  ar_sgs_set_del_reference (gsem);
  ctlstar_formula_del_reference (g);

  ccl_assert (result != NULL);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_show_s_in_E_not_U (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
		     ctlstar_formula *F, ar_sgs_set *Fsem, ar_idtable *tmpvars, 
		     ccl_config_table *conf)
{
  ar_sgs_set *tmp[2];
  ar_sgs_set *nfng;
  int stop = 0;
  int first = 1;
  ar_sgs_set *stack = ar_sgs_set_add_reference (s);
  ctlstar_formula *not_until = *ctlstar_formula_get_operands (F);
  ctlstar_formula **operands = ctlstar_formula_get_operands (not_until);
  ctlstar_formula *f = operands[0];
  ctlstar_formula *g = operands[1];
  ar_sgs_set *fsem = ar_interp_eval_ctlstar_formula (f, sgs, conf);
  ar_sgs_set *gsem = ar_interp_eval_ctlstar_formula (g, sgs, conf);
  ar_sgs_set *result = ar_sgs_set_add_reference (s);

  if (Fsem == NULL)
    Fsem = ar_interp_eval_ctlstar_formula (F, sgs, conf);
  else
    Fsem = ar_sgs_set_add_reference (Fsem);
  tmp[0] = ar_sgs_compute_difference (sgs, Fsem, s);
  ar_sgs_set_del_reference (Fsem);
  Fsem = tmp[0];

  s = ar_sgs_set_add_reference (s);

  tmp[0] = ar_sgs_compute_union (sgs, fsem, gsem);
  nfng = ar_sgs_compute_complement (sgs, tmp[0]);
  ar_sgs_set_del_reference (tmp[0]);
  
  while (! stop)
    {
      int in_nfng;
      ar_sgs_set *post;

      if (!first)
	ccl_log (log, "%*sand\n", tab, "");
      else
	first = 0;

      tmp[0] = ar_sgs_compute_intersection (sgs, nfng, s);
      in_nfng = ! ar_sgs_set_is_empty (sgs, tmp[0]);
      ar_sgs_set_del_reference (tmp[0]);

      if (in_nfng)
	{
	  ccl_log (log, "%*sstate ", tab, "");
	  ar_sgs_display_set (log, sgs, s, AR_SGS_DISPLAY_ASIS);
	  ccl_log (log, " doesn't satisfy both operands of Until.\n");
	  stop = 1;
	  continue;
	}

      tmp[0] = 
	s_show_s_in_F (log, tab + TAB_INC, sgs, s, f, fsem, tmpvars, conf);	
      tmp[1] = ar_sgs_compute_union (sgs, result, tmp[0]);      
      ar_sgs_set_del_reference (tmp[0]);
      ar_sgs_set_del_reference (result);
      result = tmp[1];

      ccl_log (log, "%*sand\n", tab + TAB_INC, "");

      post = s_compute_post (sgs, s);
      if (ar_sgs_set_is_empty (sgs, post))
	{
	  ar_sgs_set_del_reference (post);
	  ccl_log (log, "%*sstate ", tab + TAB_INC, "");
	  ar_sgs_display_set (log, sgs, s, AR_SGS_DISPLAY_ASIS);
	  ccl_log (log, " has no successor.\n");
	  stop = 1;
	  continue;	  
	}

      /* check if a successor is on the stack */
      tmp[0] = ar_sgs_compute_intersection (sgs, stack, post);

      if (! ar_sgs_set_is_empty (sgs, tmp[0]))
	{
	  ar_sgs_set_del_reference (post);
	  tmp[1] = ar_sgs_compute_pick (sgs, tmp[0]);
	  ar_sgs_set_del_reference (tmp[0]);

	  tmp[0] = s_show_transition_into (log, tab + TAB_INC, sgs, s, tmp[1]);
	  ccl_log (log, "%*swhich closes the loop.\n", tab + TAB_INC, "");
	  ar_sgs_set_del_reference (tmp[1]);

	  tmp[1] = ar_sgs_compute_union (sgs, result, tmp[0]);
	  ar_sgs_set_del_reference (result);
	  result = tmp[1];

	  ar_sgs_set_del_reference (tmp[0]);
	  stop = 1;
	  continue;
	}

      ar_sgs_set_del_reference (tmp[0]);
      ar_sgs_set_del_reference (post);

      /* take a successor in F */
      tmp[0] = s_show_transition_into (log, tab + TAB_INC, sgs, s, Fsem);

      tmp[1] = ar_sgs_compute_union (sgs, result, tmp[0]);
      ar_sgs_set_del_reference (result);
      result = tmp[1];

      /* put it on the stack */
      tmp[1] = ar_sgs_compute_union (sgs, stack, tmp[0]);
      ar_sgs_set_del_reference (stack);
      stack = tmp[1];

      /* remove if from F */
      tmp[1] = ar_sgs_compute_difference (sgs, Fsem, tmp[0]);
      ar_sgs_set_del_reference (Fsem);
      Fsem = tmp[1];

      ar_sgs_set_del_reference (s);
      s = tmp[0];
    }

  ar_sgs_set_del_reference (s);
  ar_sgs_set_del_reference (stack);
  ar_sgs_set_del_reference (fsem);
  ar_sgs_set_del_reference (Fsem);
  ar_sgs_set_del_reference (gsem);
  ar_sgs_set_del_reference (nfng);

  ccl_post (result != NULL);
  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_show_s_in_E (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
	       ctlstar_formula *F, ar_sgs_set *Fsem, ar_idtable *tmpvars, 
	       ccl_config_table *conf)
{
  ctlstar_formula *arg = *ctlstar_formula_get_operands (F);
  ctlstar_formula_kind kind = ctlstar_formula_get_kind (arg);
  ar_sgs_set *result = NULL;

  ccl_log (log, "%*s", tab, "");
  ar_sgs_display_set (log, sgs, s, AR_SGS_DISPLAY_ASIS);
  ccl_log (log, " belongs to ");
  ctlstar_formula_log (log, F, s_log_identifiers, tmpvars);
  ccl_log (log, " because:\n");

  switch (kind)
    {
    case CTLSTAR_TRUE: 
    case CTLSTAR_VAR:
    case CTLSTAR_NOT:
      result = s_show_s_in_atom (log, tab + TAB_INC, sgs, s, arg, tmpvars);
      break;

    case CTLSTAR_AND:
    case CTLSTAR_OR:
      ccl_unreachable ();
      break;


    case CTLSTAR_NOT_U:
      {
	ctlstar_formula **operands = ctlstar_formula_get_operands (arg);
	ctlstar_formula *f = operands[0];

	if (ctlstar_formula_get_kind (f) == CTLSTAR_TRUE) 
	  result = s_show_s_in_EG (log, tab, sgs, s, F, Fsem, tmpvars, conf);
	else
	  result = s_show_s_in_E_not_U (log, tab, sgs, s, F, Fsem, tmpvars, 
					conf);
      }
      break;

    case CTLSTAR_U:
      {
	ar_sgs_set *tmp0;
	ar_sgs_set *tmp1;
	ctlstar_formula **operands = ctlstar_formula_get_operands (arg);
	ctlstar_formula *f = operands[0];
	ctlstar_formula *g = operands[1];
	ar_sgs_set *fsem = ar_interp_eval_ctlstar_formula (f, sgs, conf);
	ar_sgs_set *gsem = ar_interp_eval_ctlstar_formula (g, sgs, conf);
	int first = 1;
	s = ar_sgs_set_add_reference (s);
	result = ar_sgs_set_add_reference (s);
	if (Fsem == NULL)
	  Fsem = ar_interp_eval_ctlstar_formula (F, sgs, conf);
	else
	  Fsem = ar_sgs_set_add_reference (Fsem);
	tmp0 = ar_sgs_compute_union (sgs, Fsem, gsem);
	ar_sgs_set_del_reference (Fsem);
	Fsem = tmp0;

	for (;;)
	  {
	    ar_sgs_set *tmp0 = ar_sgs_compute_intersection (sgs, gsem, s);
	    int g_is_reached = ! ar_sgs_set_is_empty (sgs, tmp0);
	    ar_sgs_set_del_reference (tmp0);

	    if (g_is_reached)
	      break;
	    first = 0;
	    tmp0 = ar_sgs_compute_difference (sgs, Fsem, s);
	    ccl_assert (! ar_sgs_set_is_empty (sgs, tmp0));
	    ar_sgs_set_del_reference (Fsem);
	    Fsem = tmp0;
	    if (ctlstar_formula_get_kind (f) != CTLSTAR_TRUE)
	      {
		tmp0 =
		  s_show_s_in_F (log, tab + TAB_INC, sgs, s, f, fsem, tmpvars, 
				 conf);
		tmp1 = ar_sgs_compute_union (sgs, result, tmp0);
		ar_sgs_set_del_reference (result);
		ar_sgs_set_del_reference (tmp0);
		result = tmp1;

		ccl_log (log, "%*sand\n", tab + TAB_INC, "");
	      }
	    tmp0 = s_show_transition_into (log, tab + TAB_INC, sgs, s, Fsem);
	    ar_sgs_set_del_reference (s);
	    s = tmp0;
	    tmp0 = ar_sgs_compute_union (sgs, result, s);
	    ar_sgs_set_del_reference (result);
	    result = tmp0;
	  }
	if (! first)
	  ccl_log (log, "%*sand\n", tab + TAB_INC, "");
	tmp0 = s_show_s_in_F (log, tab + TAB_INC, sgs, s, g, gsem, tmpvars, 
			      conf);
	tmp1 = ar_sgs_compute_union (sgs, result, tmp0);
	ar_sgs_set_del_reference (result);
	ar_sgs_set_del_reference (tmp0);
	result = tmp1;

	ar_sgs_set_del_reference (gsem);
	ar_sgs_set_del_reference (fsem);
	if (Fsem != NULL)
	  ar_sgs_set_del_reference (Fsem);
	ar_sgs_set_del_reference (s);
      }
      break;

    case CTLSTAR_X:
      {
	ar_sgs_set *tmp0;
	ctlstar_formula *X = *ctlstar_formula_get_operands (arg);
	ar_sgs_set *Xsem = ar_interp_eval_ctlstar_formula (X, sgs, conf);
	ar_sgs_set *succ = 
	  s_show_transition_into (log, tab + TAB_INC, sgs, s, Xsem);
	
	ccl_log (log, "%*sand \n", tab + TAB_INC, ""); 
	tmp0 = s_show_s_in_F (log, tab + TAB_INC, sgs, succ, X, Xsem, 
			      tmpvars, conf);
	result = ar_sgs_compute_union (sgs, succ, tmp0);
	ar_sgs_set_del_reference (tmp0);
	ar_sgs_set_del_reference (succ);
	ar_sgs_set_del_reference (Xsem);
      }
      break;

    default:
      ccl_unreachable ();
      break;
    }

  ccl_post (result != NULL);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_show_s_in_F (ccl_log_type log, int tab, ar_sgs *sgs, ar_sgs_set *s, 
	       ctlstar_formula *F, ar_sgs_set *Fsem, ar_idtable *tmpvars, 
	       ccl_config_table *conf)
{
  ctlstar_formula_kind kind = ctlstar_formula_get_kind (F);
  ar_sgs_set *result = NULL;

  switch (kind)
    {
    case CTLSTAR_TRUE: 
    case CTLSTAR_VAR:
    case CTLSTAR_NOT:
      result = s_show_s_in_atom (log, tab, sgs, s, F, tmpvars);
      break;

    case CTLSTAR_AND:
      result = s_show_s_in_AND (log, tab, sgs, s, F, tmpvars, conf);
      break;

    case CTLSTAR_OR:
      result = s_show_s_in_OR (log, tab, sgs, s, F, tmpvars, conf);
      break;

    case CTLSTAR_NOT_U:
    case CTLSTAR_U:
    case CTLSTAR_X:
      ccl_error ("formula '");
      ctlstar_formula_log (log, F, s_log_identifiers, tmpvars);
      ccl_error ("' is a not pure CTL formula.\n");      
      break;

    default:
      ccl_assert (kind == CTLSTAR_E);
      if (ctlstar_formula_get_arity (F) > 1)
	{
	  ccl_error ("formula '");
	  ctlstar_formula_log (log, F, s_log_identifiers, tmpvars);
	  ccl_error ("' is a not pure CTL formula.\n");      
	  break;
	}
      result = s_show_s_in_E (log, tab, sgs, s, F, Fsem, tmpvars, conf);      
      break;
    }

  return result;
}
