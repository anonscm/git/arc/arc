/*
 * ar-idpath.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
#include "ar-idpath.h"

typedef struct id_element_st *id_element;
struct id_element_st {
  id_element next;
  ccl_ustring id;
};

struct ar_idpath_st {
  int refcount;
  id_element path;
  id_element *last_el;
};

			/* --------------- */

ar_idpath
ar_idpath_create(ccl_ustring id)
{
  ar_idpath result = ccl_new(struct ar_idpath_st);

  result->refcount = 1;
  result->path = NULL;
  result->last_el = &result->path;
  ar_idpath_append_identifier(result,id);

  return result;
}

			/* --------------- */

void
ar_idpath_append_identifier(ar_idpath idp, ccl_ustring id)
{
  ccl_pre( idp != NULL ); ccl_pre( id != NULL );

  *idp->last_el = ccl_new(struct id_element_st);
  (*idp->last_el)->next = NULL;
  (*idp->last_el)->id = id;
}

			/* --------------- */

void
ar_idpath_append_position(ar_idpath idp, uint32_t pos)
{
  size_t s = 20;
  char *tmp = NULL;

  ccl_string_format(&tmp,&s,"[%d]",pos);
  ar_idpath_append_identifier(idp,tmp);

  ccl_string_delete(tmp);
}

			/* --------------- */

ar_idpath
ar_idpath_duplicate(ar_idpath idp)
{
  id_element iel;
  ar_idpath result = ccl_new(struct ar_idpath_st);

  result->refcount = 1;
  for(iel = idp->path; iel; iel = iel->next)
    ar_idpath_append_identifier(result,iel->id);

  return result;
}

			/* --------------- */

ar_idpath
ar_idpath_add_reference(ar_idpath idp)
{
  ccl_pre( idp != NULL );

  idp->refcount++;

  return idp;
}

			/* --------------- */

void
ar_idpath_del_reference(ar_idpath idp)
{
  ccl_pre( idp != NULL ); ccl_pre( idp->refcount > 0 );

  if( --idp->refcount == 0 )
    {
      id_element iel, next;
      
      for(iel = idp->path; iel; iel = next)
	{
	  next = iel->next;
	  ccl_delete(iel);
	}
      ccl_delete(idp);
    }
}

			/* --------------- */

void
ar_idpath_log(ccl_log_type log, ar_idpath idp)
{
  id_element iel;

  ccl_pre( idp != NULL );

  for(iel = idp->path; iel; iel = iel->next)
    {
      ccl_log(log,"%s",iel->id);
      if( iel->next != NULL && iel->next->id[0] != '[' )
	      ccl_log(log,".");
    }
}
