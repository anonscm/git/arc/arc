/*
 * obfuscation-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef OBFUSCATION_P_H
# define OBFUSCATION_P_H

# include <ccl/ccl-memory.h>
# include <ccl/ccl-hash.h>
# include "parser/altarica-tree.h"
# include "obfuscation.h"

struct obfuscation_st 
{
  ar_identifier_translator super;  
  ccl_hash *H;
  
  char *(*translate) (obfuscation *o, ar_identifier_context ctx, 
		      const char *id);
  void (*destroy) (obfuscation *o);
};


extern obfuscation *
obfuscation_create (size_t size, 
		    char * (*translate) (obfuscation *o, 
					 ar_identifier_context ctx, 
					 const char *id),
		    void (*destroy) (obfuscation *o));

#endif /* ! OBFUSCATION_P_H */
