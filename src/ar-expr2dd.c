/*
 * ar-expr2dd.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-time.h>
#include "ar-expr-p.h"
#include "mec5/mec5.h"
#include "ar-expr2dd.h"
#include "ar-semantics.h"
#include "ar-rel-semantics.h"

struct expr2dd_info
{
  void *data;
  ar_dd val;
  ccl_delete_proc *del;
};

struct substitution
{
  struct substitution *next;
  int index;
  int arity;
  ar_dd all;
  ar_dd S[1];  
};

			/* --------------- */

static ar_dd 
s_dd_for_boolean_op (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		     ccl_hash *cache, ar_expr *e, int *p_changed);

static ar_dd 
s_dd_for_boolean_var (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		      ccl_hash *cache, ar_expr *e, int *p_changed);

static ar_dd 
s_dd_for_quantifier (ar_context *ctx, ar_ddm ddm, 
		     int *slots_to_dd_indexes, ccl_hash *cache, 
		     ar_expr *e, int *p_changed);

static ar_dd 
s_dd_for_constraint (ar_context *ctx, ar_ddm ddm, 
		     int *slots_to_dd_indexes, ccl_hash *cache, 
		     ar_expr *e, int *p_changed);

static ccl_list *
s_collect_slots(ar_context *ctx, ar_expr *e);

static ar_dd 
s_dd_for_predicate (ar_context *ctx, ar_ddm ddm, 
		    int *slots_to_dd_indexes, 
		    ccl_hash *cache, ar_expr *e, int *p_changed);

static void
s_delete_info_cache (void *p);

			/* --------------- */

ccl_hash *
ar_expr2dd_create_info_cache (void)
{
  return ccl_hash_create (NULL, NULL, NULL, s_delete_info_cache);
}

			/* --------------- */

static ar_dd 
s_expr_to_dd (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
	      ccl_hash *cache, ar_expr *e, int *p_changed)
{
  ar_dd result = NULL, one, zero;

  ccl_pre (ddm != NULL);
  ccl_pre (slots_to_dd_indexes != NULL);
  ccl_pre (e != NULL);
  ccl_pre (ar_expr_get_type_kind (e) == AR_TYPE_BOOLEANS);
  
  one = ar_dd_one (ddm);
  zero = ar_dd_zero (ddm);

  switch (e->op)
    {
    case AR_EXPR_CST:
      result = (ar_constant_get_boolean_value(e->u.cst) ? one : zero);
      result = AR_DD_DUP(result);
      *p_changed = 1;
      break;

    case AR_EXPR_STRUCT_MEMBER:
    case AR_EXPR_ARRAY_MEMBER:
    case AR_EXPR_VAR:
      result = s_dd_for_boolean_var (ctx, ddm, slots_to_dd_indexes, cache, e,
				     p_changed);
      break;

    case AR_EXPR_NOT: case AR_EXPR_ITE: 
    case AR_EXPR_OR: case AR_EXPR_AND:
      result = 
	s_dd_for_boolean_op (ctx, ddm, slots_to_dd_indexes, cache, e, 
			     p_changed);
      break;

    case AR_EXPR_EQ : case AR_EXPR_NEQ:
      if (ar_expr_get_type_kind (e->args[0]) == AR_TYPE_BOOLEANS)
	{
	  result = 
	    s_dd_for_boolean_op (ctx, ddm, slots_to_dd_indexes, cache, e, 
				 p_changed);
	  break;
	}
    case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ: case AR_EXPR_GEQ:
      result = s_dd_for_constraint (ctx, ddm, slots_to_dd_indexes, cache, e,
				    p_changed);
      break;

    case AR_EXPR_EXIST: case AR_EXPR_FORALL:
      result = s_dd_for_quantifier (ctx, ddm, slots_to_dd_indexes, cache, e,
				    p_changed);
      break;

    case AR_EXPR_CALL:
      result = s_dd_for_predicate (ctx, ddm, slots_to_dd_indexes, cache, e,
				   p_changed);
      break;

      
    case AR_EXPR_PARAM: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL: 
    case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NEG: case AR_EXPR_ARRAY: 
    case AR_EXPR_STRUCT: 

    default:

      ccl_unreachable ();
      break;
    };

  return result;
}

			/* --------------- */
ar_dd 
ar_boolean_expr_to_dd (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		       ccl_hash *cache, ar_expr *e)
{
  int changed = 0;

  return s_expr_to_dd (ctx, ddm, slots_to_dd_indexes, cache, e, &changed);
}

			/* --------------- */

static ar_dd 
s_dd_for_boolean_op (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		     ccl_hash *cache, ar_expr *e, int *p_changed)
{
  int i;
  ar_dd result;
  struct expr2dd_info *info;
  ar_expr_op op = e->op;
  int nb_args = e->arity;
  ar_expr **args = e->args;
  ar_dd (*comp)(ar_ddm, ar_dd, ar_dd);
  ar_dd *ops = ccl_new_array (ar_dd, nb_args);

  *p_changed = 0;
  for (i = 0; i < nb_args; i++)
    {
      int changed = 0;
      ops[i] = s_expr_to_dd (ctx, ddm, slots_to_dd_indexes, cache, args[i], 
			     &changed);
      *p_changed = *p_changed || changed;
    }
 
  if (!*p_changed)
    {
      ccl_assert (ccl_hash_find (cache, e));

      ccl_hash_find (cache, e);
      info = ccl_hash_get (cache);
      result = AR_DD_DUP (info->val);
    }
  else
    {
      if (ccl_hash_find (cache, e))
	info = ccl_hash_get (cache);
      else
	{
	  info = ccl_new (struct expr2dd_info);
	  info->val = NULL;
	  ccl_hash_insert (cache, info);
	}

      switch (op)
	{
	case AR_EXPR_NOT:
	  result = AR_DD_DUP_NOT (ops[0]);
	  break;

	case AR_EXPR_ITE:      
	  result = ar_dd_compute_ite (ddm, ops[0], ops[1], ops[2]);
	  break;

	case AR_EXPR_EQ: case AR_EXPR_NEQ:
	  result = ar_dd_compute_iff (ddm, ops[0], ops[1]);
	  if (op == AR_EXPR_NEQ)
	    result = AR_DD_NOT (result);
	  break;

	default:
	  ccl_assert (op == AR_EXPR_OR || op == AR_EXPR_AND);
	  comp = (op == AR_EXPR_OR) ? ar_dd_compute_or : ar_dd_compute_and;

	  result = AR_DD_DUP (ops[0]);
	  for (i = 1; i < nb_args; i++)
	    {
	      ar_dd tmp = comp (ddm, ops[i], result);
	      AR_DD_FREE (result);
	      result = tmp;
	    }
	  break;
	}

      ccl_zdelete (AR_DD_FREE, info->val);
      info->val = AR_DD_DUP (result);
    }

  for (i = 0; i < nb_args; i++)
    AR_DD_FREE (ops[i]);
  ccl_delete (ops);

  return result;      
}

			/* --------------- */

static ar_dd 
s_dd_for_boolean_var_index (ar_ddm ddm, int index)
{
  ar_dd result;
  int arity = 2;
  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, EXHAUSTIVE);

  ccl_assert (index >= 0);

  AR_DD_ENTHDD (sons, 0) = ar_dd_zero (ddm);
  AR_DD_ENTHDD (sons, 0) = AR_DD_DUP (AR_DD_ENTHDD (sons, 0));
  AR_DD_ENTHDD (sons, 1) = AR_DD_DUP (ar_dd_one (ddm));
  AR_DD_ENTHDD (sons, 1) = AR_DD_DUP (AR_DD_ENTHDD (sons, 1));
  
  result = ar_dd_find_or_add (ddm,index, arity, EXHAUSTIVE, sons);
  ar_dd_free_dd_table (ddm, arity, EXHAUSTIVE, sons);

  return result;
}

			/* --------------- */
static ar_dd 
s_dd_for_boolean_var (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		      ccl_hash *cache, ar_expr *e, int *p_changed)
{
  struct expr2dd_info *info;

  *p_changed = ! ccl_hash_find (cache, e);
  if (! *p_changed)
    info = ccl_hash_get (cache);
  else
    {
      ar_context_slot *sl = ar_expr_eval_slot (e, ctx);
      int index = ar_context_slot_get_index (sl);
      int dd_index = slots_to_dd_indexes[index];
      ar_dd result = s_dd_for_boolean_var_index (ddm, dd_index);
      
      ar_context_slot_del_reference (sl);

      info = ccl_new (struct expr2dd_info);
      info->val = result;
      info->del = NULL;
      ccl_hash_insert (cache, info);
    }

  return AR_DD_DUP (info->val);
}

			/* --------------- */

static ar_dd
s_project_variables (ar_ddm ddm, ccl_pair *P, ar_dd F)
{
  ar_dd ONE = ar_dd_one (ddm);
  ar_dd ZERO = ar_dd_zero (ddm);
  ar_dd R = NULL;
  ar_dd U;
  ar_dd V;
  ar_ddtable Rsons;
  int i;
  int arity;

  if (F == ZERO) R = AR_DD_DUP (ZERO);
  else if (F == ONE) R = AR_DD_DUP (ONE);
  else if (P == NULL) R = AR_DD_DUP (F);
  else
    {
      int index = (intptr_t) CAR (P);

      R = (ar_dd) ar_dd_entry_for_operation (ddm, s_project_variables, F, P);
      if (R != NULL)
	R = AR_DD_DUP (R);
      else 
	{ 
	  if (index == AR_DD_INDEX (F))
	    {
	      R = AR_DD_DUP (ZERO);
	      if (AR_DD_CODE (F) == COMPACTED)
		{
		  for(i = 0; i < AR_DD_ARITY (F) && R != ONE; i++) 
		    {
		      U = s_project_variables (ddm, CDR (P), 
					       AR_DD_CPNTHSON (F, i));
		      V = ar_dd_compute_or (ddm, U, R);
		      AR_DD_FREE (U); 
		      AR_DD_FREE (R);
		      R = V;
		    }
		}
	      else
		{
		  for(i = 0; i < AR_DD_ARITY (F) && R != ONE; i++) 
		    {
		      U = s_project_variables (ddm, CDR (P), 
					       AR_DD_EPNTHSON (F, i));
		      V = ar_dd_compute_or (ddm, U, R);
		      AR_DD_FREE (U); 
		      AR_DD_FREE (R);
		      R = V;
		    }
		}
	    }
	  else if (index > AR_DD_INDEX (F))
	    {

	      /* -- The top index variable is not in the quantifier scope. */
	      arity = AR_DD_ARITY (F);
	      Rsons = ar_dd_allocate_dd_table (ddm, arity, AR_DD_CODE (F));
	  
	      if (AR_DD_CODE(F) == COMPACTED)
		{
		  for(i = 0; i < arity; i++)
		    {
		      AR_DD_MIN_OFFSET_NTHDD (Rsons, i) = 
			AR_DD_MIN_OFFSET_NTHSON (F, i);
		      AR_DD_MAX_OFFSET_NTHDD (Rsons, i) = 
			AR_DD_MAX_OFFSET_NTHSON (F, i);
		      AR_DD_CNTHDD (Rsons, i)=
			s_project_variables (ddm, P, AR_DD_CPNTHSON (F, i));
		    }
		}
	      else
		{
		  for (i = 0; i < arity; i++)
		    AR_DD_ENTHDD (Rsons, i) = 
		      s_project_variables (ddm, P, AR_DD_EPNTHSON (F, i));
		}
	      R = ar_dd_find_or_add (ddm, AR_DD_INDEX (F),arity, AR_DD_CODE (F),
				     Rsons);
	      ar_dd_free_dd_table (ddm, arity, AR_DD_CODE (F), Rsons);
	    }
	  else
	    {
	      R = s_project_variables (ddm, CDR (P), F);
	    }
	  ar_dd_memoize_operation (ddm, s_project_variables, F, P, R); 
	}
    }
  return R;
}

			/* --------------- */

static ccl_list *
s_get_dd_indexes_for_quantified_slots (ccl_hash *cache, ar_expr *e,
				       int *slots_to_dd_indexes)
{
  ccl_pair *p;
  ccl_list *slots = ccl_list_create ();
  
  
  for(p = FIRST (e->u.qvars); p; p = CDR (p))
    {
      ar_expr *var = (ar_expr *) CAR (p);
      ar_context_expand_composite_slot (e->qctx, var->u.slot, slots);
    }
  
  for (p = FIRST (slots); p; p = CDR (p))
    {
      ar_context_slot *sl = CAR (p);
      int index = ar_context_slot_get_index (sl);
      ccl_assert (index >= 0);
      CAR (p) = (void *) (intptr_t) slots_to_dd_indexes[index];
      ar_context_slot_del_reference (sl);
    }
  
  ccl_list_sort (slots, NULL);

  return slots;
}

			/* --------------- */

static ar_dd 
s_dd_for_quantifier (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		     ccl_hash *cache, ar_expr *e, int *p_changed)
{
  struct expr2dd_info *info;
  ar_dd result = s_expr_to_dd (e->qctx, ddm, slots_to_dd_indexes, 
			       cache, e->args[0], p_changed);

  if (ccl_hash_find (cache, e))
    info = ccl_hash_get (cache);
  else
    {
      info = ccl_new (struct expr2dd_info);
      info->data = 
	s_get_dd_indexes_for_quantified_slots (cache, e, slots_to_dd_indexes);
      info->del = (ccl_delete_proc *) ccl_list_delete;
      info->val = NULL;
      ccl_hash_insert (cache, info);
    }

  if (*p_changed)
    {
      ar_dd aux;
      ccl_pair *slots = FIRST (((ccl_list *)info->data));

      if (e->op == AR_EXPR_FORALL)
	result = AR_DD_NOT (result);

      aux = s_project_variables (ddm, slots, result);
      AR_DD_FREE (result);
      result = aux;
      
      if (e->op == AR_EXPR_FORALL)
	result = AR_DD_NOT (result);
      ccl_zdelete (AR_DD_FREE, info->val);
      info->val = AR_DD_DUP (result);
    }
  else
    {
      AR_DD_FREE (result);
      result = AR_DD_DUP (info->val);
    }

  return result;
}

			/* --------------- */

static int
s_cmp_slots(const void *s1, const void *s2, void *data)
{
  int i1 = ar_context_slot_get_index (s1);
  int i2 = ar_context_slot_get_index (s2);
  const int *slots_to_dd_indexes = data;

  return slots_to_dd_indexes[i1] - slots_to_dd_indexes[i2];
}

			/* --------------- */

static ar_expr *
s_type_get_value (ar_type *type, int i)
{
  ar_constant *cst = ar_type_get_ith_element (type, i);
  ar_expr *result = ar_expr_crt_constant (cst);
  ar_constant_del_reference (cst);
  
  return result;
}

			/* --------------- */

static ar_dd
s_enumerate_assignments_rec (ar_context *ctx, ar_ddm ddm, ar_expr *F, 
			     int *dd_indexes, ccl_pair *p)
{
  ar_dd R = NULL;
  ar_ddtable sons;
  int i, arity, index, ddindex;
  ar_context_slot *var;
  ar_ddcode encoding;
  ar_type *domain;

  if (p == NULL) 
    {
      ar_constant *cst = ar_expr_eval (F, ctx);
      if (ar_constant_get_boolean_value (cst)) 
	R = ar_dd_one (ddm);
      else 
	R = ar_dd_zero (ddm);
      R = AR_DD_DUP (R);
      ar_constant_del_reference (cst);
    }
  else
    {
      var = (ar_context_slot *) CAR (p);
      domain = ar_context_slot_get_type (var);
      arity = ar_type_get_cardinality (domain);
      index = ar_context_slot_get_index (var);
      ddindex = dd_indexes[index];
      encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
      sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

      if (encoding == COMPACTED) 
	{
	  for (i = 0; i < arity; i++)
	    {
	      ar_expr *V = s_type_get_value (domain, i);
	      ar_context_slot_set_value (var, V);
	      ar_expr_del_reference (V);

	      AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
	      AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
	      AR_DD_CNTHDD (sons,i) = 
		s_enumerate_assignments_rec (ctx, ddm, F, dd_indexes, CDR (p));
	    }
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    {
	      ar_expr *V = s_type_get_value (domain, i);
	      ar_context_slot_set_value (var, V);
	      ar_expr_del_reference (V);
	      AR_DD_ENTHDD (sons,i) = 
		s_enumerate_assignments_rec (ctx, ddm, F, dd_indexes, CDR (p));
	    }
	}
      R = ar_dd_find_or_add (ddm, ddindex, arity, encoding, sons);
      ar_dd_free_dd_table (ddm, arity, encoding, sons);

      ar_type_del_reference (domain);
    }
  ccl_post (R != NULL);

  return R;
}

			/* --------------- */

static ar_dd 
s_dd_for_constraint (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		     ccl_hash *cache, ar_expr *e, int *p_changed)
{
  struct expr2dd_info *info;

  if (ccl_hash_find (cache, e))
    {
      info = ccl_hash_get (cache);
      *p_changed = 0;
    }
  else
    {
      ccl_list *vars = s_collect_slots (ctx, e);

      info = ccl_new (struct expr2dd_info);
      info->val = NULL;
      ccl_hash_insert (cache, info);

      ccl_list_sort_with_data (vars, s_cmp_slots, slots_to_dd_indexes);
      info->val = s_enumerate_assignments_rec (ctx, ddm, e, 
					       slots_to_dd_indexes, 
					       FIRST(vars));
      while (! ccl_list_is_empty (vars))
	{
	  ar_context_slot *sl = ccl_list_take_first (vars);
	  ar_context_slot_set_value (sl, NULL);
	  ar_context_slot_del_reference (sl);
	}
      ccl_list_delete(vars);
      *p_changed = 1;
    }

  return AR_DD_DUP (info->val);
}

			/* --------------- */

static void
s_collect_slots_rec (ar_context *ctx, ar_expr *e, ccl_list *slots)
{
  switch (e->op) 
    {
    case AR_EXPR_STRUCT_MEMBER : case AR_EXPR_ARRAY_MEMBER : 
    case AR_EXPR_VAR : case AR_EXPR_PARAM :
      {
	ccl_pair *p;
	ccl_list *l = ar_expr_expand_composite_types (e, ctx, NULL);
	for (p = FIRST (l); p; p = CDR (p))
	  {
	    ar_context_slot *sl = ar_expr_eval_slot(CAR (p), ctx);
	    if (! ccl_list_has (slots, sl))
	      ccl_list_add (slots, sl);
	    else
	      ar_context_slot_del_reference (sl);
	    ar_expr_del_reference (CAR (p));
	  }
	ccl_list_delete (l);
      }
      return;

    case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ: 
    case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ: 
    case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL: 
    case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
    case AR_EXPR_EXIST: case AR_EXPR_FORALL:  case AR_EXPR_CALL: 
    case AR_EXPR_MIN : case AR_EXPR_MAX :
      {
	int i;
	for (i = 0; i < e->arity; i++)
	  s_collect_slots_rec (ctx, e->args[i], slots);
      }
      return;
      
    case AR_EXPR_CST: case AR_EXPR_ARRAY: case AR_EXPR_STRUCT:
      return;
    }

  ccl_unreachable ();
}

			/* --------------- */

static ccl_list *
s_collect_slots(ar_context *ctx, ar_expr *e)
{
  ccl_list *result = ccl_list_create ();

  s_collect_slots_rec (ctx, e, result);

  return result;
}

			/* --------------- */
#if 0
static ar_dd
s_dd_for_var_eq_val (ar_ddm ddm, int var, int arity, int min, int max)
{
  int i;
  ar_ddcode encoding = (arity > 2) ? COMPACTED : EXHAUSTIVE;
  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);
  ar_dd one = ar_dd_one (ddm);
  ar_dd zero = ar_dd_zero (ddm);
  ar_dd R;

  if (encoding == COMPACTED)
    {
      for (i = 0; i < arity; i++)
	{
	  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
	  if (min <= i && i <= max)
	    AR_DD_CNTHDD (sons,i) = AR_DD_DUP (one);
	  else
	    AR_DD_CNTHDD (sons,i) = AR_DD_DUP (zero);
	}
    }
  else    
    {
      for (i = 0; i < arity; i++)
	{
	  if (min <= i && i <= max)
	    AR_DD_ENTHDD (sons,i) = AR_DD_DUP (one);
	  else
	    AR_DD_ENTHDD (sons,i) = AR_DD_DUP (zero);
	}
    }
  R = ar_dd_find_or_add (ddm, var, arity, encoding, sons);
  ar_dd_free_dd_table (ddm, arity, encoding, sons);

  return R;
}
#endif
			/* --------------- */

#if 0
static ar_dd
s_dd_for_var1_eq_var2 (ar_ddm ddm, int var1index, int var2index,  
		       ar_type *domain)
{
  int i1;
  ar_ddtable sons1;
  int arity = ar_type_get_cardinality (domain);
  ar_ddcode encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
  ar_dd R = NULL;

  if (var1index == var2index)
    {
      R = ar_dd_one (ddm);
      return AR_DD_DUP (R);
    }

  sons1 = ar_dd_allocate_dd_table (ddm, arity, encoding);
  if (var1index > var2index)
    {
      int tmp = var2index;
      var2index = var1index;
      var1index = tmp;
    }

  if (encoding == COMPACTED) 
    {
      for (i1 = 0; i1 < arity; i1++)
	{
	  AR_DD_MIN_OFFSET_NTHDD (sons1, i1) = i1;
	  AR_DD_MAX_OFFSET_NTHDD (sons1, i1) = i1;	      
	  AR_DD_CNTHDD (sons1, i1) = 
	    s_dd_for_var_eq_val (ddm, var2index, arity, i1, i1);
	}
    }
  else
    {
      for (i1 = 0; i1 < arity; i1++)
	AR_DD_ENTHDD (sons1, i1) = 
	  s_dd_for_var_eq_val (ddm, var2index, arity, i1, i1);
    }

  R = ar_dd_find_or_add (ddm, var1index, arity, encoding, sons1);
  ar_dd_free_dd_table (ddm, arity, encoding, sons1);

  return R;
}
#endif
			/* --------------- */
#if 0
static int
s_type_get_cst_index (ar_type *type, ar_constant *cst)
{
  int result;
  ar_type_kind kind = ar_type_get_kind (type);
  switch (kind)
    {
    case AR_TYPE_BOOLEANS:
      result = (ar_constant_get_boolean_value (cst)) ? 1 : 0;
      break;

    case AR_TYPE_RANGE:
      result = (ar_constant_get_integer_value (cst) - 
		ar_type_range_get_min (type));
      break;

    case AR_TYPE_SYMBOL_SET: 
      {
	int val = ar_constant_get_symbol_value (cst);
	
	result = ar_type_symbol_set_get_value_index (type, val);
      }
      break;

    case AR_TYPE_ENUMERATION:
      result = ar_constant_get_symbol_value (cst);
      break;

    default:
      ccl_unreachable ();
    }

  return result;
}
#endif
			/* --------------- */
#if 0
static ar_dd
s_dd_for_var_eq_term_rec (ar_context *ctx, ar_ddm ddm, 
			  int *slots_to_dd_indexes, int varindex, 
			  ar_type *vartype, int valindex, ccl_pair *p, 
			  ar_expr *e, ar_expr *in_dom)
{
  ar_dd R;
  ar_dd one = ar_dd_one (ddm);
  ar_dd zero = ar_dd_zero (ddm);

  if (p == NULL)
    {
      ar_constant *is_valid_assignment = ar_expr_eval (in_dom, ctx);

      if (ar_constant_get_boolean_value (is_valid_assignment)) 
	{
	  ar_constant *value = ar_expr_eval (e, ctx);
	  int ival = s_type_get_cst_index (vartype, value);

	  if (valindex < 0)
	    {
	      int i;
	      int arity = ar_type_get_cardinality (vartype);
	      ar_ddcode encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
	      ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

	      if (encoding == COMPACTED) 
		{		  
		  for (i = 0; i < arity; i++)
		    {
		      AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
		      AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
		      if (i == ival) 
			AR_DD_CNTHDD (sons,i) = AR_DD_DUP (one);
		      else 
			AR_DD_CNTHDD (sons,i) = AR_DD_DUP (zero);
		    }
		}
	      else
		{
		  for (i = 0; i < arity; i++)
		    {
		      if (i == ival) 
			AR_DD_ENTHDD (sons,i) = AR_DD_DUP (one);
		      else 
			AR_DD_ENTHDD (sons,i) = AR_DD_DUP (zero);
		    }
		}
	      R = ar_dd_find_or_add (ddm, varindex, arity, encoding, sons);
	      ar_dd_free_dd_table (ddm, arity, encoding, sons);
	    }
	  else
	    {
	      R = (valindex == ival) ? one : zero;
	      R = AR_DD_DUP (R);
	    }
	  ar_constant_del_reference (value);
	}
      else
	{
	  R = AR_DD_DUP (zero);
	}
      ar_constant_del_reference (is_valid_assignment);
    }
  else
    {
      int i;
      int arity;
      ar_ddcode encoding;
      ar_ddtable sons;
      ar_context_slot *var = (ar_context_slot *) CAR (p);
      int index = ar_context_slot_get_index (var);
      int ddindex = slots_to_dd_indexes[index];

      if (varindex < ddindex && valindex < 0)
	{
	  ddindex = varindex;
	  arity = ar_type_get_cardinality (vartype);
	  encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
	  sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

	  if (encoding == COMPACTED) 
	    {
	      for (i = 0; i < arity; i++)
		{
		  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
		  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
		  AR_DD_CNTHDD (sons,i) = 
		    s_dd_for_var_eq_term_rec (ctx, ddm, slots_to_dd_indexes, 
					      varindex, vartype, i,
					      p, e, in_dom);
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		{
		  AR_DD_ENTHDD (sons,i) = 
		    s_dd_for_var_eq_term_rec (ctx, ddm, slots_to_dd_indexes, 
					      varindex, vartype, i, 
					      p, e, in_dom);
		}
	    }
	}
      else
	{
	  ar_type *domain = ar_context_slot_get_type (var);
	  arity = ar_type_get_cardinality (domain);
	  encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
	  sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

	  ccl_assert (varindex != ddindex);

	  if (encoding == COMPACTED) 
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_expr *V = s_type_get_value (domain, i);
		  ar_context_slot_set_value (var, V);
		  ar_expr_del_reference (V);
		  
		  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
		  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
		  AR_DD_CNTHDD (sons,i) = 
		    s_dd_for_var_eq_term_rec (ctx, ddm, slots_to_dd_indexes, 
					      varindex, vartype, valindex,
					      CDR (p), e, in_dom);
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_expr *V = s_type_get_value (domain, i);
		  ar_context_slot_set_value (var, V);
		  ar_expr_del_reference (V);
		  AR_DD_ENTHDD (sons,i) = 
		    s_dd_for_var_eq_term_rec (ctx, ddm, slots_to_dd_indexes, 
					      varindex, vartype, valindex, 
					      CDR (p), e, in_dom);
		}
	    }
	  ar_type_del_reference (domain);
	}

      R = ar_dd_find_or_add (ddm, ddindex, arity, encoding, sons);
      ar_dd_free_dd_table (ddm, arity, encoding, sons);
    }

  return R;
}
#endif
			/* --------------- */

#if 0
static ar_dd
s_dd_for_var_eq_term (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		      int varindex, ar_type *vartype, ar_expr *e,
		      ccl_hash *cache)
{
  ar_dd R = NULL;

  if (vartype == AR_BOOLEANS)
    {
      ar_dd tmp1 = s_dd_for_boolean_var_index (ddm, varindex);
      ar_dd tmp2 = 
	ar_boolean_expr_to_dd (ctx, ddm, slots_to_dd_indexes, cache, e);
      R = ar_dd_compute_iff (ddm, tmp1, tmp2);
      AR_DD_FREE (tmp1);
      AR_DD_FREE (tmp2);
    }
  else
    {  
      ccl_list *vars = s_collect_slots (ctx, e);

      if (e->op == AR_EXPR_VAR && ccl_list_get_size (vars) == 1)
	{
	  ar_context_slot *sl = CAR (FIRST (vars));
	  ar_type *t = ar_context_slot_get_type (sl);
	  
	  if (ar_type_equals (t, vartype))
	    {
	      int index = ar_context_slot_get_index (sl);
	      R = s_dd_for_var1_eq_var2 (ddm, slots_to_dd_indexes[index], 
					 varindex, vartype);
	    }

	  ar_type_del_reference (t);
	}
      
      if (R == NULL)
	{
	  ar_expr *in_dom = ar_expr_crt_in_domain (e, vartype);
	  ccl_list_sort_with_data (vars, s_cmp_slots, slots_to_dd_indexes);
	  R = s_dd_for_var_eq_term_rec (ctx, ddm, slots_to_dd_indexes, 
					varindex, 
					vartype, -1, FIRST (vars), e, in_dom);
	  ar_expr_del_reference (in_dom);
	}
      
      while (! ccl_list_is_empty (vars))
	{
	  ar_context_slot *sl = ccl_list_take_first (vars);
	  ar_context_slot_set_value (sl, NULL);
	  ar_context_slot_del_reference (sl);
	}
      ccl_list_delete(vars);
    }

  return R;
}
#endif
			/* --------------- */

#if 0
static ar_dd
s_project_dd (ar_ddm ddm, int var, ar_dd F)
{
  ar_dd ONE = ar_dd_one (ddm);
  ar_dd ZERO = ar_dd_zero (ddm);
  ar_dd R = NULL;
  ar_dd U;
  ar_dd V;
  ar_ddtable Rsons;
  int i;
  int arity;
  
  if (F == ZERO) R = AR_DD_DUP (ZERO);
  else if (F == ONE) R = AR_DD_DUP (ONE);
  else 
    {
      R = (ar_dd) ar_dd_entry_for_operation (ddm, s_project_dd, F, 
					     (void *) (intptr_t) var);
      if (R != NULL)
	R = AR_DD_DUP (R);
      else
	{
	  int index = var;

	  if (index != AR_DD_INDEX (F)) 
	    {
	      /* -- The top index variable is not in the quantifier scope. */
	      arity = AR_DD_ARITY (F);
	      Rsons = ar_dd_allocate_dd_table (ddm, arity, AR_DD_CODE (F));

	      if (AR_DD_CODE(F) == COMPACTED)
		{
		  for(i = 0; i < arity; i++)
		    {
		      AR_DD_MIN_OFFSET_NTHDD (Rsons, i) = 
			AR_DD_MIN_OFFSET_NTHSON (F, i);
		      AR_DD_MAX_OFFSET_NTHDD (Rsons, i) = 
			AR_DD_MAX_OFFSET_NTHSON (F, i);
		      AR_DD_CNTHDD (Rsons, i)=
			s_project_dd (ddm, var, AR_DD_CPNTHSON (F, i));
		    }
		}
	      else
		{
		  for (i = 0; i < arity; i++)
		    AR_DD_ENTHDD (Rsons, i) = 
		      s_project_dd (ddm, var, AR_DD_EPNTHSON (F, i));
		}
	      R = ar_dd_find_or_add (ddm, AR_DD_INDEX (F),arity, AR_DD_CODE (F),
				     Rsons);
	      ar_dd_free_dd_table (ddm, arity, AR_DD_CODE (F), Rsons);
	    }
	  else 
	    {
	      R = AR_DD_DUP (ZERO);
	      if (AR_DD_CODE (F) == COMPACTED)
		{
		  for(i = 0; i < AR_DD_ARITY (F) && R != ONE; i++) 
		    {
		      U = AR_DD_DUP (AR_DD_CPNTHSON (F, i));
		      V = ar_dd_compute_or (ddm, U, R);
		      AR_DD_FREE (U); 
		      AR_DD_FREE (R);
		      R = V;
		    }
		}
	      else
		{
		  for(i = 0; i < AR_DD_ARITY (F) && R != ONE; i++) 
		    {
		      U = AR_DD_DUP (AR_DD_EPNTHSON (F, i));
		      V = ar_dd_compute_or (ddm, U, R);
		      AR_DD_FREE (U); 
		      AR_DD_FREE (R);
		      R = V;
		    }
		}
	    }
	  ar_dd_memoize_operation (ddm, s_project_dd, F, 
				   (void *) (intptr_t) var, R); 
	}
    }

  ccl_post (R != NULL);

  return R;
}
#endif
			/* --------------- */
#if 0
static ar_dd
s_project_from (ar_ddm ddm, int var, ar_dd F)
{
  ar_dd ONE = ar_dd_one (ddm);
  ar_dd ZERO = ar_dd_zero (ddm);
  ar_dd R = NULL;
  ar_ddtable Rsons;
  int i;
  int arity;
  
  if (F == ZERO) R = AR_DD_DUP (ZERO);
  else if (F == ONE) R = AR_DD_DUP (ONE);
  else 
    {
      R = (ar_dd) 
	ar_dd_entry_for_operation (ddm, s_project_from, F, 
				   (void *) (intptr_t) var);

      if (R != NULL)
	R = AR_DD_DUP (R);
      else
	{
	  int index = var;

	  if (index <= AR_DD_INDEX (F)) 
	    {
	      arity = AR_DD_ARITY (F);
	      R = ZERO;

	      if (AR_DD_CODE(F) == COMPACTED)
		{
		  for(i = 0; i < arity && R == ZERO; i++)
		    if (AR_DD_CPNTHSON (F, i) != ZERO)
		      R = ONE;
		}
	      else
		{
		  for (i = 0; i < arity && R == ZERO; i++)
		    if (AR_DD_ENTHDD (Rsons, i) != ZERO)
		      R = ONE;
		}
	      R = AR_DD_DUP (R);
	    }
	  else 
	    {
	      arity = AR_DD_ARITY (F);
	      Rsons = ar_dd_allocate_dd_table (ddm, arity, AR_DD_CODE (F));

	      if (AR_DD_CODE(F) == COMPACTED)
		{
		  for(i = 0; i < arity; i++)
		    {
		      AR_DD_MIN_OFFSET_NTHDD (Rsons, i) = 
			AR_DD_MIN_OFFSET_NTHSON (F, i);
		      AR_DD_MAX_OFFSET_NTHDD (Rsons, i) = 
			AR_DD_MAX_OFFSET_NTHSON (F, i);
		      AR_DD_CNTHDD (Rsons, i)=
			s_project_from (ddm, var, AR_DD_CPNTHSON (F, i));
		    }
		}
	      else
		{
		  for (i = 0; i < arity; i++)
		    AR_DD_ENTHDD (Rsons, i) = 
		      s_project_from (ddm, var, AR_DD_EPNTHSON (F, i));
		}
	      R = ar_dd_find_or_add (ddm, AR_DD_INDEX (F),arity, AR_DD_CODE (F),
				     Rsons);
	      ar_dd_free_dd_table (ddm, arity, AR_DD_CODE (F), Rsons);
	    }
	  ar_dd_memoize_operation (ddm, s_project_from, F, 
				   (void *) (intptr_t) var, R); 
	}
    }

  ccl_post (R != NULL);

  return R;
}

			/* --------------- */

static ar_dd
s_dd_relabel (ar_ddm ddm, ar_dd X, ccl_hash *hrelabelling, int *relabelling)
{
  ar_dd R;

  if (X == ar_dd_one (ddm) || X == ar_dd_zero (ddm))
    return AR_DD_DUP (X);
  
  R = (ar_dd) ar_dd_entry_for_operation (ddm, s_dd_relabel, X, relabelling);
  if (R != NULL)
    R = AR_DD_DUP (R);
  else
    {
      int i, newindex;
      int arity = AR_DD_ARITY (X);
      ar_ddcode encoding = AR_DD_CODE (X);
      ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

      ccl_assert (ccl_hash_find (hrelabelling, 
				 (void *) (intptr_t) AR_DD_INDEX (X)));
      ccl_hash_find (hrelabelling, (void *) (intptr_t) AR_DD_INDEX (X));
      newindex = (intptr_t) ccl_hash_get (hrelabelling);

      if (encoding == COMPACTED)
	{
	  for(i = 0; i < arity; i++)
	    {
	      AR_DD_MIN_OFFSET_NTHDD (sons, i) = 
		AR_DD_MIN_OFFSET_NTHSON (X, i);
	      AR_DD_MAX_OFFSET_NTHDD (sons, i) = 
		AR_DD_MAX_OFFSET_NTHSON (X, i);
	      AR_DD_CNTHDD (sons, i)=
		s_dd_relabel (ddm, AR_DD_CPNTHSON (X, i), hrelabelling,
			      relabelling);
	    }
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    AR_DD_ENTHDD (sons, i) = 
	      s_dd_relabel (ddm, AR_DD_EPNTHSON (X, i), hrelabelling,
			    relabelling);
	}

      R = ar_dd_find_or_add (ddm, newindex, arity, encoding, sons);

      ar_dd_free_dd_table (ddm, arity, encoding, sons); 
      ar_dd_memoize_operation (ddm, s_dd_relabel, X, relabelling, R);
    }

  return R;
}

			/* --------------- */

static ar_dd
s_dd_project_on_var_eq_val (ar_ddm ddm, ar_dd X, int var, int arity, 
			    int min, int max)
{
  ar_dd tmp1 = s_dd_for_var_eq_val (ddm, var, arity, min, max);
  ar_dd tmp2 = ar_dd_compute_and (ddm, X, tmp1);
  ar_dd result = s_project_dd (ddm, var, tmp2);
  AR_DD_FREE (tmp1);
  AR_DD_FREE (tmp2);

  return result;
}
#endif
			/* --------------- */

static ar_dd
s_dd_substitute (ar_ddm ddm, ar_dd X, struct substitution *subst)
{
  ar_dd tmp[3];
  ar_dd R;
  ar_dd one = ar_dd_one (ddm);
  ar_dd zero = ar_dd_zero (ddm);
  
  if (X == zero)
    return AR_DD_DUP (X);

  if (subst == NULL)
    {
      ccl_assert (X == one);
      return AR_DD_DUP (X);
    }

  R = (ar_dd) ar_dd_entry_for_operation (ddm, s_dd_substitute, X, subst);
  if (R != NULL)
    return AR_DD_DUP (R);

  if (X == one || subst->index < AR_DD_INDEX (X))
    {
      if (subst->all == zero)
	R = AR_DD_DUP (zero);
      else 
	{
	  tmp[0] = s_dd_substitute (ddm, X, subst->next);
	  if (subst->all == one)
	    R = AR_DD_DUP (tmp[0]);
	  else
	    R = ar_dd_compute_and (ddm, subst->all, tmp[0]);
	  AR_DD_FREE (tmp[0]);
	}
    }
  else 
    {
      int i;
      int arity = AR_DD_ARITY (X);
      ar_ddcode encoding = AR_DD_CODE (X);

      ccl_assert (subst->index == AR_DD_INDEX (X));

      R = AR_DD_DUP (zero);
      for (i = 0; i < arity; i++)
	{
	  int min = i, max = i;
	  ar_dd son = (encoding == COMPACTED) 
	    ? AR_DD_CPNTHSON (X, i)
	    : AR_DD_EPNTHSON (X, i);
	  
	  tmp[0] = s_dd_substitute (ddm, son, subst->next);
	  if (encoding == COMPACTED) 
	    {
	      min = AR_DD_MIN_OFFSET_NTHSON (X, i);
	      max = AR_DD_MAX_OFFSET_NTHSON (X, i);
	    }
	  
	  tmp[1] = AR_DD_DUP (subst->S[min]);
	  min++;
	  while (min <= max)
	    {
	      tmp[2] = ar_dd_compute_or (ddm, tmp[1], subst->S[min]);
	      AR_DD_FREE (tmp[1]);
	      tmp[1] = tmp[2];
	      min++;
	    }
	  tmp[2] = ar_dd_compute_and (ddm, tmp[0], tmp[1]);
	  AR_DD_FREE (tmp[0]);
	  AR_DD_FREE (tmp[1]);
	  tmp[0] = tmp[2];
	
	  tmp[1] = ar_dd_compute_or (ddm, tmp[0], R);
	  AR_DD_FREE (tmp[0]);
	  AR_DD_FREE (R);
	  R = tmp[1];
	}
    }
  ar_dd_memoize_operation (ddm, s_dd_substitute, X, subst, R);

  return R;
}


			/* --------------- */

static void
s_add_substitution (ar_ddm ddm, struct substitution **psubst, int index, 
		    int arity, ar_dd *S)
{  
  int i;

  while (*psubst != NULL && (*psubst)->index < index)
    psubst = &((*psubst)->next);

  if (*psubst == NULL || (*psubst)->index > index)
    {
      struct substitution *s = 
	ccl_malloc (sizeof (struct substitution) + 
		    (sizeof (ar_dd) * (arity -1)));
      s->index = index;
      s->arity = arity;
      s->all = AR_DD_DUP (S[0]);
      s->S[0] = AR_DD_DUP (S[0]);
      for (i = 1; i < arity; i++)
	{
	  ar_dd tmp = ar_dd_compute_or (ddm, s->all, S[i]);
	  AR_DD_FREE (s->all);
	  s->all = tmp;

	  s->S[i] = AR_DD_DUP (S[i]);
	}
      s->next = *psubst;
      *psubst = s;      
    }
  else
    {
      ar_dd all;
      ar_dd tmp;

      ccl_assert ((*psubst)->index == index);

      tmp = ar_dd_compute_and (ddm, (*psubst)->S[0], S[0]);
      AR_DD_FREE ((*psubst)->S[0]);
      (*psubst)->S[0] = tmp;
      all = AR_DD_DUP (tmp);

      for (i = 1; i < arity; i++)
	{
	  tmp = ar_dd_compute_and (ddm, (*psubst)->S[i], S[i]);
	  AR_DD_FREE ((*psubst)->S[i]);
	  (*psubst)->S[i] = tmp;

	  tmp = ar_dd_compute_or (ddm, all, tmp);
	  AR_DD_FREE (all);
	  all = tmp;
	}
      AR_DD_FREE ((*psubst)->all);
      (*psubst)->all = tmp;
    }
}

			/* --------------- */

static struct substitution *
s_build_substitution (ar_context *ctx, ar_ddm ddm, ar_mec5_relation *mrel,
		      int *slots_to_dd_indexes, ar_expr *e, ccl_hash *cache)
{
  struct substitution *result = NULL;
  int cindex;
  ccl_pair *pa;
  ccl_pair *pt;
  ccl_list *subtypes = ar_expr_call_get_subtypes (e);
  ccl_list *subargs = ar_expr_call_get_subargs (e, ctx);

  for (cindex = 0, pa = FIRST (subargs), pt = FIRST (subtypes); pa != NULL;
       cindex++, pa = CDR (pa), pt = CDR (pt))
    {
      int i;
      ar_expr *t = CAR (pa);
      ar_type *vtype = CAR (pt);
      int dd_cindex = ar_mec5_relation_get_ith_dd_index (mrel, cindex);
      int arity = ar_type_get_cardinality (vtype);
      ar_dd *tmp = ccl_new_array (ar_dd, arity);

      /*
      if (ccl_debug_is_on)	
	{
	  ccl_debug ("%d -> ", dd_cindex);
	  ar_expr_log (CCL_LOG_DEBUG, t);
	  ccl_debug ("\n");
	}
      */
      for (i = 0; i < arity; i++)
	{
	  ar_expr *cst = s_type_get_value (vtype, i);
	  ar_expr *t_eq_cst = ar_expr_crt_binary (AR_EXPR_EQ, t, cst);
	  tmp[i] = ar_boolean_expr_to_dd (ctx, ddm, slots_to_dd_indexes, cache,
					  t_eq_cst);
	  if (ccl_hash_find (cache, t_eq_cst))
	    ccl_hash_remove (cache);
	  ar_expr_del_reference (cst);
	  ar_expr_del_reference (t_eq_cst);
	}
      
      s_add_substitution (ddm, &result, dd_cindex, arity, tmp);
      for (i = 0; i < arity; i++)
	AR_DD_FREE (tmp[i]);
      ccl_delete (tmp);
    }

  return result;
}

			/* --------------- */
#if 0
static void
s_display_subst (struct substitution *S)
{
  while (S)
    {
      fprintf (stderr, "%d -> %d\n", S->index, AR_DD_INDEX (S->S));
      S = S->next;
    }
  fprintf (stderr, "\n");
}
#endif
			/* --------------- */

static void
s_delete_substitution (struct substitution *s)
{
  struct substitution *next;

  for (; s != NULL; s = next)
    {
      int i;
      next = s->next;
      AR_DD_FREE (s->all);
      for (i = 0; i < s->arity; i++)
	AR_DD_FREE (s->S[i]);
      ccl_delete (s);
    }
}

			/* --------------- */

static ar_dd 
s_dd_for_predicate (ar_context *ctx, ar_ddm ddm, int *slots_to_dd_indexes, 
		    ccl_hash *cache, ar_expr *e, int *p_changed)
{
  uint32_t timer;
  int nb_rels = 0;
  ar_dd *rels;
  ar_dd result;
  struct expr2dd_info *info;
  ar_identifier *relname = ar_signature_get_name (e->u.sig);
  ar_mec5_relation *mrel = ar_mec5_get_relation (relname);
  ar_identifier_del_reference (relname);

  if (ccl_debug_is_on)
    {
      ccl_debug ("compute ");
      ar_expr_log (CCL_LOG_DEBUG, e);
      timer = ccl_time_get_cpu_time_in_milliseconds ();
    }

  if (ccl_hash_find (cache, e))
    info = ccl_hash_get (cache);
  else
    {      
      info = ccl_new (struct expr2dd_info);
      ccl_hash_insert (cache, info);
      info->data = 
	s_build_substitution (ctx, ddm, mrel, slots_to_dd_indexes, e, cache);
      info->del = (ccl_delete_proc *) s_delete_substitution;
      info->val = NULL;
    }

  nb_rels = 0;
  rels = ar_mec5_relation_get_dds (mrel, &nb_rels);
  
  if (nb_rels == 0)
    result = ar_dd_dup_one (ddm);
  else
    {
      int i;

      result = s_dd_substitute (ddm, rels[0], info->data);
      for (i = 1; i < nb_rels; i++)
	{
	  ar_dd tmp1 = s_dd_substitute (ddm, rels[i], info->data);
	  ar_dd tmp2 = ar_dd_compute_and (ddm, result, tmp1);
	  AR_DD_FREE (tmp1);
	  AR_DD_FREE (result);
	  result = tmp2;
	}
    }
  *p_changed = (info->val != result);
  ccl_zdelete (AR_DD_FREE, info->val);    
  info->val = AR_DD_DUP (result);

  ar_mec5_relation_del_reference (mrel);

  if (ccl_debug_is_on)
    {
      timer = ccl_time_get_cpu_time_in_milliseconds () - timer;
      ccl_debug (" done within ");
      ccl_time_log (CCL_LOG_DEBUG, timer);
      ccl_debug ("\n");
    }

 return result;
}

			/* --------------- */

static void
s_delete_info_cache (void *p)
{
  struct expr2dd_info *info = p;

  if (info->del != NULL)
    info->del (info->data);
  if (info->val != NULL)
    AR_DD_FREE (info->val);
  ccl_delete (info);
}
