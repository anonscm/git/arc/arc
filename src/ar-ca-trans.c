/*
 * ar-ca-trans.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-array.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-constraint-automaton.h"

struct ar_ca_trans_st {
  int refcount;
  ar_ca_expr *guard;
  ar_ca_event *event;
  ar_ca_expr_array assignments;
};

			/* --------------- */

ar_ca_trans *
ar_ca_trans_create(ar_ca_expr *guard, ar_ca_event *ev)
{
  ar_ca_trans *result = ccl_new(ar_ca_trans);

  result->refcount = 1;
  result->guard = ar_ca_expr_add_reference(guard);
  result->event = ar_ca_event_add_reference(ev);
  ccl_array_init(result->assignments);

  return result;
}

			/* --------------- */

ar_ca_trans *
ar_ca_trans_add_reference(ar_ca_trans *t)
{
  ccl_pre( t != NULL );
  
  t->refcount++;

  return t;
}

			/* --------------- */

void
ar_ca_trans_del_reference(ar_ca_trans *t)
{
  int i;

  ccl_pre( t != NULL ); ccl_pre( t->refcount > 0 );
  
  if( --t->refcount > 0 )
    return;
  
  ar_ca_expr_del_reference(t->guard);
  ar_ca_event_del_reference(t->event);
  for(i = 0; i < t->assignments.size; i++)
    ar_ca_expr_del_reference(t->assignments.data[i]);
  ccl_array_delete(t->assignments);
  ccl_delete(t);
}

			/* --------------- */

ar_ca_expr *
ar_ca_trans_get_guard (const ar_ca_trans *t)
{
  ccl_pre (t != NULL);

  return ar_ca_expr_add_reference (t->guard);
}

ar_ca_expr *
ar_ca_trans_get_guard_with_domains (const ar_ca_trans *t)
{
  ar_ca_expr *result = ar_ca_trans_get_guard (t);
  int nba = ar_ca_trans_get_number_of_assignments (t);
  ar_ca_expr **a = ar_ca_trans_get_assignments (t);
  if (nba > 0)
    {
      ar_ca_expr *constraint = ar_ca_expr_crt_in_domain_of (a[1], a[0]);
      ar_ca_expr *notconstraint = ar_ca_expr_crt_not (constraint);
      ar_ca_expr *g_and_notconstraint =
	ar_ca_expr_crt_and (t->guard, notconstraint);
      if (ar_ca_expr_is_satisfiable (g_and_notconstraint))
	{
	  ar_ca_expr *tmp = ar_ca_expr_crt_and (result, constraint);
	  ar_ca_expr_del_reference (result);
	  result = tmp;
	}
      ar_ca_expr_del_reference (constraint);
      ar_ca_expr_del_reference (g_and_notconstraint);
      ar_ca_expr_del_reference (notconstraint);
    }
  
  return result;
}

			/* --------------- */

void
ar_ca_trans_set_guard(ar_ca_trans *t, ar_ca_expr *G)
{
  ccl_pre (t != NULL);
  ccl_pre (G != NULL);

  ccl_zdelete (ar_ca_expr_del_reference, t->guard);
  t->guard = ar_ca_expr_add_reference (G);
}

			/* --------------- */

int 
ar_ca_trans_evaluate_guard(ar_ca_trans *t)
{
  ccl_pre( t != NULL );

  ar_ca_expr_evaluate(t->guard,1);

  ccl_assert( ar_ca_expr_get_min(t->guard) == ar_ca_expr_get_max(t->guard) );

  return ar_ca_expr_get_min(t->guard);
}

			/* --------------- */

ar_ca_event *
ar_ca_trans_get_event (const ar_ca_trans *t)
{
  ccl_pre (t != NULL);

  return ar_ca_event_add_reference (t->event);
}

const ar_ca_event *
ar_ca_trans_get_const_event(const ar_ca_trans *t)
{
  ccl_pre (t != NULL);

  return t->event;
}

			/* --------------- */

void
ar_ca_trans_add_assignment(ar_ca_trans *t, ar_ca_expr *var, ar_ca_expr *val)
{
  ccl_pre( t != NULL );  ccl_pre( var != NULL );  ccl_pre( val != NULL );
  
  var = ar_ca_expr_add_reference(var);
  ccl_array_add(t->assignments,var);
  val = ar_ca_expr_add_reference(val);
  ccl_array_add(t->assignments,val);
}

			/* --------------- */

int
ar_ca_trans_get_number_of_assignments(const ar_ca_trans *t)
{
  ccl_pre( t != NULL );

  return t->assignments.size/2;
}

			/* --------------- */

ar_ca_expr **
ar_ca_trans_get_assignments(const ar_ca_trans *t)
{
  ccl_pre( t != NULL );

  return t->assignments.data;
}

			/* --------------- */

void
ar_ca_trans_log (ccl_log_type log, ar_ca_trans *t)
{
  ar_ca_trans_log_gen (log, t, 0, ".", NULL);
}

			/* --------------- */

void
ar_ca_trans_log_gen (ccl_log_type log, ar_ca_trans *t, int show_epsilon, 
		     const char *separator, const char *quote)
{
  int i;

  ar_ca_expr_log_gen (log, t->guard, separator, quote);
  ccl_log (log, " |- ");
  ar_ca_event_log_gen (log, t->event, show_epsilon, separator, quote);
  ccl_log (log, " -> ");
  for (i = 0; i < t->assignments.size; i += 2)
    {
      ar_ca_expr_log_gen (log, t->assignments.data[i], separator, quote);
      ccl_log (log, " := ");
      ar_ca_expr_log_gen (log, t->assignments.data[i+1], separator, quote);
      if (i + 2 != t->assignments.size)
	ccl_log (log, ", ");
    }
}

			/* --------------- */

ccl_list *
ar_ca_trans_get_variables (ar_ca_trans *t, ccl_list *result, int flags)
{
  int i;

  if (result == NULL)
    result = ccl_list_create ();

  if ((flags & AR_CA_R_VARS) != 0)
    result = ar_ca_expr_get_variables (t->guard, result);

  for (i = 0; i < t->assignments.size; i+=2)
    {
      if ((flags & AR_CA_W_VARS) != 0)
	result = ar_ca_expr_get_variables (t->assignments.data[i], result);
      if ((flags & AR_CA_R_VARS) != 0)
	result = ar_ca_expr_get_variables (t->assignments.data[i + 1], result);
    }


  return result;
}
