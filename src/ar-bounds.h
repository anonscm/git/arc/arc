/*
 * ar-bounds.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_BOUNDS_H
# define AR_BOUNDS_H

# include <ccl/ccl-log.h>
# include "ar-backtracking-stack.h"

typedef struct ar_bounds_st {
  int min;
  int max;
} ar_bounds;

			/* --------------- */

extern void
ar_bounds_init(ar_bounds *b, int min, int max);

extern void
ar_bounds_init_singleton(ar_bounds *b, int value);

extern void
ar_bounds_log(ccl_log_type log, const ar_bounds *b);

extern void
ar_bounds_save(ar_bounds *b, ar_backtracking_stack *bs);

extern int
ar_bounds_contains(const ar_bounds *b, int value);

extern int
ar_bounds_include(const ar_bounds *b, const ar_bounds *other);

extern void
ar_bounds_set(ar_bounds *b, int min, int max);

extern int 
ar_bounds_is_singleton(const ar_bounds *b);

extern void
ar_bounds_copy(ar_bounds *dst, const ar_bounds *src);

extern int
ar_bounds_are_equal(const ar_bounds *b1, const ar_bounds *b2);

extern void
ar_bounds_or(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_and(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_not(ar_bounds *r, ar_bounds *b);

extern void
ar_bounds_neg(ar_bounds *r, ar_bounds *b);

extern void
ar_bounds_eq(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_lt(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_add(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_mul(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_div(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_mod(ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_min (ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_max (ar_bounds *r, ar_bounds *b1, ar_bounds *b2);

extern void
ar_bounds_ite(ar_bounds *r, ar_bounds *i, ar_bounds *t, ar_bounds *e);

#endif /* ! AR_BOUNDS_H */
