/*
 * ar-sgs-quot.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-state-graph-semantics-p.h"

#define QUOT_NODES_LABELS_PROP 	   "translators.ts.quot.nodes.labels"
#define QUOT_NODES_TOOLTIPS_PROP   "translators.ts.quot.nodes.tooltips"   
#define QUOT_NODES_ATTRIBUTES_PROP "translators.ts.quot.nodes.attributes"
#define QUOT_EDGES_LABELS_PROP 	   "translators.ts.quot.edges.labels"  
#define QUOT_EDGES_ATTRIBUTES_PROP "translators.ts.quot.edges.attributes"
#define QUOT_EDGES_EPSILON_PROP    "translators.ts.quot.edges.epsilon"

			/* --------------- */

static void
s_digraph_header (ccl_log_type log, ar_sgs *sgs, ccl_config_table *conf,
		  int nb_classes);

static void
s_dot_class (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *C, 
	     ccl_config_table *conf);

static void
s_display_trans_between (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *C1, 
			 ar_sgs_set *C2, int label_enabled, int show_epsilon);

			/* --------------- */

void
ar_sgs_to_quot (ccl_log_type log, ar_sgs *sgs, 
		ccl_config_table *conf)
{
  ccl_pair *pC;
  ccl_list *classes = ar_sgs_compute_classes (sgs);
  int show_epsilon = 
    ccl_config_table_get_boolean (conf, QUOT_EDGES_EPSILON_PROP);
  int label_enabled = 
    ccl_config_table_get_boolean (conf, QUOT_EDGES_LABELS_PROP);

  s_digraph_header (log, sgs, conf, ccl_list_get_size (classes));

  for (pC = FIRST(classes); pC; pC = CDR (pC))
    {
      ar_sgs_set *C = (ar_sgs_set *) CAR (pC);

      s_dot_class (log, sgs, C, conf);
    }

  while (! ccl_list_is_empty (classes))
    {
      ar_sgs_set *C1 = ccl_list_take_first (classes);

      s_display_trans_between (log, sgs, C1, C1, label_enabled, show_epsilon);

      for (pC = FIRST (classes); pC; pC = CDR (pC))
	{
	  ar_sgs_set *C2 = CAR (pC);
	  s_display_trans_between (log, sgs, C1, C2, label_enabled, 
				   show_epsilon);
	  s_display_trans_between (log, sgs, C2, C1, label_enabled, 
				   show_epsilon);
	}

      ar_sgs_set_del_reference (C1);
    }
  ccl_log (log, "}\n");  
  ccl_list_delete (classes);
}


			/* --------------- */

static void
s_digraph_header (ccl_log_type log, ar_sgs *sgs, ccl_config_table *conf,
		  int nb_classes)
{
  const char *tmp = ccl_config_table_get (conf, QUOT_NODES_ATTRIBUTES_PROP);
  ar_identifier *name = ar_sgs_get_name(sgs);

  ccl_log (log, "digraph \"");
  ar_identifier_log (log, name);
  ccl_log (log, "/Q\" {\n");
  ccl_log (log, "  label=\"");
  ar_identifier_log (log, name);
  ccl_log (log, "/Q (%d classes)\";\n", nb_classes);

  ar_identifier_del_reference (name);

  if (tmp != NULL && *tmp != 0)
    ccl_log (log, "\tnode[%s];\n", tmp);

  tmp = ccl_config_table_get (conf, QUOT_EDGES_ATTRIBUTES_PROP);
  if (tmp != NULL && *tmp != 0) 
    ccl_log (log, "\tedge[%s];\n", tmp);
  ccl_log(log,"\n");
}

			/* --------------- */

static void
s_dot_class(ccl_log_type log, ar_sgs *sgs, ar_sgs_set *C,  
	    ccl_config_table *conf)
{
  int one = ccl_config_table_get_boolean (conf, QUOT_NODES_LABELS_PROP);

  ccl_log(log,"\tC%p[", C);

  ccl_log(log,"label=\"");
  if (one)
    ar_sgs_display_set (log, sgs, C, AR_SGS_DISPLAY_DOT);
  ccl_log(log,"\"");
  
  if (ccl_config_table_get_boolean (conf, QUOT_NODES_TOOLTIPS_PROP))
    {    
      ccl_list *properties = ccl_list_create ();
      ar_identifier_iterator *props = ar_sgs_get_state_sets (sgs);
  
      while (ccl_iterator_has_more_elements (props))
	{
	  ar_identifier *id = ccl_iterator_next_element (props);
	  ar_sgs_set *P = ar_sgs_get_state_set (sgs, id);
	  ar_sgs_set *C_and_P = ar_sgs_compute_intersection (sgs, P, C);
	  
	  if (! ar_sgs_set_is_empty (sgs, C_and_P))
	    ccl_list_add (properties, id);
	  else
	    ar_identifier_del_reference (id);
	  ar_sgs_set_del_reference (C_and_P);
	  ar_sgs_set_del_reference (P);
	}

      if (! ccl_list_is_empty (properties))
	{
	  ar_identifier *id = 
	    (ar_identifier *) ccl_list_take_first (properties);

	  if (one) 
	    ccl_log (log, ",");
	  
	  ccl_log (log, "URL=\"#\", tooltip=\"");
	  ar_identifier_log (log, id);
	  ar_identifier_del_reference(id);

	  while (! ccl_list_is_empty (properties))
	    {
	      id = (ar_identifier *) ccl_list_take_first (properties);
	      ccl_log(log,", ");
	      ar_identifier_log (log, id);
	      ar_identifier_del_reference(id);
	    }
	  ccl_log(log,"\"");
	}
      ccl_list_delete(properties);
      ccl_iterator_delete (props);
    }
  ccl_log(log,"];\n");
}

			/* --------------- */

static void
s_display_trans_between (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *C1, 
			 ar_sgs_set *C2, int label_enabled, int show_epsilon)
{
  ar_sgs_set *tmp[3];
  ar_sgs_set *edges;

  tmp[0] = ar_sgs_compute_rsrc (sgs, C1);
  tmp[1] = ar_sgs_compute_rtgt (sgs, C2);
  edges = ar_sgs_compute_intersection (sgs, tmp[0], tmp[1]);

  ar_sgs_set_del_reference (tmp[0]);
  ar_sgs_set_del_reference (tmp[1]);
  
  if (! ar_sgs_set_is_empty (sgs, edges))
    {
      if (label_enabled)
	{
	  ccl_hash *events = ar_sgs_set_get_events (sgs, edges);
	  ccl_pointer_iterator *ei = ccl_hash_get_elements (events);
      
	  while (ccl_iterator_has_more_elements (ei))
	    {
	      ar_ca_event *e = ccl_iterator_next_element (ei);
	      ccl_log (log, "\tC%p -> C%p [label=\"", C1, C2);
	      ar_ca_event_log_gen (log, e, show_epsilon, ".", NULL);
	      ccl_log (log, "\"];\n");
	    }
	  ccl_iterator_delete (ei);
	  ccl_hash_delete (events);
	}
      else
	{
	  ccl_log (log, "\tC%p -> C%p;\n", C1, C2);
	}
    }
  ar_sgs_set_del_reference (edges);
}
