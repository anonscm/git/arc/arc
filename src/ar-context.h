/*
 * ar-context.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CONTEXT_H
# define AR_CONTEXT_H

# include "ar-identifier.h"
# include "ar-constant.h"
# include "ar-signature.h"
# include "ar-type.h"
# include <sas/sas-params.h>

typedef struct ar_context_st ar_context;
typedef struct ar_context_slot_st ar_context_slot;
typedef struct ar_context_parameter_st ar_context_parameter;
CCL_ITERATOR_TYPEDEF(ar_context_slot_iterator,ar_context_slot *);

			/* --------------- */

struct ar_expr_st;

extern ar_context *
ar_context_create_top(void);
extern ar_context *
ar_context_create(ar_context *parent);
extern ar_context *
ar_context_create_from_slots(ar_context *parent, ccl_list *slots);
extern ar_context *
ar_context_add_reference(ar_context *ctx);
extern void
ar_context_del_reference(ar_context *ctx);
extern ar_context *
ar_context_get_parent(ar_context *ctx);

			/* --------------- */

extern void
ar_context_add_type(ar_context *ctx, ar_identifier *id, ar_type *type);
extern int
ar_context_has_type(ar_context *ctx, ar_identifier *id);
extern ar_type *
ar_context_get_type(ar_context *ctx, ar_identifier *id);
extern void
ar_context_remove_type(ar_context *ctx, ar_identifier *id);
extern ccl_list *
ar_context_get_types(ar_context *ctx);

			/* --------------- */

extern void
ar_context_add_signature(ar_context *ctx, ar_identifier *id, 
			 ar_signature *signature);
extern int
ar_context_has_signature(ar_context *ctx, ar_identifier *id);
extern ar_signature *
ar_context_get_signature(ar_context *ctx, ar_identifier *id);
extern void
ar_context_remove_signature(ar_context *ctx, ar_identifier *id);
extern ccl_list *
ar_context_get_signatures(ar_context *ctx);

			/* --------------- */

extern void
ar_context_add_law_parameter (ar_context *ctx, ar_identifier *id, 
			  sas_law_param *law_param);
extern int
ar_context_has_law_parameter (ar_context *ctx, ar_identifier *id);
extern sas_law_param *
ar_context_get_law_parameter (ar_context *ctx, ar_identifier *id);
extern void
ar_context_remove_law_parameter (ar_context *ctx, ar_identifier *id);
extern ccl_list *
ar_context_get_law_parameters (ar_context *ctx);

			/* --------------- */

extern void
ar_context_add_slot(ar_context *ctx, ar_identifier *id, ar_context_slot *slot);
extern int
ar_context_has_slot(ar_context *ctx, ar_identifier *id);
extern ar_context_slot *
ar_context_get_slot(ar_context *ctx, const ar_identifier *id);
extern void
ar_context_remove_slot(ar_context *ctx, ar_identifier *id);
extern ccl_list *
ar_context_get_slots(ar_context *ctx, uint32_t mask);
extern ccl_list *
ar_context_get_local_slots(ar_context *ctx, uint32_t mask);
extern void
ar_context_log_slots(ar_context *ctx, uint32_t mask,
		     ccl_list *(*flags_to_strings)(uint32_t flags));

extern ccl_list *
ar_context_expand_composite_slot(ar_context *ctx, ar_context_slot *slot,
				 ccl_list *result);

			/* --------------- */

extern ar_context_slot *
ar_context_slot_create(void);
extern ar_context_slot *
ar_context_slot_create_filled(int index,
			      ar_identifier *name,
			      ar_type *type,
			      uint32_t flags,
			      ccl_list *attr,
			      struct ar_expr_st *value);
extern ar_context_slot *
ar_context_slot_add_reference(ar_context_slot *slot);
extern void
ar_context_slot_del_reference(ar_context_slot *slot);
extern int
ar_context_slot_get_index(const ar_context_slot *slot);
extern void
ar_context_slot_set_index(ar_context_slot *slot, int index);
extern ar_identifier *
ar_context_slot_get_name(const ar_context_slot *slot);
extern void
ar_context_slot_set_name(ar_context_slot *slot, ar_identifier *id);
extern ar_type *
ar_context_slot_get_type(const ar_context_slot *slot);
extern void
ar_context_slot_set_type(ar_context_slot *slot, ar_type *type);
extern uint32_t
ar_context_slot_get_flags(const ar_context_slot *slot);
extern void
ar_context_slot_set_flags(ar_context_slot *slot, uint32_t flags);
extern ccl_list *
ar_context_slot_get_attributes(const ar_context_slot *slot);
extern void
ar_context_slot_set_attributes(ar_context_slot *slot, ccl_list *attr);
extern struct ar_expr_st *
ar_context_slot_get_value(const ar_context_slot *slot);
extern int
ar_context_slot_has_value(ar_context_slot *slot);
extern void
ar_context_slot_set_value(ar_context_slot *slot, struct ar_expr_st *value);
extern ar_context_slot *
ar_context_slot_rename(ar_context_slot *slot, ar_identifier *newname);

			/* --------------- */

extern ccl_list *
ar_context_slot_expand(int index, ar_identifier *name, ar_type *type,
		       uint32_t flags, ccl_list *attr, 
		       struct ar_expr_st *value, ccl_list *result);

extern ccl_list *
ar_context_slot_expand_slot (int index, ar_context_slot *sl, ccl_list *result);

#endif /* ! AR_CONTEXT_H */
