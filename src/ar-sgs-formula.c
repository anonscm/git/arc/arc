/*
 * ar-sgs-formula.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-exception.h>
#include <ccl/ccl-assert.h>
#include "ar-state-graph-semantics-p.h"

			/* --------------- */

static void
s_assign_set_type (ar_sgs_formula *F, int is_state);

static ar_sgs_formula *
s_allocate_formula (ar_sgs_formula_type type, int is_state);

static void
s_delete_formula (ar_sgs_formula *F);

static ar_sgs_formula *
s_crt_unary (ar_sgs_formula_type type, ar_sgs_formula *F, int is_state);

static ar_sgs_formula *
s_crt_binary (ar_sgs_formula_type type, ar_sgs_formula *F1, ar_sgs_formula *F2,
	      int is_state);

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_cst (ar_sgs_set *set, int is_state)
{
  ar_sgs_formula *result = s_allocate_formula (AR_SGS_F_CST, is_state);

  result->set = ar_sgs_set_add_reference (set);

  return result;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_var (ar_identifier *varname)
{
  ar_sgs_formula *result = 
    s_allocate_formula (AR_SGS_F_VAR, AR_SGS_UNDEF_SET_KIND);

  result->ident = ar_identifier_add_reference (varname);

  return result;
}


			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_assert (ar_sgs_formula *S)
{
  ar_sgs_formula *result = s_crt_unary (AR_SGS_F_ASSERT, S, 1);

  s_assign_set_type (S, 1);
  
  return result;  
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_src (ar_sgs_formula *T)
{
  ar_sgs_formula *result = s_crt_unary (AR_SGS_F_SRC, T, 1);

  s_assign_set_type (T, 0);
  
  return result;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_tgt (ar_sgs_formula *T)
{
  ar_sgs_formula *result = s_crt_unary (AR_SGS_F_TGT, T, 1);

  s_assign_set_type (T, 0);
  
  return result;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_rsrc (ar_sgs_formula *S)
{
  ar_sgs_formula *result = s_crt_unary (AR_SGS_F_RSRC, S, 0);

  s_assign_set_type (S, 1);
  
  return result;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_rtgt (ar_sgs_formula *S)
{
  ar_sgs_formula *result = s_crt_unary (AR_SGS_F_RTGT, S, 0);

  s_assign_set_type (S, 1);
  
  return result;
}


			/* --------------- */

static int
s_set_kind_for_binary_op (ar_sgs_formula *F1, ar_sgs_formula *F2)
{
  if (F1->is_state != AR_SGS_UNDEF_SET_KIND)
    return F1->is_state;

  if (F2->is_state != AR_SGS_UNDEF_SET_KIND)
    return F2->is_state;

  return AR_SGS_UNDEF_SET_KIND;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_or (ar_sgs_formula *F1, ar_sgs_formula *F2)
{
  int is_state = s_set_kind_for_binary_op (F1, F2);
  ar_sgs_formula *R = s_crt_binary (AR_SGS_F_OR, F1, F2, is_state);
  s_assign_set_type (F1, is_state);
  s_assign_set_type (F2, is_state);

  return R;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_and (ar_sgs_formula *F1, ar_sgs_formula *F2)
{
  int is_state = s_set_kind_for_binary_op (F1, F2);
  ar_sgs_formula *R = s_crt_binary (AR_SGS_F_AND, F1, F2, is_state);
  s_assign_set_type (F1, is_state);
  s_assign_set_type (F2, is_state);

  return R;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_sub (ar_sgs_formula *F1, ar_sgs_formula *F2)
{
  ar_sgs_formula *aux1 = ar_sgs_formula_crt_not (F2);
  ar_sgs_formula *R = ar_sgs_formula_crt_and (F1, aux1);
  ar_sgs_formula_del_reference (aux1);

  return R;
}

			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_crt_not (ar_sgs_formula *F)
{
  return s_crt_unary (AR_SGS_F_NOT, F, F->is_state);
}


			/* --------------- */

ar_sgs_formula *
ar_sgs_formula_add_reference (ar_sgs_formula *F)
{
  ccl_pre (F != NULL);
  ccl_pre (F->refcount > 0);

  F->refcount++;

  return F;
}

			/* --------------- */

void
ar_sgs_formula_del_reference (ar_sgs_formula *F)
{
  ccl_pre (F != NULL);
  ccl_pre (F->refcount > 0);
  
  F->refcount--;
  if (F->refcount == 0)
    s_delete_formula (F);
}

			/* --------------- */

int
ar_sgs_formula_is_state_formula (ar_sgs_formula *F)
{
  ccl_pre (F != NULL);

  return F->is_state;
}

			/* --------------- */

int
ar_sgs_formula_is_constant (ar_sgs_formula *F)
{
  int result = 1;

  ccl_pre (F != NULL);

  switch (F->type) 
    {
    case AR_SGS_F_VAR: 
      result = 0;
      break;
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      result = result && ar_sgs_formula_is_constant (F->args[1]);
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_TGT:
    case AR_SGS_F_RSRC: 
    case AR_SGS_F_RTGT:
    case AR_SGS_F_NOT: 
      result = result && ar_sgs_formula_is_constant (F->args[0]);
      break;
    case AR_SGS_F_CST: 
      break;
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }

  return result;
}

			/* --------------- */

void
ar_sgs_formula_get_varnames (ar_sgs_formula *F, ccl_list *variables)
{
  ccl_pre (F != NULL);
  ccl_pre (variables != NULL);

  switch (F->type) 
    {
    case AR_SGS_F_VAR: 
      if (!ccl_list_has (variables, F->ident))
	ccl_list_add (variables, F->ident);
      break;
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      ar_sgs_formula_get_varnames (F->args[1], variables);
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_TGT:
    case AR_SGS_F_RSRC: 
    case AR_SGS_F_RTGT:
    case AR_SGS_F_NOT: 
      ar_sgs_formula_get_varnames (F->args[0], variables);
      break;
    case AR_SGS_F_CST: 
      break;
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }
}

			/* --------------- */

static void
s_compute_constant_formula (ar_sgs_formula *F, ar_sgs *sgs)

{
  ccl_pre (F != NULL);

  if (F->set != NULL)
    return;

  if (F->type == AR_SGS_F_VAR)
    {
      if (ar_sgs_has_state_set (sgs, F->ident))
	F->set = ar_sgs_get_state_set (sgs, F->ident);
      else
	{
	  ccl_assert (ar_sgs_has_trans_set (sgs, F->ident));
	  F->set = ar_sgs_get_trans_set (sgs, F->ident);
	}
    }
  else
    {
      int i;

      for (i = 0; i < 2; i++)
	{
	  if (F->args[i] != NULL)
	    s_compute_constant_formula (F->args[i], sgs);
	}

      switch (F->type) 
	{
	case AR_SGS_F_OR: 
	  F->set = ar_sgs_compute_union (sgs, F->args[0]->set, F->args[1]->set);
	  break;
	case AR_SGS_F_AND: 
	  F->set = ar_sgs_compute_intersection (sgs, F->args[0]->set, 
						F->args[1]->set);
	  break;
	case AR_SGS_F_NOT:
	  F->set = ar_sgs_compute_complement (sgs, F->args[0]->set);
	  break;
	case AR_SGS_F_ASSERT: 
	  F->set = ar_sgs_compute_assert (sgs, F->args[0]->set);
	  break;
	case AR_SGS_F_SRC: 
	  F->set = ar_sgs_compute_src (sgs, F->args[0]->set);
	  break;
	case AR_SGS_F_TGT:
	  F->set = ar_sgs_compute_tgt (sgs, F->args[0]->set);
	  break;
	case AR_SGS_F_RSRC: 
	  F->set = ar_sgs_compute_rsrc (sgs, F->args[0]->set);
	  break;
	case AR_SGS_F_RTGT: 
	  F->set = ar_sgs_compute_rtgt (sgs, F->args[0]->set);
	  break;
	default:
	  ccl_throw (internal_error, "bad SGS formula type");
	}
    }
}

			/* --------------- */

ar_sgs_set *
ar_sgs_formula_compute_constant_formula (ar_sgs_formula *F, ar_sgs *sgs)
{
  ar_sgs_set *result;

  s_compute_constant_formula (F, sgs);
  result = ar_sgs_set_add_reference (F->set);  
  ar_sgs_formula_clean_sets (F);

  return result;
}

			/* --------------- */

void
ar_sgs_formula_clean_sets (ar_sgs_formula *F)
{
  ccl_pre (F != NULL);

  if (F->set != NULL && F->type != AR_SGS_F_CST)
    {
      ar_sgs_set_del_reference (F->set);
      F->set = NULL;
    }

  switch (F->type) 
    {
    case AR_SGS_F_CST:
    case AR_SGS_F_VAR: 
      break;
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      ar_sgs_formula_clean_sets (F->args[1]);
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_TGT:
    case AR_SGS_F_RSRC: 
    case AR_SGS_F_RTGT:
    case AR_SGS_F_NOT: 
      ar_sgs_formula_clean_sets (F->args[0]);
      break;
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }
}

			/* --------------- */

void
ar_sgs_formula_clean_indexes (ar_sgs_formula *F)
{
  ccl_pre (F != NULL);

  F->index = -1;

  switch (F->type) 
    {
    case AR_SGS_F_CST:
    case AR_SGS_F_VAR: 
      break;
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      ar_sgs_formula_clean_indexes (F->args[1]);
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_TGT:
    case AR_SGS_F_RSRC: 
    case AR_SGS_F_RTGT:
    case AR_SGS_F_NOT: 
      ar_sgs_formula_clean_indexes (F->args[0]);
      break;
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }
}

			/* --------------- */

void
ar_sgs_formula_set_indexes (ar_sgs_formula *F, int *p_last_index)
{
  if (F->index >= 0)
    return;

  F->index = *p_last_index;
  (*p_last_index)++;

  switch (F->type) 
    {
    case AR_SGS_F_CST:
    case AR_SGS_F_VAR: 
      break;
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      ar_sgs_formula_set_indexes (F->args[1], p_last_index);
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_TGT:
    case AR_SGS_F_RSRC: 
    case AR_SGS_F_RTGT:
    case AR_SGS_F_NOT: 
      ar_sgs_formula_set_indexes (F->args[0], p_last_index);
      break;
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }
}

			/* --------------- */

static ar_sgs_formula *
s_allocate_formula (ar_sgs_formula_type type, int is_state)
{
  ar_sgs_formula *result = ccl_new (ar_sgs_formula);

  result->refcount = 1;
  result->type = type;
  result->set = NULL;
  result->is_state = is_state;
  result->ident = NULL;

  return result;
}

			/* --------------- */

static void
s_delete_formula (ar_sgs_formula *F)
{
  ccl_pre (F != NULL);

  ccl_zdelete (ar_sgs_set_del_reference, F->set);

  switch (F->type) 
    {
    case AR_SGS_F_CST: 
      break;
    case AR_SGS_F_VAR: 
      ar_identifier_del_reference (F->ident);
      break;
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      ar_sgs_formula_del_reference (F->args[1]);
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_TGT:
    case AR_SGS_F_RSRC: 
    case AR_SGS_F_RTGT:
    case AR_SGS_F_NOT: 
      ar_sgs_formula_del_reference (F->args[0]);
      break;
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }
  ccl_delete (F);
}

			/* --------------- */

static ar_sgs_formula *
s_crt_unary (ar_sgs_formula_type type, ar_sgs_formula *F, int is_state)
{
  ar_sgs_formula *result = s_allocate_formula (type, is_state);

  ccl_pre (F != NULL);

  result->args[0] = ar_sgs_formula_add_reference (F);

  return result;
}

			/* --------------- */

static ar_sgs_formula *
s_crt_binary (ar_sgs_formula_type type, ar_sgs_formula *F1, ar_sgs_formula *F2,
	      int is_state)
{
  ar_sgs_formula *result = s_allocate_formula (type, is_state);

  ccl_pre (F1 != NULL);
  ccl_pre (F2 != NULL);

  result->args[0] = ar_sgs_formula_add_reference (F1);
  result->args[1] = ar_sgs_formula_add_reference (F2);

  return result;
}

			/* --------------- */

static void
s_assign_set_type (ar_sgs_formula *F, int is_state)
{
  ccl_pre (F != NULL);
  ccl_pre ((F->is_state == AR_SGS_UNDEF_SET_KIND) || 
	   (F->is_state == is_state));
    
  if (F->is_state != AR_SGS_UNDEF_SET_KIND)
    return;

  switch (F->type) 
    {
    case AR_SGS_F_OR: 
    case AR_SGS_F_AND: 
      s_assign_set_type (F->args[1], is_state);      
    case AR_SGS_F_NOT: 
      s_assign_set_type (F->args[0], is_state);

    case AR_SGS_F_VAR: 
      F->is_state = is_state;
      break;


    case AR_SGS_F_CST: 
    case AR_SGS_F_SRC: 
    case AR_SGS_F_ASSERT: 
    case AR_SGS_F_TGT: 
    case AR_SGS_F_RSRC:
    case AR_SGS_F_RTGT: 
    default:
      ccl_throw (internal_error, "bad SGS formula type");
    }
}
