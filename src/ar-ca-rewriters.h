/*
 * ar-ca-rewriters.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_REWRITERS_H
# define AR_CA_REWRITERS_H

# include <deps/fdset.h>
# include "ar-constraint-automaton.h"

/*!
 * Replace each functional dependency defined in fds. Each replaced variable
 * is then removed form the CA.
 */
extern ar_ca *
ar_ca_remove_functional_dependencies (const ar_ca *ca, const fdset *fds);

extern void
ar_ca_reduce_assertions (ar_ca *ca);

/*!
 * Keep elements that modify values of V i.e. assertions that intersect V and
 * transitions that write at least one variable in V. Then the closure of this
 * elements is taken to give a coherent automaton. If wired_state_variables is
 * true then V is extended with state variables that related by assignments
 * in transitions.
 */
extern ar_ca *
ar_ca_restrict_to_variables (ar_ca *ca, ccl_hash *V, ccl_list **closure,
			     int wire_state_variables);

/*!
 * Simply duplicate storage structure of the CA. Expressions are not duplicated;
 * only their counter of references are incremented. Transitions are actually
 * duplicated.
 */
extern ar_ca *
ar_ca_duplicate (const ar_ca *ca);


/*!
 * Trim given CA to elements appearing in 'elements' table. 'elements' must be
 * built in a wayt that guarantee a coherent result.
 */
extern void
ar_ca_trim (ar_ca *ca, ccl_hash *elements);

extern void
ar_ca_trim_to_reachables_from_variables (ar_ca *ca, ccl_hash *variables);

extern void
ar_ca_trim_to_reachables_from_formula (ar_ca *ca, ar_ca_expr *F);

extern void
ar_ca_trim_to_reachables_from_formulas (ar_ca *ca, ccl_list *formulas);

extern ar_ca_expr *
ar_ca_rewrite_expr_with_fd (const fdset *fds, ar_ca_expr *e, int simplify);
extern ar_ca_expr *
ar_ca_rewrite_expr_with_const_and_var_fd (const fdset *fds, ar_ca_expr *e,
					  int simplify);

extern ar_ca_expr *
ar_ca_rewrite_expr_with_bounded_fd (const fdset *fds, ar_ca_expr *e,
				    int simplify, int maxsize);

#endif /* ! AR_CA_REWRITERS_H */
