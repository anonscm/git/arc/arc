/*
 * ar-semantics.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-model.h"
#include "ar-flat-semantics.h"
#include "ar-ca-semantics.h"
#include <ts/ts-semantics.h>
#include "ar-rel-semantics.h"
#include "ar-semantics.h"

typedef struct node_semantics_st {
  ar_node *node;
  ar_node *flat_node;
  ar_ca *ca;
  ar_ca *rca;
  ar_ts *ts;
  ar_relsem *relsem;
} node_semantics;

			/* --------------- */


static void 
s_semantics_clean(node_semantics *sem);
static void 
s_semantics_delete(node_semantics *sem);

			/* --------------- */

CCL_DEFINE_EXCEPTION (domain_cardinality_exception, exception);
CCL_DEFINE_EXCEPTION (abstract_type_exception, exception);

static ar_idtable *SEMANTICS;


void
ar_semantics_init (ccl_config_table *conf)
{
  SEMANTICS = ar_idtable_create (1, NULL, (ccl_delete_proc *)
				 s_semantics_delete);
  ar_relsem_init (conf);
}

			/* --------------- */

void
ar_semantics_terminate(void)
{
  ar_relsem_terminate ();
  ar_idtable_del_reference (SEMANTICS);
}

			/* --------------- */

void *
ar_semantics_get (ar_node *node, ar_semantics_type type)
{
  node_semantics *s;
  void *result = NULL;
  ar_identifier *nodename;

  ccl_pre (node != NULL);

  nodename = ar_node_get_name (node);
  s = ar_idtable_get (SEMANTICS, nodename);
  if (s == NULL)
    {
      s = ccl_new (node_semantics);
      s->node = ar_node_add_reference (node);
      s->flat_node = NULL;
      ar_idtable_put (SEMANTICS, nodename, s);
    }

  if (ar_node_get_uid (s->node) != ar_node_get_uid (node))
    {
      s_semantics_clean (s);
      s->node = ar_node_add_reference (node);
    }
  ar_identifier_del_reference (nodename);

  switch (type) 
    {
    case AR_SEMANTICS_FLATTENED :
      if (s->flat_node == NULL)
	s->flat_node = ar_compute_flat_semantics (s->node, AR_MODEL_CONTEXT);
      result = ar_node_add_reference (s->flat_node);
      break;

    case AR_SEMANTICS_CONSTRAINT_AUTOMATON :
      if (s->ca == NULL)
	{
	  ar_node *flat = ar_semantics_get (node, AR_SEMANTICS_FLATTENED);
	  ccl_try (exception)
	    {
	      s->ca = ar_compute_constraint_automaton_semantics (flat);
	    }
	  ccl_catch
	    {
	      ar_node_del_reference (flat);
	      ccl_rethrow ();
	    }
	  ccl_end_try;
	  ar_node_del_reference (flat);
	}
      result = ar_ca_add_reference (s->ca);
      break;

    case AR_SEMANTICS_TRANSITION_SYSTEM :
      if (s->ts == NULL)
	s->ts = ar_compute_transition_system_semantics (s->node);
      result = ar_ts_add_reference (s->ts);
      break;

    case AR_SEMANTICS_RELATIONS:
      if (s->relsem == NULL)
	{
	  ar_ca *ca = 
	    ar_semantics_get (node, AR_SEMANTICS_CONSTRAINT_AUTOMATON);
	  s->relsem = ar_compute_relational_semantics (ca, NULL, NULL);
	  ar_ca_del_reference (ca);
	}
      result = ar_relsem_add_reference (s->relsem);
      break;
  }

  return result;
}

			/* --------------- */

extern void
ar_semantics_cleanup (ar_node *node, ar_semantics_type type)
{
  node_semantics *s;
  ar_identifier *nodename;

  ccl_pre (node != NULL);

  nodename = ar_node_get_name (node);
  s = ar_idtable_get (SEMANTICS, nodename);
  ar_identifier_del_reference (nodename);

  if (s == NULL)
    return;

  switch (type) 
    {
    case AR_SEMANTICS_FLATTENED :
      ccl_zdelete (ar_node_del_reference, s->flat_node);
      s->flat_node = NULL;
      break;

    case AR_SEMANTICS_CONSTRAINT_AUTOMATON :
      ccl_zdelete (ar_ca_del_reference, s->ca);
      s->ca = NULL;
      break;

    case AR_SEMANTICS_TRANSITION_SYSTEM :
      ccl_zdelete (ar_ts_del_reference, s->ts);
      s->ts = NULL;
      break;

    case AR_SEMANTICS_RELATIONS:
      ccl_zdelete (ar_relsem_del_reference, s->relsem);
      s->relsem = NULL;
      break;
  }
}

			/* --------------- */

static void 
s_semantics_clean (node_semantics *sem)
{
  ccl_pre (sem != NULL);

  ccl_zdelete (ar_node_del_reference, sem->node);
  sem->node = NULL;

  ccl_zdelete (ar_node_del_reference, sem->flat_node);
  sem->flat_node = NULL;

  ccl_zdelete (ar_ca_del_reference, sem->ca);
  sem->ca = NULL;

  ccl_zdelete (ar_ts_del_reference, sem->ts);
  sem->ts = NULL;

  ccl_zdelete (ar_relsem_del_reference, sem->relsem);
  sem->relsem = NULL;
}

			/* --------------- */

static void 
s_semantics_delete(node_semantics *sem)
{
  ccl_pre( sem != NULL );

  s_semantics_clean(sem);
  ccl_delete(sem);
}
