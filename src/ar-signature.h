/*
 * ar-signature.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_SIGNATURE_H
# define AR_SIGNATURE_H

# include <ccl/ccl-log.h>
# include <ccl/ccl-serializer.h>
# include "ar-type.h"

typedef struct ar_signature_st ar_signature;

extern ar_signature *
ar_signature_create(ar_identifier *signame, ar_type *ret);

extern ar_signature *
ar_signature_add_reference(ar_signature *sig);

extern void
ar_signature_del_reference(ar_signature *sig);

extern void
ar_signature_log(ccl_log_type log, const ar_signature *sig);

extern void
ar_signature_add_arg_domain(ar_signature *sig, ar_type *type);

extern ar_identifier *
ar_signature_get_name(const ar_signature *sig);

extern int
ar_signature_get_arity(const ar_signature *sig);

extern ar_type *
ar_signature_get_ith_arg_domain(const ar_signature *sig, int i);

extern int
ar_signature_get_size (const ar_signature *sig);

extern ar_type *
ar_signature_get_return_domain(const ar_signature *sig);

extern int
ar_signature_equals(const ar_signature *sig, const ar_signature *other);

extern void
ar_signature_write (ar_signature *sig, FILE *out, ccl_serializer_status *p_err);

extern void
ar_signature_read (ar_signature **p_sig, FILE *in, 
		   ccl_serializer_status *p_err);

extern ar_signature *
ar_signature_rename (ar_signature *sig, ar_identifier *newname);

#endif /* ! AR_SIGNATURE_H */
