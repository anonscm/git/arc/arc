/*
 * ar-interp-acheck.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdio.h>
#include <math.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-time.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-list.h>
#include "ar-semantics.h"
#include "ar-state-graph-semantics.h"
#include "commands/validate.h"
#include "ts/ts-translators.h"
#include "ts/ts-algorithms.h"
#include "ts/ts.h"
#include "ctlstar.h"
#include "ar-acheck-preferences.h"
#include "ar-ca-semantics.h"
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-expr.h"
#include "ar-interp-altarica-p.h"
#include "ar-acheck.h"
#include "ar-interp-acheck.h"
#include "mec5/mec5.h"
#include "mec5/mec5-interp-ast.h"
#include "arc-shell.h"
#include "ar-rel-semantics.h"

#define TEST_DESC_FMT "%-50s"
#define IS_LABELLED_WITH(_t,_l) ((_t)->node_type == AR_TREE_ ## _l)
#define TIMERS_ARE_ENABLED() \
  ccl_config_table_get_boolean (conf, ACHECK_PREF_TIMERS_STR)

#define MUST_CLEANUP_SEMANTICS() \
  ccl_config_table_get_boolean (conf, ACHECK_PREF_CLEANUP_SEMANTICS_STR)

#define NRTEST_FAILURE_ABORTS()	\
  ccl_config_table_get_boolean (conf, ACHECK_PREF_NRTEST_FAILURE_ABORTS_STR)

			/* --------------- */

CCL_DEFINE_EXCEPTION (acheck_interpretation_exception, 
		      altarica_interpretation_exception);

			/* --------------- */


static ar_sgs_formula *
s_interp_sgs_formula (altarica_tree *t, ar_sgs *sgs, int *is_state);

			/* --------------- */

static ar_sgs_formula *
s_interp_state_sgs_formula (altarica_tree *t, ar_sgs *sgs)
{
  int is_state = 0;
  ar_sgs_formula *result = s_interp_sgs_formula (t, sgs, &is_state);

  if (is_state == 0)
    {
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a state formula.\n");
      ar_sgs_formula_del_reference (result);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }

  return result;
}

			/* --------------- */

static ar_sgs_formula *
s_interp_transition_sgs_formula (altarica_tree *t, ar_sgs *sgs)
{
  int is_state = 0;
  ar_sgs_formula *result = s_interp_sgs_formula (t, sgs, &is_state);

  if (is_state == 1)
    {
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a transition formula.\n");
      ar_sgs_formula_del_reference (result);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_eval_state_or_transition_sgs_formula (altarica_tree *t, ar_sgs *sgs,
					      int *is_state)
{
  ar_sgs_set *result = NULL;
  ar_sgs_formula *F = s_interp_sgs_formula (t, sgs, is_state);

  if (!ar_sgs_formula_is_constant (F))
    {      
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a constant state formula.\n");
      ar_sgs_formula_del_reference (F);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }
  else
    {
      result = ar_sgs_formula_compute_constant_formula (F, sgs);      
    }
  ar_sgs_formula_del_reference (F);

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_eval_state_sgs_formula (altarica_tree *t, ar_sgs *sgs)
{
  ar_sgs_set *result = NULL;
  ar_sgs_formula *F = s_interp_state_sgs_formula (t, sgs);

  if (!ar_sgs_formula_is_constant (F))
    {      
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a constant state formula.\n");
      ar_sgs_formula_del_reference (F);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }
  else
    {
      result = ar_sgs_formula_compute_constant_formula (F, sgs);      
    }
  ar_sgs_formula_del_reference (F);

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_eval_transition_sgs_formula (altarica_tree *t, ar_sgs *sgs)
{
  ar_sgs_set *result = NULL;
  ar_sgs_formula *F = s_interp_transition_sgs_formula (t, sgs);

  if (!ar_sgs_formula_is_constant (F))
    {
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a constant transition formula.\n");
      ar_sgs_formula_del_reference (F);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }
  else
    {
      result = ar_sgs_formula_compute_constant_formula (F, sgs);
    }
  ar_sgs_formula_del_reference (F);

  return result;
}

			/* --------------- */


static ar_sgs_formula *
s_interp_sgs_formula (altarica_tree *t, ar_sgs *sgs, int *is_state)
{
  int i;
  ar_sgs_formula *result = NULL;
  ar_sgs_formula *op[3] = { NULL, NULL, NULL };
  ar_sgs_set *sets[4] = { NULL, NULL, NULL };
  int is[3] = { 0, 0, 0 };
  
  ccl_try (acheck_interpretation_exception)
    {
      switch (t->node_type) 
	{
	case AR_TREE_IDENTIFIER : 
	  {
	    ar_identifier *tmp = ar_interp_symbol_as_identifier (t);
	    
	    if (ar_sgs_has_set (sgs, tmp, is_state))
	      {
		ar_sgs_set *m;
		
		if (*is_state)
		  m = ar_sgs_get_state_set (sgs, tmp);
		else
		  m = ar_sgs_get_trans_set (sgs, tmp);
		result = ar_sgs_formula_crt_cst (m, *is_state);
		ar_sgs_set_del_reference (m);
	      }
	    else 
	      {
		result = ar_sgs_formula_crt_var (tmp);
		*is_state = AR_SGS_UNDEF_SET_KIND;
	      }
	    ar_identifier_del_reference(tmp);
	  }
	  break;
	  

	case AR_TREE_PARENTHEZED_EXPR :
	  result = s_interp_sgs_formula (t->child, sgs, is_state);
	  break;
	  
	case AR_TREE_AND : 
	case AR_TREE_OR : 
	case AR_TREE_SUB :
	  op[0] = s_interp_sgs_formula (t->child, sgs, &is[0]);

	  if (is[0] == AR_SGS_UNDEF_SET_KIND)
	    op[1] = s_interp_sgs_formula (t->child->next, sgs, &is[1]);
	  else if (is[0] == 1) 
	    op[1] = s_interp_state_sgs_formula (t->child->next, sgs);
	  else
	    op[1] = s_interp_transition_sgs_formula (t->child->next, sgs);
    
	  if (IS_LABELLED_BY (t, AND)) 
	    result = ar_sgs_formula_crt_and (op[0], op[1]);
	  else if (IS_LABELLED_BY (t, OR))
	    result = ar_sgs_formula_crt_or (op[0], op[1]);
	  else
	    result = ar_sgs_formula_crt_sub (op[0], op[1]);
	  
	  *is_state = ar_sgs_formula_is_state_formula (result);
	  break;

	case AR_TREE_NOT :
	  op[0] = s_interp_sgs_formula (t->child, sgs, is_state);
	  result = ar_sgs_formula_crt_not (op[0]);
	  break;
	
	case AR_TREE_RSRC : 
	case AR_TREE_RTGT :
	  *is_state = 0;
	  op[0] = s_interp_state_sgs_formula (t->child, sgs);

	  if (IS_LABELLED_BY (t, RSRC))
	    result = ar_sgs_formula_crt_rsrc (op[0]);
	  else 
	    result = ar_sgs_formula_crt_rtgt (op[0]);      
	  break;

	case AR_TREE_SRC : 
	case AR_TREE_TGT :
	  *is_state = 1;
	  op[0] = s_interp_transition_sgs_formula (t->child, sgs);

	  if (IS_LABELLED_BY (t, SRC))
	    result = ar_sgs_formula_crt_src (op[0]);
	  else 
	    result = ar_sgs_formula_crt_tgt (op[0]);
	  break;

	case AR_TREE_ASSERT : 
	  *is_state = 1;
	  op[0] = s_interp_state_sgs_formula (t->child, sgs);

	  result = ar_sgs_formula_crt_assert (op[0]);
	  break;
	  
	case AR_TREE_REACH : 
	case AR_TREE_COREACH :
	  *is_state = 1;
	  sets[0] = s_interp_eval_state_sgs_formula (t->child, sgs);
	  sets[1] = s_interp_eval_transition_sgs_formula (t->child->next, sgs);
	  
	  if (IS_LABELLED_BY (t, REACH))
	    sets[2] = ar_sgs_compute_reach (sgs, sets[0], sets[1]);
	  else 
	    sets[2] = ar_sgs_compute_coreach (sgs, sets[0], sets[1]);	    
	  result = ar_sgs_formula_crt_cst (sets[2], *is_state);
	  break;

	case AR_TREE_PICK : 
	case AR_TREE_PROJ_S : 
	case AR_TREE_PROJ_F :
	  sets[0] = 
	    s_interp_eval_state_or_transition_sgs_formula (t->child, sgs, 
							   is_state);
	  if (IS_LABELLED_BY (t, PICK))
	    sets[1] = ar_sgs_compute_pick (sgs, sets[0]);
	  else 
	    sets[1] = ar_sgs_compute_proj (sgs, sets[0], 
					   IS_LABELLED_BY (t, PROJ_S));
	  result = ar_sgs_formula_crt_cst (sets[1], *is_state);
	  break;

	case AR_TREE_LOOP :
	  *is_state = 0;
	  sets[0] = s_interp_eval_transition_sgs_formula (t->child, sgs);
	  sets[1] = s_interp_eval_transition_sgs_formula (t->child->next, sgs);
	  sets[2] = ar_sgs_compute_loop (sgs, sets[0], sets[1]);
	  result = ar_sgs_formula_crt_cst (sets[2], *is_state);
	  break;

	case AR_TREE_TRACE :
	  *is_state = 0;
	  sets[0] = s_interp_eval_state_sgs_formula (t->child, sgs);
	  sets[1] = s_interp_eval_transition_sgs_formula (t->child->next, sgs);
	  sets[2] = s_interp_eval_state_sgs_formula (t->child->next->next, sgs);
	  sets[3] = ar_sgs_compute_trace (sgs, sets[0], sets[1], sets[2]);
	  result = ar_sgs_formula_crt_cst (sets[3], *is_state);
	  break;

	case AR_TREE_UNAV :
	  *is_state = 1;
	  sets[0] = s_interp_eval_transition_sgs_formula (t->child, sgs);
	  sets[1] = s_interp_eval_state_sgs_formula (t->child->next, sgs);
	  sets[2] = ar_sgs_compute_unav (sgs, sets[0], sets[1]);
	  result = ar_sgs_formula_crt_cst (sets[2], *is_state);
	  break;

	case AR_TREE_EXPR : 
	  {
	    ar_ca_exprman *man = ar_sgs_get_expr_manager (sgs);
	    ar_node *node = ar_sgs_get_node (sgs);
	    ar_context *ctx = ar_node_get_context (node);
	    ar_expr *expr = NULL;
	    ar_expr *flat_expr = NULL;
	    ar_ca_expr *ca_expr = NULL;

	    *is_state = 1;
	    ccl_try (exception)
	      {
		expr = 
		  ar_interp_boolean_expression(t->child, ctx,
					       ar_interp_acheck_member_access);
		flat_expr = ar_expr_remove_composite_slots (expr, ctx);
		ca_expr = ar_model_expr_to_ca_expr (flat_expr, man, NULL);
		sets[0] = ar_sgs_compute_state_set (sgs, ca_expr);
		result = ar_sgs_formula_crt_cst (sets[0], *is_state);
	      }
	    ccl_no_catch;

	    ccl_zdelete (ar_ca_expr_del_reference, ca_expr);
	    ccl_zdelete (ar_expr_del_reference, flat_expr);
	    ccl_zdelete (ar_expr_del_reference, expr);

	    ar_context_del_reference (ctx);
	    ar_node_del_reference (node);
	    ar_ca_exprman_del_reference (man);
	  }
	  break;

      case AR_TREE_LABEL : 
      case AR_TREE_EVENT_ATTRIBUTE : 
	{
	  ar_ca *ca = ar_sgs_get_constraint_automaton (sgs);
	  ar_node *node = ar_ca_get_node (ca);
	  ar_context *ctx = ar_node_get_context (node);
	  ar_identifier *tmp = ar_interp_identifier_path (t->child, ctx);
	  ar_context_del_reference (ctx);
	  ar_node_del_reference (node);

	  if (IS_LABELLED_BY (t, LABEL))
	    {
	      if (ar_ca_has_subevent(ca, tmp))
		{
		  *is_state = 0;	  
		  sets[0] = ar_sgs_compute_label (sgs, tmp);
		  result = ar_sgs_formula_crt_cst (sets[0], *is_state);
		}
	      else
		{
		  ccl_error("%s:%d: error: undefined event '", t->filename, 
			    t->line);
		  ar_identifier_log (CCL_LOG_ERROR, tmp);
		  ccl_error ("'.\n");
		}
	    }
	  else
	    {
	      *is_state = 0;	  
	      sets[0] = ar_sgs_compute_attribute (sgs, tmp);
	      result = ar_sgs_formula_crt_cst (sets[0], *is_state);
	    }
	  ar_ca_del_reference (ca);
	  ar_identifier_del_reference (tmp);
	}
	break;

      default :
	ccl_throw_no_msg (internal_error);
      }
    }
  ccl_no_catch;

  for (i = 0; i < (int) (sizeof (op) / sizeof (op[0])); i++)
    ccl_zdelete (ar_sgs_formula_del_reference, op[i]);
    
  for (i = 0; i < (int) (sizeof (sets) / sizeof (sets[0])); i++)
    ccl_zdelete (ar_sgs_set_del_reference, sets[i]);

  if (result == NULL)
    ccl_throw_no_msg (acheck_interpretation_exception);

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_state_or_transition_formula(altarica_tree *t, ar_sgs *sgs, 
				     int *is_state);

			/* --------------- */

static ar_sgs_set *
s_interp_state_formula(altarica_tree *t, ar_sgs *sgs)
{
  int is_state = 0;
  ar_sgs_set *result = s_interp_state_or_transition_formula (t, sgs, &is_state);

  if (! is_state)
    {
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a state formula.\n");
      ar_sgs_set_del_reference (result);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_transition_formula (altarica_tree *t, ar_sgs *sgs)
{
  int is_state = 0;
  ar_sgs_set *result = s_interp_state_or_transition_formula (t, sgs, &is_state);

  if (is_state)
    {
      ccl_error ("%s:%d: error: '", t->filename, t->line);
      ar_tree_log_acheck_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
      ccl_error ("' is not a transition formula.\n");
      ar_sgs_set_del_reference (result);
      ccl_throw_no_msg (acheck_interpretation_exception);
    }

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_state_or_transition_formula (altarica_tree *t, ar_sgs *sgs, 
				      int *is_state)
{
  int i;
  ar_sgs_set *result = NULL;
  ar_sgs_set *op[3] = { NULL, NULL, NULL };
  int is[3] = { 0, 0, 0 };
  
  ccl_try (acheck_interpretation_exception)
    {
      switch (t->node_type) 
	{
	case AR_TREE_IDENTIFIER : 
	  {
	    ar_identifier *tmp = ar_interp_symbol_as_identifier (t);
	  
	    if (ar_sgs_has_set (sgs, tmp, is_state)) 
	      {
		if (*is_state) 
		  result = ar_sgs_get_state_set (sgs, tmp);
		else 
		  result = ar_sgs_get_trans_set (sgs, tmp);
		ccl_assert (result != NULL);
	      }
	    else 
	      {
		ccl_error ("%s:%d: error: undefined property '%s'.\n",
			   t->filename, t->line, t->value.id_value);
	      }
	    ar_identifier_del_reference(tmp);
	    break;
	  }

	case AR_TREE_PARENTHEZED_EXPR :
	  result = s_interp_state_or_transition_formula (t->child, sgs, 
							 is_state);
	  break;
	  
	  
	case AR_TREE_AND : case AR_TREE_OR : case AR_TREE_SUB :
	  op[0] = s_interp_state_or_transition_formula (t->child, sgs, &is[0]);
	  *is_state = is[0];
	
	  if (is[0]) 
	    op[1] = s_interp_state_formula (t->child->next, sgs);
	  else
	    op[1] = s_interp_transition_formula(t->child->next, sgs);
    
	  if (IS_LABELLED_BY (t, AND)) 
	    result = ar_sgs_compute_intersection (sgs, op[0], op[1]);
	  else if (IS_LABELLED_BY (t, OR))
	    result = ar_sgs_compute_union (sgs, op[0], op[1]);
	  else
	    result = ar_sgs_compute_difference (sgs, op[0], op[1]);    
	  break;

	case AR_TREE_NOT :
	  op[0] = s_interp_state_or_transition_formula (t->child, sgs, 
							is_state);
	  result = ar_sgs_compute_complement (sgs, op[0]);
	  break;
	
	case AR_TREE_RSRC : case AR_TREE_RTGT :
	  *is_state = 0;
	  op[0] = s_interp_state_formula (t->child, sgs);

	  if (IS_LABELLED_BY(t, RSRC))
	    result = ar_sgs_compute_rsrc (sgs, op[0]);
	  else 
	    result = ar_sgs_compute_rtgt(sgs, op[0]);      
	  break;
	  
	case AR_TREE_SRC : case AR_TREE_TGT :
	  *is_state = 1;
	  op[0] = s_interp_transition_formula (t->child, sgs);
	  
	  if (IS_LABELLED_BY (t, SRC))
	    result = ar_sgs_compute_src (sgs, op[0]);
	  else 
	    result = ar_sgs_compute_tgt (sgs, op[0]);
	  break;

	case AR_TREE_ASSERT:
	  *is_state = 1;
	  op[0] = s_interp_state_formula (t->child, sgs);
	  
	  result = ar_sgs_compute_assert (sgs, op[0]);
	  break;

	case AR_TREE_REACH : case AR_TREE_COREACH :
	  *is_state = 1;
	  op[0] = s_interp_state_formula (t->child, sgs);
	  op[1] = s_interp_transition_formula (t->child->next, sgs);
	  
	  if (IS_LABELLED_BY(t,REACH))
	    result = ar_sgs_compute_reach (sgs, op[0], op[1]);
	  else 
	    result = ar_sgs_compute_coreach (sgs, op[0], op[1]);
	  break;

	case AR_TREE_PICK: case AR_TREE_PROJ_S: case AR_TREE_PROJ_F:
	  op[0] = 
	    s_interp_state_or_transition_formula (t->child, sgs, is_state);

	  if (IS_LABELLED_BY (t, PICK))
	    result = ar_sgs_compute_pick (sgs, op[0]);
	  else 
	    result = ar_sgs_compute_proj (sgs, op[0], 
					   IS_LABELLED_BY (t, PROJ_S));
	  break;

	case AR_TREE_LOOP :
	  *is_state = 0;
	  op[0] = s_interp_transition_formula (t->child,sgs);
	  op[1] = s_interp_transition_formula (t->child->next,sgs);
	  result = ar_sgs_compute_loop (sgs, op[0], op[1]);
	  break;

	case AR_TREE_TRACE :
	  *is_state = 0;
	  op[0] = s_interp_state_formula (t->child, sgs);
	  op[1] = s_interp_transition_formula (t->child->next, sgs);
	  op[2] = s_interp_state_formula (t->child->next->next, sgs);
	  result = ar_sgs_compute_trace (sgs, op[0], op[1], op[2]);
	  break;

	case AR_TREE_UNAV :
	  *is_state = 1;
	  op[0] = s_interp_transition_formula (t->child, sgs);
	  op[1] = s_interp_state_formula (t->child->next, sgs);
	  result = ar_sgs_compute_unav (sgs, op[0], op[1]);
	  break;

	case AR_TREE_EXPR : 
	  {
	    ar_ca *ca = ar_sgs_get_constraint_automaton (sgs);
	    ar_ca_exprman *man = ar_ca_get_expression_manager (ca);
	    ar_node *node = ar_ca_get_node (ca);
	    ar_context *ctx = ar_node_get_context (node);
	    ar_expr *expr = NULL;
	    ar_expr *flat_expr = NULL;
	    ar_ca_expr *ca_expr = NULL;


	    *is_state = 1;
	    ccl_try (exception)
  	      {
		expr = 
		  ar_interp_boolean_expression (t->child, ctx,
						ar_interp_acheck_member_access);
		flat_expr = ar_expr_remove_composite_slots (expr, ctx);
		ca_expr = ar_model_expr_to_ca_expr (flat_expr, man, NULL);
		result = ar_sgs_compute_state_set (sgs, ca_expr);
	      }
	    ccl_no_catch;
	  
	    ccl_zdelete (ar_ca_expr_del_reference, ca_expr);
	    ccl_zdelete (ar_expr_del_reference, flat_expr);
	    ccl_zdelete (ar_expr_del_reference, expr);
	    ar_context_del_reference (ctx);
	    ar_node_del_reference (node);
	    ar_ca_exprman_del_reference (man);
	    ar_ca_del_reference (ca);
	    break;
	  }

	case AR_TREE_LABEL : 
	case AR_TREE_EVENT_ATTRIBUTE : 
	  {
	    ar_ca *ca = ar_sgs_get_constraint_automaton (sgs);
	    ar_node *node = ar_ca_get_node (ca);
	    ar_context *ctx = ar_node_get_context (node);
	    ar_identifier *tmp = ar_interp_identifier_path (t->child, ctx);
	    ar_context_del_reference (ctx);
	    ar_node_del_reference (node);

	    if (IS_LABELLED_BY (t, LABEL))
	      {
		if (ar_ca_has_subevent (ca, tmp))
		  {
		    *is_state = 0;	  
		    result = ar_sgs_compute_label (sgs, tmp);
		  }
		else
		  {
		    ccl_error("%s:%d: error: undefined event '", t->filename,
			      t->line);
		    ar_identifier_log (CCL_LOG_ERROR, tmp);
		    ccl_error ("'.\n");
		  }
	      }
	    else
	      {
		*is_state = 0;	  
		result = ar_sgs_compute_attribute (sgs, tmp);
	      }
	    ar_ca_del_reference (ca);
	    ar_identifier_del_reference (tmp);
	    break;
	  }

	default :
	  ccl_throw_no_msg (internal_error);
	}
    }
  ccl_no_catch;

  for(i = 0; i < (int)(sizeof (op)/sizeof (op[0])); i++)
    {
      if (op[i] != NULL)
	ar_sgs_set_del_reference (op[i]);
    }

  if (result == NULL)
    ccl_throw_no_msg (acheck_interpretation_exception);

  return result;
}

			/* --------------- */

ctlstar_formula *
ar_interp_ctlstar_formula (altarica_tree *t, ar_sgs *sgs, int allow_paths,
			   ar_idtable *tmpvars)
{
  ctlstar_formula *args[2] = { NULL, NULL };
  ctlstar_formula *result = NULL;

  ccl_try (altarica_interpretation_exception)
    {
      ccl_exception_local_variable (ctlstar_formula_del_reference, &args[0]);
      ccl_exception_local_variable (ctlstar_formula_del_reference, &args[1]);

      switch (t->node_type)
	{
	case AR_TREE_CTLSTAR_ATOM:      
	  if (IS_LABELLED_BY (t->child, TRUE))
	    result = ctlstar_formula_crt_true ();
	  else if (IS_LABELLED_BY (t->child, FALSE))
	    result = ctlstar_formula_crt_false ();
	  else if (IS_LABELLED_BY (t->child, IDENTIFIER))
	    {
	      ar_identifier *tmp = ar_interp_symbol_as_identifier (t->child);
	    
	      if (ar_sgs_has_state_set (sgs, tmp))
		{
		  /* we enforce to computation of the set in order to register
		     it in Mec5 context. */
		  ar_sgs_set *set = 
		    s_interp_eval_state_sgs_formula (t->child, sgs);
	      
		  result = ctlstar_formula_crt_variable (tmp);
		  ar_sgs_set_del_reference (set);
		}
	      else
		{
		  ar_identifier_del_reference (tmp);
		  ccl_error ("%s:%d: error: '", t->filename, t->line);
		  ar_tree_log_acheck_expr (CCL_LOG_ERROR, t->child, 
					   AR_NO_ID_TRANSLATION);
		  ccl_error ("' is not a state formula.\n");
		  ccl_throw_no_msg (acheck_interpretation_exception);
		}
	      ar_identifier_del_reference (tmp);
	    }
	  else
	    {	      
	      ar_identifier *varname;
	      ar_identifier *any_s = ar_identifier_create ("any_s");
	      ar_sgs_set *tmp1 = ar_sgs_get_state_set (sgs, any_s);
	      ar_sgs_set *tmp2 = 
		s_interp_eval_state_sgs_formula (t->child, sgs);
	      ar_sgs_set *set = ar_sgs_compute_intersection (sgs, tmp1, tmp2);
	      ar_sgs_set_del_reference (tmp1);
	      ar_sgs_set_del_reference (tmp2);
	      ar_identifier_del_reference (any_s);
	      result = ctlstar_formula_crt_dummy_variable (&varname);
	      ar_idtable_put (tmpvars, varname, t);
	      ar_identifier_del_reference (varname);
	      ar_sgs_add_state_set (sgs, varname, set);
	      ar_sgs_set_del_reference (set);
	    }
	  break;

	case AR_TREE_CTLSTAR_PARENTHESED:
	  result = ar_interp_ctlstar_formula (t->child, sgs, allow_paths, 
					      tmpvars);
	  break;

	case AR_TREE_CTLSTAR_X: case AR_TREE_CTLSTAR_F: case AR_TREE_CTLSTAR_G: 
	case AR_TREE_CTLSTAR_U: case AR_TREE_CTLSTAR_W:
	  if (! allow_paths)
	    {
	      ccl_error ("%s:%d: error: use path formula '", t->filename, 
			 t->line);
	      ar_tree_log_ctlstar_expr (CCL_LOG_ERROR, t, AR_NO_ID_TRANSLATION);
	      ccl_error ("' where a state formula is expected.\n");
	      ccl_throw_no_msg (acheck_interpretation_exception);
	    }

	default:	  
	  if (IS_LABELLED_BY (t, CTLSTAR_A) || 
	      IS_LABELLED_BY (t, CTLSTAR_E))
	    args[0] = ar_interp_ctlstar_formula (t->child, sgs, 1, tmpvars);
	  else
	    args[0] = ar_interp_ctlstar_formula (t->child, sgs, allow_paths,
						 tmpvars);

	  if (t->child->next != NULL)
	    args[1] = ar_interp_ctlstar_formula (t->child->next, sgs, 
						 allow_paths, tmpvars);

	  switch (t->node_type)
	    {
	    case AR_TREE_CTLSTAR_AND:
	      result = ctlstar_formula_crt_and (args[0], args[1]); break;
	    case AR_TREE_CTLSTAR_OR:
	      result = ctlstar_formula_crt_or (args[0], args[1]); break;
	    case AR_TREE_CTLSTAR_IMPLY:
	      result = ctlstar_formula_crt_imply (args[0], args[1]); break;
	    case AR_TREE_CTLSTAR_XOR:
	      result = ctlstar_formula_crt_xor (args[0], args[1]); break;
	    case AR_TREE_CTLSTAR_EQUIV:
	      result = ctlstar_formula_crt_equiv (args[0], args[1]); break;
	    case AR_TREE_CTLSTAR_NOT: 
	      result = ctlstar_formula_crt_not (args[0]); break;
	    case AR_TREE_CTLSTAR_AX:
	      result = ctlstar_formula_crt_AX (args[0]); break;
	    case AR_TREE_CTLSTAR_AG:
	      result = ctlstar_formula_crt_AG (args[0]); break;
	    case AR_TREE_CTLSTAR_AF:
	      result = ctlstar_formula_crt_AF (args[0]); break;
	    case AR_TREE_CTLSTAR_EX:
	      result = ctlstar_formula_crt_EX (args[0]); break;
	    case AR_TREE_CTLSTAR_EG:
	      result = ctlstar_formula_crt_EG (args[0]); break;
	    case AR_TREE_CTLSTAR_EF:
	      result = ctlstar_formula_crt_EF (args[0]); break;
	    case AR_TREE_CTLSTAR_A: 
	      result = ctlstar_formula_crt_A (args[0]); break;
	    case AR_TREE_CTLSTAR_E: 
	      result = ctlstar_formula_crt_E (args[0]); break;	      
	    case AR_TREE_CTLSTAR_X: 
	      result = ctlstar_formula_crt_X (args[0]); break;	      
	    case AR_TREE_CTLSTAR_F: 
	      result = ctlstar_formula_crt_F (args[0]); break;	      
	    case AR_TREE_CTLSTAR_G: 
	      result = ctlstar_formula_crt_G (args[0]); break;	      
	    case AR_TREE_CTLSTAR_U:
	      result = ctlstar_formula_crt_U (args[0], args[1]); break;	      
	    case AR_TREE_CTLSTAR_W:
	      result = ctlstar_formula_crt_W (args[0], args[1]); break;	      
	    default:
	      ccl_throw_no_msg (internal_error);
	    }
	}
    }
  ccl_catch_rethrow;

  ccl_zdelete (ctlstar_formula_del_reference, args[0]);
  ccl_zdelete (ctlstar_formula_del_reference, args[1]);

  if (result == NULL)
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static ctlstar_formula *
s_interp_ctlstar_state_formula (altarica_tree *t, ar_sgs *sgs,
				ar_idtable *tmpvars)
{
  return ar_interp_ctlstar_formula (t, sgs, 0, tmpvars);
}

			/* --------------- */

static int
s_is_constant_set (ar_identifier *setid, void *data)
{
  return ar_sgs_has_state_set (data, setid);
}

			/* --------------- */

static void 
s_ctl_translation (altarica_tree *t, ar_sgs *sgs, const char *nodename, 
		   ccl_config_table *conf)
{
  ar_idtable *tmpvars = ar_idtable_create (0, NULL, NULL);
  ctlstar_formula *R = NULL;

  ccl_try (altarica_interpretation_exception)
    {
      ar_identifier *Rname;
      altarica_tree *eqsys;
      ar_identifier *nodeid;

      R = s_interp_ctlstar_state_formula (t->child, sgs, tmpvars);

      nodeid = ar_identifier_create (nodename);
      Rname = ar_identifier_create ("R$");
      eqsys = ctlstar_compute_equation_system (R, s_is_constant_set, sgs, 
					       nodeid, Rname);
      ctlstar_formula_del_reference (R);
      ar_identifier_del_reference (Rname);
      ar_identifier_del_reference (nodeid);

      ccl_display ("translation of ");
      ar_tree_log_ctlstar_expr (CCL_LOG_DISPLAY, t->child, 
				AR_NO_ID_TRANSLATION);
      ccl_display (" into Mec 5 specs:\n");
      ar_tree_log_mecv (CCL_LOG_DISPLAY, eqsys, AR_NO_ID_TRANSLATION);
      ccl_display ("\n");
      ccl_parse_tree_delete_tree (eqsys);
    }
  ccl_no_catch;

  ar_idtable_del_reference (tmpvars);
}

			/* --------------- */

ar_sgs_set *
ar_interp_eval_ctlstar_formula (ctlstar_formula *spec, ar_sgs *sgs, 
				ccl_config_table *conf)
{
  ar_sgs_set *result = NULL;
  ar_identifier *Rname = ar_identifier_create ("R$~");
  ar_identifier *nodeid = ar_sgs_get_name (sgs);
  altarica_tree *eqsys = 
    ctlstar_compute_equation_system (spec, s_is_constant_set, sgs, nodeid, 
				     Rname);
  ar_identifier_del_reference (nodeid);

  if (ccl_debug_is_on)
    ar_tree_log_mecv (CCL_LOG_DEBUG, eqsys, AR_NO_ID_TRANSLATION);

  ccl_try (exception)
    {
      if (ar_interp_mec5_equation_system (eqsys, conf, 0))
	{
	  ar_mec5_relation *mrel = ar_mec5_get_relation (Rname);

	  ccl_assert (mrel != NULL);
	  
	  result =
	    ar_relsem_translate_configuration_relation ((ar_relsem *) sgs,
							mrel);
	  ccl_assert (ar_mec5_relation_get_cardinality (mrel) == 
		      ar_sgs_get_set_cardinality (sgs, result));
	  ar_mec5_relation_del_reference (mrel);
	  ar_mec5_remove_relation (Rname);
	}
      else
	{
	  ccl_unreachable ();
	}
    }
  ccl_no_catch;

  ccl_parse_tree_delete_tree (eqsys);
  ar_identifier_del_reference (Rname);

  if (result == NULL)
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_interp_ctlstar (altarica_tree *t, ar_sgs *sgs, ccl_config_table *conf)
{
  ar_sgs_set *result= NULL;
  ar_idtable *tmpvars = ar_idtable_create (0, NULL, NULL);

  ccl_pre (IS_LABELLED_BY (t, CTLSTAR_SPEC));

  ccl_try (exception)
    {
      ctlstar_formula *spec = 
	s_interp_ctlstar_state_formula (t->child, sgs, tmpvars);
      result = ar_interp_eval_ctlstar_formula (spec, sgs, conf);
      ctlstar_formula_del_reference (spec);
    }
  ccl_no_catch;

  {
    ar_identifier_iterator *ik = ar_idtable_get_keys (tmpvars);
    
    while (ccl_iterator_has_more_elements (ik))
      {
	ar_identifier *id = ccl_iterator_next_element (ik);
	ar_sgs_remove_state_set (sgs, id);
	ar_identifier_del_reference (id);
      }
    ccl_iterator_delete (ik);
    ar_idtable_del_reference (tmpvars);
  }

  if (result == NULL)
    ccl_rethrow ();

  return result;
}

			/* --------------- */

static void
s_check_sign_and_type (altarica_tree *t, int positive, ar_sgs *sgs, 
		       int *is_state, ar_identifier *qvar, int qsign)
{
  int is, sr, error = 0;

  switch( t->node_type ) 
    {
    case AR_TREE_IDENTIFIER : 
      {
	ar_identifier *tmp = ar_interp_symbol_as_identifier (t);
    
	if (tmp == qvar)
	  {
	    if (positive != qsign)
	      {
		ccl_error("%s:%d: error: %s is not monotone.\n",
			  t->filename, t->line, t->value.id_value);
		error = 1;
	      }
	  }
	else if (! ar_sgs_has_set (sgs, tmp, is_state))
	  {
	    ccl_error ("%s:%d: error: undefined property '%s'.\n",
		       t->filename, t->line, t->value.id_value);
	    
	    error = 1;
	  }
	ar_identifier_del_reference (tmp);
	break;
      }

    case AR_TREE_PARENTHEZED_EXPR :
      s_check_sign_and_type (t->child, positive, sgs, is_state, qvar, qsign);
      break;

    case AR_TREE_AND : case AR_TREE_OR : case AR_TREE_SUB :
      sr = positive;
      if (t->node_type == AR_TREE_SUB) 
	sr = ! sr;
      s_check_sign_and_type (t->child, positive, sgs, is_state, qvar, qsign);
      s_check_sign_and_type (t->child->next, sr, sgs, &is, qvar, qsign);
      break;

    case AR_TREE_NOT : 
      s_check_sign_and_type (t->child, !positive, sgs, is_state, qvar, qsign);
      break;

    case AR_TREE_PROJ_S : case AR_TREE_PROJ_F : case AR_TREE_PICK : 
      s_check_sign_and_type (t->child, positive, sgs, is_state, qvar, qsign);
      break;

    case AR_TREE_RTGT : case AR_TREE_RSRC : 
      *is_state = 0; 
      s_check_sign_and_type (t->child, positive, sgs, &is, qvar, qsign);
      break;
      

    case AR_TREE_SRC  : case AR_TREE_TGT  : 
      *is_state = 1; 
      s_check_sign_and_type (t->child, positive, sgs, &is, qvar, qsign);
      break;

    case AR_TREE_ASSERT : 
      *is_state = 1; 
      s_check_sign_and_type (t->child, positive, sgs, &is, qvar, qsign);
      break;

    case AR_TREE_EXPR     : *is_state = 1; break;
    case AR_TREE_LABEL    : *is_state = 0; break;
    case AR_TREE_COREACH:
      ccl_error("%s:%d: error: coreach is no allowed in fixpoint.\n",
		t->filename, t->line);
      error = 1;
      break;

    case AR_TREE_REACH :
      ccl_error("%s:%d: error: reach is no allowed in fixpoint.\n",
		t->filename, t->line);
      error = 1;
      break;

    case AR_TREE_LOOP :
      ccl_error ("%s:%d: error: loop is no allowed in fixpoint.\n",
		 t->filename, t->line);
      error = 1;
      break;

    case AR_TREE_TRACE :
      ccl_error ("%s:%d: error: trace is no allowed in fixpoint.\n",
		 t->filename, t->line);
      error = 1;
      break;

    case AR_TREE_UNAV :
      ccl_error ("%s:%d: error: unav is no allowed in fixpoint.\n",
		 t->filename, t->line);
      error = 1;
      break;

    default : 
      ccl_throw_no_msg (internal_error);
    }

  if (error)
    ccl_throw_no_msg (acheck_interpretation_exception);
}

			/* --------------- */
#if 0
static int
s_mark_equality (ar_ts_mark *M1, ar_ts_mark *M2)
{
  int w = ar_ts_mark_get_width (M1);

  if (w != ar_ts_mark_get_width (M2))
    return 0;

  while (w--)
    if ((ar_ts_mark_has (M1, w) && !ar_ts_mark_has (M2, w)) ||
	(!ar_ts_mark_has (M1, w) && ar_ts_mark_has (M2, w)))
      return 0;
  return 1;
}
#endif
			/* --------------- */

static void
s_interp_fixpoint(altarica_tree *t, int positive, ar_sgs *sgs, 
		  ar_identifier *name)
{
  ar_sgs_set *R = NULL;
  int is_state = 0;
  ar_sgs_formula *F;

  s_check_sign_and_type (t, positive, sgs, &is_state, name, positive);

  if (is_state)
    F = s_interp_state_sgs_formula (t, sgs);
  else
    F = s_interp_transition_sgs_formula (t, sgs);

  if (positive) 
    R = ar_sgs_compute_lfp (sgs, name, F);
  else
    R = ar_sgs_compute_gfp (sgs, name, F);
  ar_sgs_formula_del_reference (F);

  if (is_state) 
    ar_sgs_add_state_set (sgs, name, R);
  else
    ar_sgs_add_trans_set (sgs, name, R);
  ar_sgs_set_del_reference (R);
}

			/* --------------- */

static int
s_interp_equation (altarica_tree *t, ar_sgs *sgs, ccl_config_table *conf,
		   ar_semantics_type semtype)
{
  int ok = 1;
  ar_identifier *name = NULL;
  altarica_tree *ptName = t->child;

  ccl_try (altarica_interpretation_exception)
    {
      int is_state = 0;
      altarica_tree *ptForm = ptName->next;
      uint32_t t0 = 0;

      name = ar_interp_symbol_as_identifier (ptName);
      if (TIMERS_ARE_ENABLED ())
	t0 = ccl_time_get_cpu_time_in_milliseconds ();

      if (ccl_debug_is_on)
	{
	  ccl_debug ("compute property '");
	  ar_identifier_log (CCL_LOG_DEBUG, name);
	  ccl_debug ("'\n");
	}

      if (ar_sgs_has_set (sgs, name, &is_state))
	{
	  ccl_error ("%s:%d: error: %s property redefinition '%s'.\n",
		     t->filename, t->line, is_state?"state":"transition",
		     ptName->value.id_value);
	  ccl_throw_no_msg (acheck_interpretation_exception);
	}

      if (IS_LABELLED_BY (t, EQ_DEF))
	{	  
	  ar_sgs_set *set;

	  if (IS_LABELLED_BY (ptForm, CTLSTAR_SPEC))
	    {
	      if (semtype == AR_SEMANTICS_RELATIONS)
		{
		  is_state = 1;
		  set = s_interp_ctlstar (ptForm, sgs, conf);
		}
	      else
		{
		  set = NULL;
		  ccl_error ("%s:%d: error: CTL* formulas are not supported "
			     "for exhaustive representation.\n",
			     t->filename, t->line);
		  ccl_throw_no_msg (acheck_interpretation_exception);
		}
	    }
	  else
	    {
	      set =
		s_interp_state_or_transition_formula(ptForm, sgs, &is_state);
	    }

	  if (is_state)
	    ar_sgs_add_state_set (sgs, name, set);
	  else
	    ar_sgs_add_trans_set (sgs, name, set);
	  ar_sgs_set_del_reference (set);
	}
      else 
	{
	  s_interp_fixpoint (ptForm, IS_LABELLED_BY(t,EQ_LFP), sgs, name);
	}

      if (TIMERS_ARE_ENABLED ())
	{
	  uint32_t elapsed = ccl_time_get_cpu_time_in_milliseconds () - t0;

	  ar_identifier_log (CCL_LOG_DISPLAY, name);
	  ccl_display (" computed within ");

	  if (elapsed == 0)
	    ccl_display ("a time < 1 ms\n");
	  else
	    {
	      ccl_time_log (CCL_LOG_DISPLAY, elapsed);
	      ccl_display ("\n");
	    }
	}

      ar_identifier_del_reference (name);
    }
  ccl_catch
    {
      ok = 0;
      if(name != NULL)
	ar_identifier_del_reference (name);
    }
  ccl_end_try;

  return ok;
}

			/* --------------- */

static void
s_display_example (ar_sgs *sgs, ar_sgs_set *S, const char *comment)
{
  ar_sgs_set *ex = ar_sgs_compute_pick (sgs, S);
  ccl_display (comment);
  ar_sgs_display_set (CCL_LOG_DISPLAY, sgs, ex, AR_SGS_DISPLAY_ASIS);
  ar_sgs_set_del_reference (ex);
}

static void
s_test (altarica_tree *t, ar_sgs *sgs, ccl_config_table *conf)
{
  int is_state;
  int test_ok;
  ar_sgs_set *to_test = 
    s_interp_state_or_transition_formula (t->child, sgs, &is_state);
  int counter_ex = IS_LABELLED_BY (t->child->next, TRUE);
  double card = ar_sgs_get_set_cardinality (sgs, to_test);
  size_t expected_value = 0;
  char *testdesc = NULL;
  
  ccl_log_redirect_to_string (CCL_LOG_USR1, &testdesc);

  ccl_log (CCL_LOG_USR1, "TEST(");
  ar_tree_log_acheck_expr (CCL_LOG_USR1, t->child, AR_NO_ID_TRANSLATION);

  if (t->child->next->next != NULL)
    {
      expected_value = t->child->next->next->value.int_value;
      test_ok = (card == (double) expected_value);
      ccl_log (CCL_LOG_USR1, ", %u", expected_value);
    }
  else
    {
      test_ok = (card > 0.0);
      ccl_log (CCL_LOG_USR1, " is not empty");
    }
  ccl_log (CCL_LOG_USR1, ")");
  ccl_log_pop_redirection (CCL_LOG_USR1);
  
  ccl_display (TEST_DESC_FMT "\t[%s]",
	       testdesc, (test_ok?"PASSED":"FAILED"));
  ccl_string_delete (testdesc);

  if (!test_ok)
    {
      if (t->child->next->next != NULL)
	{
	  ccl_display (" (!= %g)", card);
	  if (counter_ex && expected_value == 0)
	    s_display_example (sgs, to_test, "\nA counter-example is:\n ");
	}
      else
	ccl_display (" (empty)");
    }
  else if (t->child->next->next == NULL && counter_ex)
    s_display_example (sgs, to_test, "\nA witness is:\n ");
  ccl_display ("\n");
  if (t->value.int_value && !test_ok && NRTEST_FAILURE_ABORTS ())
    exit (EXIT_FAILURE);
  ar_sgs_set_del_reference (to_test);
}

			/* --------------- */

static void
s_events (altarica_tree *t, ar_sgs *sgs, const char *nodename,
	  ccl_config_table *conf)
{
  ar_sgs_set *T = s_interp_transition_formula (t->child, sgs);
  ccl_hash *events = ar_sgs_set_get_events (sgs, T);
  ccl_pointer_iterator *pi = ccl_hash_get_keys (events);

  while (ccl_iterator_has_more_elements (pi))
    {
      ar_ca_event *e = ccl_iterator_next_element (pi);
      ar_ca_event_log (CCL_LOG_DISPLAY, e);
      ccl_display ("\n");
    }
  ccl_iterator_delete (pi);
  ar_sgs_set_del_reference (T);
  ccl_hash_delete (events);
}

			/* --------------- */


static char *
s_nodename_replace (const char *s, const char *nodename)
{
  char *result = NULL;
  const char *NODENAME = "$NODENAME";
  int len = strlen (NODENAME);

  while (*s)
    {
      if (strncmp (s, NODENAME, len) == 0) 
	{
	  ccl_string_format_append (&result, "%s", nodename);
	  s += len;
	}
      else
	{
	  ccl_string_format_append (&result, "%c", *s);
	  s++;
	}
    }

  return result;
}

			/* --------------- */


static FILE *
s_interp_open_file(altarica_tree *t, const char *mode, const char *nodename, 
		   int inpath)
{
  FILE *result = NULL;
  
  if(t != NULL)
    {
      char *tmp = s_nodename_replace (t->value.id_value, nodename);
      char *filename = inpath ? arc_shell_find_file_in_path (tmp) : NULL;

      if (filename == NULL)
	filename = tmp;
      else
	ccl_string_delete (tmp);
	
      result = fopen (filename, mode);
     
      if (result == NULL)
	{
	  ccl_error ("%s:%d: error: can't open file '%s'.\n", t->filename,
		     t->line, filename);
	  ccl_string_delete (filename);
	  ccl_throw_no_msg (acheck_interpretation_exception);
	}
      ccl_string_delete (filename);
    }

  return result;
}

			/* --------------- */

static void
s_nrtest (altarica_tree *t, ar_sgs *sgs, const char *nodename,
	  ccl_config_table *conf)
{
  int line = 1;
  char buf[1000];
  int check_ok = 1;
  FILE *input = s_interp_open_file (t->child, "r", nodename, 1);
  
  while (! feof (input))
    {
      char *sp;

      fgets (buf, sizeof (buf), input);
      if (*buf == '#')
	{
	  line++;
	  continue;
	}
      sp = strrchr (buf, ' ');
      if (sp != NULL)
	{
	  int is_state;
	  ar_sgs_set *set;
	  ar_identifier *propid;
	  double expected_card = atof (sp + 1);
	  *sp = '\0';
	  propid = ar_identifier_create (buf);
	  if (ar_sgs_has_set (sgs, propid, &is_state))
	    {
	      double card;

	      if (is_state)
		set = ar_sgs_get_state_set (sgs, propid);
	      else 
		set = ar_sgs_get_trans_set (sgs, propid);
	      card = ar_sgs_get_set_cardinality (sgs, set);

	      if (card != expected_card)
		{
		  ccl_error ("%s:%d: error: regression test failure : "
			     "difference of cardinality for property '%s' "
			     "on node '%s'.\n", 
			     t->filename, t->line, buf, nodename);
		  ccl_error ("%s:%d: error: expected=%g computed=%g.\n", 
			     t->filename, t->line, expected_card, card);
		  check_ok = 0;
		}
	      ar_sgs_set_del_reference (set);
	    }
	  else
	    {
	      ccl_error ("%s:%d: error: regression test failure : "
			 "missing property '%s' on node '%s'.\n",
			 t->filename, t->line, buf, nodename);
	      check_ok = 0;
	    }
	  ar_identifier_del_reference (propid);
	}
      line++;
    }
  fclose (input);
  if (! check_ok && NRTEST_FAILURE_ABORTS ())
    exit (EXIT_FAILURE);
}

			/* --------------- */

static void
s_show (altarica_tree *t, ar_sgs *sgs)
{
  int i;
  int nb_state_props = 0;
  ccl_list *state_properties = ccl_list_create ();
  int nb_trans_props = 0;
  ccl_list *trans_properties = ccl_list_create ();

  t = t->child;

  ccl_pre (IS_LABELLED_BY (t, ID_LIST));

  ccl_display ("/*\n");

  {
    ar_identifier *name = ar_sgs_get_name (sgs);
    ccl_display (" * Properties for node : ");
    ar_identifier_log (CCL_LOG_DISPLAY, name);
    ccl_display ("\n");
    ar_identifier_del_reference (name);
  }

  for (t = t->child; t; t = t->next)
    {
      ccl_assert (IS_LABELLED_BY (t,IDENTIFIER));

      if (ccl_string_equals (t->value.id_value, "all"))
	{
	  for(i = 0; i < 2; i++)
	    {
	      int *pc;
	      ccl_list *props;
	      ccl_list *tmp;
	      ar_identifier_iterator *ii;
	      ar_sgs_set *(*get_mark)(ar_sgs *, ar_identifier *);

	      if (i == 0)
		{
		  pc = &nb_state_props;
		  props = state_properties ;
		  ii = ar_sgs_get_state_sets (sgs);
		  get_mark = ar_sgs_get_state_set;
		}
	      else
		{
		  pc = &nb_trans_props;
		  props = trans_properties;
		  ii = ar_sgs_get_trans_sets (sgs);
		  get_mark = ar_sgs_get_trans_set;
		}
	      
	      tmp = ccl_list_create_from_iterator ((ccl_pointer_iterator *) ii);
	      ccl_iterator_delete(ii);
	      ccl_list_sort (tmp, 
			     (ccl_compare_func *) ar_identifier_lex_compare);
	      while (! ccl_list_is_empty (tmp))
		{
		  ar_identifier *id = ccl_list_take_first (tmp);
		  ar_sgs_set *set = get_mark (sgs, id);

		  ccl_list_add (props, id);
		  ccl_list_add (props, set);
		  (*pc)++;
		}
	      ccl_list_delete (tmp);
	    }
	}     
      else
	{
	  ccl_try (acheck_interpretation_exception)
	    {
	      int is_state = 0;
	      ar_sgs_set *set =
		s_interp_state_or_transition_formula (t, sgs, &is_state);
	      ccl_list *props = is_state ? state_properties : trans_properties;
	      
	      if (is_state) 
		nb_state_props++;
	      else 
		nb_trans_props++;

	      ccl_list_add (props, NULL);
	      ccl_list_add (props, t);
	      ccl_list_add (props, set);
	    }
	  ccl_no_catch;
	}
    }

  for (i = 0; i < 2; i++)
    {
      ccl_list *props = (i == 0) ? state_properties : trans_properties;
      int props_size = (i == 0) ? nb_state_props : nb_trans_props;

      ccl_display (" * # %s propert%s : %d\n"
		   " *\n",
		   (i == 0) ? "state" : "trans",
		   props_size ? "ies" : "y",
		   props_size);
      
      while (!ccl_list_is_empty (props))
	{
	  ar_sgs_set *prop = NULL;
	  void *p1 = ccl_list_take_first (props);

	  ccl_display (" * ");
	  if (p1 == NULL)
	    {
	      altarica_tree *id = (altarica_tree *) ccl_list_take_first (props);
	      ccl_display ("%s", id->value.id_value);
	    }
	  else
	    {
	      ar_identifier *propname = (ar_identifier *) p1;
	      ar_identifier_log (CCL_LOG_DISPLAY, propname);
	      ar_identifier_del_reference (propname);
	    }
	    
	  prop = (ar_sgs_set *) ccl_list_take_first (props);
	  
	  ccl_display(" = %g\n", ar_sgs_get_set_cardinality(sgs, prop));
	  ar_sgs_set_del_reference (prop);
	}

      if (i == 0)
	ccl_display (" *\n");
      ccl_list_delete (props);
    }

  ccl_display (" */\n");
}

			/* --------------- */

static int
s_dottrace (altarica_tree *t, ar_sgs *sgs, ccl_config_table *conf)
{
  int result = 1;
  ar_sgs_set *initial = NULL;
  ar_sgs_set *trans = NULL;
  ar_sgs_set *final = NULL;

  ccl_pre (IS_LABELLED_BY (t, DOTTRACE));

  ccl_try (acheck_interpretation_exception)
    {
      initial = s_interp_eval_state_sgs_formula (t->child, sgs);
      trans = s_interp_eval_transition_sgs_formula (t->child->next, sgs);
      final = s_interp_eval_state_sgs_formula (t->child->next->next, sgs);
      ar_sgs_dot_trace (CCL_LOG_DISPLAY, sgs, initial, trans, final, conf);
    }
  ccl_catch    
    {
      result = 0;
    }
  ccl_end_try;

  ccl_zdelete (ar_sgs_set_del_reference, initial);
  ccl_zdelete (ar_sgs_set_del_reference, trans);
  ccl_zdelete (ar_sgs_set_del_reference, final);

  return result;
}

			/* --------------- */

typedef void 
(*sgs_translator)(ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
		  ccl_config_table *conf);

static int 
s_write(altarica_tree *t, ar_sgs *sgs, sgs_translator translator,
	ccl_config_table *conf)
{
  int result = 1;
  ar_sgs_set *S = s_interp_eval_state_sgs_formula (t->child, sgs);

  ccl_try (acheck_interpretation_exception)
    {
      ar_sgs_set *T = 
	s_interp_eval_transition_sgs_formula (t->child->next, sgs);
      translator (CCL_LOG_DISPLAY, sgs, S, T, conf);
      ar_sgs_set_del_reference (T);
    }
  ccl_catch
    {
      result = 0;
    }
  ccl_end_try;

  ar_sgs_set_del_reference(S);

  return result;
}

			/* --------------- */

static ar_identifier *
s_interp_project_tgt (altarica_tree *t, const char *nodename)
{
  char *tgtname;
  ar_identifier *result;

  ccl_pre (IS_LABELLED_BY (t, IDENTIFIER));

  tgtname = s_nodename_replace (t->value.id_value, nodename);
  result = ar_identifier_create (tgtname);
  ccl_string_delete (tgtname);

  return result;
}

			/* --------------- */

static ar_node *
s_get_subnode (altarica_tree *t, ar_sgs *sgs, ar_identifier **psubid)
{
  ar_node *result = NULL;

  if (t != NULL)
    {
      ar_identifier *id = ar_interp_identifier_path (t, AR_MODEL_CONTEXT);
      ar_identifier *sgs_name = ar_sgs_get_name (sgs);
      ar_node *n = ar_model_get_node (sgs_name);  
      result = ar_node_get_subnode_model (n, id);

      ar_identifier_del_reference (sgs_name);
      ar_node_del_reference (n);

      if (result == NULL)
	{
	  ccl_error ("%s:%d: error: undefined subnode '", t->filename, t->line);
	  ar_identifier_log (CCL_LOG_ERROR, id);
	  ccl_error ("'.\n");
	  ar_identifier_del_reference (id);
	  ccl_throw_no_msg (acheck_interpretation_exception);
	}
      else
	{
	  *psubid = id;
	}
    }


  return result;
}

			/* --------------- */

static void
s_project (altarica_tree *t, ar_sgs *sgs, const char *nodename)
{	
  altarica_tree *t_S = t->child;
  ar_sgs_set *S = NULL;
  altarica_tree *t_TE = t_S->next;
  ar_sgs_set *TE = NULL;
  int has_TA = !(IS_LABELLED_BY (t_TE->next->next, TRUE) ||
		 IS_LABELLED_BY (t_TE->next->next, FALSE));
  altarica_tree *t_TA = has_TA ? t_TE->next: NULL;
  ar_sgs_set *TA = NULL;
  altarica_tree *t_tgt = has_TA ? t_TA->next : t_TE->next;
  ar_identifier *tgt = NULL;
  altarica_tree *t_sim = t_tgt->next;
  int simplify = IS_LABELLED_BY (t_sim, TRUE);
  altarica_tree *subt = t_sim->next;
  ar_identifier *subid = NULL;
  ar_node *subnode = NULL;

  ccl_try (acheck_interpretation_exception)
    {
      S = s_interp_eval_state_sgs_formula (t_S, sgs);
      TE = s_interp_eval_transition_sgs_formula (t_TE, sgs);
      if (t_TA != NULL)
	TA = s_interp_eval_transition_sgs_formula (t_TA, sgs);
      tgt = s_interp_project_tgt (t_tgt, nodename);
      subnode = s_get_subnode (subt, sgs, &subid);

      ar_sgs_compute_project (sgs, CCL_LOG_DISPLAY, subid, subnode, tgt, S, TE,
			      TA, simplify);
    }
  ccl_no_catch;

  ccl_zdelete (ar_sgs_set_del_reference, S);
  ccl_zdelete (ar_sgs_set_del_reference, TE);  
  ccl_zdelete (ar_sgs_set_del_reference, TA);  
  ccl_zdelete (ar_node_del_reference, subnode);  
  ccl_zdelete (ar_identifier_del_reference, tgt);  
  ccl_zdelete (ar_identifier_del_reference, subid);
}

			/* --------------- */

static void
s_display (altarica_tree *t, ar_sgs *sgs, const char *nodename,
	   ccl_config_table *conf)
{
  t = t->child;

  ccl_pre (IS_LABELLED_BY (t, ID_LIST));

  for (t = t->child; t; t = t->next)
    {
      ccl_assert (IS_LABELLED_BY (t,IDENTIFIER));

      ccl_try (acheck_interpretation_exception)
        {
	  int is_state;
	  ar_sgs_set *set =
	    s_interp_state_or_transition_formula (t, sgs, &is_state);
	  ccl_display ("set '%s' contains\n", t->value.id_value);
	  ar_sgs_display_set (CCL_LOG_DISPLAY, sgs, set, AR_SGS_DISPLAY_ASIS);
	  ar_sgs_set_del_reference (set);
	  ccl_display ("\n");
	}
      ccl_no_catch;
    }
}

			/* --------------- */

static void
s_remove (altarica_tree *t, ar_sgs *sgs, const char *nodename,
	  ccl_config_table *conf)
{
  ccl_list *to_remove;

  t = t->child;

  ccl_pre (IS_LABELLED_BY (t, ID_LIST));

  to_remove = ar_interp_id_list (t, 0);
  while (! ccl_list_is_empty (to_remove))
    {
      int is_state = 0;
      ar_identifier *setid = ccl_list_take_first (to_remove);

      if (ar_sgs_has_set (sgs, setid, &is_state))
	{
	  if (is_state)
	    ar_sgs_remove_state_set (sgs, setid);
	  else
	    ar_sgs_remove_trans_set (sgs, setid);
	}
      else
	{
	  ccl_warning ("%s:%d: warning: unknown property '", 
		       t->filename, t->line);
	  ar_identifier_log (CCL_LOG_WARNING, setid);
	  ccl_warning ("'.\n");
	}	  
      ar_identifier_del_reference (setid);
    }
  ccl_list_delete (to_remove);
}

			/* --------------- */

static void
s_validate (altarica_tree *t, ar_sgs *sgs, const char *nodename,
	    ccl_config_table *conf)
{
  ar_ca *ca = ar_sgs_get_constraint_automaton (sgs);

  ccl_try (domain_cardinality_exception)
    {
      ar_validate_ca (CCL_LOG_DISPLAY, ca);
    }
  ccl_no_catch;
  ar_ca_del_reference (ca);
}

static int
s_interp_command (altarica_tree *t, ar_sgs *sgs, const char *nodename,
		  ccl_config_table *conf)
{
  int ok = 1;

  switch (t->node_type) 
    {
    case AR_TREE_TEST : s_test (t, sgs, conf); break;
    case AR_TREE_NRTEST : s_nrtest (t, sgs, nodename, conf); break;
    case AR_TREE_SHOW : s_show (t, sgs); break;
    case AR_TREE_WTS : ok = s_write (t, sgs, ar_sgs_to_mec4, conf); break;
    case AR_TREE_DOT : ok = s_write (t, sgs, ar_sgs_to_dot, conf); break;
    case AR_TREE_DOTTRACE : ok = s_dottrace (t, sgs, conf); break;
    case AR_TREE_GML : ok = s_write (t, sgs, ar_sgs_to_gml, conf); break;
    case AR_TREE_QUOT : ar_sgs_to_quot (CCL_LOG_DISPLAY, sgs, conf); break;
    case AR_TREE_MODES : 
      ar_sgs_to_modes_automaton (CCL_LOG_DISPLAY, sgs, conf); break;
    case AR_TREE_VALIDATE : s_validate (t, sgs, nodename, conf); break;
    case AR_TREE_PROJECT : s_project (t, sgs, nodename); break;
    case AR_TREE_DISPLAY : s_display (t, sgs, nodename, conf); break;
    case AR_TREE_EVENTS : s_events (t, sgs, nodename, conf); break;
    case AR_TREE_REMOVE : s_remove (t, sgs, nodename, conf); break;
    case AR_TREE_CTL2MU : s_ctl_translation (t, sgs, nodename, conf); break;
  };

  return ok;
}

			/* --------------- */

static void
s_log_proc_for_FILE (ccl_log_type type, const char  *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    {
      FILE *stream = (FILE *) data;
      fprintf (stream, "%s", msg);
      fflush (stream);
    }
}

			/* --------------- */

static int
s_interp_equation_or_command (altarica_tree *t, ar_sgs *sgs, 
			      const char *nodename, ccl_config_table *conf,
			      ar_semantics_type semtype)
{
  int ok = 0;
  FILE *out = NULL;

  ccl_try (acheck_interpretation_exception)
    {
      switch (t->node_type) 
	{
	case AR_TREE_CMD : 
	case AR_TREE_CRT_CMD : 
	case AR_TREE_APPEND_CMD : 
	  if (IS_LABELLED_BY (t, CRT_CMD))
	    out = s_interp_open_file (t->child->next, "w", nodename, 0);
	  else if( IS_LABELLED_BY(t,APPEND_CMD) )
	    out = s_interp_open_file (t->child->next, "a", nodename, 0);

	  if (out != NULL)
	    ccl_log_push_redirection (CCL_LOG_DISPLAY, 
				      s_log_proc_for_FILE, out);
	  ok = s_interp_command (t->child, sgs, nodename, conf);
	  break;

	default:
	  ok = s_interp_equation (t, sgs, conf, semtype);
	  break;
	}
    }
  ccl_no_catch;

  if (out != NULL)
    {
      ccl_log_pop_redirection (CCL_LOG_DISPLAY);
      fflush (out);
      fclose (out);
    }

  return ok;
}

			/* --------------- */

static ar_sgs *
s_get_sgs (altarica_tree *t, ar_semantics_type semtype)
{
  ar_sgs *result = NULL;
  ar_identifier *id = ar_interp_symbol_as_identifier (t);
  ar_node *node = ar_model_get_node (id);

  if (node == NULL)
    ccl_error ("%s:%d error: undefined node '%s'.\n",t->filename,t->line,
	       t->value.id_value);
  else
    {
      ccl_try (exception)
        {
	  result = ar_semantics_get (node, semtype);
	}
      ccl_catch
	{
	  if (ccl_exception_is_raised (domain_cardinality_exception))
	    {
	      int max_card;
	      if (semtype == AR_SEMANTICS_TRANSITION_SYSTEM)
		max_card = AR_TS_MAX_STATE_CARD;
	      else
		max_card = AR_DD_MAX_ARITY;
	      ccl_error ("Can't compute semantics. Some variables have a "
			 "domain with a cardinality exceeding %d.\n", 
			 max_card); 
	    }
	}
      ccl_end_try;
      ar_node_del_reference(node);
    }
  ar_identifier_del_reference(id);

  return result;
}

			/* --------------- */

int
ar_interp_acheck_tree(altarica_tree *t, ccl_config_table *conf)
{  
  int error = 0;

  for(; t != NULL; t = t->next)
    {      
      altarica_tree *id;
      altarica_tree *stmt;
      altarica_tree *first_id = t->child;
      ar_semantics_type semtype;

      ccl_assert (IS_LABELLED_BY (t,WITH));
      ccl_assert (IS_LABELLED_BY (t->child,SYMBOLICALLY) || 
		  IS_LABELLED_BY (t->child,EXHAUSTIVELY));

      if (t->child->node_type == AR_TREE_SYMBOLICALLY)
	semtype = AR_SEMANTICS_RELATIONS;
      else
	semtype = AR_SEMANTICS_TRANSITION_SYSTEM;

      first_id = t->child->next;
      for(id = first_id->child; id; id = id->next)
	{
	  ar_sgs *sgs = s_get_sgs (id, semtype);

	  if (sgs != NULL)
	    {
	      ar_identifier *name = ar_interp_symbol_as_identifier (id);
	      ar_node *node = ar_model_get_node (name);	

	      for(stmt = first_id->next; stmt && !error; stmt = stmt->next)
		error = ! s_interp_equation_or_command (stmt, sgs, 
							id->value.id_value,
							conf, semtype);

	      ar_sgs_del_reference (sgs);
	      ar_identifier_del_reference(name);
	      if (MUST_CLEANUP_SEMANTICS ())
		{
		  if(ccl_debug_is_on)
		    ccl_debug ("cleaning up the computed semantics\n");
		  ar_semantics_cleanup (node, semtype);
		}
	      ar_node_del_reference (node);
	    }
	  else
	    error = 1;
	} 
    }

  return ! error;
}

