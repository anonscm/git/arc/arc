/*
 * arc-debug.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_DEBUG_H
# define ARC_DEBUG_H

# include <ccl/ccl-config-table.h>

extern const char *arc_debug_preferences_prefix;
extern const char *arc_debug_default_dot_filename;
extern const char *arc_debug_default_dot_attributes;
extern int arc_debug_max_log_level;

extern int
arc_debug_init (ccl_config_table *conf);

extern void
arc_debug_terminate (void);

extern void
arc_debug_update_integer (const char *id, void *addr, ccl_config_table *conf);

extern void
arc_debug_update_string (const char *id, void *addr, ccl_config_table *conf);

extern void
arc_debug_update_boolean (const char *id, void *addr, ccl_config_table *conf);

extern void
arc_debug_register_flag (const char *id, void *addr, 
			 void (*update)(const char *id,
					void *pflag, ccl_config_table *conf));

#define arc_debug_register_integer_flag(id_, addr_)			\
  arc_debug_register_flag (id_, addr_, arc_debug_update_integer)

#define arc_debug_register_string_flag(id_, addr_)		\
  arc_debug_register_flag (id_, addr_, arc_debug_update_string)

#define arc_debug_register_boolean_flag(id_, addr_)			\
  arc_debug_register_flag (id_, addr_, arc_debug_update_boolean)
			 
extern void
arc_debug_update_flags (ccl_config_table *conf);

#endif /* ! ARC_DEBUG_H */
