/*
 * arc-readline.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef ARC_READLINE_H
# define ARC_READLINE_H

extern void
arc_readline_init (const char *history, int allow_rl_lib);

extern void
arc_readline_terminate (void);

extern char *
arc_readline_get_line (int allow_rl, const char *prompt);

extern char *
arc_readline_get_line_no_rl (const char *prompt);

#endif /* ! ARC_READLINE_H */
