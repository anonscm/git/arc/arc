/*
 * ar-rel-semantics.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_REL_SEMANTICS_H
# define AR_REL_SEMANTICS_H

# include "ar-dd.h"
# include <ccl/ccl-exception.h>
# include <ccl/ccl-hash.h>
# include <ccl/ccl-config-table.h>
# include "ar-constraint-automaton.h"
# include "ar-node.h"
# include "ar-state-graph-semantics.h"
# include "mec5/mec5-relations.h"

typedef struct ar_relational_semantics_st ar_relsem;

extern int
ar_relsem_init (ccl_config_table *conf);

extern void
ar_relsem_terminate (void);

			/* --------------- */

extern int ar_relsem_debug_is_on;
extern int ar_relsem_debug_log_order;
extern int ar_relsem_debug_comp_initial;
extern int ar_relsem_debug_comp_post;
extern int ar_relsem_debug_comp_transrel; 
extern int ar_relsem_debug_comp_reach;

extern ar_relsem *
ar_compute_relational_semantics(ar_ca *ca, 
				void (*order_variables) (ar_ca *ca,
							 varorder *order,
							 void *clientdata),
				void *clientdata)
  CCL_THROW(domain_cardinality_exception);

extern ar_relsem *
ar_relsem_add_reference(ar_relsem *rs);

extern void
ar_relsem_del_reference(ar_relsem *rs);

extern void
ar_relsem_log_info(ccl_log_type log, ar_relsem *rs);

			/* --------------- */

extern ar_ca *
ar_relsem_get_automaton (ar_relsem *rs);

extern ar_ca_exprman *
ar_relsem_get_expression_manager (ar_relsem *rs);

extern ar_ddm 
ar_relsem_get_decision_diagrams_manager (ar_relsem *rs);

extern const varorder *
ar_relsem_get_order (ar_relsem *rs);

			/* --------------- */

extern int
ar_relsem_get_var_dd_index (ar_relsem *rs, ar_identifier *id);

extern int
ar_relsem_get_primed_var_dd_index (ar_relsem *rs, ar_identifier *id);

extern int
ar_relsem_get_primed_var_dd_index_from_var (ar_relsem *rs, ar_ca_expr *var);

extern int
ar_relsem_get_primed_dd_index (ar_relsem *rs, int dd_index);

extern int
ar_relsem_get_event_var_dd_index (ar_relsem *rs, ar_identifier *id);

extern ar_dd
ar_relsem_get_initial (ar_relsem *rs);

extern ar_dd
ar_relsem_get_assertion (ar_relsem *rs);

extern ar_dd *
ar_relsem_get_assertions (ar_relsem *rs, int *p_nb_assertions);

extern ar_dd
ar_relsem_get_reachables (ar_relsem *rs);

extern ar_dd_array
ar_relsem_get_valid_moves (ar_relsem *rs, int precond, int postcond);

extern ar_dd_array
ar_relsem_get_no_self_epsilon_moves (ar_relsem *rs, int precond, int postcond);

extern ar_dd 
ar_relsem_get_valid_states (ar_relsem *rs);

extern ar_dd 
ar_relsem_get_transition_relation (ar_relsem *rs, int precond, int postcond,
				   ar_ca_trans *t);

extern ar_dd 
ar_relsem_unlabelled_transition_relation (ar_relsem *rs, 
					  int precond, int postcond,
					  ar_ca_expr *guard, 
					  int nb_assignments,
					  ar_ca_expr **assignments);

extern ar_dd_array  
ar_relsem_get_transition_relations (ar_relsem *rs, int precond, int postcond);

extern ar_dd_array
ar_relsem_get_event_relations (ar_relsem *rs, int precond, int postcond);

extern double
ar_relsem_get_cardinality (ar_relsem *rs, ar_dd dd, int all);

extern ar_ca_expr *
ar_relsem_dd_to_expr (ar_relsem *rs, ar_dd dd);

extern void
ar_relsem_add_eq_representative (ar_relsem *rs, ar_ca_expr **e, 
				 ar_ca_expr *var);

extern ar_dd 
ar_relsem_compute_post (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I);

extern ar_dd 
ar_relsem_compute_pre (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I);

extern ar_dd 
ar_relsem_compute_post_by_T (ar_relsem *rs, ar_dd S, ar_dd T, 
			     int nb_I, ar_dd *I);

extern ar_dd 
ar_relsem_compute_pre_by_T (ar_relsem *rs, ar_dd S, ar_dd T, 
			    int nb_I, ar_dd *I);

extern ar_sgs_set *
ar_relsem_create_state_set (ar_relsem *rs, ar_dd S);

extern ar_dd 
ar_relsem_compute_simple_set (ar_relsem *rs, ar_ca_expr *e);

extern ar_dd 
ar_relsem_lazy_post_by_ith_transition (ar_relsem *rs, int i, ar_dd S);

extern ar_dd 
ar_relsem_lazy_post_by_transition (ar_relsem *rs, ar_ca_trans *t, ar_dd S);

extern ar_dd 
ar_relsem_lazy_pre_by_transition (ar_relsem *rs, ar_ca_trans *t, ar_dd S);

extern ar_dd 
ar_relsem_lazy_post (ar_relsem *rs, ar_dd S);

extern ar_dd 
ar_relsem_lazy_pre (ar_relsem *rs, ar_dd S);

extern ar_dd 
ar_relsem_lazy_reachables (ar_relsem *rs);

extern ar_dd 
ar_relsem_join (ar_relsem *rs, ar_dd T1, ar_dd T2, int nbI, ar_dd *I);

extern void
ar_relsem_log_variables_of_dd (ccl_log_type log, ar_relsem *rs, ar_dd R);

extern ccl_hash *
ar_relsem_variables_of_dd (ar_relsem *rs, ar_dd R);

extern ar_dd 
ar_relsem_make_it_prime (ar_relsem *rs, ar_dd R);

extern ar_dd 
ar_relsem_pick (ar_relsem *rs, ar_dd S);

extern ar_sgs_set *
ar_relsem_translate_configuration_relation (ar_relsem *rs,
					    ar_mec5_relation *mrel);

extern ar_sgs_set *
ar_relsem_translate_transition_relation (ar_relsem *rs, ar_mec5_relation *mrel);

#endif /* ! AR_REL_SEMANTICS_H */
