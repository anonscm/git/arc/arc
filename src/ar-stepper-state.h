/*
 * ar-stepper-state.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_STEPPER_STATE_H
# define AR_STEPPER_STATE_H

# include <ccl/ccl-array.h>
# include "varorder.h"
# include "ar-ca-expression.h"
# include "ar-backtracking-stack.h"

typedef struct ar_stepper_state_st ar_stepper_state;
typedef CCL_ARRAY(ar_stepper_state *) ar_stepper_state_array;

extern ar_stepper_state *
ar_stepper_state_create(int width);

extern void
ar_stepper_state_delete(ar_stepper_state *state);

extern uint32_t
ar_stepper_state_hash(ar_stepper_state *state);

extern int
ar_stepper_state_equal(const ar_stepper_state *s1, const ar_stepper_state *s2);

extern int
ar_stepper_state_compare (const ar_stepper_state *s1, 
			  const ar_stepper_state *s2);

extern int
ar_stepper_state_get_width(ar_stepper_state *state);

extern void
ar_stepper_state_set_range(ar_stepper_state *state, 
			   int index, int min, int max);

extern int
ar_stepper_state_set_values(ar_stepper_state *state, const varorder *vo, 
			    ar_ca_expr **variables, int nb_variables);

extern void
ar_stepper_state_set_variables(ar_stepper_state *state, const varorder *vo, 
			       ar_ca_expr **variables, int nb_variables);

extern void
ar_stepper_state_copy(ar_stepper_state *dst, ar_stepper_state *src);

extern ar_stepper_state *
ar_stepper_state_dup(ar_stepper_state *state);

extern void
ar_stepper_state_log(ccl_log_type log, ar_stepper_state *state);

extern void
ar_stepper_state_get_range(ar_stepper_state *state, int i, int *min, int *max);

extern ar_bounds *
ar_stepper_state_get_bounds_address(ar_stepper_state *state, int i);

extern void
ar_stepper_state_save(ar_stepper_state *state,  ar_backtracking_stack *bstack);

# define ar_stepper_state_expand_state(state, indom, data) \
  ar_stepper_state_expand_state_with_limit(state,-1, indom, data)

extern void
ar_stepper_state_expand_state_with_limit(ar_stepper_state *state, 
					 ar_stepper_state_array *set,
					 int *pset_card,
					 int max,
					 int (*in_domain)(int varindex,
							  int val, void *data),
					 void *data);

/*
extern void
ar_stepper_state_get_digest (ar_stepper_state *state, ccl_buffer *buf);
*/

#define ar_stepper_state_array_clean(a)				\
  do {								\
    int i = 0;							\
    for (i = 0; i < (a).size; i++)				\
      ccl_zdelete (ar_stepper_state_delete, (a).data[i]);	\
    ccl_array_delete (a);					\
  } while (0)

#endif /* ! AR_STEPPER_STATE_H */
