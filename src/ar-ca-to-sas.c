/*
 * ar-ca-to-sas.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-graph.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-heap.h>

#if HAVE_PTHREAD
# include <pthread.h>
# include <signal.h>
#endif

#include "sas/sas-p.h"
#include "ar-ca-to-sas.h"

typedef struct ca_sas_model ca_sas_model;
typedef struct ca_sas_action ca_sas_action;
typedef struct ca_sas_trans ca_sas_trans;
typedef struct ca_sas_observer ca_sas_observer;
typedef struct ca_sas_simstate ca_sas_simstate;
typedef struct ca_sas_exprnode ca_sas_exprnode;
typedef struct ca_sas_propagation_set ca_sas_propagation_set;
typedef int ca_sas_value;
typedef void *ca_sas_bytecode;

#define BYTECODE_MAX_SIZE 5

#define valueof(S_, node_) (((S_)->values[(node_)->offset]).value)
#define timestampof(S_, node_) (((S_)->values[(node_)->offset]).timestamp)

struct ca_sas_propagation_set
{
  ccl_heap *entries;
  int *next;
  int *layers;
};

struct ca_sas_exprnode
{
  int layer;
  int nb_related_actions;
  int *related_actions;
  ar_ca_expr *e;
  ca_sas_exprnode *fun;
  ca_sas_exprnode *args[3];
  ca_sas_exprnode **preds;
  intptr_t offset; 
  ccl_vertex *vertex; 
};

typedef struct timed_value
{
  int inqueue;
  uint32_t timestamp;
  ca_sas_value value;
} timed_value;

struct ca_sas_simstate
{
  uint32_t timestamp;  
  ccl_bittable *touched_trans;
  ccl_heap *touched_indexes;
  int nb_touched;
  ca_sas_propagation_set *propagation_set;
  timed_value *values;
  ca_sas_bytecode *bytecodes;
};
  
struct ca_sas_trans
{
  ar_ca_trans *T;
  sas_law_param *proba;
  ca_sas_exprnode **assignment;
};

struct ca_sas_action
{
  int index;
  int is_self_dependent;
  ca_sas_exprnode *guard;
  int guard_offset;
  
  sas_law *law;
  int has_memory;
  int priority;  
  char *description;
  ccl_list *depobs;

  int nb_trans;
  ca_sas_trans trans[1];
};

struct ca_sas_observer
{
  int index;
  char *description;
  ca_sas_exprnode *expr;
  int expr_offset;
};

struct ca_sas_model
{
  sas_model super;
  ar_ca *ca;
  char *errmsg;
  
  int nb_actions;
  ca_sas_action **actions;
  
  int nb_observers;
  ca_sas_observer **observers;

  const fdset *fds;
  ccl_list *fv;
  
  ca_sas_simstate *initial_state;
  int state_size; /* it should the size of exprnodes */

  int nb_layers;
  ca_sas_exprnode **nodes;
  ccl_hash *exprnodes; /* map each expr to ca_sas_exprnode **/
  ccl_graph *exprdag; /* graph of expressions. nodes are labelled with 
			 ca_sas_exprnodes */
#if HAVE_PTHREAD
  pthread_mutex_t error_mtx;
  pthread_mutexattr_t error_mtxattr;
# define SAS_LOCK(csm)   pthread_mutex_lock(&(csm)->error_mtx)
# define SAS_UNLOCK(csm)   pthread_mutex_unlock(&(csm)->error_mtx)
#else
# define SAS_LOCK(csm)   ((void)0)
# define SAS_UNLOCK(csm) ((void)0)
  
#endif
};

#define vertex_mask(v) (((uintptr_t)(v))&(~(uintptr_t)0x3))

#define vertex_to_obj(v) ((void *)(vertex_mask (v)))
#define vertex_to_act(v) ((ca_sas_action *)(vertex_to_obj (v)))
#define vertex_to_var(v) ((ar_ca_expr *)(vertex_to_obj (v)))
#define vertex_to_obs(v) ((ca_sas_observer *)(vertex_to_obj (v)))

#define vertex_is_act(v) ((((uintptr_t)(v))&0x3)==0)
#define vertex_is_var(v) ((((uintptr_t)(v))&0x3)==1)
#define vertex_is_obs(v) ((((uintptr_t)(v))&0x3)==2)

#define act_to_vertex(a) ((void *)(((uintptr_t)(a))|0x0))
#define var_to_vertex(v) ((void *)(((uintptr_t)(v))|0x1))
#define obs_to_vertex(o) ((void *)(((uintptr_t)(o))|0x2))

static sas_model_state *
s_ca_sas_crt_state (sas_model *model);
static void
s_ca_sas_delete_state (sas_model *model, sas_model_state *state);
static sas_action * const *
s_ca_sas_get_actions (sas_model *model, int *p_size);
static int
s_ca_sas_get_action_priority (sas_model *model, sas_action *act);
static int 
s_ca_sas_has_memory (sas_model *model, sas_action *act);
static void 
s_ca_sas_map_dependent_observers (sas_model *model, sas_action *act,
				  sas_map_observer_proc *map, void *data);
static void 
s_ca_sas_map_touched_actions (sas_model *model, sas_action *act,
			      sas_model_state *state, 
			      sas_map_action_proc *map, void *data);

static int 
s_ca_sas_trigger_action (sas_model *model, sas_model_state *state,
			 sas_action *action, sas_prng *prng);
static sas_law *
s_ca_sas_get_action_law (sas_model *model, sas_action *action);
static int 
s_ca_sas_is_enabled (sas_model *model, sas_model_state *state, sas_action *act);
static const char *
s_ca_sas_get_action_desc (sas_model *model, sas_action *act);
static void 
s_ca_sas_set_initial_state (sas_model *model, sas_model_state *state);
static sas_observer * const * 
s_ca_sas_get_observers (sas_model *model, int *p_size);
static int 
s_ca_sas_observer_is_boolean (sas_model *model, sas_observer *obs);
static sas_double 
s_ca_sas_get_observer_value (sas_model *model, sas_model_state *state,
			     sas_observer *obs);
static const char *
s_ca_sas_get_observer_desc (sas_model *model, sas_observer *obs);
static int
s_ca_sas_error (sas_model *model);
static void
s_ca_sas_log_error (sas_model *model, ccl_log_type log);
static void
s_ca_sas_set_error (ca_sas_model *model, char *errmsg);
static void
s_ca_sas_set_cst_error (ca_sas_model *model, const char *errmsg);

static const sas_model CA_SAS_MODEL = {
  s_ca_sas_crt_state,
  s_ca_sas_delete_state,
  s_ca_sas_get_actions,
  s_ca_sas_get_action_priority, 
  s_ca_sas_has_memory, 
  s_ca_sas_map_dependent_observers, 
  s_ca_sas_trigger_action,
  s_ca_sas_map_touched_actions, 
  s_ca_sas_get_action_law, 
  s_ca_sas_is_enabled, 
  s_ca_sas_get_action_desc, 
  s_ca_sas_set_initial_state, 
  s_ca_sas_get_observers,
  s_ca_sas_observer_is_boolean, 
  s_ca_sas_get_observer_value,
  s_ca_sas_get_observer_desc, 
  s_ca_sas_error,
  s_ca_sas_log_error
};


static void
s_delete_action (ca_sas_action *a);

static void
s_add_fdset_to_dag (ca_sas_model *csm);

static void
s_build_actions (ca_sas_model *csm);

static void
s_delete_observer (ca_sas_observer *o);

static void
s_build_observers (ca_sas_model *csm);

static void
s_build_dependencies (ca_sas_model *csm);

static void
s_build_initial_state (ca_sas_model *csm);

static ca_sas_exprnode *
s_add_expr_to_dag (ca_sas_model *csm, ar_ca_expr *e);

static void
s_delete_exprnode (ca_sas_exprnode *node);

static void
s_build_bytecode (const ca_sas_simstate *S, const ca_sas_exprnode *n,
		  void * const *opcodes, ca_sas_bytecode *dest);

static void **
s_propagate_assignment (ca_sas_model *csm, ca_sas_simstate *S);

static void
s_compute_layers (ca_sas_model *csm);

static int
s_compare_layers (const void *n1, const void *n2);

static ca_sas_propagation_set *
s_crt_propagation_set (ca_sas_model *csm);

static void
s_delete_propagation_set (ca_sas_propagation_set *set);

static void
s_add_to_propagation_set (ca_sas_model *csm, ca_sas_simstate *S,
			  ca_sas_exprnode *n);

static ca_sas_exprnode *
s_take_from_propagation_set (ca_sas_model *csm, ca_sas_simstate *S);

static void
s_check_ca_for_sas (ca_sas_model *csm);

int sas_debug_ca_checker = 1;
int sas_debug_ca_bytecodes = 1;
int sas_debug_ca_bytecode_exec = 1;
int sas_debug_ca_bytecodes_lengths = 1;

sas_model *
ar_ca_to_sas_model (ar_ca *ca)
{
  ca_sas_model *result;
      
  SAS_DBG_START_TIMER (("translate CA into SAS"));

  result = ccl_new (ca_sas_model);    
  result->super = CA_SAS_MODEL;
  result->ca = ar_ca_add_reference (ca);

#if HAVE_PTHREAD
  pthread_mutexattr_init (&result->error_mtxattr);
  pthread_mutexattr_settype (&result->error_mtxattr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init (&result->error_mtx, &result->error_mtxattr);
#endif
  
  s_check_ca_for_sas (result);

  if (result->errmsg == NULL)
    {
      result->fds = ar_ca_get_fdset (ca);  
      result->fv = fdset_get_ordered_variables (result->fds, 0);
      result->exprnodes =
	ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
			 s_delete_exprnode);
      result->exprdag = ccl_graph_create_default ();
      
      s_add_fdset_to_dag (result);
      s_build_actions (result);
      s_build_observers (result);
      s_build_dependencies (result);
      s_compute_layers (result);
      s_build_initial_state (result);
    }  
  SAS_DBG_END_TIMER ();
  
  return (sas_model *) result;
}

void
ar_ca_destroy_sas_model (sas_model *model)
{
  int i;
  ca_sas_model *csm = (ca_sas_model *) model;

  ccl_zdelete (ccl_delete, csm->nodes);
  ccl_zdelete (ccl_list_delete, csm->fv);

  if (csm->actions)
    {
      for (i = 0; i < csm->nb_actions; i++)
	s_delete_action (csm->actions[i]);
      ccl_delete (csm->actions);
    }
  if (csm->observers)
    {
      for (i = 0; i < csm->nb_observers; i++)
	s_delete_observer (csm->observers[i]);
      ccl_delete (csm->observers);
    }

  if (csm->initial_state)
    s_ca_sas_delete_state (model, (sas_model_state *) csm->initial_state);
  ccl_zdelete (ccl_hash_delete, csm->exprnodes);
  ccl_zdelete (ccl_graph_del_reference, csm->exprdag);  
  ccl_zdelete (ccl_delete, csm->errmsg);
#if HAVE_PTHREAD  
  pthread_mutexattr_destroy (&csm->error_mtxattr);
  pthread_mutex_destroy (&csm->error_mtx);
#endif
  ar_ca_del_reference (csm->ca);  
  ccl_delete (model);
}

static sas_model_state *
s_ca_sas_crt_state (sas_model *model)
{
  int i;
  ca_sas_model *csm = (ca_sas_model *) model;
  ca_sas_simstate *S = ccl_new (ca_sas_simstate);
  ca_sas_exprnode **n;
  void **opcodes = s_propagate_assignment (NULL, NULL);
  ca_sas_bytecode *ip;
  
  S->timestamp = 0;
  S->touched_trans = ccl_bittable_create (csm->nb_actions);
  S->touched_indexes = ccl_heap_create (csm->nb_actions, NULL);
  S->nb_touched = 0;
  S->propagation_set = s_crt_propagation_set (csm);
  S->values = ccl_new_array (timed_value, (csm)->state_size);
  S->bytecodes = ccl_new_array (ca_sas_bytecode,
				csm->state_size * BYTECODE_MAX_SIZE);
  for (i = 0, n = csm->nodes; i < csm->state_size; i++, n++)
    {
      ip = S->bytecodes + (BYTECODE_MAX_SIZE * (*n)->offset);
      s_build_bytecode (S, *n, opcodes, ip);
    }
  
  return (sas_model_state *) S;
}

static void
s_ca_sas_delete_state (sas_model *model, sas_model_state *state)
{
  ca_sas_simstate *S = (ca_sas_simstate *) state;

  ccl_zdelete (ccl_delete, S->bytecodes);
  ccl_bittable_delete (S->touched_trans);
  ccl_heap_delete (S->touched_indexes);
  ccl_delete (S->values);
  s_delete_propagation_set (S->propagation_set);
  ccl_delete (S);
}

static sas_action * const *
s_ca_sas_get_actions (sas_model *model, int *p_size)
{
  ca_sas_model *csm = (ca_sas_model *) model;
  
  *p_size = csm->nb_actions;
  return (sas_action **) csm->actions;
}

static int
s_ca_sas_get_action_priority (sas_model *model, sas_action *act)
{
  ccl_pre (act != NULL);
  
  return ((ca_sas_action *) act)->priority;
}

static int 
s_ca_sas_has_memory (sas_model *model, sas_action *act)
{
  ccl_pre (act != NULL);
  
  return ((ca_sas_action *) act)->has_memory;
}

static void 
s_ca_sas_map_touched_actions (sas_model *model, sas_action *action,
			      sas_model_state *state, 
			      sas_map_action_proc *map, void *data)
{
  int i;
  ca_sas_simstate *S = (ca_sas_simstate *) state;
  ca_sas_action *act = (ca_sas_action *) action;
  ca_sas_action *a;
  ca_sas_model *csm = (ca_sas_model *) model;
  
  if (act->is_self_dependent &&
      ! ccl_bittable_has (S->touched_trans, act->index))
    {
      ccl_heap_add (S->touched_indexes, (void *) (intptr_t) act->index);
      //S->touched_indexes[S->nb_touched++] = act->index;
      ccl_bittable_set (S->touched_trans, act->index);
    }
#if 0
  i = ccl_bittable_get_first (S->touched_trans);
  while (i >= 0)
    {
      a = csm->actions[i];
      map (model, (sas_action *) a, a->index, S->values[a->guard_offset].value, 
	   data);
      i = ccl_bittable_get_next (S->touched_trans, i);
    }
#else
  while (! ccl_heap_is_empty (S->touched_indexes))
    {
      i = (intptr_t) ccl_heap_take_first (S->touched_indexes);
      a = csm->actions[i];
      map (model, (sas_action *) a, a->index, S->values[a->guard_offset].value, 
	   data);
    }
#endif
}

static void 
s_ca_sas_map_dependent_observers (sas_model *model, sas_action *action,
				  sas_map_observer_proc *map, void *data)
{
  ccl_pair *p;
  ca_sas_action *act = (ca_sas_action *) action;
  
  for (p = FIRST (act->depobs); p; p = CDR (p))
    {
      ca_sas_observer *o = CAR (p);
      map (model, (sas_observer *) o, o->index, data);
    }

}

static int 
s_ca_sas_trigger_action (sas_model *model, sas_model_state *state,
			 sas_action *action, sas_prng *prng)
{
  int i;
  ca_sas_value newval;
  sas_proba p;
  sas_proba max;
  ca_sas_model *csm = (ca_sas_model *) model;
  ca_sas_action *act = (ca_sas_action *) action;
  ca_sas_trans *t = act->trans;
  ca_sas_simstate *S = (ca_sas_simstate *)state;
  ca_sas_exprnode **a;  
  timed_value *tv;

  ccl_bittable_clear (S->touched_trans);
  S->nb_touched = 0;
  
  if (act->nb_trans > 1)
    {
      p = sas_prng_probability (prng);
      max = sas_law_param_get_value (t->proba, prng);

      if (p > max)
	{
	  for (i = 1; i < act->nb_trans; i++)
	    {
	      t++;
	      max += sas_law_param_get_value (t->proba, prng);
	      if (p <= max)
		break;
	    }
	  ccl_assert (i < act->nb_trans);
	}
    }
  SAS_DBG_F_START_TIMER (ca_bytecode_exec, ("trigger %s", act->description));
  S->timestamp++;
  for (a = t->assignment; *a; a += 2)
    {
      newval = S->values[a[1]->offset].value;
      tv = S->values + a[0]->offset;
      if (tv->value != newval)
	{
	  s_add_to_propagation_set (csm, S, a[0]);
	  tv->inqueue = 1;
	  tv->timestamp = S->timestamp;
	  tv->value = newval;
	}
    }
  s_propagate_assignment (csm, S);
  
  SAS_DBG_F_END_TIMER (ca_bytecode_exec);
  
  return 1;
}

static sas_law *
s_ca_sas_get_action_law (sas_model *model, sas_action *action)
{
  return sas_law_add_reference (((ca_sas_action *)action)->law);
}

static int 
s_ca_sas_is_enabled (sas_model *model, sas_model_state *state, sas_action *act)
{
  ca_sas_action *a = (ca_sas_action *) act;
  
  return ((ca_sas_simstate *) state)->values[a->guard_offset].value;
}

static const char *
s_ca_sas_get_action_desc (sas_model *model, sas_action *act)
{
  ca_sas_action *a = (ca_sas_action *) act;
  
  return a->description;
}

static void 
s_ca_sas_set_initial_state (sas_model *model, sas_model_state *state)
{
  ca_sas_model *csm = (ca_sas_model *) model;
  ca_sas_simstate *S = (ca_sas_simstate *) state;

  S->timestamp = 1;
  ccl_bittable_clear (S->touched_trans);
  ccl_bittable_assign_union (S->touched_trans,
			     csm->initial_state->touched_trans);
  S->nb_touched = csm->initial_state->nb_touched;
  ccl_heap_copy (S->touched_indexes, csm->initial_state->touched_indexes);
  ccl_memcpy (S->values, csm->initial_state->values,
	      sizeof (timed_value) * csm->state_size);
}

static sas_observer * const * 
s_ca_sas_get_observers (sas_model *model, int *p_size)
{
  ca_sas_model *csm = (ca_sas_model *) model;
  
  *p_size = csm->nb_observers;
  
  return (sas_observer **) csm->observers;
}

static int 
s_ca_sas_observer_is_boolean (sas_model *model, sas_observer *obs)
{
  ca_sas_observer *observer = (ca_sas_observer *) obs;
  
  return ar_ca_expr_is_boolean_expr (observer->expr->e);
} 

static sas_double 
s_ca_sas_get_observer_value (sas_model *model, sas_model_state *state,
			     sas_observer *obs)
{
  ca_sas_observer *observer = (ca_sas_observer *) obs;

  return (sas_double)
    (((ca_sas_simstate *) state)->values[observer->expr_offset].value);
}

static const char *
s_ca_sas_get_observer_desc(sas_model *model, sas_observer *obs)
{
  ca_sas_observer *o = (ca_sas_observer *) obs;
  
  return o->description;
}

static int
s_ca_sas_error (sas_model *model)
{
  return ((ca_sas_model *)model)->errmsg != NULL;
}

static void
s_ca_sas_log_error (sas_model *model, ccl_log_type log)
{
  ca_sas_model *csm = ((ca_sas_model *)model);
  SAS_LOCK (csm);
  ccl_log (log, "%s", csm->errmsg);
  SAS_UNLOCK (csm);
}

static void
s_ca_sas_set_error (ca_sas_model *csm, char *errmsg)
{
  SAS_LOCK (csm);
  ccl_zdelete (ccl_delete, csm->errmsg);
  csm->errmsg = errmsg;
  SAS_UNLOCK (csm);
}

static void
s_ca_sas_set_cst_error (ca_sas_model *csm, const char *errmsg)
{
  s_ca_sas_set_error (csm, ccl_string_dup (errmsg));
}

struct map_cl_data
{
  ccl_list *result;
  ca_sas_model *csm;
};

static int 
s_eval_cond (void *data, void *runtime_data)
{
  intptr_t *offset = (intptr_t *) data;
  ca_sas_simstate *state = runtime_data;

  return state->values[*offset].value;
}

static void
s_map_conditional_laws (void *cond, sas_law *l, void *mapdata)
{
  struct map_cl_data *d = mapdata;
  ca_sas_exprnode *node = s_add_expr_to_dag (d->csm, cond);
  void *data = &node->offset;
  sas_conditional_law *cl =
    sas_conditional_law_crt (l, s_eval_cond, NULL, NULL, data);

  ccl_list_add (d->result, cl);
}

static sas_law *
s_get_law_for_event (ar_ca_event *e, ca_sas_model *csm)
{
  sas_law *result = NULL;
  ar_ca *ca = csm->ca;
  ar_identifier_iterator *ii = ar_ca_event_get_non_epsilon_ids (e);

  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      int len = ar_identifier_get_path_length (id);
      sas_law *l = ar_ca_get_law (ca, id);
      ar_identifier_del_reference (id);
      if (l != NULL)
	{
	  ccl_zdelete (sas_law_del_reference, result);
	  result = l;
	  if (len == 1)
	    break;
	}
    }
  ccl_iterator_delete (ii);
  
  if (sas_law_is_optional (result))
    {
      struct map_cl_data data;
      data.result = ccl_list_create ();
      data.csm = csm;
      
      sas_law_map_conditionals (result, s_map_conditional_laws, &data);
      sas_law_del_reference (result);
      result = sas_law_crt_optional_law (data.result);
    }
  
  return result;
}

static int
s_get_memory_for_event (ar_ca_event *e, ar_ca *ca)
{
  int result = 0;
  ar_identifier_iterator *ii = ar_ca_event_get_non_epsilon_ids (e);

  while (ccl_iterator_has_more_elements (ii) && !result)
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      result = ar_ca_is_preemptible (ca, id);
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);
  
  return result;
}

static int
s_get_priority_for_event (ar_ca_event *e, ar_ca *ca)
{
  int result = 0;
  ar_identifier_iterator *ii = ar_ca_event_get_non_epsilon_ids (e);

  while (ccl_iterator_has_more_elements (ii) && result >= 0)
    {
      int pl;
      ar_identifier *id = ccl_iterator_next_element (ii);

      if (! ar_ca_has_priority (ca, id))
	continue;
      pl = ar_ca_get_priority (ca, id);
      ar_identifier_del_reference (id);
      
      if (ar_identifier_get_path_length (id) == 1)
	{
	  result = pl;
	  break;
	}
      else if (result == 0)
	result = pl;
      else if (result != pl)
	{
	  result = -1;
	  break;
	}
    }
  ccl_iterator_delete (ii);

  return result;
}

static void
s_delete_action (ca_sas_action *a)
{
  int i;

  ccl_list_delete (a->depobs);
  sas_law_del_reference (a->law);
  ccl_delete (a->description);

  for (i = 0; i < a->nb_trans; i++)
    {
      ccl_zdelete (sas_law_param_del_reference, a->trans[i].proba);
      ccl_delete (a->trans[i].assignment);
    }
  ccl_delete (a);
}

static void
s_add_fdset_to_dag (ca_sas_model *csm)
{
  ccl_pair *p;

  for (p = FIRST (csm->fv); p; p = CDR (p))
    s_add_expr_to_dag (csm, CAR (p));
}

static ccl_hash *
s_build_event_to_bucket_table (ar_ca *ca, ccl_hash **p_probas)
{
  ccl_hash *evtable = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_pointer_iterator *pe = ar_ca_get_events_iterator (ca);
  *p_probas = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
			       sas_law_param_del_reference);
			       
  while (ccl_iterator_has_more_elements (pe))
    {
      ar_ca_event *ev = ccl_iterator_next_element (pe);
      ar_identifier_iterator *ii = ar_ca_event_get_non_epsilon_ids (ev);
      
      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *id = ccl_iterator_next_element (ii);
	  ccl_list *b = ar_ca_get_bucket (ca, id);
	  if (b != NULL)
	    {
	      sas_law_param *proba = ar_ca_get_bucket_proba (ca, id);
	      
	      ccl_assert (! ccl_hash_find (evtable, ev));
	      ccl_hash_find (evtable, ev);
	      ccl_hash_insert (evtable, b);
	      
	      ccl_assert (! ccl_hash_find (*p_probas, ev));
	      ccl_hash_find (*p_probas, ev);
	      ccl_hash_insert (*p_probas, proba);	      
	    }
	  ar_identifier_del_reference (id);
	}
      ccl_iterator_delete (ii);
    }
  ccl_iterator_delete (pe);
  
  return evtable; 
}

static ccl_list *
s_build_transition_buckets (ar_ca *ca, ccl_hash **p_probas)
{
  ccl_hash *evprobas = NULL;	       
  ccl_hash *evtable = s_build_event_to_bucket_table (ca, &evprobas);
  ccl_list *result = ccl_list_create ();
  ccl_hash *bucket_to_trans = ccl_hash_create (NULL, NULL, NULL, NULL);
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);

  *p_probas = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
			       sas_law_param_del_reference);
  
  while (nb_trans--)
    {
      ar_ca_event *ev = ar_ca_trans_get_event (*trans);
      if (ccl_hash_find (evtable, ev))
	{
	  ccl_list *translist;
	  sas_law_param *proba;
	  ccl_list *b = ccl_hash_get (evtable);

	  ccl_assert (ccl_hash_find (evprobas, ev));
	  ccl_hash_find (evprobas, ev);
	  proba = ccl_hash_get (evprobas);
	  
	  if (ccl_hash_find (bucket_to_trans, b))
	    translist = ccl_hash_get (bucket_to_trans);
	  else
	    {
	      translist = ccl_list_create ();
	      
	      ccl_list_add (translist, CAR (FIRST (b)));
	      ccl_list_add (result, translist);
	      ccl_hash_insert (bucket_to_trans, translist);
	    }
	  ccl_list_add (translist, *trans);
	  ccl_assert (! ccl_hash_find (*p_probas, *trans));
	  ccl_hash_find (*p_probas, *trans);
	  ccl_hash_insert (*p_probas, sas_law_param_add_reference (proba));
	}
      ar_ca_event_del_reference (ev);
      trans++;
    }
  
  ccl_hash_delete (bucket_to_trans);
  ccl_hash_delete (evtable);
  ccl_hash_delete (evprobas);

  return result;
}

static ca_sas_exprnode **
s_add_assignments_to_dag (ca_sas_model *csm, ar_ca_trans *t)
{  
  int nb_a = ar_ca_trans_get_number_of_assignments (t);
  ar_ca_expr **a = ar_ca_trans_get_assignments (t);
  ca_sas_exprnode **result = ccl_new_array (ca_sas_exprnode *, 2 * nb_a + 1);
  ca_sas_exprnode **e = result;
  
  while (nb_a--)
    {
      /* FIXME: should we add expr_nodes to ca_sas_trans ? */
      *(e++) = s_add_expr_to_dag (csm, a[0]);
      *(e++) = s_add_expr_to_dag (csm, a[1]);
      a += 2;
    }
  
  return result;
}

static void
s_build_buckets (ca_sas_model *csm, ccl_hash *done)
{
  ccl_list *buckets;
  ccl_hash *probas;
  ar_ca *ca = csm->ca;
  
  if (ar_ca_get_buckets (ca) == NULL)
    return;
  
  buckets = s_build_transition_buckets (ca, &probas);
  while (! ccl_list_is_empty (buckets))
    {
      ccl_list *b = ccl_list_take_first (buckets);
      ar_identifier *bid = ccl_list_take_first (b);
      int nb_trans = ccl_list_get_size (b);
      
      /* FIXME: assume all transitions have the same guard. */
      ar_ca_trans *tref = CAR (FIRST  (b));
      ar_ca_expr *g = ar_ca_trans_get_guard_with_domains (tref);
      ar_ca_event *e = ar_ca_trans_get_event (tref);
      ca_sas_action *a = ccl_malloc (sizeof (*a) +
				     sizeof (ca_sas_trans) * (nb_trans - 1));
      
      ccl_assert (! ar_ca_event_is_epsilon (e));      

      a->guard = s_add_expr_to_dag (csm, g);
      ar_ca_expr_del_reference (g);
      
      a->law = s_get_law_for_event (e, csm);
      ccl_assert (a->law != NULL);
      a->has_memory = s_get_memory_for_event (e, ca);
      a->priority = s_get_priority_for_event (e, ca);      
      ar_ca_event_del_reference (e);
      
      a->description = NULL;
      ccl_log_redirect_to_string (CCL_LOG_USR1, &a->description);
      ar_identifier_log (CCL_LOG_USR1, bid);
      ccl_log_pop_redirection (CCL_LOG_USR1);

      a->depobs = ccl_list_create ();
      a->nb_trans = 0;
      while (! ccl_list_is_empty (b))
	{	  
	  ar_ca_trans *t = ccl_list_take_first (b);
	  sas_law_param *p = ccl_hash_get_with_key (probas, t);
	  
	  ccl_assert (! ccl_hash_find (done, t));
	  ccl_assert (p != NULL);

	  ccl_hash_find (done, t);
	  ccl_hash_insert (done, a);

	  a->trans[a->nb_trans].T = t;
	  a->trans[a->nb_trans].assignment = s_add_assignments_to_dag (csm, t);
	  a->trans[a->nb_trans].proba = sas_law_param_add_reference (p);

	  a->nb_trans++;
	}
      ccl_assert (a->nb_trans == nb_trans);
      ccl_list_delete (b);
    }
  ccl_hash_delete (probas);
  ccl_list_delete (buckets);
}

static void
s_build_actions (ca_sas_model *csm)
{
  int i, k;
  ar_ca *ca = csm->ca;
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);
  ccl_hash *done = ccl_hash_create (NULL, NULL, NULL, NULL);
  
  s_build_buckets (csm, done);
    
  for (i = 0; i < nb_trans; i++)
    {      
      ca_sas_action *a;
      ar_ca_event *e = ar_ca_trans_get_event (trans[i]);
      ar_ca_expr *g;
      
      if (ccl_hash_find (done, trans[i]) || ar_ca_event_is_epsilon (e))
	{
	  ar_ca_event_del_reference (e);
	  continue;
	}
      
      a = ccl_new (ca_sas_action);
      ccl_hash_insert (done, a);

      g = ar_ca_trans_get_guard_with_domains (trans[i]);
      a->guard = s_add_expr_to_dag (csm, g);
      ar_ca_expr_del_reference (g);

      a->law = s_get_law_for_event (e, csm);
      ccl_assert (a->law != NULL);
      a->has_memory = s_get_memory_for_event (e, ca);     
      a->priority = s_get_priority_for_event (e, ca);
      
      a->description = NULL;
      ccl_log_redirect_to_string (CCL_LOG_USR1, &a->description);
      ar_ca_event_log (CCL_LOG_USR1, e);
      ccl_log_pop_redirection (CCL_LOG_USR1);

      a->nb_trans = 1;
      a->trans->T = trans[i];
      a->trans->assignment = s_add_assignments_to_dag (csm, a->trans->T);
      a->trans->proba = NULL;
      
      a->depobs = ccl_list_create ();
      
      ar_ca_event_del_reference (e);      
    }
  
  csm->actions = ccl_new_array (ca_sas_action *,
				ccl_hash_get_size (done));
  
  for (i = 0, k = 0; i < nb_trans; i++)
    {            
      if (ccl_hash_find (done, trans[i]))
	{
	  int j;
	  ca_sas_action *a = ccl_hash_get (done);

	  a->index = k;
	  csm->actions[k++] = a;
	  for (j = 0; j < a->nb_trans; j++)
	    {
	      ccl_hash_find (done, a->trans[j].T);
	      ccl_hash_remove (done);
	    }
	}
    }
  csm->nb_actions = k;
  for (i = 0; i < k; i++)
    {
      ca_sas_action *a = csm->actions[i];

      if (a->guard->related_actions == NULL)
	{
	  a->guard->related_actions = ccl_new_array (int, 1);
	  a->guard->nb_related_actions = 0;
	}
      else
	{
	  a->guard->related_actions =
	    ccl_realloc (a->guard->related_actions,
			 sizeof (int) * (a->guard->nb_related_actions + 1));
	}
      a->guard->related_actions[a->guard->nb_related_actions++] = a->index;
    }
  ccl_hash_delete (done);
}

static void
s_delete_observer (ca_sas_observer *o)
{
  ccl_delete (o->description);
  ccl_delete (o);
}

static ca_sas_observer *
s_crt_observer (int index, ar_ca_expr *e, ar_identifier *desc,
		ca_sas_model *csm)
{
  ca_sas_observer *o = ccl_new (ca_sas_observer);

  o->index = index;
  o->expr = s_add_expr_to_dag (csm, e);
  o->description = NULL;
  
  ccl_log_redirect_to_string (CCL_LOG_USR1, &o->description);
  if (desc != NULL)
    ar_identifier_log (CCL_LOG_USR1, desc);
  else
    ar_ca_expr_log (CCL_LOG_USR1, e);
  ccl_log_pop_redirection (CCL_LOG_USR1);

  return o;
}

static ccl_list *
s_compute_remaining_assertions (ca_sas_model *csm)
{
  const ccl_pair *p;
  ar_ca_expr *a;
  ar_ca *ca = csm->ca;
  int nb_asserts = ar_ca_get_number_of_assertions (ca);
  const ccl_list *assertions = ar_ca_get_assertions (ca);
  ccl_list *result = ccl_list_create ();

  SAS_DBG_START_TIMER (("compute assertions to observe"));
  for (p = FIRST (assertions); p; p = CDR (p))
    {
      a = CAR (p);
      if (! fdset_implies (csm->fds, a))
	ccl_list_add (result, a);
      nb_asserts--;
    }
  if (SAS_DBG_IS_ON)
    ccl_debug ("%d remaining assertions.\n", ccl_list_get_size (result));
  SAS_DBG_END_TIMER ();
  
  return  result;
}

static void
s_build_observers (ca_sas_model *csm)
{
  int i = 0;
  ar_ca *ca = csm->ca;
  int nb_observers = ar_ca_get_number_of_observers (ca);
  ccl_list *asserts = s_compute_remaining_assertions (csm);

  csm->nb_observers = nb_observers + ccl_list_get_size (asserts);
  csm->observers = ccl_new_array (ca_sas_observer *, csm->nb_observers);
  
  if (csm->nb_observers > 0)
    {
      if (nb_observers)
	{
	  ar_identifier_iterator *ii = ar_ca_get_observer_ids (ca);
	  
	  while (ccl_iterator_has_more_elements (ii))
	    {
	      ar_identifier *oid = ccl_iterator_next_element (ii);
	      ar_ca_expr *obs = ar_ca_get_observer (ca, oid);
	      csm->observers[i] = s_crt_observer (i, obs, oid, csm);
	      ar_identifier_del_reference (oid);
	      ar_ca_expr_del_reference (obs);
	      i++;
	    }
	  ccl_iterator_delete (ii);
	}

      while (! ccl_list_is_empty (asserts))
	{
	  ar_ca_expr *obs = ccl_list_take_first (asserts);
	  obs = ar_ca_expr_crt_not (obs);
	  csm->observers[i] = s_crt_observer (i, obs, NULL, csm);
	  ar_ca_expr_del_reference (obs);
	  i++;
	}
    }
  ccl_list_delete (asserts);
}


static void
s_add_transition_edges (ca_sas_model *csm, ccl_graph *G)
{
  int i;

  for (i = 0; i < csm->nb_actions; i++)
    {
      int j;
      ccl_pair *p;
      ccl_list *read = NULL;
      ccl_list *write = NULL;
      ca_sas_action *act = csm->actions[i];
      void *vact = act_to_vertex (act);

      for (j = 0; j < act->nb_trans; j++)
	{
	  ar_ca_trans *t = act->trans[j].T;
	  read = ar_ca_trans_get_variables (t, read, AR_CA_R_VARS);
	  write = ar_ca_trans_get_variables (t, write, AR_CA_W_VARS);
	}
      
      for (p = FIRST (write); p; p = CDR (p))
	{
	  void *vvar = var_to_vertex (CAR (p));
	  ccl_graph_add_data_edge (G, vvar, vact, NULL);
	}
      for (p = FIRST (read); p; p = CDR (p))
	{
	  void *vvar = var_to_vertex (CAR (p));
	  ccl_graph_add_data_edge (G, vact, vvar, NULL);
	}
      ccl_list_delete (read);
      ccl_list_delete (write);
    }
}

static void
s_add_observer_edges (ca_sas_model *csm, ccl_graph *G)
{
  int i;

  for (i = 0; i < csm->nb_observers; i++)
    {
      ccl_pair *p;
      ca_sas_observer *obs = csm->observers[i];
      void *vobs = obs_to_vertex (obs);
      ccl_vertex *src = ccl_graph_find_or_add_vertex (G, vobs);
      ccl_list *vars = ar_ca_expr_get_variables (obs->expr->e, NULL);

      for (p = FIRST (vars); p; p = CDR (p))
	{
	  void *vvar = var_to_vertex (CAR (p));
	  ccl_vertex *tgt = ccl_graph_find_or_add_vertex (G, vvar);
	  ccl_graph_add_edge (G, src, tgt, NULL);
	}      
      ccl_list_delete (vars);
    }
}

static void
s_add_fds_edges (ca_sas_model *csm, ccl_graph *G)
{
  const ccl_graph *gfds = fdset_get_dependency_graph (csm->fds);
  ccl_edge_iterator *ei = ccl_graph_get_edges (gfds);

  while (ccl_iterator_has_more_elements (ei))
    {
      ccl_edge *e = ccl_iterator_next_element (ei);
      ccl_vertex *src = ccl_edge_get_src (e);
      void *lsrc = ccl_vertex_get_data (src);
      ccl_vertex *tgt = ccl_edge_get_tgt (e);
      void *ltgt = ccl_vertex_get_data (tgt);

      lsrc = var_to_vertex (lsrc);
      ltgt = var_to_vertex (ltgt);
      ccl_graph_add_data_edge (G, lsrc, ltgt, NULL);	  
    }
  ccl_iterator_delete (ei);
}

static void
s_dot_vertex_data(ccl_log_type log, ccl_vertex *v, void *cbdata)
{
  void *d = ccl_vertex_get_data (v);
  ccl_log (log, "label=\"");
  if (vertex_is_obs (d))
    {
      ca_sas_observer *obs = vertex_to_obs (d);
      ccl_log (log, "obs(%s)", obs->description);
    }
  else if (vertex_is_act (d))
    {
      ca_sas_action *act = vertex_to_act (d);
      ccl_log (log, "T(%s)", act->description);
      
    }
  else 
    {
      ar_ca_expr *var = vertex_to_var (d);
      ccl_assert (vertex_is_var (d));
      ccl_log (log, "var(");
      ar_ca_expr_log (log, var);
      ccl_log (log, ")");
    }
  ccl_log (log, "\"");
}

void s_log_depgraph (ccl_log_type l, ccl_graph *G)
{
  ccl_graph_log_as_dot (l, G,
			NULL, NULL, /*graph_attributes */
			s_dot_vertex_data, NULL,
			NULL, NULL /* dot_edge_data */
			);
}

static int 
s_map_observer_reachables (ccl_vertex *v, void *data)
{
  void *label = ccl_vertex_get_data (v);
  int res = ! vertex_is_act (label);
  
  if (vertex_is_act (label))
    {
      ca_sas_action *act = vertex_to_obj (label);
      ccl_list_add (act->depobs, data);
    }
  return res;
}

static void
s_build_dependencies_for_actions (ca_sas_model *csm, ccl_graph *G)
{
  int i;
  
  SAS_DBG_START_TIMER (("compute dependent actions"));
  
  for (i = 0; i < csm->nb_actions; i++)
    {
      ca_sas_action *act = csm->actions[i];
      act->is_self_dependent = 1;
    }
  SAS_DBG_END_TIMER ();
}

static void
s_build_dependencies (ca_sas_model *csm)
{
  int i;
  static ccl_vertex_data_methods vmethods = { NULL, NULL, NULL }; 
  static ccl_edge_data_methods emethods = { NULL, NULL, NULL };
  ccl_graph *G = ccl_graph_create (&vmethods, &emethods);

  s_add_transition_edges (csm, G);
  s_add_observer_edges (csm, G);
  s_add_fds_edges (csm, G);

  SAS_DBG_START_TIMER (("compute dependent observers"));
  for (i = 0; i < csm->nb_observers; i++)
    {
      ca_sas_observer *obs = csm->observers[i];
      ccl_vertex *v = ccl_graph_get_vertex (G, obs_to_vertex (obs));
      ccl_graph_map_reachables_from (G, v, s_map_observer_reachables, obs);
      ccl_assert (ccl_vertex_get_in_degree (v) == 0);
      ccl_graph_remove_vertex (G, v, 0);
    }
  SAS_DBG_END_TIMER ();

  s_build_dependencies_for_actions (csm, G);
      
  ccl_graph_del_reference (G);
}


static ccl_set *
s_set_of_state_variables (const ar_ca *ca)
{
  ccl_pair *p;
  ccl_set *result = ccl_set_create ();
  const ccl_list *svars = ar_ca_get_state_variables (ca);

  for (p = FIRST (svars); p; p = CDR (p))
    ccl_set_add (result, CAR (p));
  
  return result;
}

static void
s_build_initial_state (ca_sas_model *csm)
{
  const ccl_pair *p;
  const ccl_list *init = ar_ca_get_initial_assignments (csm->ca);
  ccl_set *svars = s_set_of_state_variables (csm->ca);
  ccl_pointer_iterator *pi = ccl_hash_get_elements (csm->exprnodes);

  csm->state_size = ccl_hash_get_size (csm->exprnodes);
  csm->initial_state = (ca_sas_simstate *)
    s_ca_sas_crt_state ((sas_model *) csm);
  csm->initial_state->timestamp = 1;
  
  while (ccl_iterator_has_more_elements (pi))
    {
      ca_sas_exprnode *node = ccl_iterator_next_element (pi);

      if (ar_ca_expr_get_kind (node->e) == AR_CA_CST)
	{	  
	  timed_value *tv = &csm->initial_state->values[node->offset];
	  tv->value = ar_ca_expr_get_min (node->e);
	  tv->timestamp = csm->initial_state->timestamp;
	  tv->inqueue = 1;
	  s_add_to_propagation_set (csm, csm->initial_state, node);
	}
    }
  ccl_iterator_delete (pi);

  for (p = FIRST (init); p; p = CDDR (p))
    {
      timed_value *tv;
      ar_ca_expr *v = CAR (p);
      ar_ca_expr *val = CADR (p);
      ca_sas_exprnode *vnode = ccl_hash_get_with_key (csm->exprnodes, v);

      ccl_assert (ar_ca_expr_get_kind (val) == AR_CA_CST);
      ccl_assert (vnode != NULL);

      tv = &csm->initial_state->values[vnode->offset];
      tv->value = ar_ca_expr_get_min (val);
      tv->timestamp = csm->initial_state->timestamp;
      if (! tv->inqueue)
	{
	  tv->inqueue = 1;
	  s_add_to_propagation_set (csm, csm->initial_state, vnode);
	  ccl_set_remove (svars, v);
	}
    }
  
  if (! ccl_set_is_empty (svars))
    {
      char *errmsg = NULL;
      ccl_pointer_iterator *pi = ccl_set_get_elements (svars);
      ccl_log_redirect_to_string (CCL_LOG_USR1, &errmsg);
      ccl_log (CCL_LOG_USR1, "state variable '");
      ar_ca_expr_log (CCL_LOG_USR1, ccl_iterator_next_element (pi));
      ccl_log (CCL_LOG_USR1, "' is not initialized.");
      ccl_log_pop_redirection (CCL_LOG_USR1);
      ccl_iterator_delete (pi);
      s_ca_sas_set_error (csm, errmsg);
    }
  ccl_set_delete (svars);
  s_propagate_assignment (csm, csm->initial_state);
}


static ca_sas_exprnode *
s_add_expr_to_dag (ca_sas_model *csm, ar_ca_expr *e)
{
  ca_sas_exprnode *node;
  
  if (ccl_hash_find (csm->exprnodes, e))
    node = ccl_hash_get (csm->exprnodes);
  else
    {
      ar_ca_expression_kind kind = ar_ca_expr_get_kind (e);

      node = ccl_new (ca_sas_exprnode);
      node->layer = -1;
      node->related_actions = NULL;
      node->nb_related_actions = 0;
      node->e = ar_ca_expr_add_reference (e);
      node->offset = ccl_hash_get_size (csm->exprnodes);
      node->vertex = ccl_graph_add_vertex (csm->exprdag, node);
      ccl_hash_insert (csm->exprnodes, node);

      if (kind == AR_CA_VAR)
	{
	  funcdep *fd = fdset_get_funcdep (csm->fds, e);
	  if (fd != NULL)
	    {
	      node->fun = s_add_expr_to_dag (csm, funcdep_get_function (fd));
	      ccl_graph_add_edge (csm->exprdag, node->vertex, node->fun->vertex,
				  NULL);

	    }
	  else
	    node->fun = NULL;
	}
      else if (kind != AR_CA_CST)
	{
	  int i;
	  ar_ca_expr **args = ar_ca_expr_get_args (e);

	  for(i = 0; *args; args++, i++)
	    {
	      ca_sas_exprnode *na = s_add_expr_to_dag (csm, *args);
	      ccl_graph_add_edge (csm->exprdag, node->vertex, na->vertex, NULL);
	      node->args[i] = na;
	    }
	}      
    }
  
  return node;
}

static void
s_delete_exprnode (ca_sas_exprnode *node)
{
  ccl_pre (node != NULL);
  ccl_zdelete (ccl_delete, node->related_actions);
  ar_ca_expr_del_reference (node->e);
  ccl_zdelete (ccl_delete, node->preds);
  ccl_delete (node);
}

#if 0
static int
s_eval_expr (ca_sas_simstate *S, ca_sas_exprnode *n)
{
  int result = 0;
  ar_ca_expression_kind kind = ar_ca_expr_get_kind (n->e);
    
  switch (kind)
    {
    case AR_CA_CST:
      result = valueof (S, n);
      break;
    case AR_CA_VAR:
      if (n->fun == NULL)
	result = valueof (S, n);
      else
	{
	  ccl_assert (timestampof (S, n->fun) >= timestampof (S, n));
	  result = valueof (S, n->fun);
	}
      break;
      
    case AR_CA_AND:
      result = valueof (S, n->args[0]) && valueof (S, n->args[1]);
      break;
      
    case AR_CA_OR:
      result = valueof (S, n->args[0]) || valueof (S, n->args[1]);
      break;
      
    case AR_CA_NOT: result = ! valueof (S, n->args[0]); break;
    case AR_CA_NEG: result = - valueof (S, n->args[0]); break;
      
    case AR_CA_ADD:
      result = valueof (S, n->args[0]) + valueof (S, n->args[1]);
      break;

    case AR_CA_MUL:
      result = valueof (S, n->args[0]) * valueof (S, n->args[1]);
      break;
      
    case AR_CA_DIV:
      result = valueof (S, n->args[0]) / valueof (S, n->args[1]);
      break;
      
    case AR_CA_MOD:
      result = valueof (S, n->args[0]) % valueof (S, n->args[1]);
      break;
      
    case AR_CA_EQ:
      result = valueof (S, n->args[0]) == valueof (S, n->args[1]);
      break;

    case AR_CA_LT:
      result = valueof (S, n->args[0]) < valueof (S, n->args[1]);
      break;

    case AR_CA_ITE:
      if (valueof (S, n->args[0]))
	result = valueof (S, n->args[1]);
      else 
	result = valueof (S, n->args[2]);
      break;

    case AR_CA_MIN:
      if (valueof (S, n->args[0]) < valueof (S, n->args[1]))
	result = valueof (S, n->args[0]);
      else 
	result = valueof (S, n->args[1]);
      break;
    case AR_CA_MAX:
      if (valueof (S, n->args[0]) < valueof (S, n->args[1]))
	result = valueof (S, n->args[1]);
      else 
	result = valueof (S, n->args[2]);
      break;
    };
  
  return result;
}
#endif

#define BYTECODE_OPCODES \
  AR_CA_EXPR_KINDS \
  AR_CA_EXPR_OP(TERMINATE)

#define AR_CA_EXPR_OP(k) JIT_OPCODE_ ## k,
enum jitopcode { BYTECODE_OPCODES };
#undef AR_CA_EXPR_OP
#define OP(opc_,k_) ((opc_)[(k_)])
#define ARG(i_) ((ca_sas_value *)(ip)[(i_)])
#define VAL(i_) (*ARG(i_))
#define R VAL(0)
#define VAL1 VAL(1)
#define VAL2 VAL(2)
#define VAL3 VAL(3)
#define GOTONEXT goto **((ip)++)

static void
s_build_bytecode (const ca_sas_simstate *S, const ca_sas_exprnode *n,
		  void * const *opcodes, ca_sas_bytecode * dest)
		  
{
  int i;
  timed_value *tv = &S->values[n->offset];  
  ar_ca_expression_kind kind = ar_ca_expr_get_kind (n->e);
  
  if (! (kind == AR_CA_CST || (kind == AR_CA_VAR && n->fun == NULL)))
    {
      *(dest++) = OP (opcodes, kind);
      *(dest++) = &tv->value;
      if (kind == AR_CA_VAR)
	*(dest++) = &valueof (S, n->fun);
      else 
	{

	  for (i = 0; i < 3; i++)
	    if (n->args[i] != NULL)
	      *(dest++) = &valueof (S, n->args[i]);
	}
    }
  *dest = OP (opcodes, JIT_OPCODE_TERMINATE);
}

static void **
s_propagate_assignment (ca_sas_model *csm, ca_sas_simstate *S)
{
#define AR_CA_EXPR_OP(k) &&OP_ ## k,
  static void *addr[] = { BYTECODE_OPCODES };
#undef AR_CA_EXPR_OP
  register ca_sas_bytecode *ip;
  int i, oldval;
  ca_sas_exprnode *n;
  ca_sas_exprnode **p;
  timed_value *tv;
  timed_value *tvpred;

  if (csm == NULL)
    return addr;
  
  while ((n = s_take_from_propagation_set (csm, S)) != NULL)
    {
      tv = S->values + n->offset;
      if (SAS_DBG (ca_bytecode_exec))
	{
	  ccl_debug ("propagate ");
	  ar_ca_expr_log (CCL_LOG_DEBUG, n->e);
	  ccl_debug (" (current=%d)\n", valueof (S, n));
	  ccl_assert (tv->timestamp <= S->timestamp);
	  ccl_assert (tv->inqueue);
	}

      tv->inqueue = 0;
      if (tv->timestamp < S->timestamp)
	{
#if 1
	  oldval = tv->value;	  
	  ip = S->bytecodes + (BYTECODE_MAX_SIZE * n->offset);
	  
	  GOTONEXT;
	  
	OP_VAR: R = VAL1; ip += 2; GOTONEXT;   
	OP_NEG: R = -VAL1; ip += 2; GOTONEXT;
	OP_NOT: R = ! VAL1; ip += 2; GOTONEXT;
	OP_AND: R = VAL1 && VAL2; ip += 3; GOTONEXT;  
	OP_OR : R = VAL1 || VAL2; ip += 3; GOTONEXT;  
	OP_ADD: R = VAL1 + VAL2; ip += 3; GOTONEXT;  
	OP_MUL: R = VAL1 * VAL2; ip += 3; GOTONEXT;  
	OP_DIV: R = VAL1 / VAL2; ip += 3; GOTONEXT;  
	OP_MOD: R = VAL1 % VAL2; ip += 3; GOTONEXT;  
	OP_MIN: R = ((VAL1 < VAL2) ? VAL1 : VAL2); ip += 3; GOTONEXT;
	OP_MAX: R = ((VAL1 < VAL2) ? VAL2 : VAL1); ip += 3; GOTONEXT;
	OP_EQ : R = (VAL1 == VAL2); ip += 3; GOTONEXT;
	OP_LT : R = ((VAL1 < VAL2)); ip += 3; GOTONEXT;
	OP_ITE: R = (VAL1 ? VAL2 : VAL3); ip += 4; GOTONEXT;
	OP_CST: 
	OP_TERMINATE:
	  if (oldval != tv->value || S->timestamp == 1)
	    {
	      tv->timestamp = S->timestamp;
	      if (SAS_DBG (ca_bytecode_exec))
		ccl_debug ("new value %d\n", valueof (S, n));
	    }
#else
	  oldval = s_eval_expr (S, n);
	  if (oldval != tv->value || S->timestamp == 1)
	    {
	      tv->value = oldval;
	      tv->timestamp = S->timestamp;
	      if (SAS_DBG (ca_bytecode_exec))
		ccl_debug ("new value %d\n", valueof (S, n));
	    }
#endif
	}

      if (tv->timestamp == S->timestamp)
	{
	  if (n->related_actions != NULL)
	    {
	      for (i = 0; i < n->nb_related_actions; i++)
		{
		  intptr_t ai = n->related_actions[i];
		  if (! ccl_bittable_has (S->touched_trans, ai))
		    ccl_heap_add (S->touched_indexes, (void *) ai);
		}
	      ccl_bittable_set_array (S->touched_trans, n->nb_related_actions,
				      n->related_actions);
	    }
	  
	  if (n->preds != NULL)
	    {
	      for (p = n->preds; *p; p++)
		{
		  tvpred = &S->values[(*p)->offset];
		  if (! tvpred->inqueue)
		    {
		      tvpred->inqueue = 1;
		      s_add_to_propagation_set (csm, S, *p);
		    }
		}
	    }
	}
    }
  return addr;    
}

static void
s_comp_layer_rec (ccl_vertex *v)
{
  int max;
  ccl_vertex_iterator *vi;
  ca_sas_exprnode *n = ccl_vertex_get_data (v);

  if (n->layer >= 0)
    return;
  
  if (ccl_vertex_get_out_degree (v) == 0)
    n->layer = 0;
  else
    {
      max = 0;
      vi = ccl_vertex_get_successors (v);

      while (ccl_iterator_has_more_elements (vi))
	{
	  ccl_vertex *succ = ccl_iterator_next_element (vi);
	  ca_sas_exprnode *succn = ccl_vertex_get_data (succ);
	  s_comp_layer_rec (succ);
	  if (succn->layer > max)
	    max = succn->layer;
	}
      n->layer = max + 1;
      ccl_iterator_delete (vi);
    }
}

static void
s_build_pred_tables (ccl_vertex *v, void *data)
{
  ca_sas_exprnode *n = ccl_vertex_get_data (v);
  int indegree = ccl_vertex_get_in_degree (v);

  if (indegree > 0)
    {
      int i = 0;
      ccl_vertex_iterator *vi = ccl_vertex_get_predecessors (v);
      
      n->preds = ccl_new_array (ca_sas_exprnode *, indegree + 1);
      
      while (ccl_iterator_has_more_elements (vi))
	{
	  ccl_vertex *p = ccl_iterator_next_element (vi);
	  n->preds[i++] = ccl_vertex_get_data (p);
	}
      ccl_iterator_delete (vi);
    }
  else
    {
      n->preds = NULL;
    }
}

static void
s_compute_layers (ca_sas_model *csm)
{
  int i;
  int offset;
  ccl_heap *heap;
  int nb_nodes = ccl_hash_get_size (csm->exprnodes);
  ccl_vertex_iterator *vi = ccl_graph_get_roots (csm->exprdag);
  ccl_pointer_iterator *pi;
  int maxlayer = 0;
  
  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *v = ccl_iterator_next_element (vi);
      s_comp_layer_rec (v);
    }
  ccl_iterator_delete (vi);

  heap = ccl_heap_create (nb_nodes, s_compare_layers);  
  pi = ccl_hash_get_elements (csm->exprnodes);
  while (ccl_iterator_has_more_elements (pi))
    {
      ca_sas_exprnode *n = ccl_iterator_next_element (pi);
      ccl_heap_add (heap, n);
    }
  ccl_iterator_delete (pi);

  csm->nodes = ccl_new_array (ca_sas_exprnode *, nb_nodes);
  offset = 0;
  while (ccl_heap_get_size (heap) > 0)
    {
      ca_sas_exprnode *n = ccl_heap_take_first (heap);
      n->offset = offset++;
      csm->nodes[n->offset] = n;
      if (n->layer > maxlayer)
	maxlayer = n->layer;
    }
  ccl_heap_delete (heap);
  csm->nb_layers = maxlayer + 1;
  
  for (i = 0; i < csm->nb_actions; i++)
    csm->actions[i]->guard_offset = csm->actions[i]->guard->offset;
  for (i = 0; i < csm->nb_observers; i++)
    csm->observers[i]->expr_offset = csm->observers[i]->expr->offset;

  ccl_graph_map_vertices (csm->exprdag, s_build_pred_tables, NULL);
}

static int
s_compare_layers (const void *n1, const void *n2)
{
  return (((const ca_sas_exprnode *) n1)->layer -
	  ((const ca_sas_exprnode *) n2)->layer);
}

#if 0
static void
s_log_state (ccl_log_type log, ca_sas_simstate *S, ca_sas_model *csm)
{
  ccl_pointer_iterator *pi = ccl_hash_get_elements (csm->exprnodes);
  
  while (ccl_iterator_has_more_elements (pi))
    {
      ca_sas_exprnode *n = ccl_iterator_next_element (pi);

      ar_ca_expr_log (log, n->e);
      ccl_log (log, " = %2d (timestamp = %d)\n", valueof (S, n),
	       timestampof (S, n));      
    }
  ccl_iterator_delete (pi);
}
#endif

static ca_sas_propagation_set *
s_crt_propagation_set (ca_sas_model *csm)
{
  int i;
  ca_sas_propagation_set *result = ccl_new (ca_sas_propagation_set);

  result->entries = ccl_heap_create (csm->nb_layers, NULL);
  result->layers = ccl_new_array (int, csm->nb_layers);
  result->next = ccl_new_array (int, csm->state_size);
  for (i = 0; i < csm->nb_layers; i++)
    result->layers[i] = 0;
  for (i = 0; i < csm->state_size; i++)
    result->next[i] = 0;
    
  return result;
}

static void
s_delete_propagation_set (ca_sas_propagation_set *set)
{
  ccl_heap_delete (set->entries);
  ccl_delete (set->layers);
  ccl_delete (set->next);
  ccl_delete (set);
}

static void
s_add_to_propagation_set (ca_sas_model *csm, ca_sas_simstate *S,
			  ca_sas_exprnode *n)
{
  ca_sas_propagation_set *set = S->propagation_set;

  set->next[n->offset] = set->layers[n->layer];
  set->layers[n->layer] = n->offset + 1;
  if (! ccl_heap_has (set->entries, (void *) (intptr_t) n->layer))
    ccl_heap_add (set->entries, (void *) (intptr_t) n->layer);
}

static ca_sas_exprnode *
s_take_from_propagation_set (ca_sas_model *csm, ca_sas_simstate *S)
{   
  int c;
  int *layers;
  int *next;
  ca_sas_propagation_set *set = S->propagation_set;
  ca_sas_exprnode *result;

  if (ccl_heap_is_empty (set->entries))
    return NULL;
  
  c = (intptr_t) ccl_heap_get_first (set->entries);
  layers = set->layers + c;
  ccl_assert (*layers);
  next = set->next;
  result = csm->nodes[*layers - 1]; 
  if ((*layers = next[result->offset]) == 0)
    ccl_heap_take_first (set->entries);
  next[result->offset] = 0;

  return result;
}

/*******************************************************************************
 *
 * Check validity of the extern clauses for SAS module
 *
 ******************************************************************************/


static int
s_count_laws (ca_sas_model *csm, ar_ca_event *e)
{
  int result = 0;
  ar_identifier_iterator *ii = ar_ca_event_get_non_epsilon_ids (e);
  
  while (ccl_iterator_has_more_elements (ii) && result <= 1)
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      if (ar_ca_has_law (csm->ca, id))
	{
	  if (ar_identifier_get_path_length (id) == 1)
	    {
	      ar_identifier_del_reference (id);
	      result = 1;
	      break;
	    }
	  result++;
	}
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);

  return result;
}

static void
s_check_event_for_laws (ca_sas_model *csm, ar_ca_event *e)
{
  int nb_laws;
  char *errmsg;
  
  if (ar_ca_event_is_epsilon (e))
    return;
  
  nb_laws = s_count_laws (csm, e);  
  if (nb_laws == 1)
    return;

  errmsg = NULL;
  ccl_log_redirect_to_string (CCL_LOG_USR1, &errmsg);
  if (nb_laws == 0)
    {
      ccl_log (CCL_LOG_USR1, "no law is associated to event ");
      ar_ca_event_log (CCL_LOG_USR1, e);
      ccl_log (CCL_LOG_USR1, ".\n");
    }
  else 
    {
      ccl_log (CCL_LOG_USR1, "law associated to event ");
      ar_ca_event_log (CCL_LOG_USR1, e);
      ccl_log (CCL_LOG_USR1, " is not uniquely defined.\n");
    }
  ccl_log_pop_redirection (CCL_LOG_USR1);
  s_ca_sas_set_error (csm, errmsg);  
}

static void
s_check_ca_laws (ca_sas_model *csm)
{
  if (s_ca_sas_error ((sas_model *) csm))
    return;
  
  SAS_DBG_START_TIMER (("checking laws"));
  if (ar_ca_get_number_of_laws (csm->ca) == 0)
    s_ca_sas_set_cst_error (csm, "no probalistic law is specified");
  else
    {
      ccl_pointer_iterator *ei = ar_ca_get_events_iterator (csm->ca);
      while (ccl_iterator_has_more_elements (ei) &&
	     ! s_ca_sas_error ((sas_model *) csm))
	{
	  ar_ca_event *e = ccl_iterator_next_element (ei);
	  s_check_event_for_laws (csm, e);
	}
      ccl_iterator_delete (ei);
    }
  SAS_DBG_END_TIMER ();
}

static void 
s_register_bucket_events (ca_sas_model *csm, ccl_list *events, ccl_set *reg)
{
  ccl_pair *p;

  for (p = FIRST (events); p; p = CDR (p))
    {
      if (ccl_hash_find (reg, CAR (p)))
	{
	  if (ccl_hash_get (reg) == events)
	    continue;
	  
	  s_ca_sas_set_cst_error (csm,
				  "a synchronized event occurs in "
				  "several buckets.");
	  return;
	}
      
      ccl_hash_insert (reg, events);
    }
}


static void
s_check_ca_buckets (ca_sas_model *csm) 
{
  ccl_pair *p;
  ccl_hash *in_bucket;

  if (ar_ca_get_buckets (csm->ca) == NULL ||
      s_ca_sas_error ((sas_model *) csm))
    return;
  
  SAS_DBG_START_TIMER (("checking buckets"));
  in_bucket = ccl_hash_create (NULL, NULL, NULL, NULL);
  for (p = FIRST (ar_ca_get_buckets (csm->ca)); p; p = CDR (p))
    {
      if (s_ca_sas_error ((sas_model *) csm))
	break;
      s_register_bucket_events (csm, CAR (p), in_bucket);
    }
  ccl_hash_delete (in_bucket);
  SAS_DBG_END_TIMER ();
}

static void
s_check_ca_priorities (ca_sas_model *csm) 
{
  ccl_pointer_iterator *ei;
  if (s_ca_sas_error ((sas_model *) csm))
    return;

  SAS_DBG_START_TIMER (("checking priorities"));
  ei = ar_ca_get_events_iterator (csm->ca);  
  while (ccl_iterator_has_more_elements (ei) &&
	 ! s_ca_sas_error ((sas_model *) csm))
    {
      ar_ca_event *e = ccl_iterator_next_element (ei);
      int pl = s_get_priority_for_event (e, csm->ca);
      if (pl < 0)
	{	  
	  char *errmsg = NULL;
	  ccl_log_redirect_to_string (CCL_LOG_USR1, &errmsg);
	  ccl_log (CCL_LOG_USR1, "event '");
	  ar_ca_event_log (CCL_LOG_USR1, e);
	  ccl_log (CCL_LOG_USR1, "' contains label with different priority "
		   "levels.");
	  ccl_log_pop_redirection (CCL_LOG_USR1);
	  
	  s_ca_sas_set_cst_error (csm, errmsg);
	  ccl_string_delete (errmsg);
	  break;
	}
      else if (SAS_DBG(ca_checker))
	{
	  ccl_debug ("'");
	  ar_ca_event_log (CCL_LOG_DEBUG, e);
	  ccl_debug ("' at priority level %d.\n", pl);
	}
    }
  ccl_iterator_delete (ei);
  SAS_DBG_END_TIMER ();
}

static void
s_check_ca_for_sas (ca_sas_model *csm) 
{
  s_check_ca_laws (csm);
  s_check_ca_priorities (csm);
  s_check_ca_buckets (csm);
}
