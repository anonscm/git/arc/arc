/*
 * ar-broadcast.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-bittable.h>
#include "ar-poset.h"
#include "ar-event.h"
#include "ar-broadcast.h"

struct ar_broadcast_st {
  int refcount;
  ar_idtable *events;
  ar_idtable *marks;
  int min;
  int max;
  int nb_marked;
  int maximize;
};

			/* --------------- */

static ar_identifier_array
s_get_subnodes_with_mark (ar_broadcast *bv, int mark);

ar_broadcast *
ar_broadcast_create(int maximize)
{
  ar_broadcast *result = ccl_new(ar_broadcast);

  result->events = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_identifier_add_reference,
		      (ccl_delete_proc *)ar_identifier_del_reference);
  result->refcount = 1;
  result->marks = ar_idtable_create(0, NULL,NULL);
  result->min = 0;
  result->max = 0;
  result->nb_marked = 0;
  result->maximize = maximize;

  return result;
}

			/* --------------- */

ar_broadcast *
ar_broadcast_clone(ar_broadcast *bv)
{
  ar_broadcast *result = ccl_new(ar_broadcast);

  ccl_pre( bv != NULL );

  result->events = ar_idtable_clone(bv->events);
  result->refcount = 1;
  result->marks = ar_idtable_clone(bv->marks);
  result->min = bv->min;
  result->max = bv->max;
  result->nb_marked = bv->nb_marked;
  result->maximize = bv->maximize;

  return result;
}
			/* --------------- */

int
ar_broadcast_get_size (const ar_broadcast *bv)
{
  ccl_pre (bv != NULL);

  return ar_idtable_get_size (bv->events);
}

			/* --------------- */

ar_broadcast *
ar_broadcast_add_reference(ar_broadcast *bv)
{
  ccl_pre( bv != NULL );

  bv->refcount++;

  return bv;
}

			/* --------------- */

void
ar_broadcast_del_reference(ar_broadcast *bv)
{
  ccl_pre( bv != NULL ); ccl_pre( bv->refcount > 0 );

  if( --bv->refcount == 0 )
    {
      ar_idtable_del_reference(bv->events);
      ar_idtable_del_reference(bv->marks);
      ccl_delete(bv);
    }
}

			/* --------------- */

void
ar_broadcast_set_maximize (ar_broadcast *bv, int maximize)
{
  ccl_pre (bv != NULL);

  bv->maximize = maximize;
}

			/* --------------- */

int
ar_broadcast_get_maximize (ar_broadcast *bv)
{
  ccl_pre (bv != NULL);

  return bv->maximize;
}

			/* --------------- */
void
ar_broadcast_add_event(ar_broadcast *bv, ar_identifier *event, int marked)
{
  ar_identifier *idnode;

  ccl_pre (bv != NULL); 
  ccl_pre (event != NULL);

  idnode = ar_identifier_get_prefix(event);

  if (ar_broadcast_has_subnode(bv, idnode))
    {
      ar_identifier *ev = ar_idtable_get (bv->events, idnode);

      if (ar_idtable_get (bv->marks, ev))
	bv->nb_marked--;
      ar_identifier_del_reference (ev);
    }

  ar_idtable_put(bv->events,idnode,event);
  ar_idtable_put(bv->marks,idnode, (void *) ((intptr_t) marked));
  bv->nb_marked += (marked?1:0);
  ar_identifier_del_reference(idnode);
}

			/* --------------- */

int
ar_broadcast_has_subnode(ar_broadcast *bv, const ar_identifier *subnode)
{
  ccl_pre( bv != NULL ); ccl_pre( subnode != NULL );

  return ar_idtable_has(bv->events, subnode);
}

			/* --------------- */

int
ar_broadcast_has_event (ar_broadcast *bv, ar_identifier *event)
{
  int result;
  ar_identifier *idnode;

  ccl_pre (bv != NULL);
  ccl_pre (event != NULL);
  
  idnode = ar_identifier_get_prefix (event);
  if (ar_broadcast_has_subnode (bv, idnode))
    {
      ar_identifier *id = ar_idtable_get (bv->events, idnode);
      result = (id == event);
      ar_identifier_del_reference (id);
    }
  else
    {
      result = 0;
    }
  ar_identifier_del_reference (idnode);
  
  return result;
}

			/* --------------- */

int
ar_broadcast_has_decomposed_event (ar_broadcast *bv, ar_identifier *subnode,
				   ar_identifier *event)
{
  int result;
  if (ar_broadcast_has_subnode (bv, subnode))
    {
      ar_identifier *id = ar_idtable_get (bv->events, subnode);
      result = (id == event);
      ar_identifier_del_reference (id);
    }
  else
    result = 0;
  
  return result;
}

			/* --------------- */

int 
ar_broadcast_get_nb_marked(ar_broadcast *bv)
{
  ccl_pre( bv != NULL );

  return bv->nb_marked;
}

			/* --------------- */

int 
ar_broadcast_get_nb_not_marked(ar_broadcast *bv)
{
  ccl_pre( bv != NULL );

  return ar_idtable_get_size(bv->events)-bv->nb_marked;
}

			/* --------------- */

void
ar_broadcast_set_bounds(ar_broadcast *bv, int min, int max)
{
  ccl_pre( bv != NULL );
  ccl_pre( 0 <= min && min <= max && max <= bv->nb_marked);

  bv->min = min;
  bv->max = max;
}

			/* --------------- */

void
ar_broadcast_get_bounds(ar_broadcast *bv, int *pmin, int *pmax)
{
  ccl_pre( bv != NULL );

  if( pmin != NULL )
    *pmin = bv->min;
  if( pmax != NULL )
    *pmax = bv->max;
}

			/* --------------- */

ar_identifier_iterator *
ar_broadcast_get_subnodes (ar_broadcast *bv)
{
  ccl_pre (bv != NULL);

  return ar_idtable_get_keys (bv->events);
}

			/* --------------- */

ar_identifier_array
ar_broadcast_get_marked_subnodes (ar_broadcast *bv)
{
  ccl_pre( bv != NULL );

  return s_get_subnodes_with_mark (bv, 1);
}

			/* --------------- */

ar_identifier_array
ar_broadcast_get_not_marked_subnodes (ar_broadcast *bv)
{
  ccl_pre( bv != NULL );

  return s_get_subnodes_with_mark (bv, 0);
}

			/* --------------- */

ar_identifier *
ar_broadcast_get_event(ar_broadcast *bv, ar_identifier *subnode)
{
  ccl_pre( bv != NULL ); ccl_pre( subnode != NULL );

  return ar_idtable_get(bv->events,subnode);
}

			/* --------------- */

int
ar_broadcast_is_marked (ar_broadcast *bv, ar_identifier *subnode)
{
  ccl_pre (bv != NULL);
  ccl_pre (subnode != NULL);
  ccl_pre (ar_broadcast_has_subnode (bv, subnode));

  return (intptr_t) ar_idtable_get (bv->marks, subnode);
}

			/* --------------- */

typedef struct stack_element_st {
  ccl_list *labels;
  ccl_bittable *set;
  struct stack_element_st *next;
  struct stack_element_st *prev;
} stack_element;

			/* --------------- */

static void
s_delete_identifier_list(void *l)
{
  ccl_list_clear_and_delete (l, 
			     (ccl_delete_proc *) ar_identifier_del_reference);
}

			/* --------------- */

static unsigned int
s_hash_lists (const void *l)
{
  return ccl_list_hash (l);
}

static int
s_cmp_lists (const void *l1, const void *l2)
{
  return ccl_list_compare (l1, l2, NULL);
}

			/* --------------- */

static void
s_compute_instance_vectors (ar_poset *result, ar_broadcast *bv)
{
  ccl_list *stack = ccl_list_create();
  stack_element *levels = ccl_new_array(stack_element,bv->nb_marked+1);
  stack_element *se;
  ccl_list *mandatory_events = ccl_list_create();
  ar_identifier **optional_events = 
    ccl_new_array(ar_identifier *,bv->nb_marked + 1);

  {
    int k = 0;
    ar_identifier_iterator *ii = ar_idtable_get_keys (bv->events);

    while (ccl_iterator_has_more_elements (ii))
      {
	ar_identifier *subnode = ccl_iterator_next_element (ii);
	ar_identifier *event = ar_idtable_get (bv->events, subnode);

	if( (intptr_t) ar_idtable_get (bv->marks, subnode) )
	  {
	    levels[k].next = &levels[k];
	    levels[k].prev = &levels[k];
	    optional_events[k] = event;
	    k++;
	  }
	else
	  ccl_list_add (mandatory_events, event);
      }
    ccl_iterator_delete (ii);
    levels[k].next = &levels[k];
    levels[k].prev = &levels[k];
  }

  ccl_assert (ccl_list_get_size (mandatory_events) ==
	      ar_broadcast_get_nb_not_marked (bv));

  se = ccl_new(stack_element);
  se->labels = mandatory_events;
  se->set = ccl_bittable_create(bv->nb_marked + 1);
  se->next = &(levels[0]);
  se->prev = &(levels[0]);
  if( bv->min == 0 )
    ar_poset_add(result,se->labels);
  ccl_list_add(stack,se);

  while( ! ccl_list_is_empty(stack) )
    {
      stack_element *se = (stack_element *)ccl_list_take_first(stack);
      int size = ccl_bittable_get_nb_one(se->set);

      ccl_assert( size <= bv->max );

      if( size < bv->max )
	{
	  int i;
	  stack_element *nse;
	  ccl_bittable *bt;

	  for(i = 0; i < bv->nb_marked; i++)
	    {
	      if( ccl_bittable_has(se->set,i) )
		continue;

	      bt = ccl_bittable_dup(se->set);
	      ccl_bittable_set(bt,i);

	      for(nse = levels[size+1].next; nse != &levels[size+1]; 
		  nse = nse->next)
		{
		  if( ccl_bittable_equals(nse->set,bt) )
		    break;
		}

	      if( nse == &levels[size+1] )
		{
		  ar_identifier *label = 
		    ar_identifier_add_reference(optional_events[i]);

		  nse = ccl_new(stack_element);
		  nse->labels = 
		    ccl_list_deep_dup(se->labels,(ccl_duplicate_func *)
				      ar_identifier_add_reference);
		  ccl_assert( ! ccl_list_has(nse->labels,optional_events[i]) );

		  ccl_list_insert(nse->labels,label,
				  (ccl_compare_func *)ar_identifier_compare);

		  nse->set = bt;

		  nse->next = levels[size+1].next;
		  nse->prev = &(levels[size+1]);
		  levels[size+1].next = nse;
		  nse->next->prev = nse;

		  if( size+1 >= bv->min )
		    ar_poset_add(result,nse->labels);

		  ccl_list_add(stack,nse);
		}
	      else
		{
		  ccl_bittable_delete(bt);
		}

	      if( size >= bv->min )
		{
		  ar_poset_add_pair_no_check(result,nse->labels,se->labels);
		}

	    }	  
	}

      if( size < bv->min )
	ccl_list_clear_and_delete(se->labels,(ccl_delete_proc *)
				  ar_identifier_del_reference);
      se->prev->next = se->next;
      se->next->prev = se->prev;
      ccl_bittable_delete(se->set);
      ccl_delete(se);
    }
  ccl_list_delete(stack);
  ccl_delete(levels);

  {
    int i;
    for(i = 0; i < bv->nb_marked; i++)
      {
	ar_identifier_del_reference(optional_events[i]);
      }
    ccl_delete(optional_events);
  }

  if( ! bv->maximize )
    ar_poset_reverse(result);
}

ar_poset *
ar_broadcast_get_instance_vectors (ar_broadcast *bv)
{
  ar_poset *result = 
    ar_poset_create (s_hash_lists, NULL, (ccl_compare_func *) s_cmp_lists,
		     (ccl_delete_proc *) s_delete_identifier_list);

  if (bv->nb_marked != 0)  
    s_compute_instance_vectors (result, bv);
  else
    {
      ccl_list *mandatory_events = 
	ar_broadcast_get_instance_vector (bv);
      ar_poset_add (result,mandatory_events);
      ccl_assert (ccl_list_get_size (mandatory_events) == 
		  ar_idtable_get_size (bv->marks));
    }
  
  return result;
}

ccl_list *
ar_broadcast_get_instance_vector (ar_broadcast *bv)
{
  ar_identifier_iterator *ii = ar_idtable_get_keys (bv->events);
  ccl_list *result = ccl_list_create ();

  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *n = ccl_iterator_next_element (ii);
      ccl_list_add (result, ar_idtable_get (bv->events, n));
      ar_identifier_del_reference (n);
    }
  
  ccl_pre (bv->nb_marked == 0);
  ccl_iterator_delete (ii);
      
  return result;
}


			/* --------------- */

void
ar_broadcast_log(ccl_log_type log, ar_broadcast *bv)
{
  int first = 1;
  ccl_list *keys = ar_idtable_get_list_of_keys (bv->events);

  ccl_list_sort (keys, (ccl_compare_func *) ar_identifier_lex_compare);
  
  ccl_log(log,"<");
  while (! ccl_list_is_empty (keys))
    {
      ar_identifier *name;
      ar_identifier *subnode = ccl_list_take_first (keys);
      ar_identifier *eventname = 
	(ar_identifier *) ar_idtable_get (bv->events, subnode);
      ar_identifier_del_reference (subnode);
      name = ar_identifier_get_name_and_prefix (eventname, &subnode);
      ar_identifier_del_reference(eventname);
      
      if (name != AR_EPSILON_ID)
	{
	  if (! first)
	    ccl_log (log, ", ");
	  else 
	    first = 0;

	  if (subnode != AR_EMPTY_ID)
	    {
	      ar_identifier_log (log, subnode);
	      ccl_log (log, ".");
	    }
	  ar_identifier_log_quote_gen (log, name, ".", "''"); 

	  if( (intptr_t) ar_idtable_get (bv->marks,subnode) )
	    ccl_log(log,"?");
	}
      ar_identifier_del_reference (name);
      ar_identifier_del_reference(subnode);
    }
  ccl_list_delete (keys);
  ccl_log(log,">");

  if( bv->nb_marked > 0 ) 
    {
      if( bv->min == bv->max ) 
	ccl_log(log," = %d",bv->max);
      else if( bv->max == bv->nb_marked )
	{ 
	  if( bv->min > 0 ) 
	    ccl_log(log," >= %d",bv->min);
	}
      else
	{
	  ccl_log(log," <= %d",bv->max);
	}
    }

  if( ! bv->maximize  )
    ccl_log(log," min");
}

static ar_identifier_array
s_get_subnodes_with_mark(ar_broadcast *bv, int mark)
{
  int i = 0;
  int sz = ar_idtable_get_size(bv->events);
  int size = mark?bv->nb_marked:sz-bv->nb_marked;
  ar_identifier_array result;
  ar_identifier_iterator *it = ar_idtable_get_keys(bv->events);

  ccl_array_init_with_size (result, size);
  while( ccl_iterator_has_more_elements(it) )
    {
      ar_identifier *id = ccl_iterator_next_element(it);
      int marked = (intptr_t) ar_idtable_get (bv->marks, id);

      if ((marked && mark) || (!marked && !mark))
	result.data[i++] = ar_identifier_add_reference(id);
      ar_identifier_del_reference(id);
    }
  ccl_iterator_delete(it);

  ccl_assert (i == result.size);

  return result;
}


