/*
 * ar-sync.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_SYNC_H
# define AR_SYNC_H

# include "ar-event.h"

typedef struct ar_sync_st *ar_sync;

#endif /* ! AR_SYNC_H */
