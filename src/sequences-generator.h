/*
 * sequences-generator.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef SEQUENCES_GENERATOR_H
# define SEQUENCES_GENERATOR_H

# include <ccl/ccl-set.h>
# include "ar-constraint-automaton.h"

extern void
ar_orderered_cuts_generator (ar_ca *observer, ar_ca_expr *acceptance, 
			     ccl_set *visible_tags, ccl_set *disabled_tags,
			     int bound, int mincuts, int boolean_formula,
			     ccl_log_type log, const char *prefix);

/*!
 * \brief Minimal cuts generator for a given Boolean objective.
 * 
 * This function outputs on 'log' the set of cuts (or minimal cuts if 'mincuts'
 * is true) that yield the automaton 'observer' in a configuration that 
 * satisfies the Boolean condition 'acceptance'. 
 *
 * The algorithm is based on the one proposed by A. Rauzy for mode-automata.
 * This version is based on a symbolic computation of reachable configurations.
 * The automaton is decorated with new Boolean variables. Each variable 
 * corresponds to an elementary event that has been marked with the attribute 
 * 'visible_tag' in the model.
 * 
 * If cuts are generated (i.e mincuts = false) a Boolean formula is produced on
 * the stream 'log'. The output format is the one of ARALIA i.e. a set of 
 * equations: x_i := f. The whole formula is defined by the equation for the
 * variable named 'root'.
 * 
 * If minimal cuts are requested (i.e mincuts = true) a list of vectors (e_1, 
 * ..., e_n) is produced; each vector represents a minimal cut. 
 *
 * Parameters:
 *   ca          : the studied constraint automaton
 *   acceptance  : the Boolean condition that one wants to reach
 *   visible_tags  : attributes used to identify visible events.
 *   disabled_tags : attributes used to identify disabled events.
 *   mincuts       : true if minimal cuts are requested.
 *   log           : the output stream.
 */
extern void
ar_cuts_generator (ar_ca *observer, ar_ca_expr *acceptance, 
		   ccl_set *visible_tags, ccl_set *disabled_tags,
		   int mincuts, int boolean_formula, ccl_log_type log,
		   const char *prefix);

#endif /* ! SEQUENCES_GENERATOR_H */
