/*
 * ctlstar.h -- translation of CTL* formulas into MEC 5 equation systemts
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

/*!
 * \brief translation of CTL* formulas into MEC 5 equation systemts
 * 
 * This module implements the algorithnm of Mads Dam for the translation of 
 * CTL* formulas into mu-calculus formulas:
 *
 * Dam, M. "CTL * and ECTL* as fragments of the modal \mu-calculs", Theoretical
 * Computer Science 126, pp 77-96, 1994
 */

#ifndef CTLSTAR_H
# define CTLSTAR_H

# include <ar-identifier.h>
# include "parser/altarica-tree.h"

typedef struct ctlstar_formula ctlstar_formula;

enum ctlstar_formula_kind_enum 
  {
    CTLSTAR_TRUE,
    CTLSTAR_VAR,
    CTLSTAR_NOT,
    CTLSTAR_AND,
    CTLSTAR_OR,
    CTLSTAR_NOT_U,
    CTLSTAR_U, /* don't change ordering of U and X. 
		  used by s_is_simple_node. */ 
    CTLSTAR_X,
    CTLSTAR_E,
    FXP_MU,
    FXP_NU,
    FXP_EX
  };

typedef enum ctlstar_formula_kind_enum ctlstar_formula_kind;

extern int 
ctlstar_init (void);

extern int 
ctlstar_terminate (void);

extern ctlstar_formula *
ctlstar_formula_crt_true (void);

extern ctlstar_formula *
ctlstar_formula_crt_variable (ar_identifier *X);

extern ctlstar_formula *
ctlstar_formula_crt_dummy_variable (ar_identifier **pvarname);

extern ctlstar_formula *
ctlstar_formula_crt_not (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_and (ctlstar_formula *F1, ctlstar_formula *F2);

extern ctlstar_formula *
ctlstar_formula_crt_or (ctlstar_formula *F1, ctlstar_formula *F2);

extern ctlstar_formula *
ctlstar_formula_crt_X (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_U (ctlstar_formula *F1, ctlstar_formula *F2);

extern ctlstar_formula *
ctlstar_formula_crt_E (ctlstar_formula *F);

extern void
ctlstar_formula_log (ccl_log_type log, ctlstar_formula *F, 
		     void (*logid) (ccl_log_type log, ar_identifier *id, 
				    void *data),
		     void *data);

extern void
ctlstar_formula_log_as_dot (ccl_log_type log, ctlstar_formula *F,
			    void (*logid) (ccl_log_type log, ar_identifier *id, 
					   void *data),
			    void *data);


extern ctlstar_formula *
ctlstar_formula_add_reference (ctlstar_formula *F);

extern void
ctlstar_formula_del_reference (ctlstar_formula *F);

extern int
ctlstar_formula_is_state_formula (const ctlstar_formula *F);

extern ctlstar_formula_kind 
ctlstar_formula_get_kind (const ctlstar_formula *F);

extern ctlstar_formula **
ctlstar_formula_get_operands (const ctlstar_formula *F);

extern int
ctlstar_formula_get_arity (const ctlstar_formula *F);

extern ar_identifier *
ctlstar_formula_get_name (const ctlstar_formula *F);

/* shortcuts */

extern ctlstar_formula *
ctlstar_formula_crt_false (void);

extern ctlstar_formula *
ctlstar_formula_crt_imply (ctlstar_formula *F1, ctlstar_formula *F2);

extern ctlstar_formula *
ctlstar_formula_crt_equiv (ctlstar_formula *F1, ctlstar_formula *F2);

extern ctlstar_formula *
ctlstar_formula_crt_xor (ctlstar_formula *F1, ctlstar_formula *F2);

extern ctlstar_formula *
ctlstar_formula_crt_AX (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_AF (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_AG (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_EX (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_EF (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_EG (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_A (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_F (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_G (ctlstar_formula *F);

extern ctlstar_formula *
ctlstar_formula_crt_W (ctlstar_formula *F1, ctlstar_formula *F2);

			/* --------------- */

extern altarica_tree *
ctlstar_compute_equation_system (ctlstar_formula *F, 
				 int (*is_constant_set)(ar_identifier *id, 
							void *data),
				 void *data,
				 ar_identifier *nodeid,
				 ar_identifier *Rname);

#endif /* ! CTLSTAR_H */
