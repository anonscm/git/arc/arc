/*
 * sequences-generator.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include "ar-attributes.h"
#include "sequences-generator.h"
#include "sequences-generator-p.h"

#define SKIP if(0) 

#define COUNTER_ATTRIBUTE "counter"
#define SUFFIX_FOR_COUNTER_VARIABLES "$c"

static ar_identifier *
s_generate_counter_variable_name (ar_identifier *varname);

			/* --------------- */

static ar_ca_expr *
s_get_flow_variable (ar_ca *observer, ar_identifier *varname)
{
  const ccl_pair *p;
  const ccl_list *flows = ar_ca_get_flow_variables(observer);
  ar_ca_expr *result = NULL;

  CCL_LIST_FOREACH (p, flows)
    {
      ar_identifier *id = ar_ca_expr_variable_get_name (CAR (p));

      if (id == varname)
	result = ar_ca_expr_add_reference (CAR (p));
      ar_identifier_del_reference (id);
      if (result != NULL)
	break;
    }
  ccl_assert (result != NULL);

  return result;
}


			/* --------------- */

static ar_identifier *
s_generate_counter_variable_name (ar_identifier *varname)
{
  return ar_identifier_rename_with_suffix (varname, 
					   SUFFIX_FOR_COUNTER_VARIABLES);
}

			/* --------------- */

static ar_ca_expr *
s_add_counter_variable (ar_ca *observer, ar_identifier *varname)
{
  ar_ca_exprman *eman = ar_ca_get_expression_manager (observer);
  ar_identifier *cname = s_generate_counter_variable_name (varname);
  ar_ca_expr *flow_var = s_get_flow_variable (observer, varname);
  ar_ca_domain *dom = ar_ca_expr_variable_get_not_const_domain (flow_var);
  ar_ca_expr *result = ar_ca_expr_crt_variable (eman, cname, dom, NULL);
  ar_ca_expr *zero = ar_ca_expr_crt_integer_constant (eman, 0);
  ar_ca_expr *a = ar_ca_expr_crt_eq (flow_var, result);

  ar_ca_add_state_variable (observer, result);
  ar_ca_add_initial_assignment (observer, result, zero);
  ar_ca_add_assertion (observer, a);

  ar_ca_expr_del_reference (a);
  ar_ca_expr_del_reference (zero);
  ar_ca_expr_del_reference (flow_var);
  ar_identifier_del_reference (cname);
  ar_ca_domain_del_reference (dom);
  ar_ca_exprman_del_reference (eman); 

  return result;
}

			/* --------------- */

static void
s_decorate_transitions_with_counter (ar_ca *observer, ar_identifier *attr)
{
  int i;
  ar_ca_exprman *eman = ar_ca_get_expression_manager (observer);
  int nbT = ar_ca_get_number_of_transitions (observer);
  ar_ca_expr *var = NULL;
  ar_ca_expr *inc = NULL;
  ar_ca_trans **trans = ar_ca_get_transitions (observer);

  for (i = 0; i < nbT; i++)
    {      
      ar_ca_event *e = ar_ca_trans_get_event (trans[i]);

      if (ar_ca_event_has_attribute (e, attr))
	{
	  if (var == NULL)
	    {
	      ar_ca_expr *one = ar_ca_expr_crt_integer_constant (eman, 1);
	      var = s_add_counter_variable (observer, attr);
	      inc = ar_ca_expr_crt_add (var, one);
	      ar_ca_expr_del_reference (one);
	    }
	  ccl_assert (var != NULL && inc != NULL);
	  ar_ca_trans_add_assignment (trans[i], var, inc);
	}
      
      ar_ca_event_del_reference (e);
    }

  if (var == NULL)
    {
      ccl_warning ("warning: attribute '");
      ar_identifier_log (CCL_LOG_WARNING, attr);
      ccl_warning ("' is never used.\n");
    }
  else
    {
      ar_ca_expr_del_reference (inc);
      ar_ca_expr_del_reference (var);
    }
  ar_ca_exprman_del_reference (eman);
}

			/* --------------- */

ccl_list *
sgen_list_of_counters_identifiers (ar_ca *ca)
{
  ccl_list *result = ccl_list_create ();
  ar_identifier *counter_attr = ar_identifier_create (COUNTER_ATTRIBUTE);
  const ccl_list *vars = ar_ca_get_flow_variables (ca);
  const ccl_pair *p;

  CCL_LIST_FOREACH (p, vars)
    {
      ccl_list *attrs = ar_ca_expr_variable_get_attributes (CAR (p));
      if (ccl_list_has (attrs, counter_attr))
	{
	  ar_identifier *varname = ar_ca_expr_variable_get_name (CAR (p));
	  ccl_list_add (result, varname);
	}
    }
  ar_identifier_del_reference (counter_attr);

  return result;
}

			/* --------------- */

void
ar_decorate_observer (ar_ca *observer, ccl_list *result)
{
  ccl_list *counters = sgen_list_of_counters_identifiers (observer);

  while (! ccl_list_is_empty (counters))
    {
      ar_identifier *c = ccl_list_take_first (counters);
      s_decorate_transitions_with_counter (observer, c);
      if (result != NULL)
	{
	  ar_identifier *cname = s_generate_counter_variable_name (c);
	  ccl_list_add (result, cname);
	}
      ar_identifier_del_reference (c);
    }
  ccl_list_delete (counters);
}

			/* --------------- */


