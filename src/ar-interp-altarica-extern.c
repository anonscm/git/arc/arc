/*
 * ar-interp-altarica-extern.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */
#include <ccl/ccl-string.h>
#include <sas/sas-params.h>
#include "ar-model.h"
#include "ar-interp-altarica-p.h"

struct law_interp 
{
  const char *lawid;
  sas_law * (*interp) (altarica_tree *args, ar_node *node);
};

#define SAS_LAW(id) \
  static sas_law *s_interp_ ## id (altarica_tree *args, ar_node *node);
SAS_LAWS
#undef SAS_LAW

#define SAS_LAW(id) \
  { # id, s_interp_ ## id }, 

static struct law_interp LAW_INTERPS[] = {
  SAS_LAWS
  { NULL, NULL}
};
#undef SAS_LAWS

static ar_context *
s_ctx (ar_node *node)
{
  if (node)
    return ar_node_get_context (node);
  return ar_context_add_reference (AR_MODEL_CONTEXT);
}

static ar_identifier * 
s_interp_law_param_id (ar_node *node, altarica_tree *path, int *p_exists)
{
  ar_context *ctx = s_ctx (node);
  ar_identifier *result = NULL;
  
  ccl_try (altarica_interpretation_exception)
    {
      result = ar_interp_identifier_path (path, ctx);
    }
  ccl_no_catch;
  ar_context_del_reference (ctx);
  
  if (result != NULL)
    {
      int exists = 0;
      
      if (ar_identifier_get_path_length (result) == 1)
	exists = ar_context_has_law_parameter (ctx, result);
      else if (node != NULL)
	{	  
	  ar_identifier *nodename;
	  ar_identifier *paramid =
	    ar_identifier_get_name_and_prefix (result, &nodename);
	  ar_node *sn = ar_node_get_subnode_model (node, nodename);
	  if (sn == NULL)
	    {
	      ar_error (path, "undefined subnode '");
	      ar_identifier_log (CCL_LOG_ERROR, nodename);
	      ccl_error ("'.\n");
	      ar_identifier_del_reference (paramid);
	      INTERP_EXCEPTION ();
	    }
	  exists = ar_node_has_law_parameter (sn, paramid);
	  ar_identifier_del_reference (nodename);
	  ar_identifier_del_reference (paramid);
	  ar_node_del_reference (sn);
	}
      *p_exists = exists;	
    }
  else
    {
      ccl_rethrow ();
    }
  
  return result;
}

/*!
 * Syntax: 
 * param ::= float
 *       ::= hierarchy-path
 *       ::= RNG-identifier (param_1, ..., param_n) 
 * with n = 2 and RNG-identifier in { 'uniform', 'normal', 'lognormal' }
 */
static sas_law_param *
s_interp_extern_parameter_value (altarica_tree *t, ar_node *node)
{  
  sas_law_param *result = NULL;
  
  if (IS_LABELLED_BY (t, FLOAT))
    result = sas_law_param_crt_constant (t->value.flt_value);
  else if (IS_LABELLED_BY (t, INTEGER))
    result = sas_law_param_crt_constant ((sas_double) t->value.int_value);
  else if (IS_LABELLED_BY (t, IDENTIFIER_PATH))
    {
      int exists = 0;
      ar_identifier *id = s_interp_law_param_id (node, t, &exists);
      if (exists)
	result = sas_law_param_crt_variable (id);
      else
	{
	  ar_error (t, "unknown law parameter '");
	  ar_identifier_log (CCL_LOG_ERROR, id);
	  ccl_error ("'.\n");
	}
      ar_identifier_del_reference (id);
    }
  else if (IS_LABELLED_BY (t, EXTERN_TERM_FUNCTION))
    {
      const char *rng;
      sas_law_param * (*cstr) (sas_law_param *, sas_law_param *) = NULL;

      ccl_pre (IS_LABELLED_BY (t->child, IDENTIFIER));
      rng = t->child->value.id_value;
      if (ccl_string_equals (rng, "uniform"))
	cstr = sas_law_param_crt_uniform;
      else if (ccl_string_equals (rng, "normal"))
	cstr = sas_law_param_crt_normal;
      else if (ccl_string_equals (rng, "lognormal"))
	cstr = sas_law_param_crt_lognormal;
      else
	ar_error (t, "unknown random-number '%s'.\n", rng);
      if (cstr != NULL)
	{
	  sas_law_param *p1 = NULL;
	  sas_law_param *p2 = NULL;

	  if (ccl_parse_tree_count_siblings (t->child) != 3)
	    ar_error (t, "wrong number of arguments for '%s'.\n", rng);
	  else
	    {
	      altarica_tree *tp1 = t->child->next;
	      altarica_tree *tp2 = tp1->next;
	      ccl_try (altarica_interpretation_exception)
	        {		  
		  p1 = s_interp_extern_parameter_value (tp1, node);
		  p2 = s_interp_extern_parameter_value (tp2, node);
		  result = cstr (p1, p2);
		}
	      ccl_no_catch;
	      ccl_zdelete (sas_law_param_del_reference, p1);
	      ccl_zdelete (sas_law_param_del_reference, p2);
	    }
	}
    }
  else
    {
      ar_error (t, "syntax error in law parameter specification.\n");
    }
  
  if (result == NULL)
    INTERP_EXCEPTION ();
  
  return result;
}

/*!
 * Syntax: 
 *  parameter hierarchy-path  = param
 */
static void
s_interp_extern_parameter_decl (altarica_tree *lval, altarica_tree *rval,
				ar_node *node)
{
  int exists;
  ar_identifier *paramid;

  if (! IS_LABELLED_BY (lval, IDENTIFIER_PATH))
    {
      ar_error (lval, "bad identifier for law parameter definition.\n");
      INTERP_EXCEPTION ();
    }
  exists = 0; 
  paramid = s_interp_law_param_id (node, lval, &exists);

  if ((ar_identifier_get_path_length (paramid) == 1 && exists) ||
      (node != NULL && ar_node_has_law_parameter (node, paramid)))
    {
      ar_error (lval, "law parameter redefinition.\n");
      INTERP_EXCEPTION ();
    }
  
  if (ar_identifier_get_path_length (paramid) > 1 && node == NULL)
    {
      ar_error (lval, "identifier path not allowed out of nodes for law "
		"parameter definition.\n");
      INTERP_EXCEPTION ();
    }
  
  ccl_try (altarica_interpretation_exception)
    {
      sas_law_param *val = s_interp_extern_parameter_value (rval, node);
      if (node == NULL)
	ar_model_set_law_parameter (paramid, val);
      else
	ar_node_add_law_parameter (node, paramid, val);
      sas_law_param_del_reference (val);
    }
  ccl_catch
    {
      ar_identifier_del_reference (paramid);
      ccl_rethrow ();
    }
  ccl_end_try;
  ar_identifier_del_reference (paramid);
}

static void
s_interp_events (altarica_tree *events, ar_context *ctx, ccl_list *result)
{
  if (IS_LABELLED_BY (events, EXTERN_TERM_SET))
    {
      altarica_tree *e;
      
      for (e = events->child; e != NULL; e = e->next)
	s_interp_events (e, ctx, result);	  
    }
  else if (IS_LABELLED_BY (events, EXTERN_TERM_EVENT))
    {
      ar_identifier *id = ar_interp_identifier_path (events->child, ctx);
      ccl_list_add (result, id);
    }
  else
    {
      ar_error (events, "invalid event specification.\n");
      INTERP_EXCEPTION ();
    }
}

static sas_law *
s_interp_extern_law (altarica_tree *law, ar_node *node)
{
  const char *lstr;
  struct law_interp *li;
  altarica_tree *args;
  sas_law *result = NULL;
    
  ccl_pre (IS_LABELLED_BY (law, EXTERN_TERM_FUNCTION));  
  ccl_pre (IS_LABELLED_BY (law->child, IDENTIFIER));
  lstr = law->child->value.id_value;
  args = law->child->next;
  ccl_pre (args != NULL);
  
  for (li = LAW_INTERPS; li->lawid != NULL && result == NULL; li++)
    {
      if (ccl_string_equals (lstr, li->lawid))
	result = li->interp (args, node);
    }

  if (result == NULL)
    {
      ar_error (law, "unknown law '%s'.\n", lstr);
      INTERP_EXCEPTION ();
    }
  
  return result;
}

static void
s_interp_extern_law_decl (altarica_tree *lval, altarica_tree *rval, 
			  ar_node *node)
{
  int err = 0;
  ccl_list *events = ccl_list_create ();
  ar_context *ctx = ar_node_get_context (node);
  
  ccl_try (altarica_interpretation_exception)
    {
      sas_law *law;
      s_interp_events (lval, ctx, events);
      law = s_interp_extern_law (rval, node);
      
      while (! ccl_list_is_empty (events))
	{
	  ar_identifier *id = ccl_list_take_first (events);
	  if (ar_node_has_law (node, id))
	    {
	      ar_warning (lval, "event law redefinition '");
	      ar_identifier_log (CCL_LOG_WARNING, id);
	      ccl_warning ("'.\n");
	    }
	  ar_node_add_law (node, id, law);
	  ar_identifier_del_reference (id);
	}
      sas_law_del_reference (law);
    }
  ccl_catch
    {
      err = 1;
    }
  ccl_end_try;
  
  ccl_list_clear_and_delete (events, (ccl_delete_proc *)
			     ar_identifier_del_reference);
  ar_context_del_reference (ctx);
  if (err)
    ccl_rethrow ();
}

static void
s_interp_extern_observer_decl (altarica_tree *lval, altarica_tree *rval, 
			       ar_node *node, int is_boolean)
{
  ar_expr *obsval = NULL;
  ar_identifier *obsid = NULL;
  ar_context *ctx;

  if (! IS_LABELLED_BY (lval, IDENTIFIER_PATH))
    {
      ar_error (lval, "bad identifier for observer definition.\n");
      INTERP_EXCEPTION ();
    }
  if (! IS_LABELLED_BY (rval, EXTERN_TERM_EXPR))
    {
      ar_error (rval, "bad value for observer definition.\n");
      INTERP_EXCEPTION ();
    }

  ctx = ar_node_get_context (node); 
  ccl_try (altarica_interpretation_exception)
    {
      ar_type *expected_type = is_boolean ? AR_BOOLEANS : AR_INTEGERS;
      obsid = ar_interp_identifier_path (lval, ctx);
      obsval = ar_interp_expression(rval->child, expected_type, ctx,
				    ar_interp_acheck_member_access);
      ar_node_add_observer (node, obsid, obsval);
      ar_expr_del_reference (obsval);
    }
  ccl_no_catch;

  ar_context_del_reference (ctx);
  ccl_zdelete (ar_identifier_del_reference, obsid);
  if (obsval == NULL)
    ccl_rethrow ();
}

static void
s_interp_extern_preemptible_decl (altarica_tree *lval, altarica_tree *rval, 
				  ar_node *node, int is_boolean)
{
  int err = 1;
  ccl_list *events;
  
  ar_context *ctx;
  if (rval != NULL)
    {
      ar_error (lval, "bad preemptible specification.\n");
      INTERP_EXCEPTION ();
    }
    
  ctx = ar_node_get_context (node);
  events = ccl_list_create ();  
  ccl_try (altarica_interpretation_exception)
    {
      ccl_pair *p;
      
      s_interp_events (lval, ctx, events);
      for (p = FIRST (events); p; p = CDR (p))
	ar_node_add_preemptible (node, CAR (p));
      err = 0;
    }
  ccl_no_catch;

  ar_context_del_reference (ctx);
  ccl_list_clear_and_delete (events,
			     (ccl_delete_proc *) ar_identifier_del_reference);
  if (err)
    ccl_rethrow ();
}

static void
s_interp_extern_bucket_decl (altarica_tree *lval, altarica_tree *rval, 
			     ar_node *node)
{
  sas_double proba = 0.0;
  ccl_list *events;
  ccl_list *params;
  int err = 1;
  ar_context *ctx;
  ar_identifier *bid = NULL;
  
  if (! IS_LABELLED_BY (lval, IDENTIFIER_PATH))
    {
      ar_error (lval, "bad identifier for bucket definition.\n");
      INTERP_EXCEPTION ();
    }
  
  if (! IS_LABELLED_BY (rval, EXTERN_TERM_SET))
    {
      ar_error (rval, "bad set for bucket definition.\n");
      INTERP_EXCEPTION ();
    }
  
  ctx = ar_node_get_context (node);
  events = ccl_list_create ();
  params = ccl_list_create ();
    
  ccl_try (altarica_interpretation_exception)
    {
      altarica_tree *t = rval->child;
      bid = ar_interp_identifier_path (lval, ctx);
      
      while (t) 
	{
	  if (IS_LABELLED_BY (t, EXTERN_TERM_EVENT))
	    {
	      ar_identifier *id = ar_interp_identifier_path (t->child, ctx);
	      
	      ccl_list_add (events, id);
	      if (ar_identifier_get_path_length (id) > 1)
		{
		  ar_error (t, "buckets are allowed only for local events: '");
		  ar_identifier_log (CCL_LOG_ERROR, id);
		  ccl_error ("'.\n");
		  INTERP_EXCEPTION ();      
		}

	      if (! ar_node_has_event (node, id))
		{
		  ar_error (t, "unknown event '");
		  ar_identifier_log (CCL_LOG_ERROR, id);
		  ccl_error ("' in bucket specification.\n");
		  INTERP_EXCEPTION ();      
		}

	      if (ar_node_is_in_bucket (node, id))
		{
		  ar_error (t, "event '");
		  ar_identifier_log (CCL_LOG_ERROR, id);
		  ccl_error ("' is already attached to a bucket "
			     "specification.\n");
		  INTERP_EXCEPTION ();      
		}
	    }
	  else
	    {
	      ar_error (t, "invalid bucket specification.\n");
	      INTERP_EXCEPTION ();      
	    }
	  t = t->next;
	  if (t != NULL)
	    {
	      sas_double p;
	      sas_law_param *param = 
		s_interp_extern_parameter_value (t, node);
	      ccl_list_add (params, param);	      
	      if (sas_law_param_is_constant (param, &p))
		{
		  proba += p;
		  if (proba > 1.0)
		    {
		      ar_error (t, "probability is > 1 in bucket "
				"specification.\n");
		      INTERP_EXCEPTION ();
		    }
		}
	      else
		{
		  ar_error (t, "non constant parameters are not allowed in "
			    "bucket specification.\n");
		  INTERP_EXCEPTION ();      		  
		}
	      t = t->next;
	      if (t == NULL)
		{
		  ar_error (t, "an event is missing in "
			    "bucket specification.\n");
		  INTERP_EXCEPTION ();      
		}		
	    }
	}
      ar_node_add_bucket (node, bid, events, params);
      
      err = 0;
    }
  ccl_no_catch;

  ccl_zdelete (ar_identifier_del_reference, bid);
  ar_context_del_reference (ctx);
  ccl_list_clear_and_delete (events,
			     (ccl_delete_proc *) ar_identifier_del_reference);
  ccl_list_clear_and_delete (params,
			     (ccl_delete_proc *) sas_law_param_del_reference);
  if (err)
    ccl_rethrow ();
}

static void
s_interp_extern_priority_decl (altarica_tree *lval, altarica_tree *rval, 
			       ar_node *node)
{
  ccl_list *events;
  int err = 1;
  ar_context *ctx;
  
  if (! IS_LABELLED_BY (rval, INTEGER))
    {
      ar_error (rval, "bad priority level.\n");
      INTERP_EXCEPTION ();
    }
  
  ctx = ar_node_get_context (node);
  events = ccl_list_create ();
    
  ccl_try (altarica_interpretation_exception)
    {      
      int priority_level = rval->value.int_value;

      if (priority_level < 0)
	{
	  ar_error (rval, "priority must be >= 0; here %d is specified.\n",
		    priority_level);
	  INTERP_EXCEPTION ();
	}
      
      s_interp_events (lval, ctx, events);
      while (! ccl_list_is_empty (events))
	{
	  ar_identifier *id = ccl_list_take_first (events);	  
	  ar_node_add_priority (node, id, priority_level);
	  ar_identifier_del_reference (id);
	}
      err = 0;
    }
  ccl_no_catch;

  ar_context_del_reference (ctx);
  ccl_list_clear_and_delete (events,
			     (ccl_delete_proc *) ar_identifier_del_reference);
  if (err)
    ccl_rethrow ();
}

static void
s_interp_extern_decl (altarica_tree *t, ar_node *node)
{
  int ignored = 0;
  altarica_tree *id;
  altarica_tree *lval;
  altarica_tree *rval;
  
  ccl_pre (IS_LABELLED_BY (t, EXTERN_DIRECTIVE));
  id = t->child;
  ccl_pre (IS_LABELLED_BY (id, IDENTIFIER));
  lval = id->next;
  rval = lval->next;

  if (node == NULL && ! ccl_string_equals (id->value.id_value, "parameter"))
    ignored = 1;
  else if (ccl_string_equals (id->value.id_value, "preemptible"))
    s_interp_extern_preemptible_decl (lval, rval, node, 0);
  else if (rval == NULL)
    ignored = 1;
  else if (ccl_string_equals (id->value.id_value, "parameter"))
    s_interp_extern_parameter_decl (lval, rval, node);
  else if (ccl_string_equals (id->value.id_value, "law"))
    s_interp_extern_law_decl (lval, rval, node);
  else if (ccl_string_equals (id->value.id_value, "predicate"))
    s_interp_extern_observer_decl (lval, rval, node, 1);
  else if (ccl_string_equals (id->value.id_value, "property"))
    s_interp_extern_observer_decl (lval, rval, node, 0);  
  else if (ccl_string_equals (id->value.id_value, "bucket"))
    s_interp_extern_bucket_decl (lval, rval, node);
  else if (ccl_string_equals (id->value.id_value, "priority"))
    s_interp_extern_priority_decl (lval, rval, node);
  else
    ignored = 1;
  
  if (ignored)
    ar_warning (t, "extern directive ignored.\n");
}

void
ar_interp_extern_decl (altarica_tree *t, ar_node *node)
{
  ccl_pre (IS_LABELLED_BY (t, EXTERN_DECL));

  for (t = t->child; t; t = t->next)
    s_interp_extern_decl (t, node);
}

/******************************************************************************
 *
 * INTERPRETERS FOR LAWS
 *
 ******************************************************************************/

static ccl_list *
s_interp_list_of_params (altarica_tree *args, ar_node *node, int arity)
{
  ccl_list *result = ccl_list_create ();

  ccl_try (altarica_interpretation_exception)
    {
      altarica_tree *a;
      for (a = args; a != NULL; a = a->next)
	{
	  sas_law_param *p = s_interp_extern_parameter_value (a, node);
	  ccl_list_add (result, p);
	}
      if (arity >= 0 && ccl_list_get_size (result) != arity)
	{
	  ar_error (args, "wrong number of law parameters.\n");
	  ccl_throw_no_msg (altarica_interpretation_exception);
	}
      if (arity < 0 && ccl_list_get_size (result) < -arity)
	{
	  ar_error (args, "not enough law parameters (%d).\n", -arity);
	  ccl_throw_no_msg (altarica_interpretation_exception);
	}
    }
  ccl_catch
    {
      ccl_list_clear_and_delete (result, (ccl_delete_proc *)
				 sas_law_param_del_reference);
      ccl_rethrow ();
    }
  ccl_end_try;
  
  return result;
}

static sas_law *
s_interp_unary_law (altarica_tree *args, ar_node *node,
		    sas_law *(*crt)(sas_law_param *))
{
  ccl_list *l = s_interp_list_of_params (args, node, 1);
  sas_law *result = crt (CAR (FIRST (l)));
  ccl_list_clear_and_delete (l, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  return result;
}

static sas_law *
s_interp_binary_law (altarica_tree *args, ar_node *node,
		     sas_law *(*crt)(sas_law_param *, sas_law_param *))
{
  ccl_list *l = s_interp_list_of_params (args, node, 2);
  sas_law *result = crt (CAR (FIRST (l)), CADR (FIRST (l)));
  ccl_list_clear_and_delete (l, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  return result;
}

static sas_law *
s_interp_ternary_law (altarica_tree *args, ar_node *node,
		      sas_law *(*crt)(sas_law_param *, sas_law_param *,
				      sas_law_param *))
{
  ccl_list *l = s_interp_list_of_params (args, node, 3);
  sas_law *result = crt (CAR (FIRST (l)), CADR (FIRST (l)), CADDR (FIRST (l)));
  ccl_list_clear_and_delete (l, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  return result;
}

static sas_law *
s_interp_dirac (altarica_tree *args, ar_node *node)
{
  return s_interp_unary_law (args, node, sas_law_crt_dirac);
}


static sas_law *
s_interp_empiric (altarica_tree *args, ar_node *node)
{
  int nbc;
  ccl_list *l = s_interp_list_of_params (args, node, -2);
  sas_law_param **histo = (sas_law_param **) ccl_list_to_array (l, &nbc);
  sas_law *result = sas_law_crt_empiric (nbc - 1, histo);
  ccl_list_clear_and_delete (l, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  ccl_delete (histo);
  
  return result;
}

static sas_law *
s_interp_erlang (altarica_tree *args, ar_node *node)
{
  return s_interp_binary_law (args, node, sas_law_crt_erlang);
}

static sas_law *
s_interp_exponential (altarica_tree *args, ar_node *node)
{
  return s_interp_unary_law (args, node, sas_law_crt_exponential);
}

static sas_law *
s_interp_exponential_wow (altarica_tree *args, ar_node *node)
{
  int nbp;
  ccl_list *l = s_interp_list_of_params (args, node, 13);
  sas_law_param **params = (sas_law_param **) ccl_list_to_array (l, &nbp);
  sas_law *result = sas_law_crt_exponential_wow (params[0], params + 1);
  ccl_list_clear_and_delete (l, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  ccl_delete (params);
  
  return result;
}

static sas_law *
s_interp_gen_erlang (altarica_tree *args, ar_node *node)
{
  int nbl;
  ccl_list *l = s_interp_list_of_params (args, node, -1);
  sas_law_param **lambdas = (sas_law_param **) ccl_list_to_array (l, &nbl);
  sas_law *result = sas_law_crt_generalized_erlang (nbl, lambdas);
  ccl_list_clear_and_delete (l, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  ccl_delete (lambdas);
  
  return result;  
}

static sas_law *
s_interp_ifa (altarica_tree *args, ar_node *node)
{
  return s_interp_binary_law (args, node, sas_law_crt_ifa);
}

static sas_law *
s_interp_ipa (altarica_tree *args, ar_node *node)
{
  return s_interp_unary_law (args, node, sas_law_crt_ipa);
}

static sas_law *
s_interp_nlog (altarica_tree *args, ar_node *node)
{
  return s_interp_binary_law (args, node, sas_law_crt_nlog);
}

static sas_law *
s_interp_triangle (altarica_tree *args, ar_node *node)
{
  return s_interp_ternary_law (args, node, sas_law_crt_triangle);
}

static sas_law *
s_interp_truncated_weibull (altarica_tree *args, ar_node *node)
{
  return s_interp_ternary_law (args, node, sas_law_crt_truncated_weibull);
}

static sas_law *
s_interp_uniform (altarica_tree *args, ar_node *node)
{
  return s_interp_binary_law (args, node, sas_law_crt_uniform);
}

static sas_law *
s_interp_weibull (altarica_tree *args, ar_node *node)
{
  return s_interp_binary_law (args, node, sas_law_crt_weibull);
}

static sas_conditional_law *
s_interp_conditional_law (altarica_tree *a, ar_node *node)
{
  sas_law *law = NULL;
  ar_expr *cond = NULL;
  sas_conditional_law *result = NULL;
  ar_context *ctx = NULL;

  if (! IS_LABELLED_BY (a, EXTERN_TERM_EXPR))
    {
      ar_error (a, "bad condition for optional law.\n");
      INTERP_EXCEPTION ();
    }
  
  ctx = ar_node_get_context (node);      
  ccl_try (altarica_interpretation_exception)
    {
      cond = ar_interp_boolean_expression (a->child, ctx,
					   ar_interp_acheck_member_access);
      law = s_interp_extern_law (a->next, node);
      if (sas_law_is_optional (law))
	{
	  ar_error (a, " optional laws can not be nested.\n");
	  INTERP_EXCEPTION ();
	}
      result =
	sas_conditional_law_crt (law, NULL,
				 (ccl_to_string_func *) ar_expr_to_string,
				 (ccl_delete_proc *) ar_expr_del_reference,
				 ar_expr_add_reference (cond));
    }
  ccl_no_catch;
  
  ar_context_del_reference (ctx);
  ccl_zdelete (ar_expr_del_reference, cond);
  ccl_zdelete (sas_law_del_reference, law);
  
  if (result == NULL)
    ccl_rethrow ();
  
  return result;  
}

static sas_law *
s_interp_optional (altarica_tree *args, ar_node *node)
{  
  sas_law *result = NULL;
  ccl_list *optional_laws = ccl_list_create ();

  ccl_try (altarica_interpretation_exception)
    {
      altarica_tree *a;
      for (a = args; a != NULL; a = a->next->next)
	{
	  sas_conditional_law *cl;
	  if (a->next == NULL)
	    {
	      ar_error (a, "missing argument for conditional law.\n");
	      INTERP_EXCEPTION ();
	    }
	  cl = s_interp_conditional_law (a, node);
	  ccl_list_add (optional_laws, cl);
	}
      result = sas_law_crt_optional_law (optional_laws);
    }
  ccl_catch
    {
      ccl_list_clear_and_delete (optional_laws, (ccl_delete_proc *)
				 sas_conditional_law_destroy);
      ccl_rethrow ();
    }
  ccl_end_try;
  ccl_post (result != NULL);
  
  return result;  
}

