/*
 * ar-stepper.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_STEPPER_H
# define AR_STEPPER_H

# include <ccl/ccl-array.h>
# include "ar-constraint-automaton.h"
# include "ar-stepper-state.h"

# define AR_STEPPER_ALL -1

# define AR_STEPPER_NO_VAR      0x00
# define AR_STEPPER_FLOW_VARS   0x01
# define AR_STEPPER_STATE_VARS  0x02
# define AR_STEPPER_LOCAL_VARS  0x04
# define AR_STEPPER_ALL_VARS  (AR_STEPPER_FLOW_VARS|AR_STEPPER_STATE_VARS)

typedef struct ar_stepper_st ar_stepper;
typedef CCL_ARRAY (ar_ca_trans *) ar_stepper_trans_array;
typedef CCL_ARRAY (int) ar_stepper_index_array;

			/* --------------- */

extern ar_stepper *
ar_stepper_create(ar_ca *ca, varorder *order);

extern ar_stepper *
ar_stepper_add_reference(ar_stepper *stepper);

extern void
ar_stepper_del_reference(ar_stepper *stepper);

			/* --------------- */

extern ar_ca_expr **
ar_stepper_get_variables (ar_stepper *stepper, int *p_nb_vars);

extern void *
ar_stepper_choice_point(ar_stepper *stepper);

extern void
ar_stepper_restore_choice_point(ar_stepper *stepper, void *cp);

extern void
ar_stepper_restore_last_choice_point(ar_stepper *stepper);

extern void
ar_stepper_save_variables(ar_stepper *stepper, int which);

extern void
ar_stepper_reset_variables(ar_stepper *stepper, int save, int which);

			/* --------------- */

/**
 * Current assignment of S/F variables is not modified.
 */
extern ar_stepper_state *
ar_stepper_get_initial_state (ar_stepper *stepper);

extern ar_stepper_state_array
ar_stepper_get_initial_states(ar_stepper *stepper, int max);

extern ar_stepper_state_array
ar_stepper_get_states_for_expr (ar_stepper *stepper, ar_ca_expr *e, int max);

/**
 * Assign to each S/F variable the value specified by 'state'.
 * Current assignment is saved into the backtracking stack if the 'save'
 * argument is not null.
 * No new choice-point is inserted.
 */
extern void
ar_stepper_set_state(ar_stepper *stepper, ar_stepper_state *state, int save);

/**
 * From the current assignment the stepper determines which transitions
 * might be triggered. If the argument 'prepost' is not null then pre and
 * post-conditions are checked; in the other case only the pre-condition (the
 * guard) is checked.
 *
 * When the function returns, the current assignment is not modified and
 * no new choice-point is inserted in the backtracking stack.
 */
extern ar_stepper_trans_array 
ar_stepper_get_valid_transitions(ar_stepper *stepper, int prepost);

extern ar_stepper_index_array 
ar_stepper_get_index_of_valid_transitions (ar_stepper *stepper, int prepost);

extern int 
ar_stepper_is_valid_transition (ar_stepper *stepper, ar_ca_trans *t,
				int prepost);

/**
 * Equivalent to ar_stepper_get_valid_transitions(stepper,0);
 */
extern ar_stepper_trans_array 
ar_stepper_get_valid_transitions_pre(ar_stepper *stepper);

/**
 * Equivalent to ar_stepper_get_valid_transitions(stepper,1);
 */
extern ar_stepper_trans_array 
ar_stepper_get_valid_transitions_prepost(ar_stepper *stepper);

/**
 * From the current assignment the stepper computes the successor states
 * when the transition 'T' is triggered. If 'max' is positive or null then
 * it represents the maximal size of the set of successors computed by the 
 * function; if 'max' is negative (@see use AR_STEPPER_ALL) then no limit is 
 * considered.
 *
 * When the function returns the current assignment is not modified and no
 * new choice-point is inserted into the backtracking stack);
 */
extern ar_stepper_state_array
ar_stepper_trigger_transition(ar_stepper *stepper, ar_ca_trans *T, int max);

extern ar_stepper_state *
ar_stepper_trigger (ar_stepper *stepper, ar_ca_trans *T);

#endif /* ! AR_STEPPER_H */
