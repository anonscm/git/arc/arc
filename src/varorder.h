/*
 * varorder.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef VARORDER_H
# define VARORDER_H

# include <ccl/ccl-protos.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-iterator.h>

typedef struct varorder_st varorder;

typedef void varorder_log_proc (ccl_log_type log, void *v, void *data);

extern varorder * 
varorder_create (ccl_delete_proc * vardel, varorder_log_proc *logvar, 
		 void *logvardata);

extern varorder *
varorder_add_reference (varorder *vo);

extern void
varorder_del_reference (varorder *vo);

#define varorder_get_size(vo) varorder_get_dd_indexes (vo, NULL)

extern int
varorder_get_nb_dd_indexes (const varorder *vo);

extern int
varorder_get_dd_indexes (const varorder *vo, int **p_indexes);

extern int
varorder_get_max_dd_index (const varorder *vo);

extern int
varorder_add_variable (const varorder *vo, void *var);

extern void
varorder_reset_dd_indexes (const varorder *vo);

extern void
varorder_set_dd_index (const varorder *vo, const void *var, int dd_index);

extern void
varorder_unset_dd_index (const varorder *vo, const void *var);

extern void
varorder_swap_dd_index (const varorder *vo, const void *v1, const void *v2);

extern int
varorder_get_index (const varorder *vo, const void *var);

extern int
varorder_get_dd_index (const varorder *vo, const void *var);

extern void *
varorder_get_representative (const varorder *vo, const void *var);

extern void 
varorder_merge (const varorder *vo, const void *rep, const void *var);

extern void *
varorder_get_variable_from_index (const varorder *vo, const int index);

extern void *
varorder_get_variable_from_dd_index (const varorder *vo, const int dd_index);

extern int
varorder_has_dd_index (const varorder *vo, const int dd_index);

extern void
varorder_log (const ccl_log_type log, const varorder *vo);

extern void
varorder_log_with_deps (ccl_log_type log, const varorder *vo,
			void (*deps)(ccl_log_type log, void *var, void *data),
			void *data);

extern void
varorder_log_sorted_by_dd_index (const ccl_log_type log, const varorder *vo);

extern void
varorder_log_sorted_by_dd_index_with_deps (ccl_log_type log, const varorder *vo,
					   void (*deps)(ccl_log_type log,
							void *var, void *data),
					   void *data);

extern ccl_pointer_iterator *
varorder_get_variables (const varorder *vo);

#endif /* ! VARORDER_H */
