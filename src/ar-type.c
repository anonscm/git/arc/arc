/*
 * ar-type.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-array.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-memory.h>
#include "ar-node.h"
#include "ar-model.h"
#include "ar-bang-ids.h"
#include "ar-type.h"

struct field {
  int index;
  ar_identifier *fname;
  ar_type *ftype;
};
			/* --------------- */

#define TYPE_IS_A(t, k) ((t)->base.kind == (AR_TYPE_ ## k))

#if CCL_ENABLE_ASSERTIONS
# define CHECK_TYPE_IS_A(t, k)	  \
  do {				  \
    const ar_type *__t = t;	  \
    ccl_pre ((__t) != NULL);	  \
    ccl_pre (TYPE_IS_A (__t, k)); \
  } while (0)
#else
# define CHECK_TYPE_IS_A(t, k) CCL_NOP ()
#endif

#define KIND_OF(t) ((t)->base.kind)
struct base_type
{
  ar_type_kind kind;
  uint32_t refcount;
};

struct range_type
{
  struct base_type base;
  int min, max;
};

struct enum_set_type
{
  struct base_type base;
  ar_idtable *symbols_to_int;
  CCL_ARRAY(ar_identifier *) int_to_symbols;  
};

struct symbols_type
{
  struct base_type base;
  ccl_bittable *all_symbols;
};

struct abstract_type
{
  struct base_type base;
  ar_identifier *name;
};

struct array_type
{
  struct base_type base;
  ar_type *type_of_elements;
  int size;
};

struct structure_type
{
  struct base_type base;
  ccl_list *fields;
  ar_idtable *fieldsmap;
  char is_ordered;
};

struct bang_type
{
  struct structure_type structure;
  ar_type_bang_kind kind;
  ar_node *node;
};

union ar_type_union 
{
  struct base_type base;
  struct range_type range;
  struct enum_set_type enumset;
  struct symbols_type symbols;
  struct abstract_type abstract;
  struct structure_type structure;
  struct array_type array;
  struct bang_type bang;
};

			/* --------------- */

static ar_type *
s_alloc_type (ar_type_kind kind);
static void
s_delete_type (ar_type *type);
static void
s_struct_map_unordered_fields (const ar_type *type,
			       void (*map)(ar_identifier *id, ar_type *type,
					   void *mapdata),
			       void *mapdata);

			/* --------------- */

static ar_type *SYMBOLS;

			/* --------------- */

ar_type *AR_BOOLEANS;
ar_type *AR_INTEGERS;
ar_type *AR_SYMBOLS;

			/* --------------- */

void
ar_type_init (void)
{
  AR_BOOLEANS = s_alloc_type (AR_TYPE_BOOLEANS);
  AR_INTEGERS = s_alloc_type (AR_TYPE_INTEGERS);
  AR_SYMBOLS  = s_alloc_type (AR_TYPE_SYMBOLS);
  SYMBOLS = ar_type_crt_enum ();
}

			/* --------------- */

void
ar_type_terminate(void)
{
  ar_type_del_reference (SYMBOLS);
  ar_type_del_reference(AR_SYMBOLS);
  ar_type_del_reference(AR_INTEGERS);
  ar_type_del_reference(AR_BOOLEANS);
}


			/* --------------- */

ar_type *
ar_type_crt_abstract(ar_identifier *name)
{
  ar_type *result = s_alloc_type(AR_TYPE_ABSTRACT);

  result->abstract.name = ar_identifier_add_reference (name);

  return result;
}

			/* --------------- */

ar_identifier *
ar_type_abstract_get_name(ar_type *type)
{
  return ar_identifier_add_reference(type->abstract.name);
}

			/* --------------- */

ar_type *
ar_type_crt_range(int min, int max)
{
  ar_type *result = s_alloc_type(AR_TYPE_RANGE);

  ccl_pre( min <= max );

  result->range.min = min;
  result->range.max = max;

  return result;
}

			/* --------------- */

int
ar_type_get_width (const ar_type *type)
{
  int result;

  ccl_pre (type != NULL); 

  if (TYPE_IS_A (type, STRUCTURE))
    result = ccl_list_get_size (type->structure.fields);
  else if (TYPE_IS_A (type, ARRAY))
    result = type->array.size;
  else
    result = 1;
  
  return result;
}

			/* --------------- */

static void
s_struct_size_map (ar_identifier *fname, ar_type *ftype, void *data)
{
  *((int *)data) += ar_type_get_size (ftype);
}

int
ar_type_get_size (const ar_type *type)
{
  int result;

  ccl_pre (type != NULL); 

  if (TYPE_IS_A (type, STRUCTURE) || TYPE_IS_A (type, BANG))
    {
      result = 0;
      s_struct_map_unordered_fields (type, s_struct_size_map, &result);
    }
  else if (TYPE_IS_A (type, ARRAY))
    result = (type->array.size *
	      ar_type_get_size (type->array.type_of_elements));
  else
    result = 1;

  return result;
}

			/* --------------- */

static void
s_struct_card_map (ar_identifier *fname, ar_type *ftype, void *data)
{
  *((int *)data) *= ar_type_get_cardinality (ftype);
}

int
ar_type_get_cardinality (const ar_type *type)
{
  int result = -1;

  ccl_pre (type != NULL);

  switch (KIND_OF (type))
    {
    case AR_TYPE_BOOLEANS: 
      result = 2;
      break;

    case AR_TYPE_INTEGERS: case AR_TYPE_SYMBOLS: case AR_TYPE_ABSTRACT: 
      result = -1;
      break;
      
    case AR_TYPE_RANGE: 
      result = type->range.max - type->range.min + 1;
      break;

    case AR_TYPE_SYMBOL_SET:
      result = ccl_bittable_get_nb_one (type->symbols.all_symbols);
      break;

    case AR_TYPE_ARRAY:
      result = (type->array.size * 
		ar_type_get_cardinality (type->array.type_of_elements));
      break;

    case AR_TYPE_BANG:
    case AR_TYPE_STRUCTURE:
      result = 1;
      s_struct_map_unordered_fields (type, s_struct_card_map, &result);
      break;
      
    case AR_TYPE_ENUMERATION:
      result = ar_idtable_get_size (type->enumset.symbols_to_int);
      break;
  }

  return result;
}

			/* --------------- */

int
ar_type_range_get_min (ar_type *type)
{
  CHECK_TYPE_IS_A (type, RANGE);
  
  return type->range.min;
}

			/* --------------- */

int
ar_type_range_get_max (ar_type *type)
{
  CHECK_TYPE_IS_A (type, RANGE);
  
  return type->range.max;
}


			/* --------------- */

void
ar_type_range_get_bounds (ar_type *type, int *pmin, int *pmax)
{
  ccl_pre (pmin != NULL); 
  ccl_pre (pmax != NULL);

  *pmin = ar_type_range_get_min (type);
  *pmax = ar_type_range_get_max (type);
}

			/* --------------- */

ar_type *
ar_type_crt_symbol_set (void)
{
  ar_type *result = s_alloc_type (AR_TYPE_SYMBOL_SET);
  uint32_t size = ar_type_get_cardinality (SYMBOLS);
  
  if (size == 0)
    size++;

  result->symbols.all_symbols = ccl_bittable_create (size);

  return result;
}

			/* --------------- */

int
ar_type_symbol_set_add_value (ar_type *type, ar_identifier *id)
{
  int index;

  CHECK_TYPE_IS_A (type, SYMBOL_SET);

  index = ar_type_enum_add_value (SYMBOLS, id);
  ccl_bittable_set (type->symbols.all_symbols, index);

  return index;
}

			/* --------------- */

int
ar_type_symbol_set_has_value_name (ar_type *type, ar_identifier *id)
{
  int index = ar_type_symbol_set_get_value (id);

  if (index < 0)
    return 0;

  return ccl_bittable_has (type->symbols.all_symbols, index);
}

			/* --------------- */

int
ar_type_symbol_set_has_value (ar_type *type, int id)
{
  ccl_pre (id >= 0);
  CHECK_TYPE_IS_A (type, SYMBOL_SET);

  return ccl_bittable_has (type->symbols.all_symbols, id);
}

			/* --------------- */

int
ar_type_symbol_set_get_value (ar_identifier *id)
{
  ccl_pre (id != NULL);

  return ar_type_enum_get_value_index (SYMBOLS, id);
}

			/* --------------- */

extern int
ar_type_symbol_set_get_value_name_index (ar_type *type, ar_identifier *id)
{
  int val = ar_type_symbol_set_get_value (id);

  ccl_pre (type != NULL);

  return ar_type_symbol_set_get_value_index (type, val);
}

			/* --------------- */

int
ar_type_symbol_set_get_value_index (ar_type *type, int val)
{
  int result = 0;
  int i;

  ccl_pre (type != NULL);
  ccl_pre (ccl_bittable_has (type->symbols.all_symbols, val));

  for (i = ccl_bittable_get_first (type->symbols.all_symbols); i >= 0; 
       i = ccl_bittable_get_next (type->symbols.all_symbols, i))
    {
      if (i == val)
	break;
      result++;
    }

  ccl_post (i >= 0);

  return result;
}

			/* --------------- */

ar_identifier *
ar_type_symbol_set_get_value_name (uint32_t id)
{
  return ar_type_enum_get_value_at_index (SYMBOLS, id);
}



			/* --------------- */

ccl_int_iterator *
ar_type_symbol_set_get_values (ar_type *type)
{
  ccl_pre (type != NULL);
  CHECK_TYPE_IS_A (type, SYMBOL_SET);

  return ccl_bittable_get_ones (type->symbols.all_symbols);
}

			/* --------------- */

ar_type *
ar_type_crt_enum (void)
{
  struct enum_set_type *result = 
    (struct enum_set_type *) s_alloc_type (AR_TYPE_ENUMERATION);

  result->symbols_to_int = ar_idtable_create (1, NULL, NULL);
  ccl_array_init_with_size (result->int_to_symbols, 1);

  return (ar_type *) result;
}

			/* --------------- */

int
ar_type_enum_add_value (ar_type *type, ar_identifier *id)
{
  int result;

  CHECK_TYPE_IS_A (type, ENUMERATION);

  if (ar_idtable_has (type->enumset.symbols_to_int, id))
    result = (intptr_t) ar_idtable_get (type->enumset.symbols_to_int, id);
  else
    {
      uint32_t size = 
	(uintptr_t) ar_idtable_get_size (type->enumset.symbols_to_int);

      if (size == type->enumset.int_to_symbols.size)
	ccl_array_ensure_size (type->enumset.int_to_symbols, 2 * size);

      type->enumset.int_to_symbols.data[size] = id;
      ar_idtable_put (type->enumset.symbols_to_int, id, 
		      (void *) (uintptr_t) size);

      result = size;
    }

  return result;
}

			/* --------------- */

int
ar_type_enum_has_value (const ar_type *type, ar_identifier *id)
{
  return (ar_type_enum_get_value_index (type, id) >= 0);
}

			/* --------------- */

int
ar_type_enum_get_value_index (const ar_type *type, ar_identifier *id)
{
  if (!ar_idtable_has (type->enumset.symbols_to_int, id))
    return -1;

  return (intptr_t) ar_idtable_get (type->enumset.symbols_to_int, id);
}

			/* --------------- */

ar_identifier *
ar_type_enum_get_value_at_index (const ar_type *type, int id)
{
  if (id < ar_idtable_get_size (type->enumset.symbols_to_int))
    return ar_identifier_add_reference (type->enumset.int_to_symbols.data[id]);

  return NULL;
}

			/* --------------- */

ar_type *
ar_type_crt_array (ar_type *base_type, uint32_t size)
{
  ar_type *result = s_alloc_type (AR_TYPE_ARRAY);

  ccl_pre (base_type != NULL);

  result->array.type_of_elements = ar_type_add_reference (base_type);
  result->array.size = size;

  return result;
}


			/* --------------- */

ar_type *
ar_type_array_get_base_type (const ar_type *type)
{
  CHECK_TYPE_IS_A (type, ARRAY);

  return ar_type_add_reference (type->array.type_of_elements);
}

			/* --------------- */

uint32_t
ar_type_array_get_size (const ar_type *type)
{
  CHECK_TYPE_IS_A (type, ARRAY);

  return type->array.size;
}

			/* --------------- */

static ar_type *
s_alloc_structure (ar_type_kind kind)
{
  ar_type *result = s_alloc_type (kind);

  result->structure.fields = ccl_list_create ();
  result->structure.fieldsmap = ar_idtable_create (0, NULL, NULL);
  result->structure.is_ordered = 0;
  
  return result;
}

			/* --------------- */

static void
s_delete_field (struct field *f)
{
  ar_identifier_del_reference (f->fname);
  ar_type_del_reference (f->ftype);
  ccl_delete (f);
}

static void
s_clear_structure (struct structure_type *st)
{
  ccl_list_clear_and_delete (st->fields, (ccl_delete_proc *) s_delete_field);
  ar_idtable_del_reference (st->fieldsmap);
}

static int
s_cmp_fields (const void *p1, const void *p2)
{
  const struct field *f1 = p1;
  const struct field *f2 = p2;
  if (f1->fname == f2->fname) return 0;
  if (f1->fname < f2->fname) return -1;
  return 1;
}

static void
s_sort_fields_is_necessary (ar_type *type)
{
  ccl_pair *p;
  int i;
  
  if (type->structure.is_ordered)
    return;
  i = 0;
  ccl_list_sort (type->structure.fields, s_cmp_fields);
  for (p = FIRST (type->structure.fields); p; p = CDR (p))
    ((struct field *) CAR (p))->index = i++;
  type->structure.is_ordered = 1;
}

static void
s_struct_map_unordered_fields (const ar_type *type,
			       void (*map)(ar_identifier *id, ar_type *type,
					   void *mapdata),
			       void *mapdata)
{
  ccl_pair *p;
  
  for (p = FIRST (type->structure.fields); p; p = CDR (p))
    {
      struct field *f = CAR (p);
      map (f->fname, f->ftype, mapdata);
    }
}

ar_type *
ar_type_crt_structure(void)
{
  return s_alloc_structure (AR_TYPE_STRUCTURE);
}

void 
ar_type_struct_add_field (ar_type *type, ar_identifier *fname, ar_type *ftype)
{
  struct field *f;

  ccl_pre (ftype != NULL); 
  ccl_pre (!ar_type_struct_has_field (type, fname));

  f = ccl_new (struct field);
  f->fname = ar_identifier_add_reference (fname);
  f->ftype = ar_type_add_reference (ftype);
  f->index = -1;

  type->structure.is_ordered = 0;
  ccl_list_add (type->structure.fields, f);
  ar_idtable_put (type->structure.fieldsmap, fname, f);
}

			/* --------------- */

ar_type *
ar_type_struct_get_field (ar_type *type, ar_identifier *fname)
{
  ar_type *result;
  struct field *f = ar_idtable_get (type->structure.fieldsmap, fname);

  if (f != NULL)
    result = ar_type_add_reference (f->ftype);
  else
    result = NULL;
  
  return result;
}

			/* --------------- */

int
ar_type_struct_has_field (ar_type *type, ar_identifier *fname)
{
  return ar_idtable_has (type->structure.fieldsmap, fname);
}

			/* --------------- */

struct id_iterator 
{
  ar_identifier_iterator super;
  ar_type *reftype;
  ccl_pair *current;
};

			/* --------------- */

static int
s_id_iterator_has_more_elements (const ar_identifier_iterator *i)
{
  struct id_iterator *iter = (struct id_iterator *) i;

  return iter->current != NULL;
}

			/* --------------- */

static ar_identifier *
s_id_iterator_next_element (ar_identifier_iterator *i)
{
  struct id_iterator *iter = (struct id_iterator *) i;
  struct field *f;
  ar_identifier *result;

  ccl_pre (iter->current != NULL);
  ccl_pre (iter->reftype->structure.is_ordered);

  f = CAR (iter->current);
  result = ar_identifier_add_reference (f->fname);
  iter->current = CDR (iter->current);

  return result;
}

			/* --------------- */

static void
s_id_iterator_delete_iterator (ar_identifier_iterator *i)
{
  struct id_iterator *iter = (struct id_iterator *) i;

  ar_type_del_reference (iter->reftype);
  ccl_delete (i);
}

			/* --------------- */

ar_identifier_iterator *
ar_type_struct_get_fields (ar_type *type)
{
  struct id_iterator *result = ccl_new (struct id_iterator);

  result->super.has_more_elements = s_id_iterator_has_more_elements;
  result->super.next_element = s_id_iterator_next_element;
  result->super.delete_iterator = s_id_iterator_delete_iterator;
  result->reftype = ar_type_add_reference (type);

  s_sort_fields_is_necessary (type);
  result->current = FIRST (type->structure.fields);

  return (ar_identifier_iterator *) result;
}

			/* --------------- */

int 
ar_type_struct_get_field_index (ar_type *type, ar_identifier *fname)
{
  int result;
  struct field *f;

  ccl_pre (type != NULL);
  ccl_pre (TYPE_IS_A (type, STRUCTURE) || TYPE_IS_A (type, BANG));
  ccl_pre (fname != NULL); 

  s_sort_fields_is_necessary (type);
  f = ar_idtable_get (type->structure.fieldsmap, fname);
  if (f != NULL)
    {
      result = f->index;
      ccl_assert (f->index >= 0);
    }
  else
    result = -1;
  return result;
}

void
ar_type_struct_map_fields (ar_type *type,
			   void (*map)(ar_identifier *id, ar_type *type,
				       void *mapdata),
			   void *mapdata)
{
  s_sort_fields_is_necessary (type);
  s_struct_map_unordered_fields (type, map, mapdata);
}

  
			/* --------------- */

static ar_type *
s_alloc_bang (ar_type_bang_kind kind, struct ar_node_st *node)
{
  ar_type *result = s_alloc_structure (AR_TYPE_BANG);

  result->bang.node = node;
  result->bang.kind = kind;

  return result;
}

			/* --------------- */

static void
s_clear_bang (struct bang_type *bang)
{
  s_clear_structure (&bang->structure);
}

			/* --------------- */

ar_type *
ar_type_crt_bang (ar_type_bang_kind kind, struct ar_node_st *node)
{
  return s_alloc_bang (kind, node);
}

struct ar_node_st *
ar_type_bang_get_node (ar_type *type)
{
  CHECK_TYPE_IS_A (type, BANG);

  return ar_node_add_reference (type->bang.node);
}

			/* --------------- */

ar_type_bang_kind
ar_type_bang_get_kind (ar_type *type)
{
  CHECK_TYPE_IS_A (type, BANG);

  return type->bang.kind;
}

			/* --------------- */

int
ar_type_is_scalar (ar_type *type) 
{
  ccl_pre (type != NULL);

  return !(TYPE_IS_A (type, ARRAY) || TYPE_IS_A (type, STRUCTURE) ||
	   TYPE_IS_A (type, BANG));
}

			/* --------------- */

ar_type_kind
ar_type_get_kind (ar_type *type)
{
  ccl_pre (type != NULL);

  return KIND_OF (type);
}

			/* --------------- */

ar_type *
ar_type_add_reference (ar_type *type)
{
  ccl_pre (type != NULL); 

  type->base.refcount++;

  return type;
}

			/* --------------- */

void
ar_type_del_reference(ar_type *type)
{
  ccl_pre (type != NULL); 
  ccl_pre (type->base.refcount > 0);

  type->base.refcount--;
  if (type->base.refcount == 0)
    s_delete_type (type);
}

			/* --------------- */

static ar_type *
s_collect_array_dims (ar_type *type, ccl_list *dims)
{
  ar_type *result = NULL;

  ccl_pre (KIND_OF (type) == AR_TYPE_ARRAY);

  if (KIND_OF (type->array.type_of_elements) != AR_TYPE_ARRAY)
    {
      result = type->array.type_of_elements;
      ccl_list_put_first (dims, (void *) (intptr_t) type->array.size);
    }
  else
    {
      result = s_collect_array_dims (type->array.type_of_elements, dims);
      ccl_list_put_first (dims, (void *) (intptr_t) type->array.size);
    }  

  return result;
}

			/* --------------- */

static void
s_struct_log_map (ar_identifier *fname, ar_type *type, void *data)
{
  ccl_log_type log = (ccl_log_type) data;
  ar_identifier_log (log, fname);
  ccl_log (log," : ");
  ar_type_log (log, type);
  ccl_log (log,"; ");
}

void
ar_type_log (ccl_log_type log, ar_type *type)
{
  int i, max;

  ccl_pre (type != NULL);

  switch (KIND_OF (type))
    {
    case AR_TYPE_ABSTRACT : 
      ar_identifier_log (log, type->abstract.name);
      break;

    case AR_TYPE_BOOLEANS : 
      ccl_log (log, "bool"); 
      break;

    case AR_TYPE_INTEGERS : 
      ccl_log (log, "integer"); 
      break;
    case AR_TYPE_SYMBOLS : 
      ccl_log (log, "symbol"); 
      break;

    case AR_TYPE_RANGE : 
      ccl_log (log, "[%d, %d]", type->range.min, type->range.max);
      break;

    case AR_TYPE_SYMBOL_SET :
      ccl_log (log, "{ ");
      max = ccl_bittable_get_nb_one (type->symbols.all_symbols);
      for (i = ccl_bittable_get_first (type->symbols.all_symbols); i >= 0; 
	   i = ccl_bittable_get_next (type->symbols.all_symbols,i) )
	{
	  ar_identifier *id = ar_type_symbol_set_get_value_name(i);

	  ar_identifier_log (log, id);
	  if (--max)
	    ccl_log (log, ", ");
	  ar_identifier_del_reference (id);
	}

      ccl_log (log, " }");
      break;
      
    case AR_TYPE_ARRAY :
      {
	ccl_list *dims = ccl_list_create ();
	ar_type *base = s_collect_array_dims (type, dims);
	ar_type_log (log, base);
	while (! ccl_list_is_empty (dims))
	  ccl_log (log, "[%ld]", (intptr_t) ccl_list_take_first (dims));
	ccl_list_delete (dims);
      }
      break;

    case AR_TYPE_BANG :
      {
	ar_identifier *id = ar_node_get_name (type->bang.node);
	ar_identifier_log (log, id);
	ar_identifier_del_reference(id);
	ccl_log (log, "!%s", ar_bang_labels[type->bang.kind]);
      }
      break;

    case AR_TYPE_STRUCTURE :
      ccl_log (log,"struct ");
      ar_type_struct_map_fields (type, s_struct_log_map,
				 (void *) (intptr_t) log);
      ccl_log (log," tcurts");
      break;

    case AR_TYPE_ENUMERATION:
      ccl_log (log, "{ ");
      if ((max = ar_type_get_cardinality (type)) > 0)
	{
	  ar_identifier_log (log, type->enumset.int_to_symbols.data[0]);
	  for (i = 1; i < max; i++)
	    {
	      ccl_log (log, ", ");
	      ar_identifier_log (log, type->enumset.int_to_symbols.data[i]);
	    }
	  ccl_log (log, " ");
	}
      ccl_log (log, "}");
      break;
  };
}

			/* --------------- */

char *
ar_type_to_string (ar_type *type)
{ 
  char *result = NULL;
  ccl_log_redirect_to_string (CCL_LOG_DISPLAY, &result);
  ar_type_log (CCL_LOG_DISPLAY, type);
  ccl_log_pop_redirection (CCL_LOG_DISPLAY);

  return result;
}

			/* --------------- */

static void
s_struct_base_type_map (ar_identifier *id, ar_type *type,  void *result)
{
  ar_type *bt = ar_type_get_base_type (type);
  ar_type_struct_add_field (result, id, bt);
  ar_type_del_reference (bt);
}

ar_type *
ar_type_get_base_type (ar_type *type)
{
  ar_type *result;

  ccl_pre (type != NULL);

  switch (KIND_OF (type))
    {
    case AR_TYPE_RANGE : 
      result = ar_type_add_reference (AR_INTEGERS); 
      break;

    case AR_TYPE_SYMBOL_SET : 
      result = ar_type_add_reference (AR_SYMBOLS); 
      break;

    case AR_TYPE_ARRAY : 
      {
	ar_type *bt = ar_type_get_base_type (type->array.type_of_elements);
	result = ar_type_crt_array (bt, type->array.size);
	ar_type_del_reference (bt);
      }
      break;

    case AR_TYPE_STRUCTURE : 
      result = ar_type_crt_structure ();
      ar_type_struct_map_fields (type, s_struct_base_type_map, result);
      break;

    case AR_TYPE_BANG:
      if (type->bang.kind == AR_TYPE_BANG_CONFIGURATIONS)
	return ar_node_get_configuration_base_type (type->bang.node);

    default : 
      result = ar_type_add_reference (type); 
      break;
    };

  return result;
}

			/* --------------- */

int
ar_type_equals (ar_type *t1, ar_type *t2)
{
  ar_identifier_iterator *f1;
  ar_identifier_iterator *f2;

  ccl_pre (t1 != NULL); 
  ccl_pre (t2 != NULL);

  if (KIND_OF (t1) != KIND_OF (t2))
    return 0;

  switch (KIND_OF (t1)) 
    {
    case AR_TYPE_ABSTRACT :
      return t1->abstract.name == t2->abstract.name;
      
    case AR_TYPE_BOOLEANS : case AR_TYPE_INTEGERS : case AR_TYPE_SYMBOLS : 
      return t1 == t2;

    case AR_TYPE_RANGE : 
      return ((t1->range.min == t2->range.min) &&
	      (t1->range.max == t2->range.max));
      
    case AR_TYPE_ENUMERATION:
      if (ar_idtable_get_size (t1->enumset.symbols_to_int) != 
	  ar_idtable_get_size (t2->enumset.symbols_to_int))
	return 0;
      return ccl_memcmp (t1->enumset.int_to_symbols.data,
			 t2->enumset.int_to_symbols.data, 
			 sizeof (ar_identifier *) * 
			 ar_idtable_get_size (t1->enumset.symbols_to_int)) == 0;

    case AR_TYPE_SYMBOL_SET :
      {
	int i;

	if (ccl_bittable_get_nb_one (t1->symbols.all_symbols) !=
	    ccl_bittable_get_nb_one (t2->symbols.all_symbols))
	  return 0;

	for (i = ccl_bittable_get_first (t1->symbols.all_symbols);
	     i >= 0;
	     i = ccl_bittable_get_next (t1->symbols.all_symbols, i))
	  if (!ccl_bittable_has (t2->symbols.all_symbols, i))
	    return 0;
	return 1;
      }
    case AR_TYPE_ARRAY :
      return (ar_type_equals (t1->array.type_of_elements, 
			      t2->array.type_of_elements) &&
	      t1->array.size == t2->array.size);

    case AR_TYPE_BANG :
      return (t1->bang.node == t2->bang.node && t1->bang.kind == t2->bang.kind);
      
    case AR_TYPE_STRUCTURE :
      {
	int result = 1;
	
	if (ar_type_get_width (t1) != ar_type_get_width (t2))
	  return 0;

	s_sort_fields_is_necessary (t1);
	s_sort_fields_is_necessary (t2);

	f1 = ar_type_struct_get_fields (t1);
	f2 = ar_type_struct_get_fields (t2);

	while (ccl_iterator_has_more_elements (f1) &&
	       ccl_iterator_has_more_elements (f2) && result)
	  {
	    ar_identifier *f1name = ccl_iterator_next_element (f1);
	    ar_identifier *f2name = ccl_iterator_next_element (f2);
	    
	    result = (f1name == f2name);
	    if (result)
	      {
		ar_type *ft1 = ar_type_struct_get_field (t1, f1name);
		ar_type *ft2 = ar_type_struct_get_field (t2, f2name);
		result = ar_type_equals (ft1, ft2);
		ar_type_del_reference (ft1);
		ar_type_del_reference (ft2);
	      }
	    ar_identifier_del_reference (f1name);
	    ar_identifier_del_reference (f2name);
	  }
	result = (result &&
		  ! ccl_iterator_has_more_elements (f1) &&
		  ! ccl_iterator_has_more_elements (f2));
	ccl_iterator_delete (f1);
	ccl_iterator_delete (f2);

	return result;
      }
    }
  
  ccl_unreachable ();
  
  return 0;
}

			/* --------------- */

int
ar_type_have_same_base_type(ar_type *t1, ar_type *t2)
{
  int r;
  ar_type *bt1, *bt2;

  if( t1 == t2 ) 
    return 1;

  bt1 = ar_type_get_base_type(t1);
  bt2 = ar_type_get_base_type(t2);
  r =  ar_type_equals(bt1,bt2);
  ar_type_del_reference(bt1);
  ar_type_del_reference(bt2);

  return r;
}

			/* --------------- */

ccl_list *
ar_type_expand (ar_type *t, ccl_list *result)
{
  return ar_type_expand_with_prefix (NULL, t, result);
}

			/* --------------- */

struct struct_expand_map_data
{
  ccl_list *result;
  ar_identifier *prefix;
};

static void
s_struct_expand_map (ar_identifier *fname, ar_type *type, void *data)
{
  struct struct_expand_map_data *d = data;
  ar_identifier *lpref;

  if (d->prefix)
    lpref = ar_identifier_add_prefix (fname, d->prefix);
  else
    lpref = NULL;
  
  ar_type_expand_with_prefix (lpref, type, d->result);
  ccl_zdelete (ar_identifier_del_reference, lpref);
}

ccl_list *
ar_type_expand_with_prefix (ar_identifier *prefix, ar_type *t, 
			    ccl_list *result)
{
  if (result == NULL)
    result = ccl_list_create ();

  switch (KIND_OF (t))
    {
    case AR_TYPE_INTEGERS : case AR_TYPE_SYMBOLS : case AR_TYPE_ABSTRACT :
    case AR_TYPE_BOOLEANS : case AR_TYPE_RANGE : case AR_TYPE_SYMBOL_SET :
    case AR_TYPE_ENUMERATION:
      if (prefix)
	{
	  prefix = ar_identifier_add_reference (prefix);
	  ccl_list_add (result, prefix);
	}

      t = ar_type_add_reference (t);
      ccl_list_add (result, t);
      break;

    case AR_TYPE_STRUCTURE:
    case AR_TYPE_BANG:
      {
	struct struct_expand_map_data data = { result , prefix };
	ar_type_struct_map_fields (t, s_struct_expand_map, &data);
      }
      break;

    case AR_TYPE_ARRAY:
      {
	int i;
	char tmp[100];

	for (i = 0; i < t->array.size; i++)
	  {
	    ar_identifier *lpref = NULL;
	    if (prefix)
	      {
		sprintf (tmp, "[%d]", i);
		lpref = ar_identifier_rename_with_suffix(prefix, tmp);
	      }
	    ar_type_expand_with_prefix (lpref, t->array.type_of_elements, 
					 result);
	    ccl_zdelete (ar_identifier_del_reference, lpref);
	  }
      }
      break;

    }

  return result;
}

			/* --------------- */

ar_constant *
ar_type_get_ith_element (ar_type *t, int i)
{
  ccl_pre (t != NULL);
  ccl_pre (0 <= i && i < ar_type_get_cardinality (t));

  switch (KIND_OF (t)) 
    {
    case AR_TYPE_BOOLEANS : 
      return ar_constant_crt_boolean (i);

    case AR_TYPE_RANGE : 
      return ar_constant_crt_integer (t->range.min + i);
      
    case AR_TYPE_ENUMERATION:
      return ar_constant_crt_symbol (i, t);

    case AR_TYPE_SYMBOL_SET:
      {
	int index = ccl_bittable_get_first (t->symbols.all_symbols);
	while (i--)
	  {
	    ccl_assert (index >= 0);
	    index = ccl_bittable_get_next (t->symbols.all_symbols, index);
	  }

	return ar_constant_crt_symbol (index, t);
      }
      break;
      
    case AR_TYPE_INTEGERS: case AR_TYPE_SYMBOLS: case AR_TYPE_ABSTRACT: 
    case AR_TYPE_ARRAY: case AR_TYPE_BANG: case AR_TYPE_STRUCTURE:
      break;
    };

  ccl_unreachable ();

  return NULL;
}

			/* --------------- */

struct struct_write_map_data
{
  FILE *out;
  ccl_serializer_status *p_err;
};

static void
s_struct_write_map (ar_identifier *fname, ar_type *type, void *data)
{
  struct struct_write_map_data *d = data;
  ar_identifier_write (fname, d->out, d->p_err);
  ar_type_write (type, d->out, d->p_err);
}

void
ar_type_write (ar_type *t, FILE *out, ccl_serializer_status *p_err)
{
  ar_identifier *id;
  int i;
  ar_type_kind kind = t->base.kind;
  
  ccl_serializer_write_uint8 (kind, out, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    return;
  
  switch (kind) 
    {
    case AR_TYPE_BOOLEANS:
    case AR_TYPE_INTEGERS:
    case AR_TYPE_SYMBOLS:
      break;

    case AR_TYPE_ABSTRACT:
      ar_identifier_write (t->abstract.name, out, p_err);
      break;

    case AR_TYPE_RANGE:
      ccl_serializer_write_int32 (t->range.min, out, p_err);
      ccl_serializer_write_int32 (t->range.max, out, p_err);
      break;


    case AR_TYPE_ARRAY:
      ccl_serializer_write_int32 (t->array.size, out, p_err);
      ar_type_write (t->array.type_of_elements, out, p_err);
      break;

    case AR_TYPE_ENUMERATION:
      {
	int card = ar_type_get_cardinality (t);
	ccl_serializer_write_uint32 (card, out, p_err);
	for (i = 0; i < card && *p_err == CCL_SERIALIZER_OK; i++)
	  ar_identifier_write (t->enumset.int_to_symbols.data[i], out, p_err);
      }
      break;

    case AR_TYPE_SYMBOL_SET:
      ccl_serializer_write_uint32 (ar_type_get_cardinality (t), out, p_err);
      for (i = ccl_bittable_get_first (t->symbols.all_symbols);
	   i >= 0 && *p_err == CCL_SERIALIZER_OK;
	   i = ccl_bittable_get_next (t->symbols.all_symbols, i))
	{
	  ccl_serializer_write_uint32 (i, out, p_err);
	  id = ar_type_symbol_set_get_value_name (i);
	  ar_identifier_write (id, out, p_err);
	  ar_identifier_del_reference (id);
	}
      break;

    case AR_TYPE_BANG:      
      ccl_serializer_write_uint8 (t->bang.kind, out, p_err);	
      id = ar_node_get_name (t->bang.node);
      ar_identifier_write (id, out, p_err);
      ar_identifier_del_reference (id);

    case AR_TYPE_STRUCTURE:
      {
	struct struct_write_map_data data = { out, p_err };
	int width = ccl_list_get_size (t->structure.fields);
	/* !!! dont't use type_get_width. For bang ids it returns 1. */
	ccl_serializer_write_uint32 (width, out, p_err);
	ar_type_struct_map_fields (t, s_struct_write_map, &data);
      }
      break;
    }
}

			/* --------------- */


void
ar_type_read (ar_type **p_t, FILE *in, ccl_serializer_status *p_err)
{
  uint8_t k;
  ar_identifier *id = NULL;
  ar_type_kind kind;

  *p_t = NULL;
  ccl_serializer_read_uint8 (&k, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    return;
  kind = k;

  switch (kind)
    {
    case AR_TYPE_BOOLEANS:
      *p_t = ar_type_add_reference (AR_BOOLEANS);
      break;
    case AR_TYPE_INTEGERS:
      *p_t = ar_type_add_reference (AR_INTEGERS);
      break;
    case AR_TYPE_SYMBOLS:
      *p_t = ar_type_add_reference (AR_SYMBOLS);
      break;

    case AR_TYPE_ABSTRACT:
      ar_identifier_read (&id, in, p_err);
      if (*p_err == CCL_SERIALIZER_OK)
	*p_t = ar_type_crt_abstract(id);
      ccl_zdelete (ar_identifier_del_reference, id);
      break;

    case AR_TYPE_RANGE:
      {
	int32_t min, max;

	ccl_serializer_read_int32 (&min, in, p_err);
	ccl_serializer_read_int32 (&max, in, p_err);
	if (*p_err == CCL_SERIALIZER_OK)
	  *p_t = ar_type_crt_range (min, max);
      }
      break;


    case AR_TYPE_ARRAY:
      {
	int32_t size = 0;
	ar_type *et = NULL;

	ccl_serializer_read_int32 (&size, in, p_err);
	ar_type_read (&et, in, p_err);
	if (*p_err == CCL_SERIALIZER_OK)
	  *p_t = ar_type_crt_array (et, size);
	ccl_zdelete (ar_type_del_reference, et);
      }
      break;

    case AR_TYPE_ENUMERATION:
      {
	uint32_t size = 0;

	ccl_serializer_read_uint32 (&size, in, p_err);
	if (*p_err != CCL_SERIALIZER_OK)
	  return;

	*p_t = ar_type_crt_enum ();
	while (size-- && *p_err == CCL_SERIALIZER_OK)
	  {
	    id = NULL;
	    ar_identifier_read (&id, in, p_err);

	    if (*p_err == CCL_SERIALIZER_OK)
	      {
		if (ar_type_enum_has_value (*p_t, id))
		  *p_err = CCL_SERIALIZER_DATA_ERROR;
		else
		  ar_type_enum_add_value (*p_t, id);
	      }
	    ccl_zdelete (ar_identifier_del_reference, id);
	  }
	if (*p_err != CCL_SERIALIZER_OK)
	  {
	    ar_type_del_reference (*p_t);
	    *p_t = NULL;
	  }
      }
      break;


    case AR_TYPE_SYMBOL_SET:
      {
	uint32_t size = 0;

	ccl_serializer_read_uint32 (&size, in, p_err);
	if (*p_err != CCL_SERIALIZER_OK)
	  return;

	*p_t = ar_type_crt_symbol_set ();
	while (size-- && *p_err == CCL_SERIALIZER_OK)
	  {
	    uint32_t i;
	    id = NULL;
	    ccl_serializer_read_uint32 (&i, in, p_err);
	    ar_identifier_read (&id, in, p_err);
	    if (*p_err == CCL_SERIALIZER_OK)
	      {
		if (ar_type_symbol_set_add_value (*p_t, id) != i)
		  *p_err = CCL_SERIALIZER_DATA_ERROR;
	      }
	    ccl_zdelete (ar_identifier_del_reference, id);
	  }
	if (*p_err != CCL_SERIALIZER_OK)
	  {
	    ar_type_del_reference (*p_t);
	    *p_t = NULL;
	  }
      }
      break;

    case AR_TYPE_BANG:
      {
	uint8_t k;
	ar_identifier *id = NULL;

	ccl_serializer_read_uint8 (&k, in, p_err);
	if (k >= AR_TYPE_LAST_AND_UNUSED_BANG_TYPE)
	  {
	    *p_err = CCL_SERIALIZER_DATA_ERROR;
	    return;
	  }
	ar_identifier_read (&id, in, p_err);

	if (*p_err == CCL_SERIALIZER_OK)
	  {
	    if (ar_model_has_node (id))
	      {
		ar_node *n = ar_model_get_node (id);
		*p_t = ar_type_crt_bang (k, n);
		ar_node_del_reference (n);
	      }
	    else
	      {
		*p_err = CCL_SERIALIZER_DATA_ERROR;
	      }	    
	  }
	ccl_zdelete (ar_identifier_del_reference, id);
	if (*p_err != CCL_SERIALIZER_OK)
	  goto end;
      }

    case AR_TYPE_STRUCTURE:
      {
	uint32_t width = 0;

	if (*p_t == NULL)
	  *p_t = ar_type_crt_structure ();

	ccl_serializer_read_uint32 (&width, in, p_err);
	while (width-- && *p_err == CCL_SERIALIZER_OK)
	  {
	    ar_type *t = NULL;
	    ar_identifier *id = NULL;
	    ar_identifier_read (&id, in, p_err);
	    ar_type_read (&t, in, p_err);
	    if (*p_err == CCL_SERIALIZER_OK)
	      ar_type_struct_add_field (*p_t, id, t);
	    ccl_zdelete (ar_identifier_del_reference, id);
	    ccl_zdelete (ar_type_del_reference, t);
	  }
      }
      break;

    default:
      *p_err = CCL_SERIALIZER_DATA_ERROR;
    }

 end:
  if (*p_err != CCL_SERIALIZER_OK)
    {
      ccl_zdelete (ar_type_del_reference, *p_t);
      *p_t = NULL;
    }
}

			/* --------------- */

static ar_type *
s_alloc_type (ar_type_kind kind)
{
  ar_type *result = ccl_new (union ar_type_union);

  result->base.kind = kind;
  result->base.refcount = 1;

  return result;
}

			/* --------------- */

static void
s_delete_type(ar_type *type)
{
  switch (KIND_OF (type)) 
    {
    case AR_TYPE_BOOLEANS : case AR_TYPE_INTEGERS : case AR_TYPE_SYMBOLS : 
    case AR_TYPE_RANGE : 
      break;

    case AR_TYPE_ABSTRACT : 
      ar_identifier_del_reference (type->abstract.name);
      break;
      
    case AR_TYPE_ENUMERATION:
      ar_idtable_del_reference (type->enumset.symbols_to_int);      
      ccl_array_delete (type->enumset.int_to_symbols);
      break;

    case AR_TYPE_SYMBOL_SET :
      ccl_bittable_delete (type->symbols.all_symbols);
      break;
      
    case AR_TYPE_ARRAY :
      ar_type_del_reference (type->array.type_of_elements);
      break;

    case AR_TYPE_BANG :
      s_clear_bang (&type->bang);
      break;

    case AR_TYPE_STRUCTURE :
      s_clear_structure (&type->structure);
      break;
    };

  ccl_delete (type);
}

			/* --------------- */

#if 0
static int
s_enum_set_equals (const struct enum_set_type *e1, 
		   const struct enum_set_type *e2)
{
  int i, max = s_enum_set_get_size (e1);

  if (max != s_enum_set_get_size (e2))
    return 0;

  for (i = 0; i < max; i++)
    {
      if (e1->int_to_symbols[i] != e2->int_to_symbols[i])
	return 0;
    }
  
  return 1;
}

			/* --------------- */

typedef struct enum_set_type_iterator_st 
{
  ccl_int_iterator super;
  ar_identifier_iterator *keys;
  struct enum_set_type *e;
} enum_set_iterator;

			/* --------------- */

static int
s_enum_set_iterator_has_more_elements (const ccl_int_iterator *i)
{
  enum_set_iterator *esi = (enum_set_iterator *) i;

  ccl_pre (esi != NULL);

  return ccl_iterator_has_more_elements (esi->keys);
}

			/* --------------- */

static int
s_enum_set_iterator_next_element (ccl_int_iterator *i)
{
  int result;
  ar_identifier *val;
  enum_set_iterator *esi = (enum_set_iterator *) i;

  ccl_pre (esi != NULL);

  val = ccl_iterator_next_element (esi->keys);
  ccl_assert (ar_idtable_has (esi->e->symbols_to_int, val));
  result = (int) ar_idtable_get (esi->e->symbols_to_int, val);
  ar_identifier_del_reference (val);

  return result;
}

			/* --------------- */

static void
s_enum_set_iterator_delete_iterator (ccl_int_iterator *i)
{
  enum_set_iterator *esi = (enum_set_iterator *) i;

  ccl_pre (i != NULL);
  ccl_iterator_delete (esi->keys);
  ccl_delete (i);
}

			/* --------------- */

static ccl_int_iterator ENUM_SET_ITERATOR = {
  s_enum_set_iterator_has_more_elements,
  s_enum_set_iterator_next_element,
  s_enum_set_iterator_delete_iterator
};
	
			/* --------------- */

static ccl_int_iterator *
s_enum_set_get_values (struct enum_set_type *e)
{
  enum_set_iterator *result = ccl_new (enum_set_iterator);

  result->super = ENUM_SET_ITERATOR;
  result->keys = ar_idtable_get_keys (e->symbols_to_int);
  result->e = e;

  return (ccl_int_iterator *) result;
}
#endif
