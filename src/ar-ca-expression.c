/*
 * ar-ca-expression.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
# include <sat/glucose.h>
# include <sat/bitblaster.h>
#include "ar-solver.h"
#include "ar-ca-domain.h"
#include "ar-ca-expression.h"


#define HTABLE_FILL_DEGREE 5
#define MAX_PREDEF_INTEGER_CONSTANT 10

#define CST_INT  0
#define CST_BOOL 1
#define CST_ENUM 2

struct ar_ca_expression_st {
  ar_ca_expr *next;
  int refcount;
  ar_ca_exprman *man;
  ar_ca_expression_kind kind;
  ar_ca_expr *args[3];
  ar_bounds bounds;  
  int cst_type;
  ar_ca_domain *domain;
  ar_identifier *name;
  ccl_list *attrs;
  unsigned int hval;
};

struct ar_ca_expression_manager_st {
  int refcount;
  ar_ca_expr **htable;
  int htable_size;
  int nb_expressions;

  ar_idtable *variables;
  
  ar_ca_expr *symbolic_constants;
  int nb_symbolic_constants;
  int last_symbol_value;

  ar_ca_expr *boolean_constants[2];
  ar_ca_expr *predef_integer_constants[2*MAX_PREDEF_INTEGER_CONSTANT+1];
};

			/* --------------- */

static ar_ca_expr *
s_crt_cst(ar_ca_exprman *man, int type, int value);

static void
s_expr_delete(ar_ca_expr *e);

static uint32_t
s_expr_hash(ar_ca_expr *e);

static int
s_expr_equals(ar_ca_expr *e, ar_ca_expr *other);

static ar_ca_expr *
s_find_or_add_expr(ar_ca_expr *e);

static ar_ca_expr *
s_crt_expr(ar_ca_exprman *man, ar_ca_expression_kind kind,
	   ar_ca_expr *arg1, ar_ca_expr *arg2, ar_ca_expr *arg3);

static void
s_exprman_remove_expr(ar_ca_exprman *man, ar_ca_expr *e);

			/* --------------- */

ar_ca_exprman *
ar_ca_exprman_create(void)
{
  int i;
  ar_ca_exprman *result = ccl_new(ar_ca_exprman);

  result->refcount = 1;
  result->htable = ccl_new_array(ar_ca_expr *,1);
  result->htable_size = 1;
  result->nb_expressions = 0;

  result->variables = ar_idtable_create (0, NULL, NULL);

  result->symbolic_constants = NULL;
  result->nb_symbolic_constants = 0;
  result->last_symbol_value = 0;

  result->boolean_constants[0] = s_crt_cst(result,CST_BOOL,0);
  result->boolean_constants[1] = s_crt_cst(result,CST_BOOL,1);

  for(i = -MAX_PREDEF_INTEGER_CONSTANT; i <= MAX_PREDEF_INTEGER_CONSTANT; i++)
    {
      ar_ca_expr *expr = s_crt_cst(result,CST_INT,i);
      expr = s_find_or_add_expr(expr);
      result->predef_integer_constants[i+MAX_PREDEF_INTEGER_CONSTANT] = expr;
    }


  return result;
}

			/* --------------- */

ar_ca_exprman *
ar_ca_exprman_add_reference(ar_ca_exprman *man)
{
  ccl_pre(man != NULL );

  man->refcount++;

  return man;
}

			/* --------------- */

void
ar_ca_exprman_del_reference(ar_ca_exprman *man)
{
  ccl_pre( man != NULL ); ccl_pre( man->refcount > 0 );

  man->refcount--;
  if (man->refcount == 0)
    {
      ar_ca_expr *e;
      ar_ca_expr *next;
      int i;

      for(i = 0; i < 2*MAX_PREDEF_INTEGER_CONSTANT+1; i++)
	ar_ca_expr_del_reference(man->predef_integer_constants[i]);

      ccl_assert( man->nb_expressions == 0 );

      ar_ca_expr_del_reference(man->boolean_constants[0]);
      ar_ca_expr_del_reference(man->boolean_constants[1]);
      for(e = man->symbolic_constants; e; e = next)
	{ next = e->next; ar_ca_expr_del_reference(e); }

      ccl_assert (ar_idtable_get_size (man->variables) == 0);
      ccl_assert( man->symbolic_constants == NULL );

      ar_idtable_del_reference (man->variables);
      
      ccl_delete(man->htable);
      ccl_delete(man);
    }
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_boolean_constant(ar_ca_exprman *man, int value)
{
  ccl_pre( man != NULL );
  ccl_pre( man->boolean_constants[value?1:0] != NULL );

  return ar_ca_expr_add_reference(man->boolean_constants[value?1:0]);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_integer_constant(ar_ca_exprman *man, int val)
{
  ar_ca_expr *result;

  ccl_pre( man != NULL );

  if( -MAX_PREDEF_INTEGER_CONSTANT <= val && 
      val <= MAX_PREDEF_INTEGER_CONSTANT )
    {
      result = man->predef_integer_constants[val+MAX_PREDEF_INTEGER_CONSTANT];
      result = ar_ca_expr_add_reference(result);
    }
  else 
    {
      result = s_crt_cst(man,CST_INT,val);
      result = s_find_or_add_expr(result);
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_enum_constant(ar_ca_exprman *man, ar_identifier *name)
{
  ar_ca_expr **pe;
  ar_ca_expr *result;
  int cmp = 0;

  ccl_pre( man != NULL ); ccl_pre( name != NULL ); 

  for(pe = &man->symbolic_constants; *pe; pe = &((*pe)->next))
    {
      if( (cmp = ar_identifier_compare(name,(*pe)->name)) <= 0 )
	break;
    }
  
  if( *pe == NULL || cmp < 0 )
    {
      result = s_crt_cst(man,CST_ENUM,man->last_symbol_value++);
      result->next = *pe;
      result->name = ar_identifier_add_reference(name);
      result->hval = s_expr_hash (result);
      *pe = result;
      man->nb_symbolic_constants++;
    }
  result = ar_ca_expr_add_reference(*pe);

  return result;
}

			/* --------------- */

uint32_t
ar_ca_expr_get_number_of_enum_constants (ar_ca_exprman *man)
{
  ccl_pre (man != NULL);
  
  return man->nb_symbolic_constants;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_get_enum_constant_by_name(ar_ca_exprman *man, ar_identifier *name)
{
  ar_ca_expr *e;
  ar_ca_expr *result = NULL;

  ccl_pre( man != NULL ); ccl_pre( name != NULL ); 

  for(e = man->symbolic_constants; e && ! result && name <= e->name; 
      e = e->next)
    {
      if( name == e->name  )
	result = ar_ca_expr_add_reference(e);
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_get_enum_constant_by_index(ar_ca_exprman *man, int value)
{
  ar_ca_expr *e;

  ccl_pre( man != NULL ); ccl_pre( value >= 0 );

  for(e = man->symbolic_constants; e; e = e->next)
    {
      if( e->bounds.min == value )
	return ar_ca_expr_add_reference(e);
    }

  return NULL;
}

			/* --------------- */


ar_ca_expr *
ar_ca_expr_crt_variable(ar_ca_exprman *man, ar_identifier *name,
			ar_ca_domain *domain, ccl_list *attrs)
{
  ar_ca_expr *result = NULL;

  ccl_pre( man != NULL ); ccl_pre( name != NULL );

  result = ar_idtable_get (man->variables, name);
  if (result != NULL)
    return ar_ca_expr_add_reference (result);

  result = ccl_new(ar_ca_expr);

  result->man = man;
  result->next = NULL;
  result->refcount = 1;
  result->kind = AR_CA_VAR;
  result->domain = ar_ca_domain_add_reference(domain);
  result->name = ar_identifier_add_reference(name);
  if (attrs)
    result->attrs =
      ccl_list_deep_dup (attrs,
			 (ccl_duplicate_func *) ar_identifier_add_reference);
  else
    result->attrs = ccl_list_create ();
  result->hval = s_expr_hash (result);

  ar_idtable_put (man->variables, name, result);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_get_variable_by_name(ar_ca_exprman *man, ar_identifier *name)
{
  ccl_pre (man != NULL);
  ar_ca_expr *res = ar_idtable_get (man->variables, name);
  if (res != NULL)
    res = ar_ca_expr_add_reference (res);
  
  return res;
}

			/* --------------- */

static void
s_collect_op_rec (ar_ca_expr *e, ccl_list *res, ccl_set *cache,
		  ar_ca_expression_kind kind)
{
  if (e->kind == kind)
    {
      s_collect_op_rec (e->args[0], res, cache, kind);
      s_collect_op_rec (e->args[1], res, cache, kind);
    }
  else if (! ccl_set_has (cache, e))
    {      
      ccl_list_add (res, ar_ca_expr_add_reference (e));
      ccl_set_add (cache, e);
    }
}

static ccl_list *
s_collect_op (ar_ca_expression_kind kind, ar_ca_expr *l, ar_ca_expr *r)
{
  ccl_set *cache = ccl_set_create ();
  ccl_list *result = ccl_list_create ();
  s_collect_op_rec (l, result, cache, kind);
  s_collect_op_rec (r, result, cache, kind);
  ccl_set_delete (cache);
  
  return result;
}

static ar_ca_expr *
s_balance_op (ar_ca_expression_kind kind, ar_ca_expr *l, ar_ca_expr *r)
{
  ar_ca_expr *arg1;
  ar_ca_expr *arg2;
  ar_ca_expr *aux;
  ccl_list *ops = s_collect_op (kind, l, r);
  ar_ca_expr *result;
  
  ccl_pre (ccl_list_get_size (ops) >= 2);
  
  while (ccl_list_get_size (ops) >= 2)
    {
      arg1 = ccl_list_take_first (ops);
      arg2 = ccl_list_take_first (ops);
      aux = s_crt_expr (l->man, kind, arg1, arg2, NULL);
      ccl_list_add (ops, aux);
      ar_ca_expr_del_reference (arg1);
      ar_ca_expr_del_reference (arg2);
    }
  result = ccl_list_take_first (ops);
  ccl_list_delete (ops);
  
  return result;
}


ar_ca_expr *
ar_ca_expr_crt_and(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *result;

  ccl_pre (lop != NULL); 
  ccl_pre (rop != NULL); 

  if (lop->kind == AR_CA_CST)
    {
      if (lop->man->boolean_constants[0] == lop)
	result = ar_ca_expr_crt_boolean_constant (lop->man, 0);
      else 
	result = ar_ca_expr_add_reference (rop);
    }
  else if (rop->kind == AR_CA_CST)
    {
      if (rop->man->boolean_constants[0] == rop)
	result = ar_ca_expr_crt_boolean_constant (rop->man, 0);
      else 
	result = ar_ca_expr_add_reference (lop);
    }
  else if (lop == rop)
    result = ar_ca_expr_add_reference (lop);
  else if (lop->kind == AR_CA_NOT && lop->args[0] == rop)
    result = ar_ca_expr_crt_boolean_constant (lop->man, 0);
  else if (rop->kind == AR_CA_NOT && rop->args[0] == lop)
    result = ar_ca_expr_crt_boolean_constant (lop->man, 0);
  else if (lop->kind == AR_CA_AND || rop->kind == AR_CA_AND)
    result = s_balance_op (AR_CA_AND, lop, rop);
  else
    result = s_crt_expr (lop->man, AR_CA_AND, lop, rop, NULL);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_or(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *result;

  ccl_pre (lop != NULL); 
  ccl_pre (rop != NULL); 

  if (lop->kind == AR_CA_CST)
    {
      if (lop->man->boolean_constants[0] == lop)
	result = ar_ca_expr_add_reference (rop);
      else 
	result = ar_ca_expr_crt_boolean_constant (lop->man, 1);
    }
  else if (rop->kind == AR_CA_CST)
    {
      if (rop->man->boolean_constants[0] == rop)
	result = ar_ca_expr_add_reference (lop);
      else 
	result = ar_ca_expr_crt_boolean_constant (rop->man, 1);
    }
  else if (lop == rop)
    result = ar_ca_expr_add_reference (lop);
  else if (lop->kind == AR_CA_NOT && lop->args[0] == rop)
    result = ar_ca_expr_crt_boolean_constant (lop->man, 1);
  else if (rop->kind == AR_CA_NOT && rop->args[0] == lop)
    result = ar_ca_expr_crt_boolean_constant (lop->man, 1);
  else if (lop->kind == AR_CA_OR || rop->kind == AR_CA_OR)
    result = s_balance_op (AR_CA_OR, lop, rop);
  else
    result = s_crt_expr (lop->man, AR_CA_OR, lop, rop, NULL);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_imply(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *not_lop = ar_ca_expr_crt_not(lop);
  ar_ca_expr *result = ar_ca_expr_crt_or(not_lop,rop);
  ar_ca_expr_del_reference(not_lop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_not(ar_ca_expr *op)
{
  ar_ca_expr *result;
  
  ccl_pre( op != NULL ); 

  if (op->kind == AR_CA_CST)
    {
      if (op->bounds.min) 
	result = ar_ca_expr_crt_boolean_constant (op->man, 0);
      else
	result = ar_ca_expr_crt_boolean_constant (op->man, 1);      
    }
  else if (op->kind == AR_CA_NOT)
    result = ar_ca_expr_add_reference (op->args[0]);
  else
    result = s_crt_expr (op->man, AR_CA_NOT, op, NULL, NULL);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_neg(ar_ca_expr *op)
{
  ar_ca_expr *result;

  ccl_pre( op != NULL ); 
  
  if (op->kind == AR_CA_CST)
    result = ar_ca_expr_crt_integer_constant (op->man, -op->bounds.min);
  else if (op->kind == AR_CA_NEG)
    result = ar_ca_expr_add_reference (op->args[0]);
  else if (op->kind == AR_CA_ITE)
    {
      ar_ca_expr *tmp1 = ar_ca_expr_crt_neg (op->args[1]);
      ar_ca_expr *tmp2 = ar_ca_expr_crt_neg (op->args[2]);
      result = ar_ca_expr_crt_ite (op->args[0], tmp1, tmp2);
      ar_ca_expr_del_reference (tmp1);
      ar_ca_expr_del_reference (tmp2);
    }
  else
    result = s_crt_expr (op->man, AR_CA_NEG, op, NULL, NULL);

  return result;
}

			/* --------------- */

#if 0
ar_ca_expr *
ar_ca_expr_crt_min(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *lop_lt_rop = ar_ca_expr_crt_lt(lop,rop);
  ar_ca_expr *result = ar_ca_expr_crt_ite(lop_lt_rop,lop,rop);
  ar_ca_expr_del_reference(lop_lt_rop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_max(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *lop_lt_rop = ar_ca_expr_crt_lt(lop,rop);
  ar_ca_expr *result = ar_ca_expr_crt_ite(lop_lt_rop,rop,lop);
  ar_ca_expr_del_reference(lop_lt_rop);

  return result;
}
#endif
			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_mod(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ccl_pre( lop != NULL ); ccl_pre( rop != NULL ); 

  return s_crt_expr(lop->man,AR_CA_MOD,lop,rop,NULL);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_add (ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *result;

  ccl_pre (lop != NULL); 
  ccl_pre (rop != NULL); 

  if (lop->kind == AR_CA_CST && rop->kind == AR_CA_CST)
    {
      int val = (lop->bounds.min + rop->bounds.min);
      result = ar_ca_expr_crt_integer_constant (lop->man, val);
    }
  else if (lop->kind == AR_CA_CST && lop->bounds.min == 0)
    {
      result = ar_ca_expr_add_reference (rop);
    }
  else if (rop->kind == AR_CA_CST && rop->bounds.min == 0)
    {
      result = ar_ca_expr_add_reference (lop);
    }
  else if (lop->kind == AR_CA_ITE)
    {
      ar_ca_expr *tmp1 = ar_ca_expr_crt_add (lop->args[1], rop);
      ar_ca_expr *tmp2 = ar_ca_expr_crt_add (lop->args[2], rop);
      result = ar_ca_expr_crt_ite (lop->args[0], tmp1, tmp2);
      ar_ca_expr_del_reference (tmp1);
      ar_ca_expr_del_reference (tmp2);
    }
  else if (rop->kind == AR_CA_ITE)
    {
      ar_ca_expr *tmp1 = ar_ca_expr_crt_add (lop, rop->args[1]);
      ar_ca_expr *tmp2 = ar_ca_expr_crt_add (lop, rop->args[2]);
      result = ar_ca_expr_crt_ite (rop->args[0], tmp1, tmp2);
      ar_ca_expr_del_reference (tmp1);
      ar_ca_expr_del_reference (tmp2);
    }
  else
    {
      result = s_crt_expr (lop->man, AR_CA_ADD, lop, rop, NULL);
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_sub(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *neg_rop = ar_ca_expr_crt_neg(rop);
  ar_ca_expr *result = ar_ca_expr_crt_add(lop,neg_rop);
  ar_ca_expr_del_reference(neg_rop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_mul(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *result;
  
  ccl_pre (lop != NULL);
  ccl_pre (rop != NULL); 

  if (lop->kind == AR_CA_CST && lop->bounds.min == 0)
    {
      result = ar_ca_expr_add_reference (lop);
    }
  else if (rop->kind == AR_CA_CST && rop->bounds.min == 0)
    {
      result = ar_ca_expr_add_reference (rop);
    }
  else if (lop->kind == AR_CA_CST && lop->bounds.min == 1)
    {
      result = ar_ca_expr_add_reference (rop);
    }
  else if (lop->kind == AR_CA_CST && lop->bounds.min == -1)
    {
      result = ar_ca_expr_crt_neg (rop);
    }
  else if (rop->kind == AR_CA_CST && rop->bounds.min == 1)
    {
      result = ar_ca_expr_add_reference (lop);
    }
  else if (rop->kind == AR_CA_CST && rop->bounds.min == -1)
    {
      result = ar_ca_expr_crt_neg (lop);
    }
  else
    {
      result = s_crt_expr (lop->man, AR_CA_MUL, lop, rop, NULL);
    }
  
  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_div(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *result;
  
  ccl_pre (lop != NULL);
  ccl_pre (rop != NULL); 

  if (rop->kind == AR_CA_CST && rop->bounds.min == 1)
    {
      result = ar_ca_expr_add_reference (lop);
    }
  else if (rop->kind == AR_CA_CST && rop->bounds.min == -1)
    {
      result = ar_ca_expr_crt_neg (lop);
    }
  else
    {
      result = s_crt_expr (lop->man, AR_CA_DIV, lop, rop, NULL);
    }
  return result;
}

			/* --------------- */

static ar_ca_expr *
s_crt_eq (ar_ca_expr *lop, ar_ca_expr *rop, int is_assign)
{
  ar_ca_expr *result = NULL;

  ccl_pre (lop != NULL); 
  ccl_pre (rop != NULL); 

  if (lop == rop)
    result = ar_ca_expr_crt_boolean_constant (lop->man, 1);
  else if (lop->kind == AR_CA_CST && rop->kind == AR_CA_CST)
    result = ar_ca_expr_crt_boolean_constant (lop->man, 0);
  else if (lop->kind == AR_CA_CST || rop->kind == AR_CA_CST)
    {
      ar_ca_expr *cst = lop->kind == AR_CA_CST ? lop : rop;
      ar_ca_expr *e = lop->kind == AR_CA_CST ? rop : lop;

      if (cst->cst_type == CST_BOOL) 
	{
	  if (cst->bounds.min)
	    result = ar_ca_expr_add_reference (e);
	  else
	    result = ar_ca_expr_crt_not (e);
	}
    }

  if (is_assign && result == NULL &&
      (lop->kind == AR_CA_ITE || rop->kind == AR_CA_ITE))
    {
      ar_ca_expr *eq[2];
      ar_ca_expr *ite;
      ar_ca_expr *op;

      if (lop->kind == AR_CA_ITE)
	{
	  ite = lop;
	  op = rop;
	}
      else
	{ 
	  ite = rop;
	  op = lop;
	}
      eq[0] = ar_ca_expr_crt_eq (ite->args[1], op);
      eq[1] = ar_ca_expr_crt_eq (ite->args[2], op);
      result = ar_ca_expr_crt_ite (ite->args[0], eq[0], eq[1]);
      ar_ca_expr_del_reference (eq[0]);
      ar_ca_expr_del_reference (eq[1]);
    }

  if (result == NULL)
    result = s_crt_expr (lop->man, AR_CA_EQ, lop, rop, NULL);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_eq_for_assign (ar_ca_expr *lop, ar_ca_expr *rop)
{
  return s_crt_eq (lop, rop, 1);
}

ar_ca_expr *
ar_ca_expr_crt_eq (ar_ca_expr *lop, ar_ca_expr *rop)
{
  return s_crt_eq (lop, rop, 0);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_neq(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *lop_eq_rop = ar_ca_expr_crt_eq(lop,rop);
  ar_ca_expr *result = ar_ca_expr_crt_not(lop_eq_rop);
  ar_ca_expr_del_reference(lop_eq_rop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_leq(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *lop_eq_rop = ar_ca_expr_crt_eq(lop,rop);
  ar_ca_expr *lop_lt_rop = ar_ca_expr_crt_lt(lop,rop);
  ar_ca_expr *result = ar_ca_expr_crt_or(lop_eq_rop,lop_lt_rop);
  ar_ca_expr_del_reference(lop_eq_rop);
  ar_ca_expr_del_reference(lop_lt_rop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_geq(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *lop_lt_rop = ar_ca_expr_crt_lt(lop,rop);
  ar_ca_expr *result = ar_ca_expr_crt_not(lop_lt_rop);
  ar_ca_expr_del_reference(lop_lt_rop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_lt(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ccl_pre( lop != NULL ); ccl_pre( rop != NULL ); 

  return s_crt_expr(lop->man,AR_CA_LT,lop,rop,NULL);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_gt(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *lop_leq_rop = ar_ca_expr_crt_leq(lop,rop);
  ar_ca_expr *result = ar_ca_expr_crt_not(lop_leq_rop);
  ar_ca_expr_del_reference(lop_leq_rop);

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_min (ar_ca_expr *lop, ar_ca_expr *rop)
{
  ccl_pre (lop != NULL); 
  ccl_pre (rop != NULL); 

  return s_crt_expr (lop->man, AR_CA_MIN, lop, rop, NULL);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_max (ar_ca_expr *lop, ar_ca_expr *rop)
{
  ccl_pre (lop != NULL); 
  ccl_pre (rop != NULL); 

  return s_crt_expr (lop->man, AR_CA_MAX, lop, rop, NULL);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_exist(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ar_ca_expr *result = NULL;

  ccl_pre( lop != NULL ); ccl_pre( rop != NULL ); 
  ccl_pre( lop->kind == AR_CA_VAR );

  if( ar_ca_domain_is_boolean(lop->domain) )
    {
      ar_ca_expr *tt = ar_ca_expr_crt_boolean_constant(lop->man,1);
      ar_ca_expr *ff = ar_ca_expr_crt_boolean_constant(lop->man,0);
      ar_ca_expr *aux1 = ar_ca_expr_replace(rop,lop,tt);
      ar_ca_expr *aux2 = ar_ca_expr_replace(rop,lop,ff);
      result = ar_ca_expr_crt_or(aux1,aux2);
      ar_ca_expr_del_reference(tt);
      ar_ca_expr_del_reference(ff);
      ar_ca_expr_del_reference(aux1);
      ar_ca_expr_del_reference(aux2);
    }  
  else if( ar_ca_domain_is_integer(lop->domain) )
    {
      int i;
      ar_bounds bounds;
      ar_ca_domain_get_bounds(lop->domain,&bounds);
      for(i = bounds.min; i <= bounds.max; i++)
	{
	  ar_ca_expr *icst = ar_ca_expr_crt_integer_constant(lop->man,i);
	  ar_ca_expr *aux1 = ar_ca_expr_replace(rop,lop,icst);

	  if( result == NULL )
	    result = aux1;
	  else
	    {
	      ar_ca_expr *aux2 = ar_ca_expr_crt_or(result,aux1);
	      ar_ca_expr_del_reference(aux1);
	      ar_ca_expr_del_reference(result);
	      result = aux2;
	    }
	  ar_ca_expr_del_reference(icst);
	}
    }
  else
    {
      int i, sz;
      const int *values = ar_ca_domain_get_enum_values(lop->domain,&sz);

      for(i = 0; i < sz; i++)
	{
	  ar_ca_expr *icst = 
	    ar_ca_expr_get_enum_constant_by_index(lop->man,values[i]);
	  ar_ca_expr *aux1 = ar_ca_expr_replace(rop,lop,icst);

	  if( result == NULL )
	    result = aux1;
	  else
	    {
	      ar_ca_expr *aux2 = ar_ca_expr_crt_or(result,aux1);
	      ar_ca_expr_del_reference(aux1);
	      ar_ca_expr_del_reference(result);
	      result = aux2;
	    }
	  ar_ca_expr_del_reference(icst);
	}
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_forall(ar_ca_expr *lop, ar_ca_expr *rop)
{
  ccl_pre( lop != NULL ); ccl_pre( rop != NULL ); 

  return ar_ca_expr_crt_not(ar_ca_expr_crt_exist(lop,ar_ca_expr_crt_not(rop)));
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_ite(ar_ca_expr *i, ar_ca_expr *t, ar_ca_expr *e)
{
  ar_ca_expr *result;

  ccl_pre( i != NULL ); ccl_pre( t != NULL );   ccl_pre( e != NULL ); 

  if (t == e)
    result = ar_ca_expr_add_reference (t);
  else if (i == i->man->boolean_constants[1])
    result = ar_ca_expr_add_reference (t);
  else if (i == i->man->boolean_constants[0])
    result = ar_ca_expr_add_reference (e);
  else if (t == t->man->boolean_constants[1])
    result = ar_ca_expr_crt_or (i, e);
  else if (t == t->man->boolean_constants[0])
    {
      ar_ca_expr *tmp = ar_ca_expr_crt_not (i);
      result = ar_ca_expr_crt_and (tmp, e);
      ar_ca_expr_del_reference (tmp);
    }
  else if (e == e->man->boolean_constants[1])
    {
      ar_ca_expr *tmp = ar_ca_expr_crt_not (i);
      result = ar_ca_expr_crt_or (tmp, t);
      ar_ca_expr_del_reference (tmp);
    }
  else if (e == e->man->boolean_constants[0])
    result = ar_ca_expr_crt_and (i, t); 
  else
    result = s_crt_expr (i->man, AR_CA_ITE, i, t, e);
  

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_in_domain(ar_ca_expr *e, const ar_ca_domain *dom)
{
  ar_ca_expr *result;

  ccl_pre( e != NULL ); ccl_pre( dom != NULL );

  if( ar_ca_domain_is_boolean(dom) )
    result = ar_ca_expr_crt_boolean_constant(e->man,1);
  else if( ar_ca_domain_is_integer(dom) )
    {
      ar_bounds b;
      ar_ca_expr *cst;
      ar_ca_expr *geqmin;
      ar_ca_expr *leqmax;

      ar_ca_domain_get_bounds(dom,&b);

      cst = ar_ca_expr_crt_integer_constant(e->man,b.min);
      geqmin = ar_ca_expr_crt_geq(e,cst);
      ar_ca_expr_del_reference(cst);

      cst = ar_ca_expr_crt_integer_constant(e->man,b.max);
      leqmax = ar_ca_expr_crt_leq(e,cst);      
      ar_ca_expr_del_reference(cst);

      result = ar_ca_expr_crt_and(geqmin,leqmax);
      ar_ca_expr_del_reference(geqmin);
      ar_ca_expr_del_reference(leqmax);
    }
  else
    {
      int i, size;
      const int *values = ar_ca_domain_get_enum_values(dom,&size);
      ar_ca_expr *tmp1;
      ar_ca_expr *tmp2;
      
      tmp1 = ar_ca_expr_get_enum_constant_by_index(e->man,values[0]);
      result = ar_ca_expr_crt_eq(e,tmp1);
      ar_ca_expr_del_reference(tmp1);

      for(i = 1; i < size ;i++)
	{
	  tmp1 = ar_ca_expr_get_enum_constant_by_index(e->man,values[i]);
	  tmp2 = ar_ca_expr_crt_eq(e,tmp1);
	  ar_ca_expr_del_reference(tmp1);
	  tmp1 = ar_ca_expr_crt_or(result,tmp2);
	  ar_ca_expr_del_reference(tmp2);
	  ar_ca_expr_del_reference(result);
	  result = tmp1;
	}
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_crt_in_domain_of (ar_ca_expr *e, const ar_ca_expr *var)
{
  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);

  return ar_ca_expr_crt_in_domain (e, dom);
}

ar_ca_expr *
ar_ca_expr_add_reference(ar_ca_expr *e)
{
  ccl_pre( e != NULL );
  
  e->refcount++;

  return e;
}

			/* --------------- */

void
ar_ca_expr_del_reference(ar_ca_expr *e)
{
  ccl_pre( e != NULL ); ccl_pre( e->refcount > 0 );
  
  if( --e->refcount == 0 )
    s_exprman_remove_expr(e->man,e);
}

			/* --------------- */

ar_ca_exprman *
ar_ca_expr_get_manager (ar_ca_expr *e)
{
  ccl_pre (e != NULL);

  return ar_ca_exprman_add_reference (e->man);
}

			/* --------------- */

ar_identifier *
ar_ca_expr_variable_get_name(const ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  return ar_identifier_add_reference(var->name);
}

			/* --------------- */

const ar_identifier *
ar_ca_expr_variable_get_const_name(const ar_ca_expr *var)
{
  ccl_pre (var != NULL && var->kind == AR_CA_VAR);

  return var->name;
}
  

const ar_ca_domain *
ar_ca_expr_variable_get_domain(const ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  return var->domain;
}

			/* --------------- */

ccl_list *
ar_ca_expr_variable_get_attributes (const ar_ca_expr *var)
{
  ccl_pre (var != NULL && var->kind == AR_CA_VAR);

  return var->attrs;
}

			/* --------------- */

ar_ca_domain *
ar_ca_expr_variable_get_not_const_domain(const ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  return ar_ca_domain_add_reference(var->domain);
}

			/* --------------- */

int
ar_ca_expr_variable_is_boolean(const ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  return ar_ca_domain_is_boolean(var->domain);
}

			/* --------------- */

int
ar_ca_expr_variable_is_enum(const ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  return ar_ca_domain_is_enum(var->domain);
}

			/* --------------- */

int
ar_ca_expr_variable_is_integer(const ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  return ar_ca_domain_is_integer(var->domain);
}

			/* --------------- */

void
ar_ca_expr_variable_reset_bounds(ar_ca_expr *var)
{
  ccl_pre( var != NULL && var->kind == AR_CA_VAR );

  ar_ca_domain_get_bounds(var->domain,&var->bounds);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_variable_get_ith_value (const ar_ca_expr *var, int i)
{
  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
  int val = ar_ca_domain_get_ith_value (dom, i);
  ar_ca_expr *result;

  if (ar_ca_domain_is_boolean (dom))
    result = ar_ca_expr_crt_boolean_constant (var->man, val);
  else if (ar_ca_domain_is_integer (dom))
    result = ar_ca_expr_crt_integer_constant (var->man, val);
  else 
    result = ar_ca_expr_get_enum_constant_by_index (var->man, val);

  return result;
}

			/* --------------- */

void
ar_ca_expr_set_bounds(ar_ca_expr *e, int min, int max)
{
  ccl_pre( e != NULL ); ccl_pre( min <= max );

  ar_bounds_set(&e->bounds,min,max);
}

			/* --------------- */

void
ar_ca_expr_set_bounds_to_ith_domain_value (ar_ca_expr *e, int i)
{
  int val;

  ccl_pre (e != NULL); 
  ccl_pre (0 <= i && i < ar_ca_domain_get_cardinality (e->domain));

  val = ar_ca_domain_get_ith_value (e->domain, i);
  ar_bounds_set (&e->bounds, val, val);
}

			/* --------------- */

ar_bounds *
ar_ca_expr_get_bounds_address(ar_ca_expr *e)
{
  ccl_pre( e != NULL ); 

  return &e->bounds;
}

			/* --------------- */

ar_ca_expr **
ar_ca_expr_get_args(ar_ca_expr *e)
{
  ccl_pre( e != NULL );

  return e->args;
}

			/* --------------- */

ar_ca_expression_kind
ar_ca_expr_get_kind(ar_ca_expr *e)
{
  ccl_pre( e != NULL );

  return e->kind;
}

			/* --------------- */

ar_ca_exprman *
ar_ca_expr_get_expression_manager(ar_ca_expr *e)
{
  ccl_pre( e != NULL );

  return ar_ca_exprman_add_reference(e->man);
}

			/* --------------- */

void
ar_ca_expr_reset_bounds(ar_ca_expr *e)
{
  ccl_pre( e != NULL ); ccl_pre( e->kind == AR_CA_VAR );

  ar_ca_domain_get_bounds(e->domain,&e->bounds);
}

			/* --------------- */

void
ar_ca_expr_save_bounds(ar_ca_expr *e, ar_backtracking_stack *bs)
{
  ccl_pre( e != NULL ); 
  
  ar_bounds_save(&e->bounds,bs);
}

			/* --------------- */

int
ar_ca_expr_enum_constant_get_index(const ar_ca_expr *cst)
{
  ccl_pre( cst != NULL );

  return cst->bounds.min;
}

			/* --------------- */

ar_identifier *
ar_ca_expr_enum_constant_get_name(const ar_ca_expr *cst)
{
  ccl_pre( cst != NULL );

  return ar_identifier_add_reference(cst->name);
}

			/* --------------- */

void
ar_ca_expr_log (ccl_log_type log, const ar_ca_expr *expr)
{
  ar_ca_expr_log_gen (log, expr, ".", NULL);
}

			/* --------------- */

void
ar_ca_expr_log_quoted (ccl_log_type log, const ar_ca_expr *expr)
{
  ar_ca_expr_log_gen (log, expr, ".", "''");
}

			/* --------------- */

void
ar_ca_expr_log_gen (ccl_log_type log, const ar_ca_expr *expr, 
		    const char *separator, const char *quotes)
{
  switch (expr->kind) 
    {
    case AR_CA_CST: 
      if (expr->cst_type == CST_INT) 
	ccl_log (log, "%d", expr->bounds.min);
      else if (expr->cst_type == CST_BOOL) 
	ccl_log (log, expr->bounds.min ? "true" : "false");
      else if (quotes)	
	ar_identifier_log_global_quote_gen (log, expr->name, separator, quotes);
      else
	ar_identifier_log (log, expr->name);
      break;

    case AR_CA_VAR:
      if (quotes)
	ar_identifier_log_global_quote_gen (log, expr->name, separator, quotes);
      else
	ar_identifier_log (log, expr->name);
      break;

    case AR_CA_ITE:
      ccl_log (log, "(if ");
      ar_ca_expr_log_gen (log, expr->args[0], separator, quotes);
      ccl_log (log, " then ");
      ar_ca_expr_log_gen (log, expr->args[1], separator, quotes);
      ccl_log (log, " else ");
      ar_ca_expr_log_gen (log, expr->args[2], separator, quotes);
      ccl_log (log, ")");
      break;

    case AR_CA_AND: case AR_CA_OR: case AR_CA_ADD: case AR_CA_MUL: 
    case AR_CA_DIV: 
      {
	const char *op;
	int pleft = (expr->args[0]->kind != expr->kind && 
		     expr->args[0]->kind != AR_CA_CST && 
		     expr->args[0]->kind != AR_CA_NOT && 
		     expr->args[0]->kind != AR_CA_VAR);
	int pright = (expr->args[1]->kind != expr->kind && 
		      expr->args[1]->kind != AR_CA_CST && 
		      expr->args[1]->kind != AR_CA_NOT && 
		      expr->args[1]->kind != AR_CA_VAR);
	switch (expr->kind) 
	  {
	  case AR_CA_AND: op = "and"; break;
	  case AR_CA_OR:  op = "or"; break;
	  case AR_CA_ADD: op = "+"; break;
	  case AR_CA_MUL: op = "*"; break;
	  case AR_CA_DIV: op = "/"; break;
	  case AR_CA_MOD: op = "%"; break;
	  case AR_CA_EQ:  op = "="; break;
	  default :
	    ccl_assert (expr->kind == AR_CA_LT); 
	    op = "<"; 
	    break;	
	  };

	if (pleft)
	  ccl_log (log, "(");	
	ar_ca_expr_log_gen (log, expr->args[0], separator, quotes);
	if (pleft)
	  ccl_log (log, ")");	
	ccl_log (log, " %s ", op);
	if (pright)
	  ccl_log (log, "(");	
	ar_ca_expr_log_gen (log, expr->args[1], separator, quotes);
	if (pright)
	  ccl_log (log, ")");	
      }
      break;

    case AR_CA_MOD: case AR_CA_EQ: case AR_CA_LT: 
      {
	const char *op;

	switch (expr->kind) 
	  {
	  case AR_CA_AND: op = "and"; break;
	  case AR_CA_OR:  op = "or"; break;
	  case AR_CA_ADD: op = "+"; break;
	  case AR_CA_MUL: op = "*"; break;
	  case AR_CA_DIV: op = "/"; break;
	  case AR_CA_MOD: op = "%"; break;
	  case AR_CA_EQ:  op = "="; break;
	  default :
	    ccl_assert (expr->kind == AR_CA_LT); 
	    op = "<"; 
	    break;	
	  };

	ccl_log (log, "(");
	ar_ca_expr_log_gen (log, expr->args[0], separator, quotes);
	ccl_log (log, " %s ", op);
	ar_ca_expr_log_gen (log, expr->args[1], separator, quotes);
	ccl_log (log, ")");
      }
      break;

    case AR_CA_MIN: case AR_CA_MAX:
      ccl_log (log, (expr->kind == AR_CA_MIN) ? "min" : "max");
      ccl_log (log, "(");
      ar_ca_expr_log_gen (log, expr->args[0], separator, quotes);
      ccl_log (log, ", ");
      ar_ca_expr_log_gen (log, expr->args[1], separator, quotes);
      ccl_log (log, ")");
      break;

    case AR_CA_NOT: case AR_CA_NEG:
      ccl_log (log, "%s", expr->kind == AR_CA_NOT ? "not ":"- ");
      if (expr->args[0]->kind != AR_CA_CST && expr->args[0]->kind != AR_CA_VAR)
	ccl_log (log, "(");
      ar_ca_expr_log_gen (log, expr->args[0], separator, quotes);
      if (expr->args[0]->kind != AR_CA_CST && expr->args[0]->kind != AR_CA_VAR)
	ccl_log (log, ")");
      break;
    };
}

			/* --------------- */

int
ar_ca_expr_get_min(const ar_ca_expr *expr)
{
  ccl_pre( expr != NULL );

  return expr->bounds.min;
}

			/* --------------- */

int
ar_ca_expr_get_max(const ar_ca_expr *expr)
{
  ccl_pre( expr != NULL );

  return expr->bounds.max;
}

			/* --------------- */

void
ar_ca_expr_evaluate(ar_ca_expr *expr, int rec)
{
  if (rec && (expr->kind == AR_CA_AND || expr->kind == AR_CA_OR))
    {
      int i0 = 0;
      int i1 = (i0 + 1) & 0x1;
      int val = (expr->kind == AR_CA_AND) ? 0 : 1;
      
      ar_ca_expr_evaluate (expr->args[i0], rec);
      if (ar_bounds_is_singleton (&expr->args[i0]->bounds) && 
	  expr->args[i0]->bounds.min == val)
	{
	  ar_bounds_set (&expr->bounds, val, val);
	  return;
	}
      else
	ar_ca_expr_evaluate (expr->args[i1], rec);
    }
  else if( rec && expr->kind != AR_CA_CST && expr->kind != AR_CA_VAR )
    {      
      int i;

      for(i = 0; i < 3; i++) 
	{
	  if( expr->args[i] )
	    ar_ca_expr_evaluate(expr->args[i],rec);
	}
    }

  switch( expr->kind ) {
  case AR_CA_CST: case AR_CA_VAR:
    break;
  case AR_CA_NEG:
    ar_bounds_neg(&expr->bounds,&expr->args[0]->bounds);
    break;
  case AR_CA_NOT:
    ar_bounds_not(&expr->bounds,&expr->args[0]->bounds);
    break;
  case AR_CA_AND:    
    ar_bounds_and(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_OR:
    ar_bounds_or(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_ADD:
    ar_bounds_add(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_MUL:
    ar_bounds_mul(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_DIV:
    ar_bounds_div(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_MOD:
    ar_bounds_mod(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_MIN:
    ar_bounds_min (&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_MAX:
    ar_bounds_max (&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_EQ:
    ar_bounds_eq(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_LT:
    ar_bounds_lt(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds);
    break;
  case AR_CA_ITE: 
    ar_bounds_ite(&expr->bounds,&expr->args[0]->bounds,&expr->args[1]->bounds,
		  &expr->args[2]->bounds);
    break;
  };
}

static int
s_evaluate (ar_ca_expr *expr)
{
  int val;
  
  switch( expr->kind ) {
  case AR_CA_CST: case AR_CA_VAR:
    return expr->bounds.min;
  case AR_CA_NEG:
    val = -expr->args[0]->bounds.min;
    break;
  case AR_CA_NOT:
    val = expr->args[0]->bounds.min ? 0 : 1;
    break;
  case AR_CA_AND:
    val = expr->args[0]->bounds.min && expr->args[1]->bounds.min;
    break;
  case AR_CA_OR:
    val = expr->args[0]->bounds.min || expr->args[1]->bounds.min;
    break;
  case AR_CA_ADD:
    val = expr->args[0]->bounds.min + expr->args[1]->bounds.min;
    break;
  case AR_CA_MUL:
    val = expr->args[0]->bounds.min * expr->args[1]->bounds.min;
    break;
  case AR_CA_DIV:
    val = expr->args[0]->bounds.min / expr->args[1]->bounds.min;
    break;
  case AR_CA_MOD:
    val = expr->args[0]->bounds.min % expr->args[1]->bounds.min;
    break;
  case AR_CA_MIN:
    val = (expr->args[0]->bounds.min < expr->args[1]->bounds.min ?
	   expr->args[0]->bounds.min : expr->args[1]->bounds.min);
    break;
  case AR_CA_MAX:
    val = (expr->args[0]->bounds.min < expr->args[1]->bounds.min ?
	   expr->args[1]->bounds.min : expr->args[0]->bounds.min);
    break;
  case AR_CA_EQ:
    val = (expr->args[0]->bounds.min == expr->args[1]->bounds.min);
    break;
  case AR_CA_LT:
    val = (expr->args[0]->bounds.min < expr->args[1]->bounds.min);
    break;
  case AR_CA_ITE:
    val = (expr->args[0]->bounds.min
	   ? expr->args[0]->bounds.min : expr->args[1]->bounds.min);
    break;
  };
  expr->bounds.min = expr->bounds.max = val;
  
  return val;
}

int
ar_ca_expr_evaluate_min (ar_ca_expr *expr, int rec)
{
  if (rec && (expr->kind == AR_CA_AND || expr->kind == AR_CA_OR))
    {
      int i0 = 0;
      int i1 = (i0 + 1) & 0x1;
      int val = (expr->kind == AR_CA_AND) ? 0 : 1;
      
      ar_ca_expr_evaluate_min (expr->args[i0], rec);
      if (expr->args[i0]->bounds.min == val)
	{
	  ar_bounds_set (&expr->bounds, val, val);
	  return val;
	}
      else
	ar_ca_expr_evaluate_min (expr->args[i1], rec);
    }
  else if( rec && expr->kind != AR_CA_CST && expr->kind != AR_CA_VAR )
    {      
      int i;

      for(i = 0; i < 3; i++) 
	{
	  if( expr->args[i] )
	    ar_ca_expr_evaluate_min (expr->args[i], rec);
	}
    }
  
  return s_evaluate (expr);
}

void
ar_ca_expr_evaluate_list (ccl_list *l)
{
  ccl_pair *p;

  for (p = FIRST (l); p; p = CDR (p))
    s_evaluate (CAR (p));
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_replace(ar_ca_expr *expr, ar_ca_expr *subterm, 
		   ar_ca_expr *replacement)
{
  ar_ca_expr *result;
  ccl_hash *subst = ccl_hash_create (NULL, NULL, NULL, NULL);

  ccl_hash_find (subst, subterm);
  ccl_hash_insert (subst, replacement);
  result = ar_ca_expr_replace_with_table (expr, subst);
  ccl_hash_delete (subst);

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_replace_with_table (ar_ca_expr *expr, ccl_hash *subst, ccl_hash *cache)
{
  ar_ca_expr *result = NULL;

  if (ccl_hash_find (cache, expr))
    {
      result = ccl_hash_get (cache);
      result = ar_ca_expr_add_reference (result);
    }
  else if (ccl_hash_find (subst, expr))
    {
      result = ccl_hash_get (subst);
      result = ar_ca_expr_add_reference (result);
    }
  else if( expr->kind == AR_CA_CST || expr->kind == AR_CA_VAR )
    result = ar_ca_expr_add_reference(expr);
  else
    {
      int i;
      int nochange = 1;
      ar_ca_expr *args[3] = { NULL, NULL, NULL };

      for(i = 0; i < 3 ; i++) 
	{
	  if( expr->args[i] )
	    {
	      args[i] = s_replace_with_table (expr->args[i], subst, cache);
	      nochange = nochange && (args[i]==expr->args[i]);
	    }
	}

      if( ! nochange )
	{
	  switch( expr->kind ) {
	  case AR_CA_CST: case AR_CA_VAR:
	    ccl_throw_no_msg (internal_error);

	  case AR_CA_NEG:
	    if( args[0]->kind == AR_CA_CST )
	      result = 
		ar_ca_expr_crt_integer_constant(expr->man,
						-args[0]->bounds.min);
	    else if( args[0]->kind == AR_CA_NEG )
	      result = ar_ca_expr_add_reference(args[0]->args[0]);
	    break;

	  case AR_CA_NOT:
	    if( args[0]->kind == AR_CA_CST )
	      result = ar_ca_expr_crt_boolean_constant(expr->man,
						       ! args[0]->bounds.min);
	    else if( args[0]->kind == AR_CA_NOT )
	      result = ar_ca_expr_add_reference(args[0]->args[0]);
	    break;

	  case AR_CA_AND: 
	    if( args[0] == args[1] )
	      result = ar_ca_expr_add_reference(args[0]);
	    else if( args[0]->kind == AR_CA_CST )
	      {
		if( args[0]->bounds.min == 0 )
		  result = ar_ca_expr_crt_boolean_constant(expr->man,0);
		else if( args[0]->bounds.min == 1 )
		  result = ar_ca_expr_add_reference(args[1]);
	      }
	    else if( args[1]->kind == AR_CA_CST )
	      {
		if( args[1]->bounds.min == 0 )
		  result = ar_ca_expr_crt_boolean_constant(expr->man,0);
		else if( args[1]->bounds.min == 1 )
		  result = ar_ca_expr_add_reference(args[0]);
	      }
	    break;

	  case AR_CA_OR:
	    if( args[0] == args[1] )
	      result = ar_ca_expr_add_reference(args[0]);
	    else if( args[0]->kind == AR_CA_CST )
	      {
		if( args[0]->bounds.min == 0 )
		  result = ar_ca_expr_add_reference(args[1]);
		else if( args[0]->bounds.min == 1 )
		  result = ar_ca_expr_crt_boolean_constant(expr->man,1);
	      }
	    else if( args[1]->kind == AR_CA_CST )
	      {
		if( args[1]->bounds.min == 0 )
		  result = ar_ca_expr_add_reference(args[0]);
		else if( args[1]->bounds.min == 1 )
		  result = ar_ca_expr_crt_boolean_constant(expr->man,1);
	      }
	    break;

	  case AR_CA_ADD:
	    result = ar_ca_expr_crt_add (args[0], args[1]);
	    break;
	  case AR_CA_MUL:
	    result = ar_ca_expr_crt_mul (args[0], args[1]);
	    break;
	  case AR_CA_DIV:
	    result = ar_ca_expr_crt_div (args[0], args[1]);
	    break;
	  case AR_CA_MOD:
	    break;

	  case AR_CA_EQ:
	    result = ar_ca_expr_crt_eq (args[0], args[1]);
	    break;
	    
	  case AR_CA_LT:
	    break;
	  case AR_CA_ITE: 
	    result = ar_ca_expr_crt_ite (args[0], args[1], args[2]);
	    break;
	  case AR_CA_MIN: 
	    break;
	  case AR_CA_MAX: 
	    break;
	  };
	}

      if (result == NULL)
	result = s_crt_expr(expr->man,expr->kind,args[0],args[1],args[2]);

      for(i = 0; i < 3 ; i++) 
	ccl_zdelete(ar_ca_expr_del_reference,args[i]);

      if (! ccl_hash_find (cache, expr))
	ccl_hash_insert (cache, ar_ca_expr_add_reference (result));
    }

  return result;
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_replace_with_table (ar_ca_expr *expr, ccl_hash *subst)
{
  ar_ca_expr *result;

  if (ccl_hash_get_size (subst) == 0)
    result = ar_ca_expr_add_reference (expr);
  else
    {
      ccl_hash *cache = 
	ccl_hash_create (NULL, NULL, NULL,
			 (ccl_delete_proc *) ar_ca_expr_del_reference);
      result = s_replace_with_table (expr, subst, cache);
      ccl_hash_delete (cache);
    }

  return result;
}

			/* --------------- */

int
ar_ca_expr_is_boolean_expr(ar_ca_expr *expr)
{
  switch( expr->kind ) {
  case AR_CA_AND: case AR_CA_OR: case AR_CA_NOT: case AR_CA_LT: case AR_CA_EQ:
    return 1;
  case AR_CA_CST:
    return   expr->cst_type == CST_BOOL;
  case AR_CA_VAR:
    return ar_ca_domain_is_boolean(expr->domain);
  case AR_CA_ITE:
    return ar_ca_expr_is_boolean_expr(expr->args[1]);
  default:
    return 0;
  }
}

			/* --------------- */

int
ar_ca_expr_is_enum_expr (ar_ca_expr *expr)
{
  switch( expr->kind ) {
  case AR_CA_CST:
    return  expr->cst_type == CST_ENUM;
  case AR_CA_VAR:
    return ar_ca_domain_is_enum(expr->domain);
  case AR_CA_ITE:
    return ar_ca_expr_is_enum_expr(expr->args[1]);
  default:
    return 0;
  }
}

			/* --------------- */

int
ar_ca_expr_is_integer_expr (ar_ca_expr *expr)
{
  switch (expr->kind) 
    {
    case AR_CA_ADD: case AR_CA_NEG: case AR_CA_MUL: case AR_CA_DIV: 
    case AR_CA_MOD: case AR_CA_MIN: case AR_CA_MAX:
      return 1;
    case AR_CA_CST:
      return expr->cst_type == CST_INT;
    case AR_CA_VAR:
      return ar_ca_domain_is_integer (expr->domain);
    case AR_CA_ITE:
      return ar_ca_expr_is_integer_expr (expr->args[1]);
    default:
      return 0;
    }
}

			/* --------------- */

static void
s_collect_variables(ar_ca_expr *expr, ccl_list *list, ccl_hash *table,
		    ccl_hash *cache)
{
  if (ccl_hash_find (cache, expr))
    return;

  switch( expr->kind ) 
    {
    case AR_CA_CST: 
      break;

    case AR_CA_VAR:
      if (table && ! ccl_hash_find (table, expr))
	ccl_hash_insert (table, expr);
      if (list)
	ccl_list_add (list, expr);
      break;

    default:
      if (expr->args[0]) 
	s_collect_variables (expr->args[0], list, table, cache);
      if (expr->args[1]) 
	s_collect_variables (expr->args[1], list, table, cache);
      if (expr->args[2]) 
	s_collect_variables (expr->args[2], list, table, cache);
      break;
    }
  
  ccl_hash_find (cache, expr);
  ccl_hash_insert (cache, expr);
}

			/* --------------- */

ccl_list *
ar_ca_expr_get_variables (ar_ca_expr *expr, ccl_list *result)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);

  if (result != NULL)
    {
      ccl_pair *p;
      for (p = FIRST (result); p; p = CDR (p))
	{
	  ccl_hash_find (cache, CAR (p));
	  ccl_hash_insert (cache, CAR (p));
	}
    }
  else
    {
      result = ccl_list_create ();
    }

  s_collect_variables (expr, result, NULL, cache);
  ccl_hash_delete (cache);

  return result;
}

			/* --------------- */

ccl_hash *
ar_ca_expr_get_variables_in_table (ar_ca_expr *expr, ccl_hash *result)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);

  if (result == NULL)
    result = ccl_hash_create (NULL, NULL, NULL, NULL);

  s_collect_variables (expr, NULL, result, cache);
  ccl_hash_delete (cache);
  
  return result;
}

int
s_expr_has_variable_rec (ar_ca_expr *expr, ar_ca_expr *var, ccl_hash *cache,
			 ccl_set *vars)
{
  intptr_t result;

  if (ccl_hash_find (cache, expr))
    return (intptr_t) ccl_hash_get (cache);

  result = 0;
  if (expr == var || (vars != NULL && ccl_set_has (vars, expr)))
    result = 1;
  else if(! (expr->kind == AR_CA_CST || expr->kind == AR_CA_VAR))
    {
      if (expr->args[0])
	result = s_expr_has_variable_rec (expr->args[0], var, cache, vars);
      if (!result && expr->args[1]) 
	result = s_expr_has_variable_rec (expr->args[1], var, cache, vars);
      if (! result && expr->args[2]) 
	result = s_expr_has_variable_rec (expr->args[2], var, cache, vars);
    }    

  ccl_hash_find (cache, expr);
  ccl_hash_insert (cache, (void *) result);
      
  return result;
  
}

int
ar_ca_expr_has_variable (ar_ca_expr *expr, ar_ca_expr *var)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
  int result = s_expr_has_variable_rec (expr, var, cache, NULL);
  ccl_hash_delete (cache);

  return result;
}

int
ar_ca_expr_has_variable_in_table (ar_ca_expr *expr, ccl_set *vars)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
  int result = s_expr_has_variable_rec (expr, NULL, cache, vars);
  ccl_hash_delete (cache);

  return result;
}


			/* --------------- */

int
ar_ca_expr_nb_solutions (ar_ca_expr *e)
{
  return ar_solver_count_solutions (&e, 1, -1);
}

			/* --------------- */

ar_ca_expr *
ar_ca_expr_negate (ar_ca_expr *e)
{
  ar_ca_expr *result;
  ar_ca_expr *tmpargs[3] = { NULL, NULL, NULL };
  ccl_pre (ar_ca_expr_is_boolean_expr (e));

  switch (ar_ca_expr_get_kind (e))
    {
    case AR_CA_CST:      
      result = ar_ca_expr_crt_boolean_constant (e->man, 
						(1 + e->bounds.min) % 2);
      break;
  
    case AR_CA_VAR:
      result = ar_ca_expr_crt_not (e);
      break;

    case AR_CA_AND:
      tmpargs[0] = ar_ca_expr_negate (e->args[0]);
      tmpargs[1] = ar_ca_expr_negate (e->args[1]);
      result = ar_ca_expr_crt_or (tmpargs[0], tmpargs[1]);
      break;

    case AR_CA_OR:
      tmpargs[0] = ar_ca_expr_negate (e->args[0]);
      tmpargs[1] = ar_ca_expr_negate (e->args[1]);
      result = ar_ca_expr_crt_and (tmpargs[0], tmpargs[1]);
      break;
    case AR_CA_NOT:
      result = ar_ca_expr_add_reference (e->args[0]);
      break;

    case AR_CA_EQ: 
      result = ar_ca_expr_crt_neq (e->args[0], e->args[1]);
      break;

    case AR_CA_LT:
      result = ar_ca_expr_crt_geq (e->args[0], e->args[1]);
      break;

    case AR_CA_ITE:
      tmpargs[0] = ar_ca_expr_add_reference (e->args[0]);
      tmpargs[1] = ar_ca_expr_negate (e->args[1]);
      tmpargs[2] = ar_ca_expr_negate (e->args[2]);
      result = ar_ca_expr_crt_ite (tmpargs[0], tmpargs[1], tmpargs[2]);
      break;

    case AR_CA_NEG: case AR_CA_ADD: case AR_CA_MUL: case AR_CA_DIV: 
    case AR_CA_MOD: case AR_CA_MIN: case AR_CA_MAX:
    default:
      ccl_unreachable ();
    }
  ccl_zdelete (ar_ca_expr_del_reference, tmpargs[0]);
  ccl_zdelete (ar_ca_expr_del_reference, tmpargs[1]);
  ccl_zdelete (ar_ca_expr_del_reference, tmpargs[2]);

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_crt_cst(ar_ca_exprman *man, int type, int value)
{
  ar_ca_expr *result = ccl_new(ar_ca_expr);

  result->next = NULL;
  result->refcount = 1;
  result->man = man;
  result->kind = AR_CA_CST;
  result->args[0] = result->args[1] = result->args[2] = NULL;
  ar_bounds_init_singleton(&result->bounds,value);
  result->cst_type = type;
  result->domain = NULL;
  result->name = NULL;
  result->hval = s_expr_hash (result);

  return result;
}

			/* --------------- */

static void
s_expr_delete(ar_ca_expr *e)
{
  ccl_pre( e != NULL ); ccl_pre( e->refcount == 0 );

  ccl_zdelete(ar_identifier_del_reference,e->name);
  ccl_zdelete(ar_ca_domain_del_reference,e->domain);
  ccl_delete(e);
}


			/* --------------- */

static uint32_t
s_expr_hash(ar_ca_expr *e)
{
  uint32_t result = 13 * e->kind;

  ccl_pre (e != NULL);

  if (e->kind != AR_CA_CST && e->kind != AR_CA_VAR)
    {
      if (e->args[0]) 
	result = 19 * result + e->args[0]->hval;
      if (e->args[1]) 
	result = 19 * result + e->args[1]->hval;
      if (e->args[2]) 
	result = 19 * result + e->args[2]->hval;
    }

  if (e->kind == AR_CA_CST)
    result = 111 * result + e->bounds.min;

  if (e->domain != NULL)
    result = 471 * result + ar_ca_domain_hash (e->domain);

  if (e->name != NULL)
    result = 123 * result + ar_identifier_hash (e->name);

  if (e->kind != AR_CA_VAR)
    result *= (e->cst_type + 17);

  return result;
}

			/* --------------- */

static int
s_expr_equals(ar_ca_expr *e, ar_ca_expr *other)
{
  int result;

  ccl_pre( e != NULL ); ccl_pre( other != NULL );

  if( e->kind != other->kind )
    result = 0;
  else if( e->kind == AR_CA_CST )
    {
      result = e->cst_type == other->cst_type
	&&  ar_bounds_are_equal(&e->bounds,&other->bounds);
    }
  else if( e->kind == AR_CA_VAR )
    {
      result = ar_ca_domain_equals(e->domain,other->domain)
	&&     e->name == other->name;
    }
  else
    {
      result = e->args[0] == other->args[0]
	&&     e->args[1] == other->args[1]
	&&     e->args[2] == other->args[2];
    }

  return result;
}

			/* --------------- */

static ar_ca_expr **
s_find_expr (ar_ca_expr *e)
{
  ar_ca_expr **pe;
  ar_ca_exprman *man = e->man;
  uint32_t index = e->hval % man->htable_size;

  for (pe = man->htable + index; *pe; pe = &((*pe)->next))
    {
      if (s_expr_equals (e, *pe))
	break;
    }
  return pe;
}

			/* --------------- */

static ar_ca_expr *
s_find_or_add_expr(ar_ca_expr *e)
{
  ar_ca_expr **pe = s_find_expr (e);
  ar_ca_exprman *man = e->man;

  if (*pe != NULL)
    {
      ccl_zdelete (ar_ca_expr_del_reference, e->args[0]);
      ccl_zdelete (ar_ca_expr_del_reference, e->args[1]);
      ccl_zdelete (ar_ca_expr_del_reference, e->args[2]);
      e->refcount--;
      s_expr_delete(e);

      return ar_ca_expr_add_reference(*pe);
    }

  *pe = e;
  man->nb_expressions++;

  if( man->nb_expressions > HTABLE_FILL_DEGREE*man->htable_size )
    {
      int i;
      int new_size = 2*man->htable_size+1;
      ar_ca_expr **new_table = ccl_new_array(ar_ca_expr *,new_size);

      for(i = 0; i < man->htable_size; i++)
	{
	  ar_ca_expr *e;
	  ar_ca_expr *next;

	  for(e = man->htable[i]; e; e = next)
	    {
	      uint32_t index = e->hval % new_size;

	      next = e->next;
	      e->next = new_table[index];
	      new_table[index] = e;
	    }
	}
      ccl_delete(man->htable);
      man->htable = new_table;
      man->htable_size = new_size;
    }

  return e;
}

			/* --------------- */

static ar_ca_expr *
s_crt_expr (ar_ca_exprman *man, ar_ca_expression_kind kind,
	    ar_ca_expr *arg1, ar_ca_expr *arg2, ar_ca_expr *arg3)
{
  ar_ca_expr *result = ccl_new (ar_ca_expr);

  result->next = NULL;
  result->refcount = 1;
  result->man = man;
  result->kind = kind;
  result->args[0] = arg1 ? ar_ca_expr_add_reference (arg1) : NULL;
  result->args[1] = arg2 ? ar_ca_expr_add_reference (arg2) : NULL;
  result->args[2] = arg3 ? ar_ca_expr_add_reference (arg3) : NULL;
  ar_bounds_init (&result->bounds, 0, 0);
  result->cst_type = 0;
  result->domain = NULL;
  result->name = NULL;
  result->hval = s_expr_hash (result);

  return s_find_or_add_expr (result);
}

			/* --------------- */

static void
s_exprman_remove_expr(ar_ca_exprman *man, ar_ca_expr *e)
{
  ar_ca_expr **pe = NULL;

  if( e->kind == AR_CA_CST && e->cst_type != CST_INT )
    {
      if( e->cst_type == CST_ENUM )
	{
	  pe = &man->symbolic_constants;
	  man->nb_symbolic_constants--;
	}
    }
  else if( e->kind == AR_CA_VAR )
    {
      pe = NULL;
      ar_idtable_remove (man->variables, e->name);
    }
  else
    {
      uint32_t index = e->hval % man->htable_size;
      pe = man->htable+index; 
      man->nb_expressions--;	      
    }

  if( pe != NULL )
    {
      for(; *pe && *pe != e; pe = &((*pe)->next))
	continue;

      ccl_assert( *pe != NULL );
      *pe = e->next;
    }

  ccl_zdelete(ar_ca_expr_del_reference,e->args[0]);
  ccl_zdelete(ar_ca_expr_del_reference,e->args[1]);
  ccl_zdelete(ar_ca_expr_del_reference,e->args[2]);
  if (e->attrs != NULL)
    ccl_list_clear_and_delete (e->attrs, (ccl_delete_proc *)
			       ar_identifier_del_reference);

  s_expr_delete(e);
}

			/* --------------- */

static void
s_collect_clauses (ar_ca_expr *e, ccl_list *res)
{
  if (ar_ca_expr_get_kind (e) == AR_CA_OR)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      s_collect_clauses (args[0], res);
      s_collect_clauses (args[1], res);
    }
  else
    {
      e = ar_ca_expr_add_reference (e);
      ccl_list_add (res, e);
    }
}

			/* --------------- */

static ar_ca_expr * 
s_rewrite_to_dnf (ar_ca_expr *e, ccl_hash *cache)
{
  ar_ca_expr *res = NULL;

  if (cache && ccl_hash_find (cache, e))
    {
      res = ccl_hash_get (cache);
      return ar_ca_expr_add_reference (res);
    }

  if (ar_ca_expr_get_kind (e) == AR_CA_OR)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      ar_ca_expr *op1 = s_rewrite_to_dnf (args[0], cache);
      ar_ca_expr *op2 = s_rewrite_to_dnf (args[1], cache);
      res = ar_ca_expr_crt_or (op1, op2);
      ar_ca_expr_del_reference (op1);
      ar_ca_expr_del_reference (op2);
    }
  else if (ar_ca_expr_get_kind (e) == AR_CA_AND)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);

      if (ar_ca_expr_get_kind (args[0]) == AR_CA_OR)
	{
	  ar_ca_expr **orargs = ar_ca_expr_get_args (args[0]);
	  ar_ca_expr *op1 = ar_ca_expr_crt_and (orargs[0], args[1]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_and (orargs[1], args[1]);
	  res = ar_ca_expr_crt_or (op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
      else if (ar_ca_expr_get_kind (args[1]) == AR_CA_OR)
	{
	  ar_ca_expr **orargs = ar_ca_expr_get_args (args[1]);
	  ar_ca_expr *op1 = ar_ca_expr_crt_and (args[0], orargs[0]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_and (args[0], orargs[1]);
	  res = ar_ca_expr_crt_or (op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
      else
	{
	  ar_ca_expr *op1 = s_rewrite_to_dnf (args[0], cache);
	  ar_ca_expr *op2 = s_rewrite_to_dnf (args[1], cache);
	  res = ar_ca_expr_crt_and (op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
    }
  else if (ar_ca_expr_get_kind (e) == AR_CA_NOT)
    {
      ar_ca_expr *op = ar_ca_expr_get_args (e)[0];

      if (ar_ca_expr_get_kind (op) == AR_CA_NOT)
	{
	  res = s_rewrite_to_dnf (ar_ca_expr_get_args (op)[0], cache);
	}
      else if (ar_ca_expr_get_kind (op) == AR_CA_OR)
	{
	  ar_ca_expr **args = ar_ca_expr_get_args (op);
	  ar_ca_expr *op1 = ar_ca_expr_crt_not (args[0]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_not (args[1]);
	  res = ar_ca_expr_crt_and (op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
      else if (ar_ca_expr_get_kind (op) == AR_CA_AND)
	{
	  ar_ca_expr **args = ar_ca_expr_get_args (op);
	  ar_ca_expr *op1 = ar_ca_expr_crt_not (args[0]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_not (args[1]);
	  res = ar_ca_expr_crt_or (op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
      else if (ar_ca_expr_get_kind (op) == AR_CA_ITE)
	{
	  ar_ca_expr **args = ar_ca_expr_get_args (op);
	  ar_ca_expr *op1 = ar_ca_expr_crt_not (args[1]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_not (args[2]);
	  res = ar_ca_expr_crt_ite (args[0], op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
    }
  else if (ar_ca_expr_get_kind (e) == AR_CA_ITE)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      if (ar_ca_expr_get_kind (args[0]) == AR_CA_NOT)
	{
	  res = ar_ca_expr_crt_ite (ar_ca_expr_get_args (args[0])[0],
				    args[2], args[1]);
	}
      else
	{
	  ar_ca_expr *op1 = ar_ca_expr_crt_and (args[0], args[1]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_not (args[0]);
	  ar_ca_expr *op3 = ar_ca_expr_crt_and (op2, args[2]);
	  res = ar_ca_expr_crt_or (op1, op3);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	  ar_ca_expr_del_reference (op3);
	}
    }
  else if (ar_ca_expr_get_kind (e) == AR_CA_EQ)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      if (ar_ca_expr_get_kind (args[0]) == AR_CA_ITE)
	{
	  ar_ca_expr **iteargs = ar_ca_expr_get_args (args[0]);
	  ar_ca_expr *op1 = ar_ca_expr_crt_eq (iteargs[1], args[1]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_eq (iteargs[2], args[1]);
	  
	  res = ar_ca_expr_crt_ite (iteargs[0], op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
      else if (ar_ca_expr_get_kind (args[1]) == AR_CA_ITE)
	{
	  ar_ca_expr **iteargs = ar_ca_expr_get_args (args[1]);
	  ar_ca_expr *op1 = ar_ca_expr_crt_eq (iteargs[1], args[0]);
	  ar_ca_expr *op2 = ar_ca_expr_crt_eq (iteargs[2], args[0]);	  
	  res = ar_ca_expr_crt_ite (iteargs[0], op1, op2);
	  ar_ca_expr_del_reference (op1);
	  ar_ca_expr_del_reference (op2);
	}
    }

  if (res == NULL)
    res = ar_ca_expr_add_reference (e);

  if (res != e)
    {
      ar_ca_expr *tmp = s_rewrite_to_dnf (res, NULL);
      ar_ca_expr_del_reference (res);
      res = tmp;
    }

  ccl_assert (res != NULL);

  if (cache)
    {
      ccl_hash_find (cache, ar_ca_expr_add_reference (e));
      ccl_hash_insert (cache, ar_ca_expr_add_reference (res));
    }

  return res;
}

			/* --------------- */

ccl_list * 
ar_ca_expr_get_dnf_clauses (ar_ca_expr *e)
{
  ccl_hash *cache = 
    ccl_hash_create (NULL, NULL, 
		     (ccl_delete_proc *) ar_ca_expr_del_reference, 
		     (ccl_delete_proc *) ar_ca_expr_del_reference);
  ccl_list *l = ccl_list_create ();

  e = ar_ca_expr_add_reference (e);
  for (;;)
    {
      ar_ca_expr *tmp = s_rewrite_to_dnf (e, cache);  
      ar_ca_expr_del_reference (e);
      if (tmp == e)
	break;
      else 
	e = tmp;
    }

  s_collect_clauses (e, l);
  ar_ca_expr_del_reference (e);
  ccl_hash_delete (cache);

  return l;
}

			/* --------------- */


struct node_info
{
  int start;
  int end;
  int min;
  int max;
  int last;
};

static struct node_info *
s_compute_modules (ar_ca_expr *e, ccl_hash *dates, ccl_list *result)
{
  struct node_info *ni;
  struct node_info *nic;

  if (!ccl_hash_find (dates, e))
    {
      ccl_assert (e->kind == AR_CA_CST);
      return NULL;
    }

  ni = ccl_hash_get (dates);
  
  if (ni->min >= 0)
    return ni;
  ccl_assert (ni->last >= ni->end);
  ni->min = ni->start;
  ni->max = ni->last;

  switch (e->kind)
    {
    case AR_CA_VAR:
      break;
      
    case AR_CA_ITE:
      if ((nic = s_compute_modules (e->args[2], dates, result)))
	{
	  if (nic->min < ni->min)
	    ni->min = nic->min;
	  if (nic->max > ni->max)
	    ni->max = nic->max;
	}
	
    case AR_CA_MIN: case AR_CA_MAX: case AR_CA_AND: case AR_CA_OR:
    case AR_CA_ADD: case AR_CA_MUL: case AR_CA_DIV : case AR_CA_MOD:
    case AR_CA_EQ: case AR_CA_LT:
      if ((nic = s_compute_modules (e->args[1], dates, result)))
	{
	  if (nic->min < ni->min)
	    ni->min = nic->min;
	  if (nic->max > ni->max)
	    ni->max = nic->max;
	}

    case AR_CA_NOT:
    case AR_CA_NEG:
      if ((nic = s_compute_modules (e->args[0], dates, result)))
	{
	  if (nic->min < ni->min)
	    ni->min = nic->min;
	  if (nic->max > ni->max)
	    ni->max = nic->max;
	}
      break;

    default:
      ccl_throw_no_msg (internal_error);
    }

  if (ni->start <= ni->min && ni->max <= ni->end)
    ccl_list_add (result, e);
    
  return ni;
}

			/* --------------- */

static void
s_compute_dates (ar_ca_expr *e, ccl_hash *dates, int *counter)
{
  struct node_info *ni;

  if (e->kind == AR_CA_CST)
    return;

  (*counter)++;
  if (ccl_hash_find (dates, e))
    {
      ni = ccl_hash_get (dates);
      ni->last = *counter;
      return;
    }


  ni = ccl_new (struct node_info);
  ni->start = *counter;
  ni->min = ni->max = -1;
  ni->end = ni->min = ni->max = ni->last = -1;
  ccl_hash_insert (dates, ni);

  switch (e->kind)
    {
    case AR_CA_CST:
    case AR_CA_VAR:
      break;

    case AR_CA_ITE:
      s_compute_dates (e->args[2], dates, counter);

    case AR_CA_MIN: case AR_CA_MAX: case AR_CA_AND: case AR_CA_OR:
    case AR_CA_ADD: case AR_CA_MUL: case AR_CA_DIV : case AR_CA_MOD:
    case AR_CA_EQ: case AR_CA_LT:
      s_compute_dates (e->args[1], dates, counter);

    case AR_CA_NOT:
    case AR_CA_NEG:
      s_compute_dates (e->args[0], dates, counter);
      (*counter)++;
      break;

    default:
      ccl_throw_no_msg (internal_error);
    }
  ni->end = ni->last = *counter;
}

			/* --------------- */

CCL_DEFINE_DELETE_PROC(s_delete_node_info)

ccl_list * 
ar_ca_expr_get_modules (ar_ca_expr *e)
{
  int counter = 0;
  ccl_list *result = ccl_list_create ();
  ccl_hash *dates = ccl_hash_create (NULL, NULL, NULL, s_delete_node_info);

  s_compute_dates (e, dates, &counter);
  s_compute_modules (e, dates, result);
  ccl_hash_delete (dates);

  return result;
}

			/* --------------- */

void
ar_ca_expr_log_list_of_expressions (ccl_log_type log, ccl_list *list)
{
  ccl_pair *p; 

  for (p = FIRST (list); p; p = CDR (p))
    {
      ar_ca_expr_log (log, CAR (p));
      ccl_log (log, "\n");
    }
}

			/* --------------- */

static void
s_collect_subexprs (ar_ca_expr *e, ccl_hash *cache)
{
  if (ccl_hash_find (cache, e))
    return;
  ccl_hash_insert (cache, e);

  if (e->kind != AR_CA_VAR && e->kind != AR_CA_CST)
    {
      if (e->args[0] != NULL)
	s_collect_subexprs (e->args[0], cache);
      if (e->args[1] != NULL)
	s_collect_subexprs (e->args[1], cache);
      if (e->args[2] != NULL)
	s_collect_subexprs (e->args[2], cache);
    }
}

int
ar_ca_expr_get_size (ar_ca_expr *e)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
  int result;
  s_collect_subexprs (e, cache);
  result = ccl_hash_get_size (cache);
  ccl_hash_delete (cache);

  return result;
}

			/* --------------- */


static void
s_add_clause (const bf_literal *variables, int nb_variables, void *data)
{
  glucose_solver *S = data;

  if (nb_variables == 0)
    glucose_solver_add_clause_0 (S);
  else if (nb_variables == 1)
    glucose_solver_add_clause_1 (S, variables[0]);
  else if (nb_variables == 2)
    glucose_solver_add_clause_2 (S, variables[0], variables[1]);
  else if (nb_variables == 3)
    glucose_solver_add_clause_3 (S, variables[0], variables[1], variables[2]);
  else 
    glucose_solver_add_clause (S, variables, nb_variables);  
}

static bf_index 
s_glucose_var_allocator (void *data)
{
  return glucose_solver_new_var (data);
}

int
ar_ca_expr_is_satisfiable (ar_ca_expr *e)
{
  int result = 0;

#if 1
    {
      ccl_list *vars = ar_ca_expr_get_variables (e, NULL);
      glucose_solver *S = glucose_solver_create ();
      ar_ca_exprman *eman = ar_ca_expr_get_expression_manager (e);
      bitblaster *t = bitblaster_create (eman, s_glucose_var_allocator, S);
      bf_manager *bm = bitblaster_get_bf_manager (t);
      varset *V = varset_create ();
      bf_formula *F = bitblaster_translate_boolean_expr (t, V, e);

      while (! ccl_list_is_empty (vars)) 
	{
          ar_ca_expr *v = ccl_list_take_first (vars);
          const ar_ca_domain *D = ar_ca_expr_variable_get_domain (v);
          ar_ca_expr *indom = ar_ca_expr_crt_in_domain (v, D);
          bf_formula *bf = 
	    bitblaster_translate_boolean_expr (t, V, indom);
          F = bf_crt_and (bm, F, bf);
          ar_ca_expr_del_reference (indom);
        }

      if (bf_is_true (F))
	result = 1;
      else if (bf_is_false (F))
	result = 0;
      else
	{
	  bf_to_cnf (bm, F, s_add_clause, S, 1);
	  result = glucose_solver_solve (S);
	}

      bf_unref (F);
      varset_unref (V);
      bitblaster_delete (t);
      ar_ca_exprman_del_reference (eman);
      ccl_list_delete (vars);
      glucose_solver_destroy (S);
    }
#else
    {
      int i = 0;
      ar_solver *solver;  
      ccl_list *variables = ar_ca_expr_get_variables (e, NULL);
      int nb_vars = ccl_list_get_size (variables);
      ar_ca_expr **vars = ccl_new_array (ar_ca_expr *, nb_vars);
      
      while (!ccl_list_is_empty (variables))
	{
	  vars[i] = ccl_list_take_first (variables);
	  ar_ca_expr_variable_reset_bounds (vars[i]);
	  i++;
	}
      ccl_list_delete (variables);
      solver = ar_create_gp_solver (&e, 1, vars, nb_vars, NULL, NULL, 
				    s_new_solution, NULL);
      ar_solver_solve (solver, &result);
      ar_solver_delete (solver);
      ccl_delete (vars);
    }
#endif /* HAVE_GLUCOSE */

  return result;
}

char *
ar_ca_expr_to_string (const ar_ca_expr *e)
{
  char *result = NULL;
  
  ccl_log_redirect_to_string (CCL_LOG_DISPLAY, &result);
  ar_ca_expr_log (CCL_LOG_DISPLAY, e);
  ccl_log_pop_redirection (CCL_LOG_DISPLAY);

  return result;
}

			/* --------------- */


