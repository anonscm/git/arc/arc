/*
 * ar-ca-rewriters.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-bittable.h>
#include <deps/depgraph.h>
#include "ar-ca-expr2dd.h"
#include "ar-ca-p.h"
#include "ar-ca-rewriters.h"

static void
s_rewrite_variables (const fdset *fds, const ar_ca *ca, ar_ca *result)
{
  int k;
  const ccl_list *vars = ar_ca_get_flow_variables (ca);
  void (*adder) (ar_ca *, ar_ca_expr *) = ar_ca_add_flow_variable;

  for (k = 0; k < 2; k++)
    {
      const ccl_pair *p;

      for (p = FIRST (vars); p; p = CDR (p))
	{
	  const funcdep *fd = fdset_get_funcdep (fds, CAR (p));

	  if (fd == NULL)
	    adder (result, CAR (p));
	  else
	    {
	      if (k == 1)
		{
		  ar_ca_expr *sfunc = funcdep_get_function (fd);
		  ar_ca_expr *in_dom =
		    ar_ca_expr_crt_in_domain_of (sfunc, CAR (p));
		  ar_ca_expr *c = ar_ca_expr_simplify (in_dom);
		  if (ar_ca_expr_get_kind (c) != AR_CA_CST || 
		      ar_ca_expr_get_min (c) == 0)
		    ar_ca_add_assertion (result, c);
		  ar_ca_expr_del_reference (c);
		  ar_ca_expr_del_reference (in_dom);
		}
	    }	    
	}

      vars = ar_ca_get_state_variables (ca);
      adder = ar_ca_add_state_variable;
    }  
}

static void
s_rewrite_assertions (const fdset *fds, const ar_ca *ca, ar_ca *result)
{
  const ccl_pair *p;
  const ccl_list *assertions = ar_ca_get_assertions (ca);

  for (p = FIRST (assertions); p; p = CDR (p))
    {
      ar_ca_expr *a = CAR (p);
      ar_ca_expr *newa = ar_ca_rewrite_expr_with_fd (fds, a, 0);
      ar_ca_expr *nota = ar_ca_expr_crt_not (newa);
      
      if (ar_ca_expr_is_satisfiable (nota))
	{
	  if (newa == a || (ar_ca_expr_get_kind (newa) != AR_CA_CST || 
			    ar_ca_expr_get_min(newa) == 0))
	    ar_ca_add_assertion (result, newa);
	}
      ar_ca_expr_del_reference (nota);
      ar_ca_expr_del_reference (newa);
    }
}

static void
s_rewrite_transitions (const fdset *fds, const ar_ca *ca, ar_ca *result)
{
  int i;
  int nb_trans = ar_ca_get_number_of_transitions (ca);
  ar_ca_trans **trans = ar_ca_get_transitions (ca);

  for (i = 0; i < nb_trans; i++)
    {
      int j;
      ar_ca_trans *t = trans[i];
      ar_ca_expr *g = ar_ca_trans_get_guard (t);
      ar_ca_expr *newg= ar_ca_rewrite_expr_with_fd (fds, g, 0);
      ar_ca_event *e = ar_ca_trans_get_event (t);
      ar_ca_trans *nt = ar_ca_trans_create (newg, e);
      int nb_a = ar_ca_trans_get_number_of_assignments (t);
      ar_ca_expr **ass = ar_ca_trans_get_assignments (t);

      for (j = 0; j < nb_a; j++)
	{
	  ar_ca_expr *sv = ass[2 * j];
	  ar_ca_expr *val = ass[2 * j + 1];

	  if (fdset_has_dependency_for (fds, sv))
	    {
	      /* if the assigned state variable appears in fdset we can remove
	       * it from transitions but we have to ensure that assigned val
	       * is in the domain of the removed variable. Without this 
	       * additional guard some new behavior can appear that was forbiden
	       * by domain constraint enforced by assignment.
	       */
	      ar_ca_expr *tmp = ar_ca_expr_crt_in_domain_of (val, sv);
	      val = ar_ca_rewrite_expr_with_fd (fds, tmp, 1);
	      ar_ca_expr_del_reference (tmp);
	      tmp = ar_ca_expr_crt_and (newg, val);
	      ar_ca_expr_del_reference (newg);
	      newg = tmp;
	      ar_ca_trans_set_guard (nt, newg);
	    }
	  else
	    {
	      val = ar_ca_rewrite_expr_with_fd (fds, val, 0);
	      ar_ca_trans_add_assignment (nt, sv, val);
	    }
	  ar_ca_expr_del_reference (val);
	}
      ar_ca_add_transition (result, nt);
      ar_ca_trans_del_reference (nt);
      ar_ca_expr_del_reference (g);
      ar_ca_expr_del_reference (newg);
      ar_ca_event_del_reference (e);
    }
}

ar_ca *
ar_ca_remove_functional_dependencies (const ar_ca *ca, const fdset *fds)
{
  int i;
  ar_ca *result;
  const ccl_pair *p;
  
  ccl_pre (fds != NULL && ca != NULL);

  result = ar_ca_allocate_ca (ca->node, ca->man);
  
  s_rewrite_variables (fds, ca, result);

  for (i = 0; i < ca->events.size; i++)
    ar_ca_add_event (result, ca->events.data[i]);

  for (p = FIRST (ca->initial_assignments); p; p = CDDR (p))
    {
      ar_ca_expr *sv = CAR (p);
      ar_ca_expr *val = CADR (p);
      if (! fdset_has_dependency_for (fds, sv))
	ar_ca_add_initial_assignment (result, sv, val);
    }

  for (p = FIRST (ca->initial_constraints); p; p = CDR (p))
    {
      ar_ca_expr *c = CAR (p);
      ar_ca_expr *nc = ar_ca_rewrite_expr_with_fd (fds, c, 1);
      if (ar_ca_expr_get_kind (nc) != AR_CA_CST || ar_ca_expr_get_min(nc) == 0)
	ar_ca_add_initial_constraint (result, nc);
      ar_ca_expr_del_reference (nc);
    }

  s_rewrite_transitions (fds, ca, result);
  s_rewrite_assertions (fds, ca, result);
  ccl_post (ar_ca_check (result));

  return result;
}

static int
s_expr_intersect_var_set (ar_ca_expr *E, ccl_hash *V, ccl_hash *variables)
{
  ccl_pair *p;
  int intersect_V;
  ccl_list *vars = ar_ca_expr_get_variables (E, NULL);

  for (p = FIRST (vars); p; p = CDR (p))
    {
      if (ccl_hash_find (V, CAR (p)))
	break;
    }

  intersect_V = (p != NULL);

  if (intersect_V)
    {
      while (! ccl_list_is_empty (vars))
	{
	  ar_ca_expr *v = ccl_list_take_first (vars);
	  if (! ccl_hash_find (variables, v))
	    ccl_hash_insert (variables, v);
	}
    }
  ccl_list_delete (vars);

  return intersect_V;
}

static void
s_select_transitions (const ar_ca *ca, ccl_hash *V, ar_ca *result, 
		      ccl_hash *closure)
{
  int i;

  for (i = 0; i < ca->transitions.size; i++)
    {
      ccl_pair *p;
      ar_ca_trans *t = ca->transitions.data[i];
      ccl_list *vars;
      
      vars = ar_ca_trans_get_variables (t, NULL, AR_CA_W_VARS);
      for (p = FIRST (vars); p; p = CDR (p))
	{
	  if (ccl_hash_find (V, CAR (p)))
	    break;
	}

      if (p != NULL)
	{
	  vars = ar_ca_trans_get_variables (t, vars, AR_CA_R_VARS);
	  while (! ccl_list_is_empty (vars))
	    {
	      ar_ca_expr *v = ccl_list_take_first (vars);
	      if (! ccl_hash_find (closure, v))
		ccl_hash_insert (closure, v);
	    }
	  ar_ca_add_transition (result, t);
	}
      ccl_list_delete (vars);
    }
}

static void
s_select_transitions_wired_states (const ar_ca *ca, ccl_hash *V, ar_ca *result,
				   ccl_hash *closure)
{
  ccl_bittable *bt = ccl_bittable_create (ca->transitions.size);
  int Vchanged;

  do
    {  
      int i;
      Vchanged = 0;
      for (i = 0; i < ca->transitions.size; i++)
	{
	  ccl_pair *p;
	  ar_ca_trans *t = ca->transitions.data[i];
	  ccl_list *vars;
      
	  if (ccl_bittable_has (bt, i))
	    continue;

	  vars = ar_ca_trans_get_variables (t, NULL, AR_CA_W_VARS);
	  for (p = FIRST (vars); p; p = CDR (p))
	    {
	      if (ccl_hash_find (V, CAR (p)))
		break;
	    }

	  if (p != NULL)
	    {
	      ccl_bittable_set (bt, i);
	      for (p = FIRST (vars); p; p = CDR (p))
		{
		  if (! ccl_hash_find (V, CAR (p)))
		    {
		      ccl_hash_insert (V, CAR (p));
		      Vchanged = 1;
		    }
		}

	      vars = ar_ca_trans_get_variables (t, vars, AR_CA_R_VARS);
	      while (! ccl_list_is_empty (vars))
		{
		  ar_ca_expr *v = ccl_list_take_first (vars);
		  if (! ccl_hash_find (closure, v))
		    ccl_hash_insert (closure, v);
		}
	      ar_ca_add_transition (result, t);
	    }
	  ccl_list_delete (vars);
	}
    }
  while (Vchanged);
  ccl_bittable_delete (bt);
}

static void
s_add_idle_var (ar_ca *ca)
{
  ar_ca_domain *booleans = ar_ca_domain_create_bool ();
  ar_ca_expr *idle = 
    ar_ca_expr_crt_variable (ca->man, AR_IDLE_VAR, booleans, NULL);
  ar_ca_expr *val = ar_ca_expr_crt_boolean_constant (ca->man, 1);

  ar_ca_add_state_variable (ca, idle);
  ar_ca_add_initial_assignment (ca, idle, val);
  ar_ca_expr_del_reference (val);
  ar_ca_expr_del_reference (idle);
  ar_ca_domain_del_reference (booleans);
}

static void
s_add_epsilon_trans (ar_ca *ca, ar_ca_event *epsilon)
{
  ar_ca_expr *tt = ar_ca_expr_crt_boolean_constant (ca->man, 1);
  ar_ca_trans *T = ar_ca_trans_create (tt, epsilon);

  ar_ca_expr_del_reference (tt);
  ar_ca_add_event (ca, epsilon);
  ar_ca_add_transition (ca, T);
  ar_ca_trans_del_reference (T);
}

ar_ca *
ar_ca_restrict_to_variables (ar_ca *ca, ccl_hash *V, ccl_list **closure,
			     int wired_state_variables)
{
  int i;
  ccl_pair *p;
  ar_ca *result = ar_ca_allocate_ca (ca->node, ca->man);
  ccl_hash *variables = ccl_hash_create (NULL, NULL, NULL, NULL);
  ar_ca_event *epsilon = ar_ca_get_epsilon_event (ca);

  if (wired_state_variables)
    s_select_transitions (ca, V, result, variables);
  else
    s_select_transitions_wired_states (ca, V, result, variables);

  for (p = FIRST (ca->assertions); p; p = CDR (p))
    {
      ar_ca_expr *a = CAR (p);
      if (s_expr_intersect_var_set (a, V, variables))
	ar_ca_add_assertion (result, a);      
    }

  for (p = FIRST (ca->initial_assignments); p; p = CDDR (p))
    {
      ar_ca_expr *v = CAR (p);

      if (ccl_hash_find (V, v))
	{ 
	  ar_ca_expr *val = CADR (p);
	  ar_ca_add_initial_assignment (result, v, val);
	}
    }

  for (p = FIRST (ca->initial_constraints); p; p = CDR (p))
    {
      ar_ca_expr *c = CAR (p);
      if (s_expr_intersect_var_set (c, V, variables))
	ar_ca_add_initial_constraint (result, c); 
    }
  
  {
    ccl_pointer_iterator *pi;
    ccl_hash *events = ccl_hash_create (NULL, NULL, NULL, NULL);
    for (i = 0; i < result->transitions.size; i++)
      {
	ar_ca_event *e = ar_ca_trans_get_event (result->transitions.data[i]);
	if (! ccl_hash_find (events, e))
	  ccl_hash_insert (events, e);
	else
	  ar_ca_event_del_reference (e);
      }
    pi = ccl_hash_get_elements (events);
    while (ccl_iterator_has_more_elements (pi))
      {
	ar_ca_event *e = ccl_iterator_next_element (pi);
	ar_ca_add_event (result, e);
	ar_ca_event_del_reference (e);
      }
    ccl_hash_delete (events);
    ccl_iterator_delete (pi);
  }

  {
    const ccl_pair *p;
    const ccl_list *vars = ar_ca_get_flow_variables (ca);
    void (*add)(ar_ca *, ar_ca_expr *) = ar_ca_add_flow_variable;
    int r = 2;

    while (r--)
      {
	for (p = FIRST (vars); p; p = CDR (p))
	  {
	    ar_ca_expr *v = CAR (p);
	    if (ccl_hash_find (variables, v) || ccl_hash_find (V, v))
	      add (result, v);
	  }
	vars = ar_ca_get_state_variables (ca);
	add = ar_ca_add_state_variable;
      }
  }

  if (closure != NULL)
    {
      ccl_pointer_iterator *iv = ccl_hash_get_elements (variables);
      *closure = ccl_list_create ();
      while (ccl_iterator_has_more_elements (iv))
	{
	  ar_ca_expr *v = ccl_iterator_next_element (iv);
	  ccl_list_add (*closure, v);
	}
      ccl_iterator_delete (iv);
    }

  ccl_hash_delete (variables);

  if (ar_ca_get_number_of_variables (result) == 0)
    s_add_idle_var (result);
  s_add_epsilon_trans (result, epsilon);
  ar_ca_event_del_reference (epsilon);

  result->reduced = NULL;
  ccl_post (ar_ca_check (result));

  return result;
}

static void
s_dup_expr_list (ar_ca_exprman *dstman, ccl_list *dst, const ccl_list *src)
{
  ccl_pair *p;

  ccl_pre (ccl_list_is_empty (dst));

  for (p = FIRST (src); p; p = CDR (p))
    ccl_list_add (dst, ar_ca_expr_add_reference (CAR (p)));
}
	
static ar_ca_trans *
s_dup_transition (ar_ca_exprman *man, ar_ca_trans *t)
{
  int i;
  ar_ca_expr *g = ar_ca_trans_get_guard (t);
  ar_ca_expr *ng = ar_ca_expr_add_reference (g);
  ar_ca_event *e = ar_ca_trans_get_event (t);
  int nb_assign = ar_ca_trans_get_number_of_assignments (t);
  ar_ca_expr **assign = ar_ca_trans_get_assignments (t);
  ar_ca_trans *result = ar_ca_trans_create (ng, e);
  ar_ca_expr_del_reference (g);
  ar_ca_expr_del_reference (ng);
  ar_ca_event_del_reference (e);

  for (i = 0; i < nb_assign; i++, assign += 2)
    {
      ar_ca_expr *v = ar_ca_expr_add_reference (assign[0]);
      ar_ca_expr *val = ar_ca_expr_add_reference (assign[1]);
      ar_ca_trans_add_assignment (result, v, val);
      ar_ca_expr_del_reference (v);
      ar_ca_expr_del_reference (val);
    }

  return result;
}
		
ar_ca *
ar_ca_duplicate (const ar_ca *ca)
{
  int i;  
  ar_ca *result = ar_ca_allocate_ca (ca->node, ca->man);

  
  s_dup_expr_list (result->man, result->flow_variables, ca->flow_variables);
  s_dup_expr_list (result->man, result->state_variables, ca->state_variables);
  s_dup_expr_list (result->man, result->assertions, ca->assertions);
  s_dup_expr_list (result->man, result->initial_assignments,  
		   ca->initial_assignments);
  s_dup_expr_list (result->man, result->initial_constraints, 
		   ca->initial_constraints);

  ccl_array_ensure_size (result->events, ca->events.size);
  for (i = 0; i < ca->events.size; i++)
    result->events.data[i] = ar_ca_event_add_reference (ca->events.data[i]);

  ccl_array_ensure_size (result->transitions, ca->transitions.size);
  for (i = 0; i < ca->transitions.size; i++)
    result->transitions.data[i] = 
      s_dup_transition (result->man, ca->transitions.data[i]);
  result->reduced = NULL;

  ccl_post (ar_ca_check (result));

  return result;
}
 
#define m_compact_array(a)			\
  do {						\
    int k = 0;					\
    int j;					\
    for (j = 0; j < (a)->size; j++)		\
      {						\
	if ((a)->data[j] != NULL)		\
	  {					\
	    (a)->data[k] = (a)->data[j];	\
	    k++;				\
	  }					\
      }						\
    ccl_array_trim (*(a), k);			\
  } while (0)

#define m_remove_useless_objects(ht,a,del)				\
do {								\
  int i;							\
  int remain = (a)->size;					\
  for (i = 0; i < (a)->size; i++)				\
    {								\
      if (! ccl_hash_find ((ht), (a)->data[i]))	\
	{							\
	  (del) ((a)->data[i]);					\
	  (a)->data[i] = NULL;					\
	  remain--;						\
	}							\
    }								\
								\
  if (remain > 0 && remain != (a)->size)			\
    {								\
      m_compact_array (a);					\
      ccl_assert ((a)->size == remain);				\
    }								\
  else if (remain == 0)						\
    {								\
      ccl_array_trim (*(a), 0);					\
    }								\
 } while(0)

static int
s_filter_variables (void *p, void *data)
{
  return ! ccl_hash_find (data, p);
}

static void
s_remove_useless_expressions_from_list (ccl_hash *ht, ccl_list *l)
{
  ccl_list_remove_filtered (l, s_filter_variables, ht,
			    (ccl_delete_proc *) ar_ca_expr_del_reference);
}


void
ar_ca_trim (ar_ca *ca, ccl_hash *elements)
{
  int nb_init;
  ar_ca_event *epsilon = ar_ca_get_epsilon_event (ca);

  CCL_DEBUG_START_TIMED_BLOCK (("trim CA"));
  s_remove_useless_expressions_from_list (elements, ca->flow_variables);
  s_remove_useless_expressions_from_list (elements, ca->state_variables);
  s_remove_useless_expressions_from_list (elements, ca->assertions);
  s_remove_useless_expressions_from_list (elements, ca->initial_constraints);
  m_remove_useless_objects (elements, &ca->events, ar_ca_event_del_reference);
  m_remove_useless_objects (elements, &ca->transitions, 
			    ar_ca_trans_del_reference);

  nb_init = ar_ca_get_number_of_initial_assignments (ca);

  while (nb_init--)
    {
      ar_ca_expr *var = ccl_list_take_first (ca->initial_assignments);
      ar_ca_expr *value = ccl_list_take_first (ca->initial_assignments);
      
      if (ccl_hash_find (elements, var))
	{
	  ccl_list_add (ca->initial_assignments, var);
	  ccl_list_add (ca->initial_assignments, value);
	}
      else
	{
	  ar_ca_expr_del_reference (var);
	  ar_ca_expr_del_reference (value);
	}
    }

  /* check if CA is not empty */
  if (ar_ca_get_number_of_variables (ca) == 0)
    s_add_idle_var (ca);

  s_add_epsilon_trans (ca, epsilon);
  ar_ca_event_del_reference (epsilon);

  if (ca->fds != NULL)
    {
      ccl_list *V = fdset_get_ordered_variables (ca->fds, 0);
      while (! ccl_list_is_empty (V))
	{
	  ar_ca_expr *v = ccl_list_take_first (V);
	  funcdep *Fv = fdset_get_funcdep (ca->fds, v);

	  ccl_assert (! ccl_hash_find (elements, v) || 
		      ccl_hash_find (elements, Fv));
	  if (! ccl_hash_find (elements, v))
	    fdset_remove_dependency_for (ca->fds, v);
	}
      ccl_list_delete (V);
    }
  CCL_DEBUG_END_TIMED_BLOCK ();

  ccl_post (ar_ca_check (ca));
}

void
ar_ca_trim_to_reachables_from_variables (ar_ca *ca, ccl_hash *variables)
{
  depgraph *G = depgraph_compute (ca, 1);
  ccl_hash *reach = depgraph_get_reachables_from_variables (G, variables);

  ar_ca_trim (ca, reach);
  ccl_hash_delete (reach);
  depgraph_delete (G);
}

void
ar_ca_trim_to_reachables_from_formulas (ar_ca *ca, ccl_list *formulas)
{
  ccl_pair *p;
  ccl_hash *variables = NULL;

  for (p = FIRST (formulas); p; p = CDR (p))
    variables = ar_ca_expr_get_variables_in_table (CAR (p), variables);
  ar_ca_trim_to_reachables_from_variables (ca, variables);
  ccl_hash_delete (variables);
}

void
ar_ca_trim_to_reachables_from_formula (ar_ca *ca, ar_ca_expr *F)
{
  ccl_hash *variables = ar_ca_expr_get_variables_in_table (F, NULL);
  ar_ca_trim_to_reachables_from_variables (ca, variables);
  ccl_hash_delete (variables);
}

static int
s_is_const_or_var (ar_ca_expr *e)
{
  ccl_hash *vars = ar_ca_expr_get_variables_in_table (e, NULL);
  int res = ccl_hash_get_size (vars) <= 1;
  ccl_hash_delete (vars);
  return res;
}

#define NO_DEPTH -1

static int s_cmp_vars_with_fdset (const void *p1, const void *p2, void *data)
{
  return fdset_compare_variables (p1, p2, *(fdset **) data);
}

static ar_ca_expr *
s_rewrite_expr (const fdset *fds, ar_ca_expr *e, int simplify, int constnvar,
		int depth)
{
  ar_ca_expr *result = ar_ca_expr_add_reference (e);
  ccl_list *vars = ar_ca_expr_get_variables (result, NULL);
  ccl_list_sort_with_data (vars, s_cmp_vars_with_fdset, &fds);
  int d = 0;
  
  while (! ccl_list_is_empty (vars) && depth != d)
    {
      int size = ccl_list_get_size (vars);      
      ccl_hash *subst = ccl_hash_create (NULL, NULL, NULL, NULL);
      ar_ca_expr *tmp;
      
      while (size--)
	{

	  ar_ca_expr *v = ccl_list_take_first (vars);
	  funcdep *fd = fdset_get_funcdep (fds, v);
	  ar_ca_expr *fun;
      
	  if (fd == NULL)	
	    continue;
	  fun = funcdep_get_function (fd);
	  if (constnvar && s_is_const_or_var (fun))
	    continue;

	  ccl_assert (! ccl_hash_find (subst, v) ||
		      ccl_hash_get (subst) == fun);
	  if (! ccl_hash_find (subst, v))
	    {
	      ccl_hash_insert (subst, fun);
	      ccl_list_append (vars, funcdep_get_inputs (fd));
	    }
	  /*
	  tmp = ar_ca_expr_replace (result, v, fun);

	  ar_ca_expr_del_reference (result);

	  if (simplify && result != tmp)
	    {      
	      result = ar_ca_expr_simplify (tmp);
	      ar_ca_expr_del_reference (tmp);
	    }
	  else
	    {
	      result = tmp;
	      }*/
	}
      
      tmp = ar_ca_expr_replace_with_table (result, subst);
      while (tmp != result)
	{
	  ar_ca_expr_del_reference (result);
	  result = tmp;
	  tmp = ar_ca_expr_replace_with_table (result, subst);
	}
      ar_ca_expr_del_reference (tmp);
      ccl_hash_delete (subst);

      if (simplify)
	{      
	  tmp = ar_ca_expr_simplify (result);
	  ar_ca_expr_del_reference (result);
	  result = tmp;
	}
      d++;
    }

  if (simplify && result != e)
    {      
      ar_ca_expr *tmp = ar_ca_expr_simplify (result);
      ar_ca_expr_del_reference (result);
      result = tmp;
    }
  ccl_list_delete (vars);

  return result;
}

ar_ca_expr *
ar_ca_rewrite_expr_with_fd (const fdset *fds, ar_ca_expr *e, int simplify)
{
  ccl_pre (fds != NULL);
  ccl_pre (e != NULL);

  return s_rewrite_expr (fds, e, simplify, 0, NO_DEPTH);
}

ar_ca_expr *
ar_ca_rewrite_expr_with_const_and_var_fd (const fdset *fds, ar_ca_expr *e,
					  int simplify)
{
  ccl_pre (fds != NULL);
  ccl_pre (e != NULL);

  return s_rewrite_expr (fds, e, simplify, 1, NO_DEPTH);
}

ar_ca_expr *
ar_ca_rewrite_expr_with_bounded_fd (const fdset *fds, ar_ca_expr *e,
				    int simplify, int maxsize)
{
  ccl_pre (fds != NULL);
  ccl_pre (e != NULL);

  return s_rewrite_expr (fds, e, simplify, 0, maxsize);
}

static int
s_is_useless_assertion (void *a, void *fds)
{  
  return fdset_implies (*(const fdset **)fds, a);
}

void
ar_ca_reduce_assertions (ar_ca *ca)
{
  const fdset *fds;
  
  CCL_DEBUG_START_TIMED_BLOCK (("reduce assertions wrt FDs"));
  fds = ar_ca_get_fdset (ca);
  ccl_list_remove_filtered (ca->assertions, s_is_useless_assertion, &fds,
			    (ccl_delete_proc *) ar_ca_expr_del_reference);
  if (ccl_debug_is_on)
    ccl_debug ("it remains %d assertions\n", 
	       ar_ca_get_number_of_assertions (ca));
  CCL_DEBUG_END_TIMED_BLOCK ();
}
