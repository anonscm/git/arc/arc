/*
 * cuts-generator.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-buffer.h>
#include <ccl/ccl-graph.h>
#include "ar-rldd.h"
#include "ar-stepper.h"
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-rel-semantics.h"
#include "sequences-generator.h"
#include "observer.h"
#include "sequences-generator-p.h"

struct assignment
{
  struct assignment *next;
  int dd_index;
  int value;  
};

static void
s_add_event_variables (ar_ca *ca, ccl_set *visible_tags,
		       ccl_set *disabled_tags, ccl_hash **p_e2vl,
		       ccl_hash **p_v2str);

static void
s_compute_cuts (ar_ca *ca, ar_ca_expr *target, ccl_hash *e2vl, ccl_hash *v2str, 
		int mincuts, int boolean_formula, ccl_log_type log,
		const char*prefix);

static ar_rldd *
s_dd_to_min_rldd (ar_ddm ddm, ar_dd dd, ar_rlddm *rlddm, ccl_hash *cache);

static uintptr_t 
s_log_dd_aralia_formula (ccl_log_type log, ar_ddm ddm, ar_dd dd, 
			 ccl_hash *ddindex2ev, ccl_hash *nodeids,
			 const char *prefix);

static void
s_enumerate_dd_assignments (ccl_log_type log, ar_ddm ddm, ar_dd dd, 
			    ccl_hash *ddindex2ev, struct assignment *s);

			/* --------------- */

void
ar_cuts_generator (ar_ca *ca, ar_ca_expr *target, ccl_set *visible_tags,
		   ccl_set *disabled_tags, int mincuts, int boolean_formula,
		   ccl_log_type log, const char *prefix)
{  
  ccl_hash *event2varlist;
  ccl_hash *var2str;

  s_add_event_variables (ca, visible_tags, disabled_tags, &event2varlist, 
			 &var2str);
  s_compute_cuts (ca, target, event2varlist, var2str, mincuts, boolean_formula, 
		  log, prefix);

  ccl_hash_delete (event2varlist);
  ccl_hash_delete (var2str);
}


static void 
s_add_event_variables (ar_ca *ca, ccl_set *visibles, ccl_set *disabled,
		       ccl_hash **p_e2vl, ccl_hash **p_v2str)
{
  ccl_set *vars;
  ccl_pointer_iterator *pv;
  
  *p_v2str = ccl_hash_create (NULL, NULL, NULL, ccl_string_delete);  
  ar_ca_add_event_observer (ca, visibles, disabled, &vars, p_e2vl, NULL);
  pv = ccl_set_get_elements (vars);
  
  while (ccl_iterator_has_more_elements (pv))
    {
      ar_ca_expr *var = ccl_iterator_next_element (pv);
      /*
	const ar_identifier *varname = ar_ca_expr_variable_get_const_name (var);
	char *sbvarname = ar_identifier_to_string (varname);
      */
      char *sbvarname = observer_get_event_name_from_var (var);
      ccl_assert (! ccl_hash_find (*p_v2str, var));
      ccl_hash_find (*p_v2str, var);
      ccl_hash_insert (*p_v2str, sbvarname);
    }
  ccl_iterator_delete (pv);
  ccl_set_delete (vars);
}

/*!
 * Build the DD varlist that encodes event variables. The list is used by
 * the final projection that gives the set if cuts.
 * 
 * Parameter:
 *   rs : the DD encoded semantics of the studied constraint automaton
 *   e2vl : the table that associates to each synchronization vector a list
 *          Boolean variables (one per elementary event).
 * Result:
 *   the DD encoded list of variables.
 */
static ar_dd 
s_built_varlist (ar_relsem *rs, ccl_hash *e2vl)
{
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_dd varlist = ar_dd_create_projection_list (ddm);
  ccl_pointer_iterator *vars = ccl_hash_get_elements (e2vl);

  while (ccl_iterator_has_more_elements (vars))
    {
      ccl_pair *p;
      ccl_list *vlist = ccl_iterator_next_element (vars);
      
      for (p = FIRST (vlist); p; p = CDR (p))
	{
	  ar_ca_expr *var = CAR (p);
	  ar_identifier *id = ar_ca_expr_variable_get_name (var);
	  int ddindex = ar_relsem_get_var_dd_index (rs, id);

	  ar_dd_projection_list_add (ddm, &varlist, ddindex, ddindex);
	  
	  ccl_assert (ddindex >= 0);
	  ar_identifier_del_reference (id);
	}
    }
  ccl_iterator_delete (vars);

  return varlist;
}

/*!
 * Build the table that translates DD index into human-readable strings that 
 * correspond to an event variable.
 * 
 * Parameters:
 *   rs    : the DD encoded semantics of the studied CA 
 *   v2str : the variable to strings conversion table
 * Result:
     the translation table.
 */
static ccl_hash *
s_build_dd_index_to_event (ar_relsem *rs, ccl_hash *v2str)
{
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_pointer_iterator *pi = ccl_hash_get_keys (v2str);

  while (ccl_iterator_has_more_elements (pi))
    {
      ar_ca_expr *var = ccl_iterator_next_element (pi);
      ar_identifier *id = ar_ca_expr_variable_get_name (var);
      intptr_t ddindex = ar_relsem_get_var_dd_index (rs, id);
      char *str = NULL;

      ar_identifier_del_reference (id);
      ccl_assert (!ccl_hash_find (result, (void *) ddindex));

      ccl_hash_find (v2str, var);
      str = ccl_hash_get (v2str);
      ccl_hash_find (result, (void *) ddindex);
      ccl_hash_insert (result, str);
    }
  ccl_iterator_delete (pi);

  return result;
}

/*!
 * Callback for used by ar_rldd_display_node_sequences to display variables
 * of minimal cuts. The WDD encoding minimal cuts is labelled by DD indices.
 * The 'data' parameter is used to convert DD indices into human-readable 
 * strings.
 * 
 * Parameters:
 *   log   : the output stream
 *   label : the DD index 
 *   data  : pointer of the conversion table DD index -> hr string
 */
static void 
s_rldd_log_label (ccl_log_type log, const void *label, void *data)
{
  intptr_t l = (intptr_t) label;

  ccl_assert (ccl_hash_find (data, (void *) l) && 
	      ccl_hash_get (data) != NULL);
  ccl_hash_find (data, (void *) l);
  ccl_log (log, "%s", ccl_hash_get (data));
}

/*!
 * Compute reachable configurations for the constraint automaton 'ca'.
 * Result:
 *   the DD encoding reachable configuration
 */
static ar_dd
s_compute_reach (ar_ca *ca, ar_relsem *rs)
{
  ar_dd R = ar_relsem_get_reachables (rs);
  
  return R;
}

/*!
 * Compute the intersection of reachable configurations of the decorated 
 * constraint automaton with bad configurations described by 'target'.
 * 
 * Parameters:
 *   ca     : the studied constraint automaton
 *   rs     : the DD encoded semantics of 'ca'
 *   target : the Booolean formula describing expected states.
 * Result:
 *   the DD that encodes the intersection
 */
static ar_dd 
s_compute_bad (ar_ca *ca, ar_relsem *rs, ar_ca_expr *target)
{
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_dd tmp = ar_relsem_compute_simple_set (rs, target);
  ar_dd reachables = s_compute_reach (ca, rs);
  ar_dd bad = ar_dd_compute_and (ddm, reachables, tmp);

  if (ccl_debug_is_on)
    {
      ccl_debug ("#bad = %f/%f\n", 
		 ar_relsem_get_cardinality (rs, bad, 0),
		 ar_relsem_get_cardinality (rs, reachables, 0));
    }
  AR_DD_FREE (tmp);
  AR_DD_FREE (reachables);

  return bad;
}

/*!
 * Main routine that compute cuts. The algorithm first compute the intersection
 * of decorated reachable configurations with target states (cf. s_compute_bad).
 * Then the obtained DD is projected on event variables. According to the para-
 * meter 'mincuts', minimal cuts are displayed or not. 
 *
 * Parameters:
 *  ca      : the studied constraint automaton
 *  target  : the Boolean formula describing bad states
 *  e2vl    : table that maps to each synchronization vector a list of Boolean
 *            variables. There exists one Boolean per visible elementary event
 *	      appearing in the vector.
 *  v2str   : table that maps to each event variable the string that must 
 *            displayed in the result.
 *  mincuts : if true, indicates that minimal cuts have to be computed.
 *  log     : the output stream
 */
static void
s_compute_cuts (ar_ca *ca, ar_ca_expr *target, ccl_hash *e2vl, ccl_hash *v2str, 
		int mincuts, int boolform, ccl_log_type log, const char *prefix)
{  
  ar_relsem *rs = ar_compute_relational_semantics (ca, NULL, NULL);
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  ar_dd tmp = s_compute_bad (ca, rs, target);
  ar_dd varlist = s_built_varlist (rs, e2vl);
  ar_dd badseqs = ar_dd_project_on_variables (ddm, tmp, varlist);
  ccl_hash *ddindex2ev = s_build_dd_index_to_event (rs, v2str);

  AR_DD_FREE (tmp);
  AR_DD_FREE (varlist);

  if (mincuts)
    {
      ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
					 
      ar_rlddm *rlddm = 
	ar_rldd_create_manager (10001, 3, 100001, 100, NULL, NULL, NULL);
      ar_rldd *mincuts = s_dd_to_min_rldd (ddm, badseqs, rlddm, cache);
      
      if (!boolform)
	ar_rldd_display_node_sequences (rlddm, mincuts, log, s_rldd_log_label, 
					ddindex2ev);
      else
	ar_rldd_to_aralia (rlddm, mincuts, log, s_rldd_log_label, ddindex2ev,
			   prefix);
      ar_rldd_delete (rlddm, mincuts);
      ar_rldd_destroy_manager (rlddm);
      ccl_hash_delete (cache);
    }
  else if (boolform)
    {
      ccl_hash *node_ids = ccl_hash_create (NULL, NULL, NULL, NULL);
      const char *sign = AR_DD_POLARITY (badseqs) ? "-" : "";
      uintptr_t id = 
	s_log_dd_aralia_formula (log, ddm, AR_DD_ADDR (badseqs), ddindex2ev, 
				 node_ids, prefix); 
      ccl_log (log, "%sroot := %s%sN%d; \n", prefix, sign, prefix, id);
      ar_dd_initialize_decision_diagram_flag (badseqs);
      ccl_hash_delete (node_ids);
    }
  else
    {
      s_enumerate_dd_assignments (log, ddm, badseqs, ddindex2ev, NULL);
    }
  ccl_hash_delete (ddindex2ev);
  AR_DD_FREE (badseqs);
  ar_relsem_del_reference (rs);    
}

/*!
 * Compile the DD encoding cuts into minimal cuts. The algorithm used is 
 * inspired by the one described in:
 *   A. Rauzy. Mathematical Foundation of Minimal Cutsets. Vol. 50 (4). 
 *   pages 389--396. IEEE Transactions on Reliability. 2001.
 * The implementation is based on the data structure used to compute minimal 
 * subwords of a finite language described in Point's thesis. This data 
 * structure is implemented in the WDD module. It permits to not use a ZDD 
 * package.
 *
 * Parameters:
 *   ddm : The DD manager
 *   dd : the DD encoding cuts
 *   rlddm : The WDD manager used for the result.
 * Return:
 *   a WDD encoding minimal cuts.
 */
static ar_rldd *
s_dd_to_min_rldd (ar_ddm ddm, ar_dd dd, ar_rlddm *rlddm, ccl_hash *cache)
{
  ar_rldd *R;

  if (ar_dd_is_zero (ddm, dd))
    R = ar_rldd_empty (rlddm);
  else if (ar_dd_is_one (ddm, dd))
    R = ar_rldd_epsilon (rlddm);
  else  if (ccl_hash_find (cache, dd))
    R = ar_rldd_dup (rlddm, ccl_hash_get (cache));
  else
    {
      intptr_t index = AR_DD_INDEX (dd);
      ar_rldd *tmp;
      ar_rldd *S[2];

      ccl_assert (AR_DD_ARITY (dd) == 2);
      ccl_assert (AR_DD_CODE (dd) == EXHAUSTIVE);

      S[0] = s_dd_to_min_rldd (ddm, AR_DD_EPNTHSON (dd, 0), rlddm, cache);
      S[1] = s_dd_to_min_rldd (ddm, AR_DD_EPNTHSON (dd, 1), rlddm, cache);
      tmp = ar_rldd_extract (rlddm, S[1], S[0]);
      ar_rldd_delete (rlddm, S[1]);
      S[1] = ar_rldd_concat_letter (rlddm, (void *) index, tmp);
      ar_rldd_delete (rlddm, tmp);
      R = ar_rldd_union (rlddm, S[1], S[0]);
      ar_rldd_delete (rlddm, S[0]);
      ar_rldd_delete (rlddm, S[1]);
      if (! ccl_hash_find (cache, dd))
	ccl_hash_insert (cache, R);
    }

  return R;
}

/*!
 * Display 'dd' as a Boolean formula using the ARALIA syntax.
 * Each node of the DD is represented by an ITE equation N := (v ? S0 : S1)
 * where:
 *   N is the identifier of the equation for the DD 'dd'.
 *   v is the event variable labelling the DD node.
 *   S0 and S1 are the identifier of equations for cofactors.
 * Identifiers of equations use the pattern 'N'dd where 'dd' is used as is.
 *
 * Parameters:
 * log : the output stream
 * ddm : the DD manager
 * dd : the DD converted into an ARALIA formula
 * ddindex2ev: the mapping from DD index to strings displayed for each event.
 */
static uintptr_t 
s_log_dd_aralia_formula (ccl_log_type log, ar_ddm ddm, ar_dd dd, 
			 ccl_hash *ddindex2ev, ccl_hash *node_ids,
			 const char *prefix)
{
  uintptr_t id;

  ccl_pre (AR_DD_POLARITY (dd) == 0);

  if (ccl_hash_find (node_ids, dd))
    return (uintptr_t) ccl_hash_get (node_ids);

  id = ccl_hash_get_size (node_ids);
  ccl_hash_insert (node_ids, (void *) id);

  if (ar_dd_is_one (ddm, dd))
    ccl_log (log, "%sN%d := 1; \n", prefix, id);
  else 
    {
      uintptr_t id0, id1;
      ar_dd l0 = AR_DD_EPNTHSON (dd, 0);
      const char *sl0 = AR_DD_POLARITY (l0) ? "-" : "";
      ar_dd l1 = AR_DD_EPNTHSON (dd, 1);
      const char *sl1 = AR_DD_POLARITY (l1) ? "-" : "";

      ccl_assert (AR_DD_CODE (dd) == EXHAUSTIVE);
      ccl_assert (AR_DD_ARITY (dd) == 2);
      
      l0 = AR_DD_ADDR (l0);
      id0 = s_log_dd_aralia_formula (log, ddm, l0, ddindex2ev, node_ids,
				     prefix);
      l1 = AR_DD_ADDR (l1);
      id1 = s_log_dd_aralia_formula (log, ddm, l1, ddindex2ev, node_ids,
				     prefix);

      ccl_assert (ccl_hash_find (ddindex2ev, 
				 (void *) (intptr_t) AR_DD_INDEX (dd)));

      ccl_hash_find (ddindex2ev, (void *) (intptr_t) AR_DD_INDEX (dd));
      ccl_log (log, "%sN%d := (`%s' ? %s%sN%d : %s%sN%d); \n", prefix,
	       id, ccl_hash_get (ddindex2ev), 
	       sl1, prefix, id1, sl0,
	       prefix, id0);
    }
  
  return id;
}

static uintptr_t 
s_rldd_to_aralia (ar_rlddm *man, ar_rldd *rldd, ccl_log_type log, 
		  ccl_hash *node_ids, ar_rldd_log_label *ll, void *data,
		  const char *prefix)
{
  uintptr_t id;

  if (ccl_hash_find (node_ids, rldd))
    return (uintptr_t) ccl_hash_get (node_ids);

  id = ccl_hash_get_size (node_ids);
  ccl_hash_insert (node_ids, (void *) id);

  if (ar_rldd_is_epsilon (man, rldd))
    ccl_log (log, "%sN%d := 1; \n", prefix, id);
  else if (ar_rldd_is_nil (man, rldd))
    ccl_log (log, "%sN%d := 0; \n", prefix, id);
  else
    {
      uintptr_t left = 
	s_rldd_to_aralia (man, ar_rldd_get_left (rldd), log, node_ids, ll, data,
			  prefix);
      uintptr_t right = 
	s_rldd_to_aralia (man, ar_rldd_get_right (rldd), log, node_ids, ll,
			  data, prefix);

      ccl_log (log, "%sN%d := ((`", prefix, id);
      ll (log, ar_rldd_get_label (rldd), data);
      ccl_log (log, "' & %sN%d) | %sN%d); \n", prefix, left, prefix, right);
    }

  return id;
}

			/* --------------- */

void
ar_rldd_to_aralia (ar_rlddm *man, ar_rldd *rldd, ccl_log_type log, 
		   ar_rldd_log_label *ll, void *ll_data, const char *prefix)
{
  ccl_hash *node_ids = ccl_hash_create (NULL, NULL, NULL, NULL);
  uintptr_t id = s_rldd_to_aralia (man, rldd, log, node_ids, ll, ll_data,
				   prefix);
  ccl_log (log, "%sroot := %sN%d; \n", prefix, prefix, id);
  ccl_hash_delete (node_ids);
}

			/* --------------- */

static void
s_enumerate_dd_assignments (ccl_log_type log, ar_ddm ddm, ar_dd dd, 
			    ccl_hash *ddindex2ev, struct assignment *s)
{
  if (ar_dd_is_zero (ddm, dd))
    return;

  if (ar_dd_is_one (ddm, dd))
    {
      ccl_log (log, "(");
      if (! s->value)
	ccl_log (log, "-");
      s_rldd_log_label (log, (void *) (intptr_t) s->dd_index, ddindex2ev);
      for (s = s->next; s != NULL; s = s->next)
	{
	  ccl_log (log, ", ");
	  if (! s->value)
	    ccl_log (log, "-");
	  s_rldd_log_label (log, (void *) (intptr_t) s->dd_index, ddindex2ev);
	}
      ccl_log (log, ") \n");
    }
  else
    {
      struct assignment ls;

      ccl_assert (AR_DD_CODE (dd) == EXHAUSTIVE);
      ccl_assert (AR_DD_ARITY (dd) == 2);
      
      ls.next = s;
      ls.dd_index = AR_DD_INDEX (dd);
      ls.value = 0;

      s_enumerate_dd_assignments (log, ddm, AR_DD_EPNTHSON (dd, 0),
				  ddindex2ev, &ls);
      ls.value = 1;
      s_enumerate_dd_assignments (log, ddm, AR_DD_EPNTHSON (dd, 1),
				  ddindex2ev, &ls);
    }
}
