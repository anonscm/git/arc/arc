/*
 * ar-attributes.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_ATTRIBUTES_H
# define AR_ATTRIBUTES_H

# include "ar-identifier.h"

# define AR_SLOT_FLAG_PARAMETER (0x1<<0)
# define AR_SLOT_FLAG_STATE_VAR (0x1<<1)
# define AR_SLOT_FLAG_FLOW_VAR  (0x1<<2)
# define AR_SLOT_FLAG_NODE      (0x1<<3)
# define AR_SLOT_FLAG_EVENT     (0x1<<4)

# define AR_SLOT_FLAG_IN        (0x1<<5)
# define AR_SLOT_FLAG_OUT       (0x1<<6)
# define AR_SLOT_FLAG_PRIVATE   (0x1<<7)
# define AR_SLOT_FLAG_PARENT    (0x1<<8)
# define AR_SLOT_FLAG_PUBLIC    (0x1<<9)

# define AR_SLOT_FLAG_DFLT_STATE_VIS AR_SLOT_FLAG_PRIVATE
# define AR_SLOT_FLAG_DFLT_FLOW_VIS AR_SLOT_FLAG_PARENT
# define AR_SLOT_FLAG_DFLT_EVENT_VIS AR_SLOT_FLAG_PARENT
# define AR_SLOT_FLAG_DFLT_PARAMETER_VIS AR_SLOT_FLAG_PARENT

extern ar_identifier *AR_ATTR_PUBLIC;
extern ar_identifier *AR_ATTR_PRIVATE;
extern ar_identifier *AR_ATTR_PARENT;
extern ar_identifier *AR_ATTR_IN;
extern ar_identifier *AR_ATTR_OUT;

extern void
ar_attributes_init(void);

extern void
ar_attributes_terminate(void);

#endif /* ! AR_ATTRIBUTES_H */
