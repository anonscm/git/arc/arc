/*
 * ar-backtracking-stack.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "ar-backtracking-stack.h"

typedef struct stack_cell_st stack_cell;
struct stack_cell_st {
  int *addr;
  int value;
  stack_cell *next;
};

struct ar_backtracking_stack_st {
  int refcount;
  stack_cell *top;
  stack_cell *free_cells;
};

			/* --------------- */

static void
s_backtracking_stack_destroy(ar_backtracking_stack *bstack);

			/* --------------- */

ar_backtracking_stack *
ar_backtracking_stack_create(void)
{
  ar_backtracking_stack *result = ccl_new(ar_backtracking_stack);

  result->refcount = 1;
  result->top = NULL;
  result->free_cells = NULL;

  return result;
}

			/* --------------- */

ar_backtracking_stack *
ar_backtracking_stack_add_reference(ar_backtracking_stack *bstack)
{
  ccl_pre( bstack != NULL );

  bstack->refcount++;

  return bstack;
}

			/* --------------- */

void
ar_backtracking_stack_del_reference(ar_backtracking_stack *bstack)
{
  ccl_pre( bstack != NULL ); ccl_pre( bstack->refcount > 0 );
  
  if( --bstack->refcount == 0 ) 
    s_backtracking_stack_destroy(bstack);
}
			/* --------------- */

static stack_cell *
s_new_cell(ar_backtracking_stack *bstack)
{
  stack_cell *result; 

  if( (result = bstack->free_cells) == NULL )
    result = ccl_new(stack_cell);
  else
    bstack->free_cells = result->next;

  return result;
}

			/* --------------- */

# define s_free_cell(_bs, _cell) \
  do { (_cell)->next = (_bs)->free_cells; (_bs)->free_cells = _cell; } while(0)


			/* --------------- */

void
ar_backtracking_stack_save(ar_backtracking_stack *bstack, int *pdata)
{
  stack_cell *cell = s_new_cell (bstack);

  cell->addr  = pdata;
  cell->value = *pdata;
  cell->next  = bstack->top;
  bstack->top = cell;
}

			/* --------------- */

void
ar_backtracking_stack_restore(ar_backtracking_stack *bstack, 
			      void *choice_point)
{
  stack_cell *next;
  stack_cell *top;

  for(top = bstack->top; top != NULL && top != choice_point; top = next)
    {
      next = top->next;
      *(top->addr) = top->value;
      s_free_cell (bstack, top);
    }

  ccl_assert( top != NULL );
  next = top->next;
  s_free_cell (bstack, top);
  bstack->top = next;
}

			/* --------------- */

void
ar_backtracking_stack_restore_last(ar_backtracking_stack *bstack)
{
  stack_cell *next, *top;

  for(top = bstack->top; top != NULL && top->addr != &(top->value); top = next)
    {
      next = top->next;      
      *(top->addr) = top->value;
      s_free_cell (bstack, top);
    }

  ccl_assert( top != NULL );
  
  next = top->next;
  s_free_cell (bstack, top);
  bstack->top = next;
}

			/* --------------- */

void *
ar_backtracking_stack_choice_point(ar_backtracking_stack *bstack)
{
  stack_cell *cell = s_new_cell (bstack);

  cell->addr = &(cell->value);
  cell->value = 0;
  cell->next = bstack->top;
  bstack->top = cell;

  return bstack->top;
}

			/* --------------- */

int
ar_backtracking_stack_is_empty(ar_backtracking_stack *bstack)
{
  return (bstack->top == NULL);
}

			/* --------------- */


static void
s_backtracking_stack_destroy(ar_backtracking_stack *bstack)
{
  stack_cell *next;
  stack_cell *top;

  ccl_pre( bstack->top == NULL );

  for(top = bstack->free_cells; top != NULL; top = next)
    {
      next = top->next;
      ccl_delete(top);
    }
  ccl_delete(bstack);
}

			/* --------------- */
