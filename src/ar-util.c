/*
 * ar-util.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-string.h>
#include "ar-identifier.h"
#include "ar-util.h"

void
ar_util_init(void)
{
}

void
ar_util_terminate(void)
{
}

char *
ar_tilde_substitution(const char *filename)
{
  char *result;
  char *home = getenv("HOME");

  if( home == NULL || *filename != '~' )
    result = ccl_string_dup(filename);
  else
    result = ccl_string_format_new("%s%s",home,filename+1);

  return result;
}

			/* --------------- */

uint32_t 
ar_bit_size (uint32_t v)
{
  uint32_t r; 
  uint32_t val;

  if ((val = (v & 0xFF000000))) { val >>= 24; r = 24; }
  else if ((val = (v & 0xFF0000))) { val >>= 16; r = 16; }
  else if ((val = (v & 0xFF00))) { val >>= 8; r = 8; }
  else if ((val = (v & 0xF0))) { val >>= 4; r = 4; }
  else { val = v; r = 0; }

  if (val & 0x8) r += 4;
  else if (val & 0x4) r += 3;
  else if (val & 0x2) r += 2;
  else r += 1;

  return r;
}

			/* --------------- */

ccl_set *
ar_parse_id_list (const char *taglist, ccl_set *result)
{
  char *t;
  char *tags = t = ccl_string_dup (taglist);
  
  if (result == NULL)
    result = ccl_set_create_for_objects (NULL, NULL, (ccl_delete_proc *) 
					 ar_identifier_del_reference);
  while (t)
    {
      ar_identifier *id;
      char *tmp = strchr(t, ',');

      if (tmp != NULL)
	*tmp = '\0';
      id = ar_identifier_create (t);
      if (! ccl_set_has (result, id))
	ccl_set_add (result, id);
      else 
	ar_identifier_del_reference (id);

      if (tmp != NULL)	
	t = tmp + 1;
      else
	t = NULL;
    }
  ccl_string_delete (tags);

  return result;
}
