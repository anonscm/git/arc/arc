/*
 * ar-state-graph-semantics.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_STATE_GRAPH_SEMANTICS_H
# define AR_STATE_GRAPH_SEMANTICS_H

# include <ccl/ccl-list.h>
# include <ccl/ccl-config-table.h>
# include "ar-identifier.h"
# include "ar-constraint-automaton.h"

typedef struct ar_state_graph_semantics_st ar_sgs;
typedef struct ar_state_graph_semantics_methods_st ar_sgs_methods;
typedef struct ar_state_graph_set_st ar_sgs_set;
typedef struct ar_state_graph_set_methods_st ar_sgs_set_methods;
typedef struct ar_state_graph_formula_st ar_sgs_formula;

# define AR_SGS_UNDEF_SET_KIND (-1)

typedef enum ar_sgs_display_format_enum {
  AR_SGS_DISPLAY_MEC4,
  AR_SGS_DISPLAY_GML,
  AR_SGS_DISPLAY_DOT,
  AR_SGS_DISPLAY_ASIS
} ar_sgs_display_format;

			/* --------------- */

extern ar_sgs *
ar_sgs_add_reference (ar_sgs *sgs);

extern void
ar_sgs_del_reference (ar_sgs *sgs);

extern ar_ca *
ar_sgs_get_constraint_automaton (ar_sgs *sgs);

extern const varorder *
ar_sgs_get_varorder (ar_sgs *sgs);

extern ar_node *
ar_sgs_get_node (ar_sgs *sgs);

extern ar_ca_exprman *
ar_sgs_get_expr_manager (ar_sgs *sgs);

extern ar_identifier *
ar_sgs_get_name (ar_sgs *sgs);

extern int
ar_sgs_has_set (ar_sgs *sgs, ar_identifier *name, int *is_state);

extern int
ar_sgs_has_state_set (ar_sgs *sgs, ar_identifier *name);

extern void
ar_sgs_remove_state_set (ar_sgs *sgs, ar_identifier *name);

extern int
ar_sgs_has_trans_set (ar_sgs *sgs, ar_identifier *name);

extern void
ar_sgs_remove_trans_set (ar_sgs *sgs, ar_identifier *name);

extern void
ar_sgs_add_state_set (ar_sgs *sgs, ar_identifier *name, ar_sgs_set *set);

extern void
ar_sgs_add_trans_set (ar_sgs *sgs, ar_identifier *name, ar_sgs_set *set);

extern ar_sgs_set *
ar_sgs_get_state_set (ar_sgs *sgs, ar_identifier *name);

extern ar_sgs_set *
ar_sgs_get_trans_set (ar_sgs *sgs, ar_identifier *name);

extern ar_identifier_iterator *
ar_sgs_get_state_sets (ar_sgs *sgs);

extern ar_identifier_iterator *
ar_sgs_get_trans_sets (ar_sgs *sgs);

			/* --------------- */

extern ar_sgs_set * 
ar_sgs_compute_proj (ar_sgs *sgs, ar_sgs_set *X, int on_state);

extern ar_sgs_set * 
ar_sgs_compute_pick (ar_sgs *sgs, ar_sgs_set *X);

extern ar_sgs_set * 
ar_sgs_compute_assert (ar_sgs *sgs, ar_sgs_set *T);

extern ar_sgs_set * 
ar_sgs_compute_src (ar_sgs *sgs, ar_sgs_set *T);

extern ar_sgs_set * 
ar_sgs_compute_tgt (ar_sgs *sgs, ar_sgs_set *T);

extern ar_sgs_set * 
ar_sgs_compute_rsrc (ar_sgs *sgs, ar_sgs_set *S);

extern ar_sgs_set * 
ar_sgs_compute_rtgt (ar_sgs *sgs, ar_sgs_set *S);

extern ar_sgs_set * 
ar_sgs_compute_loop (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2); 

extern ar_sgs_set * 
ar_sgs_compute_trace (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, 
		      ar_sgs_set *S2);

extern ar_sgs_set * 
ar_sgs_compute_unav (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S);

extern ar_sgs_set * 
ar_sgs_compute_reach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);

extern ar_sgs_set * 
ar_sgs_compute_post (ar_sgs *sgs, ar_sgs_set *S);

extern ar_sgs_set * 
ar_sgs_compute_pre (ar_sgs *sgs, ar_sgs_set *S);

extern ar_sgs_set * 
ar_sgs_compute_post_star (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *I);

extern ar_sgs_set * 
ar_sgs_compute_pre_star (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *I);

extern ar_sgs_set * 
ar_sgs_compute_coreach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);

extern ar_sgs_set * 
ar_sgs_compute_state_set (ar_sgs *sgs, ar_ca_expr *cond);

extern ar_sgs_set * 
ar_sgs_compute_label (ar_sgs *sgs, ar_identifier *label);

extern ar_sgs_set * 
ar_sgs_compute_attribute (ar_sgs *sgs, ar_identifier *attr);

extern ccl_list *
ar_sgs_compute_classes (ar_sgs *sgs);

extern void 
ar_sgs_compute_project (ar_sgs *sgs, ccl_log_type log, ar_identifier *subid,
			ar_node *subnode, ar_identifier *pname, ar_sgs_set *S, 
			ar_sgs_set *TE, ar_sgs_set *TA, int simplify);


extern ar_sgs_set * 
ar_sgs_compute_initial (ar_sgs *sgs);

/*
extern ar_sgs_set * 
ar_sgs_compute_any_state (ar_sgs *sgs);

extern ar_sgs_set * 
ar_sgs_compute_any_trans (ar_sgs *sgs);

extern ar_sgs_set * 
ar_sgs_compute_epsilon (ar_sgs *sgs);

extern ar_sgs_set * 
ar_sgs_compute_self (ar_sgs *sgs);

extern ar_sgs_set * 
ar_sgs_compute_self_epsilon (ar_sgs *sgs); 

extern ar_sgs_set * 
ar_sgs_compute_not_deterministic (ar_sgs *sgs);
*/

extern ar_sgs_set * 
ar_sgs_compute_lfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);

extern ar_sgs_set * 
ar_sgs_compute_gfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);

extern ar_sgs_set *
ar_sgs_compute_complement (ar_sgs *sgs, ar_sgs_set *S);

extern ar_sgs_set *
ar_sgs_compute_union (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

extern ar_sgs_set *
ar_sgs_compute_intersection (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

extern ar_sgs_set *
ar_sgs_compute_difference (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

extern ar_sgs_set *
ar_sgs_compute_imply (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

extern double
ar_sgs_get_set_cardinality (ar_sgs *sgs, ar_sgs_set *set);

extern int
ar_sgs_is_set_empty (ar_sgs *sgs, ar_sgs_set *set);

extern void
ar_sgs_dot_trace (ccl_log_type log, ar_sgs *sgs, 
		  ar_sgs_set *initial, ar_sgs_set *trans, ar_sgs_set *target,
		  ccl_config_table *conf);

extern void
ar_sgs_to_mec4 (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
		ccl_config_table *conf);

extern void
ar_sgs_to_dot (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
	       ccl_config_table *conf);

extern void
ar_sgs_to_gml (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T,
	       ccl_config_table *conf);

extern void
ar_sgs_to_quot (ccl_log_type log, ar_sgs *sgs, ccl_config_table *conf);

extern void
ar_sgs_to_modes_automaton (ccl_log_type log, ar_sgs *sgs, 
			   ccl_config_table *conf);

extern ar_sgs_set *
ar_sgs_compute_macro_transition (ar_sgs *sgs, ar_trans *t);

			/* --------------- */

extern void
ar_sgs_display_set (ccl_log_type log, ar_sgs *sgs, ar_sgs_set *set,
		    ar_sgs_display_format df);

extern int
ar_sgs_sets_are_equal (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

			/* --------------- */

extern ar_sgs_set *
ar_sgs_set_add_reference (ar_sgs_set *set);

extern void
ar_sgs_set_del_reference (ar_sgs_set *set);

extern int
ar_sgs_set_is_state_set (ar_sgs_set *set);

extern int
ar_sgs_set_is_empty (ar_sgs *sgs, ar_sgs_set *set);

extern ar_expr *
ar_sgs_set_states_to_formula (ar_sgs *sgs, ar_sgs_set *S, ccl_list *parent_vars,
			      ccl_list *vars, int exist);

extern ar_ca_expr *
ar_sgs_set_states_to_ca_expr (ar_sgs *sgs, ar_sgs_set *S, ccl_hash *vars);

extern ccl_hash *
ar_sgs_set_get_events (ar_sgs *sgs, ar_sgs_set *T);

extern ar_sgs_set *
ar_sgs_set_get_rel_for_event (ar_sgs *sgs, ar_sgs_set *T, ar_ca_event *e);

			/* --------------- */

extern ar_sgs_formula *
ar_sgs_formula_crt_cst (ar_sgs_set *set, int is_state);

extern ar_sgs_formula *
ar_sgs_formula_crt_var (ar_identifier *varname);

extern ar_sgs_formula *
ar_sgs_formula_crt_src (ar_sgs_formula *T);

extern ar_sgs_formula *
ar_sgs_formula_crt_assert (ar_sgs_formula *S);

extern ar_sgs_formula *
ar_sgs_formula_crt_tgt (ar_sgs_formula *T);

extern ar_sgs_formula *
ar_sgs_formula_crt_rsrc (ar_sgs_formula *S);

extern ar_sgs_formula *
ar_sgs_formula_crt_rtgt (ar_sgs_formula *S);

extern ar_sgs_formula *
ar_sgs_formula_crt_or (ar_sgs_formula *F1, ar_sgs_formula *F2);

extern ar_sgs_formula *
ar_sgs_formula_crt_and (ar_sgs_formula *F1, ar_sgs_formula *F2);

extern ar_sgs_formula *
ar_sgs_formula_crt_sub (ar_sgs_formula *F1, ar_sgs_formula *F2);

extern ar_sgs_formula *
ar_sgs_formula_crt_not (ar_sgs_formula *F);

			/* --------------- */

extern ar_sgs_formula *
ar_sgs_formula_add_reference (ar_sgs_formula *F);

extern void
ar_sgs_formula_del_reference (ar_sgs_formula *F);

extern int
ar_sgs_formula_is_state_formula (ar_sgs_formula *F);

extern int
ar_sgs_formula_is_constant (ar_sgs_formula *F);

			/* --------------- */

extern void
ar_sgs_formula_get_varnames (ar_sgs_formula *F, ccl_list *variables);

extern ar_sgs_set *
ar_sgs_formula_compute_constant_formula (ar_sgs_formula *F, ar_sgs *sgs);

/*!
 * Returns a set of transitions that satisfies given assignement. 
 */
extern ar_sgs_set *
ar_sgs_formula_compute_assignment (ar_sgs *sgs, ar_ca_expr **assignments, 
				   int nb_assignments);
				   

#endif /* ! AR_STATE_GRAPH_SEMANTICS_H */
