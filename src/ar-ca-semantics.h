/*
 * ar-ca-semantics.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_CA_SEMANTICS_H
# define AR_CA_SEMANTICS_H

# include <ccl/ccl-hash.h>
# include "ar-node.h"
# include "ar-constraint-automaton.h"

extern ar_ca *
ar_compute_constraint_automaton_semantics(ar_node *node)
  CCL_THROW(altarica_interpretation_exception);

extern ar_ca_domain *
ar_model_type_to_ca_domain(ar_type *type, ar_ca_exprman *man,
			   ccl_hash *domaincache)
  CCL_THROW(abtract_type_exception);

extern ar_ca_expr *
ar_model_expr_to_ca_expr(ar_expr *expr, ar_ca_exprman *man, 
			 ccl_hash *domaincache)
  CCL_THROW(abtract_type_exception);

#endif /* ! AR_CA_SEMANTICS_H */
