/*
 * ar-constant.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <stdlib.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-constant.h"

struct field {
  ar_identifier *fname;
  ar_constant *fval;
};

struct ar_constant_st {
  int refcount;
  ar_constant_kind kind;
  ar_type *type;
  int ival;
  union {
    struct field *fields;    
    ar_constant **array;
  } u;
};

			/* --------------- */

# define PREDEFINED_I_CST_MAX 10

static ar_constant *B_CST_TRUE;
static ar_constant *B_CST_FALSE;
static ar_constant *I_CST_PREDEF[2*PREDEFINED_I_CST_MAX+1];

			/* --------------- */

static ar_constant *
s_crt_constant(ar_constant_kind kind, int ival, ar_type *type);
static void
s_del_constant(ar_constant *cst);

			/* --------------- */

void
ar_constant_init(void)
{ 
  int i;

  B_CST_TRUE  = s_crt_constant(AR_CST_SCALAR,1,AR_BOOLEANS);
  B_CST_FALSE = s_crt_constant(AR_CST_SCALAR,0,AR_BOOLEANS);

  for(i = -PREDEFINED_I_CST_MAX; i <= PREDEFINED_I_CST_MAX; i++)
    I_CST_PREDEF[i+PREDEFINED_I_CST_MAX] = 
      s_crt_constant(AR_CST_SCALAR,i,AR_INTEGERS);
}

			/* --------------- */

void
ar_constant_terminate(void)
{
  int i;

  ar_constant_del_reference(B_CST_TRUE);
  ar_constant_del_reference(B_CST_FALSE);

  for(i = -PREDEFINED_I_CST_MAX; i <= PREDEFINED_I_CST_MAX; i++)
    ar_constant_del_reference(I_CST_PREDEF[i+PREDEFINED_I_CST_MAX]);
}

			/* --------------- */

ar_constant *
ar_constant_crt_boolean(int value)
{
  if( value ) return ar_constant_add_reference(B_CST_TRUE);
  else        return ar_constant_add_reference(B_CST_FALSE);
}

			/* --------------- */

ar_constant *
ar_constant_crt_integer(int value)
{
  if( -PREDEFINED_I_CST_MAX <= value && value <= PREDEFINED_I_CST_MAX )
    return ar_constant_add_reference(I_CST_PREDEF[value+PREDEFINED_I_CST_MAX]);

  return s_crt_constant(AR_CST_SCALAR,value,AR_INTEGERS);
}

			/* --------------- */

ar_constant *
ar_constant_crt_symbol(int value, ar_type *type)
{
  ccl_pre( type != NULL );

  return s_crt_constant(AR_CST_SCALAR,value,type);
}

			/* --------------- */

ar_constant *
ar_constant_crt_structure(ar_identifier **fields, 
			  ar_constant **values, 
			  int width,
			  ar_type *type)
{
  int i;
  ar_constant *result = s_crt_constant(AR_CST_STRUCTURE,width,type);

  result->u.fields = ccl_new_array(struct field,width);
  if( result->type == NULL )
    {
      result->type = ar_type_crt_structure();      

      for(i = 0; i < width; i++)
	{
	  ar_type *ctype = ar_constant_get_type(values[i]);
	  ar_type_struct_add_field(result->type,fields[i],ctype);
	  ar_type_del_reference(ctype);
	}
    }

  for(i = 0; i < width; i++)
    {
      int index = ar_type_struct_get_field_index(result->type,fields[i]);

      ccl_assert( 0 <= index && index < width );

      result->u.fields[index].fname = ar_identifier_add_reference(fields[i]);
      result->u.fields[index].fval = ar_constant_add_reference(values[i]);
    }

  return result;
}

			/* --------------- */

ar_constant *
ar_constant_crt_array(ar_constant **values, int width, ar_type *type)
{
  int i;
  ar_constant *result = s_crt_constant(AR_CST_ARRAY,width,type);

  if( result->type == NULL )
    {
      ar_type *cst_type = ar_constant_get_type(values[0]);
      result->type = ar_type_crt_array(cst_type,width);
      ar_type_del_reference(cst_type);
    }

  result->u.array = ccl_new_array(ar_constant *,width);
  for(i = 0; i < width; i++)
    result->u.array[i] = ar_constant_add_reference(values[i]);

  return result;  
}

			/* --------------- */

int
ar_constant_get_boolean_value(ar_constant *cst)
{
  ccl_pre( ar_constant_get_kind(cst) == AR_CST_SCALAR );

  return cst->ival;
}

			/* --------------- */

int
ar_constant_get_integer_value(ar_constant *cst)
{
  ccl_pre( ar_constant_get_kind(cst) == AR_CST_SCALAR );

  return cst->ival;
}

			/* --------------- */

int
ar_constant_get_symbol_value(ar_constant *cst)
{
  ccl_pre( ar_constant_get_kind(cst) == AR_CST_SCALAR );

  return cst->ival;
}

			/* --------------- */

int
ar_constant_get_width(ar_constant *cst)
{
  ccl_pre( ar_constant_get_kind(cst) != AR_CST_SCALAR );
  
  return cst->ival;
}

			/* --------------- */

ar_constant *
ar_constant_get_at(ar_constant *cst, int pos)
{
  ccl_pre( ar_constant_get_kind(cst) == AR_CST_ARRAY );
  ccl_pre( 0 <= pos && pos < ar_constant_get_width(cst) );

  return ar_constant_add_reference(cst->u.array[pos]);
}

			/* --------------- */

int
ar_constant_has_field(ar_constant *cst, ar_identifier *fname)
{
  int i;

  ccl_pre( ar_constant_get_kind(cst) == AR_CST_STRUCTURE );

  for(i = 0; i < cst->ival; i++)
    if( cst->u.fields[i].fname == fname )
      return 1;

  return 0;
}

			/* --------------- */

ar_constant *
ar_constant_get_field(ar_constant *cst, ar_identifier *fname)
{
  int i;
  ar_constant *result = NULL;
  ccl_pre( ar_constant_has_field(cst,fname) );

  for(i = 0; result == NULL && i < cst->ival; i++)
    if( cst->u.fields[i].fname == fname )
      result = ar_constant_add_reference(cst->u.fields[i].fval);

  ccl_post( result != NULL );

  return result;
}

			/* --------------- */

ar_constant_kind
ar_constant_get_kind(ar_constant *cst)
{
  ccl_pre( cst != NULL );

  return cst->kind;
}

			/* --------------- */

int
ar_constant_has_base_type(ar_constant *cst, ar_type *type)
{
  ccl_pre( cst != NULL );  ccl_pre( cst->type != NULL );

  return ar_type_have_same_base_type(cst->type,type);
}

			/* --------------- */

int
ar_constant_has_type(ar_constant *cst, ar_type *type)
{
  ccl_pre( cst != NULL );  

  return ar_type_equals(cst->type,type);
}


			/* --------------- */

ar_type *
ar_constant_get_type(ar_constant *cst)
{
  ccl_pre( cst != NULL ); 

  return ar_type_add_reference(cst->type);
}

			/* --------------- */

void
ar_constant_set_type(ar_constant *cst, ar_type *type)
{
  ccl_pre( cst != NULL ); ccl_pre( cst->type != NULL );

  ar_type_del_reference(cst->type);

  cst->type = ar_type_add_reference(type);
}

			/* --------------- */

ar_constant *
ar_constant_add_reference(ar_constant *cst)
{
  ccl_pre( cst != NULL );

  cst->refcount++;

  return cst;
}

			/* --------------- */

void
ar_constant_del_reference(ar_constant *cst)
{
  ccl_pre( cst != NULL ); ccl_pre( cst->refcount > 0 );

  if( --cst->refcount == 0 )
    s_del_constant(cst);    
}

			/* --------------- */

int
ar_constant_are_equal(ar_constant *c1, ar_constant *c2)
{
  ccl_pre( c1 != NULL); ccl_pre( c2 != NULL );

  if( c1 == c2 ) return 1;

  if( c1->kind != c2->kind ) return 0;

  if( c1->ival != c2->ival ) return 0;

  if( ! ar_type_have_same_base_type(c1->type,c2->type) ) return 0;

  if( c1->kind == AR_CST_STRUCTURE )
    {
      int i;

      for(i = 0; i < c1->ival; i++)
	{
	  if( ! ar_constant_are_equal(c1->u.fields[i].fval,
				      c2->u.fields[i].fval) )
	    return 0;

	}
    }
  else if( c1->kind == AR_CST_ARRAY )
    {
      int i;

      for(i = 0; i < c1->ival; i++)
	{
	  if( ! ar_constant_are_equal(c1->u.array[i],c2->u.array[i]) )
	    return 0;
	}
    }

  return 1;
}

			/* --------------- */

void
ar_constant_log (ccl_log_type log, ar_constant *cst)
{
  int i;

  ccl_pre (cst != NULL);

  if (cst->kind == AR_CST_SCALAR)
    {
      if (ar_constant_has_base_type (cst, AR_BOOLEANS))
	ccl_log (log, "%s", cst->ival ? "true" : "false");
      else if (ar_constant_has_base_type (cst, AR_INTEGERS))
	ccl_log (log, "%d", cst->ival);
      else 
	{
	  ar_identifier *id;

	  ccl_assert (ar_constant_has_base_type (cst, AR_SYMBOLS) ||
		      ar_type_get_kind (cst->type) == AR_TYPE_ENUMERATION);
	  if (ar_type_get_kind (cst->type) == AR_TYPE_ENUMERATION)
	    id = ar_type_enum_get_value_at_index (cst->type, cst->ival);
	  else
	    id = ar_type_symbol_set_get_value_name (cst->ival);
	  
	  ar_identifier_log (log, id);
	  ar_identifier_del_reference (id);
	}
    }
  else if (cst->kind == AR_CST_STRUCTURE)
    {
      ccl_log (log, "{ ");
      for (i = 0; i < cst->ival; i++)
	{
	  ccl_log (log, ".");
	  ar_identifier_log (log, cst->u.fields[i].fname);
	  ccl_log (log, " = ");
	  ar_constant_log (log, cst->u.fields[i].fval);
	  if (i + 1 < cst->ival)
	    ccl_log (log, ", ");
	}
      ccl_log (log, " }");
    }
  else
    {
      ccl_log (log, "{ ");
      for (i = 0; i < cst->ival; i++)
	{
	  ar_constant_log (log, cst->u.array[i]);
	  if (i + 1 < cst->ival)
	    ccl_log (log, ", ");
	}
      ccl_log (log, " }");      
    }
}

			/* --------------- */

char *
ar_constant_to_string(ar_constant *cst)
{
  char *result = NULL;

  ccl_log_redirect_to_string (CCL_LOG_DISPLAY, &result);
  ar_constant_log (CCL_LOG_DISPLAY, cst);
  ccl_log_pop_redirection (CCL_LOG_DISPLAY);

  return result;
}

			/* --------------- */

int
ar_constant_is_in_domain (ar_constant *cst, ar_type *type)
{
  int result = 1;

  ccl_pre (cst != NULL); 
  ccl_pre (type != NULL);

  if (! ar_constant_has_base_type (cst,type))
    return 0;

  switch (ar_type_get_kind (type)) 
    {
    case AR_TYPE_ENUMERATION:
      ccl_assert (0 <= cst->ival && cst->ival < ar_type_get_cardinality (type));
      break;

    case AR_TYPE_BOOLEANS :   case AR_TYPE_INTEGERS :  case AR_TYPE_SYMBOLS :
      break;

    case AR_TYPE_SYMBOL_SET :
      result = ar_type_symbol_set_has_value(type,cst->ival);
      break;

    case AR_TYPE_ABSTRACT :
      result = 0;
      break;

    case AR_TYPE_RANGE :
      result = (ar_type_range_get_min (type) <= cst->ival  && 
		cst->ival <= ar_type_range_get_max (type));
      break;

    case AR_TYPE_ARRAY :
      {
	int i;
	ar_type *abtype = ar_type_array_get_base_type (type);

	for (i = 0; i < cst->ival && result; i++)
	  result = ar_constant_is_in_domain (cst->u.array[i], abtype);
	ar_type_del_reference (abtype);
      }
      break;
    
    case AR_TYPE_BANG :
    case AR_TYPE_STRUCTURE :
      {
	int i;
	struct field *f;
	
	for (f = cst->u.fields, i = 0; i < cst->ival; i++, f++)
	  {
	    ar_type *ftype = ar_type_struct_get_field (type, f->fname);
	    result = ar_constant_is_in_domain (f->fval, ftype);
	    ar_type_del_reference (ftype);
	  }
      }
      break;
  };

  return result;
}

			/* --------------- */

static ar_constant *
s_crt_constant(ar_constant_kind kind, int ival, ar_type *type)
{
  ar_constant *result = ccl_new(struct ar_constant_st);

  result->refcount = 1;
  result->kind = kind;
  if( type != NULL )
    result->type = ar_type_add_reference(type);
  else 
    result->type = NULL;

  result->ival = ival;

  return result;
}

			/* --------------- */

static void
s_del_constant(ar_constant *cst)
{
  int i;

  ar_type_del_reference(cst->type);
  if( cst->kind == AR_CST_STRUCTURE )
    {
      for(i = 0; i < cst->ival; i++)
	{
	  ar_identifier_del_reference(cst->u.fields[i].fname);
	  ar_constant_del_reference(cst->u.fields[i].fval);
	}
      ccl_delete(cst->u.fields);
    }
  else if( cst->kind == AR_CST_ARRAY )
    {
      for(i = 0; i < cst->ival; i++)
	ar_constant_del_reference(cst->u.array[i]);
      ccl_delete(cst->u.array);
    }
  ccl_delete(cst);
}
