/*
 * ar-stepper.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-solver.h"
#include "ar-stepper.h"

typedef struct solver_data_st {
  varorder *order;
  int nb_variables;
  ar_ca_expr **variables;
  enum { CHECK, SOLVE } mode;  
  int nb_solutions;
  int max_nb_solutions;
  ar_stepper_state_array solutions;
} solver_data;

struct ar_stepper_st {
  int refcount;

  ar_ca *ca;
  varorder *order;

  ar_ca_expr **variables;
  int nb_variables;
  ar_ca_expr **state_variables;
  int nb_state_variables;
  ar_ca_expr **flow_variables;
  int nb_flow_variables;
  ar_ca_expr **local_variables;
  int nb_local_variables;

  ar_backtracking_stack *bstack;
  ar_solver *solver;
  ar_solver *init_solver;
};

			/* --------------- */

#define s_stepper_start_solution_set NULL

static int
s_stepper_add_solution(ar_solver *solver, void *clientdata);

#define s_stepper_end_solution_set NULL

static ar_stepper_state_array
s_do_assignment (ar_stepper *stepper, ar_solver *solver, ar_ca_expr **a, 
		 int a_size, int max);

static int
s_check_assignment(ar_stepper *stepper, ar_ca_expr **a, int a_size);

			/* --------------- */

static void
s_copy_variables (ar_ca *ca, ar_ca_expr **dst)
{
  const ccl_pair *p;
  int r = 2;
  ar_ca_expr **v = dst;
  const ccl_list *vars = ar_ca_get_state_variables(ca);
  while (r--)
    {
      CCL_LIST_FOREACH (p, vars)
	*(v++) = ar_ca_expr_add_reference (CAR (p));
      vars = ar_ca_get_flow_variables (ca);
    }
}

ar_stepper *
ar_stepper_create(ar_ca *ca, varorder *order)
{
  int i;  
  ar_stepper *result = ccl_new(ar_stepper);
  int nb_constraints = ar_ca_get_number_of_assertions (ca);
  ar_ca_expr **constraints = ccl_new_array (ar_ca_expr *, nb_constraints);
  int nb_initial_constraints = ar_ca_get_number_of_initial_constraints (ca);  

  {
    int k = 0;
    const ccl_pair *p;
    const ccl_list *assertions = ar_ca_get_assertions (ca);

    for (p = FIRST (assertions); p; p = CDR (p))
      constraints[k++] = ar_ca_expr_add_reference (CAR (p));
  }

  result->refcount = 1;
  result->ca = ar_ca_add_reference(ca);
  result->order = varorder_add_reference (order);
  result->nb_state_variables = ar_ca_get_number_of_state_variables(ca);
  result->nb_flow_variables = ar_ca_get_number_of_flow_variables(ca);
  result->nb_variables = result->nb_flow_variables + result->nb_state_variables;
  result->variables = ccl_new_array (ar_ca_expr *, result->nb_variables);

  result->state_variables = result->variables;
  result->flow_variables = result->variables + result->nb_state_variables;
  s_copy_variables (ca, result->variables);

  result->bstack = ar_backtracking_stack_create();

  result->solver = ar_create_gp_solver (constraints,nb_constraints,
					result->variables,result->nb_variables,
					result->bstack,
					s_stepper_start_solution_set,
					s_stepper_add_solution,
					s_stepper_end_solution_set);

  {
    const ccl_pair *p;
    const ccl_list *initial_constraints = ar_ca_get_initial_constraints (ca);
    ar_ca_expr **tab = 
      ccl_new_array (ar_ca_expr *, nb_constraints + nb_initial_constraints);
    ccl_memcpy (tab, constraints, sizeof (ar_ca_expr *) * nb_constraints);
    for (i = nb_constraints, p = FIRST (initial_constraints); p;
	 p = CDR (p), i++)
      tab[i] = CAR (p);
    result->init_solver = 
      ar_create_gp_solver (tab, nb_constraints + nb_initial_constraints,
			   result->variables, result->nb_variables,
			   result->bstack, s_stepper_start_solution_set,
			   s_stepper_add_solution, s_stepper_end_solution_set);
    ccl_delete (tab);
  }
  ar_stepper_reset_variables (result, 0, AR_STEPPER_ALL_VARS);

  {
    for (i = 0; i < nb_constraints; i++)
      ar_ca_expr_del_reference (constraints[i]);
    ccl_delete (constraints);
  }
 
  return result;
}

			/* --------------- */

ar_stepper *
ar_stepper_add_reference(ar_stepper *stepper)
{
  ccl_pre( stepper != NULL );

  stepper->refcount++;

  return stepper;
}

			/* --------------- */

void
ar_stepper_del_reference(ar_stepper *stepper)
{
  int i;

  ccl_pre( stepper != NULL );  ccl_pre( stepper->refcount > 0 );

  if( --stepper->refcount > 0 )
    return;

  for(i = 0; i < stepper->nb_variables; i++)
    ar_ca_expr_del_reference(stepper->variables[i]);
  ccl_delete(stepper->variables);
  ar_solver_delete(stepper->solver);
  ar_solver_delete(stepper->init_solver);
  ar_backtracking_stack_del_reference(stepper->bstack);
  varorder_del_reference(stepper->order);
  ar_ca_del_reference(stepper->ca);
  ccl_delete(stepper);
}

			/* --------------- */

ar_ca_expr **
ar_stepper_get_variables (ar_stepper *stepper, int *p_nb_vars)
{
  *p_nb_vars = stepper->nb_variables;

  return stepper->variables;
}

			/* --------------- */

void *
ar_stepper_choice_point(ar_stepper *stepper)
{
  ccl_pre( stepper != NULL );

  return ar_backtracking_stack_choice_point(stepper->bstack);
}

			/* --------------- */

void
ar_stepper_restore_choice_point(ar_stepper *stepper, void *cp)
{
  ccl_pre( stepper != NULL );

  ar_backtracking_stack_restore(stepper->bstack,cp);
}

			/* --------------- */

void
ar_stepper_restore_last_choice_point(ar_stepper *stepper)
{
  ccl_pre( stepper != NULL );

  ar_backtracking_stack_restore_last(stepper->bstack);
}

			/* --------------- */

static void
s_save_variables (ar_stepper *stepper, int size, ar_ca_expr **vars)
{
  for(; size--; vars++ )
    {
      ar_ca_expr_save_bounds (*vars, stepper->bstack);
      ar_ca_expr_reset_bounds (*vars);
    }
}

			/* --------------- */

static void
s_apply_to_variables (ar_stepper *stepper, int which, 
		      void (*func)(ar_stepper *, int, ar_ca_expr **))
{
  ccl_pre (stepper != NULL);

  if( which == AR_STEPPER_NO_VAR ) 
    return;

  if (which == AR_STEPPER_ALL_VARS) 
    func (stepper, stepper->nb_variables, stepper->variables);
  else
    {
      if (which & AR_STEPPER_STATE_VARS)
	func (stepper, stepper->nb_state_variables, stepper->state_variables);

      if (which & AR_STEPPER_FLOW_VARS)
	func (stepper, stepper->nb_flow_variables, stepper->flow_variables);

      if (which & AR_STEPPER_LOCAL_VARS)
	func (stepper, stepper->nb_local_variables, stepper->local_variables);
    }
}

			/* --------------- */

void
ar_stepper_save_variables(ar_stepper *stepper, int which)
{
  s_apply_to_variables (stepper, which, s_save_variables);
}

			/* --------------- */

static void
s_reset_variables (ar_stepper *stepper, int size, ar_ca_expr **vars)
{
  for(; size--; vars++)
    ar_ca_expr_reset_bounds (*vars);
}

			/* --------------- */

static void
s_save_and_reset_variables (ar_stepper *stepper, int size, ar_ca_expr **vars)
{
  for (; size--; vars++)
    {
      ar_ca_expr_save_bounds (*vars, stepper->bstack);
      ar_ca_expr_reset_bounds (*vars);
    }
}

			/* --------------- */

void
ar_stepper_reset_variables(ar_stepper *stepper, int save, int which)
{
  void (*func)(ar_stepper *, int, ar_ca_expr **);

  if (save)
    func = s_save_and_reset_variables;
  else 
    func = s_reset_variables;
  s_apply_to_variables (stepper, which, func);
}

			/* --------------- */

static ar_stepper_state *
s_ensure_single_state (ar_stepper_state_array states)
{
  ar_stepper_state *result;

  ccl_pre (states.size == 1);
  
  result = states.data[0];
  ccl_array_delete (states);
  
  return result;
}
		       
ar_stepper_state *
ar_stepper_get_initial_state (ar_stepper *stepper)
{
  return s_ensure_single_state (ar_stepper_get_initial_states (stepper, 2));
}

ar_stepper_state_array
ar_stepper_get_initial_states (ar_stepper *stepper, int max)
{
  ar_ca_expr **a;
  int a_size;
  ar_stepper_state_array result;

  ccl_pre (stepper != NULL); 
  
  ar_stepper_choice_point (stepper);
  ar_stepper_reset_variables (stepper, 1, AR_STEPPER_STATE_VARS);

  a_size = ar_ca_get_number_of_initial_assignments (stepper->ca);
  a = (ar_ca_expr **)
    ccl_list_to_array (ar_ca_get_initial_assignments (stepper->ca), NULL);
  result = s_do_assignment (stepper, stepper->init_solver, a, a_size, max);
  ar_stepper_restore_last_choice_point (stepper);
  ccl_delete (a);
  
  return result;
}

			/* --------------- */

void
ar_stepper_set_state (ar_stepper *stepper, ar_stepper_state *state, int save)
{
  if (save)
    ar_stepper_save_variables (stepper, AR_STEPPER_ALL_VARS);
  ar_stepper_state_set_variables (state, stepper->order, 
				  stepper->variables, stepper->nb_variables);
}

			/* --------------- */

ar_stepper_trans_array 
ar_stepper_get_valid_transitions(ar_stepper *stepper, int prepost)
{
  int i;
  ar_ca_trans **T = ar_ca_get_transitions(stepper->ca);
  ar_stepper_index_array  indices = 
    ar_stepper_get_index_of_valid_transitions (stepper, prepost);
  ar_stepper_trans_array result;

  ccl_array_init_with_size (result, indices.size);
  for (i = 0; i < indices.size; i++)
    result.data[i] = ar_ca_trans_add_reference (T[indices.data[i]]);

  ccl_array_delete (indices);

  return result;
}

			/* --------------- */

int 
ar_stepper_is_valid_transition (ar_stepper *stepper, ar_ca_trans *t,
				int prepost)
{
  int is_valid = 1;
  
  if (! ar_ca_trans_evaluate_guard (t))
    is_valid = 0;
  else if (prepost)
    {
      ar_ca_expr **a = ar_ca_trans_get_assignments (t);
      int a_size = ar_ca_trans_get_number_of_assignments (t);
      
      is_valid = s_check_assignment (stepper, a, a_size);
    }
  return is_valid;
}

ar_stepper_index_array 
ar_stepper_get_index_of_valid_transitions (ar_stepper *stepper, int prepost)
{
  int i;
  ar_stepper_index_array result;
  int nbT = ar_ca_get_number_of_transitions (stepper->ca);
  ar_ca_trans **T = ar_ca_get_transitions (stepper->ca);
  int nb_valid = 0;

  ccl_array_init_with_size (result, nbT);
  for(i = 0; i < nbT; i++)
    {
      if (ar_stepper_is_valid_transition (stepper, T[i], prepost))
	{
	  ccl_assert (nb_valid < result.size);
	  result.data[nb_valid++] = i;
	}
    }

  ccl_array_trim (result, nb_valid);

  return result;
}

			/* --------------- */

ar_stepper_trans_array 
ar_stepper_get_valid_transitions_pre(ar_stepper *stepper)
{
  return ar_stepper_get_valid_transitions(stepper,0);
}

			/* --------------- */

ar_stepper_trans_array 
ar_stepper_get_valid_transitions_prepost(ar_stepper *stepper)
{
  return ar_stepper_get_valid_transitions(stepper,1);
}

			/* --------------- */

ar_stepper_state_array
ar_stepper_trigger_transition(ar_stepper *stepper, ar_ca_trans *T, int max)
{
  ar_ca_expr **a;
  int a_size;
  ar_stepper_state_array result;

  ccl_pre( stepper != NULL ); ccl_pre( T != NULL ); 

  ar_stepper_choice_point(stepper); 
  a = ar_ca_trans_get_assignments(T);
  a_size = ar_ca_trans_get_number_of_assignments(T);
  result = s_do_assignment(stepper,stepper->solver, a,a_size,max);
  ar_stepper_restore_last_choice_point(stepper);

  return result;
}

ar_stepper_state *
ar_stepper_trigger (ar_stepper *stepper, ar_ca_trans *T)
{
  return s_ensure_single_state (ar_stepper_trigger_transition (stepper, T, 2));
}

			/* --------------- */

static int
s_check_value_is_in_domain (int varindex, int val, void *data)
{
  solver_data *sd = data;
  ar_ca_expr *var = varorder_get_variable_from_index (sd->order, varindex);
  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
  
  return ar_ca_domain_has_element (dom, val);
}

static int
s_stepper_add_solution(ar_solver *solver, void *clientdata)
{
  solver_data *data = (solver_data *)clientdata;

  if( data->mode == SOLVE )
    {
      ar_stepper_state *state = ar_stepper_state_create (data->nb_variables);
      int is_singleton = 
	ar_stepper_state_set_values (state, data->order, data->variables, 
				     data->nb_variables);

      if( is_singleton )
	{
	  if( data->nb_solutions == data->solutions.size )
	    ccl_array_ensure_size(data->solutions,2*data->nb_solutions);
	  data->solutions.data[data->nb_solutions++] = state;
	}
      else
	{
	  ar_stepper_state_expand_state_with_limit(state,
						   &data->solutions,
						   &data->nb_solutions,
						   data->max_nb_solutions,
						   s_check_value_is_in_domain,
						   data);
	  ar_stepper_state_delete(state);
	}
    }
  else
    {
      data->nb_solutions++;
    }

  return (data->mode == SOLVE);
}

			/* --------------- */

static int
s_perform_assignment (ar_stepper *stepper, ar_ca_expr **a, int a_size)
{
  ar_ca_expr *var;
  ar_ca_expr *value;
  ar_bounds *bv;
  int vmin, vmax, valmin, valmax;

  if( a_size == 0 )
    return 1;

  var  = a[0];
  bv = ar_ca_expr_get_bounds_address(var);
  vmin = bv->min;
  vmax = bv->max;
  value = a[1];      
  ar_ca_expr_evaluate(value,1);
  valmin = ar_ca_expr_get_min(value);
  valmax = ar_ca_expr_get_max(value);
  
  {
    const ar_ca_domain *dom = ar_ca_expr_variable_get_domain(var);

    ccl_assert( valmin == valmax );

    if( ! ar_ca_domain_has_element(dom,valmin) )
      return 0;
  }

  if( ! s_perform_assignment(stepper,a+2,a_size-1) )
    return 0;

  if( ! (vmin == valmin && vmax == valmax ) )
    {
      ar_bounds_save(bv,stepper->bstack);
      ar_bounds_set(bv,valmin,valmax);
    }

  return 1;
}

			/* --------------- */


static ar_stepper_state_array
s_do_assignment(ar_stepper *stepper, ar_solver *solver, ar_ca_expr **a, 
		int a_size, int max)
{
  solver_data data;
  ar_stepper_state_array result;

  if( ! s_perform_assignment (stepper, a, a_size) )    
    {
      ccl_array_init(result);
      return result;
    }

  data.order = stepper->order;
  data.variables = stepper->variables;
  data.nb_variables = stepper->nb_variables;
  data.mode = SOLVE;
  data.nb_solutions = 0;
  data.max_nb_solutions = max;
  ccl_array_init_with_size(data.solutions,1);
      
  ar_stepper_reset_variables(stepper,1,AR_STEPPER_FLOW_VARS);

  ar_solver_solve (solver, &data);
  result = data.solutions;

  if( result.size != data.nb_solutions )
    {
      ccl_assert( data.nb_solutions < result.size );
      ccl_array_trim(result,data.nb_solutions);
    }

  return result;
}

			/* --------------- */

static int
s_check_assignment(ar_stepper *stepper, ar_ca_expr **a, int a_size)
{
  solver_data data;

  ar_stepper_choice_point(stepper);
  data.nb_solutions = 0;
      
  /* s_perform_assignment saves state variables into the stack if modified. */
  if( s_perform_assignment(stepper,a,a_size) )
    {
      data.variables = stepper->variables;
      data.nb_variables = stepper->nb_variables;
      data.mode = CHECK;   
      ar_stepper_reset_variables(stepper,1,AR_STEPPER_FLOW_VARS);
      ar_solver_solve(stepper->solver,&data);
    }
  ar_stepper_restore_last_choice_point(stepper);

  return (data.nb_solutions > 0);
}

			/* --------------- */

ar_stepper_state_array
ar_stepper_get_states_for_expr (ar_stepper *stepper, ar_ca_expr *e, int max)
{
  int i;
  const ccl_pair *p;
  solver_data data;
  ar_stepper_state_array result;
  ar_solver *solver;
  ar_ca *ca = stepper->ca;
  int nb_constraints = ar_ca_get_number_of_assertions (ca);
  ar_ca_expr **constraints = ccl_new_array (ar_ca_expr *, 1 + nb_constraints);
  const ccl_list *assertions = ar_ca_get_assertions (ca);
  int nb_variables = ar_ca_get_number_of_variables (ca);
  ar_ca_expr **variables = ccl_new_array (ar_ca_expr *, nb_variables);
   
  constraints[0] = e;
  for (i = 1, p = FIRST (assertions); p; p = CDR (p), i++)
    constraints[i] =  CAR (p);

  nb_constraints++;
  ccl_assert (i == nb_constraints);
  s_copy_variables (ca, variables);
  
  for (i = 0; i < nb_variables; i++)
    ar_ca_expr_variable_reset_bounds (variables[i]);

  solver = ar_create_gp_solver (constraints, nb_constraints, 
				variables, nb_variables, 
				NULL,
				s_stepper_start_solution_set,
				s_stepper_add_solution,
				s_stepper_end_solution_set);

  data.order = stepper->order;
  data.nb_variables = nb_variables;
  data.variables = variables;
  data.mode = SOLVE;
  data.nb_solutions = 0;
  data.max_nb_solutions = max;
  ccl_array_init_with_size (data.solutions, 1);

  ar_solver_solve (solver, &data);

  result = data.solutions;

  if (result.size != data.nb_solutions)
    {
      ccl_assert (data.nb_solutions < result.size);
      ccl_array_trim (result, data.nb_solutions);
    }

  for (i = 0; i < nb_variables; i++)
    ar_ca_expr_del_reference (variables[i]);
  ccl_delete (variables);
  ccl_delete (constraints);
  ar_solver_delete (solver);

  return result;
}
