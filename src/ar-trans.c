/*
 * ar-trans.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include "ar-trans.h"

struct ar_trans_st {
  int refcount;
  ar_expr *g;
  ar_event *e;
  ar_trans_assignment *a;
};

ar_trans *
ar_trans_create(ar_expr *g, ar_event *e)
{
  ar_trans *result = ccl_new(ar_trans);

  result->refcount = 1;
  result->g = ar_expr_add_reference(g);
  result->e = ar_event_add_reference(e);
  result->a = NULL;

  return result;
}

			/* --------------- */

ar_trans *
ar_trans_add_reference(ar_trans *t)
{
  ccl_pre( t != NULL );

  t->refcount++;

  return t;
}

			/* --------------- */

void
ar_trans_del_reference(ar_trans *t)
{
  ccl_pre( t != NULL ); ccl_pre( t->refcount > 0 );

  if( --t->refcount == 0 )
    {
      ar_trans_assignment *a;
      ar_trans_assignment *next;

      ar_expr_del_reference(t->g);
      ar_event_del_reference(t->e);
      for(a = t->a; a; a = next)
	{
	  next = a->next;
	  ar_expr_del_reference (a->lvalue);
	  ar_expr_del_reference(a->value);
	  ccl_delete(a);
	}
      ccl_delete(t);
    }
}

			/* --------------- */

ar_expr *
ar_trans_get_guard(ar_trans *t)
{
  ccl_pre( t != NULL );

  return ar_expr_add_reference(t->g);
}

			/* --------------- */

void
ar_trans_set_guard(ar_trans *t, ar_expr *g)
{
  ccl_pre( t != NULL );

  if( t->g !=  NULL )
    ar_expr_del_reference(t->g);
  t->g = ar_expr_add_reference(g);
}

			/* --------------- */

ar_event *
ar_trans_get_event(ar_trans *t)
{
  ccl_pre( t != NULL );

  return ar_event_add_reference(t->e);
}

			/* --------------- */

int
ar_trans_has_epsilon_event (ar_trans *t)
{
  ccl_pre( t != NULL );

  return ar_event_has_epsilon_name (t->e);
}

int
ar_trans_has_event(ar_trans *t, ar_event *ev)
{
  ccl_pre( t != NULL );

  return ar_event_equals(t->e,ev);
}

			/* --------------- */

void
ar_trans_set_event(ar_trans *t, ar_event *e)
{
  ccl_pre( t != NULL ); ccl_pre( t->e != NULL ); ccl_pre( e != NULL );

  ar_event_del_reference(t->e);
  t->e = ar_event_add_reference(e);
}

			/* --------------- */

void
ar_trans_add_assignment(ar_trans *t, ar_expr *lvalue, ar_expr *value)
{
  ar_trans_assignment *a;

  a = ccl_new (ar_trans_assignment);
  a->lvalue = ar_expr_add_reference (lvalue);
  a->value = ar_expr_add_reference (value);
  a->next = t->a;
  t->a = a;
}

			/* --------------- */

static ar_trans_assignment *
s_find_assignment_for (ar_trans *t, ar_expr *lvalue)
{
  ar_trans_assignment *a;

  ccl_pre (t != NULL);

  for(a = t->a; a && a->lvalue != lvalue; a = a->next)
    /* do nothing */;

  return a;
}

			/* --------------- */

ar_expr *
ar_trans_get_assignment (ar_trans *t, ar_expr *lvalue)
{
  ar_expr *result = NULL;
  ar_trans_assignment *a = s_find_assignment_for (t, lvalue);

  if (a != NULL)
    {
      ccl_assert (a->lvalue == lvalue);
      result = ar_expr_add_reference (a->value);
    }

  return result;
}

			/* --------------- */

int
ar_trans_is_assigned (ar_trans *t, ar_expr *lvalue)
{
  return (s_find_assignment_for (t, lvalue) != NULL);
}

			/* --------------- */

const ar_trans_assignment *
ar_trans_get_assignments(ar_trans *t)
{
  ccl_pre( t != NULL );

  return t->a;
}

			/* --------------- */

void
ar_trans_log(ccl_log_type log, ar_trans *t)
{
  ar_trans_log_gen(log,t,ar_identifier_log);
}

			/* --------------- */

void
ar_trans_log_gen(ccl_log_type log, ar_trans *t,
		 void (*idlog)(ccl_log_type log, const ar_identifier *id))
{
  ar_trans_assignment *a;

  ccl_pre( t != NULL );

  ar_expr_log_gen(log,t->g,idlog);
  ccl_log(log," |- ");
  ar_event_log (log, t->e);
  ccl_log(log," -> ");
  for(a = t->a; a; a = a->next)
    {
      ar_expr_log (log, a->lvalue);
      ccl_log(log," := ");
      ar_expr_log_gen(log,a->value,idlog);
      if( a->next != NULL )
	ccl_log(log,", ");
    }
}

			/* --------------- */

ar_trans *
ar_trans_dup(ar_trans *t)
{
  ar_trans_assignment *a;
  ar_trans *result = ar_trans_create(t->g,t->e);

  for(a = t->a; a; a = a->next)
    ar_trans_add_assignment(result,a->lvalue,a->value);

  return result;
}

int
ar_trans_is_epsilon_loop (ar_trans *t)
{
  ccl_pre (t != NULL);
  
  return (ar_expr_is_true (t->g) && ar_event_is_epsilon_vector (t->e) &&
	  t->a == NULL);
}
