/*
 * ar-interp-altarica.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-string.h>
#include <ccl/ccl-memory.h>
#include "ar-model.h"
#include "ar-interp-altarica-p.h"

CCL_DEFINE_EXCEPTION (invalid_node_type_error, internal_error);
CCL_DEFINE_EXCEPTION (altarica_interpretation_exception, exception);

			/* --------------- */

void
ar_interp_altarica_constant_decl (altarica_tree *t, ar_context *ctx)
{
  int error = 0;
  altarica_tree *ident = t->child;
  altarica_tree *domain = NULL;
  altarica_tree *value = NULL;
  ar_identifier *cname;
  ar_type * volatile type = NULL;
  ar_expr * volatile expr = NULL;

  ccl_pre (IS_LABELLED_BY (t, CONSTANT));

  if (t->value.int_value)
    {
      domain = ident->next;
      value = domain->next;
    }
  else
    {
      value = ident->next;
    }

  cname = ar_interp_symbol_as_identifier (ident);

  if (ar_model_has_parameter (cname))
    {      
      ar_identifier_del_reference (cname);
      INTERP_EXCEPTION_MSG ((t,"redefinition of constant '%s'.\n",
			     t->child->value.id_value));	
    }
  
  if (ar_model_has_node (cname))
    {
      ar_identifier_del_reference (cname);
      INTERP_EXCEPTION_MSG ((t, "the constant '%s' clashes with a node with "
			     "the same name.\n", t->child->value.id_value));	
    }

  ccl_try (altarica_interpretation_exception)
    {
      if (domain != NULL)
	type = ar_interp_domain (domain, ctx, ar_interp_member_access);

      if (value != NULL)
	{
	  expr = ar_interp_expression (value, type, ctx, 
				       ar_interp_member_access);
	  if (type != NULL && ar_expr_is_computable (expr))
	    {
	      ar_constant *cst = ar_expr_eval (expr, ctx);
	      int in_dom = ar_constant_is_in_domain (cst, type);
	      ar_constant_del_reference (cst);
	      
	      if (!in_dom)
		{
		  INTERP_EXCEPTION_MSG ((t,"value assigned to constant "
					 "goes out of its domain.\n"));	
		}
	    }
	}

      ccl_assert (type != NULL || expr != NULL);

      if (type == NULL)
	type = ar_expr_get_type (expr);

      ar_model_add_parameter (cname, type, expr);
    }
  ccl_catch
    {
      error = 1;
    }
  ccl_end_try;

  ar_identifier_del_reference (cname);

  ccl_zdelete (ar_type_del_reference, type);
  ccl_zdelete (ar_expr_del_reference, expr);

  if( error )
    ccl_rethrow();
}

			/* --------------- */

void
ar_interp_altarica_domain_decl (altarica_tree *t, ar_context *ctx)
{
  ar_identifier *dname;
  ar_type *type = NULL;

  ccl_pre (IS_LABELLED_BY (t, DOMAIN));

  dname = ar_interp_symbol_as_identifier (t->child);
  if (ar_model_has_type (dname))
    {
      ar_identifier_del_reference (dname);
      INTERP_EXCEPTION_MSG ((t,"redefinition of domain '%s'.\n",
			     t->child->value.id_value));  
    }

  ccl_try (altarica_interpretation_exception)
    {
      type = ar_interp_domain (t->child->next, ctx, ar_interp_member_access);
      ar_model_set_type (dname, type);
    }
  ccl_no_catch;

  ar_identifier_del_reference (dname);
  if (type != NULL)
    ar_type_del_reference (type);
  else
    ccl_rethrow ();
}

			/* --------------- */

static void
s_interp_node_decl (altarica_tree *t)
{
  ar_node *node = NULL;
  ar_identifier *nname;

  ccl_pre (IS_LABELLED_BY (t, NODE));

  nname  = ar_interp_symbol_as_identifier (t->child);
  if (ar_model_has_node (nname))
    {
      ar_identifier_del_reference (nname);
      INTERP_EXCEPTION_MSG ((t, "redefinition of node '%s'.\n",
			     t->child->value.id_value));  
    }

  if (ar_model_has_parameter (nname))
    {
      ar_identifier_del_reference (nname);
      INTERP_EXCEPTION_MSG ((t, "the node '%s' redefines the constant with "
			     "the same name.\n",
			     t->child->value.id_value));  
    }

  ccl_try (altarica_interpretation_exception)
    {
      node = ar_interp_node (t, AR_MODEL_CONTEXT);
      ar_model_set_node (nname, node);
    }
  ccl_no_catch;

  ar_identifier_del_reference (nname);

  if (node != NULL)
    ar_node_del_reference (node);
  else
    ccl_rethrow ();
}

			/* --------------- */

static void
s_interp_sort_decl (altarica_tree *t)
{
  int error = 0;
  ccl_list *idlist;

  ccl_pre (IS_LABELLED_BY (t, SORT));

  idlist = ar_interp_id_list (t->child, 1);
  ccl_try (altarica_interpretation_exception)
    {
      ccl_pair *p;

      for(p = FIRST (idlist); p; p = CDR (p))
	{
	  ar_identifier *sort_name = (ar_identifier *) CAR (p);


	  if (ar_model_has_type (sort_name))
	    {
	      ar_identifier_del_reference (sort_name);
	      ar_error (t, "redefinition of type '");
	      ar_identifier_log (CCL_LOG_ERROR, sort_name);
	      ccl_error ("'.\n");
	      INTERP_EXCEPTION();
	    }
	  else
	    {
	      ar_type *new_type = ar_type_crt_abstract (sort_name);
	      ar_model_set_type (sort_name, new_type);
	      ar_type_del_reference (new_type);
	    }
	}
    }
  ccl_catch
    {
      error = 1;
    }
  ccl_end_try;

  while (! ccl_list_is_empty (idlist))
    {
      ar_identifier *id = ccl_list_take_first (idlist);
      ar_identifier_del_reference (id);
    }
  ccl_list_delete (idlist);

  if (error)
    ccl_rethrow ();
}

			/* --------------- */

static void
s_interp_signature_decl (altarica_tree *t)
{
  ar_type *retdom = ar_interp_domain (t->child->next->next, AR_MODEL_CONTEXT,
				      ar_interp_member_access);
  ar_identifier *signame = ar_interp_symbol_as_identifier (t->child);
  ar_signature *sig = ar_signature_create (signame, retdom);

  ar_type_del_reference (retdom);

  ccl_try (altarica_interpretation_exception)
    {

      if (ar_model_has_signature (signame))
	INTERP_EXCEPTION_MSG ((t, "redefinition of signature '%s'.\n",
			       t->child->value.id_value));  
				
      t = t->child->next->child;

      while (t != NULL)
	{
	  ar_type *dom = ar_interp_domain (t, AR_MODEL_CONTEXT,
					   ar_interp_member_access);
	  ar_signature_add_arg_domain (sig, dom);
	  ar_type_del_reference (dom);
	  t = t->next;
	}
      ar_model_set_signature (signame, sig);
    }
  ccl_catch
    {
      ar_identifier_del_reference (signame);
      ar_signature_del_reference (sig);
      ccl_rethrow ();
    }
  ccl_end_try;

  ar_signature_del_reference (sig);
  ar_identifier_del_reference (signame);
}

			/* --------------- */

int
ar_interp_altarica_tree (altarica_tree *t)
{
  int result = 1;
  altarica_tree *next;

  for (; t != NULL; t = next)
    {
      next = t->next;

      ccl_try (altarica_interpretation_exception)
	{
	  switch (t->node_type) 
	    {
	    case AR_TREE_CONSTANT : 
	      ar_interp_altarica_constant_decl (t, AR_MODEL_CONTEXT); 
	      break;

	    case AR_TREE_DOMAIN : 
	      ar_interp_altarica_domain_decl (t, AR_MODEL_CONTEXT); 
	      break;

	    case AR_TREE_NODE      : s_interp_node_decl (t); break;
	    case AR_TREE_SORT      : s_interp_sort_decl (t); break;
	    case AR_TREE_SIGNATURE : s_interp_signature_decl (t); break;
	    case AR_TREE_EXTERN_DECL : ar_interp_extern_decl (t, NULL); break;
	    default                : INVALID_NODE_TYPE (); break;
	    }
	}
      ccl_catch
	{      
	  result = 0;
	}
      ccl_end_try;
    }

  return result;
}

			/* --------------- */

ar_identifier *
ar_interp_symbol_as_identifier (altarica_tree *t)
{
  ccl_pre (t != NULL); 
  ccl_pre (IS_LABELLED_BY (t,IDENTIFIER));

  return ar_identifier_create_simple (t->value.id_value);
}

			/* --------------- */

ccl_list *
ar_interp_id_list (altarica_tree *t, int unique)
{
  ccl_list *result;

  ccl_pre (IS_LABELLED_BY (t, ID_LIST));

  result = ccl_list_create ();
  for (t = t->child; t; t = t->next)
    {
      ar_identifier *id = ar_interp_symbol_as_identifier (t);

      if (unique && ccl_list_has (result, id))
	{
	  ccl_list_clear_and_delete (result,
				    (ccl_delete_proc *)
				     ar_identifier_del_reference);

	  ar_identifier_del_reference (id);

	  ccl_error ("%s:%d: error: duplicated identifier '", t->filename,
		     t->line);
	  ar_identifier_log (CCL_LOG_ERROR, id);
	  ccl_error ("'.\n");
	  INTERP_EXCEPTION ();
	}
      ccl_list_add (result, id);
    }

  return result;
}

			/* --------------- */

void
ar_error (altarica_tree *loc, const char *fmt, ...)
{
  va_list  pa;

  if (loc != NULL)
    ccl_error ("%s:%d: error: ", loc->filename, loc->line);
    
  va_start (pa, fmt);
  ccl_log_va (CCL_LOG_ERROR, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ar_warning (altarica_tree *loc, const char *fmt, ...)
{
  va_list  pa;

  if (loc != NULL)
    ccl_warning ("%s:%d: warning: ", loc->filename, loc->line);
    
  va_start (pa, fmt);
  ccl_log_va (CCL_LOG_WARNING, fmt, pa);
  va_end (pa);
}
