/*
 * ar-event-poset.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_EVENT_POSET_H
# define AR_EVENT_POSET_H

# include "ar-poset.h"
# include "ar-event.h"

typedef ar_poset ar_event_poset;

CCL_ITERATOR_TYPEDEF(ar_event_iterator, ar_event *);

extern ar_event_poset *
ar_event_poset_create(void);

# define ar_event_poset_add_reference ar_poset_add_reference

# define ar_event_poset_del_reference ar_poset_del_reference

# define ar_event_poset_get_set_size ar_poset_get_set_size

# define ar_event_poset_add_event ar_poset_add

# define ar_event_poset_has_event ar_poset_has

# define ar_event_poset_get_event(ep,e) ((ar_event *)ar_poset_get_object(ep,e))

# define ar_event_poset_has_path ar_poset_has_path 

# define ar_event_poset_add_pair ar_poset_add_pair

# define ar_event_poset_add_poset ar_poset_add_poset

# define ar_event_poset_add_lt ar_poset_add_lt

# define ar_event_poset_reverse ar_poset_reverse 

# define ar_event_poset_get_greater_events ar_poset_get_greater_objects

# define ar_event_poset_get_events(ep) \
  ((ar_event_iterator *)ar_poset_get_objects(ep))

# define ar_event_poset_is_empty ar_poset_is_empty

# define ar_event_poset_log(l,ep) \
 ar_poset_log(l,ep,(void (*)(ccl_log_type, void *))ar_event_log)

# define ar_event_poset_dup(ep) ar_poset_dup(ep)

#endif /* ! AR_EVENT_POSET_H */
