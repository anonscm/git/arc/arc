/*
 * ar-bang-ids.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_BANG_IDS_H
# define AR_BANG_IDS_H

# include "ar-identifier.h"
# include "ar-node.h"

#define AR_BANG_DEF(_id, _label, _hlp, _desc) _id, 
typedef enum {
# include "ar-bang-ids.def"
  AR_BANG_LAST_AND_UNUSED_ID
} ar_bang_t;
#undef AR_BANG_DEF

extern const char *const ar_bang_labels[];

extern ar_identifier *
ar_bang_id_for_node (ar_node *node, ar_bang_t bang);

extern ar_identifier *
ar_bang_id_for_id (ar_identifier *id, ar_bang_t bang);

extern int 
ar_bang_str_to_bang_t (const char *str, ar_bang_t *pbang);

#endif /* ! AR_BANG_IDS_H */
