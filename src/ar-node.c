/*
 * ar-node.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include "ar-model.h"
#include "ar-attributes.h"
#include "ar-broadcast.h"
#include "ar-node.h"

typedef struct node_parameter {
  struct node_parameter *next;
  ar_context_slot *slot;
} node_parameter;

typedef struct node_subnode {
  struct node_subnode *next;
  ar_context_slot *slot;
  ar_node *model;
} node_subnode;

typedef struct bucket {
  ar_identifier *id;
  ccl_list *events;
  ccl_list *probas;
} bucket;
			/* --------------- */

struct ar_node_st {
  int refcount;
  int uid;
  ar_identifier *name;

  ar_node *ref;

  altarica_tree *node_template;
  
  ar_type *vars_type;
  ar_type *conf_type;
  ar_type *event_type;
  ar_type *this_event_type;

  ar_context *ctx;
  node_parameter *parameters;
  ccl_list *variables;
  node_subnode *subfields;
  node_subnode *subnodes;

  ar_idtable *param_setting;
  ccl_list *attributes;
  ccl_list *transitions;
  ccl_list *assertions;
  ccl_list *sync_vectors;
  ccl_list *initialized_slots;
  ccl_hash *initial_state_map;
  ccl_list *initial_constraints;
  
  ccl_list *law_params;
  ccl_list *laws;
  ar_idtable *lawtable;
  ccl_list *observers;
  ar_idtable *obstable;  
  ccl_set *preemptibles;

  ar_idtable *priorities;
  
  ccl_list *buckets;
  ar_idtable *event_to_buckets_table;
  
  ar_event_poset *poset;
  ar_idtable *events;
};

			/* --------------- */

static int ID_COUNTER = 0;

static void
s_allocate_slots(ar_node *node, ar_identifier *id, ar_type *type, 
		 uint32_t flags, ccl_list *attr, ar_expr *value,
		 ar_context_slot **presult);

static void
s_delete_event_list(void *ptr);

static void
s_delete_bucket (bucket *b);

			/* --------------- */

ar_identifier *AR_IDLE_VAR;


int 
ar_node_init (void)
{
  AR_IDLE_VAR = ar_identifier_create ("_idle");

  return 1;
}

			/* --------------- */

void
ar_node_terminate (void)
{
  ar_identifier_del_reference (AR_IDLE_VAR);
}

			/* --------------- */

ar_node *
ar_node_create(ar_context *ctx)
{
  ar_node *result = ccl_new(ar_node);
  ar_identifier *empty = ar_identifier_create ("");

  result->refcount = 1;
  result->uid = ID_COUNTER++;
  result->name = NULL;
  result->ref = NULL;
  result->node_template = NULL;
  result->vars_type = ar_type_crt_variables (result);
  result->conf_type = ar_type_crt_configurations (result);
  result->event_type = ar_type_crt_events (result);
  result->this_event_type = ar_type_crt_enum ();
  ar_type_struct_add_field (result->event_type, empty, result->this_event_type);
  result->ctx = ar_context_create(ctx);
  
  s_allocate_slots (result, empty, result->this_event_type, AR_SLOT_FLAG_EVENT,
		    NULL, NULL, NULL);
  ar_identifier_del_reference (empty);


  result->variables = ccl_list_create ();
  result->parameters = NULL;
  result->subnodes = NULL;
  result->param_setting= 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_idtable_add_reference,
		      (ccl_delete_proc *)ar_idtable_del_reference);
  result->attributes = ccl_list_create();
  result->transitions = ccl_list_create();
  result->assertions = ccl_list_create();
  result->sync_vectors = ccl_list_create();
  result->initialized_slots = ccl_list_create();
  result->initial_state_map =
    ccl_hash_create (NULL, NULL,
		     (ccl_delete_proc *) ar_context_slot_del_reference,
		     (ccl_delete_proc *) ar_expr_del_reference);
  result->initial_constraints = ccl_list_create ();
  result->poset = ar_event_poset_create();
  result->events = ar_idtable_create(1, NULL,
				     (ccl_delete_proc *)s_delete_event_list);

  result->law_params = ccl_list_create ();
  result->laws = ccl_list_create ();
  result->lawtable =
    ar_idtable_create (1,
		       (ccl_duplicate_func *) sas_law_add_reference,
		       (ccl_delete_proc *) sas_law_del_reference);

  result->observers = ccl_list_create ();
  result->obstable =
    ar_idtable_create (1,
		       (ccl_duplicate_func *) ar_expr_add_reference,
		       (ccl_delete_proc *) ar_expr_del_reference);
  result->preemptibles =
    ccl_set_create_for_objects (NULL, NULL,
				(ccl_delete_proc *)
				ar_identifier_del_reference);
  result->buckets = ccl_list_create ();
  result->event_to_buckets_table = ar_idtable_create (0, NULL, NULL);
  result->priorities = ar_idtable_create (1, NULL, NULL);
  
  return result;
}


			/* --------------- */

ar_node *
ar_node_create_parametrized(altarica_tree *tmpl, ar_context *ctx)
{
  ar_node *result = ccl_new(struct ar_node_st);
  
  result->refcount = 1;
  result->uid = ID_COUNTER++;
  result->name = NULL;
  result->ref = NULL;
  result->node_template = ccl_parse_tree_duplicate(tmpl,NULL);
  result->ctx = ar_context_create(ctx);
  result->param_setting = 
    ar_idtable_create(1, (ccl_duplicate_func *)ar_idtable_add_reference,
		      (ccl_delete_proc *)ar_idtable_del_reference);
  result->vars_type = NULL;
  result->conf_type = NULL;
  result->event_type = NULL;
  result->variables = NULL;
  result->subnodes = NULL;
  result->attributes = NULL;
  result->transitions = NULL;
  result->assertions = NULL;
  result->sync_vectors = NULL;
  result->initialized_slots = NULL;
  result->initial_state_map = NULL;
  result->initial_constraints = NULL;
  result->poset = NULL;
  result->events = NULL;
  result->law_params = NULL;
  result->laws = NULL;
  result->lawtable = NULL;  
  result->observers = NULL;
  result->obstable = NULL;
  result->preemptibles = NULL;  
  result->buckets = NULL;
  result->event_to_buckets_table = NULL;
  
  return result;
}


			/* --------------- */

ar_node *
ar_node_create_reduced(ar_node *node)
{
  ar_node *result = ccl_new(ar_node);
  ccl_list *trans = ar_node_get_transitions(node);
  
  result->refcount = 1;
  result->uid = ID_COUNTER++;
  result->name = NULL;
  result->ref = ar_node_add_reference(node);
  result->node_template = NULL;
  result->ctx = NULL;
  result->param_setting = NULL;
  result->vars_type = NULL;
  result->conf_type = NULL;
  result->event_type = NULL;
  result->variables = NULL;
  result->subnodes = NULL;
  result->attributes = NULL;
  
  result->transitions = ccl_list_deep_dup(trans,
					  (ccl_duplicate_func *)
					  ar_trans_add_reference);

  result->assertions = NULL;
  result->sync_vectors = NULL;
  result->initialized_slots = NULL;
  result->initial_state_map = NULL;
  result->initial_constraints = NULL;
  result->poset = NULL;
  result->events = NULL;
  result->law_params = NULL;
  result->laws = NULL;
  result->lawtable = NULL;
  result->observers = NULL;
  result->obstable = NULL;  
  result->preemptibles = NULL;
  result->buckets = NULL;
  result->event_to_buckets_table = NULL;
  
  return result;
}

			/* --------------- */

int 
ar_node_get_uid(ar_node *node)
{
  ccl_pre( node != NULL );

  return node->uid;
}

			/* --------------- */

ar_identifier *
ar_node_get_name(ar_node *node)
{
  ar_identifier *result = NULL;

  ccl_pre( node != NULL );

  if( node->name != NULL )
    result = ar_identifier_add_reference(node->name);
  else if( node->ref != NULL )
    result = ar_node_get_name(node->ref);

  return result;
}

			/* --------------- */

void
ar_node_set_name(ar_node *node, ar_identifier *id)
{
  ccl_pre( node != NULL ); ccl_pre( node->name == NULL );

  node->name = ar_identifier_add_reference(id);
}

			/* --------------- */

ar_node *
ar_node_add_reference(ar_node *node)
{
  ccl_pre( node != NULL );

  node->refcount++;

  return node;
}

			/* --------------- */

void
ar_node_del_reference(ar_node *node)
{
  ccl_pre( node != NULL ); ccl_pre( node->refcount > 0 );

  if( --node->refcount > 0 )
    return;

  ccl_zdelete(ar_node_del_reference,node->ref);    
  ccl_zdelete(ar_identifier_del_reference,node->name);

  ccl_zdelete(ar_context_del_reference,node->ctx);

  ccl_zdelete(ar_idtable_del_reference,node->param_setting);

  if( node->node_template != NULL )
    {
      ccl_parse_tree_delete_tree(node->node_template);
    }
  else if( node->ref == NULL )
    {
      ar_type_del_reference(node->vars_type);
      ar_type_del_reference(node->conf_type);
      ar_type_del_reference(node->event_type);
      ar_type_del_reference(node->this_event_type);

      ccl_list_clear_and_delete (node->variables, (ccl_delete_proc *)
				 ar_context_slot_del_reference);

      {
	node_parameter *np, *next;
	for(np = node->parameters; np; np = next)
	  {
	    next = np->next;
	    ar_context_slot_del_reference(np->slot);
	    ccl_delete(np);
	  }
      }


      {
	node_subnode *ns, *next;
	for(ns = node->subnodes; ns; ns = next)
	  {
	    next = ns->next;
	    ar_context_slot_del_reference(ns->slot);
	    ar_node_del_reference(ns->model);
	    ccl_delete(ns);
	  }
      }

      {
	node_subnode *ns, *next;
	for(ns = node->subfields; ns; ns = next)
	  {
	    next = ns->next;
	    ar_context_slot_del_reference(ns->slot);
	    ar_node_del_reference(ns->model);
	    ccl_delete(ns);
	  }
      }

      ccl_list_clear_and_delete(node->attributes,
				(ccl_delete_proc *)
				ar_identifier_del_reference);
      ccl_list_clear_and_delete(node->assertions,
				(ccl_delete_proc *)ar_expr_del_reference);
      ccl_list_clear_and_delete(node->sync_vectors,
				(ccl_delete_proc *)ar_broadcast_del_reference);

      if( node->initialized_slots != NULL )
	{
	  ccl_list_delete (node->initialized_slots);
	  ccl_hash_delete (node->initial_state_map);
	}

      ccl_list_clear_and_delete (node->initial_constraints,
				 (ccl_delete_proc *) ar_expr_del_reference);

      ccl_zdelete(ar_event_poset_del_reference,node->poset);
      ccl_zdelete(ar_idtable_del_reference,node->events);
    }
  
  if( node->transitions != NULL )
    ccl_list_clear_and_delete(node->transitions,
			      (ccl_delete_proc *)ar_trans_del_reference);

  ccl_zdelete (ccl_list_delete, node->law_params);
  ccl_zdelete (ccl_list_delete, node->laws);
  ccl_zdelete (ar_idtable_del_reference, node->lawtable);
  ccl_zdelete (ccl_list_delete, node->observers);
  ccl_zdelete (ar_idtable_del_reference, node->obstable);
  ccl_zdelete (ccl_set_delete, node->preemptibles);
  ccl_zdelete (ar_idtable_del_reference, node->priorities);
  if (node->buckets != NULL)
    {
      ccl_list_clear_and_delete (node->buckets, (ccl_delete_proc *)
				 s_delete_bucket);
      ar_idtable_del_reference (node->event_to_buckets_table);
    }
  ccl_delete(node);
}

			/* --------------- */

ar_context *
ar_node_get_context (const ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_context(node->ref);
  return ar_context_add_reference(node->ctx);
}

			/* --------------- */

ar_context *
ar_node_get_parent_context(ar_node *node)
{
  ar_context *ctx = ar_node_get_context(node);
  ar_context *result = ar_context_get_parent(ctx);
 ar_context_del_reference(ctx);

 return result;
}

			/* --------------- */

ar_type *
ar_node_get_configuration_type (ar_node *node)
{
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_configuration_type (node->ref);

  return ar_type_add_reference (node->conf_type);
}

			/* --------------- */

ar_type *
ar_node_get_configuration_base_type(ar_node *node)
{  
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_configuration_type (node->ref);

  return ar_type_add_reference (node->vars_type);
}

			/* --------------- */

ar_type *
ar_node_get_event_type (ar_node *node)
{
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_event_type (node->ref);

  return ar_type_add_reference(node->event_type);
}

			/* --------------- */

#define FIELD_TAB "  "
#define ELEMENT_TAB FIELD_TAB " "


static void
s_display_node_header(ccl_log_type log, ar_node *node,
		      void (*idlog)(ccl_log_type, const ar_identifier*))
{
  ccl_log(log,"node ");

  {
    ar_identifier *id = ar_node_get_name(node);
    if( id != NULL )
      {
	idlog(log,id);
	ar_identifier_del_reference(id);
      }
  }

  {
    ccl_list *attr = ar_node_get_attributes(node);
    if( ccl_list_get_size(attr) )
      {
	ccl_pair *p;
	
	ccl_log(log," : ");
	for(p = FIRST(attr); p; p = CDR(p))
	  {
	    idlog(log,(ar_identifier *)CAR(p));
	    if( CDR(p) )
	      ccl_log(log,", ");
	  }
      }
  }
  ccl_log(log,"\n");
}

			/* --------------- */

static void
s_display_parameters(ccl_log_type log, ar_node *node,
		     void (*idlog)(ccl_log_type, const ar_identifier*),
		     ccl_hash *type_cache)
{
  ar_context_slot_iterator *i = ar_node_get_slots_for_parameters(node);

  if( ccl_iterator_has_more_elements(i) )
    {
      int n = 1;

      ccl_log(log,FIELD_TAB "param\n");
	
      while( ccl_iterator_has_more_elements(i) )
	{
	  ar_context_slot *sl = ccl_iterator_next_element(i);
	  ar_identifier *id = ar_context_slot_get_name(sl);
	  ar_type *type = ar_context_slot_get_type(sl);
	  
	  ccl_log(log,ELEMENT_TAB,n++);
	  idlog(log,id);
	  ccl_log(log," : ");
	  if( type_cache != NULL && ccl_hash_find(type_cache,type) )
	    {
	      ccl_hash_find(type_cache,type);
	      idlog(log,ccl_hash_get(type_cache));
	    }
	  else
	    {
	      ar_type_log(log,type);
	    }
	  ccl_log(log,";\n");
	  ar_identifier_del_reference(id);
	  ar_type_del_reference(type);
	  ar_context_slot_del_reference(sl);
	}
    }
  ccl_iterator_delete(i);
}

			/* --------------- */

static void
s_display_variables(ccl_log_type log, ar_node *node,
		    void (*idlog)(ccl_log_type, const ar_identifier*),
		    ccl_hash *type_cache)
{    
  ar_context_slot_iterator *i = ar_node_get_slots_for_variables(node);
  uint32_t current_kind = 0;
  int sn = 1, fn = 1, *pn = &sn;

  while( ccl_iterator_has_more_elements(i) )
    {
      ar_context_slot *slot = ccl_iterator_next_element(i);
      int flags = ar_context_slot_get_flags(slot);
      ar_identifier *id = ar_context_slot_get_name(slot);
      ar_type *type = ar_context_slot_get_type(slot);
      ccl_list *attr = ar_context_slot_get_attributes(slot);

      ar_context_slot_del_reference(slot);

      if( (flags & current_kind) == 0 )
	{
	  if( (flags & AR_SLOT_FLAG_STATE_VAR) )
	    {
	      current_kind = AR_SLOT_FLAG_STATE_VAR;
	      pn = &sn;
	      ccl_log(log,FIELD_TAB "state\n");
	    }
	  else 
	    {
	      current_kind = AR_SLOT_FLAG_FLOW_VAR;
	      pn = &fn;
	      ccl_log(log,FIELD_TAB "flow\n");
	    }
	}

      ccl_log (log, ELEMENT_TAB, (*pn)++);
	
      idlog(log,id);
      ar_identifier_del_reference(id);
      ccl_log(log," : ");

      if( type_cache != NULL && ccl_hash_find(type_cache,type) )
	idlog(log,ccl_hash_get(type_cache));
      else
	ar_type_log(log,type);
      ar_type_del_reference(type);

      if( attr != NULL )
	{
	  if( ccl_list_get_size(attr) > 0 )
	    {
	      ccl_pair *p;
		
	      ccl_log(log," : ");
		
	      for(p = FIRST(attr); p; p = CDR(p))
		{
		  idlog(log,CAR(p));
		  if( CDR(p) != NULL )
		    ccl_log(log,", ");
		}
	    }
	  ccl_list_clear_and_delete(attr,(ccl_delete_proc *)
				    ar_identifier_del_reference);
	}
      ccl_log(log,";\n");
    }
  ccl_iterator_delete(i);
}

			/* --------------- */

static void
s_display_events(ccl_log_type log, ar_node *node, 
		 void (*idlog)(ccl_log_type, const ar_identifier*))
{
  if( ar_node_get_number_of_events(node) <=1 )
    ccl_log(log,FIELD_TAB "// no event is explicitly defined.\n");
  else
    {
      int n = 1;
      ar_event_poset *poset = ar_node_get_event_poset(node);
      ar_event_iterator *eit = ar_event_poset_get_events(poset);
	
      ccl_log(log,FIELD_TAB "event\n",
	      ar_event_poset_get_set_size(poset));
	
      while( ccl_iterator_has_more_elements(eit) )
	{
	  ar_event *ev = ccl_iterator_next_element(eit);
	  if (! ar_event_is_epsilon_vector (ev))
	    {
	      const ccl_list *attr = ar_event_get_attr(ev);
	    
	      ccl_log(log, ELEMENT_TAB, n++);
	      ar_event_log_gen (log, ev, idlog);
	      
	      if( attr != NULL && ccl_list_get_size(attr) > 0 )
		{
		  ccl_pair *p;
		  
		  ccl_log(log," : ");
		  
		  for(p = FIRST(attr); p; p = CDR(p))
		    {
		      idlog(log,CAR(p));
		      if( CDR(p) != NULL )
			ccl_log(log,", ");
		    }
		}
	      ccl_log(log,";\n");
	    }
	  ar_event_del_reference(ev);
	}
      ccl_iterator_delete(eit);

      eit = ar_event_poset_get_events(poset);
      while( ccl_iterator_has_more_elements(eit) )
	{
	  ar_event *ev = ccl_iterator_next_element(eit);
	  ccl_list *gt = ar_event_poset_get_greater_events(poset,ev);
	  
	  if( ccl_list_get_size(gt) > 0 )
	    {
	      ccl_pair *p;

	      ccl_assert (! ar_event_is_epsilon_vector (ev));
	      ccl_log(log,ELEMENT_TAB);
	      ar_event_log_gen (log, ev, idlog);
	      if( ccl_list_get_size(gt) > 1 ) ccl_log(log," < {");
	      else ccl_log(log," < ");
	      for(p = FIRST(gt); p; p = CDR(p))
		{
		  ar_event_log_gen (log, (ar_event *)CAR(p), idlog);
		  if( CDR(p) != NULL )
		    ccl_log(log,", ");
		}
	      if( ccl_list_get_size(gt) > 1 ) ccl_log(log,"};\n");
	      else ccl_log(log,";\n");
	    }
	  ccl_list_delete(gt);
	  ar_event_del_reference(ev);
	}
      ccl_iterator_delete(eit);
      ar_event_poset_del_reference(poset);
    }
}

			/* --------------- */

static void
s_display_param_settings(ccl_log_type log, ar_node *node,
			 void (*idlog)(ccl_log_type, const ar_identifier*))
{
  ar_idtable *settings = ar_node_get_subnode_parameter_settings(node);

  if( ar_idtable_get_size(settings) > 0 )
    {
      ar_identifier_iterator *sit = ar_idtable_get_keys(settings);

      ccl_log(log,FIELD_TAB "param_set\n");
      ccl_log(log,ELEMENT_TAB);
      
      while( ccl_iterator_has_more_elements(sit) )
	{
	  ar_identifier *subname = ccl_iterator_next_element(sit);
	  ar_idtable *table = ar_idtable_get(settings,subname);
	  ar_identifier_iterator *pit = ar_idtable_get_keys(table);
	  
	  while( ccl_iterator_has_more_elements(pit) )
	    {
	      ar_identifier *param = ccl_iterator_next_element(pit);
	      ar_expr *value = ar_idtable_get(table,param);
	      
	      idlog(log,subname);
	      ccl_log(log,".");
	      idlog(log,param);
	      ccl_log(log," := ");
	      ar_expr_log_gen(log,value,idlog);
	      
	      if( ccl_iterator_has_more_elements(sit) || 
		  ccl_iterator_has_more_elements(pit) )
		ccl_log(log,", ");
	      ar_identifier_del_reference(param);
	      ar_expr_del_reference(value);
	    }

	  ccl_iterator_delete(pit);
	  ar_idtable_del_reference(table);
	  ar_identifier_del_reference(subname);
	}
      ccl_iterator_delete(sit);
      ccl_log(log,";\n");
    }
  ar_idtable_del_reference(settings);
}

			/* --------------- */

void
ar_node_display_subnode_type (ccl_log_type log, ar_type *type)
{
  ar_type_kind k = ar_type_get_kind (type);

  if (k == AR_TYPE_BANG)
    {
      ar_node *n = ar_type_bang_get_node (type);
      ar_identifier *name = ar_node_get_name (n);
      ar_identifier_log (log, name);
      ar_identifier_del_reference (name);
      ar_node_del_reference (n);
    }
  else
    {
      ar_type *at = ar_type_array_get_base_type (type);
      ar_node_display_subnode_type (log, at);
      ccl_log (log, "[%d]", ar_type_get_width (type));
      ar_type_del_reference (at);      
    }
}

static void 
s_hierarchy_identifiers_rec (ar_node *n, ar_identifier *prefix, ccl_list *R) 
{
  ar_context_slot_iterator *si;
  
  ccl_list_add (R, ar_identifier_add_reference (prefix));

  if (ar_node_is_leaf (n))
    return;
      
  si = ar_node_get_slots_for_subnodes (n);
  while (ccl_iterator_has_more_elements (si))
    {
      ar_context_slot *sl = ccl_iterator_next_element (si);
      ar_identifier *sname = ar_context_slot_get_name (sl);
      ar_node *sub = ar_node_get_subnode_model (n, sname);
      ar_identifier *np = ar_identifier_add_prefix (sname, prefix);
      s_hierarchy_identifiers_rec (sub, np, R);

      ar_identifier_del_reference (np);
      ar_node_del_reference (sub);
      ar_identifier_del_reference (sname);
      ar_context_slot_del_reference (sl);
    }
  ccl_iterator_delete (si);
}

ccl_list *
ar_node_get_hierarchy_identifiers (ar_node *n)
{
  ccl_list *result = ccl_list_create ();
  ar_identifier *nname = ar_node_get_name (n);
  ar_node *actual_node = ar_model_get_node (nname);
  
  s_hierarchy_identifiers_rec (actual_node, AR_EMPTY_ID, result);
  ar_identifier_del_reference (nname);
  ar_node_del_reference (actual_node);
  
  return result;
}

			/* --------------- */

static void
s_display_subnodes (ccl_log_type log, ar_node *node,
		   void (*idlog) (ccl_log_type, const ar_identifier*))
{
  if (! ar_node_is_leaf (node))
    {
      ar_context_slot_iterator *i = ar_node_get_slots_for_subnode_fields (node);

      ccl_log (log, FIELD_TAB "sub\n");

      while (ccl_iterator_has_more_elements (i))
	{
	  ar_context_slot *slot = ccl_iterator_next_element (i);
	  ar_identifier *id = ar_context_slot_get_name (slot);
	  ar_type *type = ar_context_slot_get_type (slot);

	  ccl_log (log, ELEMENT_TAB);
	  idlog (log, id);
	  ccl_log (log, " : ");
	  ar_node_display_subnode_type (log, type);
	  ccl_log (log, ";\n");

	  ar_identifier_del_reference (id);
	  ar_type_del_reference (type);
	  ar_context_slot_del_reference (slot);
	}
      ccl_iterator_delete (i);
    }
}

			/* --------------- */

static void
s_display_transitions(ccl_log_type log, ar_node *node,
		     void (*idlog)(ccl_log_type, const ar_identifier*))
{
  ccl_list *trans = ar_node_get_transitions(node);

  if( ccl_list_get_size(trans) == 0 ||
      (ccl_list_get_size(trans) == 1 &&
       ar_trans_is_epsilon_loop (CAR (FIRST (trans)))))
    ccl_log(log,FIELD_TAB "// no transition is explicitly defined.\n");
  else
    {
      ccl_pair *p;
	
      ccl_log(log,FIELD_TAB "trans\n");
      for(p = FIRST(trans); p; p = CDR(p))
	{
	  if (! ar_trans_is_epsilon_loop (CAR (p)))
	    {
	      ccl_log(log,ELEMENT_TAB);
	      ar_trans_log_gen(log,(ar_trans *)CAR(p),idlog);
	      ccl_log(log,";\n");
	    }
	}
    }
}

			/* --------------- */

static void
s_display_assertions(ccl_log_type log, ar_node *node,
		     void (*idlog)(ccl_log_type, const ar_identifier*))
{
  ccl_list *assertions = ar_node_get_assertions(node);
  
  if( ccl_list_get_size(assertions) == 0 )
    ccl_log(log,FIELD_TAB "// assertion is (implicitly) 'true'.\n");
  else
    {
      ccl_pair *p;
	
      ccl_log(log,FIELD_TAB "assert\n");
      for(p = FIRST(assertions); p; p = CDR(p))
	{
	  ccl_log(log,ELEMENT_TAB);
	  ar_expr_log_gen(log,(ar_expr *)CAR(p),idlog);
	  ccl_log(log,";\n");
	}
    }
}

			/* --------------- */

static void
s_display_broadcasts(ccl_log_type log, ar_node *node,
		     void (*idlog)(ccl_log_type, const ar_identifier*))
{
  if( ar_node_get_nb_subnode_fields(node) > 0 )
    {
      ccl_list *vectors = ar_node_get_broadcasts(node);
      
      if( ccl_list_get_size(vectors) == 0 )
	ccl_log(log,FIELD_TAB "// no synchronization vector is specified.\n");
      else
	{
	  ccl_pair *p;
	  
	  ccl_log(log,FIELD_TAB "sync\n");
	  for(p = FIRST(vectors); p; p = CDR(p))
	    {
	      ccl_log(log,ELEMENT_TAB);
	      ar_broadcast_log(log,(ar_broadcast *)CAR(p));
	      ccl_log(log,";\n");
	    }
	}
    }
  else
    {
      ccl_assert( ccl_list_get_size(ar_node_get_broadcasts(node)) == 0 );
    }
}

			/* --------------- */

static void
s_display_initial_state (ccl_log_type log, ar_node *node,
			 void (*idlog)(ccl_log_type, const ar_identifier*))
{
  ccl_list *init = ar_node_get_initialized_slots (node);
    
  if( ccl_list_get_size (init) == 0 )
    ccl_log(log,FIELD_TAB "// no initial assignment is specified.\n");
  else
    {
      ccl_pair *p;
      
      ccl_log(log,FIELD_TAB "init\n");
      ccl_log(log,ELEMENT_TAB);
      for(p = FIRST(init); p; p = CDR(p))
	{
	  ar_context_slot *slot = (ar_context_slot *)CAR(p);
	  const ar_expr *value = ar_node_get_initial_value_cst (node, slot);
	  ar_identifier *name = ar_context_slot_get_name(slot);
	  
	  idlog(log,name);
	  ar_identifier_del_reference(name);
	  ccl_log(log," := ");
	  ar_expr_log_gen(log,value,idlog);
	  
	  if( CDR(p) != NULL )
	    ccl_log(log,", ");
	}
      ccl_log(log,";\n");
    }

  init = ar_node_get_initial_constraints (node);

  if (ccl_list_get_size (init) == 0)
    ccl_log (log, FIELD_TAB "// no initial constraint is specified.\n");
  else
    {
      ccl_pair *p;
      
      ccl_log (log, FIELD_TAB "init\n");
      ccl_log (log, ELEMENT_TAB);
      for(p = FIRST (init); p; p = CDR (p))
	{
	  ar_expr_log_gen (log, CAR (p), idlog);
	  
	  if (CDR (p) != NULL)
	    ccl_log (log,", ");
	}
      ccl_log (log,";\n");
    }
}

			/* --------------- */

static void
s_extern_keyword (ccl_log_type log, int *pfirst)
{
  if (*pfirst)
    {
      ccl_log (log, "  extern \n");
      *pfirst = 0;
    }
}

static void 
s_display_law_parameters (ccl_log_type log, ar_node *node,
			  void (*idlog)(ccl_log_type, const ar_identifier*),
			  int *pfirst)
{
  ccl_list *law_params = ar_node_get_ordered_ids_of_law_parameters (node);
  
  if (! ccl_list_is_empty (law_params))
    {
      s_extern_keyword (log, pfirst);
      while (! ccl_list_is_empty (law_params))
	{
	  ar_identifier *id = ccl_list_take_first (law_params);
	  sas_law_param *lp = ar_node_get_law_parameter (node, id);
	  ccl_log (log, "    parameter ");
	  idlog (log, id);
	  ccl_log (log, " = ");
	  sas_law_param_log_gen (log, lp, idlog);
	  ccl_log (log, "; \n");
	  sas_law_param_del_reference (lp);
	}
    }
  ccl_list_delete (law_params);
}

static void
s_display_laws (ccl_log_type log, ar_node *node,
		void (*idlog)(ccl_log_type, const ar_identifier*),
		int *pfirst)
{
  ccl_pair *p;
  const ccl_list *laws = ar_node_get_ids_of_laws (node);
    
  if (ccl_list_is_empty (laws))
    return;

  s_extern_keyword (log, pfirst);
  for (p = FIRST (laws); p; p = CDR (p))
    {
      sas_law *l = ar_node_get_law (node, CAR (p));
      ccl_log (log, "    law ");
      idlog (log, CAR (p));
      ccl_log (log, " = ");
      sas_law_log_gen (log, l, idlog);
      ccl_log (log, "; \n");
      sas_law_del_reference (l);
    }
}

static void
s_display_observers (ccl_log_type log, ar_node *node,
		     void (*idlog)(ccl_log_type, const ar_identifier*),
		     int *pfirst)
{
  ccl_pair *p;
  const ccl_list *observers = ar_node_get_ids_of_observers (node);
    
  if (ccl_list_is_empty (observers))
    return;

  s_extern_keyword (log, pfirst);
  for (p = FIRST (observers); p; p = CDR (p))    
    {
      ar_expr *obs = ar_node_get_observer (node, CAR (p));
      if (ar_expr_get_type_kind (obs) == AR_TYPE_BOOLEANS)
	ccl_log (log, "    predicate ");
      else
	ccl_log (log, "    property ");
      idlog (log, CAR (p));
      ccl_log (log, " = <term (");
      ar_expr_log_gen (log, obs, idlog);
      ccl_log (log, ")>; \n");
      ar_expr_del_reference (obs);
    }  
}

static void
s_display_preemptibles (ccl_log_type log, ar_node *node,
			void (*idlog)(ccl_log_type, const ar_identifier*),
			int *pfirst)
{
  ccl_pointer_iterator *pi = ar_node_get_preemptibles (node);
    
  if (ccl_iterator_has_more_elements (pi))
    {
      s_extern_keyword (log, pfirst);

      while (ccl_iterator_has_more_elements (pi))
	{
	  ar_identifier *id = ccl_iterator_next_element (pi);
	  ccl_log (log, "    preemptible <event ");
	  idlog (log, id);
	  ccl_log (log, ">; \n");
	}
    }
  ccl_iterator_delete (pi);
}

struct disp_buckets_data
{
  ccl_log_type log;
  int *pfirst;
  void (*idlog)(ccl_log_type, const ar_identifier*);
};
  
static void
s_display_bucket (ar_identifier *bid, ccl_list *events, ccl_list *probas,
		  void *data)
{
  ccl_pair *pe = FIRST (events);
  ccl_pair *pp = FIRST (probas);
  struct disp_buckets_data *d = data;

  s_extern_keyword (d->log, d->pfirst);
  
  ccl_log (d->log, "    bucket ");
  d->idlog (d->log, bid);
  ccl_log (d->log, " = { ");

  while (pe != NULL)
    {
      int last = (pp == NULL);
      
      d->idlog (d->log, CAR (pe));
      if (! last)
	{
	  ccl_log (d->log, ", ");
	  sas_law_param_log_gen (d->log, CAR (pp), d->idlog);
	  ccl_log (d->log, ", \n             ");
	  pp = CDR (pp);
	}
      pe = CDR (pe);
      ccl_assert (! last || pe == NULL);
    }
  ccl_log (d->log, "};\n");
}

static void
s_display_buckets (ccl_log_type log, ar_node *node,
		   void (*idlog)(ccl_log_type, const ar_identifier*),
		   int *pfirst)
{
  struct disp_buckets_data data;
  data.log = log;
  data.idlog = idlog;
  data.pfirst = pfirst;
  
  ar_node_map_buckets (node, s_display_bucket, &data);
}

static void
s_display_priorities (ccl_log_type log, ar_node *node,
		      void (*idlog)(ccl_log_type, const ar_identifier*),
		      int *pfirst)
{
  ar_identifier_iterator *ii = ar_node_get_events_with_priorities (node);

  if (ccl_iterator_has_more_elements (ii))
    {
      s_extern_keyword (log, pfirst);
  
      CCL_DEBUG_START_TIMED_BLOCK (("translate priorities"));  
      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *id = ccl_iterator_next_element (ii);
	  int pl = ar_node_get_priority (node, id);
	  ccl_log (log, "    priority ");
	  idlog (log, id);
	  ccl_log (log, " = %d;\n", pl);
	  ar_identifier_del_reference (id);
	}
      CCL_DEBUG_END_TIMED_BLOCK ();
    }
  ccl_iterator_delete (ii);
}

static void
s_node_log(ccl_log_type log, ar_node *node, int with_epsilon, 
	   ccl_hash *type_cache)
{
  int first_extern = 1;
  void (*idlog)(ccl_log_type, const ar_identifier*);

  if( node->node_template != NULL )
    {
      ar_tree_log_altarica (log,node->node_template, AR_NO_ID_TRANSLATION);
      return;
    }

  if( ar_node_is_leaf(node) )
    idlog = ar_identifier_log_global_quote;
  else
    idlog = ar_identifier_log;

  s_display_node_header(log,node,idlog);
  s_display_parameters(log,node,idlog,type_cache);
  s_display_variables(log,node,idlog,type_cache);
  s_display_events (log, node, ar_identifier_log);
  s_display_param_settings(log,node,idlog);
  s_display_subnodes(log,node,idlog);
  s_display_transitions(log,node,idlog);
  s_display_assertions(log,node,idlog);
  s_display_broadcasts(log,node,idlog);
  s_display_initial_state(log,node,idlog);
  
  s_display_law_parameters (log, node, idlog, &first_extern);
  s_display_laws (log, node, idlog, &first_extern);
  s_display_observers (log, node, idlog, &first_extern);
  s_display_preemptibles (log, node, idlog, &first_extern);
  s_display_buckets (log, node, idlog, &first_extern);
  s_display_priorities (log, node, idlog, &first_extern);

  ccl_log(log,"edon");
}

			/* --------------- */

void
ar_node_log(ccl_log_type log, ar_node *node, int with_epsilon)
{
  s_node_log(log,node,with_epsilon,NULL);
}

			/* --------------- */

altarica_tree *
ar_node_get_template(ar_node *node)
{
  ccl_pre( node != NULL );

  return node->node_template;
}

			/* --------------- */

void
ar_node_add_local_parameter(ar_node *node, ar_identifier *id, ar_type *type,
			    ar_expr *value)
{
  node_parameter *p;

  ccl_pre( node != NULL ); ccl_pre( id != NULL ); ccl_pre( type != NULL ); 
  ccl_pre( ar_context_get_slot(node->ctx,id) == NULL );
  ccl_pre( node->ref == NULL );

  p = ccl_new(node_parameter);
  p->next = node->parameters;
  node->parameters = p;
  p->slot = NULL;
  s_allocate_slots(node,id,type,
		   AR_SLOT_FLAG_PARAMETER|AR_SLOT_FLAG_DFLT_PARAMETER_VIS,
		   NULL,value,&(p->slot));

  ar_type_struct_add_field(node->vars_type,id,type);
  ar_type_struct_add_field(node->conf_type,id,type);
}

			/* --------------- */


void
ar_node_set_subnode_parameter(ar_node *node, ar_identifier *subnode,
			      ar_identifier *pname, ar_expr *value)
{
  ar_idtable *p_setting;

  ccl_pre( node != NULL ); ccl_pre( pname != NULL ); ccl_pre( value != NULL );
  ccl_pre( subnode != NULL );
  ccl_pre( node->ref == NULL );

  if( (p_setting = ar_idtable_get(node->param_setting,subnode)) == NULL )
    {
      p_setting = 
	ar_idtable_create(1, (ccl_duplicate_func *)ar_expr_add_reference,
			  (ccl_delete_proc *)ar_expr_del_reference);
      ar_idtable_put(node->param_setting,subnode,p_setting);
    }

  ar_idtable_put(p_setting,pname,value);
  ar_idtable_del_reference(p_setting);
}

			/* --------------- */

int
ar_node_has_subnode_parameter_setting(ar_node *node, ar_identifier *subnode,
				      ar_identifier *pname)
{
  int result = 0;
  ar_idtable *table = ar_node_get_subnode_parameter_setting(node,subnode);

  if( table != NULL )
    {
      result = ar_idtable_has(table,pname);
      ar_idtable_del_reference(table);
    }

  return result;
}

			/* --------------- */

ar_idtable *
ar_node_get_subnode_parameter_setting(ar_node *node, ar_identifier *subnode)
{
  ccl_pre( node != NULL ); ccl_pre( subnode != NULL );

  if( node->ref != NULL )
    return ar_node_get_subnode_parameter_setting(node->ref,subnode);
  return ar_idtable_get(node->param_setting,subnode);
}

			/* --------------- */

ar_idtable *
ar_node_get_subnode_parameter_settings(ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_subnode_parameter_settings(node->ref);

  return ar_idtable_add_reference(node->param_setting);
}

			/* --------------- */

typedef struct param_slot_iterator_st {
  ar_context_slot_iterator super;
  node_parameter *p;
} param_slot_iterator;

			/* --------------- */

static int 
s_param_slot_iterator_has_more_elements(const ar_context_slot_iterator *i)
{
  ccl_pre( i != NULL ); 

  return ((param_slot_iterator *)i)->p != NULL;
}

			/* --------------- */

static ar_context_slot *
s_param_slot_iterator_next_element(ar_context_slot_iterator *i)
{
  ar_context_slot *result;
  param_slot_iterator *psi = (param_slot_iterator *)i;

  ccl_pre( psi != NULL ); ccl_pre( psi->p != NULL );

 result = ar_context_slot_add_reference(psi->p->slot);
 psi->p = psi->p->next;

 return result;
}

			/* --------------- */

static void
s_param_slot_iterator_delete_iterator(ar_context_slot_iterator *i)
{
  ccl_pre( i != NULL );
  ccl_delete(i);
}

			/* --------------- */

static ar_context_slot_iterator PARAM_SLOT_ITERATOR = {
  s_param_slot_iterator_has_more_elements,
  s_param_slot_iterator_next_element,
  s_param_slot_iterator_delete_iterator
};

			/* --------------- */

ar_context_slot_iterator *
ar_node_get_slots_for_parameters (const ar_node *node)
{
  ar_context_slot_iterator *result;

  ccl_pre( node != NULL );

  if( node->ref != NULL )
    result = ar_node_get_slots_for_parameters(node->ref);
  else
    {
      param_slot_iterator *psi = ccl_new(param_slot_iterator);

      psi->super = PARAM_SLOT_ITERATOR;
      psi->p = node->parameters;
      result = (ar_context_slot_iterator *)psi;
    }

  return result;
}

			/* --------------- */

typedef struct name_iterator_st
{
  ar_identifier_iterator super;
  ar_context_slot_iterator *sli;
} name_iterator;

static int 
s_name_iterator_has_more_elements (const ar_identifier_iterator *i)
{
  ccl_pre (i != NULL); 

  return ccl_iterator_has_more_elements (((name_iterator *) i)->sli);
}

			/* --------------- */

static ar_identifier *
s_name_iterator_next_element (ar_identifier_iterator *i)
{
  ar_identifier *result;
  name_iterator *subsi = (name_iterator *) i;
  ar_context_slot *sl;

  ccl_pre (subsi != NULL); 
  ccl_pre (subsi->sli != NULL);

  sl = ccl_iterator_next_element (subsi->sli);
  result = ar_context_slot_get_name (sl);
  ar_context_slot_del_reference (sl);

 return result;
}

			/* --------------- */

static void
s_name_iterator_delete_iterator (ar_identifier_iterator *i)
{
  ccl_pre (i != NULL);
  ccl_iterator_delete (((name_iterator *) i)->sli);
  ccl_delete (i);
}


static ar_identifier_iterator NAME_ITERATOR = {
  s_name_iterator_has_more_elements,
  s_name_iterator_next_element,
  s_name_iterator_delete_iterator
};

			/* --------------- */

static ar_identifier_iterator *
s_name_iterator (const ar_node *node, 
		 ar_context_slot_iterator *(*get)(const ar_node *))
{
  name_iterator *subsi = ccl_new (name_iterator);

  subsi->super = NAME_ITERATOR;
  subsi->sli = get (node);

  return (ar_identifier_iterator *) subsi;
}

			/* --------------- */

ar_identifier_iterator *
ar_node_get_names_of_parameters (ar_node *node)
{
  return s_name_iterator (node, ar_node_get_slots_for_parameters);
}

			/* --------------- */


ar_type *
ar_node_get_type_of_parameter (ar_node *node, ar_identifier *id)
{
  ar_context *ctx = ar_node_get_context (node);
  ar_context_slot *sl = ar_context_get_slot (ctx, id);
  ar_type *result = ar_context_slot_get_type (sl);
  ar_context_slot_del_reference (sl);
  ar_context_del_reference (ctx);

  return result;
}

			/* --------------- */

ar_expr *
ar_node_get_value_of_parameter (ar_node *node, ar_identifier *id)
{
  ar_context *ctx = ar_node_get_context (node);
  ar_context_slot *sl = ar_context_get_slot (ctx, id);
  ar_expr *result = ar_context_slot_get_value (sl);
  ar_context_slot_del_reference (sl);
  ar_context_del_reference (ctx);

  return result;
}

			/* --------------- */

void
ar_node_add_attribute(ar_node *node, ar_identifier *id)
{
  ccl_pre( node != NULL ); ccl_pre( node->ref == NULL );  

  if( ! ccl_list_has(node->attributes,id) )
    ccl_list_add(node->attributes,ar_identifier_add_reference(id));
}

			/* --------------- */

int
ar_node_has_attribute(ar_node *node, ar_identifier *id)
{
  ccl_pre( node != NULL ); 

  if( node->ref != NULL )
    return ar_node_has_attribute(node->ref,id);
  return ccl_list_has(node->attributes,id);
}


			/* --------------- */

ccl_list *
ar_node_get_attributes (const ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_attributes(node->ref);
  return node->attributes;
}

			/* --------------- */

static void
s_allocate_slots(ar_node *node, ar_identifier *id, ar_type *type, 
		 uint32_t flags, ccl_list *attr, ar_expr *value,
		 ar_context_slot **presult)
{
  ar_context_slot *result;

  if (ar_context_has_slot(node->ctx,id))
    return;

  result = 
    ar_context_slot_create_filled(-1,id,type,flags,attr,value);
  ar_context_add_slot(node->ctx,id,result);

  if( presult != NULL )
    *presult = ar_context_slot_add_reference(result);

  if( ar_type_is_scalar(type) )
    {
      ar_context_slot_del_reference(result);
      return;
    }

  if( ar_type_get_kind(type) == AR_TYPE_ARRAY )
    {
      int i, width = ar_type_get_width(type);
      ar_type *base_type = ar_type_array_get_base_type(type);
      char *pos_str = NULL;
      size_t pos_str_size = 0;
      ar_expr *subvalue = NULL;

      for(i = 0; i < width; i++)
	{
	  ar_identifier *slot_id;
	  
	  ccl_string_format(&pos_str,&pos_str_size,"[%d]",i);
	  slot_id = ar_identifier_rename_with_suffix(id,pos_str);

	  if( value != NULL )
	    {
	      ar_constant *pos = ar_constant_crt_integer(i);
	      ar_expr *epos = ar_expr_crt_constant(pos);
	      subvalue = ar_expr_crt_binary(AR_EXPR_ARRAY_MEMBER,value,epos);
	      ar_constant_del_reference(pos);
	      ar_expr_del_reference(epos);
	    }
	  s_allocate_slots(node,slot_id,base_type,flags,attr,subvalue,NULL);
	  ar_identifier_del_reference(slot_id);
	  ccl_zdelete(ar_expr_del_reference,subvalue);
	}
      ccl_string_delete(pos_str);
      ar_type_del_reference(base_type);
    }
  else if( ar_type_get_kind(type) == AR_TYPE_STRUCTURE )
    {
      ar_expr *subvalue = NULL;
      ar_identifier_iterator *fields;

      fields = ar_type_struct_get_fields(type);

      while( ccl_iterator_has_more_elements(fields) )
	{
	  ar_identifier *field_name = ccl_iterator_next_element(fields);
	  ar_identifier *field_id = ar_identifier_add_prefix(field_name,id);
	  ar_type *field_type = ar_type_struct_get_field(type,field_name);
	  
	  if( value != NULL )
	    subvalue = ar_expr_crt_struct_member(value,field_name);

	  s_allocate_slots(node,field_id,field_type,flags,attr,subvalue,NULL);
	  ar_type_del_reference(field_type);
	  ar_identifier_del_reference(field_id);
	  ar_identifier_del_reference(field_name);
	  ccl_zdelete(ar_expr_del_reference,subvalue);
	}
      ccl_iterator_delete(fields);
    }
  else 
    {
      ar_identifier_iterator *fields;
      ar_node *model = ar_type_bang_get_node(type);      

      ccl_assert( ar_type_get_kind(type) == AR_TYPE_BANG );
      ccl_assert( value == NULL );

      if( ar_identifier_get_path_length(id) == 1 )
	{
	  node_subnode *ns = ccl_new(node_subnode);
	  ns->next = node->subnodes;
	  ns->model = ar_node_add_reference(model);
	  ns->slot = ar_context_slot_add_reference(result);
	  node->subnodes = ns;
	}

      fields = ar_type_struct_get_fields(type);
      while( ccl_iterator_has_more_elements(fields) )
	{
	  ar_identifier *field_name = ccl_iterator_next_element(fields);
	  ar_identifier *field_id = ar_identifier_add_prefix(field_name,id);
	  ar_type *field_type = ar_type_struct_get_field(type,field_name);
	  ar_context_slot *slot = ar_context_get_slot(model->ctx,field_name);
	  uint32_t flags = ar_context_slot_get_flags(slot);
	  ccl_list *attr = ar_context_slot_get_attributes(slot);
	  ar_expr *value = ar_context_slot_get_value(slot);

	  ccl_assert( slot != NULL );
	  
	  s_allocate_slots(node,field_id,field_type,flags,attr,value,NULL);
	  ar_type_del_reference(field_type);
	  ar_identifier_del_reference(field_id);
	  ar_identifier_del_reference(field_name);
	  if( attr != NULL )
	    ccl_list_clear_and_delete(attr,(ccl_delete_proc *)
				      ar_identifier_del_reference);
	  ccl_zdelete(ar_expr_del_reference,value);
	  ar_context_slot_del_reference(slot);
	}
      ccl_iterator_delete(fields);

      type = ar_node_get_event_type (model);
      fields = ar_type_struct_get_fields (type);
      while (ccl_iterator_has_more_elements (fields))
	{
	  ar_identifier *field_name = ccl_iterator_next_element (fields);
	  ar_identifier *field_id = ar_identifier_add_prefix (field_name, id);
	  ar_type *field_type = ar_type_struct_get_field (type, field_name);
	  ar_context_slot *slot = ar_context_get_slot (model->ctx, field_name);
	  uint32_t flags = ar_context_slot_get_flags (slot);
	  ccl_list *attr = ar_context_slot_get_attributes (slot);
	  ar_expr *value = ar_context_slot_get_value (slot);

	  ccl_assert( slot != NULL );
	  
	  s_allocate_slots(node,field_id,field_type,flags,attr,value,NULL);
	  ar_type_del_reference(field_type);
	  ar_identifier_del_reference(field_id);
	  ar_identifier_del_reference(field_name);
	  if( attr != NULL )
	    ccl_list_clear_and_delete(attr,(ccl_delete_proc *)
				      ar_identifier_del_reference);
	  ccl_zdelete(ar_expr_del_reference,value);
	  ar_context_slot_del_reference(slot);
	}
      ccl_iterator_delete(fields);
      
      ar_type_del_reference (type);
      ar_node_del_reference(model);
    }

  ar_context_slot_del_reference(result);
}

			/* --------------- */

void
ar_node_add_variable(ar_node *node, ar_identifier *id, ar_type *type, 
		     uint32_t flags, ccl_list *attr)
{
  ar_context_slot *sl = NULL;
  
  ccl_pre( node != NULL ); ccl_pre( id != NULL ); ccl_pre( type != NULL ); 
  ccl_pre( ar_context_get_slot(node->ctx,id) == NULL );
  ccl_pre( node->ref == NULL );

  s_allocate_slots (node, id, type, flags, attr, NULL, &sl);
#if 0
  node_variable **pv;
  node_variable *var = ccl_new(node_variable);


  for(pv = &(node->variables); *pv; pv = &((*pv)->next))
    {
      if( ar_context_slot_get_flags((*pv)->slot) < flags )
	break;
    }

  var->next = *pv;
  *pv = var;

  var->slot = sl;
#else
  ccl_list_add (node->variables, sl);
#endif
  
  ar_type_struct_add_field (node->vars_type, id, type);
  ar_type_struct_add_field (node->conf_type, id, type);
}

struct stable_sort_st
{
  int index;
  ar_context_slot *sl;
};

static int
s_compare_slots (const void *p1, const void *p2)
{
  const ar_context_slot *sl1 = p1;
  const ar_context_slot *sl2 = p2;  
  int f1 = ar_context_slot_get_flags (sl1);
  int f2 = ar_context_slot_get_flags (sl2);

  return f2 - f1;
}

static int
s_compare_slot_flags (const void *p1, const void *p2)
{
  const struct stable_sort_st *psl1 = p1;
  const struct stable_sort_st *psl2 = p2;  
  int f1 = ar_context_slot_get_flags (psl1->sl);
  int f2 = ar_context_slot_get_flags (psl2->sl);

  if (f1 == f2)
    return psl1->index - psl2->index;
  return f2 - f1;
}

ar_context_slot_iterator *
ar_node_get_slots_for_variables (const ar_node *node)
{
  ar_context_slot_iterator *result;

  if( node->ref != NULL )
    result = ar_node_get_slots_for_variables(node->ref);
  else
    {
      if (! ccl_list_is_empty (node->variables))
	{
	  int sorted = 1;
	  ccl_pair *p;
	  for (p = FIRST (node->variables); sorted && CDR (p) != NULL;
	       p = CDR (p))
	    {
	      sorted = (s_compare_slots (CAR (p), CADR (p)) <= 0);
	    }
	  if (! sorted)
	    {
	      int i;
	      ccl_pair *p;
	      int nb_slots = ccl_list_get_size (node->variables);
	      struct stable_sort_st *slots =
		ccl_new_array (struct stable_sort_st, nb_slots);	      
	      for (i = 0, p = FIRST (node->variables); p; p = CDR (p), i++)
		{
		  slots[i].index = i;
		  slots[i].sl = CAR (p);
		}
	      qsort (slots, nb_slots, sizeof (*slots), s_compare_slot_flags);
	      for (i = 0, p = FIRST (node->variables); p; p = CDR (p), i++)
		CAR (p) = slots[i].sl;
	      ccl_delete (slots);
	    }
#if CCL_ENABLE_ASSERTIONS	  
	  sorted = 1;
	  for (p = FIRST (node->variables); sorted && CDR (p) != NULL;
	       p = CDR (p))
	    {
	      sorted = (s_compare_slots (CAR (p), CADR (p)) <= 0);
	    }
	  ccl_assert (sorted);
#endif
	}
      result = (ar_context_slot_iterator *)
	ccl_list_get_iterator (node->variables,
			       (ccl_duplicate_func *) ar_context_add_reference,
			       (ccl_delete_proc *) ar_context_del_reference);
  }

  return result;
}

			/* --------------- */

int
ar_node_get_nb_variables (const ar_node *node)
{
  int result;

  ccl_pre( node != NULL );
  
  result = ccl_list_get_size (node->variables);
  
  return result;
}

			/* --------------- */

ar_context_slot *
ar_node_get_variable_slot (ar_node *node, ar_identifier *id)
{
  ar_context *ctx = ar_node_get_context (node);
  ar_context_slot *result;

  ccl_pre( node != NULL ); ccl_pre( id != NULL );

  result = ar_context_get_slot(ctx, id);
  ccl_assert( result == NULL || (ar_context_slot_get_flags(result) & 
	       (AR_SLOT_FLAG_FLOW_VAR|AR_SLOT_FLAG_STATE_VAR)) != 0 );
  ar_context_del_reference (ctx);

  return result;
}

			/* --------------- */
ar_expr *
ar_node_get_variable (ar_node *node, ar_identifier *id)
{
  ar_context_slot *sl = ar_node_get_variable_slot (node, id);
  ar_expr *result = ar_expr_crt_variable (sl);
  ar_context_slot_del_reference (sl);

  return result;
}
			/* --------------- */

int
ar_node_has_flow_variable(ar_node *node, ar_identifier *id)
{
  ar_context_slot *sl = ar_node_get_variable_slot (node, id);
  int result = sl != NULL 
    &&         (ar_context_slot_get_flags(sl)&AR_SLOT_FLAG_FLOW_VAR);
  ccl_zdelete(ar_context_slot_del_reference,sl);

  return result;
}

			/* --------------- */

int
ar_node_has_state_variable(ar_node *node, ar_identifier *id)
{
  ar_context_slot *sl = ar_node_get_variable_slot (node, id);
  int result = sl != NULL 
    &&         (ar_context_slot_get_flags(sl)&AR_SLOT_FLAG_STATE_VAR);
  ccl_zdelete(ar_context_slot_del_reference,sl);

  return result;
}

			/* --------------- */

ar_identifier_iterator *
ar_node_get_names_of_variables (const ar_node *node)
{
  return s_name_iterator (node, ar_node_get_slots_for_variables);
}

			/* --------------- */

uint32_t 
ar_node_get_flags_of_variable (ar_node *node, ar_identifier *id)
{

  ar_context_slot *sl = ar_node_get_variable_slot (node, id);
  uint32_t result = ar_context_slot_get_flags (sl);
  ar_context_slot_del_reference (sl);

  return result;
}

			/* --------------- */

ar_type *
ar_node_get_type_of_variable (ar_node *node, ar_identifier *id)
{
  ar_context_slot *sl = ar_node_get_variable_slot (node, id);
  ar_type *result = ar_context_slot_get_type (sl);
  ar_context_slot_del_reference (sl);

  return result;
}

			/* --------------- */

ccl_list * 
ar_node_get_attributes_of_variable (ar_node *node, ar_identifier *id)
{
  ar_context_slot *sl = ar_node_get_variable_slot (node, id);
  ccl_list *result = ar_context_slot_get_attributes (sl);
  ar_context_slot_del_reference (sl);

  return result;
}

			/* --------------- */

static ar_type *
s_subnode_event_type (ar_type *ntype)
{
  ar_type *result;

  if (ar_type_get_kind (ntype) == AR_TYPE_BANG)
    {
      ccl_assert (ar_type_bang_get_kind (ntype) != AR_TYPE_BANG_EVENTS);
      {
	ar_node *n = ar_type_bang_get_node (ntype);
	result = ar_node_get_event_type (n);
	ar_node_del_reference (n);
      }
    }
  else
    {
      ccl_assert (ar_type_get_kind (ntype) == AR_TYPE_ARRAY);
      {
	ar_type *subtype = ar_type_array_get_base_type(ntype);
	int width = ar_type_get_width (ntype);
	ar_type *tmp = s_subnode_event_type (subtype);
	result = ar_type_crt_array (tmp, width);
	ar_type_del_reference (tmp);
	ar_type_del_reference (subtype);
      }
    }
  return result;
}

			/* --------------- */

void
ar_node_add_subnode (ar_node *node, ar_identifier *id, ar_node *model, 
		     ar_type *ntype)
{
  ar_type *etype;
  node_subnode *ns = ccl_new (node_subnode);

  ccl_pre (node != NULL && id != NULL && model != NULL); 
  ccl_pre (ar_context_get_slot(node->ctx,id) == NULL);
  ccl_pre (node->ref == NULL);

  ns->next = node->subfields;
  ns->model = ar_node_add_reference (model);
  ns->slot = NULL;
  s_allocate_slots (node, id, ntype, AR_SLOT_FLAG_NODE, NULL, NULL, 
		    &(ns->slot));
  node->subfields = ns;

  ar_type_struct_add_field (node->vars_type, id, ntype);
  ar_type_struct_add_field (node->conf_type, id, ntype);

  etype = s_subnode_event_type (ntype);
    
  ar_type_struct_add_field (node->event_type, id, etype);
  ar_type_del_reference (etype);
}

			/* --------------- */

int
ar_node_has_subnode (const ar_node *node, const ar_identifier *n)
{
  int result = 0;
  ar_context *ctx = ar_node_get_context(node);
  ar_context_slot *slot = ar_context_get_slot (ctx, n);
  ar_context_del_reference(ctx);

  if( slot != NULL )
    {
      result = (ar_context_slot_get_flags(slot)&AR_SLOT_FLAG_NODE) != 0;
      ar_context_slot_del_reference(slot);
    }

  return result;
}

			/* --------------- */

ar_node *
ar_node_get_subnode_model (const ar_node *node, const ar_identifier *n)
{
  ar_context *ctx = ar_node_get_context(node);
  ar_context_slot *slot = ar_context_get_slot(ctx,n);
  ar_node *result = NULL;

  ar_context_del_reference(ctx);

  if( slot !=  NULL )
    {
      ar_type *t = ar_context_slot_get_type(slot);
      if( ar_type_get_kind(t) == AR_TYPE_BANG )
	result = ar_type_bang_get_node(t);
      ar_type_del_reference(t);
      ar_context_slot_del_reference(slot);
    }

  return result;
}

			/* --------------- */

int
ar_node_get_nb_subnodes (const ar_node *node)
{
  int result = 0;

  ccl_pre( node != NULL );

  if( node->ref != NULL )
    result = ar_node_get_nb_subnodes(node->ref);
  else
    {
      node_subnode *sub;
      
      for(result = 0, sub = node->subnodes; sub ; sub = sub->next, result++)
	/* do nothing */;
    }

  return result;
}

			/* --------------- */

int
ar_node_get_nb_subnode_fields(ar_node *node)
{
  int result = 0;

  ccl_pre( node != NULL );

  if( node->ref != NULL )
    result = ar_node_get_nb_subnode_fields(node->ref);
  else
    {
      node_subnode *sub;
      
      for(result = 0, sub = node->subfields; sub ; sub = sub->next, result++)
	/* do nothing */;
    }

  return result;
}

			/* --------------- */

typedef struct subnode_slot_iterator_st {
  ar_context_slot_iterator super;
  node_subnode *sub;
} subnode_slot_iterator;

			/* --------------- */

static int 
s_subnode_slot_iterator_has_more_elements(const ar_context_slot_iterator *i)
{
  ccl_pre( i != NULL ); 

  return ((subnode_slot_iterator *)i)->sub != NULL;
}

			/* --------------- */

static ar_context_slot *
s_subnode_slot_iterator_next_element(ar_context_slot_iterator *i)
{
  ar_context_slot *result;
  subnode_slot_iterator *subsi = (subnode_slot_iterator *)i;

  ccl_pre( subsi != NULL ); ccl_pre( subsi->sub != NULL );

 result = ar_context_slot_add_reference(subsi->sub->slot);
 subsi->sub = subsi->sub->next;

 return result;
}

			/* --------------- */

static void
s_subnode_slot_iterator_delete_iterator(ar_context_slot_iterator *i)
{
  ccl_pre( i != NULL );
  ccl_delete(i);
}

			/* --------------- */

static ar_context_slot_iterator SUBNODE_SLOT_ITERATOR = {
  s_subnode_slot_iterator_has_more_elements,
  s_subnode_slot_iterator_next_element,
  s_subnode_slot_iterator_delete_iterator
};

			/* --------------- */

ar_context_slot_iterator *
ar_node_get_slots_for_subnodes (const ar_node *node)
{
  ar_context_slot_iterator *result;

  ccl_pre( node != NULL );
  if( node->ref != NULL )
    result = ar_node_get_slots_for_subnodes(node->ref);
  else
    {
      subnode_slot_iterator *subsi = ccl_new(subnode_slot_iterator);

      subsi->super = SUBNODE_SLOT_ITERATOR;
      subsi->sub = node->subnodes;
      result = (ar_context_slot_iterator *)subsi;
    }

  return result;
}

			/* --------------- */


ar_identifier_iterator *
ar_node_get_names_of_subnodes (const ar_node *node)
{
  return s_name_iterator (node, ar_node_get_slots_for_subnodes);
}

			/* --------------- */

ar_context_slot_iterator *
ar_node_get_slots_for_subnode_fields(ar_node *node)
{
  ar_context_slot_iterator *result;

  ccl_pre( node != NULL );
  if( node->ref != NULL )
    result = ar_node_get_slots_for_subnode_fields (node->ref);
  else
    {
      subnode_slot_iterator *subsi = ccl_new(subnode_slot_iterator);

      subsi->super = SUBNODE_SLOT_ITERATOR;
      subsi->sub = node->subfields;
      result = (ar_context_slot_iterator *)subsi;
    }

  return result;
}

			/* --------------- */

void
ar_node_add_event (ar_node *node, ar_event *ev)
{
  ccl_pre (node != NULL); 
  ccl_pre (ev != NULL); 
  ccl_pre (node->ref == NULL); 

  if (! ar_event_poset_has_event (node->poset, ev))
    {
      ar_identifier *event_name = ar_event_get_name (ev);
      ccl_list *evlist = ar_idtable_get (node->events, event_name);
      ar_event_poset_add_event (node->poset, ev);
      if (! ar_type_enum_has_value (node->this_event_type, event_name))
	ar_type_enum_add_value (node->this_event_type, event_name);

      if (evlist == NULL)
	{
	  evlist = ccl_list_create ();
	  ar_idtable_put (node->events, event_name, evlist);
	}
      ccl_list_add (evlist, ar_event_add_reference (ev));
      ar_identifier_del_reference (event_name);
    }
}

			/* --------------- */

int
ar_node_has_event(ar_node *node, ar_identifier *ev)
{
  ccl_pre( node != NULL ); ccl_pre( ev != NULL ); 

  if( node->ref != NULL )
    return ar_node_has_event(node->ref,ev);
  return ar_idtable_has(node->events,ev);
}

			/* --------------- */

ar_event *
ar_node_get_event(ar_node *node, ar_identifier *ev)
{
  ar_event *result = NULL;

  ccl_pre( node != NULL ); ccl_pre( ev != NULL ); 

  if( node->ref != NULL )
    result = ar_node_get_event(node->ref,ev);
  else
    {
      ccl_list *l = ar_idtable_get(node->events,ev);

      if( l != NULL )
	{
	  ccl_assert( ccl_list_get_size(l) == 1 );
	  result = ar_event_add_reference(CAR(FIRST(l)));
	}
    }

  return result;
}

			/* --------------- */

ar_event *
ar_node_get_event_from_event(ar_node *node, ar_event *ev)
{
  ccl_pre( node != NULL ); ccl_pre( ev != NULL );

  if( node->ref != NULL )
    return ar_node_get_event_from_event(node->ref,ev);
  return ar_event_poset_get_event(node->poset,ev);
}

			/* --------------- */

int
ar_node_get_number_of_events(ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_number_of_events(node->ref);
  return ar_event_poset_get_set_size(node->poset);
}

			/* --------------- */

ar_event_poset *
ar_node_get_event_poset(ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_event_poset(node->ref);
  return ar_event_poset_add_reference(node->poset);
}

			/* --------------- */

ar_event_iterator *
ar_node_get_events(ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_events(node->ref);
  return ar_event_poset_get_events(node->poset);
}

			/* --------------- */

ar_identifier_iterator *
ar_node_get_event_identifiers(ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_event_identifiers(node->ref);
  return ar_idtable_get_keys(node->events);
}

			/* --------------- */

ccl_list *
ar_node_get_events_for_identifier(ar_node *node, ar_identifier *ev)
{
  ccl_pre( node != NULL ); ccl_pre( ev != NULL );

  if( node->ref != NULL )
    return ar_node_get_events_for_identifier(node->ref,ev);

  return ar_idtable_get(node->events,ev);
}

/* 
 *                                                                  transitions
 */
void
ar_node_add_transition(ar_node *node, ar_trans *t)
{
  ccl_pre( node != NULL );

  ccl_list_add(node->transitions,ar_trans_add_reference(t));
}

			/* --------------- */

void
ar_node_remove_transition(ar_node *node, ar_trans *t)
{
  ccl_pre( node != NULL );

  ccl_list_remove(node->transitions,t);
  ar_trans_del_reference(t);
}

int
ar_node_has_non_trivial_epsilon (ar_node *node)
{
  ccl_pair *p;
  int result = 0;
  
  for (p = FIRST (node->transitions); p && !result; p = CDR (p))
    {
      ar_trans *t = CAR (p);
      
      if (ar_trans_has_epsilon_event (t))
	{
	  ar_event *e = ar_trans_get_event (t);
	  result =  ((! ar_event_is_epsilon_vector (e)) ||
		     ar_trans_get_assignments (t) != NULL);
	  ar_event_del_reference (e);
	  
	  if (! result)
	    {
	      ar_expr *g = ar_trans_get_guard (t);
	      result = ! ar_expr_is_true (g);
	      ar_expr_del_reference (g);
	    }
	}	  
    }
	  
  return result;
}


ccl_list *
ar_node_get_transitions(ar_node *node)
{
  ccl_pre( node != NULL );

  return node->transitions;
}

			/* --------------- */

int 
ar_node_get_number_of_transitions (ar_node *node)
{
  ccl_pre (node != NULL);

  return ccl_list_get_size (node->transitions);
}


/*
 *                                                                   assertions
 */
void
ar_node_add_assertion(ar_node *node, ar_expr *a)
{
  ccl_pre( node != NULL ); ccl_pre( a != NULL ); ccl_pre( node->ref == NULL );

  ccl_list_add(node->assertions,ar_expr_add_reference(a));
}

			/* --------------- */

ccl_list *
ar_node_get_assertions(ar_node *node)
{
  ccl_pre( node != NULL );

  if( node->ref != NULL )
    return ar_node_get_assertions(node->ref);
  return node->assertions;
}

/*                                                                      init */
static void
s_add_initial_state (ar_node *node, ar_context_slot *slot, ar_expr *value)
{
  value = ar_expr_add_reference (value);
  if (! ccl_hash_find (node->initial_state_map, slot))
    slot = ar_context_slot_add_reference (slot);
  ccl_hash_insert (node->initial_state_map, value);
  ccl_list_add (node->initialized_slots, slot);
}

			/* --------------- */

void
ar_node_add_initial_state(ar_node *node, ar_context_slot *slot, ar_expr *value)
{
  ccl_list *slots;
  ccl_list *values;
  ar_type *sltype;

  ccl_pre( node != NULL );  ccl_pre( slot != NULL ); ccl_pre( value != NULL ); 
  ccl_pre( node->ref == NULL );

  sltype = ar_context_slot_get_type(slot);
  if( ar_type_is_scalar(sltype) )
    {
      slots = ccl_list_create();
      ccl_list_add(slots,ar_context_slot_add_reference(slot));
      values = ccl_list_create();
      ccl_list_add(values,ar_expr_add_reference(value));
    }
  else
    {
      slots = ar_context_expand_composite_slot(node->ctx,slot,NULL);
      values = ar_expr_expand_composite_types(value,node->ctx, NULL);
    }
  ccl_assert( ccl_list_get_size(slots) == ccl_list_get_size(values) );

  while( ! ccl_list_is_empty(slots) )
    {
      ar_context_slot *sl = (ar_context_slot *)ccl_list_take_first(slots);
      ar_expr *val = (ar_expr *)ccl_list_take_first(values);
      
      s_add_initial_state (node,sl,val);
      ar_expr_del_reference(val);
      ar_context_slot_del_reference(sl);
    }
  ar_type_del_reference(sltype);
  ccl_list_delete(slots);
  ccl_list_delete(values);
}

			/* --------------- */

ccl_list *
ar_node_get_initialized_slots (ar_node *node)
{
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_initialized_slots (node->ref);
  return node->initialized_slots;
}

const ar_expr *
ar_node_get_initial_value_cst (ar_node *node, ar_context_slot *sl)
{
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_initial_value_cst (node->ref, sl);
  return ccl_hash_get_with_key (node->initial_state_map, sl);
}

ar_expr *
ar_node_get_initial_value (ar_node *node, ar_context_slot *sl)
{
  ar_expr *result;
  
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_initial_value (node->ref, sl);
  result = ccl_hash_get_with_key (node->initial_state_map, sl);
  if (result != NULL)
    result = ar_expr_add_reference (result);
  return result;
}


void
ar_node_add_initial_constraint (ar_node *node, ar_expr *value)
{
  ccl_pre (node != NULL);
  ccl_pre (value != NULL);
  ccl_pre (node->ref == NULL);

  value = ar_expr_add_reference (value);
  ccl_list_add (node->initial_constraints, value);
}

			/* --------------- */

ccl_list *
ar_node_get_initial_constraints (ar_node *node)
{
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_initial_constraints (node->ref);

  return node->initial_constraints;
}

			/* --------------- */

void
ar_node_add_broadcast(ar_node *node, ar_broadcast *v)
{
  ccl_pre( node != NULL ); ccl_pre( v != NULL ); 
  ccl_pre( node->ref == NULL );

  ccl_list_add(node->sync_vectors,ar_broadcast_add_reference(v));
}

			/* --------------- */

ccl_list *
ar_node_get_broadcasts(ar_node *node)
{
  ccl_pre( node != NULL ); 

  if( node->ref != NULL )
    return ar_node_get_broadcasts(node->ref);
  return node->sync_vectors;
}

			/* --------------- */

static void
s_add_buckets (ar_identifier *bid, ccl_list *events, ccl_list *probas,
	       void *data)
{
  ar_node *n = data;
  ar_node_add_bucket (n, bid, events, probas);
}

ar_node *
ar_node_remove_composite_types(ar_node *node)
{
  ar_context *lctx = ar_node_get_context(node);
  ar_context *ctx = ar_node_get_parent_context(node);
  ar_node *result = ar_node_create(ctx);

  ccl_pre( ar_node_get_template(node) == NULL );
  ccl_pre(ar_node_is_leaf(node) );

  ar_context_del_reference(ctx);

  ctx = ar_node_get_context(result);

  {
    ar_identifier *name = ar_node_get_name(node);
    ar_node_set_name(result,name);
    ar_identifier_del_reference(name);
  }
    
  { /* ATTRIBUTES */
    ccl_list *attr = ar_node_get_attributes(node);
    ccl_list_delete(result->attributes);
    result->attributes = ccl_list_deep_dup(attr,(ccl_duplicate_func *)
					   ar_identifier_add_reference);
  }
  
  { /* SLOTS */
    ccl_pair *p;
    ccl_list *slots = ar_context_get_slots(lctx,
					  AR_SLOT_FLAG_STATE_VAR|
					  AR_SLOT_FLAG_FLOW_VAR|
					  AR_SLOT_FLAG_PARAMETER);
      
    for(p = FIRST(slots); p; p = CDR(p))
      {
	ar_context_slot *slot;
	ar_type *type;
	ar_identifier *sid = (ar_identifier *)CAR(p);

	if( ar_context_has_slot(ctx,sid) )
	  {
	    ar_identifier_del_reference(sid);
	    continue;
	  }

	slot = ar_context_get_slot(lctx,sid);
	type = ar_context_slot_get_type(slot);
	
	if( ar_type_is_scalar(type) )
	  {
	    uint32_t flags = ar_context_slot_get_flags(slot);

	    if( (flags & AR_SLOT_FLAG_PARAMETER) != 0 )
	      {
		ar_expr *value = ar_context_slot_get_value(slot);

		if( value && ar_expr_is_computable(value) )
		  {
		    ar_constant *cst = ar_expr_eval(value, lctx);
		    ar_expr *tmp = ar_expr_crt_constant(cst);
		    
		    ar_constant_del_reference(cst);
		    ar_expr_del_reference(value);
		    value = tmp;
		  }
		ar_node_add_local_parameter(result,sid,type,value);
		ccl_zdelete(ar_expr_del_reference,value);
	      }
	    else 
	      {
		ccl_list *attr = ar_context_slot_get_attributes(slot);
		ar_node_add_variable(result,sid,type,flags,attr);
		if( attr != NULL )
		  ccl_list_clear_and_delete(attr,(ccl_delete_proc *)
					    ar_identifier_del_reference);
	      }
	  }

	ar_type_del_reference(type);
	ar_context_slot_del_reference(slot);
	ar_identifier_del_reference(sid);
      }

    ccl_list_delete(slots);
  }

  { /* PARAMETER SETTING */
    
  }
  
  { /* TRANSITIONS */
    ccl_pair *p;
    ccl_list *trans = ar_node_get_transitions(node);
    ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *)
				       ar_expr_del_reference);
    for(p = FIRST(trans); p; p = CDR(p))
      {
	ar_trans *t = (ar_trans *)CAR(p);
	ar_expr *G = ar_trans_get_guard(t);
	ar_expr *newG = ar_expr_remove_composite_slots_with_cache(G,ctx,cache);	
	ar_event *E = ar_trans_get_event(t);
	ar_trans *new_t = ar_trans_create(newG,E);
	ar_expr_del_reference(G);
	ar_event_del_reference(E);
	ar_expr_del_reference(newG);
	
	ar_node_add_transition(result,new_t);

	{
	  const ar_trans_assignment *a = ar_trans_get_assignments(t);
	  for(; a; a = a->next)
	    {
	      ccl_list *lvalues = 
		ar_expr_expand_composite_types (a->lvalue, lctx, NULL);
	      ccl_pair *ps = FIRST(lvalues);
	      ccl_list *values = 
		ar_expr_expand_composite_types (a->value,lctx, NULL);
	      ccl_pair *pa = FIRST(values);

	      ccl_assert( ccl_list_get_size(lvalues) == 
			  ccl_list_get_size(values) );
	      for(; ps; ps = CDR(ps), pa = CDR(pa))
		ar_trans_add_assignment(new_t,(ar_expr *) CAR(ps),
					(ar_expr *)CAR(pa));
	      ccl_list_clear_and_delete(lvalues, (ccl_delete_proc *)
					ar_expr_del_reference);
	      ccl_list_clear_and_delete(values,(ccl_delete_proc *)
					ar_expr_del_reference);
	    }
	}
	ar_trans_del_reference(new_t);
      }    
    ccl_hash_delete (cache);
  }

  { /* ASSERTIONS */
    ccl_pair *p;
    ccl_list *A = ar_node_get_assertions(node);

    for(p = FIRST(A); p; p = CDR(p))
      {
	ar_expr *a = ar_expr_remove_composite_slots(CAR(p),ctx);
	ar_node_add_assertion(result,a);
	ar_expr_del_reference(a);
      }
  }

  { /* INITIAL CONSTRAINTS */
    ccl_pair *p;
    ccl_list *I = ar_node_get_initial_constraints (node);

    for (p = FIRST (I); p; p = CDR (p))
      {
	ar_expr *a = ar_expr_remove_composite_slots (CAR (p), ctx);
	ar_node_add_initial_constraint (result, a);
	ar_expr_del_reference (a);
      }
  }

  { /* INITIAL ASSIGNMENTS */
    ccl_pair *p;
    ccl_list *init = ar_node_get_initialized_slots (node);

    for(p = FIRST(init); p; p = CDR(p))
      {
	ar_context_slot *slot = (ar_context_slot *)CAR(p);
	ar_expr *value = ar_node_get_initial_value (node, slot);
	ccl_list *slots = ar_context_expand_composite_slot(lctx,slot,NULL);
	ccl_pair *ps = FIRST(slots);
	ccl_list *values = ar_expr_expand_composite_types(value,lctx, NULL);
	ccl_pair *pa = FIRST(values);

	ccl_assert( ccl_list_get_size(slots) == ccl_list_get_size(values) );

	for(; ps; ps = CDR(ps), pa = CDR(pa))
	  ar_node_add_initial_state(result,(ar_context_slot *)CAR(ps),
				    (ar_expr *)CAR(pa));
	ccl_list_clear_and_delete(slots,(ccl_delete_proc *)
				  ar_context_slot_del_reference);
	ccl_list_clear_and_delete(values,(ccl_delete_proc *)
				  ar_expr_del_reference);
	ar_expr_del_reference (value);
      }
  }

  { /* EVENTS */
    ar_event_poset *poset = ar_node_get_event_poset(node);
    ar_identifier_iterator *ii = ar_node_get_event_identifiers(node);
    
    ar_event_poset_del_reference(result->poset);
    result->poset = ar_event_poset_dup(poset);
    while( ccl_iterator_has_more_elements(ii) )
      {
	ar_identifier *eid = ccl_iterator_next_element(ii);
	ccl_list *events = ar_node_get_events_for_identifier(node,eid);
	ccl_list *revents = ccl_list_deep_dup(events,(ccl_duplicate_func *)
					     ar_identifier_add_reference);
	ar_idtable_put(result->events,eid,revents);	
	ar_identifier_del_reference(eid);
      }
    
    ar_event_poset_del_reference(poset);
    ccl_iterator_delete(ii);
  }

  {
    ccl_pair *p;
    const ccl_list *law_params = ar_node_get_ids_of_law_parameters (node);
    const ccl_list *laws = ar_node_get_ids_of_laws (node);
    
    /* LAW PARAMETERS and LAWS */
    for (p = FIRST (law_params); p; p = CDR (p))
      {
	sas_law_param *lp = ar_node_get_law_parameter (node, CAR (p));
	ar_node_add_law_parameter (result, CAR (p), lp);
	sas_law_param_del_reference (lp);
      }
	   
    for (p = FIRST (laws); p; p = CDR (p))
      {
	sas_law *l = ar_node_get_law (node, CAR (p));
	ar_node_add_law (result, CAR (p), l);
	sas_law_del_reference (l);
      }
  }

  {
    ccl_pair *p;
    const ccl_list *observers = ar_node_get_ids_of_observers (node);

    /* OBSERVERS  */
    for (p = FIRST (observers); p; p = CDR (p))
      {
	ar_expr *o = ar_node_get_observer (node, CAR (p));
	ar_expr *so = ar_expr_remove_composite_slots (o,ctx);
	ar_node_add_observer (result, CAR (p), so);
	ar_expr_del_reference (o);
	ar_expr_del_reference (so);
      }
  }

  {
    ccl_pointer_iterator *pi = ar_node_get_preemptibles (node);
    
    while (ccl_iterator_has_more_elements (pi))
      {
	ar_identifier *id = ccl_iterator_next_element (pi);
	ar_node_add_preemptible (result, id);
      }
    ccl_iterator_delete (pi);      
  }
  
  ar_node_map_buckets (node, s_add_buckets, result);

  {
    ar_identifier_iterator *ii = ar_node_get_events_with_priorities (node);
    
    while (ccl_iterator_has_more_elements (ii))
      {
	ar_identifier *id = ccl_iterator_next_element (ii);
	int pl = ar_node_get_priority (node, id);
	ar_node_add_priority (result, id, pl);
      }
    ccl_iterator_delete (ii);      
  }

  ar_context_del_reference(lctx);
  ar_context_del_reference(ctx);

  return result;
}

			/* --------------- */

static void
s_delete_event_list(void *ptr)
{
  ccl_list_clear_and_delete((ccl_list *)ptr,(ccl_delete_proc *)
			    ar_event_del_reference);
}

			/* --------------- */

static ar_identifier *
s_find_type(ar_type *type)
{
  ar_identifier *result = NULL;
  ccl_pair *p;
  ccl_list *types = ar_model_get_types();

  for(p = FIRST(types); p && result == NULL; p = CDR(p))
    {
      ar_identifier *id = (ar_identifier *)CAR(p);
      ar_type *t = ar_model_get_type(id);
      if( ar_type_equals(type,t) )
	result = ar_identifier_add_reference(id);
      ar_type_del_reference(t);
    }

  ccl_list_clear_and_delete(types,(ccl_delete_proc *)
			    ar_identifier_del_reference);

  return result;
}

			/* --------------- */

static void
s_dump_types(ccl_log_type log, ccl_list *types, ccl_hash *type_cache)
{
  ccl_pair *p;

  if( ccl_list_has(types,AR_BOOLEANS) )
    {
      ccl_list_remove(types,AR_BOOLEANS);
      ar_type_del_reference(AR_BOOLEANS);
    }

  if( ccl_list_has(types,AR_INTEGERS) )
    {
      ccl_list_remove(types,AR_INTEGERS);
      ar_type_del_reference(AR_INTEGERS);
    }

  if( ccl_list_is_empty(types) )
    return;

  for(p = FIRST(types); p; p = CDR(p))
    {
      ar_type *t = (ar_type *)CAR(p);

      if( ar_type_get_kind(t) == AR_TYPE_ABSTRACT )
	{
	  ccl_log(log,"sort ");
	  ar_type_log(log,t);
	  ccl_log(log,";\n");
	}
      else
	{
	  ar_identifier *tid = s_find_type(t);
      
	  if( tid != NULL )	
	    {
	      ccl_log(log,"domain ");
	      ar_identifier_log(log,tid);
	      ccl_log(log," = ");
	      ar_type_log(log,t);
	      ccl_log(log,";\n");
	      ccl_hash_find(type_cache,t);
	      ccl_hash_insert(type_cache,tid);
	    }
	}
    }
  ccl_log(log,"\n");
}

			/* --------------- */

static void
s_dump_signatures(ccl_log_type log, ccl_list *signatures)
{
  ccl_pair *p;

  if( ccl_list_is_empty(signatures) )
    return;

  for(p = FIRST(signatures); p; p = CDR(p))
    {
      ar_signature *sig = (ar_signature *)CAR(p);

      ccl_log(log,"sig ");
      ar_signature_log(log,sig);
      ccl_log(log,";\n");
    }
  ccl_log(log,"\n");
}

			/* --------------- */

static void
s_dump_constants(ccl_log_type log, ccl_list *constants)
{
  ccl_pair *p;

  if( ccl_list_is_empty(constants) )
    return;

  for(p = FIRST(constants); p; p = CDR(p))
    {
      ar_context_slot *sl = (ar_context_slot *)CAR(p);
      ar_identifier *id = ar_context_slot_get_name(sl);
      ar_type *type = ar_context_slot_get_type(sl);
      ar_expr *value = ar_context_slot_get_value(sl);

      ccl_log(log,"const ");
      ar_identifier_log_global_quote(log,id);
      ccl_log(log," : ");
      ar_type_log(log,type);
      if( value != NULL )
	{
	  ccl_log(log," = ");
	  ar_expr_log(log,value);
	  ar_expr_del_reference(value);
	}
      ccl_log(log,";\n");
      ar_identifier_del_reference(id);
      ar_type_del_reference(type);
    }
  ccl_log(log,"\n");
}

			/* --------------- */

static void
s_dump_nodes(ccl_log_type log, ccl_list *nodes, ccl_hash *types, 
	     int with_epsilon)
{  
  ccl_pair *p;

  for(p = FIRST(nodes); p; p = CDR(p))
    {
      ar_node *n = CAR(p);

      s_node_log(log,n,with_epsilon,types);
      ccl_log(log,"\n");
    }
  ccl_log(log,"\n");
}
			/* --------------- */


static void
s_collect_type(ar_type *type, ccl_list *types)
{
  ar_type_kind k = ar_type_get_kind(type);

  if( k == AR_TYPE_ARRAY )
    {
      ar_type *t = ar_type_array_get_base_type(type);

      s_collect_type(t,types);
      ar_type_del_reference(t);
    }
  else if( k == AR_TYPE_STRUCTURE )
    {
      ar_identifier_iterator *it = ar_type_struct_get_fields(type);

      while( ccl_iterator_has_more_elements(it) )
	{
	  ar_identifier *fname = ccl_iterator_next_element(it);
	  ar_type *ftype = ar_type_struct_get_field(type,fname);
	  s_collect_type(ftype,types);
	  ar_type_del_reference(ftype);
	  ar_identifier_del_reference(fname);
	}
      ccl_iterator_delete(it);
    }
      
  if( ! ccl_list_has(types,type) )
    ccl_list_add(types,ar_type_add_reference(type));
}
			/* --------------- */

static void
s_collect_global_parameters(ar_expr *expr, ccl_list *constants, ccl_list *types)
{
  ar_expr_op op = ar_expr_get_op(expr);

  switch( op ) {
  case AR_EXPR_PARAM :
    {
      ar_context_slot *sl = ar_expr_get_slot(expr);
      ar_identifier *id = ar_context_slot_get_name(sl);

      if( ar_model_has_parameter(id) && ! ccl_list_has(constants,sl) )
	{
	  ar_type *type = ar_context_slot_get_type(sl);
	  ar_expr *value = ar_context_slot_get_value(sl);

	  s_collect_type(type,types);
	  if( value != NULL )
	    {
	      s_collect_global_parameters(value,constants,types);
	      ar_expr_del_reference(value);
	    }
	  ar_type_del_reference(type);
	  ccl_list_add(constants,ar_context_slot_add_reference(sl));
	}

      ar_context_slot_del_reference(sl);
      ar_identifier_del_reference(id);
    }
    break;

  case AR_EXPR_ITE: case AR_EXPR_OR: case AR_EXPR_AND: case AR_EXPR_EQ:
  case AR_EXPR_NEQ: case AR_EXPR_LT: case AR_EXPR_GT: case AR_EXPR_LEQ:
  case AR_EXPR_GEQ: case AR_EXPR_ADD: case AR_EXPR_SUB: case AR_EXPR_MUL:
  case AR_EXPR_DIV: case AR_EXPR_MOD: case AR_EXPR_NOT: case AR_EXPR_NEG:
  case AR_EXPR_STRUCT_MEMBER:
  case AR_EXPR_ARRAY_MEMBER:
  case AR_EXPR_CALL:
  case AR_EXPR_ARRAY:
  case AR_EXPR_STRUCT:
  case AR_EXPR_EXIST: case AR_EXPR_FORALL:
    {
      int i, arity;
      ar_expr **args = ar_expr_get_args(expr,&arity);

      for(i = 0; i < arity; i++)
	s_collect_global_parameters(args[i],constants,types);
    }
    break;

  case AR_EXPR_CST: case AR_EXPR_VAR:
    break;


  default:
    ccl_throw_no_msg (internal_error);
  }
}
			/* --------------- */

static void
s_collect_dependencies(ar_node *node, ccl_list *types, ccl_list *constants, 
		       ccl_list *signatures, ccl_list *nodes)
{
  /* depth first search */
  {
    ar_context_slot_iterator *it = ar_node_get_slots_for_subnodes(node);

    while( ccl_iterator_has_more_elements(it) )
      {
	ar_context_slot *sl = ccl_iterator_next_element(it);
	ar_type *type = ar_context_slot_get_type(sl);
	ar_node *subnode = ar_type_bang_get_node(type);
	
	if( ! ccl_list_has(nodes,subnode) )
	  s_collect_dependencies(subnode,types,constants,signatures,nodes);
	ar_node_del_reference(subnode);
	ar_type_del_reference(type);
	ar_context_slot_del_reference(sl);
      }
    ccl_iterator_delete(it);
  }

  {
    ar_context_slot_iterator *it = ar_node_get_slots_for_parameters(node);

    while( ccl_iterator_has_more_elements(it) )
      {
	ar_context_slot *sl = ccl_iterator_next_element(it);
	ar_type *type = ar_context_slot_get_type(sl);
	ar_expr *value = ar_context_slot_get_value(sl);

	s_collect_type(type,types);
	ar_type_del_reference(type);

	if( value != NULL )
	  {
	    signatures = ar_expr_get_signatures(value,signatures);
	    ar_expr_del_reference(value);
	  }
	ar_context_slot_del_reference(sl);
      }
    ccl_iterator_delete(it);
  }

  {
    ar_context_slot_iterator *it = ar_node_get_slots_for_variables(node);

    while( ccl_iterator_has_more_elements(it) )
      {
	ar_context_slot *sl = ccl_iterator_next_element(it);
	ar_type *type = ar_context_slot_get_type(sl);
	s_collect_type(type,types);
	ar_type_del_reference(type);
	ar_context_slot_del_reference(sl);
      }
    ccl_iterator_delete(it);
  }

  {
    ccl_pair *p;
    ccl_list *assertions = ar_node_get_assertions(node);

    for(p = FIRST(assertions); p; p = CDR(p))
      {
	ar_expr *a = CAR(p);
	signatures = ar_expr_get_signatures(a,signatures);
	s_collect_global_parameters(a,constants,types);
      }
  }

  {
    ccl_pair *p;
    ccl_list *trans = ar_node_get_transitions(node);

    for(p = FIRST(trans); p; p = CDR(p))
      {
	ar_trans *t = CAR(p);
	const ar_trans_assignment *a = ar_trans_get_assignments(t);
	ar_expr *g = ar_trans_get_guard(t);

	s_collect_global_parameters(g,constants,types);
	signatures = ar_expr_get_signatures(g,signatures);
	for(; a; a = a->next)
	  {
	    signatures = ar_expr_get_signatures(a->value,signatures); 
	    s_collect_global_parameters(a->value,constants,types);
	  }
	ar_expr_del_reference(g);
      }
  }

  {
    ccl_pair *p;
    ccl_list *iconstraints = ar_node_get_initial_constraints (node);

    for(p = FIRST (iconstraints); p; p = CDR (p))
      {
	ar_expr *c = CAR (p);
	signatures = ar_expr_get_signatures (c, signatures);
	s_collect_global_parameters (c, constants, types);
      }
  }

  {
    ccl_pair *p;
    ccl_list *init = ar_node_get_initialized_slots(node);    
    for(p = FIRST(init); p; p = CDR(p))
      {
	ar_expr *val = ar_node_get_initial_value (node, CAR (p));
	signatures = ar_expr_get_signatures (val, signatures); 
	s_collect_global_parameters (val, constants, types);
	ar_expr_del_reference (val);
      }
  }

  {
    ccl_pair *p;

    for(p = FIRST(signatures); p; p = CDR(p))
      {
	int i;
	ar_type *t;
	ar_signature *sig = CAR(p);
	int arity = ar_signature_get_arity(sig);
	
	for(i = 0; i < arity; i++)
	  {
	    t = ar_signature_get_ith_arg_domain(sig,i);
	    s_collect_type(t,types);
	    ar_type_del_reference(t);
	  }
	t = ar_signature_get_return_domain(sig);
	s_collect_type(t,types);
	ar_type_del_reference(t);
      }
  }
  
  ccl_assert( ! ccl_list_has(nodes,node) );
  ccl_list_add(nodes,ar_node_add_reference(node));
}

			/* --------------- */

void
ar_node_dump_node(ccl_log_type log, ar_node *node, int with_epsilon)
{  
  ccl_list *types = ccl_list_create();
  ccl_list *constants = ccl_list_create();
  ccl_list *signatures = ccl_list_create();
  ccl_list *nodes = ccl_list_create();
  ccl_hash *type_cache = ccl_hash_create(NULL,NULL,NULL,(ccl_delete_proc *)
					 ar_identifier_del_reference);

  s_collect_dependencies(node,types,constants,signatures,nodes);
  s_dump_types(log,types,type_cache);
  s_dump_signatures(log,signatures);
  s_dump_constants(log,constants);

  s_dump_nodes(log,nodes,type_cache,with_epsilon);

  ccl_list_clear_and_delete(types,(ccl_delete_proc *)ar_type_del_reference);
  ccl_list_clear_and_delete(nodes,(ccl_delete_proc *)ar_node_del_reference);
  ccl_list_clear_and_delete(signatures,(ccl_delete_proc *)
			    ar_signature_del_reference);
  ccl_list_clear_and_delete(constants,(ccl_delete_proc *)
			    ar_context_slot_del_reference);
  ccl_hash_delete(type_cache);
}

			/* --------------- */

int
ar_node_has_law_parameter (ar_node *node, ar_identifier *id)
{
  if (node->ref != NULL)
    return ar_node_has_law_parameter (node->ref, id);
  return ar_context_has_law_parameter (node->ctx, id);
}

sas_law_param *
ar_node_get_law_parameter (ar_node *node, ar_identifier *id)
{
  if (node->ref != NULL)
    return ar_node_get_law_parameter (node->ref, id);
  return ar_context_get_law_parameter (node->ctx, id);
}

static void
s_law_parameters_in_topological_order (ar_node *node, ar_identifier *id,
				       ccl_set *done, ccl_list *result)
{
  ccl_set *deps;
  sas_law_param *lp;
  ccl_pointer_iterator *pi;
  
  if (ccl_set_has (done, id))
    return;    
  ccl_set_add (done, id);
  
  lp = ar_node_get_law_parameter (node, id);
  if (lp != NULL)
    {
      deps = sas_law_param_get_variables (lp, NULL);
      pi = ccl_set_get_elements (deps);
      while (ccl_iterator_has_more_elements (pi))
	{
	  ar_identifier *depid = ccl_iterator_next_element (pi);
	  s_law_parameters_in_topological_order (node, depid, done, result);
	}
      ccl_iterator_delete (pi);
      ccl_set_delete (deps);
      sas_law_param_del_reference (lp);
      ccl_list_add (result, id);
    }  
}

const ccl_list *
ar_node_get_ids_of_law_parameters (ar_node *node)
{  
  if (node->ref != NULL)
    return ar_node_get_ids_of_law_parameters (node->ref);
  return node->law_params; 
}

ccl_list *
ar_node_get_ordered_ids_of_law_parameters (ar_node *node)
{
  ccl_pair *p;
  ccl_list *result;
  ccl_set *done;
  ccl_list *global;
  
  if (node->ref != NULL)
    return ar_node_get_ordered_ids_of_law_parameters (node->ref);
  result = ccl_list_create ();
  done = ccl_set_create ();
  global = ar_model_get_law_parameters ();
  while (!ccl_list_is_empty (global))
    {
      ar_identifier *id = ccl_list_take_first (global);
      s_law_parameters_in_topological_order (node, id, done, result);
      ar_identifier_del_reference (id);
    }
  ccl_list_delete (global);
  
  for (p = FIRST (node->law_params); p; p = CDR (p))
    s_law_parameters_in_topological_order (node, CAR (p), done, result);
  ccl_set_delete (done);
  
  return result;
}

void
ar_node_add_law_parameter (ar_node *node, ar_identifier *paramid,
			   sas_law_param *param)
{
  if (!ar_node_has_law_parameter (node, paramid))
    ccl_list_add (node->law_params, paramid);
  ar_context_add_law_parameter (node->ctx, paramid, param);
}

int
ar_node_has_law (ar_node *node, ar_identifier *eventid)
{
  if (node->ref != NULL)
    return ar_node_has_law (node->ref, eventid);
  return ar_idtable_has (node->lawtable, eventid);  
}

void
ar_node_add_law (ar_node *node, ar_identifier *eventid, sas_law *law)
{
  if (!ar_node_has_law (node, eventid))
    ccl_list_add (node->laws, eventid);
  ar_idtable_put (node->lawtable, eventid, law);
}

const ccl_list *
ar_node_get_ids_of_laws (ar_node *node)
{
  if (node->ref != NULL)
    return ar_node_get_ids_of_laws (node->ref);
  return node->laws;
}

sas_law *
ar_node_get_law (ar_node *node, ar_identifier *id)
{
  if (node->ref != NULL)
    return ar_node_get_law (node->ref, id);
  return ar_idtable_get (node->lawtable, id);
}

			/* --------------- */

void
ar_node_add_observer (ar_node *node, ar_identifier *id, ar_expr *e)
{
  ccl_pre (node != NULL);
  ccl_pre (id != NULL);
  ccl_pre (e != NULL);
  if (! ar_idtable_has (node->obstable, id))
    ccl_list_add (node->observers, id);
  ar_idtable_put (node->obstable, id, e);
}

ar_expr * 
ar_node_get_observer (ar_node *node, ar_identifier *id)
{
  ccl_pre (node != NULL);
  ccl_pre (id != NULL);
  
  if (node->ref != NULL)
    return ar_node_get_observer (node->ref, id);
  return ar_idtable_get (node->obstable, id);
}

const ccl_list *
ar_node_get_ids_of_observers (ar_node *node)
{
  ccl_pre (node != NULL);
  if (node->ref != NULL)
    return ar_node_get_ids_of_observers (node->ref);
  return node->observers;
}

void
ar_node_add_preemptible (ar_node *node, ar_identifier *eventid)
{
  ccl_pre (node != NULL);
  ccl_pre (eventid != NULL);
  
  return ccl_set_add (node->preemptibles,
		      ar_identifier_add_reference (eventid));
}

ccl_pointer_iterator *
ar_node_get_preemptibles (ar_node *node)
{
  ccl_pre (node != NULL);

  if (node->ref != NULL)
    return ar_node_get_preemptibles (node->ref);
  return ccl_set_get_elements (node->preemptibles);
}

static void
s_delete_bucket (bucket *b)
{
  ar_identifier_del_reference (b->id);
  ccl_list_clear_and_delete (b->events, (ccl_delete_proc *)
			     ar_identifier_del_reference);
  ccl_list_clear_and_delete (b->probas, (ccl_delete_proc *)
			     sas_law_param_del_reference);
  ccl_delete (b);
}
		 
void
ar_node_add_bucket (ar_node *node, ar_identifier *id, ccl_list *events,
		    ccl_list *probas)
{
  ccl_pair *p;
  bucket *b = ccl_new (bucket);
  ccl_pre (node != NULL);
  ccl_pre (events != NULL);
  ccl_pre (probas != NULL);

  b->id = ar_identifier_add_reference (id);
  b->events = ccl_list_deep_dup (events, (ccl_duplicate_func *)
				 ar_identifier_add_reference);
  b->probas = ccl_list_deep_dup (probas, (ccl_duplicate_func *)
				 sas_law_param_add_reference);
  ccl_list_add (node->buckets, b);
  for (p = FIRST (events); p; p = CDR (p))
    {
      ccl_pre (! ar_idtable_has (node->event_to_buckets_table, CAR (p)));
      ar_idtable_put (node->event_to_buckets_table, CAR (p), b);
    }
}

int 
ar_node_is_in_bucket (ar_node *node, ar_identifier *id)
{
  ccl_pre (node != NULL);
  ccl_pre (id != NULL);

  if (node->ref != NULL)
    return ar_node_is_in_bucket (node->ref, id);
  return ar_idtable_has (node->event_to_buckets_table, id);
}

void
ar_node_map_buckets (ar_node *node,
		     void (*map)(ar_identifier *id, ccl_list *events,
				 ccl_list *probas, void *data),
		     void *clientdata)
{
  if (node->ref != NULL)
    ar_node_map_buckets (node->ref, map, clientdata);
  else
    {
      ccl_pair *p;
      
      for (p = FIRST (node->buckets); p; p = CDR (p))
	{
	  bucket *b = CAR (p);
	  map (b->id, b->events, b->probas, clientdata);
	}
    }
}

void
ar_node_add_priority (ar_node *node, ar_identifier *eventid, int pl)
{
  ccl_pre (node != NULL);
  ccl_pre (eventid != NULL);

  ar_idtable_put (node->priorities, eventid, (void *)(intptr_t) pl);
}

int
ar_node_get_priority (ar_node *node, ar_identifier *eventid)
{
  ccl_pre (node != NULL);
  ccl_pre (eventid != NULL);

  if (node->ref != NULL)
    return ar_node_get_priority (node->ref, eventid);
  return (intptr_t) ar_idtable_get (node->priorities, eventid);  
}
  

ar_identifier_iterator *
ar_node_get_events_with_priorities (ar_node *node)
{
  ccl_pre (node != NULL);
  
  if (node->ref != NULL)
    return ar_node_get_events_with_priorities (node->ref);
  return ar_idtable_get_keys (node->priorities);
}
