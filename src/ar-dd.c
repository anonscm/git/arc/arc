/*
 * ar-dd.c -- Toupie's Decision Diagram Package written by A. Rauzy
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include <stdio.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-log.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-pool.h>

#include "ar-dd.h"

#define DUMP_MAPS 0

#define DD_P1			12582917
#define DD_P2			4256249
#define DD_P3			741457
#define DD_P4			1618033999

#if 1
# define __LARGE_DD_TABLES__ 1
#else
# define __SMALL_DD_TABLES__ 1
#endif

#if !defined(__LARGE_DD_TABLES__) && !defined(__SMALL_DD_TABLES__)
#  define __LARGE_DD_TABLES__ 1
#endif

#define MAX_DD_TABLE_SIZE 100
#define NB_INSERTIONS_BEFORE_GC 10000000

#ifdef __LARGE_DD_TABLES__
# define DECISION_DIAGRAM_PAGE_SIZE       32768
# define DECISION_DIAGRAM_TABLE_PAGE_SIZE 65536
# define MINIMUM_HASHTABLE_SIZE           131072
# define MAXIMUM_HASHTABLE_SIZE           ((33554507<<1)+7)
# define ENLARGEMENT_RATIO                (float)5.00
# define REDUCTION_RATIO                  (float)0.001
# define MINIMUM_HASHCACHE_SIZE            524288
# define MAXIMUM_HASHCACHE_SIZE           21229331
#endif /* __LARGE_TABLES__ */

#ifdef __SMALL_DD_TABLES__
# define DECISION_DIAGRAM_PAGE_SIZE       1024
# define DECISION_DIAGRAM_TABLE_PAGE_SIZE 2048
# define MINIMUM_HASHTABLE_SIZE           1024
# define MAXIMUM_HASHTABLE_SIZE           32768
# define ENLARGEMENT_RATIO                (float)0.25
# define REDUCTION_RATIO                  (float)0.001
# define MINIMUM_HASHCACHE_SIZE           2048
# define MAXIMUM_HASHCACHE_SIZE           65536
#endif /* __SMALL_TABLES__ */

			/* --------------- */

# define DD_MALLOC(_type_) ccl_new(_type_)
# define DD_CALLOC(_type_,_nb_) ccl_new_array(_type_,_nb_)
# define DD_FREE(_ptr_) ccl_delete(_ptr_)

			/* --------------- */

# define IS_TABLE_UNARY(T)     (CCL_PTRHASBIT(T))
# define TABLE_ADDRESS(T)      CCL_BITPTR2PTR(ar_ddtable,T)
# define UNARY_TABLE(T)        CCL_BITPTR(ar_ddtable,T)
# define NEXT_TABLE(T)         (((ar_ddword) (T))->table)    
# define TABLE_SIZE(T)         (((ar_ddword) (T)+1)->size)

			/* --------------- */

typedef struct memo_record *memo_record;
struct memo_record {
  ar_dd if_son;
  ar_dd then_son;
  ar_dd else_son;
  union {
    ar_dd dd_result;
    double double_result;
  };
};

#define IF_SON(T)             ((T)->if_son)
#define THEN_SON(T)           ((T)->then_son)
#define ELSE_SON(T)           ((T)->else_son)
#define RESULT_SON(T)         ((T)->dd_result)

#define FIRST_OPERAND(T)      ((T)->if_son)
#define SECOND_OPERAND(T)     ((T)->then_son)
#define THIRD_OPERAND(T)      ((T)->else_son)
#define OPERATION_RESULT(T)   ((T)->dd_result)
#define OPERATION_DOUBLE_RESULT(T)   ((T)->double_result)

typedef struct dd_node_page *dd_node_page;
struct dd_node_page {
  long         size;
  dd_node_page prev;
  ar_dd      page;
};

typedef struct dd_table_page *dd_table_page;
struct dd_table_page {
  long          size;
  dd_table_page prev;
  ar_ddtable  page;
};

struct ar_decision_diagram_manager_st {
  /* leaves of decision diagrams */
  ar_dd       one;
  ar_dd       zero;

  /* main hashtable of decision diagrams nodes */
  ar_ddtable  ht_nodes;
  long          ht_actual_size;
  long          ht_minimum_size;
  long          ht_maximum_size;
  long          ht_number_of_active_nodes;
  long          ht_number_of_empty_entries;
  long          ht_number_of_collisions;
  long          ht_size_longest_collision_chain;
  long          ht_entry_longest_collision_chain;
  float         ht_actual_ratio;
  float         ht_enlargement_ratio;
  float         ht_reduction_ratio;
  long          ht_number_of_insertions;

  /* memoization table */
  memo_record   op_cache;
  long          op_actual_size;
  long          op_minimum_size;
  long          op_maximum_size;
  long          op_number_of_stored_records;

  /* page manager */
  dd_node_page  pm_first_page;
  long          pm_current_page_size;
  long          pm_number_of_pages;
  ar_dd       pm_first_free;
  long          pm_number_of_allocated_nodes;
  long          pm_number_of_free_nodes;

  ccl_pool **tm_pools_of_tables;
  long       tm_max_table_size;

  int gc_enabled;
};

			/* --------------- */

static int init_count = 0;

ar_ddm DDM;
static int s_create_new_dd_page (ar_ddm ddm);

int
ar_dd_init (void)
{
  init_count++;
  if (init_count == 1)
    {
      DDM = ar_dd_create_manager ();
    }

  return 1;
}

			/* --------------- */

void
ar_dd_terminate (void)
{
  init_count--;
  if (init_count == 0)
    {      
#if DUMP_MAPS
      FILE *f = fopen("ht.map", "w");
      ar_dd_display_node_map (DDM, f);
      fflush (f);
      fclose (f);
      f = fopen("cache.map", "w");
      ar_dd_display_cache_map (DDM, f);
      fflush (f);
      fclose (f);
#endif

      ar_dd_delete_manager (DDM);
    }
}



/*
 *
 * MISCELLANEOUS OPERATIONS ON DDs
 *
 */

static int
s_are_all_identical_in_table(int size, ar_ddtable table)
{
  int   identical;
  int n;

  identical=1;
  for (n=1;(identical)&&(n<size);n++)
    identical=(AR_DD_ENTHDD(table,0)==AR_DD_ENTHDD(table,n));

  return(identical);
}

static int
s_are_tables_identical(int arity, ar_ddcode encoding,
		       ar_ddtable table1, ar_ddtable table2)
{  
  int size = AR_DD_TABLE_SIZE(arity,encoding);

  ccl_pre (arity <= AR_DD_MAX_ARITY);

  return ccl_memcmp (table1, table2, size * sizeof (ar_dd)) == 0;
}

			/* --------------- */

static int
s_compact_decision_diagram_table(int arity, ar_ddtable table)
{
  int new_arity, j;

  ccl_pre (arity <= AR_DD_MAX_ARITY);

  new_arity = 0;
  for( j = 1; j < arity; j++ )
    {
      if(AR_DD_CNTHDD(table,new_arity) == AR_DD_CNTHDD(table,j)) 
	{
	  AR_DD_MAX_OFFSET_NTHDD(table,new_arity) = 
	    AR_DD_MAX_OFFSET_NTHDD(table,j);
	  AR_DD_FREE(AR_DD_CNTHDD(table,j));
	  AR_DD_CNTHDD(table,j) = NULL;
	}
      else 
	{
	  new_arity++;
	  if( new_arity != j ) 
	    {
	      AR_DD_MIN_OFFSET_NTHDD(table,new_arity) = 
		AR_DD_MIN_OFFSET_NTHDD(table,j);
	      AR_DD_MAX_OFFSET_NTHDD(table,new_arity) = 
		AR_DD_MAX_OFFSET_NTHDD(table,j);
	      AR_DD_CNTHDD(table,new_arity) = AR_DD_CNTHDD(table,j);
	      AR_DD_CNTHDD(table,j) = NULL;
	    }
	}
    }

  ccl_post (new_arity + 1 <= AR_DD_MAX_ARITY);

  return new_arity + 1;
}
			/* --------------- */

static void
s_update_reference_count (ar_dd F)
{
  int     i;
  ar_dd G;
  
  AR_DD_FLAG (F) = 1;
  if (AR_DD_CODE (F) == EXHAUSTIVE)
    {
      for (i = 0; i < AR_DD_ARITY (F); i++) 
	{
	  G = AR_DD_ENTHSON (F, i);
	  AR_DD_FREE (G);
	  if (AR_DD_REFCOUNT (G) == 0)
	    s_update_reference_count (G);
	}
    }
  else
    {
      for (i = 0; i < AR_DD_ARITY (F); i++) 
	{
	  G = AR_DD_CNTHSON (F, i);
	  AR_DD_FREE (G);
	  if (AR_DD_REFCOUNT (G) == 0)
	    s_update_reference_count (G);
	}
    }
}

			/* --------------- */

void
ar_dd_initialize_decision_diagram_flag(ar_dd F)
{
  int i;
  if( AR_DD_FLAG(F) != 0 )
    {
      if( AR_DD_CODE(F) == EXHAUSTIVE )
	{
	  for( i = 0; i < AR_DD_ARITY(F); i++) 
	    ar_dd_initialize_decision_diagram_flag(AR_DD_ENTHSON(F,i));
	}
      else
	{
	  for( i = 0; i < AR_DD_ARITY(F); i++) 
	    ar_dd_initialize_decision_diagram_flag(AR_DD_CNTHSON(F,i));
	}
      AR_DD_FLAG(F)=0;
    }
}

			/* --------------- */

typedef enum { SIZE_IN_BYTES, NUMBER_OF_NODES } dd_size;

			/* --------------- */

static long
s_decision_diagram_size(ar_dd F, dd_size which_size)
{
  long size = 0;
  int  i;

  if( AR_DD_FLAG(F) != 0 ) size=0;
  else 
    {
      AR_DD_FLAG(F)=1;
      switch( which_size ) {
      case SIZE_IN_BYTES :
	size = sizeof(struct ar_decision_diagram_st)
	  +4 * AR_DD_TABLE_SIZE(AR_DD_ARITY(F),AR_DD_CODE(F));
	break;

      case NUMBER_OF_NODES :
	size=1;
	break;
      }
      if( AR_DD_CODE(F) == EXHAUSTIVE )
	{
	  for( i = 0; i < AR_DD_ARITY(F); i++ ) 
	    size += s_decision_diagram_size(AR_DD_ENTHSON(F,i),which_size);
	}
      else
	{
	  for( i = 0; i < AR_DD_ARITY(F); i++ ) 
	    size += s_decision_diagram_size(AR_DD_CNTHSON(F,i),which_size);
	}
    }
  return size;
}

			/* --------------- */

long
ar_dd_get_number_of_nodes (ar_dd F)
{
  long size;

  ar_dd_initialize_decision_diagram_flag(F);
  size=s_decision_diagram_size(F,NUMBER_OF_NODES);
  ar_dd_initialize_decision_diagram_flag(F);

  return(size);
}

			/* --------------- */

long
ar_dd_get_size_in_bytes (ar_dd F)
{
  long size;

  ar_dd_initialize_decision_diagram_flag(F);
  size=s_decision_diagram_size(F,SIZE_IN_BYTES);
  ar_dd_initialize_decision_diagram_flag(F);

  return(size);
}

/*
 *
 * OPERATIONS OF THE TABLE MANAGER
 *
 */

static void
s_delete_table_manager (ar_ddm ddm)
{
  long i;

  for (i = 1; i < ddm->tm_max_table_size ; i++)    
    ccl_zdelete (ccl_pool_delete, ddm->tm_pools_of_tables[i]);
  ccl_zdelete (DD_FREE, ddm->tm_pools_of_tables);
}

			/* --------------- */

static void
s_collect_dd_table(ar_ddm     ddm, 
		   int          arity, 
		   ar_ddcode  encoding,
		   ar_ddtable T)
{
  long Tsize = (long) AR_DD_TABLE_SIZE (arity, encoding);
  ccl_pre (arity <= AR_DD_MAX_ARITY);
  if (Tsize > MAX_DD_TABLE_SIZE)
    DD_FREE (T);
  else
    {
      ccl_assert (1 <= Tsize && Tsize <= ddm->tm_max_table_size);
      ccl_pool_release (ddm->tm_pools_of_tables[Tsize], T);
    }
}

			/* --------------- */

static void
s_reinitialize_table_manager(ar_ddm ddm)
{
  s_delete_table_manager (ddm);
  ddm->tm_pools_of_tables = DD_CALLOC (ccl_pool *, 2);
  ddm->tm_pools_of_tables[1] = 
    ccl_pool_create ("DD sons table", sizeof (ar_dd), 1000);
  ddm->tm_max_table_size = 1;
}

			/* --------------- */

static void
s_create_table_manager(ar_ddm ddm, long page_size)
{
  s_reinitialize_table_manager(ddm);
}

			/* --------------- */

ar_ddtable
ar_dd_allocate_dd_table(ar_ddm ddm, int arity, ar_ddcode encoding)
{
  long Rsize = (long) AR_DD_TABLE_SIZE (arity, encoding);
  ar_ddtable R = NULL;

  ccl_pre (arity <= AR_DD_MAX_ARITY);
  
  if (Rsize > MAX_DD_TABLE_SIZE)
    R = DD_CALLOC (ar_dd, Rsize);
  else 
    {
      if (Rsize > ddm->tm_max_table_size)
	{
	  ccl_pool **newtable = DD_CALLOC (ccl_pool *, Rsize + 1);
	  ccl_memzero (newtable, sizeof (ccl_pool *) *(Rsize + 1));
	  ccl_memcpy (newtable, ddm->tm_pools_of_tables,
		      (ddm->tm_max_table_size + 1 ) * sizeof (ccl_pool *));
	  DD_FREE (ddm->tm_pools_of_tables);
	  ddm->tm_pools_of_tables = newtable;
	  ddm->tm_max_table_size = Rsize;
	}

      if (ddm->tm_pools_of_tables[Rsize] == NULL)
	ddm->tm_pools_of_tables[Rsize] = 
	  ccl_pool_create ("DD sons table", Rsize * sizeof (ar_dd), 1000);

      R = (ar_ddtable) ccl_pool_alloc (ddm->tm_pools_of_tables[Rsize]);
      ccl_memzero (R, Rsize * sizeof (ar_dd));
    }
  
  return R;
}

			/* --------------- */

static void
s_remove_pointers_of_decision_diagram_table(ar_ddm     ddm, 
					    int          arity, 
					    ar_ddcode  encoding,
					    ar_ddtable T)
{
  int i;

  ccl_pre (arity <= AR_DD_MAX_ARITY);
  if( encoding == EXHAUSTIVE )
    {
      for( i = 0; i < arity; i++ )
	if( AR_DD_ENTHDD(T,i) != NULL )
	  AR_DD_FREE(AR_DD_ENTHDD(T,i));
    }
  else
    {
      for( i = 0; i < arity; i++ )
	if( AR_DD_CNTHDD(T,i) != NULL )
	  AR_DD_FREE(AR_DD_CNTHDD(T,i));
    }
}

			/* --------------- */

void
ar_dd_free_dd_table (ar_ddm ddm, int arity, ar_ddcode encoding, ar_ddtable T)
{
  ccl_pre (arity <= AR_DD_MAX_ARITY);
  s_remove_pointers_of_decision_diagram_table (ddm, arity, encoding, T);
  s_collect_dd_table (ddm, arity, encoding, T);
}

/*
 *
 * NODE PAGER OPERATIONS
 *
 */

static void
s_reset_node_page(dd_node_page a_page)
{
  ar_dd F;
  long    n;
  
  for( n = 0; n < a_page->size-1; n++) 
    {
      F = a_page->page+n;
      AR_DD_INDEX(F)= 0;
      AR_DD_NEXT(F) = F+1;
    }
  F = a_page->page+a_page->size-1;
  AR_DD_INDEX(F) = 0;
  AR_DD_NEXT(F) = NULL;
}

			/* --------------- */

static int
s_create_new_dd_page (ar_ddm ddm)
{
  ar_dd F;
  dd_node_page new_page = NULL;

  ccl_try (memory_exhausted_exception)
    {
      new_page = ccl_new (struct dd_node_page);
      new_page->size = ddm->pm_current_page_size;
      new_page->page = 
	ccl_new_array (struct ar_decision_diagram_st, new_page->size);
      new_page->prev = ddm->pm_first_page;
      ddm->pm_first_page = new_page;
      ddm->pm_number_of_pages++;
      s_reset_node_page (new_page);
      F = new_page->page + new_page->size - 1;
      AR_DD_NEXT (F) = ddm->pm_first_free;
      ddm->pm_first_free = new_page->page;
      ddm->pm_number_of_allocated_nodes += new_page->size;
      ddm->pm_number_of_free_nodes += new_page->size;
    }
  ccl_catch
    {
      ccl_zdelete (ccl_delete, new_page);
      new_page = NULL;
    }
  ccl_end_try;

  return new_page != NULL;
}

			/* --------------- */

#define is_trivial(ddm, d) \
((d) == ar_dd_one (ddm) || (d) == ar_dd_zero (ddm))

# define CHECK_DEPTH 1
# define CHECK_REFCOUNT(N)  ccl_assert (AR_DD_REFCOUNT (N) > 0)

#if DD_CHECKING_ENABLED


void
s_check_node_ (ar_ddm ddm, ar_dd N, int depth, const char *file, int line);

static void
s_check_ite_ (ar_ddm ddm, int index, int arity, ar_ddcode encoding,
	      ar_ddtable sons, int depth, const char *file, int line)
{
  int i;

  if (depth == 0)
    return;
  ccl_pre (arity <= AR_DD_MAX_ARITY);
  if( encoding == EXHAUSTIVE )
    {
      for(i = 0; i<arity; i++) 
	{
	  ccl_assert (AR_DD_REFCOUNT (AR_DD_ENTHDD(sons,i)) > 0);

	  if ((!is_trivial(ddm,AR_DD_ENTHDD(sons,i))) &&
	      AR_DD_INDEX (AR_DD_ENTHDD(sons,i)) <= index)
	    {
	      fprintf (stderr, "%s:%d: bad DD structuration\n", file, line);
	      abort ();
	    }
	  s_check_node_ (ddm, AR_DD_ENTHDD(sons,i), depth-1, file, line);
	}
    }
  else
    {
      for(i = 0; i < arity; i++)
	{
	  ccl_assert (AR_DD_REFCOUNT (AR_DD_CNTHDD(sons,i)) > 0);
	  if ( (! is_trivial(ddm,AR_DD_CNTHDD(sons,i))))
	    {
	      if (AR_DD_INDEX (AR_DD_CNTHDD(sons,i)) <= index)
		{
		  fprintf (stderr, "%s:%d: bad DD structuration\n", file, line);
		  abort ();
		}
	    }
	  s_check_node_ (ddm, AR_DD_CNTHDD(sons,i), depth-1, file, line);
	}
    }
}

			/* --------------- */

void
s_check_node_ (ar_ddm ddm, ar_dd N, int depth, const char *file, int line)
{
  if (N == ddm->one || N == ddm->zero) 
    return;
  ccl_assert (AR_DD_FLAG (N) == 0);
  if (AR_DD_CODE (N) == COMPACTED)
    {
      int i;

      for (i = 0; i < AR_DD_ARITY (N); i++)
	{
	  int min = AR_DD_MIN_OFFSET_NTHSON (N, i);
	  int max = AR_DD_MAX_OFFSET_NTHSON (N, i);
	  ccl_assert (min <= max);
	}
    }

  s_check_ite_ (ddm, AR_DD_INDEX (N), AR_DD_ARITY (N), AR_DD_CODE (N), 
	       AR_DD_SONS (N), depth, file, line);
}
# define s_check_node(_ddm, _N, _depth) \
  s_check_node_ (_ddm, _N, _depth, __FILE__, __LINE__) 
# define s_check_ite(_ddm, _index, _arity, _encoding, _sons, _depth)  \
  s_check_ite_ (_ddm, _index, _arity, _encoding, _sons, _depth, __FILE__, __LINE__)

#else /*! DD_CHECKING_ENABLED */
# define s_check_node(_ddm, _N, _depth) ((void)0)
# define s_check_ite(_ddm, _index, _arity, _encoding, _sons, _depth) ((void)0)
#endif /*! DD_CHECKING_ENABLED */

			/* --------------- */

ar_dd
ar_dd_var_in_range (ar_ddm ddm, int index, int arity, ar_ddcode encoding, 
		    int min, int max)
{
  int i;
  ar_dd R;
  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

  for (i = 0; i < arity; i++)
    {
      ar_dd val;

      if (min <= i && i <= max)
	val = ar_dd_dup_one (ddm);
      else
	val = ar_dd_dup_zero (ddm);
	  
      if (encoding == EXHAUSTIVE)
	AR_DD_ENTHDD (sons, i) = val;
      else
	{
	  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_CNTHDD (sons, i) = val;
	}
    }

  R = ar_dd_find_or_add (ddm, index, arity, encoding, sons);
  ar_dd_free_dd_table (ddm, arity, encoding, sons);

  s_check_node (ddm, R, CHECK_DEPTH);

  return R;
}

#if 0
static void
s_check_node_isnt_free (ar_ddm ddm, ar_dd dd)
{
  ar_dd  R;
  dd = AR_DD_ADDR (dd);
  for (R = ddm->pm_first_free; R != NULL; R = AR_DD_NEXT (R))
    ccl_assert (! (AR_DD_ADDR (R) == dd));
}
#endif

			/* --------------- */

static void
s_update_statistics_about_the_decision_diagram_hashtable(ar_ddm ddm);

static ar_dd
s_allocate_decision_diagram (ar_ddm ddm, int index, int arity, 
			     ar_ddcode encoding, ar_ddtable sons)
{
  ar_dd  R = NULL;
  int    n;

  ccl_pre (arity <= AR_DD_MAX_ARITY);
  s_check_ite (ddm, index, arity, encoding, sons, CHECK_DEPTH);

  if (ddm->pm_first_free == NULL)
    {
      int round = 0;

      do 
	{
	  if (round == 0 || round == 2)
	    s_create_new_dd_page (ddm);
	  else if (round == 1)
	    ar_dd_garbage_collection (ddm, 1);
	  else if (round == 3)
	    ccl_throw_no_msg (memory_exhausted_exception);
	  round++;
	}
      while (ddm->pm_first_free == NULL);
    }

  R = ddm->pm_first_free;
  ddm->pm_first_free = AR_DD_NEXT (R); 
  ddm->pm_number_of_free_nodes--;  

  AR_DD_REFCOUNT (R) = 1;
  if (arity > 0)
    AR_DD_SONS (R) = ar_dd_allocate_dd_table (ddm, arity, encoding);
  else 
    AR_DD_SONS (R) = NULL;
  AR_DD_ARITY (R) = arity;
  AR_DD_INDEX (R) = index;
  AR_DD_CODE (R)  = encoding;
  AR_DD_FLAG (R)  = 0;
  AR_DD_NEXT (R)  = NULL;

  if (encoding == EXHAUSTIVE) 
    {
      for (n = 0; n < arity; n++)
	AR_DD_ENTHSON (R, n) = AR_DD_DUP (AR_DD_ENTHDD (sons, n));
    }
  else
    {
      for (n = 0; n < arity; n++)
	{
	  AR_DD_MIN_OFFSET_NTHSON (R, n) = AR_DD_MIN_OFFSET_NTHDD (sons, n);
	  AR_DD_MAX_OFFSET_NTHSON (R, n) = AR_DD_MAX_OFFSET_NTHDD (sons, n);
	  AR_DD_CNTHSON (R, n) = AR_DD_DUP (AR_DD_CNTHDD (sons, n));
	}
    }

  s_check_node (ddm, R, CHECK_DEPTH);
    
  return R;
}

			/* --------------- */

static void
s_reinitialize_page_manager (ar_ddm ddm)
{
  dd_node_page a_page;
  
  a_page = ddm->pm_first_page;

  while (a_page != NULL)
    {
      ddm->pm_first_page = a_page->prev;
      DD_FREE (a_page->page);
      DD_FREE (a_page);
      a_page = ddm->pm_first_page;
    }
  ddm->pm_number_of_pages = 0;
  ddm->pm_first_free = NULL;
  ddm->pm_number_of_allocated_nodes = 0;
  ddm->pm_number_of_free_nodes = 0;
  s_create_new_dd_page (ddm);
  ddm->one = s_allocate_decision_diagram (ddm, 0, 0, EXHAUSTIVE, NULL);
  ddm->zero = AR_DD_DUP_NOT (ddm->one);
}

			/* --------------- */
#ifndef PAGE_SIZE
# if HAVE_SYSCONF_PAGESIZE
#  include <unistd.h>
#  define PAGE_SIZE (sysconf (_SC_PAGESIZE)-sizeof(struct dd_node_page))
# else
#  define PAGE_SIZE 1024
# endif /* HAVE_SYSCONF_PAGESIZE */
#endif /* PAGE_SIZE */

static void
s_create_page_manager(ar_ddm ddm, long page_size)
{
  ddm->pm_first_page = NULL;
  ddm->pm_current_page_size = 
    PAGE_SIZE / sizeof (struct ar_decision_diagram_st);
  s_reinitialize_page_manager(ddm);
}

			/* --------------- */

static void
s_delete_page_manager(ar_ddm ddm)
{
  dd_node_page a_page = ddm->pm_first_page;
  
  while( a_page != NULL )
    {
      ddm->pm_first_page = a_page->prev;
      DD_FREE(a_page->page);
      DD_FREE(a_page);
      a_page = ddm->pm_first_page;
    }
}

			/* --------------- */

static void
s_collect_decision_diagram_node(ar_ddm ddm, ar_dd F)
{
  s_collect_dd_table (ddm, AR_DD_ARITY (F), AR_DD_CODE (F),AR_DD_SONS (F));

  AR_DD_INDEX (F) = 0;
  AR_DD_FLAG (F) = 0;
  AR_DD_NEXT (F) = ddm->pm_first_free;

  ddm->pm_first_free = F;
  ddm->pm_number_of_free_nodes++;
}


/*
 *
 * OPERATIONS ON THE HASHTABLE OF NODES
 *
 */

static void
s_reinitialize_hashtable(ar_ddm ddm)
{
  long n;
  
  for( n = 0; n<ddm->ht_actual_size; n++) ddm->ht_nodes[n] = NULL;
  ddm->ht_number_of_active_nodes=1;
  ddm->ht_actual_ratio= (float)1.0/(float)ddm->ht_actual_size;
  ddm->ht_number_of_insertions = 0;
}

			/* --------------- */

static void
s_create_hashtable(ar_ddm ddm,
		   long minimum_size, long maximum_size,
		   float enlargement_ratio, float reduction_ratio)
{
  ddm->ht_nodes             = DD_CALLOC(ar_dd,minimum_size);
  ddm->ht_actual_size       = minimum_size;
  ddm->ht_minimum_size      = minimum_size;
  ddm->ht_maximum_size      = maximum_size;
  ddm->ht_enlargement_ratio = enlargement_ratio;
  ddm->ht_reduction_ratio   = reduction_ratio;    
  ddm->ht_number_of_insertions = 0;
  s_reinitialize_hashtable(ddm);
}

			/* --------------- */

static void
s_delete_hashtable(ar_ddm ddm)
{
  if( ddm->ht_nodes != NULL )
    {
      
      DD_FREE(ddm->ht_nodes);
      ddm->ht_nodes = NULL;
    }
}

			/* --------------- */

static uintptr_t
s_hashcode_decision_diagram(ar_ddm ddm, 
			    int index, int size, ar_ddtable table)
{
  uintptr_t  hashcode, address;
  int i;

  hashcode = (2 * size + 1) * index * DD_P1;
  for( i = 0; i < size; i++)
    {
      address = (uintptr_t) AR_DD_ENTHDD (table, i);
      /* hashcode += address * (2 * i + 1); */
      hashcode = (hashcode + address * (2 * i + 1)) * DD_P4;
    }

  hashcode %= ddm->ht_actual_size;

  return hashcode;
}

			/* --------------- */

int
ar_dd_resize_hashtable(ar_ddm ddm, long new_size)
{
  ar_ddtable old_hashtable;
  ar_dd      F, G;
  long         old_size, hashcode, n, number_of_active_nodes;
  int          memory_error = 0;

  if( new_size == ddm->ht_actual_size) 
    goto tr;

  if( new_size>ddm->ht_maximum_size || new_size<ddm->ht_minimum_size)
    goto tr;

  *&old_hashtable = ddm->ht_nodes;
  ddm->ht_nodes = DD_CALLOC(ar_dd,new_size);    
  old_size=ddm->ht_actual_size;
  number_of_active_nodes=ddm->ht_number_of_active_nodes;
  ddm->ht_actual_size=new_size;
  s_reinitialize_hashtable(ddm);
  
  ddm->ht_number_of_active_nodes = 0;
  for( n = 0; n < old_size; n++) 
    {
      F = old_hashtable[n];
      while( F!=NULL ) 
	{
	  hashcode = 
	    s_hashcode_decision_diagram(ddm,AR_DD_INDEX(F),
					AR_DD_TABLE_SIZE(AR_DD_ARITY(F),
							 AR_DD_CODE(F)),
					AR_DD_SONS(F));
	  if (AR_DD_REFCOUNT (F) > 0)
	    ddm->ht_number_of_active_nodes++;
	  G = AR_DD_NEXT(F);
	  AR_DD_NEXT(F) = ddm->ht_nodes[hashcode];
	  ddm->ht_nodes[hashcode] = F;
	  F = G;
	}
      }    
  DD_FREE(old_hashtable);
  /*  ddm->ht_number_of_active_nodes = number_of_active_nodes; */
  ddm->ht_actual_ratio = 
    (float)number_of_active_nodes/(float) ddm->ht_actual_size;
  ddm->ht_size_longest_collision_chain = 2;
 tr: /* -- Table Resized */;

  return ! memory_error;
}

			/* --------------- */

static void
s_garbage_collection_on_unreferenced_decision_diagrams (ar_ddm ddm)
{
  ar_dd F, *P;
  long n;

  /*
   * First pass : the reference counts are decremented.
   */
  for (n = 0; n < ddm->ht_actual_size; n++) 
    {
      F = ddm->ht_nodes[n];
      while (F != NULL) 
	{
	  ccl_assert (AR_DD_REFCOUNT (F) >= 0);
	  if (AR_DD_REFCOUNT (F) == 0 && ! AR_DD_FLAG (F))
	    s_update_reference_count (F);
	  F = AR_DD_NEXT (F);
	}
    }

  /*
   * Second pass : the freed nodes are extracted from their
   * collision chain and put in the free list.
   */
  ddm->ht_size_longest_collision_chain = 0;
  for (n = 0; n < ddm->ht_actual_size; n++) 
    {
      int len = 0;
      P = ddm->ht_nodes+n;
      while (*P != NULL)
	{
	  if (AR_DD_REFCOUNT (*P) == 0)
	    {
	      F = *P;
	      *P = AR_DD_NEXT (F);
	      s_collect_decision_diagram_node (ddm, F);
	      ddm->ht_number_of_active_nodes--;
	    }
	  else 
	    {
	      P = &AR_DD_NEXT (*P);
	      len++;
	    }
	}
      if (len > ddm->ht_size_longest_collision_chain)
	ddm->ht_size_longest_collision_chain = len;
    }

  ddm->ht_actual_ratio = 
    (float) ddm->ht_number_of_active_nodes / (float) ddm->ht_actual_size;
}

			/* --------------- */

static void
s_update_statistics_about_the_decision_diagram_hashtable(ar_ddm ddm)
{
  ar_dd F;
  long    n;
  long    length;

  ddm->ht_number_of_active_nodes        = 0;
  ddm->ht_number_of_empty_entries       = 0;
  ddm->ht_number_of_collisions          = 0;
  ddm->ht_size_longest_collision_chain  = 0;
  ddm->ht_entry_longest_collision_chain = 0;
  for(n = 0; n < ddm->ht_actual_size; n++) 
    {
      F = ddm->ht_nodes[n];
      if( F == NULL ) ddm->ht_number_of_empty_entries++;
      else 
	{
	  length = 0;
	  while( F != NULL )
	    {
	      if (AR_DD_REFCOUNT (F))
		ddm->ht_number_of_active_nodes++;
	      ddm->ht_number_of_collisions++;
	      length++;
	      F = AR_DD_NEXT(F);
	    }
	  if( length > ddm->ht_size_longest_collision_chain )
	    {
	      ddm->ht_size_longest_collision_chain  = length;
	      ddm->ht_entry_longest_collision_chain = n;
	    }
	}
    }
}
			/* --------------- */

ar_dd
ar_dd_find_or_add (ar_ddm ddm, int index, int arity, ar_ddcode encoding,
		   ar_ddtable sons)
{
  ar_dd R, *F;
  unsigned long hashcode;
  int i, actual_arity = 0, polarity = 0;
  int len;

  ccl_pre (0 <= index && index < 0x7FFF);
  ccl_pre (0 <= arity && arity <= AR_DD_MAX_ARITY);
  s_check_ite (ddm, index, arity, encoding, sons, CHECK_DEPTH);

  if (encoding == EXHAUSTIVE)
    {
      if (s_are_all_identical_in_table (arity, sons)) 
	{
	  R = AR_DD_DUP (AR_DD_ENTHDD (sons, 0));
	  goto na;
	}
      else 
	{
	  polarity = AR_DD_POLARITY (AR_DD_ENTHDD (sons, 0));
	  if (polarity)
	    {
	      for (i = 0; i < arity; i++) 
		AR_DD_ENTHDD (sons, i) = AR_DD_NOT (AR_DD_ENTHDD (sons, i));
	    }
	  actual_arity = arity;
	}
    }
  else
    {
      actual_arity = s_compact_decision_diagram_table (arity, sons);

      if (actual_arity == 1)
	{
	  R = AR_DD_DUP (AR_DD_CNTHDD (sons, 0));
	  goto na;
	}
      else
	{
	  polarity = AR_DD_POLARITY (AR_DD_CNTHDD (sons, 0));
	  if (polarity)
	    {
	      for (i = 0; i < actual_arity; i++)
		AR_DD_CNTHDD (sons, i) = AR_DD_NOT (AR_DD_CNTHDD (sons, i));
	    }
	}
    }
  hashcode = 
    s_hashcode_decision_diagram(ddm, index,
				AR_DD_TABLE_SIZE (actual_arity, encoding),
				sons);

  F = ddm->ht_nodes+hashcode;
  R = NULL;
  len = 0;

  while (*F != NULL && R == NULL)
    {
      if (AR_DD_INDEX(*F) == index && (AR_DD_CODE(*F) == encoding) &&
	  s_are_tables_identical (actual_arity, encoding, sons, AR_DD_SONS(*F)))
	R = AR_DD_DUP (*F);
      else 
	F = &AR_DD_NEXT (*F);
      len++;
    }
  if (len > ddm->ht_size_longest_collision_chain)
    ddm->ht_size_longest_collision_chain = len;

  if(R == NULL)
    {
      R = 
	s_allocate_decision_diagram (ddm, index, actual_arity, encoding, sons);

      hashcode =  
	s_hashcode_decision_diagram (ddm, index,
				     AR_DD_TABLE_SIZE (actual_arity, 
						       encoding),
				     sons);
      
      AR_DD_NEXT (R) = ddm->ht_nodes[hashcode];
      ddm->ht_nodes[hashcode] = R;
      ddm->ht_number_of_active_nodes++;
      ddm->ht_number_of_insertions++;
      ddm->ht_actual_ratio =
        (float) ddm->ht_number_of_active_nodes / (float) ddm->ht_actual_size;
      if (ddm->ht_number_of_insertions > NB_INSERTIONS_BEFORE_GC)
	ar_dd_garbage_collection (ddm, 1);
      if(ddm->ht_actual_ratio > ddm->ht_enlargement_ratio)
	{
	  if (ddm->ht_size_longest_collision_chain >= 20)
	    ar_dd_garbage_collection (ddm, 1);

	  if(ddm->ht_actual_ratio > ddm->ht_enlargement_ratio)
	    ar_dd_enlarge_hashtables (ddm);
	}
    }

  if (polarity) 
    R = AR_DD_NOT (R);

  na : /* -- Node allocated */ ;

  s_check_node (ddm, R, CHECK_DEPTH);

  return R;
}

/*
 *
 * OPERATIONS ON THE OPERATION CACHE
 *
 */

static void
s_reinitialize_cache(ar_ddm ddm)
{
  ccl_memzero (ddm->op_cache, sizeof (struct memo_record) * ddm->op_actual_size);
}

		    /* --------------- */

static void
s_create_cache(ar_ddm ddm, long minimum_size, long maximum_size)
{
  ddm->op_cache = DD_CALLOC(struct memo_record,minimum_size);
  ddm->op_actual_size=minimum_size;
  ddm->op_minimum_size=minimum_size;
  ddm->op_maximum_size=maximum_size;
  s_reinitialize_cache(ddm);
}

			/* --------------- */

static void
s_delete_cache(ar_ddm ddm)
{
  if( ddm->op_cache != NULL ) 
    {
      DD_FREE(ddm->op_cache);
      ddm->op_cache = NULL;
    }
}

			/* --------------- */

static void
s_garbage_collection_on_memoization_records (ar_ddm ddm)
{
  long n;
  memo_record rec;

  for (rec = ddm->op_cache, n = 0; n < ddm->op_actual_size; rec++, n++)
    {
      if (IF_SON(rec) != NULL && 
	  (AR_DD_POLARITY (IF_SON (rec)) || 
	   (AR_DD_REFCOUNT (IF_SON (rec))==0) || 
	   (AR_DD_REFCOUNT (THEN_SON (rec))==0) || 
	   (AR_DD_REFCOUNT (ELSE_SON (rec))==0) ||
	   (AR_DD_REFCOUNT (RESULT_SON (rec))==0)))
	{
	  IF_SON (rec)  = NULL;
	  THEN_SON (rec) = NULL;
	  ELSE_SON (rec) = NULL;
	  RESULT_SON (rec) = NULL;
	}
    }
}

			/* --------------- */

static void
s_update_statistics_about_cache(ar_ddm ddm)
{
  long        n;
  memo_record rec;

  ddm->op_number_of_stored_records=0;
  for(rec = ddm->op_cache, n = 0; n < ddm->op_actual_size; n++, rec++)
    if( IF_SON(rec) != NULL )
      ddm->op_number_of_stored_records++;
}

			/* --------------- */

static uintptr_t 
s_ite_hashcode(ar_ddm ddm, void* F, void* G, void* H)
{
  uintptr_t hashcode;

  hashcode = ((uintptr_t) F) * DD_P1;
  hashcode = (hashcode + ((uintptr_t) G)) * DD_P2;
  hashcode = (hashcode + ((uintptr_t) H)) * DD_P3;
  hashcode %= ddm->op_actual_size;
  
  return hashcode;
}

			/* --------------- */

static void
s_memoize_ite(ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H, ar_dd R)
{
  uintptr_t hashcode;
  memo_record rec;

  ccl_pre(!AR_DD_POLARITY(F) && ! AR_DD_POLARITY(G));

  hashcode = s_ite_hashcode(ddm,F,G,H);
  rec = ddm->op_cache+hashcode;
  IF_SON(rec) = F;
  THEN_SON(rec) = G;
  ELSE_SON(rec) = H;
  RESULT_SON(rec) = R;
}

			/* --------------- */

static ar_dd
s_entry_for_ite(ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H)
{
  uintptr_t hashcode;
  memo_record record;
  ar_dd R;

  ccl_pre (!AR_DD_POLARITY (F) && ! AR_DD_POLARITY (G));

  hashcode = s_ite_hashcode (ddm, F, G, H);
  record = ddm->op_cache + hashcode;  
  if (IF_SON (record) == F && THEN_SON (record) == G && ELSE_SON (record) == H)
    {
      R = RESULT_SON (record);
      s_check_node (ddm, R, CHECK_DEPTH);
    }
  else
    R = NULL;

  return R;
}
			/* --------------- */

void
ar_dd_remove_foreign_memoization_records(ar_ddm ddm)
{
  long        n;
  memo_record rec;

  for(rec = ddm->op_cache, n = 0; n < ddm->op_actual_size; rec++, n++) 
    {
      if( ! AR_DD_POLARITY(IF_SON(rec))) continue;
      IF_SON(rec) = NULL;
      THEN_SON(rec) = NULL;
      ELSE_SON(rec) = NULL;
      RESULT_SON(rec) = NULL;
    }
}

			/* --------------- */

int
ar_dd_resize_cache(ar_ddm ddm, long new_size)
{
  int         memory_exhausted = 0;
  memo_record old_table;
  memo_record record;
  long        old_size;
  long        n;

  *&old_table = NULL;
  if( new_size == ddm->op_actual_size ) 
    goto tr;
      
  if( new_size > ddm->op_maximum_size+100 
      || new_size < ddm->op_minimum_size-100 )
    goto tr;
    
  old_table = ddm->op_cache;
  ddm->op_cache = DD_CALLOC(struct memo_record,new_size);    
  old_size = ddm->op_actual_size;
  ddm->op_actual_size = new_size;    
  s_reinitialize_cache(ddm);

  for( record = old_table, n = 0; n < old_size; n++, record++) 
    if( IF_SON(record) != NULL )
      {
	if( AR_DD_POLARITY(IF_SON(record)) )
	  ar_dd_memoize_double_operation(ddm,
					 AR_DD_ADDR(FIRST_OPERAND(record)),
					 SECOND_OPERAND(record),
					 THIRD_OPERAND(record),
					 OPERATION_DOUBLE_RESULT(record));
	else 
	  s_memoize_ite(ddm,
			IF_SON(record),THEN_SON(record),ELSE_SON(record),
			RESULT_SON(record));
      }
  DD_FREE(old_table);
    
 tr: /* Table Resized. */ ;

  return !memory_exhausted;
}

			/* --------------- */

static memo_record 
s_record_for_operation (ar_ddm ddm, void *F, void *G, void *H)
{
  uintptr_t hashcode = s_ite_hashcode(ddm,F,G,H);

  return ddm->op_cache + hashcode;
}

			/* --------------- */

void
ar_dd_memoize_operation(ar_ddm ddm, void* F, void* G, void* H, void* R)
{
  memo_record record = s_record_for_operation (ddm, F, G, H);
  ar_dd Fp = AR_DD_PADDR(F,1);

  FIRST_OPERAND(record)    = Fp;
  SECOND_OPERAND(record)   = G;
  THIRD_OPERAND(record)    = H;
  OPERATION_RESULT(record) = R;
}

			/* --------------- */

void
ar_dd_memoize_double_operation (ar_ddm ddm, void *F, void *G, void *H, 
				double R)
{
  memo_record record = s_record_for_operation (ddm, F, G, H);
  ar_dd Fp = AR_DD_PADDR (F, 1);

  FIRST_OPERAND (record) = Fp;
  SECOND_OPERAND (record) = G;
  THIRD_OPERAND (record) = H;
  OPERATION_DOUBLE_RESULT (record) = R;
}

			/* --------------- */

static memo_record 
s_get_record_for_operation (ar_ddm ddm, void* F, void* G, void* H)
{
  memo_record record = s_record_for_operation (ddm, F, G, H);
  ar_dd Fp = AR_DD_PADDR (F, 1);

  if (FIRST_OPERAND (record) == Fp && SECOND_OPERAND (record) == G && 
      THIRD_OPERAND (record) == H)
    return record;
  return NULL;
}

			/* --------------- */

void *
ar_dd_entry_for_operation (ar_ddm ddm, void* F, void* G, void* H)
{
  void  *R = NULL;
  memo_record record = s_get_record_for_operation (ddm, F, G, H);

  if (record != NULL)
    {
      R = OPERATION_RESULT (record);
      s_check_node (ddm, R, CHECK_DEPTH);
    }

  return R;
}

/*
 *
 * OPERATIONS ON THE DECISION DIAGRAM MANAGER
 *
 */

#define DIVISOR(n,p)   ((p/n)*n==p)

static int 
s_is_prime(long p)
{
  int  found;
  long n;

  found = DIVISOR(2,p);
  for(n=3;(!found)&&(n*n<=p);n+=4)
    found = DIVISOR(n,p);
 
  return(! found);
}

			/* --------------- */

static long 
s_nearest_prime(long basic_size)
{
  long    n, p;
  int found;
  if ((basic_size  % 2) == 0)
    basic_size++;

  for (n=2,found=0;!found;n+=2) {
    p=basic_size+n;
    found= s_is_prime(p);
    if (!found) {
      p=basic_size-n;
      found= s_is_prime(p);
      }
    }

 return(p);
}

			/* --------------- */

static void
s_memory_exhaustion_handler (void *data)
{
  ccl_debug ("Garbage before exhaustion\n");
  ar_dd_garbage_collection (data, 1);
  ccl_debug ("Garbage before exhaustion done\n");
}

			/* --------------- */

ar_ddm
ar_dd_create_manager(void)
{
  long new_hashtable_size;
  long new_hashcache_size;
  ar_ddm result = ccl_new(struct ar_decision_diagram_manager_st);
  
  new_hashtable_size = s_nearest_prime(MINIMUM_HASHTABLE_SIZE);
  new_hashcache_size = s_nearest_prime(MINIMUM_HASHCACHE_SIZE);

  s_create_page_manager(result,DECISION_DIAGRAM_PAGE_SIZE);
  s_create_table_manager(result,DECISION_DIAGRAM_TABLE_PAGE_SIZE);
  s_create_hashtable(result,new_hashtable_size,MAXIMUM_HASHTABLE_SIZE,
		     ENLARGEMENT_RATIO,REDUCTION_RATIO);
  s_create_cache(result, new_hashcache_size,MAXIMUM_HASHCACHE_SIZE);
  ar_dd_set_garbage_collection_status (result, 1);

  ccl_memory_add_exhaustion_handler (s_memory_exhaustion_handler, result);

  return result;
}

			/* --------------- */

void
ar_dd_delete_manager(ar_ddm ddm)
{
#if CCL_ENABLE_ASSERTIONS  
  ar_dd_garbage_collection (ddm, 0);
  s_update_statistics_about_the_decision_diagram_hashtable (ddm);
  ccl_assert (ddm->ht_number_of_active_nodes == 0);
  ccl_assert (ddm->ht_number_of_empty_entries == ddm->ht_actual_size);
#endif

#if DD_CHECKING_ENABLED
  ar_dd_display_statistics_about_decision_diagrams(ddm, CCL_LOG_DEBUG);
#endif
  s_delete_page_manager (ddm);
  s_delete_table_manager (ddm);
  s_delete_hashtable (ddm);
  s_delete_cache (ddm);
  ccl_memory_del_exhaustion_handler (s_memory_exhaustion_handler, ddm);

  DD_FREE (ddm);
}

			/* --------------- */

int
ar_dd_reset_manager(ar_ddm ddm)
{
  s_reinitialize_page_manager(ddm);
  s_reinitialize_table_manager(ddm);
  s_reinitialize_hashtable(ddm);
  s_reinitialize_cache(ddm);

  return 1;
}

			/* --------------- */

int
ar_dd_enlarge_hashtables(ar_ddm ddm)
{  
  int result;
  long new_hashtable_size = s_nearest_prime (2*ddm->ht_actual_size);
  long new_hashcache_size = s_nearest_prime (2*ddm->op_actual_size);

  result = (ar_dd_resize_hashtable(ddm,new_hashtable_size) &&
	    ar_dd_resize_cache(ddm,new_hashcache_size));

  return result;
}

			/* --------------- */

int
ar_dd_reduce_hashtables(ar_ddm ddm)
{
  long new_hashtable_size = s_nearest_prime (ddm->ht_actual_size/2);
  long new_hashcache_size = s_nearest_prime (ddm->op_actual_size/2);
  return (ar_dd_resize_hashtable(ddm,new_hashtable_size) ||
	  ar_dd_resize_cache(ddm,new_hashcache_size));
}

			/* --------------- */

static int
s_page_is_removable (dd_node_page P)
{
  int i;
  int result = 1;

  for (i = 0; result && i < P->size; i++)
    {
      ar_dd F = P->page + i;

      result = (AR_DD_REFCOUNT (F) == 0);
    }

  return result;
}

			/* --------------- */

static void
s_clear_empty_pages (ar_ddm ddm)
{
  size_t nb_rem = 0;
  dd_node_page to_remove = NULL;
  dd_node_page *ppage = &ddm->pm_first_page;

  while (*ppage)
    {
      if (s_page_is_removable (*ppage))
	{
	  int i;
	  dd_node_page P = *ppage;

	  for (i = 0; i < P->size; i++)
	    {
	      ar_dd F = P->page + i;
	      F->flag = 1;
	    }

	  *ppage = P->prev;
	  P->prev = to_remove;
	  to_remove = P;
	}
      else
	{
	  ppage = &((*ppage)->prev);
	}
    }

  if (to_remove != NULL)
    {
      ar_dd *pn = &ddm->pm_first_free;

      while (*pn)
	{
	  ar_dd n = *pn;
	  if (AR_DD_FLAG (n))
	    *pn = AR_DD_NEXT (n);
	  else
	    pn = &(AR_DD_NEXT (n));
	}

      while (to_remove)
	{
	  dd_node_page prev = to_remove->prev;
	  
	  nb_rem += to_remove->size * sizeof (struct ar_decision_diagram_st);
	  ddm->pm_number_of_pages--;
	  ddm->pm_number_of_allocated_nodes -= to_remove->size;
	  ddm->pm_number_of_free_nodes -= to_remove->size;
	  DD_FREE (to_remove->page);
	  DD_FREE (to_remove);
	  to_remove = prev;
	}
    }
  if (ccl_debug_is_on)
    {
      double f_nb_rem = nb_rem;
      const char *unit = "bytes";
      if (nb_rem > (1 << 30)) { unit = "Go"; f_nb_rem /= (1<< 30);}
      else if (nb_rem > (1 << 20)) { unit = "Mo"; f_nb_rem /= (1 << 20); }
      else if (nb_rem > (1 << 10)) { unit = "ko"; f_nb_rem /= (1 << 10); }
            
      ccl_debug ("suppress %.2f %s from DD pages\n", f_nb_rem, unit);
    }
     
}

			/* --------------- */

#define RESIZE_TABLES 0
static void
s_garbage_collection(ar_ddm ddm)
{
  int i;

#if RESIZE_TABLES
  int  reducible;
  long old_hashtable_size;
  int  old_hashcache_size;
#endif

  s_garbage_collection_on_unreferenced_decision_diagrams(ddm);
  s_garbage_collection_on_memoization_records(ddm);
  ar_dd_remove_foreign_memoization_records(ddm);
  s_clear_empty_pages (ddm);

  for (i = 0; i < ddm->tm_max_table_size; i++)
    {
      if (ddm->tm_pools_of_tables[i] != NULL)
	ccl_pool_collect (ddm->tm_pools_of_tables[i]);
    }
#if RESIZE_TABLES
  reducible = 1;
  while( reducible )
    {
      reducible = (ddm->ht_actual_ratio < ddm->ht_reduction_ratio);
      if( reducible ) {
	old_hashtable_size = ddm->ht_actual_size;
	old_hashcache_size = ddm->op_actual_size;
	reducible = ar_dd_reduce_hashtables(ddm);
	if( reducible ) 
	  reducible = ((old_hashtable_size != ddm->ht_actual_size)
		       && (old_hashcache_size != ddm->op_actual_size));
      }
    }
#endif
}

			/* --------------- */

void
ar_dd_garbage_collection(ar_ddm ddm, int show_info)
{
  if (ddm->gc_enabled)
    {
      int nb_free_nodes = ddm->pm_number_of_free_nodes;
      ddm->ht_number_of_insertions = 0;
      s_garbage_collection(ddm);
      if (ccl_debug_is_on || show_info)
	{
	  s_update_statistics_about_the_decision_diagram_hashtable (ddm);
	  ccl_warning ("GC: collect %d /%d nodes (%.2f%%)\n", 
		       ddm->pm_number_of_free_nodes - nb_free_nodes,
		       ddm->pm_number_of_allocated_nodes,
		       100.0*((float)(ddm->pm_number_of_free_nodes -
				      nb_free_nodes)/
			      (float)ddm->pm_number_of_allocated_nodes));
	  ccl_warning ("GC: %d free nodes\n", ddm->pm_number_of_free_nodes);
	  ccl_warning ("GC: size of longest collision chain %d\n", 
		       ddm->ht_size_longest_collision_chain);
	}
    }
}


			/* --------------- */

int
ar_dd_get_garbage_collection_status (ar_ddm ddm)
{
  return ddm->gc_enabled;
}

			/* --------------- */

void
ar_dd_set_garbage_collection_status (ar_ddm ddm, int on)
{
  ddm->gc_enabled = on;
}

			/* --------------- */

ar_dd
ar_dd_one(ar_ddm ddm)
{
  return ddm->one;
}

			/* --------------- */

ar_dd
ar_dd_dup_one (ar_ddm ddm)
{
  return AR_DD_DUP (ddm->one);
}

			/* --------------- */
ar_dd
ar_dd_zero(ar_ddm ddm)
{
  return ddm->zero;
}

			/* --------------- */

ar_dd
ar_dd_dup_zero (ar_ddm ddm)
{
  return AR_DD_DUP (ddm->zero);
}

			/* --------------- */

int
ar_dd_is_one (ar_ddm ddm, ar_dd dd)
{
  return ddm->one == dd;
}
			/* --------------- */

int
ar_dd_is_zero (ar_ddm ddm, ar_dd dd)
{
  return ddm->zero == dd;
}

			/* --------------- */

/*
 *
 * BOOLEAN OPERATORS ON DDs
 *
 */
static ar_dd 
s_dd_compute_ite (ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H, int D);

static ar_dd
s_compute_compacted_ite (ar_ddm ddm,
			 short isFtop, ar_dd F, 
			 short isGtop, ar_dd G, 
			 short isHtop, ar_dd H, int D)
{
  short        Farity, Garity, Harity, Rarity;
  int        i,j,k,l, current_minimum,current_maximum, top_index;
  ar_dd      R;
  ar_ddtable Rsons;

  s_check_node (ddm, F, CHECK_DEPTH);
  s_check_node (ddm, G, CHECK_DEPTH);
  s_check_node (ddm, H, CHECK_DEPTH);
  /*
   * First : determining the arity of R.
   */
  Farity = (isFtop ? AR_DD_ARITY (F) : 0);
  Garity = (isGtop ? AR_DD_ARITY (G) : 0);
  Harity = (isHtop ? AR_DD_ARITY (H) : 0);
  current_maximum = 0;
  Rarity = 0;
  for (i = 0, j = 0, k = 0; i < Farity || j < Garity || k < Harity ; Rarity++) 
    {
      current_maximum = SHRT_MAX;
      if (isFtop && current_maximum > AR_DD_MAX_OFFSET_NTHSON (F, i))
	current_maximum = AR_DD_MAX_OFFSET_NTHSON (F,i);
      if (isGtop && current_maximum > AR_DD_MAX_OFFSET_NTHSON (G, j))
	current_maximum = AR_DD_MAX_OFFSET_NTHSON (G, j);
      if (isHtop && current_maximum > AR_DD_MAX_OFFSET_NTHSON (H, k))
	current_maximum = AR_DD_MAX_OFFSET_NTHSON (H, k);

      if (isFtop && current_maximum == AR_DD_MAX_OFFSET_NTHSON (F, i)) 
	i++;
      if (isGtop && current_maximum == AR_DD_MAX_OFFSET_NTHSON (G, j)) 
	j++;
      if (isHtop && current_maximum == AR_DD_MAX_OFFSET_NTHSON (H, k)) 
	k++;
    }
  
  Rsons = ar_dd_allocate_dd_table (ddm, Rarity, COMPACTED);

  if( isFtop ) 
    top_index = AR_DD_INDEX (F);
  else if( isGtop ) 
    top_index = AR_DD_INDEX (G);
  else 
    top_index = AR_DD_INDEX (H);


    /* -- H is the only DD whose polarity may be 1 */
  current_minimum = 0;
  for (i = 0, j = 0, k = 0, l = 0; l < Rarity ; l++)
    {
      current_maximum = SHRT_MAX;
      if (isFtop && current_maximum > AR_DD_MAX_OFFSET_NTHSON (F, i))
	{
	  ccl_assert (i < AR_DD_ARITY (F));
	  current_maximum = AR_DD_MAX_OFFSET_NTHSON (F, i);
	}
      if (isGtop && current_maximum > AR_DD_MAX_OFFSET_NTHSON (G, j))
	{
	  ccl_assert (j < AR_DD_ARITY (G));
	  current_maximum = AR_DD_MAX_OFFSET_NTHSON (G, j);
	}
      if (isHtop && current_maximum > AR_DD_MAX_OFFSET_NTHSON (H, k))
	{
	  ccl_assert (k < AR_DD_ARITY (H));
	  current_maximum = AR_DD_MAX_OFFSET_NTHSON (H, k);
	}

      ccl_assert (current_minimum <= current_maximum);
      AR_DD_MIN_OFFSET_NTHDD (Rsons, l) = current_minimum;
      AR_DD_MAX_OFFSET_NTHDD (Rsons, l) = current_maximum;
      AR_DD_CNTHDD (Rsons, l) = 
	s_dd_compute_ite(ddm,
			 (isFtop ? AR_DD_CNTHSON (F,i)  : F),
			 (isGtop ? AR_DD_CNTHSON (G,j)  : G),
			 (isHtop ? AR_DD_CPNTHSON (H,k) : H), D + 1);
      CHECK_REFCOUNT (AR_DD_CNTHDD (Rsons, l));
      ccl_assert (is_trivial (ddm, AR_DD_CNTHDD (Rsons, l)) || 
		  AR_DD_INDEX (AR_DD_CNTHDD (Rsons, l)) > top_index);

      if (isFtop && current_maximum == AR_DD_MAX_OFFSET_NTHSON (F, i))
	i++;
      if (isGtop && current_maximum == AR_DD_MAX_OFFSET_NTHSON (G, j)) 
	j++;
      if (isHtop && current_maximum == AR_DD_MAX_OFFSET_NTHSON (H, k)) 
	k++;
      current_minimum = current_maximum + 1;
    }
  
  R = ar_dd_find_or_add (ddm, top_index, Rarity, COMPACTED, Rsons);  
  ar_dd_free_dd_table (ddm, Rarity, COMPACTED, Rsons);
  s_check_node (ddm, R, CHECK_DEPTH);

  return R;
}

			/* --------------- */

static ar_dd
s_compute_normalized_ite (ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H, int D)
{
  ar_dd R = NULL, L;
  ar_ddtable Rsons;
  int i, arity;
  short isFtop, isGtop, isHtop;
  
  s_check_node (ddm, F, CHECK_DEPTH);
  s_check_node (ddm, G, CHECK_DEPTH);
  s_check_node (ddm, H, CHECK_DEPTH);

  ccl_pre (AR_DD_POLARITY (F) == 0 && AR_DD_POLARITY (G) == 0);
  ccl_pre (F != ddm->one && F != G && F != AR_DD_NOT (H));
  ccl_pre (G != H && ((G != ddm->one) || (H != ddm->zero)));

  isFtop = 1; 
  L = F;
  isGtop = (G != ddm->one && AR_DD_INDEX (F) >= AR_DD_INDEX (G));
  if (isGtop && AR_DD_INDEX (F) > AR_DD_INDEX (G)) 
    { 
      isFtop = 0; 
      L = G;
    }

  isHtop = (H != ddm->one && 
	    H != ddm->zero &&
	    (!isFtop || AR_DD_INDEX (F) >= AR_DD_INDEX (H)) &&
	    (!isGtop || AR_DD_INDEX (G) >= AR_DD_INDEX (H)));
  
  if (isHtop)
    {
      if (isFtop && AR_DD_INDEX (F) > AR_DD_INDEX (H)) 
	isFtop = 0;
      if (isGtop && AR_DD_INDEX (G) > AR_DD_INDEX (H)) 
	isGtop = 0;
      L = H;
    }
  
  if (AR_DD_CODE (L) == EXHAUSTIVE)
    {
      arity = AR_DD_ARITY (L);
      Rsons = ar_dd_allocate_dd_table (ddm, arity, EXHAUSTIVE);

      /* -- H is the only DD whose polarity may be 1 */
      for (i = 0; i<arity; i++)
	{
	  AR_DD_ENTHDD (Rsons, i) = 
	    s_dd_compute_ite (ddm,
			      (isFtop ? AR_DD_ENTHSON (F, i) : F),
			      (isGtop ? AR_DD_ENTHSON (G, i) : G),
			      (isHtop ? AR_DD_EPNTHSON (H, i) : H), D + 1);
	  CHECK_REFCOUNT (AR_DD_ENTHDD (Rsons,i));
	}
      R = ar_dd_find_or_add (ddm, AR_DD_INDEX (L), arity, EXHAUSTIVE, Rsons);

      ar_dd_free_dd_table (ddm, arity, EXHAUSTIVE, Rsons);
    }
  else
    {
      R = s_compute_compacted_ite (ddm, isFtop, F, isGtop, G, isHtop, H, D);
    }

  s_check_node (ddm, R, CHECK_DEPTH);

  return (R);
}

			/* --------------- */

static ar_dd 
s_dd_compute_ite (ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H, int D)
{
  ar_dd R, Fn, Gn, Hn;
  int   polarity;

  s_check_node (ddm, F, CHECK_DEPTH);
  s_check_node (ddm, G, CHECK_DEPTH);
  s_check_node (ddm, H, CHECK_DEPTH);
  
  if (F == ddm->one) 
    R = AR_DD_DUP (G);
  else if (F == ddm->zero) 
    R = AR_DD_DUP (H);
  else 
    {
      /*
       * the formula ite(F,G,H) is now transformed in ite(Fn,Gn,Hn)
       * in such a way that POLARITY(Fn)==0 and POLARITY(Gn)==0
       * polarity is the polarity of the result.
       */

      /* -- ite(~X,Y,Z) --> ite(X,Z,Y) */
      if (AR_DD_POLARITY(F)) 
	{ 
	  Fn = AR_DD_ADDR (F); 
	  Gn = H; 
	  Hn = G; 
	}
      else
	{ 
	  Fn = F;  
	  Gn = G; 
	  Hn = H;
	}

      /* -- ite(X,~Y,Z) --> ~ite(X,Y,~Z) */
      if (AR_DD_POLARITY (Gn)) 
	{ 
	  Gn = AR_DD_ADDR (Gn); 
	  Hn = AR_DD_NOT (Hn); 
	  polarity = 1;
	}
      else
	{ 
	  polarity =0; 
	}

      /*
       * additional trivial transformations
       */
      if (Fn == Gn) 
	Gn = ddm->one;  /* -- ite(X,X,Z) --> ite(X,1,Z) */

      if (Fn == Hn)
	Hn = ddm->zero;   /* -- ite(X,Y,X) --> ite(X,Y,O) */
      else if (Fn == AR_DD_NOT (Hn)) 
	Hn = ddm->one;  /* -- ite(X,Y,~X) --> ite(X,Y,1) */

      if (Gn == Hn) 
	R = AR_DD_DUP (Gn);         /* -- ite(X,Y,Y) --> Y */
      else if (Gn == ddm->one && Hn == ddm->zero)
	R = AR_DD_DUP (Fn);         /* -- ite(X,1,0) --> X */
      else if ((R = s_entry_for_ite (ddm, Fn, Gn, Hn)) == NULL) 
	{
	  /*
	   * the computation principle itself
	   */
	  R = s_compute_normalized_ite (ddm, Fn, Gn, Hn, D);

	  s_memoize_ite (ddm, Fn, Gn, Hn, R);
	}
      else 
	R = AR_DD_DUP (R);

      if (polarity) 
	R = AR_DD_NOT (R);
    }

  s_check_node (ddm, R, CHECK_DEPTH);

  return R;
}

			/* --------------- */

ar_dd 
ar_dd_dup_dd (ar_dd F)
{
  ccl_pre (F != NULL);
  return AR_DD_DUP (F);
}

void
ar_dd_free_dd (ar_dd F)
{
  ccl_pre (F != NULL);
  AR_DD_FREE (F);
}

ar_dd 
ar_dd_compute_ite (ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H)
{
  return s_dd_compute_ite (ddm, F, G, H, 0);
}

			/* --------------- */

ar_dd
ar_dd_compute_and (ar_ddm ddm, ar_dd F, ar_dd G)
{
  if (F > G) { ar_dd H = F; F = G; G = H; }
    
  return ar_dd_compute_ite (ddm, F, G, ddm->zero);
}

ar_dd
ar_dd_compute_and_3 (ar_ddm ddm, ar_dd F, ar_dd G, ar_dd H)
{
  ar_dd tmp = ar_dd_compute_and (ddm, F, G);
  ar_dd res = ar_dd_compute_and (ddm, tmp, H);
  AR_DD_FREE (tmp);

  return (res);
}

			/* --------------- */

ar_dd 
ar_dd_compute_or (ar_ddm ddm, ar_dd F, ar_dd G)
{
  if (F > G) { ar_dd H = F; F = G; G = H; }

  return ar_dd_compute_ite (ddm, F, ddm->one, G);
}

			/* --------------- */

ar_dd 
ar_dd_compute_xor(ar_ddm ddm, ar_dd F, ar_dd G)
{
  if (F > G) { ar_dd H = F; F = G; G = H; }

  return ar_dd_compute_ite(ddm,F,AR_DD_NOT(G),G);
}

			/* --------------- */

ar_dd 
ar_dd_compute_iff(ar_ddm ddm, ar_dd F, ar_dd G)
{
  if (F > G) { ar_dd H = F; F = G; G = H; }

  return ar_dd_compute_ite (ddm, F, G, AR_DD_NOT (G));
}

			/* --------------- */

ar_dd 
ar_dd_compute_imply(ar_ddm ddm, ar_dd F,ar_dd G)
{
  return ar_dd_compute_ite(ddm,F,G,ddm->one);
}

			/* --------------- */

ar_dd 
ar_dd_compute_diff (ar_ddm ddm, ar_dd F,ar_dd G)
{
  return ar_dd_compute_ite (ddm, G, ddm->zero, F);
}

			/* --------------- */

ar_dd
ar_dd_project_on_variable (ar_ddm ddm, ar_dd F, intptr_t v)
{
  ar_dd R = NULL;
  ar_dd plist = ar_dd_create_projection_list (ddm);
  ar_dd_projection_list_add (ddm, &plist, v, v);
  R = ar_dd_project_on_variables (ddm, F, plist);
  AR_DD_FREE (plist);

  return R;
}

			/* --------------- */

ar_dd
ar_dd_project_variable (ar_ddm ddm, ar_dd F, intptr_t v)
{
  ar_dd R = NULL;
  ar_dd U;
  ar_dd V;
  ar_ddtable Rsons;
  int i;
  int arity;
  
  if (F == ddm->zero || F == ddm->one) 
    return AR_DD_DUP (F);

  R = (ar_dd) 
    ar_dd_entry_for_operation (ddm, ar_dd_project_variable, F, 
			       (void *) v);
  if (R != NULL)
    return AR_DD_DUP (R);

  if (AR_DD_INDEX(F) < v) 
    {
      /* -- The top index variable is not in the quantifier scope. */
      arity = AR_DD_ARITY (F);
      Rsons = ar_dd_allocate_dd_table (ddm, arity, AR_DD_CODE(F));
      
      if (AR_DD_CODE(F) == COMPACTED)
	{
	  for (i = 0; i < arity; i++)
	    {
	      AR_DD_MIN_OFFSET_NTHDD (Rsons, i) = 
		AR_DD_MIN_OFFSET_NTHSON (F, i);
	      AR_DD_MAX_OFFSET_NTHDD (Rsons, i) = 
		AR_DD_MAX_OFFSET_NTHSON (F, i);
	      AR_DD_CNTHDD (Rsons, i)=
		ar_dd_project_variable (ddm, AR_DD_CPNTHSON (F, i), v);
	    }
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    AR_DD_ENTHDD (Rsons, i) = 
	      ar_dd_project_variable (ddm, AR_DD_EPNTHSON (F, i), v);
	}
      R = ar_dd_find_or_add (ddm, AR_DD_INDEX(F), arity, AR_DD_CODE (F), Rsons);
      ar_dd_free_dd_table (ddm, arity, AR_DD_CODE (F), Rsons);
    }
  else if (AR_DD_INDEX(F) > v)
    {
      R = AR_DD_DUP (F);
    }
  else 
    {
      R = AR_DD_DUP (ddm->zero);
      if (AR_DD_CODE (F) == COMPACTED)
	{
	  for (i = 0; i < AR_DD_ARITY (F) && R != ddm->one; i++) 
	    {
	      U = AR_DD_DUP (AR_DD_CPNTHSON (F, i));
	      V = ar_dd_compute_or (ddm, U, R);
	      AR_DD_FREE (U); 
	      AR_DD_FREE (R);
	      R = V;
	    }
	}
      else
	{
	  for (i = 0; i < AR_DD_ARITY (F) && R != ddm->one; i++) 
	    {
	      U = AR_DD_DUP (AR_DD_EPNTHSON (F, i));
	      V = ar_dd_compute_or (ddm, U, R);
	      AR_DD_FREE (U); 
	      AR_DD_FREE (R);
	      R = V;
	    }
	}
    }
  ar_dd_memoize_operation (ddm, ar_dd_project_variable, F, (void *) v, R); 

  return R;
}

			/* --------------- */

ar_dd
ar_dd_project_variable2 (ar_ddm ddm, ar_dd F, ar_dd replace)
{
  ar_dd R = NULL;
  ar_dd U;
  ar_dd V;
  ar_ddtable Rsons;
  int i;
  int arity;
  int v;

  if (F == ddm->zero)
    return AR_DD_DUP (ddm->zero);

  if (F == ddm->one)
    return AR_DD_DUP (replace);
  
  R = (ar_dd) 
    ar_dd_entry_for_operation (ddm, ar_dd_project_variable2, F, replace);
  if (R != NULL)
    return AR_DD_DUP (R);
  v = AR_DD_INDEX (replace);

  if (AR_DD_INDEX(F) < v) 
    {
      /* -- The top index variable is not in the quantifier scope. */
      arity = AR_DD_ARITY (F);
      Rsons = ar_dd_allocate_dd_table (ddm, arity, AR_DD_CODE(F));
      
      if (AR_DD_CODE(F) == COMPACTED)
	{
	  for (i = 0; i < arity; i++)
	    {
	      AR_DD_MIN_OFFSET_NTHDD (Rsons, i) = 
		AR_DD_MIN_OFFSET_NTHSON (F, i);
	      AR_DD_MAX_OFFSET_NTHDD (Rsons, i) = 
		AR_DD_MAX_OFFSET_NTHSON (F, i);
	      AR_DD_CNTHDD (Rsons, i)=
		ar_dd_project_variable2 (ddm, AR_DD_CPNTHSON (F, i), replace);
	    }
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    AR_DD_ENTHDD (Rsons, i) = 
	      ar_dd_project_variable2 (ddm, AR_DD_EPNTHSON (F, i), replace);
	}
      R = ar_dd_find_or_add (ddm, AR_DD_INDEX(F), arity, AR_DD_CODE (F), Rsons);
      ar_dd_free_dd_table (ddm, arity, AR_DD_CODE (F), Rsons);
    }
  else if (AR_DD_INDEX(F) > v) 
    {
      R = ar_dd_compute_and (ddm, F, replace);
    }
  else 
    {
      ar_dd tmp;
      R = AR_DD_DUP (ddm->zero);
      if (AR_DD_CODE (F) == COMPACTED)
	{
	  for (i = 0; i < AR_DD_ARITY (F) && R != ddm->one; i++) 
	    {
	      U = AR_DD_DUP (AR_DD_CPNTHSON (F, i));
	      V = ar_dd_compute_or (ddm, U, R);
	      AR_DD_FREE (U); 
	      AR_DD_FREE (R);
	      R = V;
	    }
	}
      else
	{
	  for (i = 0; i < AR_DD_ARITY (F) && R != ddm->one; i++) 
	    {
	      U = AR_DD_DUP (AR_DD_EPNTHSON (F, i));
	      V = ar_dd_compute_or (ddm, U, R);
	      AR_DD_FREE (U); 
	      AR_DD_FREE (R);
	      R = V;
	    }
	}
      tmp = ar_dd_compute_and (ddm, R, replace);
      AR_DD_FREE (R);
      R = tmp;
    }
  ar_dd_memoize_operation (ddm, ar_dd_project_variable2, F, replace, R); 

  return R;
}

			/* --------------- */
ar_dd
ar_dd_project_variable3 (ar_ddm ddm, ar_dd F, ar_dd st)
{
  ar_dd R = NULL;
  int v;

  if (F == ddm->zero || F == ddm->one)
    return AR_DD_DUP (F);

  if (st == ddm->one)
    return AR_DD_DUP (F);

  if (st == ddm->zero)
    return AR_DD_DUP (ddm->zero);

  R = (ar_dd) 
    ar_dd_entry_for_operation (ddm, ar_dd_project_variable3, F, st);
  if (R != NULL)
    return AR_DD_DUP (R);

  v = AR_DD_INDEX (st);

  if (AR_DD_INDEX (F) < v) 
    {
      int i;
      int arity = AR_DD_ARITY (F);
      ar_ddtable Rsons = ar_dd_allocate_dd_table (ddm, arity, AR_DD_CODE(F));
      
      if (AR_DD_CODE(F) == COMPACTED)
	{
	  for (i = 0; i < arity; i++)
	    {
	      AR_DD_MIN_OFFSET_NTHDD (Rsons, i) = 
		AR_DD_MIN_OFFSET_NTHSON (F, i);
	      AR_DD_MAX_OFFSET_NTHDD (Rsons, i) = 
		AR_DD_MAX_OFFSET_NTHSON (F, i);
	      AR_DD_CNTHDD (Rsons, i)=
		ar_dd_project_variable3 (ddm, AR_DD_CPNTHSON (F, i), st);
	    }
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    AR_DD_ENTHDD (Rsons, i) = 
	      ar_dd_project_variable3 (ddm, AR_DD_EPNTHSON (F, i), st);
	}
      R = ar_dd_find_or_add (ddm, AR_DD_INDEX(F), arity, AR_DD_CODE (F), Rsons);
      ar_dd_free_dd_table (ddm, arity, AR_DD_CODE (F), Rsons);
    }
  else if (AR_DD_INDEX(F) > v) 
    {
      int i;
      int arity = AR_DD_ARITY (st);

      R = AR_DD_DUP (ddm->zero);
      for (i = 0; i < arity && R != ddm->one; i++) 
	{
	  ar_dd son = AR_DD_PNTHSON (st, i);
	  ar_dd U = ar_dd_project_variable3 (ddm, F, son);
	  ar_dd V = ar_dd_compute_or (ddm, U, R);
	  AR_DD_FREE (U); 
	  AR_DD_FREE (R);
	  R = V;
	}
    }
  else 
    {
      int Farity = AR_DD_ARITY (F);
      int starity = AR_DD_ARITY (st);

      R = AR_DD_DUP (ddm->zero);
      if (AR_DD_CODE (F) == COMPACTED)
	{
	  int Fi = 0, sti = 0;
	  int Fmin = AR_DD_MIN_OFFSET_NTHSON (F, 0);
	  int Fmax = AR_DD_MAX_OFFSET_NTHSON (F, 0);
	  int stmin = AR_DD_MIN_OFFSET_NTHSON (st, 0);
	  int stmax = AR_DD_MAX_OFFSET_NTHSON (st, 0);
	  ar_dd Fson = AR_DD_CPNTHSON (F, Fi);
	  ar_dd stson = AR_DD_CPNTHSON (st, sti);

	  while (sti < starity || Fi < Farity ) 
	    {
	      ar_dd U, V;

	      ccl_assert (stmin == Fmin);
		  
	      U = ar_dd_project_variable3 (ddm, Fson, stson);
	      V = ar_dd_compute_or (ddm, U, R);
	      AR_DD_FREE (U); 
	      AR_DD_FREE (R);
	      R = V;
		  
	      if (Fmin == Fmax)
		{
		  if (Fi < Farity) 
		    {
		      Fi++;
		      if (Fi < Farity)
			{
			  Fmin = AR_DD_MIN_OFFSET_NTHSON (F, Fi);
			  Fmax = AR_DD_MAX_OFFSET_NTHSON (F, Fi);
			  Fson = AR_DD_CPNTHSON (F, Fi);
			}
		    }
		}
	      else if (Fmin < Fmax)
		{
		  Fmin++;
		}

	      if (stmin == stmax)
		{
		  if (sti < starity) 
		    {
		      sti++;
		      if (sti < starity)
			{
			  stmin = AR_DD_MIN_OFFSET_NTHSON (st, sti);
			  stmax = AR_DD_MAX_OFFSET_NTHSON (st, sti);
			  stson = AR_DD_CPNTHSON (st, sti);
			}
		    }
		}
	      else if (stmin < stmax)
		{
		  stmin++;
		}
	    }
	}
      else
	{
	  int i;
	  for (i = 0; i < starity && R != ddm->one; i++) 
	    {
	      ar_dd Fson = AR_DD_EPNTHSON (F, i);
	      ar_dd stson = AR_DD_EPNTHSON (st, i);	      
	      ar_dd U = ar_dd_project_variable3 (ddm, Fson, stson);
	      ar_dd V = ar_dd_compute_or (ddm, U, R);
	      AR_DD_FREE (U); 
	      AR_DD_FREE (R);
	      R = V;
	    }
	}
    }
  ar_dd_memoize_operation (ddm, ar_dd_project_variable3, F, st, R); 

  return R;
}

void
ar_dd_assign_bin_op (ar_ddm ddm, ar_dd (*op)(ar_ddm ddm, ar_dd F, ar_dd G),
		     ar_dd *pF, ar_dd G)
{
  ar_dd tmp = op (ddm, *pF, G);
  
  AR_DD_FREE (*pF);
  *pF = tmp;
}

/*
 *
 * DISPLAYING INFORMATIONS ABOUT DDs
 *
 */

static long s_sum_previous_page_sizes(dd_node_page a_page)
{
  long sum;

  if( a_page == NULL ) sum=0;
  else sum = a_page->size + s_sum_previous_page_sizes(a_page->prev);

  return sum ;
}

			/* --------------- */

long
ar_dd_relative_address(ar_ddm ddm, ar_dd F)
{
  long         address;
  dd_node_page a_page;

  a_page = ddm->pm_first_page;
  while( AR_DD_ADDR(F) < (ar_dd)a_page->page
	 || AR_DD_ADDR(F) >= (((ar_dd)(a_page->page))+a_page->size) )
    a_page = a_page->prev;

  address = ((long)(AR_DD_ADDR(F)-(ar_dd)a_page->page)) + 1
    + s_sum_previous_page_sizes(a_page->prev);
 
  return address;
}

			/* --------------- */

# define fpf fprintf 


			/* --------------- */

void
ar_dd_display_decision_diagram_node (ar_ddm ddm, ar_dd F, FILE *out)
{
  int i;

  if( AR_DD_ADDR(F) == ddm->one )
    {  
      fpf(out,"%ld",ar_dd_relative_address(ddm,AR_DD_ADDR(F)));
      fpf(out," :: ");
      fpf(out,"1");
      fpf(out," >%d<",AR_DD_REFCOUNT(F));
      fpf(out,"\n");
    }
  else if( AR_DD_INDEX(F) > 0 )
    {
      fpf(out,"%ld",ar_dd_relative_address(ddm,AR_DD_ADDR(F)));
      fpf(out," :: ");
      fpf(out,"case(@%d", AR_DD_INDEX(F));
      if( AR_DD_CODE(F) == COMPACTED )
	{
	  for(i = 0; i < AR_DD_ARITY(F); i++) 
	    {
	      fpf(out,",%d..%d:",AR_DD_MIN_OFFSET_NTHSON(F,i),
		  AR_DD_MAX_OFFSET_NTHSON(F,i));
	      if( AR_DD_POLARITY(AR_DD_CNTHSON(F,i)) ) 
		fpf(out,"~");
	      fpf(out,"%ld",
		  ar_dd_relative_address(ddm,AR_DD_CNTHSON(F,i)));
	    }
	}
      else
	{
	  for(i = 0; i<AR_DD_ARITY(F); i++) 
	    {
	      fpf(out,",%d:",i);
	      if(AR_DD_POLARITY(AR_DD_ENTHSON(F,i))) 
		fpf(out,"~");
	      fpf(out,"%ld",
		  ar_dd_relative_address(ddm,AR_DD_ENTHSON(F,i)));
	    }
	}
      fpf(out,")");
      fpf(out," >%d<",AR_DD_REFCOUNT(F));
      fpf(out,"\n");
    }
  else { /* Do nothing */ }
}

			/* --------------- */

static void 
s_display_decision_diagram_page(ar_ddm ddm, dd_node_page a_page, 
				FILE *out, long from, long to)
{
  long    bottom;
  long    current;
  ar_dd F;
  
  if( a_page != NULL ) 
    {
      s_display_decision_diagram_page(ddm,a_page->prev,out,from,to);
      bottom = ar_dd_relative_address(ddm,a_page->page);
      if( bottom <= to && bottom+a_page->size >= from )
	{
	  if( bottom < from ) 
	    {
	      F = a_page->page+from-bottom;
	      current = from;
	    }
	  else 
	    {
	      F = a_page->page;
	      current = bottom;
	    }
	  while( current <= to && current < bottom+a_page->size)
	    {
	      ar_dd_display_decision_diagram_node(ddm,F,out);
	      F++; 
	      current++;
	    }
	}
    }
}

			/* --------------- */

void
ar_dd_display_decision_diagram_pages(ar_ddm ddm,
				       FILE* out, long from, long to)
{
  s_display_decision_diagram_page(ddm,ddm->pm_first_page,out,from,to);
}

			/* --------------- */

static void
s_display_decision_diagram_node_as_dot (ar_ddm ddm, ar_dd F, FILE *out)
{
  int i;

  if (AR_DD_ADDR (F) == ddm->one)
    {  
      fpf (out, "N%p [label=\"T\\n%p\"];\n", ddm->one, ddm->one);
    }
  else if (AR_DD_INDEX (F) >= 0)
    {
      void *addr = AR_DD_ADDR (F);

      fpf (out, "N%p [label=\"%d\\n%p\"];\n", addr, AR_DD_INDEX (F), addr);

      if (AR_DD_CODE (F) == COMPACTED)
	{
	  for (i = 0; i < AR_DD_ARITY (F); i++) 
	    {
	      void *sonaddr = AR_DD_ADDR (AR_DD_CNTHSON (F, i));

	      fpf (out, "N%p -> N%p [label=\"", addr, sonaddr);

	      if (AR_DD_POLARITY (AR_DD_CNTHSON (F, i)))
		fpf(out, "! ");
	      
	      if (AR_DD_MIN_OFFSET_NTHSON(F,i) == AR_DD_MAX_OFFSET_NTHSON(F,i))
		fpf (out, "%d", AR_DD_MIN_OFFSET_NTHSON(F,i));
	      else
		fpf (out, "%d..%d", AR_DD_MIN_OFFSET_NTHSON(F,i),
		     AR_DD_MAX_OFFSET_NTHSON(F,i));
	      fpf (out, "\"];\n");
	    }
	}
      else
	{
	  for(i = 0; i<AR_DD_ARITY(F); i++) 
	    {
	      void *sonaddr = AR_DD_ADDR (AR_DD_ENTHSON (F, i));

	      fpf (out, "N%p -> N%p [label=\"", addr, sonaddr);

	      if (AR_DD_POLARITY (AR_DD_ENTHSON (F, i)))
		fpf(out, "! ");

	      fpf(out,"%d", i);
	      fpf (out, "\"];\n");
	    }
	}
    }
  else { /* Do nothing */ }
}

			/* --------------- */

static void
s_display_decision_diagram_nodes (ar_ddm ddm, ar_dd F, FILE *out, int dot)
{
  int i;

  if (AR_DD_FLAG (F) == 1) 
    return;

  if (AR_DD_ADDR (F) == ddm->one) 
    {
      AR_DD_FLAG (F) = 1;
      if (dot)
	s_display_decision_diagram_node_as_dot (ddm, F, out);
      else
	ar_dd_display_decision_diagram_node (ddm, F, out);
    }
  else 
    {
      AR_DD_FLAG (F) = 1;
      if (dot)
	s_display_decision_diagram_node_as_dot (ddm, F, out);
      else
	ar_dd_display_decision_diagram_node (ddm, F, out);
      if (AR_DD_CODE (F) == EXHAUSTIVE)
	{
	  for (i = 0; i < AR_DD_ARITY (F); i++)
	    s_display_decision_diagram_nodes (ddm, AR_DD_ENTHSON (F, i), out,
					      dot);
	}
      else
	{
	  for (i = 0; i < AR_DD_ARITY (F); i++)
	    s_display_decision_diagram_nodes (ddm, AR_DD_CNTHSON (F, i), out, 
					      dot);
	}
    }
}


			/* --------------- */

void
ar_dd_display_decision_diagram_nodes_as_dot (ar_ddm ddm, ar_dd F, FILE *output)
{
  if (output == NULL)
    output = stderr;

  ar_dd_initialize_decision_diagram_flag(F);
  fpf (output, "digraph \"DD@%p\" {\n", F);
  fpf (output, "root -> N%p[label=\"%s\"];\n", AR_DD_ADDR (F), 
       (AR_DD_POLARITY (F) ? "!" : ""));
  s_display_decision_diagram_nodes (ddm, F, output, 1);
  fpf (output, "}\n");
  ar_dd_initialize_decision_diagram_flag(F);
}

			/* --------------- */

static void
s_display_nodes_as_dot2 (ar_ddm ddm, ar_dd F, ccl_log_type out, ccl_hash *cache,
			 int only_assignments)
{
  if (ccl_hash_find (cache, F))
    return;

  ccl_hash_insert (cache, F);

  if (ar_dd_is_one (ddm, F) || ar_dd_is_zero (ddm, F))
    {
      int one = ar_dd_is_one (ddm, F) ? 1 : 0;
      if (one || ! only_assignments)
	ccl_log (out, "N%p[label=%d,shape=square]; \n", F, one);
    }
  else if (AR_DD_CODE (F) == COMPACTED)
    {
      int i;

      ccl_log (out, "N%p[label=%d];\n", F, AR_DD_INDEX (F));
      for (i = 0; i < AR_DD_ARITY (F); i++) 
	{
	  ar_dd son = AR_DD_CPNTHSON (F, i);

	  if (only_assignments && ar_dd_is_zero (ddm, son))
	    continue;
	    
	  ccl_log (out, "N%p -> N%p [label=\"", F, son);
	    
	  if (AR_DD_MIN_OFFSET_NTHSON(F,i) == AR_DD_MAX_OFFSET_NTHSON(F,i))
	    ccl_log (out, "%d", AR_DD_MIN_OFFSET_NTHSON(F,i));
	  else
	    ccl_log (out, "%d..%d", AR_DD_MIN_OFFSET_NTHSON(F,i),
		     AR_DD_MAX_OFFSET_NTHSON(F,i));
	  ccl_log (out, "\"];\n");
	  s_display_nodes_as_dot2 (ddm, son, out, cache, only_assignments);
	}      
    }
  else
    {
      int i;

      ccl_log (out, "N%p[label=%d];\n", F, AR_DD_INDEX (F));
      for (i = 0; i < AR_DD_ARITY (F); i++) 
	{
	  ar_dd son = AR_DD_EPNTHSON (F, i);

	  if (only_assignments && ar_dd_is_zero (ddm, son))
	    continue;

	  ccl_log (out, "N%p -> N%p [label=\"%d\"];\n", F, son, i);
	  s_display_nodes_as_dot2 (ddm, son, out, cache, only_assignments);
	}
    }
}

			/* --------------- */

void
ar_dd_display_decision_diagram_nodes_as_dot2 (ar_ddm ddm, ar_dd F, 
					      ccl_log_type output, 
					      int only_assignments)
{
  ccl_hash *cache = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_log (output, "digraph \"DD@%p\" {\n", F);
  ccl_log (output, "root -> N%p;\n", F);
  s_display_nodes_as_dot2 (ddm, F, output, cache, only_assignments);
  ccl_log (output, "}\n");
  ccl_hash_delete (cache);
}

			/* --------------- */

void
ar_dd_display_decision_diagram_nodes(ar_ddm ddm, ar_dd F, FILE *out)
{
  ar_dd_initialize_decision_diagram_flag(F);
  s_display_decision_diagram_nodes (ddm, F, out, 0);
  ar_dd_initialize_decision_diagram_flag(F);
}

			/* --------------- */

void
ar_dd_display_statistics_about_decision_diagrams(ar_ddm ddm, ccl_log_type log)
{
  s_update_statistics_about_the_decision_diagram_hashtable(ddm);
  s_update_statistics_about_cache(ddm);
  
  ccl_log (log, 
      "The decision diagram hashtable:\n"
      "  Size of the hashtable: %ld (minimum %ld, maximum %ld)\n"
      "  Number of active nodes: %ld.\n"
      "  Number of insertions since GC: %ld.\n"
      "  Number of empty entries: %ld, number of collisions: %ld.\n"
      "  Size of the longest collision chain: %ld (entry %ld).\n"
      "  Load factor: %g (enlargement ratio: %g, reduction ratio: %g).\n"
      "\n"
      "The decision diagram page manager:\n"
      "  Number of pages: %ld (current page size %ld).\n"
      "  Number of allocated nodes: %ld (number of free nodes: %ld).\n"
      "\n",
      ddm->ht_actual_size,
      ddm->ht_minimum_size,
      ddm->ht_maximum_size,
      ddm->ht_number_of_active_nodes,
      ddm->ht_number_of_insertions,
      ddm->ht_number_of_empty_entries,
      ddm->ht_number_of_collisions,
      ddm->ht_size_longest_collision_chain,
      ddm->ht_entry_longest_collision_chain,
      ddm->ht_actual_ratio,
      ddm->ht_enlargement_ratio,
      ddm->ht_reduction_ratio,

      ddm->pm_number_of_pages,
      ddm->pm_current_page_size,
      ddm->pm_number_of_allocated_nodes,
      ddm->pm_number_of_free_nodes);

  ccl_log (log, 
      "The memoization hashcache:\n"
      "  Size of the hashcache: %ld (minimum %ld, maximum %ld)\n"
      "  Number of stored 4-tuples: %ld.\n",      
      ddm->op_actual_size,
      ddm->op_minimum_size,
      ddm->op_maximum_size,
      ddm->op_number_of_stored_records);
}

			/* --------------- */

ar_dd
ar_dd_shift_variables (ar_ddm ddm, ar_dd X, int offset)
{
  ar_dd R;

  if (X == ddm->one || X == ddm->zero)
    R = AR_DD_DUP (X);
  else
    {
      R = (ar_dd) ar_dd_entry_for_operation (ddm, ar_dd_shift_variables, X, 
					     (void *) (intptr_t)offset);
      if (R != NULL)
	{
	  R = AR_DD_DUP (R);
	  ccl_assert (AR_DD_INDEX (R) == AR_DD_INDEX (X) + offset);
	}
      else
	{      
	  int i;
	  int arity = AR_DD_ARITY (X);
	  int ddindex = offset + AR_DD_INDEX (X);
	  ar_ddcode encoding = AR_DD_CODE (X);
	  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);
      
	  if (encoding == COMPACTED) 
	    {
	      for (i = 0; i < arity; i++)
		{
		  AR_DD_MIN_OFFSET_NTHDD (sons, i) = 
		    AR_DD_MIN_OFFSET_NTHSON (X, i);
		  AR_DD_MAX_OFFSET_NTHDD (sons, i) = 
		    AR_DD_MAX_OFFSET_NTHSON (X, i);
		  AR_DD_CNTHDD (sons, i)=
		    ar_dd_shift_variables (ddm, AR_DD_CPNTHSON (X, i), offset);
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		AR_DD_ENTHDD (sons, i) = 
		  ar_dd_shift_variables (ddm, AR_DD_EPNTHSON (X, i), offset);
	    }
	
	  R = ar_dd_find_or_add (ddm, ddindex, arity, encoding, sons);
	  ar_dd_free_dd_table (ddm, arity, encoding, sons);

	  ar_dd_memoize_operation (ddm, ar_dd_shift_variables, X, 
				   (void *) (intptr_t) offset, R); 
	}
    }

  return R;
}

			/* --------------- */

static int 
s_register_node (ccl_hash *nodes_table, ar_dd dd)
{
  int id = ccl_hash_get_size (nodes_table);
  ccl_hash_find (nodes_table, dd);
  ccl_hash_insert (nodes_table, (void *) (intptr_t) id);

  return id;
}

			/* --------------- */

static void
s_write_node (ar_ddm ddm, ar_dd dd, ccl_hash *nodes_table, FILE *out,
	      ccl_serializer_status *p_err)
{
  int i, arity, id;
  ar_ddcode encoding;

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  if (ccl_hash_find (nodes_table, dd))
    return;

  if (dd == ar_dd_one (ddm))
    {
      ccl_hash_insert (nodes_table, 0);
      return;
    }

  arity = AR_DD_ARITY (dd);
  encoding = AR_DD_CODE (dd);
  for (i = 0; i < arity && *p_err == CCL_SERIALIZER_OK; i++)
    {
      ar_dd son = ((encoding == EXHAUSTIVE) 
		   ? AR_DD_ENTHSON (dd, i) 
		   : AR_DD_CNTHSON (dd, i));
      if (AR_DD_POLARITY (son))
	son = AR_DD_NOT (son);
      s_write_node (ddm, son, nodes_table, out, p_err);
    }
  id = s_register_node (nodes_table, dd);
  ccl_serializer_write_uint32 (id, out, p_err);
  ccl_serializer_write_uint16 (AR_DD_INDEX (dd), out, p_err);
  ccl_serializer_write_uint16 (((arity << 1) | 
				(encoding == EXHAUSTIVE ? 0 : 1)), 
			       out, p_err);

  for (i = 0; i < arity && *p_err == CCL_SERIALIZER_OK; i++)
    {
      uint32_t sonid;
      ar_dd son = ((encoding == EXHAUSTIVE) 
		   ? AR_DD_ENTHSON (dd, i) 
		   : AR_DD_CNTHSON (dd, i));
      int polarity =  AR_DD_POLARITY (son);

      if (polarity)
	son = AR_DD_NOT (son);

      ccl_assert (ccl_hash_find (nodes_table, son));
      ccl_hash_find (nodes_table, son);
      sonid = (uint32_t) (uintptr_t) ccl_hash_get (nodes_table);
      if (encoding == COMPACTED)
	{
	  int16_t min = AR_DD_MIN_OFFSET_NTHSON (dd, i);
	  int16_t max = AR_DD_MAX_OFFSET_NTHSON (dd, i);
	  ccl_serializer_write_int16 (min, out, p_err);
	  ccl_serializer_write_int16 (max, out, p_err);
	}

      sonid <<= 1;
      if (polarity)
	sonid |= 1;
      ccl_serializer_write_uint32 (sonid, out, p_err);
    }
}

			/* --------------- */

void
ar_dd_write_node (ar_ddm ddm, ar_dd dd, FILE *out, 
		  ccl_serializer_status *p_err)
{
  int nb_nodes = ar_dd_get_number_of_nodes(dd) << 1;
  ccl_hash *nodes_table = ccl_hash_create (NULL, NULL, NULL, NULL);

  if (AR_DD_POLARITY (dd))
    {
      nb_nodes += 1;
      dd = AR_DD_NOT (dd);
    }

  ccl_serializer_write_uint32 (nb_nodes, out, p_err);
  s_write_node (ddm, dd, nodes_table, out, p_err);
  ccl_hash_delete (nodes_table);
}

			/* --------------- */

static void
s_read_node (ar_ddm ddm, int nb_nodes, ar_dd *nodes_table, FILE *in, 
	     ccl_serializer_status *p_err)
{
  int i;
  uint32_t id;
  uint16_t arity;
  uint16_t index;
  ar_ddcode encoding;
  ar_ddtable sons;

  if (*p_err != CCL_SERIALIZER_OK)
    return;
  
  ccl_serializer_read_uint32 (&id, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    return;
  ccl_assert (nodes_table[id] == NULL);

  ccl_serializer_read_uint16 (&index, in, p_err);
  ccl_serializer_read_uint16 (&arity, in, p_err);
  encoding = (arity & 0x1) ? COMPACTED : EXHAUSTIVE;
  arity >>= 1;
  if (*p_err != CCL_SERIALIZER_OK)
    return;

  sons = ar_dd_allocate_dd_table (ddm, arity, encoding);
  if (encoding == EXHAUSTIVE)
    {
      for (i = 0; i < arity && *p_err == CCL_SERIALIZER_OK; i++)
	{
	  uint32_t sonid,  polarity;

	  ccl_serializer_read_uint32 (&sonid, in, p_err);
	  if (*p_err == CCL_SERIALIZER_OK)
	    {
	      polarity = sonid & 0x1;
	      sonid >>= 1;

	      ccl_assert (nodes_table[sonid] != NULL);
	      if (polarity)
		AR_DD_ENTHDD (sons, i) = AR_DD_DUP_NOT (nodes_table[sonid]);
	      else
		AR_DD_ENTHDD (sons, i) = AR_DD_DUP (nodes_table[sonid]);
	    }
	}
    }
  else
    {
      for (i = 0; i < arity && *p_err == CCL_SERIALIZER_OK; i++)
	{
	  int16_t min, max;
	  uint32_t sonid, polarity;

	  ccl_serializer_read_int16 (&min, in, p_err);
	  ccl_serializer_read_int16 (&max, in, p_err);
	  ccl_serializer_read_uint32 (&sonid, in, p_err);
	  if (*p_err == CCL_SERIALIZER_OK)
	    {
	      polarity = sonid & 0x1;
	      sonid >>= 1;

	      AR_DD_MIN_OFFSET_NTHDD (sons, i) = min;
	      AR_DD_MAX_OFFSET_NTHDD (sons, i) = max;
	      ccl_assert (nodes_table[sonid] != NULL);
	      if (polarity)
		AR_DD_CNTHDD (sons, i) = AR_DD_DUP_NOT (nodes_table[sonid]);
	      else
		AR_DD_CNTHDD (sons, i) = AR_DD_DUP (nodes_table[sonid]);
	    }
	}
    }

  if (*p_err == CCL_SERIALIZER_OK)
    {
      nodes_table[id] = ar_dd_find_or_add (ddm,  index, arity, encoding, sons);
      s_check_node (ddm, nodes_table[id], CHECK_DEPTH);
    }
  ar_dd_free_dd_table (ddm, i, encoding, sons);
}

			/* --------------- */

void
ar_dd_read_node (ar_ddm ddm, ar_dd *p_dd, FILE *in, 
		 ccl_serializer_status *p_err)
{
  int i, polarity;
  ccl_serializer_status err = CCL_SERIALIZER_OK;
  ar_dd *nodes_table = NULL;
  ar_dd result = NULL;
  uint32_t nb_nodes;

  ccl_serializer_read_uint32 (&nb_nodes, in, &err);
  if (err)
    goto end;

  polarity = nb_nodes & 0x1;
  nb_nodes >>= 1;
  nodes_table = DD_CALLOC (ar_dd, nb_nodes);
  nodes_table[0] = ar_dd_one (ddm);
  nodes_table[0] = AR_DD_DUP (nodes_table[0]);

  for (i = 0; i < nb_nodes - 1 && err == CCL_SERIALIZER_OK; i++)
    s_read_node (ddm, nb_nodes, nodes_table, in, &err);
  if (err != CCL_SERIALIZER_OK)
    goto end;

  result = AR_DD_DUP (nodes_table[nb_nodes - 1]);
  if (polarity)
    result = AR_DD_NOT (result);
  *p_dd = result;

 end:
  if (nodes_table != NULL)
    {
      for (i = 0; i < nb_nodes; i++)
	AR_DD_FREE (nodes_table[i]);
      DD_FREE (nodes_table);
    }
}

			/* --------------- */

ar_dd
ar_dd_create_projection_list (ar_ddm ddm)
{
  return AR_DD_DUP (ddm->one);
}

			/* --------------- */

void
ar_dd_projection_list_add (ar_ddm ddm, ar_dd *list, int index, int newindex)
{
  ar_dd var = 
    ar_dd_var_in_range (ddm, index, newindex + 2, COMPACTED, 
			newindex + 1, newindex + 1);
  ar_dd newlist = ar_dd_compute_and (ddm, *list, var);

  AR_DD_FREE (var);
  AR_DD_FREE (*list);
  *list = newlist;
}

			/* --------------- */

#ifdef CCL_ENABLE_ASSERTIONS
static int 
s_is_a_projection_list (ar_ddm ddm, ar_dd list)
{
  int arity, newindex;

  if (list == ddm->one || list == ddm->zero)
    return 1;

  ccl_assert (AR_DD_CODE (list) == COMPACTED);
  arity = AR_DD_ARITY (list);
  ccl_assert (arity == 2);
  newindex = AR_DD_MIN_OFFSET_NTHSON (list, 1) - 1;
  ccl_assert (newindex >= 0);
  ccl_assert (newindex + 1 == AR_DD_MAX_OFFSET_NTHSON (list, 1));
  ccl_assert (! ar_dd_is_zero (ddm, AR_DD_CPNTHSON (list, 1)));

  ccl_assert (AR_DD_MIN_OFFSET_NTHSON (list, 0) == 0);
  ccl_assert (AR_DD_MAX_OFFSET_NTHSON (list, 0) == newindex);
  ccl_assert (ar_dd_is_zero (ddm, AR_DD_CPNTHSON (list, 0)));

  return s_is_a_projection_list (ddm, AR_DD_CPNTHSON (list, 1));
}
#endif

void
ar_dd_projection_list_merge (ar_ddm ddm, ar_dd *list, ar_dd other)
{
  ar_dd tmp = ar_dd_compute_and (ddm, *list, other);
  AR_DD_FREE (*list);
  *list = tmp;
  ccl_post (s_is_a_projection_list (ddm, *list));
}

int
ar_dd_projection_list_get_new_index (ar_dd l)
{
  int result;

  if (AR_DD_CODE (l) == EXHAUSTIVE)
    result = AR_DD_ARITY (l) - 2;
  else 
    {
      ccl_assert (AR_DD_ARITY (l) == 2);
      result = AR_DD_MAX_OFFSET_NTHSON (l, 1) - 1;
    }

  return result;
}

			/* --------------- */

ar_dd
ar_dd_projection_list_get_tail (ar_dd l)
{
  ar_dd result;

  if (AR_DD_CODE (l) == EXHAUSTIVE)
    result = AR_DD_EPNTHSON (l, AR_DD_ARITY (l) - 1);
  else 
    {
      ccl_assert (AR_DD_ARITY (l) == 2);
      result = AR_DD_CPNTHSON (l, 1);
    }

  return result;
}

			/* --------------- */

ar_dd
ar_dd_project_on_variables (ar_ddm ddm, ar_dd F, ar_dd varlist)
{
  ar_dd R;

  CHECK_REFCOUNT (F);
  CHECK_REFCOUNT (varlist);

  if (varlist == ddm->one)
    {
      if (F != ddm->zero)
	return AR_DD_DUP (ddm->one);
      else
	return AR_DD_DUP (ddm->zero);
    }
  
  if (F == ddm->one || F == ddm->zero)
    return AR_DD_DUP (F);

  
  R = ar_dd_entry_for_operation (ddm, ar_dd_project_on_variables, F, varlist);
  if (R != NULL)
    R = AR_DD_DUP (R);
  else
    {      
      int i;
      int index = AR_DD_INDEX (F);
      int arity = AR_DD_ARITY (F);
      ar_ddcode encoding = AR_DD_CODE (F);

      if (index < AR_DD_INDEX (varlist))
	{
	  ar_dd tmp[2];

	  R = ar_dd_dup_zero (ddm);
	  
	  for (i = 0; i < arity; i++)
	    {
	      if (encoding == EXHAUSTIVE)
		tmp[0] = AR_DD_EPNTHSON (F, i);
	      else
		tmp[0] = AR_DD_CPNTHSON (F, i);
	      
	      tmp[1] = ar_dd_compute_or (ddm, tmp[0], R);	      
	      AR_DD_FREE (R);
	      R = tmp[1];
	    }
	  tmp[0] = ar_dd_project_on_variables (ddm, R, varlist);
	  AR_DD_FREE (R);
	  R = tmp[0];
	}
      else if (index == AR_DD_INDEX (varlist))
	{
	  int foa = 1;
	  int newindex = ar_dd_projection_list_get_new_index (varlist);
	  ar_dd tail = ar_dd_projection_list_get_tail (varlist);
	  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

	  if (encoding == EXHAUSTIVE)
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_dd tmp = 
		    ar_dd_project_on_variables (ddm, AR_DD_EPNTHSON (F, i), tail);
		  s_check_node (ddm, tmp, CHECK_DEPTH);
		  AR_DD_ENTHDD (sons, i) = tmp;
		  if (tmp != ddm->one && tmp != ddm->zero &&
		      AR_DD_INDEX (tmp) <= newindex)
		    foa = 0;
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_dd tmp;
		  AR_DD_MIN_OFFSET_NTHDD (sons, i) =
		    AR_DD_MIN_OFFSET_NTHSON (F, i);
		  AR_DD_MAX_OFFSET_NTHDD (sons, i) =
		    AR_DD_MAX_OFFSET_NTHSON (F, i);
		  tmp = 
		    ar_dd_project_on_variables (ddm, AR_DD_CPNTHSON (F, i), 
						tail);
		  s_check_node (ddm, tmp, CHECK_DEPTH);
		  AR_DD_CNTHDD (sons, i) = tmp;

		  if (tmp != ddm->one && tmp != ddm->zero &&
		      AR_DD_INDEX (tmp) <= newindex)
		    foa = 0;
		}
	    }

	  if (foa)
	    R = ar_dd_find_or_add (ddm, newindex, arity, encoding, sons);
	  else
	    {
	      int card;

	      if (encoding == EXHAUSTIVE)
		card = arity;
	      else
		card = (AR_DD_MAX_OFFSET_NTHSON (F, arity-1) - 
			AR_DD_MIN_OFFSET_NTHSON (F, 0) + 1);
	      R = AR_DD_DUP (ddm->zero);

	      for (i = 0; i < arity; i++)
		{
		  int min, max;
		  ar_dd U, aux, son;

		  if (encoding == EXHAUSTIVE)
		    {
		      min = max = i;
		      son = AR_DD_ENTHDD (sons, i);
		    }
		  else 
		    {
		      min = AR_DD_MIN_OFFSET_NTHSON (F, i);
		      max = AR_DD_MAX_OFFSET_NTHSON (F, i);
		      son = AR_DD_CNTHDD (sons, i);
		    }
		  U = ar_dd_var_in_range (ddm, newindex, card, encoding, 
					  min, max);		  
		  aux = ar_dd_compute_and (ddm, U, son);
		  AR_DD_FREE (U);
		  U = ar_dd_compute_or (ddm, R, aux);
		  AR_DD_FREE (R);
		  AR_DD_FREE (aux);
		  R = U;
		}
	    }
	  ar_dd_free_dd_table (ddm, arity, encoding, sons);
	}
      else
	{
	  ar_dd tail = ar_dd_projection_list_get_tail (varlist);

	  R = ar_dd_project_on_variables (ddm, F, tail);
	}

      ar_dd_memoize_operation (ddm, ar_dd_project_on_variables, F, varlist, R);
    }

  s_check_node (ddm, R, CHECK_DEPTH);
  CHECK_REFCOUNT (R);

  return R;
}

			/* --------------- */

ar_dd
ar_dd_project_variables (ar_ddm ddm, ar_dd F, ar_dd varlist)
{
  ar_dd R;

  CHECK_REFCOUNT (F);
  CHECK_REFCOUNT (varlist);

  if (varlist == ddm->one || F == ddm->one || F == ddm->zero)
    return AR_DD_DUP (F);
  
  R = ar_dd_entry_for_operation (ddm, ar_dd_project_variables, F, varlist);
  if (R != NULL)
    R = AR_DD_DUP (R);
  else
    {      
      int i;
      int index = AR_DD_INDEX (F);
      int arity = AR_DD_ARITY (F);
      ar_ddcode encoding = AR_DD_CODE (F);

      if (index < AR_DD_INDEX (varlist))
	{
	  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

	  if (encoding == EXHAUSTIVE)
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_dd tmp = 
		    ar_dd_project_variables (ddm, AR_DD_EPNTHSON (F, i), 
					     varlist);
		  s_check_node (ddm, tmp, CHECK_DEPTH);
		  AR_DD_ENTHDD (sons, i) = tmp;
		}
	    }
	  else
	    {
	      for (i = 0; i < arity; i++)
		{
		  ar_dd tmp;

		  AR_DD_MIN_OFFSET_NTHDD (sons, i) =
		    AR_DD_MIN_OFFSET_NTHSON (F, i);
		  AR_DD_MAX_OFFSET_NTHDD (sons, i) =
		    AR_DD_MAX_OFFSET_NTHSON (F, i);
		  tmp = 
		    ar_dd_project_variables (ddm, AR_DD_CPNTHSON (F, i), 
					     varlist);
		  s_check_node (ddm, tmp, CHECK_DEPTH);
		  AR_DD_CNTHDD (sons, i) = tmp;
		}
	    }

	  R = ar_dd_find_or_add (ddm, index, arity, encoding, sons);
	  ar_dd_free_dd_table (ddm, arity, encoding, sons);
	}
      else if (index == AR_DD_INDEX (varlist))
	{
	  ar_dd tmp[2];
	  ar_dd tail = ar_dd_projection_list_get_tail (varlist);

	  R = ar_dd_dup_zero (ddm);
	  
	  for (i = 0; i < arity && R != ddm->one; i++)
	    {
	      tmp[0] = AR_DD_PNTHSON (F, i);
	      tmp[1] = ar_dd_project_variables (ddm, tmp[0], tail);
	      tmp[0] = ar_dd_compute_or (ddm, tmp[1], R);
	      AR_DD_FREE (R);
	      AR_DD_FREE (tmp[1]);
	      R = tmp[0];
	    }
	}
      else
	{
	  ar_dd tail = ar_dd_projection_list_get_tail (varlist);

	  R = ar_dd_project_variables (ddm, F, tail);
	}

      ar_dd_memoize_operation (ddm, ar_dd_project_variables, F, varlist, R);
    }

  s_check_node (ddm, R, CHECK_DEPTH);
  CHECK_REFCOUNT (R);

  return R;
}

			/* --------------- */

ar_dd
ar_dd_get_variables (ar_ddm ddm, ar_dd F)
{
  ar_dd R;

  CHECK_REFCOUNT (F);

  if (F == ddm->one || F == ddm->zero)
    return ar_dd_create_projection_list (ddm);
  
  R = ar_dd_entry_for_operation (ddm, ar_dd_get_variables, F, NULL);
  if (R != NULL)
    R = AR_DD_DUP (R);
  else
    {      
      int i;
      int index = AR_DD_INDEX (F);
      int arity = AR_DD_ARITY (F);
      ar_ddcode encoding = AR_DD_CODE (F);

      R = ar_dd_dup_one (ddm);
      for (i = 0; i < arity; i++)
	{
	  ar_dd tmp;
	  if (encoding == EXHAUSTIVE)	      
	    tmp = AR_DD_EPNTHSON (F, i);
	  else 
	    tmp = AR_DD_CPNTHSON (F, i);
	  s_check_node (ddm, tmp, CHECK_DEPTH);
	  tmp = ar_dd_get_variables (ddm, tmp);
	  ar_dd_assign_bin_op (ddm, ar_dd_compute_and, &R, tmp);
	  AR_DD_FREE (tmp);
	}
      ar_dd_projection_list_add (ddm, &R, index, index);
      ar_dd_memoize_operation (ddm, ar_dd_get_variables, F, NULL, R);
    }

  s_check_node (ddm, R, CHECK_DEPTH);
  CHECK_REFCOUNT (R);

  return R;  
}

			/* --------------- */

ar_dd
ar_dd_create_card_list (ar_ddm ddm)
{
  return AR_DD_DUP (ddm->one);
}

			/* --------------- */

void
ar_dd_card_list_add (ar_ddm ddm, ar_dd *list, int index, 
		     ar_ddcode encoding, int card)
{
  card <<= 1;
  
  if (encoding == EXHAUSTIVE)
    card += 1;
  ar_dd_projection_list_add (ddm, list, index, card);
}

			/* --------------- */

ar_ddcode
ar_dd_card_list_get_encoding (ar_dd l)
{
  if ((ar_dd_projection_list_get_new_index (l)& 0x1))
    return EXHAUSTIVE;
  return COMPACTED;
}

int
ar_dd_card_list_get_card (ar_dd l)
{
  return ar_dd_projection_list_get_new_index (l) >> 1;
}

			/* --------------- */

ar_dd
ar_dd_card_list_get_tail (ar_dd l)
{
  return ar_dd_projection_list_get_tail (l);
}

			/* --------------- */

double
ar_dd_cardinality (ar_ddm ddm, ar_dd F, ar_dd cardlist)
{
  double result;
  memo_record rec = 
    s_get_record_for_operation (ddm, ar_dd_cardinality, F, cardlist);

  if (rec != NULL)
    result =  OPERATION_DOUBLE_RESULT (rec);
  else
    {
      if (cardlist == ddm->one)
	{
	  ccl_assert (F == ddm->one || F == ddm->zero);
	  result = (F == ddm->one) ? 1.0 : 0.0;
	}
      else if (F == ddm->zero)
	{
	  result = 0.0;
	}
      else if (F == ddm->one || AR_DD_INDEX (cardlist) < AR_DD_INDEX (F))
	{
	  result = (double) ar_dd_card_list_get_card (cardlist);
	  result *= ar_dd_cardinality (ddm, F, 
				       ar_dd_card_list_get_tail (cardlist));
	}
      else 
	{
	  int i;
	  int arity = AR_DD_ARITY (F);
	  ar_dd next_list = ar_dd_card_list_get_tail (cardlist);

	  ccl_assert (AR_DD_INDEX (cardlist) == AR_DD_INDEX (F));

	  if (AR_DD_CODE (F) == EXHAUSTIVE)
	    {
	      result = 0.0;
	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = (AR_DD_POLARITY (F)
			       ? AR_DD_NOT(AR_DD_ENTHSON (F, i))
			       : AR_DD_ENTHSON (F, i));
		  result += ar_dd_cardinality (ddm, son, next_list);
		}
	    }
	  else
	    {
	      result = 0.0;
	      for (i = 0; i < arity; i++)
		{
		  int size = (AR_DD_MAX_OFFSET_NTHSON (F, i)
			      -AR_DD_MIN_OFFSET_NTHSON (F, i) + 1);
		  ar_dd son = (AR_DD_POLARITY (F)
			       ? AR_DD_NOT(AR_DD_CNTHSON (F, i))
			       :AR_DD_CNTHSON (F, i));
		  result += ar_dd_cardinality (ddm, son, next_list) * size;
		}
	    }

	}
      ar_dd_memoize_double_operation (ddm, ar_dd_cardinality, F, cardlist, 
				      result);
    }

  return result;
}

			/* --------------- */

ar_dd
ar_dd_pick (ar_ddm ddm, ar_dd X, ar_dd V)
{
  ar_dd R;

  ccl_pre (V != ddm->zero);

  if (ddm->zero == X)
    R = AR_DD_DUP (X);
  else if (ddm->one == V)
    {      
      ccl_assert (X == ddm->zero || X == ddm->one);
      R = AR_DD_DUP (X);
    }
  else
    {
      int vindex = AR_DD_INDEX (V);
      int card = ar_dd_card_list_get_card (V);
      ar_dd tail = ar_dd_card_list_get_tail (V);

      if (! ar_dd_is_one (ddm, X) && AR_DD_INDEX (X) == vindex)
	{
	  int index = AR_DD_INDEX (X);
	  int arity = AR_DD_ARITY (X);
	  ar_ddcode encoding = AR_DD_CODE (X);
	  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, card, encoding);

	  if (encoding == EXHAUSTIVE)
	    {
	      int i;

	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = AR_DD_EPNTHSON (X,i);

		  AR_DD_ENTHDD (sons, i) = ar_dd_pick (ddm, son, tail);
		  if (AR_DD_ENTHDD (sons, i) != ddm->zero)
		    break;
		}
	      
	      for (i++; i < arity; i++)
		AR_DD_ENTHDD (sons, i) = AR_DD_DUP (ddm->zero);
	    }
	  else
	    {
	      int i;
	      int j = 0;
	      int found = 0;

	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = AR_DD_CPNTHSON (X, i);
		  int min = AR_DD_MIN_OFFSET_NTHSON (X, i);
		  int max = AR_DD_MAX_OFFSET_NTHSON (X, i);
		  ar_dd son_element;

		  if (found)
		    son_element = AR_DD_DUP (ddm->zero);
		  else 
		    {
		      son_element = ar_dd_pick (ddm, son, tail);
		      found = son_element != ddm->zero;
		    }

		  ccl_assert (min == j);

		  AR_DD_MIN_OFFSET_NTHDD (sons, j) = min;
		  AR_DD_MAX_OFFSET_NTHDD (sons, j) = min;
		  AR_DD_CNTHDD (sons, j) = son_element;
		  for (j++; j <= max; j++)
		    {
		      AR_DD_MIN_OFFSET_NTHDD (sons, j) = j;
		      AR_DD_MAX_OFFSET_NTHDD (sons, j) = j;
		      AR_DD_CNTHDD (sons, j) = AR_DD_DUP (ddm->zero);
		    }
		}
	      ccl_assert (j == card);
	    }
	  R = ar_dd_find_or_add (ddm, index, card, encoding, sons);
	  ar_dd_free_dd_table (ddm, card, encoding, sons);
	}
      else
	{
	  R = ar_dd_pick (ddm, X, tail);

	  if (R != ddm->zero)
	    {
	      ar_ddcode encoding = ar_dd_card_list_get_encoding (V);
	      ar_dd tmp1 = 
		ar_dd_var_in_range (ddm, AR_DD_INDEX (V), card, encoding, 0, 0);
	      ar_dd tmp2 = ar_dd_compute_and (ddm, R, tmp1);
	      AR_DD_FREE (tmp1);
	      AR_DD_FREE (R);
	      R = tmp2;
	    }
	}
    }

  return R;

}

			/* --------------- */

int
ar_dd_is_included_in (ar_ddm ddm, ar_dd X, ar_dd Y)
{
  int R;
  memo_record rec;

  if (X == ddm->one)
    R = (Y == ddm->one);
  else if (X == ddm->zero)
    R = 1;
  else if (Y == ddm->one)
    R = 1;
  else if (Y == ddm->zero)
    R = (X == ddm->zero);
  else if (X == Y)
    R = 1;
  else if ((rec = s_get_record_for_operation (ddm, ar_dd_is_included_in, X, Y)) 
	   != NULL)
    R = (int) OPERATION_DOUBLE_RESULT (rec);
  else
    {
      int xarity = AR_DD_ARITY (X);
      int yarity = AR_DD_ARITY (Y);

      R = 1;

      if (AR_DD_INDEX (X) == AR_DD_INDEX (Y))    
	{
	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      int i;

	      for (i = 0; R && i < xarity; i++)
		R = ar_dd_is_included_in (ddm, AR_DD_EPNTHSON (X, i),
					  AR_DD_EPNTHSON (Y, i));
	    }
	  else
	    {
	      int x = 0;
	      int xmin = AR_DD_MIN_OFFSET_NTHSON (X, x);
	      int xmax = AR_DD_MAX_OFFSET_NTHSON (X, x);
	      ar_dd xson = AR_DD_CPNTHSON (X, x) ;
	      int y = 0;
	      int ymin = AR_DD_MIN_OFFSET_NTHSON (Y, y);
	      int ymax = AR_DD_MAX_OFFSET_NTHSON (Y, y);
	      ar_dd yson = AR_DD_CPNTHSON (Y, y) ;

	      while (R && (x < xarity) && (y < yarity))
		{
		  ccl_assert (xmin == ymin);

		  R = ar_dd_is_included_in (ddm, xson, yson);
		  xmin++;
		  if (xmin > xmax)
		    {
		      x++;
		      if (x < xarity)
			{
			  xmin = AR_DD_MIN_OFFSET_NTHSON (X, x);
			  xmax = AR_DD_MAX_OFFSET_NTHSON (X, x);
			  xson = AR_DD_CPNTHSON (X, x) ;
			}
		    }
		  ymin++;
		  if (ymin > ymax)
		    {
		      y++;
		      if (y < yarity)
			{
			  ymin = AR_DD_MIN_OFFSET_NTHSON (Y, y);
			  ymax = AR_DD_MAX_OFFSET_NTHSON (Y, y);
			  yson = AR_DD_CPNTHSON (Y, y) ;
			}
		    }
		}
	      R = R && (x == xarity) && (y == yarity);
	    }
	}
      else if (AR_DD_INDEX (X) < AR_DD_INDEX (Y))
	{
	  int i;

	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      for (i = 0; R && i < xarity; i++)
		R = ar_dd_is_included_in (ddm, AR_DD_EPNTHSON (X, i), Y);
	    }
	  else
	    {
	      for (i = 0; R && i < xarity; i++)
		R = ar_dd_is_included_in (ddm, AR_DD_CPNTHSON (X, i), Y);
	    }	  
	}
      else 
	{
	  int i;

	  ccl_assert (AR_DD_INDEX (X) > AR_DD_INDEX (Y));

	  if (AR_DD_CODE (Y) == EXHAUSTIVE)
	    {
	      for (i = 0; R && i < yarity; i++)
		R = ar_dd_is_included_in (ddm, X, AR_DD_EPNTHSON (Y, i));
	    }
	  else
	    {
	      for (i = 0; R && i < yarity; i++)
		R = ar_dd_is_included_in (ddm, X, AR_DD_CPNTHSON (Y, i));
	    }	  
	}
      ar_dd_memoize_double_operation (ddm, ar_dd_is_included_in, X, Y,
				      (double) R);
    }

  return R;
}

			/* --------------- */

int
ar_dd_intersect (ar_ddm ddm, ar_dd X, ar_dd Y)
{
  int R;
  memo_record rec;

  if (X == ddm->zero || Y == ddm->zero)
    R = 0;
  else if (X == ddm->one || Y == ddm->one)
    R = 1;
  else if (X == Y)
    R = 1;
  else if ((rec = s_get_record_for_operation (ddm, ar_dd_intersect, X, Y)) 
	   != NULL)
    R = (int) OPERATION_DOUBLE_RESULT (rec);
  else
    {
      int xarity = AR_DD_ARITY (X);
      int yarity = AR_DD_ARITY (Y);

      R = 0;

      if (AR_DD_INDEX (X) == AR_DD_INDEX (Y))    
	{
	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      int i;

	      for (i = 0; R == 0 && i < xarity; i++)
		R = ar_dd_intersect (ddm, AR_DD_EPNTHSON (X, i),
				     AR_DD_EPNTHSON (Y, i));
	    }
	  else
	    {
	      int x = 0;
	      int xmin = AR_DD_MIN_OFFSET_NTHSON (X, x);
	      int xmax = AR_DD_MAX_OFFSET_NTHSON (X, x);
	      ar_dd xson = AR_DD_CPNTHSON (X, x) ;
	      int y = 0;
	      int ymin = AR_DD_MIN_OFFSET_NTHSON (Y, y);
	      int ymax = AR_DD_MAX_OFFSET_NTHSON (Y, y);
	      ar_dd yson = AR_DD_CPNTHSON (Y, y) ;

	      while (R == 0 && (x < xarity) && (y < yarity))
		{
		  ccl_assert (xmin == ymin);

		  R = ar_dd_intersect (ddm, xson, yson);
		  xmin++;
		  if (xmin > xmax)
		    {
		      x++;
		      if (x < xarity)
			{
			  xmin = AR_DD_MIN_OFFSET_NTHSON (X, x);
			  xmax = AR_DD_MAX_OFFSET_NTHSON (X, x);
			  xson = AR_DD_CPNTHSON (X, x) ;
			}
		    }
		  ymin++;
		  if (ymin > ymax)
		    {
		      y++;
		      if (y < yarity)
			{
			  ymin = AR_DD_MIN_OFFSET_NTHSON (Y, y);
			  ymax = AR_DD_MAX_OFFSET_NTHSON (Y, y);
			  yson = AR_DD_CPNTHSON (Y, y) ;
			}
		    }
		}
	    }
	}
      else if (AR_DD_INDEX (X) < AR_DD_INDEX (Y))
	{
	  int i;

	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      for (i = 0; R == 0 && i < xarity; i++)
		R = ar_dd_intersect (ddm, AR_DD_EPNTHSON (X, i), Y);
	    }
	  else
	    {
	      for (i = 0; R == 0 && i < xarity; i++)
		R = ar_dd_intersect (ddm, AR_DD_CPNTHSON (X, i), Y);
	    }	  
	}
      else 
	{
	  int i;

	  ccl_assert (AR_DD_INDEX (X) > AR_DD_INDEX (Y));

	  if (AR_DD_CODE (Y) == EXHAUSTIVE)
	    {
	      for (i = 0; R == 0 && i < yarity; i++)
		R = ar_dd_intersect (ddm, X, AR_DD_EPNTHSON (Y, i));
	    }
	  else
	    {
	      for (i = 0; R == 0 && i < yarity; i++)
		R = ar_dd_intersect (ddm, X, AR_DD_CPNTHSON (Y, i));
	    }	  
	}
      ar_dd_memoize_double_operation (ddm, ar_dd_intersect, X, Y, 
				      (double) R);
    }

  return R;
}

			/* --------------- */

static void
s_display_node_on_map (ar_ddm ddm, ar_dd F, FILE *out)
{
  int i;

  if (AR_DD_ADDR (F) == ddm->one)
    return;

  if (AR_DD_INDEX(F) >= 0)
    {
      for (i = 0; i < AR_DD_ARITY(F); i++) 
	{
	  ar_dd son = (AR_DD_CODE (F) == COMPACTED) 
	    ? AR_DD_CNTHSON (F, i) 
	    : AR_DD_ENTHSON (F, i);
	  fpf (out,"%ld %ld\n", (intptr_t) AR_DD_ADDR (F), (intptr_t) son);
	}
    }
  else { /* Do nothing */ }
}

			/* --------------- */

void
ar_dd_display_node_map (ar_ddm ddm, FILE *out)
{
  int i;

  for (i = 0; i < ddm->ht_actual_size; i++)
    {
      ar_dd dd = ddm->ht_nodes[i];
      while (dd != NULL)
	{
	  if (dd->refcount > 0)
	    s_display_node_on_map (ddm, dd, out);
	  dd = AR_DD_NEXT (dd);
	}
    }
}

			/* --------------- */

void
ar_dd_display_cache_map (ar_ddm ddm, FILE *out)
{
  int i;
  memo_record rec;

  for (rec = ddm->op_cache, i = 0; i < ddm->op_actual_size; i++, rec++)
    {
      if (AR_DD_POLARITY (IF_SON (rec)) || IF_SON (rec) == 0)
	continue;

      fpf (out, "%ld %ld\n", (intptr_t) AR_DD_ADDR (RESULT_SON (rec)),
	   (intptr_t) AR_DD_ADDR (IF_SON (rec)));
      fpf (out, "%ld %ld\n", (intptr_t) AR_DD_ADDR (RESULT_SON (rec)),
	   (intptr_t) AR_DD_ADDR (THEN_SON (rec)));
      fpf (out, "%ld %ld\n", (intptr_t) AR_DD_ADDR (RESULT_SON (rec)),
	   (intptr_t) AR_DD_ADDR (ELSE_SON (rec)));
    }
}

uintptr_t
ar_dd_get_number_of_paths (ar_ddm ddm, ar_dd X)
{
  uintptr_t R;
  memo_record rec;

  if (ar_dd_is_zero (ddm, X))
    return 0;
  if (ar_dd_is_one (ddm, X))
    return 1;

  rec = s_get_record_for_operation (ddm, ar_dd_get_number_of_paths, X, NULL);
  if (rec != NULL)
    R = (uintptr_t) OPERATION_RESULT(rec);
  else
    {
      int xarity = AR_DD_ARITY (X);

      R = 0;

      if (AR_DD_CODE (X) == EXHAUSTIVE)
	{
	  int i;
	  
	  for (i = 0; i < xarity; i++)
	    R += ar_dd_get_number_of_paths (ddm, AR_DD_EPNTHSON (X, i));
	}
      else
	{
	  int x;

	  for (x = 0; x < xarity; x++)
	    {
	      int xmin = AR_DD_MIN_OFFSET_NTHSON (X, x);
	      int xmax = AR_DD_MAX_OFFSET_NTHSON (X, x);
	      ar_dd xson = AR_DD_CPNTHSON (X, x) ;	      
	      uintptr_t nbp = ar_dd_get_number_of_paths (ddm, xson);

	      R +=  (xmax - xmin + 1) * nbp;
	    }
	}
      ar_dd_memoize_operation (ddm, ar_dd_get_number_of_paths, X, NULL, 
			       (void *) R);
    }

  return R;
}

ar_dd
ar_dd_pick_one_path (ar_ddm ddm, ar_dd F)
{
  ar_dd R;

  if (ar_dd_is_one (ddm, F) || ar_dd_is_zero (ddm, F))
    R = F;
  else 
    R = ar_dd_entry_for_operation (ddm, ar_dd_pick_one_path, F, NULL);

  if (R != NULL)
    R = AR_DD_DUP (R);
  else
    {
      int index = AR_DD_INDEX (F);
      int arity = AR_DD_ARITY (F);
      ar_ddcode encoding = AR_DD_CODE (F);
      ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);

      if (encoding == EXHAUSTIVE)
	{
	  int i;
	  
	  for (i = 0; i < arity; i++)
	    {
	      ar_dd son = AR_DD_EPNTHSON (F, i);
	      
	      AR_DD_ENTHDD (sons, i) = ar_dd_pick_one_path (ddm, son);
	      if (AR_DD_ENTHDD (sons, i) != ddm->zero)
		break;
	    }
	  
	  for (i++; i < arity; i++)
	    AR_DD_ENTHDD (sons, i) = AR_DD_DUP (ddm->zero);
	}
      else
	{
	  int i;
	  int found = 0;

	  for (i = 0; i < arity; i++)
	    {
	      ar_dd son = AR_DD_CPNTHSON (F, i);
	      int min = AR_DD_MIN_OFFSET_NTHSON (F, i);
	      int max = AR_DD_MAX_OFFSET_NTHSON (F, i);
	      ar_dd son_element;
	      
	      if (found)
		son_element = AR_DD_DUP (ddm->zero);
	      else 
		{
		  son_element = ar_dd_pick_one_path (ddm, son);
		  found = son_element != ddm->zero;
		}
	      
	      AR_DD_MIN_OFFSET_NTHDD (sons, i) = min;
	      AR_DD_MAX_OFFSET_NTHDD (sons, i) = max;
	      AR_DD_CNTHDD (sons, i) = son_element;
	    }
	}
      R = ar_dd_find_or_add (ddm, index, arity, encoding, sons);
      ar_dd_free_dd_table (ddm, arity, encoding, sons);
      ar_dd_memoize_operation (ddm, ar_dd_pick_one_path, F, NULL, R);
    }

#if CCL_ENABLE_ASSERTIONS
  {
    ar_dd tmp = ar_dd_compute_and (ddm, F, R);
    ccl_assert (tmp == R);
    AR_DD_FREE (tmp);
  }
#endif

  return R;
}

ar_dd 
ar_dd_pick_min_path (ar_ddm ddm, ar_dd F, int *D)
{
  ar_dd R;

  if (ar_dd_is_one (ddm, F) || ar_dd_is_zero (ddm, F))
    R = F;
  else 
    R = ar_dd_entry_for_operation (ddm, ar_dd_pick_min_path, F, NULL);

  if (R != NULL)
    {
      if (ar_dd_is_zero (ddm, R))
	*D = INT_MAX;
      else
	*D = ar_dd_get_number_of_nodes (R);
      R = AR_DD_DUP (R);
    }
  else
    {
      int index = AR_DD_INDEX (F);
      int arity = AR_DD_ARITY (F);
      ar_ddcode encoding = AR_DD_CODE (F);
      ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);
      int i;
      int mindepth = 0;
      int minsonindex = 0;
      ar_dd son = (encoding == EXHAUSTIVE 
		   ? AR_DD_EPNTHSON (F, minsonindex) 
		   : AR_DD_CPNTHSON (F, minsonindex));
      ar_dd minson = ar_dd_pick_min_path (ddm, son, &mindepth);

      for (i = 1; i < arity; i++)
	{
	  int depth;
	  ar_dd tmp;

	  son = (encoding == EXHAUSTIVE 
		 ? AR_DD_EPNTHSON (F, i) 
		 : AR_DD_CPNTHSON (F, i));
	  tmp = ar_dd_pick_min_path (ddm, son, &depth);
	  if (depth < mindepth)
	    {
	      mindepth = depth;
	      minsonindex = i;
	      AR_DD_FREE (minson);
	      minson = tmp;
	    }
	  else
	    {
	      AR_DD_FREE (tmp);
	    }
	}

      if (encoding == EXHAUSTIVE)
	{	  
	  for (i = 0; i < minsonindex; i++)
	    AR_DD_ENTHDD (sons, i) = AR_DD_DUP (ddm->zero);
	  AR_DD_ENTHDD (sons, i) = minson;
	  for (i++; i < arity; i++)
	    AR_DD_ENTHDD (sons, i) = AR_DD_DUP (ddm->zero);
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    {
	      int min = AR_DD_MIN_OFFSET_NTHSON (F, i);
	      int max = AR_DD_MAX_OFFSET_NTHSON (F, i);
	      ar_dd son_element;
	      
	      if (i == minsonindex)
		son_element = minson;
	      else 
		son_element = AR_DD_DUP (ddm->zero);
	      
	      AR_DD_MIN_OFFSET_NTHDD (sons, i) = min;
	      AR_DD_MAX_OFFSET_NTHDD (sons, i) = max;
	      AR_DD_CNTHDD (sons, i) = son_element;
	    }
	}
      *D = mindepth + 1;
      R = ar_dd_find_or_add (ddm, index, arity, encoding, sons);
      ar_dd_free_dd_table (ddm, arity, encoding, sons);
      ar_dd_memoize_operation (ddm, ar_dd_pick_min_path, F, NULL, R);
    }

#if CCL_ENABLE_ASSERTIONS
  {
    ar_dd tmp = ar_dd_compute_and (ddm, F, R);
    ccl_assert (tmp == R);
    AR_DD_FREE (tmp);
  }
#endif
  return R;
}

static void
s_make_prime (ar_ddm ddm, ar_dd F, ar_dd *pp)
{
  ar_dd vars;
  ar_dd v;
  ar_dd res;

  if (ar_dd_is_zero (ddm, *pp) || ar_dd_is_one (ddm, *pp))
    return;

  res = AR_DD_DUP (*pp);
  
  ccl_pre (ar_dd_is_included_in (ddm, res, F));
  
  v = vars = ar_dd_get_variables (ddm, res);
  
  while (! ar_dd_is_one (ddm, v))
    {
      int index = ar_dd_projection_list_get_new_index (v);
      ar_dd tmp = ar_dd_project_variable (ddm, res, index);

      if (ar_dd_is_included_in (ddm, tmp, F))
	{
	  AR_DD_FREE (res);
	  res = tmp;
	}
      else
	{
	  AR_DD_FREE (tmp);
	}
      v = ar_dd_projection_list_get_tail (v);
    }
  AR_DD_FREE (*pp);
  AR_DD_FREE (vars);
  *pp = res;
}

static ar_dd 
s_make_cut_rec (ar_ddm ddm, ar_dd p)
{
  ar_dd R;
  
  if (ar_dd_is_zero (ddm, p) || ar_dd_is_one (ddm, p))
    R = AR_DD_DUP (p);
  else
    {
      int index = AR_DD_INDEX (p);
      int arity = AR_DD_ARITY (p);
      ar_ddcode encoding = AR_DD_CODE (p);
      ar_dd zeroson = (encoding == EXHAUSTIVE ? AR_DD_EPNTHSON (p, 0):
		       AR_DD_CPNTHSON (p, 0));
	
      ccl_assert (arity == 2);
      if (ar_dd_is_zero (ddm, zeroson))
	{
	  ar_ddtable sons = ar_dd_allocate_dd_table (ddm, arity, encoding);
	  if (encoding == EXHAUSTIVE)
	    {
	      AR_DD_ENTHDD (sons, 0) = AR_DD_DUP (ddm->zero);
	      AR_DD_ENTHDD (sons, 1) = s_make_cut_rec (ddm,
						       AR_DD_EPNTHSON (p, 1));
	    }
	  else
	    {
	      AR_DD_MIN_OFFSET_NTHDD (sons, 0) = 0;
	      AR_DD_MAX_OFFSET_NTHDD (sons, 0) = 0;
	      AR_DD_CNTHDD (sons, 0) = AR_DD_DUP (ddm->zero);
	      AR_DD_MIN_OFFSET_NTHDD (sons, 1) = 1;
	      AR_DD_MAX_OFFSET_NTHDD (sons, 1) = 1;
	      AR_DD_CNTHDD (sons, 1) = s_make_cut_rec (ddm,
						       AR_DD_EPNTHSON (p, 1));
	    }
	  R = ar_dd_find_or_add (ddm, index, arity, encoding, sons);
	  ar_dd_free_dd_table (ddm, arity, encoding, sons);
	}
      else
	{
	  R = s_make_cut_rec (ddm, AR_DD_EPNTHSON (p, 0));
	}
    }
  
  return (R);
}

static void
s_make_cut (ar_ddm ddm, ar_dd F, ar_dd *pp)
{
  ar_dd res;

  s_make_prime (ddm, F, pp);
  res = s_make_cut_rec (ddm, *pp);
  AR_DD_FREE (*pp);
  *pp = res;
}

ar_dd 
ar_dd_pick_prime (ar_ddm ddm, ar_dd *pF)
{
  int D;
  ar_dd result, F;
  
  ccl_pre (ddm != NULL);
  ccl_pre (pF != NULL);
  ccl_pre (*pF != NULL);

  F = AR_DD_DUP (*pF);
  result = ar_dd_pick_min_path (ddm, F, &D);
  s_make_prime (ddm, F, &result);
  AR_DD_FREE (*pF);
  *pF = ar_dd_compute_diff (ddm, F, result);
  AR_DD_FREE (F);

  return result;
}

ar_dd 
ar_dd_pick_cut (ar_ddm ddm, ar_dd *pF)
{
  int D;
  ar_dd result, F;
  
  ccl_pre (ddm != NULL);
  ccl_pre (pF != NULL);
  ccl_pre (*pF != NULL);

  F = AR_DD_DUP (*pF);
  result = ar_dd_pick_min_path (ddm, F, &D);
  s_make_cut (ddm, F, &result);
  AR_DD_FREE (*pF);
  *pF = ar_dd_compute_diff (ddm, F, result);
  AR_DD_FREE (F);

  return result;
}
