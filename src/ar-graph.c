/*
 * ar-graph.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-hash.h>
#include "ar-graph.h"

struct ar_graph_st {
  int refcount;
  void *label;
  const ar_graph_label_methods *g_methods;
  const ar_graph_label_methods *v_methods;
  const ar_graph_label_methods *e_methods;
  ccl_hash *l2v_table;
  ccl_list *vertices;
  ccl_list *edges;
};

			/* --------------- */

struct ar_graph_vertex_st {
  void *label;
  ar_graph *g;
  ccl_list *out;
  ccl_list *in;
  int flags;
};

			/* --------------- */

struct ar_graph_edge_st {
  ar_graph_vertex *src;
  void *label;
  ar_graph_vertex *tgt;
};

			/* --------------- */

ar_graph *
ar_graph_create(void *graph_label, 
		const ar_graph_label_methods *graph_methods,
		const ar_graph_label_methods *vertex_methods,
		const ar_graph_label_methods *edge_methods)
{
  ar_graph *result = ccl_new(ar_graph);

  result->refcount = 1;
  result->label = graph_label;
  result->g_methods = graph_methods;
  result->v_methods = vertex_methods;
  result->e_methods = edge_methods;
  if( vertex_methods )
    result->l2v_table = ccl_hash_create(vertex_methods->hash,
					vertex_methods->cmp,
					vertex_methods->del,
					NULL);
  else
    result->l2v_table = ccl_hash_create(NULL,NULL,NULL,NULL);
  result->vertices = ccl_list_create();
  result->edges = ccl_list_create();

  return result;
}

			/* --------------- */

ar_graph *
ar_graph_add_reference(ar_graph *g)
{
  ccl_pre( g != NULL );
  
  g->refcount++;

  return g;
}

			/* --------------- */

void
ar_graph_del_reference(ar_graph *g)
{
  ccl_pair *p;

  ccl_pre( g != NULL ); ccl_pre( g->refcount > 0 );

  if( --g->refcount > 0 )
    return;


  if( g->g_methods && g->g_methods->del )
    g->g_methods->del(g->label);
  
  ccl_hash_delete(g->l2v_table);
  
  for(p = FIRST(g->vertices); p; p = CDR(p))
    {
      ar_graph_vertex *v = (ar_graph_vertex *)CAR(p);
      if( g->v_methods && g->v_methods->del )
	g->v_methods->del(v->label);
      ccl_list_delete(v->in);
      ccl_list_delete(v->out);
      ccl_delete(v);
    }
  ccl_list_delete(g->vertices);
  for(p = FIRST(g->edges); p; p = CDR(p))
    {
      ar_graph_edge *e = (ar_graph_edge *)CAR(p);
      if( g->e_methods && g->e_methods->del )
	    g->e_methods->del(e->label);
      ccl_delete(e);
    }
  ccl_list_delete(g->edges);
  ccl_delete(g);
}

			/* --------------- */

void *
ar_graph_get_label(ar_graph *g)
{
  ccl_pre( g != NULL );

  return g->label;
}

			/* --------------- */

int
ar_graph_get_number_of_vertices(ar_graph *g)
{
  ccl_pre( g != NULL );

  return ccl_list_get_size(g->vertices);
}

			/* --------------- */

int
ar_graph_get_number_of_edges(ar_graph *g)
{
  ccl_pre( g != NULL );

  return ccl_list_get_size(g->edges);
}

			/* --------------- */

void
ar_graph_log(ccl_log_type log, ar_graph *g)
{
  
}

			/* --------------- */

ccl_list *
ar_graph_get_vertices(ar_graph *g)
{
  ccl_pre( g != NULL );

  return g->vertices;
}

			/* --------------- */

ccl_list *
ar_graph_get_edges(ar_graph *g)
{
  ccl_pre( g != NULL );

  return g->edges;
}

			/* --------------- */

ar_graph_vertex *
ar_graph_add_vertex(ar_graph *g, void *label)
{
  ar_graph_vertex *result;

  if( ccl_hash_find(g->l2v_table,label) )
    result = ccl_hash_get(g->l2v_table);
  else
    {
      result = ccl_new(ar_graph_vertex);
      result->label = label;
      result->g = g;
      result->out = ccl_list_create();
      result->in = ccl_list_create();

      ccl_hash_insert(g->l2v_table,result);
      ccl_list_add(g->vertices,result);
    }

  return result;
}

			/* --------------- */

ar_graph_vertex *
ar_graph_get_vertex(ar_graph *g, void *label)
{
  ar_graph_vertex *result = NULL;

  if( ccl_hash_find(g->l2v_table,label) )
    result = ccl_hash_get(g->l2v_table);

  return result;
}

			/* --------------- */

void *
ar_graph_vertex_get_label(ar_graph_vertex *v)
{
  ccl_pre( v != NULL );
    
  return v->label;
}

			/* --------------- */

ccl_list *
ar_graph_vertex_get_out_edges(ar_graph_vertex *v)
{
  ccl_pre( v != NULL );
    
  return v->out;
}

			/* --------------- */

ccl_list *
ar_graph_vertex_get_in_edges(ar_graph_vertex *v)
{
  ccl_pre( v != NULL );
    
  return v->in;
}

			/* --------------- */

ar_graph_edge *
ar_graph_add_edge(ar_graph *g, ar_graph_vertex *v1, void *label, 
		  ar_graph_vertex *v2)
{
  ar_graph_edge * result;
  ccl_pair *p;

  ccl_pre( g != NULL ); ccl_pre( v1 != NULL ); ccl_pre( v2 != NULL );
  ccl_pre( v1->g == v2->g );

  if( g->e_methods == NULL || g->e_methods->cmp == NULL )
    {
      for(p = FIRST(v1->out); p; p = CDR(p))
	{
	  ar_graph_edge *e = (ar_graph_edge *)CAR(p);
	  
	  if( e->src == v1 && e->tgt == v2 && e->label == label )
	    return e;	      
	}
    }
  else
    {
      for(p = FIRST(v1->out); p; p = CDR(p))
	{
	  ar_graph_edge *e = (ar_graph_edge *)CAR(p);
	  
	  if( e->src == v1 && e->tgt == v2 && 
	      g->e_methods->cmp(e->label,label) == 0 )
	    return e;	      
	}
    }
  result = ccl_new(ar_graph_edge);
  result->src = v1;
  result->label = label;
  result->tgt = v2;

  ccl_list_add(v1->out,result);
  ccl_list_add(v2->in,result);
  ccl_list_add(v1->g->edges,result);

  return result;
}

			/* --------------- */

ar_graph_vertex *
ar_graph_edge_get_src(ar_graph_edge *e)
{
  ccl_pre( e != NULL );
  
  return e->src;
}

			/* --------------- */

void *
ar_graph_edge_get_label(ar_graph_edge *e)
{
  ccl_pre( e != NULL );
  
  return e->label;
}

			/* --------------- */

ar_graph_vertex *
ar_graph_edge_get_tgt(ar_graph_edge *e)
{
  ccl_pre( e != NULL );
  
  return e->tgt;
}

			/* --------------- */

# define VISITED  1
# define IN_STACK 2
CCL_DEFINE_EXCEPTION(not_a_dag_exception,exception);

typedef struct dfs_stack_st {
  ar_graph_vertex *v;
  struct dfs_stack_st *tail;
} dfs_stack;

static void
s_topological_order_dfs(dfs_stack *stack, ccl_list *order)
{
  ccl_pair *p;
  ar_graph_vertex *v = stack->v;

  if( (v->flags & IN_STACK) != 0 )
    {
      dfs_stack *s = stack;

      ccl_list_clear(order,NULL);
      do {
	ccl_list_put_first(order,s->v);
	s = s->tail;
	ccl_assert( s != NULL );
      } while( s->v != v );

      ccl_throw_no_msg (not_a_dag_exception);
    }

  v->flags |= IN_STACK;

  if( (v->flags & VISITED) == 0 )
    {
      v->flags |= VISITED;

      for(p = FIRST(v->out); p; p = CDR(p))
	{
	  ar_graph_edge *e = (ar_graph_edge *)CAR(p);
	  dfs_stack s;	
  
	  s.v = e->tgt;
	  s.tail = stack;
	  s_topological_order_dfs(&s,order);
	}
      ccl_list_add(order,v);
    }

  v->flags &= ~IN_STACK;
}

			/* --------------- */

static int
s_get_topological_order(ar_graph *g, ar_graph_vertex *v, ccl_list *order)
{
  int acyclic_graph = 1;
  ccl_pair *p;
  dfs_stack s;

  ccl_pre( ar_graph_get_number_of_vertices(g) > 0 );

  for(p = FIRST(g->vertices); p; p = CDR(p))
    ((ar_graph_vertex *)CAR(p))->flags = 0;

  ccl_try(not_a_dag_exception) 
    {
      if( v == NULL )
	{
	  for(p = FIRST(g->vertices); p; p = CDR(p))
	    {
	      ar_graph_vertex *v = (ar_graph_vertex *)CAR(p);
	      
	      if( (v->flags & VISITED) == 0 )
		{
		  s.v = v;
		  s.tail = NULL;
		  s_topological_order_dfs(&s,order);
		}
	    }
	}
      else
	{
	  s.v = v;
	  s.tail = NULL;
	  s_topological_order_dfs(&s,order);
	}
    }
  ccl_catch
    {
      acyclic_graph = 0;
    }
  ccl_end_try;

  return acyclic_graph;
}

			/* --------------- */


int
ar_graph_get_topological_order(ar_graph *g, ccl_list *order)
{
  return s_get_topological_order(g,NULL,order);
}

			/* --------------- */

int
ar_graph_get_rooted_topological_order(ar_graph *g, ar_graph_vertex *v,
				      ccl_list *order)
{
  return s_get_topological_order(g,v,order);
}

			/* --------------- */

static void
s_make_dfs (ar_graph_vertex *v)
{
  ccl_pair *p;

  v->flags |= VISITED;
  for(p = FIRST(v->out); p; p = CDR(p))
    {
      ar_graph_edge *e = (ar_graph_edge *)CAR(p);

      if ((e->tgt->flags & VISITED) == 0)
	s_make_dfs (e->tgt);
    }
}

			/* --------------- */

static int
s_is_ap (ar_graph *g, ar_graph_vertex *v)
{
  int nb_succ_visited = 0;
  ccl_pair *p;

  for(p = FIRST(g->vertices); p; p = CDR(p))
    ((ar_graph_vertex *)CAR(p))->flags = 0;

  v->flags |= VISITED;

  for(p = FIRST(v->out); p; p = CDR(p))
    {
      ar_graph_edge *e = (ar_graph_edge *)CAR(p);

      if ((e->tgt->flags & VISITED) == 0)
	{
	  nb_succ_visited++;
	  s_make_dfs (e->tgt);
	}
    }

  return nb_succ_visited >= 2;
}

			/* --------------- */

ccl_list *
ar_graph_get_articulation_points (ar_graph *g)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create ();

  for(p = FIRST(g->vertices); p; p = CDR(p))
    if (s_is_ap (g, CAR (p)))
      ccl_list_add (result, CAR (p));

  return result;
}
