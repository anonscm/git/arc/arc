/*
 * ar-rel-semantics.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <limits.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-stack.h>
#include <ccl/ccl-memory.h>
#include <math.h>
#include "ar-model.h"
#include "mec5/mec5.h"
#include "mec5/mec5-acheck-rels.h"
#include "ar-help.h"
#include "ar-ca-expr2dd.h"
#include "ar-constraint-automaton.h"
#include "ar-semantics.h"
#include "ar-dd.h"
#include "ar-state-graph-semantics-p.h"
#include "ar-acheck-builtins.h"
#include "ar-acheck-preferences.h"
#include "ar-rel-semantics.h"

#define DOT_DIFF_STATE_MODE_DEFAULT DOT_DIFF_STATE_MODE_NONE
#define DOT_DIFF_STATE_MODE_NONE 0x0
#define DOT_DIFF_STATE_MODE_PRE  0x1
#define DOT_DIFF_STATE_MODE_POST 0x2
#define DOT_DIFF_STATE_MODE_BOTH \
 (DOT_DIFF_STATE_MODE_POST|DOT_DIFF_STATE_MODE_PRE)


#define POSTPRE_USE_GLOBAL_TRANSITION_RELATION 1
#define POSTPRE_WITH_ONE_ASSERTION 1
#define PRIME_BEFORE_NON_PRIME 0
#define DD_INTERLEAVING_SCALE 5

#define COUNTER_BITS 24
#define COUNTER_ENCODING COMPACTED

#define SGS(rs) ((ar_sgs *)(rs))
#define SGS_SET(d) ((ar_sgs_set *)(d))
#define RS(sgs) ((ar_relsem *)(sgs))
#define RS_SET(s) ((relsem_set *)(s))
#define SETS(s) (RS_SET (s)->sets)
#define DDS(s) (SETS(s).data)
#define DD(s,_i_) (DDS(s)[_i_])
#define WIDTH(s) (SETS(s).size)

#define MAKE_IT_PRIME(rs, R) \
  ar_dd_project_on_variables ((rs)->ddm, (R), (rs)->to_prime)
#define IS_TRIVIAL(rs, R) \
  (ar_dd_one ((rs)->ddm) == (R) || ar_dd_zero ((rs)->ddm) == (R))

struct ar_relational_semantics_st 
{
  ar_sgs super;
  ar_ca_exprman *eman;

  ar_ddm ddm;
  
  varorder *order;

  ar_idtable *event_indexes;
  ar_idtable *event_types;
  
  ccl_hash *is_state_variable;
  ccl_list *ordered_state_variables;
  ar_ca_expr **prime_variables; 

  ar_dd assertion;

  int nb_assertions;
  ar_dd *assertions;
  ar_dd *post_assertions;

  ar_dd post_reachables;
  ar_dd initial;
  ar_dd reachables;

  int nb_transitions;
  int has_epsilon;
  ar_dd global_transition_relation; 
  ar_dd *transition_relations; 

  int nb_events;
  ar_dd *event_relations;
  ar_dd *event_dds;
  ar_dd event_pl;
  
  /* lists of variables that are used to compute cardinalities of DDs:
   * - all_variables is the list of all transition relations
   * - not_primed_variables is the list for configuration relations
   */
  ar_dd all_variables;
  ar_dd not_primed_variables;

  /* lists used for projections of DDs:
   * - to_prime is used to convert non prime variables into prime variables 
   * - to_non_prime is used to convert prime variables into non prime variables
   * - non_prime is used to projection on non prime variables 
   *   variable into non-prime space 
   * - state_variables is used to projection on state variables only 
   * - flow_variables is used to projection on flow variables only
   */
  ar_dd to_prime;
  ar_dd to_non_prime;
  ar_dd non_prime;
  ar_dd state_variables;
  ar_dd flow_variables;

  int max_dd_index;

  /* projection lists for join */
  ar_dd prime_to_second;     /* V->V; V'-> V" */
  ar_dd non_prime_to_prime;  /* V->V'; V"-> V". Assume no V' in relation */
  ar_dd prime;               /* V'->V' used for variables removal */
  ar_dd second_to_prime;     /* V->V; V"-> V'. Assume no V' in relation */
    
};

typedef struct relsem_set_st
{
  ar_sgs_set super;
  ar_dd_array sets;
} relsem_set;

			/* --------------- */

#define DBG_IS_ON (ccl_debug_is_on && ar_relsem_debug_is_on)
#define DBG(flag_) (DBG_IS_ON && ar_relsem_debug_ ## flag_)
#define DBG_START_TIMER(args_) \
  do { if (DBG_IS_ON) CCL_DEBUG_START_TIMED_BLOCK (args_); } while (0)
#define DBG_END_TIMER() \
  do { if (DBG_IS_ON) CCL_DEBUG_END_TIMED_BLOCK (); } while (0)
#define DBG_F_START_TIMER(f_, args_)					\
  do { if (DBG (f_)) CCL_DEBUG_START_TIMED_BLOCK (args_); } while (0)
#define DBG_F_END_TIMER(f_) \
  do { if (DBG (f_)) CCL_DEBUG_END_TIMED_BLOCK (); } while (0)

int ar_relsem_debug_is_on = 0;
int ar_relsem_debug_log_order = 1;
int ar_relsem_debug_comp_initial = 1;
int ar_relsem_debug_comp_post = 1;
int ar_relsem_debug_comp_transrel = 1; 
int ar_relsem_debug_comp_reach = 1;

static ar_relsem *
s_build_relsem (ar_ca *ca, 
		void (*order_variables) (ar_ca *ca, varorder *order, void *cd),
		void *clientdata);

static void
s_compute_assertions (ar_relsem *rs);

static void
s_compute_assertion (ar_relsem *rs);

static void
s_compute_post_assertions (ar_relsem *rs);

static void
s_compute_initial (ar_relsem *rs);

static void
s_compute_reachables (ar_relsem *rs);

static void
s_make_prime_variables (ar_relsem *rs);

static void
s_make_ordering_for_events (ar_relsem *rs);

#if POSTPRE_USE_GLOBAL_TRANSITION_RELATION==1
static void
s_compute_global_transition_relation (ar_relsem *rs);
#endif

static void
s_compute_transition_relations (ar_relsem *rs);

static ar_dd
s_compute_ith_transition_relation (ar_relsem *rs, int tindex, ccl_hash *cache);

static ar_dd
s_compute_relation_for_transition (ar_relsem *rs, ar_ca_trans *t, 
				   ccl_hash *cache);
static ar_dd
s_compute_transition_relation (ar_relsem *rs, ar_ca_expr *guard, 
			       int nb_assignments, ar_ca_expr **assignments,
			       ccl_hash *cache);

static void
s_compute_event_relations (ar_relsem *rs);

static ar_dd 
s_post (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I);

static ar_dd
s_pre (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I);

static ar_sgs_set *
s_create_empty_set (ar_relsem *rs, int is_state);

static void
s_compute_event_types (ar_relsem *rs);

static void
s_compute_event_dds (ar_relsem *rs);

static void
s_compute_event_indexes (ar_relsem *rs);

			/* --------------- */

static int 
s_rs_sets_are_equal (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

static ccl_hash *
s_rs_get_events (ar_sgs *sgs, ar_sgs_set *T);

static ar_sgs_set * 
s_rs_get_rel_for_event (ar_sgs *sgs, ar_sgs_set *T, ar_ca_event *e);

static ar_expr *
s_rs_states_to_formula (ar_sgs *sgs, ar_sgs_set *S, ccl_list *pvars,
			ccl_list *vars, int exist);

static ar_ca_expr *
s_rs_states_to_ca_expr (ar_sgs *sgs, ar_sgs_set *S, ccl_hash *vars);

static void 
s_rs_post_add_set (ar_sgs *sgs, ar_identifier *id, ar_sgs_set *set);

static void 
s_rs_pre_remove_set (ar_sgs *sgs, ar_identifier *id);

static void 
s_rs_display_set (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *set,
		  ar_sgs_display_format df);
static void 
s_rs_destroy (ar_sgs *sgs);
static double 
s_rs_card (ar_sgs *sgs, ar_sgs_set *S);
static void 
s_rs_to_mec4 (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	      ccl_config_table *conf);
static void
s_rs_to_dot (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf);

static void 
s_rs_to_gml (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf);

static ar_sgs_set *
s_rs_assignment (ar_sgs *sgs, ar_ca_expr **assignments,  int nb_assignments);

static int
s_rs_is_empty (ar_sgs *sgs, ar_sgs_set *S);

static ar_sgs_set *
s_rs_post (ar_sgs *sgs, ar_sgs_set *X);

static ar_sgs_set *
s_rs_pre (ar_sgs *sgs, ar_sgs_set *X);

static ar_sgs_set * 
s_rs_proj (ar_sgs *sgs, ar_sgs_set *X, int on_state);
static ar_sgs_set * 
s_rs_pick (ar_sgs *sgs, ar_sgs_set *X);
static ar_sgs_set * 
s_rs_assert_config (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_rs_src (ar_sgs *sgs, ar_sgs_set *T);
static ar_sgs_set * 
s_rs_tgt (ar_sgs *sgs, ar_sgs_set *T);
static ar_sgs_set * 
s_rs_rsrc (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_rs_rtgt (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_rs_loop (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2); 
static ar_sgs_set * 
s_rs_trace (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, ar_sgs_set *S2);
static ar_sgs_set * 
s_rs_unav (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S);
static ar_sgs_set * 
s_rs_reach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);
static ar_sgs_set * 
s_rs_coreach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);
static ar_sgs_set * 
s_rs_state_set (ar_sgs *sgs, ar_ca_expr *cond);
static ar_sgs_set * 
s_rs_label (ar_sgs *sgs, ar_identifier *label);
static ar_sgs_set * 
s_rs_event_attribute (ar_sgs *sgs, ar_identifier *attribute);
static ccl_list *
s_rs_classes (ar_sgs *sgs);
static ar_sgs_set * 
s_rs_lfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);
static ar_sgs_set * 
s_rs_gfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);
static ar_sgs_set * 
s_rs_set_complement (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_rs_set_union (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
static ar_sgs_set * 
s_rs_set_intersection (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
static ar_sgs_set * 
s_rs_set_difference (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

			/* --------------- */

#define AR_ACHECK_BUILTIN(_enumid, _strid) \
  { _strid, s_predef_ ## _enumid },

static ar_sgs_set *
s_predef_ANY_C (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_ANY_S (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_EMPTY_S (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_INITIAL_STATES (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_VALID_STATE_ASSIGNMENTS (ar_sgs *sgs, const ar_identifier *id);

static const ar_sgs_predef_set PREDEFINED_STATE_SETS[] = 
{
#define AR_ACHECK_NO_BUILTIN_TRANS_SET 1
#include "ar-acheck-builtins.def"
#undef AR_ACHECK_NO_BUILTIN_TRANS_SET 
  { NULL , NULL }
};

			/* --------------- */

static ar_sgs_set *
s_predef_ANY_T (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_EMPTY_T (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_ANY_TRANS (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_EPSILON (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_SELF (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_SELF_EPSILON (ar_sgs *sgs, const ar_identifier *id);

static ar_sgs_set *
s_predef_VALID_STATE_CHANGES (ar_sgs *sgs, const ar_identifier *id);

#define s_predef_NOT_DETERMINISTIC NULL

static const ar_sgs_predef_set PREDEFINED_TRANS_SETS[] =
{
#define AR_ACHECK_NO_BUILTIN_STATE_SET 1
#include "ar-acheck-builtins.def"
#undef AR_ACHECK_NO_BUILTIN_STATE_SET 
  { NULL, NULL}
};

			/* --------------- */

static const struct ar_state_graph_semantics_methods_st RS_METHODS = 
{
  PREDEFINED_STATE_SETS,  
  PREDEFINED_TRANS_SETS,  
  s_rs_sets_are_equal,
  s_rs_get_events,
  s_rs_get_rel_for_event,
  s_rs_states_to_formula,
  s_rs_states_to_ca_expr,
  s_rs_post_add_set,
  s_rs_pre_remove_set,
  s_rs_display_set,
  s_rs_destroy,
  s_rs_card,
  s_rs_to_mec4,
  s_rs_to_dot,
  s_rs_to_gml,
  s_rs_assignment,
  s_rs_is_empty,
  s_rs_post,
  s_rs_pre,
  s_rs_proj,
  s_rs_pick,
  s_rs_assert_config,
  s_rs_src,
  s_rs_tgt,
  s_rs_rsrc,
  s_rs_rtgt,
  s_rs_loop, 
  s_rs_trace,
  s_rs_unav,
  s_rs_reach,
  s_rs_coreach,
  s_rs_state_set,
  s_rs_label,
  s_rs_event_attribute,
  s_rs_classes,
  s_rs_lfp,
  s_rs_gfp,
  s_rs_set_complement,
  s_rs_set_union,
  s_rs_set_intersection,
  s_rs_set_difference
};

			/* --------------- */

static ar_sgs_set *
s_relsem_set_create (int is_state, int width);

static void
s_rs_set_destroy (ar_sgs_set *set);

			/* --------------- */

const struct ar_state_graph_set_methods_st RS_SET_METHODS = 
{
  s_rs_set_destroy
};

			/* --------------- */

int
ar_relsem_init (ccl_config_table *conf)
{
  return 1;
}

			/* --------------- */

void
ar_relsem_terminate (void)
{
}

			/* --------------- */

static void
s_order_variables (ar_ca *ca, varorder *order, void *dummy)
{
  ar_ca_order_variables (ca, order);
}

ar_relsem *
ar_compute_relational_semantics(ar_ca *ca, 
				void (*order_variables) (ar_ca *ca, 
							 varorder *order,
							 void *clientdata),
				void *clientdata)
  CCL_THROW ((altarica_interpretation_exception, 
	      domain_cardinality_exception))
{
  ar_relsem *result;

  if (ar_ca_get_max_cardinality (ca) > AR_DD_MAX_ARITY)
    {
      ar_ca_del_reference (ca);
      ccl_throw_no_msg (domain_cardinality_exception);
    }

  if (order_variables == NULL)
    {
      order_variables = s_order_variables;
      clientdata = NULL;
    }

  result = s_build_relsem (ca, order_variables, clientdata);

  return result;
}

			/* --------------- */

ar_relsem *
ar_relsem_add_reference(ar_relsem *relsem)
{
  return RS(ar_sgs_add_reference (SGS (relsem)));
}

			/* --------------- */

void
ar_relsem_del_reference (ar_relsem *rs)
{
  ar_sgs_del_reference (SGS (rs));
}

			/* --------------- */

void
ar_relsem_log_info(ccl_log_type log, ar_relsem *rs)
{
  /*
  ccl_log(log,"size of global constraints : %g configurations\n",
	  ar_relsem_get_cardinality(rs,rs->assertion,0));
  
  ccl_log(log,"size of the initial constraints : %g configuration(s)\n",
	  ar_relsem_get_cardinality(rs,rs->initial,0));
  */
}

			/* --------------- */

ar_ca *
ar_relsem_get_automaton (ar_relsem *rs)
{
  return ar_ca_add_reference (rs->super.ca);
}

			/* --------------- */

ar_ca_exprman *
ar_relsem_get_expression_manager (ar_relsem *rs)
{
  return ar_ca_exprman_add_reference (rs->eman);
}

			/* --------------- */

ar_ddm 
ar_relsem_get_decision_diagrams_manager (ar_relsem *rs)
{
  return rs->ddm;
}

			/* --------------- */

const varorder *
ar_relsem_get_order (ar_relsem *rs)
{
  return rs->order;
}

static void s_log_int (ccl_log_type log, void *p, void *data)
{
  ccl_log (log, "%d", p);
}


static void s_log_var_deps (ccl_log_type log, void*var, void *deps)
{
  const funcdep *f = fdset_get_funcdep (deps, var);
  if (f != NULL)
    {
      const ccl_pair *p;
      const ccl_list *l = funcdep_get_inputs (f);

      if (ccl_list_is_empty (l))
	{
	  ar_ca_expr *func = funcdep_get_function (f);
	  ccl_log (log, " = ");
	  ar_ca_expr_log (log, func);
	}
      else
	{
	  ccl_log (log, " deps: ");
	  for (p = FIRST (l); p; p = CDR (p))
	    {
	      ar_ca_expr_log (log, CAR (p));
	      if (CDR (p) != NULL)
		ccl_log (log, ", ");
	    }
	}
    }
}

static ar_relsem *
s_build_relsem (ar_ca *ca,
		void (*order_variables) (ar_ca *ca,varorder *order, void *cd),
		void *clientdata)

{
  ar_relsem *result = (ar_relsem *)
    ar_sgs_create (ca, sizeof (ar_relsem), &RS_METHODS);

  result->eman = ar_ca_get_expression_manager (ca);
  result->ddm = DDM;
  result->assertion = NULL;
  result->nb_assertions = 0;
  result->assertions = NULL;
  result->post_assertions = NULL;
  result->initial = NULL;
  result->nb_transitions =  ar_ca_get_number_of_transitions (ca);
  result->has_epsilon = 0;
  result->transition_relations = NULL;
  result->nb_events = ar_ca_get_number_of_events (ca);
  result->event_relations = NULL;
  result->event_dds = NULL;
  result->event_pl = NULL;
  result->event_indexes = NULL;
  result->event_types = NULL;
  result->all_variables = NULL; 
  result->not_primed_variables = NULL; 
  result->to_prime = NULL; 
  result->to_non_prime = NULL; 
  result->non_prime = NULL; 
  result->state_variables = NULL;
  result->flow_variables = NULL;
  result->ordered_state_variables = NULL;

  result->order = varorder_add_reference (result->super.order);
  order_variables (ca, result->order, clientdata);
  s_make_prime_variables (result);
  result->max_dd_index = varorder_get_max_dd_index (result->order);
  
  if (DBG(log_order))
    {
      s_compute_event_indexes (result);
      varorder_log_sorted_by_dd_index_with_deps (CCL_LOG_DEBUG, result->order,
						 s_log_var_deps,
						 ar_ca_get_fdset (ca));
      ar_idtable_log (CCL_LOG_DEBUG, result->event_indexes, s_log_int, NULL);
    }

  result->prime_to_second = NULL;
  result->non_prime_to_prime = NULL;
  result->prime = NULL;
  result->second_to_prime = NULL;

  return result;
}

			/* --------------- */

int
ar_relsem_get_var_dd_index (ar_relsem *rs, ar_identifier *id)
{
  ar_ca_expr *var = ar_ca_expr_get_variable_by_name (rs->eman, id);
  int result = varorder_get_dd_index (rs->order, var);
  ar_ca_expr_del_reference (var);

  return result;
}

			/* --------------- */

int
ar_relsem_get_primed_var_dd_index (ar_relsem *rs, ar_identifier *id)
{
  ar_ca_expr *var = ar_ca_expr_get_variable_by_name (rs->eman, id);

  return ar_relsem_get_primed_var_dd_index_from_var (rs, var);
}

int
ar_relsem_get_primed_var_dd_index_from_var (ar_relsem *rs, ar_ca_expr *var)
{
  int index = varorder_get_index (rs->order, var);
  int result = varorder_get_dd_index (rs->order, rs->prime_variables[index]);
  ar_ca_expr_del_reference (var);

  return result;
}

			/* --------------- */
int
ar_relsem_get_primed_dd_index (ar_relsem *rs, int dd_index)
{
  ar_ca_expr *var = varorder_get_variable_from_dd_index (rs->order, dd_index);
  int index = varorder_get_index (rs->order, var);
  int result = varorder_get_dd_index (rs->order, rs->prime_variables[index]);

  return result;
}


extern int
ar_relsem_get_event_var_dd_index (ar_relsem *rs, ar_identifier *id)
{
  int result = -1;

  s_compute_event_indexes (rs);
  if (ar_idtable_has (rs->event_indexes, id))
    result = (intptr_t) ar_idtable_get (rs->event_indexes, id);

  return result;
}

			/* --------------- */

ar_dd
ar_relsem_get_initial (ar_relsem *rs)
{
  s_compute_initial (rs);

  return AR_DD_DUP (rs->initial);
}

			/* --------------- */

ar_dd
ar_relsem_get_assertion (ar_relsem *rs)
{
  s_compute_assertion (rs);

  return AR_DD_DUP (rs->assertion);
}

			/* --------------- */

ar_dd *
ar_relsem_get_assertions (ar_relsem *rs, int *p_nb_assertions)
{
  s_compute_assertions (rs);

  *p_nb_assertions = rs->nb_assertions;

  return rs->assertions;
}

			/* --------------- */

ar_dd
ar_relsem_get_reachables (ar_relsem *rs)
{
  s_compute_reachables (rs);

  return AR_DD_DUP (rs->reachables);
}

			/* --------------- */

static void
s_assign_and (ar_ddm ddm, ar_dd *pR, int nb_dds, ar_dd *dds)
{
  int i;
  ar_dd result;

  if (nb_dds == 0)
    return;

  result = ar_dd_compute_and (ddm, *pR, dds[0]);
  for (i = 1; i < nb_dds; i++)
    {
      ar_dd tmp = ar_dd_compute_and (ddm, result, dds[i]);
      AR_DD_FREE (result);
      result = tmp;
    }

  AR_DD_FREE (*pR);
  *pR = result;
}

			/* --------------- */

static void
s_apply_assertions (ar_relsem *rs, ar_dd *pR, int post)
{
  if (post)
    s_compute_post_assertions (rs);
  else
    s_compute_assertions (rs);
  if (rs->nb_assertions == 1 && ar_dd_is_one (rs->ddm, rs->assertions[0]))
    return;

  DBG_START_TIMER (("applying assertions"));
  {
    ar_dd *assertions = post ? rs->post_assertions : rs->assertions;
    s_assign_and (rs->ddm, pR, rs->nb_assertions, assertions);
  }
  DBG_END_TIMER ();
}

			/* --------------- */

static ar_dd_array
s_apply_prepost (ar_relsem *rs, const ar_dd *sets, int nb_sets, int precond, 
		 int postcond)
{
  int i;
  ar_dd_array result;
    
  if (postcond)
    s_compute_post_assertions (rs);

  ccl_array_init_with_size (result, nb_sets);
  for (i = 0; i < nb_sets; i++)
    {
      result.data[i] = AR_DD_DUP (sets[i]);

      if (precond)
	s_apply_assertions (rs, &result.data[i], 0);

      if (postcond)
	s_apply_assertions (rs, &result.data[i], 1);      
    }

  return result;
}

			/* --------------- */

ar_dd_array 
ar_relsem_get_valid_moves (ar_relsem *rs, int precond, int postcond)
{
  ar_sgs_set *set = s_predef_ANY_TRANS ((ar_sgs *) rs, NULL);
  relsem_set *rset = RS_SET (set);
  ar_dd_array result = s_apply_prepost (rs, DDS(rset), WIDTH(rset),
					precond, postcond);
  ar_sgs_set_del_reference (set);

  return result;
}

			/* --------------- */

ar_dd_array 
ar_relsem_get_no_self_epsilon_moves (ar_relsem *rs, int precond, int postcond)
{
  ar_sgs_set *any_t = s_predef_ANY_T (SGS (rs), NULL);
  ar_sgs_set *selfeps = s_predef_SELF_EPSILON (SGS (rs), NULL);
  ar_sgs_set *set = ar_sgs_compute_difference (SGS (rs), any_t, selfeps);
  relsem_set *rset = RS_SET (set);
  ar_dd_array result = s_apply_prepost (rs, DDS (rset), WIDTH (rset), 
					precond, postcond);
  ar_sgs_set_del_reference (any_t);
  ar_sgs_set_del_reference (selfeps);
  ar_sgs_set_del_reference (set);

  return result;
}

			/* --------------- */


ar_dd 
ar_relsem_get_valid_states (ar_relsem *rs)
{
  ar_sgs_set *set = s_predef_VALID_STATE_ASSIGNMENTS ((ar_sgs *) rs, NULL);
  ar_dd result = DD (set, 0);

  result = AR_DD_DUP (result);
  ar_sgs_set_del_reference (set);

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_get_ith_transition_relation (ar_relsem *rs, int precond, int postcond,
				      int tindex)
{
  ar_dd Trel;

  ccl_pre (0 <= tindex && tindex < rs->nb_transitions);

  if (rs->transition_relations != NULL)
    Trel = AR_DD_DUP (rs->transition_relations[tindex]);
  else
    Trel = s_compute_ith_transition_relation (rs, tindex, NULL);
  if (precond)
    s_apply_assertions (rs, &Trel, 0);
  if (postcond)
    s_apply_assertions (rs, &Trel, 1);

  return Trel;
}

			/* --------------- */

ar_dd 
ar_relsem_get_transition_relation (ar_relsem *rs, int precond, int postcond,
				   ar_ca_trans *t)
{
  ar_ca_expr *guard = ar_ca_trans_get_guard (t);
  int alpha_size = ar_ca_trans_get_number_of_assignments (t);
  ar_ca_expr **alpha = ar_ca_trans_get_assignments (t);
  ar_dd result =
    ar_relsem_unlabelled_transition_relation (rs, precond, postcond,
					      guard, alpha_size, alpha);
  ar_ca_expr_del_reference (guard);

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_unlabelled_transition_relation (ar_relsem *rs, 
					  int precond, int postcond,
					  ar_ca_expr *guard, 
					  int nb_assignments,
					  ar_ca_expr **assignments)
{
  ar_dd Trel;
  ccl_hash *cache = NULL;

  ar_ca_expr_build_dd_caches (&cache, NULL);
  Trel = 
    s_compute_transition_relation (rs, guard, nb_assignments, assignments, 
				   cache);
  ar_ca_expr_clean_up_dd_caches (cache, NULL);

  if (precond)
    s_apply_assertions (rs, &Trel, 0);
  if (postcond)
    s_apply_assertions (rs, &Trel, 1);

  return Trel;
}

ar_dd_array 
ar_relsem_get_transition_relations (ar_relsem *rs, int precond, int postcond)
{
  s_compute_transition_relations (rs); 
  return s_apply_prepost (rs, rs->transition_relations, rs->nb_transitions,
			  precond, postcond);
}

			/* --------------- */

ar_dd_array 
ar_relsem_get_event_relations (ar_relsem *rs, int precond, int postcond)
{
  s_compute_event_relations (rs); 

  return s_apply_prepost (rs, rs->event_relations, rs->nb_events, 
			  precond, postcond);
}

			/* --------------- */

struct data {
  ar_relsem *rs;
  ar_dd dd;
  int min;
  int max;
};

static void
s_get_min_max_index(ar_relsem *rs, ar_ca_expr *e, int *pmin, int *pmax)
{
  ccl_list *vars = ar_ca_expr_get_variables (e, NULL);
  ar_ca_expr *var = ccl_list_take_first (vars);

  *pmin = *pmax = varorder_get_dd_index (rs->order, var);

  while (! ccl_list_is_empty (vars))
    {
      ar_ca_expr *var = ccl_list_take_first (vars);
      int index = varorder_get_dd_index (rs->order, var);

      if (*pmin > index) 
	*pmin = index;
      if (*pmax < index) 
	*pmax = index;
    }
  ccl_list_delete (vars);
}

			/* --------------- */

static int
s_compare_dd_indexes (const void *p1, const void *p2)
{
  const ar_dd *d1 = (const ar_dd *) p1;
  const ar_dd *d2 = (const ar_dd *) p2;

  return AR_DD_INDEX (*d2) - AR_DD_INDEX (*d1);
}

			/* --------------- */
#if 1
static int
s_compare_dd_data (const void *p1, const void *p2)
{
  const struct data *d1 = p1;
  const struct data *d2 = p2;
  int result = 0;

  /* d1 included in d2 -> d1 treated first -> d1 < d2 -> -1 */
  if (d2->min <= d1->min && d1->max <= d2->max)
    result = -1;
  /* d2 included in d1 -> d2 treated first -> d2 < d1 -> 1 */
  else if (d1->min <= d2->min && d2->max <= d1->max)
     result = 1;
  /* d1.max <= d2.min -> d2 treated first -> d2 < d1 -> 1 */
  else if (d1->max <= d2->max)
     result = 1;
  /* d2.max <= d1.min -> d1 treated first -> d1 < d2 -> -1 */
  else if (d2->max <= d1->max)
     result = -1;
  /* d1.min <= d2.min <= d1.max -> d2 treated first -> d2 < d1 -> -1 */
  else if (d1->min <= d2->min && d2->min <= d1->max)
     result = -1;
  /* d2.min <= d1.min <= d2.max -> d1 treated first -> d1 < d2 -> 1 */
  else if (d1->min <= d2->min && d2->min <= d1->max)
     result = 1;
  else if (d1->min == d2->min && d2->max == d1->max)
    result = (ar_dd_get_number_of_nodes (d1->dd) - 
	      ar_dd_get_number_of_nodes (d2->dd));

  return result;
}
#endif
			/* --------------- */

static void
s_compute_assertions (ar_relsem *rs)
{
  const ccl_pair *p;
  ccl_list *dd_of_assertions;
  const ccl_list *assertions = ar_ca_get_assertions (rs->super.ca);    
  
  if (rs->assertions != NULL)
    return;

  dd_of_assertions = ccl_list_create ();
  assertions = ar_ca_get_assertions (rs->super.ca);

  for (p = FIRST (assertions); p; p = CDR (p))
    {
      ar_ca_expr *a = CAR (p);
      ar_dd r = ar_ca_expr_compute_dd (a, rs->ddm, rs->order, NULL);
      
      if (r != ar_dd_one (rs->ddm))
	{
	  struct data *pdata = ccl_new (struct data);
	  pdata->rs = rs;
	  pdata->dd = AR_DD_DUP (r);
	  if (r != ar_dd_zero (rs->ddm))	      
	    s_get_min_max_index (rs, a, &pdata->min, &pdata->max);
	  else
	    pdata->min = pdata->max = 0;
	  ccl_list_add (dd_of_assertions, pdata);
	}
      AR_DD_FREE (r);
    }

  if (ccl_list_get_size (dd_of_assertions))
    {
      ar_dd *a;
      
       ccl_list_sort (dd_of_assertions, s_compare_dd_data); 
      
      rs->nb_assertions = ccl_list_get_size (dd_of_assertions);
      a = rs->assertions = ccl_new_array (ar_dd, rs->nb_assertions);
      while (! ccl_list_is_empty (dd_of_assertions))
	{
	  struct data *pdata = ccl_list_take_first (dd_of_assertions);
	  *(a++) = pdata->dd;
	  ccl_delete (pdata);
	}
    }
  else
    {
      rs->nb_assertions = 1;
      rs->assertions = ccl_new_array (ar_dd, 1);
      rs->assertions[0] = ar_dd_dup_one (rs->ddm);
    }
  ccl_list_delete (dd_of_assertions);
}

			/* --------------- */

static void
s_compute_post_assertions (ar_relsem *rs)
{
  int i;

  if (rs->post_assertions != NULL)
    return;

  s_compute_assertions (rs);
  rs->post_assertions = ccl_new_array (ar_dd, rs->nb_assertions);

  for (i = 0; i < rs->nb_assertions; i++)
    rs->post_assertions[i] = MAKE_IT_PRIME (rs, rs->assertions[i]);
}

			/* --------------- */

static void
s_compute_assertion (ar_relsem *rs)
{
  if (rs->assertion != NULL)
    return;
  
  DBG_START_TIMER (("compute assertion"));
  rs->assertion = ar_dd_dup_one (rs->ddm);
  s_apply_assertions (rs, &rs->assertion, 0);
  if (DBG_IS_ON)
    ccl_debug ("nb nodes = %ld\n",
	       ar_dd_get_number_of_nodes (rs->assertion));
  DBG_END_TIMER ();
}

			/* --------------- */

static void
s_compute_initial (ar_relsem *rs)
{
  ar_dd I;
  int nb_init;
  int nb_init_constraints;

  if (rs->initial != NULL)
    return;

  nb_init = ar_ca_get_number_of_initial_assignments (rs->super.ca);
  nb_init_constraints = ar_ca_get_number_of_initial_constraints (rs->super.ca);

  DBG_F_START_TIMER (comp_initial, ("compute initial relation"));
  if (nb_init + nb_init_constraints == 0)
    {
      if (DBG (comp_initial))
	ccl_debug ("no initial condition is specified");
      s_compute_assertion (rs);
      I = AR_DD_DUP (rs->assertion);
    }
  else
    {
      int k = 0;
      const ccl_pair *p;
      const ccl_list *initial = ar_ca_get_initial_assignments (rs->super.ca);
      int nb_dds = nb_init + nb_init_constraints;
      ar_dd *dds = ccl_new_array (ar_dd, nb_dds);

      for (p = FIRST (initial); p; p = CDDR (p), k++)
	{      
	  ar_ca_expr *var = CAR (p);
	  ar_ca_expr *value = CADR (p);
	  ar_ca_expr *eq = ar_ca_expr_crt_eq (var,value);
	  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (var);
	  ar_ca_expr *in_dom = ar_ca_expr_crt_in_domain (value, dom);
	  ar_ca_expr *assign = ar_ca_expr_crt_and (eq, in_dom);
      
	  if (DBG (comp_initial))
	    {
	      ccl_debug ("compute init for variable '");
	      ar_ca_expr_log (CCL_LOG_DEBUG, var);
	      ccl_debug ("' index = %d with expr\n'",
			 varorder_get_dd_index (rs->order, var));
	      ar_ca_expr_log (CCL_LOG_DEBUG, assign);
	      ccl_debug ("'\n");
	    }

	  dds[k] = ar_ca_expr_compute_dd (assign, rs->ddm, rs->order, NULL);

	  if (DBG (comp_initial))
	    {	      
	      if (dds[k] == ar_dd_one (rs->ddm))
		ccl_debug (" TRUE");
	      else if (dds[k] == ar_dd_zero(rs->ddm))
		ccl_debug (" FALSE");
	      ccl_debug (" card=%g\n", 
			 ar_relsem_get_cardinality (rs, dds[k], 0));
	    }
	  ar_ca_expr_del_reference (eq);
	  ar_ca_expr_del_reference (in_dom);
	  ar_ca_expr_del_reference (assign);
	}

      DBG_F_START_TIMER (comp_initial, ("adding initial constraints"));
      initial = ar_ca_get_initial_constraints (rs->super.ca);
      for (p = FIRST (initial); p; p = CDR (p), k++)
	{
	  ar_ca_expr *ic = CAR (p);
	  if (DBG (comp_initial))
	    {
	      ccl_debug ("compute initial constraint '");
	      ar_ca_expr_log (CCL_LOG_DEBUG, ic);
	      ccl_debug ("'.\n");
	    }
	  dds[k] = ar_ca_expr_compute_dd (ic, rs->ddm, rs->order, NULL);
	}
      DBG_F_END_TIMER (comp_initial);

      ccl_assert (k == nb_dds);
      if (nb_dds > 1)
	qsort (dds, nb_dds, sizeof (ar_dd), s_compare_dd_indexes);

      DBG_F_START_TIMER (comp_initial, ("compute conjunction of initial "
				      "constraints"));      
      I = dds[0];
      for (k = 1; k < nb_dds; k++)
	{
	  ar_dd tmp;
	  tmp = ar_dd_compute_and (rs->ddm, I, dds[k]);
	  AR_DD_FREE (I);
	  AR_DD_FREE (dds[k]);
	  I = tmp;
	}
      DBG_F_END_TIMER (comp_initial);
      ccl_free (dds);

      DBG_F_START_TIMER (comp_initial, ("adding global constraints"));      
      s_apply_assertions (rs, &I, 0);
      DBG_F_END_TIMER (comp_initial);
    }

  if (DBG (comp_initial))
    {
      ccl_debug ("initial relation card = %g\n", 
		 ar_relsem_get_cardinality (rs, I, 0));
      DBG_END_TIMER ();
    }

  rs->initial = I;
}

			/* --------------- */

static void
s_compute_reachables (ar_relsem *rs)
{
  int stop;
  ar_dd S;
  int nbA;
  ar_dd *pA;
  if (rs->reachables != NULL)
    return;

  DBG_F_START_TIMER (comp_reach, ("compute reachable configurations"));
  stop = 0;
  s_compute_initial (rs);

  S = AR_DD_DUP (rs->initial);
#if POSTPRE_WITH_ONE_ASSERTION
  s_compute_assertion (rs);
  nbA = 1;
  pA = &rs->assertion;
#else
  s_compute_assertions (rs);
  nbA = rs->nb_assertions;
  pA = rs->assertions;
#endif
  while (!stop)
    {
      ar_dd tmp0;
      
      DBG_F_START_TIMER (comp_reach, ("compute post"));
      tmp0 = s_post (rs, S, nbA, pA);

      if (! rs->has_epsilon)
	{
	  ar_dd tmp1 = ar_dd_compute_or (rs->ddm, S, tmp0);
	  
	  AR_DD_FREE (tmp0);
	  tmp0 = tmp1;
	}
      
      AR_DD_FREE (S);
	
      stop = (tmp0 == S);
      S = tmp0;
      
      if (DBG (comp_reach))
	ccl_debug ("reachables @%p card=%g nb nodes=%ld\n", S,
		   ar_relsem_get_cardinality (rs, S, 0),
		   ar_dd_get_number_of_nodes (S));
      DBG_F_END_TIMER (comp_reach);
    }  
  rs->reachables = S;
  DBG_F_END_TIMER (comp_reach);
}

			/* --------------- */

static ar_ca_expr * 
s_crt_prime_variable (ar_relsem *rs, ar_ca_expr *var, int *indexes)
{
  int index = varorder_get_index (rs->order, var);
  ar_ca_expr *result = rs->prime_variables[index];

  if (result == NULL)
    {    
      int pindex;
      ar_ca_expr *rep = varorder_get_representative (rs->order, var);
      ar_identifier *name = ar_ca_expr_variable_get_name (var);
      ar_ca_domain *dom = ar_ca_expr_variable_get_not_const_domain (var);
      ar_identifier *pname = ar_identifier_rename_with_suffix (name, "'");
      ccl_list *attrs = ar_ca_expr_variable_get_attributes (var);
      
      result = ar_ca_expr_crt_variable (rs->eman, pname, dom, attrs);
      rs->prime_variables[index] = ar_ca_expr_add_reference (result);

      pindex = varorder_add_variable (rs->order, result);

      if (rep == var)
	{
	  int card = ar_ca_domain_get_cardinality (dom);
	  ar_ddcode encoding = card > 2 ? COMPACTED : EXHAUSTIVE;
	  
	  ccl_assert (indexes[index] == -1);
	  ccl_assert (indexes[pindex] == -1);

	  if (PRIME_BEFORE_NON_PRIME)
	    {
	      indexes[pindex] = 
		DD_INTERLEAVING_SCALE * varorder_get_dd_index (rs->order, var);
	      indexes[index] = indexes[pindex] + 2;
	    }
	  else
	    {
	      indexes[index] = 
		DD_INTERLEAVING_SCALE * varorder_get_dd_index (rs->order, var);
	      indexes[pindex] = indexes[index] + 2;
	    }

	  /* cardlist for all variables */
	  ar_dd_card_list_add (rs->ddm, &rs->all_variables, 
			       indexes[pindex], encoding, card);
	  ar_dd_card_list_add (rs->ddm, &rs->all_variables, 
			       indexes[index], encoding, card);

	  /* cardlist for not primed variables */
	  ar_dd_card_list_add (rs->ddm, &rs->not_primed_variables, 
			       indexes[index], encoding, card);

	  /* projection list for non prime to prime conversion */
	  ar_dd_projection_list_add (rs->ddm, &rs->to_prime, 
				     indexes[index], indexes[pindex]);

	  /* projection list for prime to non prime conversion */
	  ar_dd_projection_list_add (rs->ddm, &rs->to_non_prime,
				     indexes[pindex], indexes[index]);

	  /* list for projection on non prime variables */
	  ar_dd_projection_list_add (rs->ddm, &rs->non_prime,
				     indexes[index], indexes[index]);

	  if (ccl_hash_find (rs->is_state_variable, var))
	    {
	      ccl_hash_find (rs->is_state_variable, result);
	      ccl_hash_insert (rs->is_state_variable, result);

	      ar_dd_projection_list_add (rs->ddm, &rs->state_variables,
					 indexes[pindex],  
					 indexes[pindex]); 
	      ar_dd_projection_list_add (rs->ddm, &rs->state_variables,
					 indexes[index],  
					 indexes[index]); 
	    }
	  else
	    {
	      ar_dd_projection_list_add (rs->ddm, &rs->flow_variables,
					 indexes[pindex], 
					 indexes[pindex]); 
	      ar_dd_projection_list_add (rs->ddm, &rs->flow_variables,
					 indexes[index],  
					 indexes[index]); 
	    }
	}
      else
	{
	  ar_ca_expr *prep = s_crt_prime_variable (rs, rep, indexes);
	  varorder_merge (rs->order, prep, result);
	}

      ar_identifier_del_reference (pname);
      ar_identifier_del_reference (name);
      ar_ca_domain_del_reference (dom);
    }

  return result;
}

			/* --------------- */

static int
s_cmp_vars_indexes (const void *v1, const void *v2, void *data)
{
  int i1 = varorder_get_dd_index (data, v1);
  int i2 = varorder_get_dd_index (data, v2);
  return i2 - i1;
}

			/* --------------- */

static void
s_make_prime_variables (ar_relsem *rs)
{
  int n;
  const ccl_list *vars;
  const ccl_pair *p;
  int nb_vars = varorder_get_size (rs->order);
  int *indexes = ccl_new_array (int, 2 * nb_vars);

  rs->all_variables = ar_dd_create_card_list (rs->ddm);
  rs->not_primed_variables = ar_dd_create_card_list (rs->ddm);
  rs->to_prime = ar_dd_create_projection_list (rs->ddm);
  rs->to_non_prime = ar_dd_create_projection_list (rs->ddm);
  rs->non_prime = ar_dd_create_projection_list (rs->ddm);
  rs->state_variables = ar_dd_create_projection_list (rs->ddm);
  rs->flow_variables = ar_dd_create_projection_list (rs->ddm);

  rs->prime_variables = ccl_new_array (ar_ca_expr *, nb_vars);

  rs->is_state_variable = ccl_hash_create (NULL, NULL, NULL, NULL);
  rs->ordered_state_variables = ccl_list_create ();

  vars = ar_ca_get_state_variables (rs->super.ca);
  CCL_LIST_FOREACH(p, vars)
    {
      ccl_hash_find (rs->is_state_variable, CAR (p));
      ccl_hash_insert (rs->is_state_variable, CAR (p));
    }

  for (n = 0; n < 2 * nb_vars; n++)
    indexes[n] = -1;

  {
    ccl_list *allvars = ccl_list_create ();
    for (n = 0; n < 2; n++)
      {
	CCL_LIST_FOREACH(p, vars)
	  {
	    ccl_list_add (allvars, CAR (p));
	    if (n == 0)
	      ccl_list_add (rs->ordered_state_variables, CAR (p));
	  }
	vars = ar_ca_get_flow_variables (rs->super.ca);
      }
    ccl_list_sort_with_data (allvars, s_cmp_vars_indexes, rs->order);
    ccl_list_sort_with_data (rs->ordered_state_variables, s_cmp_vars_indexes, 
			     rs->order);
    while (! ccl_list_is_empty (allvars))
      {
	ar_ca_expr *v = ccl_list_take_first (allvars);
	s_crt_prime_variable (rs, v, indexes);
      }
    ccl_list_delete (allvars);
  }

  varorder_reset_dd_indexes (rs->order);
  for (n = 2 * nb_vars - 1; n >= 0; n--)
    {
      ar_ca_expr *var = varorder_get_variable_from_index (rs->order, n);
      if (varorder_get_representative (rs->order, var) == var)
	{
	  ccl_assert (indexes[n] >= 0);
	  varorder_set_dd_index (rs->order, var, indexes[n]);
	}
    }
  ccl_delete (indexes);
}

			/* --------------- */

static ar_identifier *
s_var_name_to_event_var_name (ar_identifier *id, ar_idtable *events)
{
  ar_identifier *label;
  ar_identifier *result;  
  ar_identifier *tmp0;
  int stop = 0;

  id = ar_identifier_add_reference (id);
  while (!stop)
    {      
      label = ar_identifier_get_name_and_prefix(id, &tmp0);
      ar_identifier_del_reference (label);
      stop = ar_idtable_has (events, tmp0);
      if (!stop)
	{
	  if (ar_identifier_get_path_length (tmp0) == 0)
	    {
	      result = ar_identifier_create ("");
	      stop = 1;
	    }
	  else
	    {
	      ar_identifier_del_reference (id);
	      id = ar_identifier_add_reference (tmp0);
	    }
	  ar_identifier_del_reference (tmp0);
	}
      else
	{
	  result = tmp0;
	}
    }
  ar_identifier_del_reference (id);

  return result;
}

			/* --------------- */

static ar_identifier *
s_event_name_to_event_var_name (ar_identifier *id)
{
  ar_identifier *tmp0;
  ar_identifier *tmp1 = ar_identifier_create ("");
  ar_identifier *label = ar_identifier_get_name_and_prefix(id, &tmp0);
  ar_identifier *result = ar_identifier_add_prefix (tmp1, tmp0);
  ar_identifier_del_reference (tmp0);
  ar_identifier_del_reference (tmp1);
  ar_identifier_del_reference (label);

  return result;
}

			/* --------------- */

static ar_dd
s_dd_for_event_id (ar_relsem *rs, ar_identifier *ev)
{
  int i;
  ar_dd result;
  ar_identifier *tmp[2];
  ar_identifier *slname;
  ar_ddm ddm = rs->ddm;
  ar_dd one = ar_dd_one (ddm);
  ar_dd zero = ar_dd_zero (ddm);
  ar_identifier *label;
  ar_type *et;
  int index;
  int valindex;
  int arity;
  ar_ddcode encoding;
  ar_ddtable sons;

  ccl_pre (rs->event_types != NULL);
  
  label = ar_identifier_get_name_and_prefix(ev, &tmp[0]);
  tmp[1] = ar_identifier_create ("");
  slname = ar_identifier_add_prefix (tmp[1], tmp[0]);
  ar_identifier_del_reference (tmp[0]);
  ar_identifier_del_reference (tmp[1]);

  et = ar_idtable_get (rs->event_types, slname);
  index = (intptr_t) ar_idtable_get (rs->event_indexes, slname);
  ccl_assert (index >= 0);
  
  valindex = ar_type_enum_get_value_index (et, label);
  arity = ar_type_get_cardinality (et);
  encoding = ((arity > 2) ? COMPACTED : EXHAUSTIVE);
  sons =  ar_dd_allocate_dd_table (ddm, arity, encoding);

  ccl_assert (valindex >= 0);

  if (encoding == COMPACTED)
    {
      for (i = 0; i < arity; i++)
	{
	  ar_dd tmp = i == valindex ? one : zero;

	  AR_DD_MIN_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_MAX_OFFSET_NTHDD (sons, i) = i;
	  AR_DD_CNTHDD (sons,i) = AR_DD_DUP (tmp);
	}
    }  
  else
    {
      for (i = 0; i < arity; i++)
	{
	  ar_dd tmp = i == valindex ? one : zero;
	  AR_DD_ENTHDD (sons,i) = AR_DD_DUP (tmp);
	}
    }

  result = ar_dd_find_or_add (ddm, index, arity, encoding, sons);

  ar_dd_free_dd_table (ddm, arity, encoding, sons);
  ar_identifier_del_reference (slname);
  ar_identifier_del_reference (label);
  ar_type_del_reference (et);

  return result;
}

static ar_dd
s_dd_for_event (ar_relsem *rs, ar_ca_event *ev)
{
  ar_dd result = ar_dd_dup_one (rs->ddm);
  ar_identifier_iterator *ii = ar_ca_event_get_ids (ev);

  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      ar_dd tmp1 = s_dd_for_event_id (rs, id);
      ar_dd tmp2 = ar_dd_compute_and (rs->ddm, result, tmp1);
      AR_DD_FREE (tmp1);
      AR_DD_FREE (result);
      result = tmp2;
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);
  
  return result;
}

static void
s_compute_event_dds (ar_relsem *rs)
{
  int i;
  ar_ca_event **events;
  
  if (rs->event_dds != NULL)
    return;
  
  s_compute_event_types (rs);
  events = ar_ca_get_events (rs->super.ca);
  rs->event_dds = ccl_new_array (ar_dd, rs->nb_events);
  for (i = 0; i < rs->nb_events; i++)
    rs->event_dds[i] = s_dd_for_event (rs, events[i]);
}

static void
s_compute_event_types (ar_relsem *rs)
{
  ar_node *flatnode;
  ar_node *actual_node;
  ar_identifier *nodename;
  ar_type *type;
  ccl_list *stypes;

  if (rs->event_types != NULL)
    return;
  
  flatnode = ar_sgs_get_node ((ar_sgs *) rs);
  nodename = ar_node_get_name (flatnode);
  actual_node = ar_model_get_node (nodename);

  type = ar_node_get_event_type (actual_node);
  stypes = ar_type_expand_with_prefix (AR_EMPTY_ID, type, NULL);
  ar_type_del_reference (type);
  ar_identifier_del_reference (nodename);
  ar_node_del_reference (actual_node);
  ar_node_del_reference (flatnode);

  rs->event_types =
    ar_idtable_create (0, (ccl_duplicate_func *) ar_type_add_reference,
		       (ccl_delete_proc *) ar_type_del_reference);
  while (! ccl_list_is_empty (stypes))
    {
      ar_identifier *name = ccl_list_take_first (stypes);
      ar_type *type = ccl_list_take_first (stypes);
      ar_idtable_put (rs->event_types, name, type);
      ar_identifier_del_reference (name);
      ar_type_del_reference (type);
    }
  ccl_list_delete (stypes);
}

static void
s_compute_event_indexes (ar_relsem *rs)
{
  int i;
  int nb_events;
  ar_ca_event **events;
  ar_identifier_iterator *ei;
  
  if (rs->event_indexes != NULL)
    return;
  
  nb_events = ar_ca_get_number_of_events (rs->super.ca);
  events = ar_ca_get_events (rs->super.ca);
  rs->event_indexes = ar_idtable_create (0, NULL, NULL);
  
  for (i = 0; i < nb_events; i++)
    {
      ar_identifier_iterator *ii = ar_ca_event_get_ids (events[i]);

      while (ccl_iterator_has_more_elements (ii))
	{
	  ar_identifier *event_name = ccl_iterator_next_element (ii);
	  ar_identifier *event_var_name = 
	    s_event_name_to_event_var_name (event_name);

	  if (! ar_idtable_has (rs->event_indexes, event_var_name))
	    ar_idtable_put (rs->event_indexes, event_var_name,
			    (void *)(intptr_t) -1);
	  ar_identifier_del_reference (event_var_name);
	  ar_identifier_del_reference (event_name);
	}
      ccl_iterator_delete (ii);
    }
  s_make_ordering_for_events (rs);
  rs->event_pl = ar_dd_create_projection_list (rs->ddm);
  ei = ar_idtable_get_keys (rs->event_indexes);
  while (ccl_iterator_has_more_elements (ei))
    {
      ar_identifier *id = ccl_iterator_next_element (ei);
      int index = (intptr_t) ar_idtable_get (rs->event_indexes, id);
      ar_dd_projection_list_add (rs->ddm, &rs->event_pl, index, index);
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ei);
}

			/* --------------- */

static void
s_make_ordering_for_events (ar_relsem *rs)
{
  int k;
  ar_identifier_iterator *sli;
  int last_event_index;
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_state_variables (rs->super.ca);
  ccl_hash *indexes = ccl_hash_create (NULL, NULL, NULL, NULL);
 
  for (k = 0; k < 2; k++)
    {
      CCL_LIST_FOREACH (p, vars)
	{
	  ar_identifier *varid = ar_ca_expr_variable_get_name (CAR (p));
	  ar_identifier *eid = 
	    s_var_name_to_event_var_name (varid, rs->event_indexes);
	  int eindex = (intptr_t) ar_idtable_get (rs->event_indexes, eid);

	  ccl_assert (ar_idtable_has (rs->event_indexes, eid));
	  
	  if (eindex == -1)	
	    {
#if PRIME_BEFORE_NON_PRIME
	      eindex = varorder_get_dd_index (rs->order, CAR (p)) - 1;
#else
	      eindex = varorder_get_dd_index (rs->order, CAR (p)) + 1;
#endif 
	      ccl_assert (! varorder_has_dd_index (rs->order, eindex));
	      ccl_assert (! ccl_hash_find (indexes, 
					   (void *) (intptr_t) eindex));
	      ccl_hash_find (indexes, (void *) (intptr_t) eindex);
	      ccl_hash_insert (indexes, (void *) (intptr_t) eindex);

	      ar_idtable_put (rs->event_indexes, eid, 
			      (void *) (intptr_t) eindex);
	    }
	  ar_identifier_del_reference (eid);
	  ar_identifier_del_reference (varid);
	}
      vars = ar_ca_get_flow_variables (rs->super.ca);
    }
  last_event_index = 0;

  sli = ar_idtable_get_keys (rs->event_indexes);

  while (ccl_iterator_has_more_elements (sli))
    {
      ar_identifier *event_var_name = ccl_iterator_next_element (sli);
      int index = 
	(intptr_t) ar_idtable_get (rs->event_indexes, event_var_name);

      if (index == -1)
	{
	  while (varorder_has_dd_index (rs->order, last_event_index) ||
		 ccl_hash_find (indexes, (void *) (intptr_t) last_event_index))
	    last_event_index++;

	  ccl_hash_find (indexes, (void *) (intptr_t) last_event_index);
	  ccl_hash_insert (indexes, (void *) (intptr_t) last_event_index);

	  ar_idtable_put (rs->event_indexes, event_var_name,
			  (void *) (intptr_t) last_event_index);
	  last_event_index++;	  
	}
      ar_identifier_del_reference (event_var_name);
    }
  ccl_iterator_delete (sli);
  ccl_hash_delete (indexes);
}

			/* --------------- */

double
ar_relsem_get_cardinality (ar_relsem *rs, ar_dd dd, int all)
{
  ar_dd cardlist = all ? rs->all_variables : rs->not_primed_variables;

  return ar_dd_cardinality (rs->ddm, dd, cardlist);
}

			/* --------------- */

ar_ca_expr *
ar_relsem_dd_to_expr (ar_relsem *rs, ar_dd dd)
{
  return ar_ca_expr_compute_expr_from_dd (rs->ddm, rs->eman, rs->order, dd,
					  NULL);
}

			/* --------------- */

void
ar_relsem_add_eq_representative (ar_relsem *rs, ar_ca_expr **e, ar_ca_expr *var)
{
  ar_ca_expr *rep = varorder_get_representative (rs->order, var);

  if (rep != var)
    {
      ar_ca_expr *tmp = ar_ca_expr_crt_eq (rep, var);

      rep = ar_ca_expr_crt_and (*e, tmp);
      ar_ca_expr_del_reference (tmp);
      ar_ca_expr_del_reference (*e);
      *e = rep;
    }
}

			/* --------------- */

static ar_ca_expr *
s_get_prime_variable (ar_relsem *rs, ar_ca_expr *v)
{
  int index = varorder_get_index (rs->order, v);

  return ar_ca_expr_add_reference (rs->prime_variables[index]);
}

			/* --------------- */

static int
s_is_epsilon_loop (const ar_ca_trans *t)
{  
  int result = (ar_ca_trans_get_number_of_assignments (t) == 0);
  
  if (result)
    {
      ar_ca_expr *g =  ar_ca_trans_get_guard (t);
      result = (ar_ca_expr_is_boolean_expr (g) && 
		ar_ca_expr_get_kind (g) == AR_CA_CST && 
		ar_ca_expr_get_min (g));
      ar_ca_expr_del_reference (g);
    }

  if (result)
    {
      ar_ca_event *e = ar_ca_trans_get_event (t);

      result = ar_ca_event_is_epsilon (e);
      ar_ca_event_del_reference (e);
    }
  
  return result;
}

			/* --------------- */

#if POSTPRE_USE_GLOBAL_TRANSITION_RELATION == 1
struct trans_dd
{
  ar_dd rel;
  int min_index;
  int max_index;
};

static int
s_cmp_trans_dd (const void *el1, const void *el2)
{
  const struct trans_dd *e1 = el1;
  int d1 = e1->max_index - e1->min_index;
  const struct trans_dd *e2 = el2;
  int d2 = e2->max_index - e2->min_index;

  if (d1 == d2)
    {
      if (e2->max_index == e1->max_index)
	return e2->min_index - e1->min_index;
      return e2->max_index - e1->max_index;      
    }
  
  return d1 - d2;
}

static struct trans_dd *
s_transition_min_dd_index (ar_relsem *rs)
{
  int i;
  struct trans_dd *result = ccl_new_array (struct trans_dd, rs->nb_transitions);
  ar_ca_trans **trans = ar_ca_get_transitions (rs->super.ca);
  
  for (i = 0; i < rs->nb_transitions; i++)
    {
      ar_ca_trans *t = trans[i];
      ccl_list *vars = ar_ca_trans_get_variables (t, NULL, AR_CA_RW_VARS);
      int min_index = INT_MAX;
      int max_index = -1;
      
      while (! ccl_list_is_empty (vars))
	{
	  ar_ca_expr *v = ccl_list_take_first (vars);
	  int vindex = varorder_get_dd_index (rs->order, v);
	  if (vindex > max_index)
	    max_index = vindex;
	  if (vindex < min_index)
	    min_index = vindex;
	}
      result[i].rel = rs->transition_relations[i];
      result[i].min_index = min_index;
      result[i].max_index = max_index;
      ccl_list_delete (vars);
    }
  qsort (result, rs->nb_transitions, sizeof (*result), s_cmp_trans_dd);
    
  return result;
}

static void
s_compute_global_transition_relation (ar_relsem *rs)
{
  int i;
  struct trans_dd *trans;
  
  if (rs->global_transition_relation != NULL)
    return;

  s_compute_transition_relations (rs); 
  
  DBG_F_START_TIMER (comp_transrel, ("compute global transition relation"));

  rs->global_transition_relation = ar_dd_dup_zero (rs->ddm);
  trans = s_transition_min_dd_index (rs);
    
  for(i = 0; i < rs->nb_transitions; i++)
    {
      //ar_dd rel = rs->transition_relations[i];
      ar_dd rel = trans[i].rel;
      
      ar_dd tmp = ar_dd_compute_or (rs->ddm, 
				    rs->global_transition_relation,
				    rel);
      
      AR_DD_FREE (rs->global_transition_relation);
      rs->global_transition_relation = tmp;
      if (DBG (comp_transrel))
	ccl_debug ("%d %d |t|=%d |GT|=%d\n",
		   trans[i].min_index,
		   trans[i].max_index, 
		   ar_dd_get_number_of_nodes (rs->transition_relations[i]),
		   ar_dd_get_number_of_nodes (rs->global_transition_relation));
    }
  ccl_delete (trans);
  DBG_F_END_TIMER (comp_transrel);
}
#endif
			/* --------------- */

static void
s_compute_transition_relations (ar_relsem *rs)
{
  int i;
  ar_ca_trans **trans = ar_ca_get_transitions (rs->super.ca);
  ccl_hash *e2dd;

  if (rs->transition_relations != NULL)
    return;

  ar_ca_expr_build_dd_caches (&e2dd, NULL);
  rs->transition_relations = ccl_new_array (ar_dd, rs->nb_transitions);

  for(i = 0; i < rs->nb_transitions; i++)
    {
      if (DBG (comp_transrel))
	{
	  ar_ca_event *e = ar_ca_trans_get_event (trans[i]);
	  CCL_DEBUG_START_TIMED_BLOCK (("compute transition relation %d/%d",
					i, rs->nb_transitions));
	  ar_ca_event_log (CCL_LOG_DEBUG, e);
	  ar_ca_event_del_reference (e);
	  ccl_debug ("\n");
	}
      
      rs->has_epsilon = rs->has_epsilon || s_is_epsilon_loop (trans[i]);
      rs->transition_relations[i] = 
	s_compute_relation_for_transition (rs, trans[i], e2dd);

      if (DBG (comp_transrel))
	{
	  double card = 
	    ar_relsem_get_cardinality (rs, rs->transition_relations[i],1);
	  ccl_debug ("size=%ld ", 
		     ar_dd_get_number_of_nodes (rs->transition_relations[i]));
	  
	  ccl_debug ("card=%g", card);
	  if (rs->transition_relations[i] == ar_dd_one(rs->ddm))
	    ccl_debug(" TRUE");
	  else if (rs->transition_relations[i] == ar_dd_zero(rs->ddm))
	    ccl_debug(" FALSE");
	  ccl_debug ("\n");
	  CCL_DEBUG_END_TIMED_BLOCK ();
	}
    }
  ar_ca_expr_clean_up_dd_caches (e2dd, NULL);
}


			/* --------------- */

static ar_dd
s_compute_ith_transition_relation (ar_relsem *rs, int i,
				   ccl_hash *cache)
{  
  ar_ca_trans **trans = ar_ca_get_transitions (rs->super.ca);
  int delete_cache = (cache == NULL);
  ar_dd result;

  if (delete_cache)
    ar_ca_expr_build_dd_caches (&cache, NULL);
  result = s_compute_relation_for_transition (rs, trans[i], cache);
  if (delete_cache)
    ar_ca_expr_clean_up_dd_caches (cache, NULL);

  return result;
}

			/* --------------- */

static ar_dd
s_compute_assignment_relation (ar_relsem *rs, int with_identity, 
			       int nb_assignments, ar_ca_expr **assignments,
			       ccl_hash *cache)
{  
  int i;
  ccl_pair *p;
  ar_dd tmp;
  ar_dd result = ar_dd_dup_one (rs->ddm);

  for(i = 0, p = FIRST (rs->ordered_state_variables); p; p = CDR (p), i++)
    { 
      int j;
      ar_dd dd_a;
      ar_ca_expr *var = CAR (p);
      ar_ca_expr *pvar;
      ar_ca_expr *a;

      for(j = 0; j < nb_assignments; j++)
	{
	  if( assignments[2 * j] == var )
	    break;
	}
      if (j == nb_assignments && ! with_identity)
	continue;
      
      pvar = s_get_prime_variable (rs, var);
      if (j == nb_assignments)
	a = ar_ca_expr_crt_eq (var, pvar);
      else 
	a = ar_ca_expr_crt_eq_for_assign (pvar, assignments[2 * j + 1]);
      ar_ca_expr_del_reference (pvar);  
      dd_a = ar_ca_expr_compute_dd (a, rs->ddm, rs->order, cache);
      ar_ca_expr_del_reference (a);
      tmp = ar_dd_compute_and (rs->ddm, dd_a, result);
      AR_DD_FREE (dd_a);
      AR_DD_FREE (result);
      result = tmp;
    }
  
  return result;
}


static ar_dd
s_compute_transition_relation (ar_relsem *rs, 
			       ar_ca_expr *g, 
			       int nb_assignments, ar_ca_expr **assignments,
			       ccl_hash *cache)
{  
  ar_dd tmp1 = 
    ar_ca_expr_compute_dd (g, rs->ddm, rs->order, cache);
  ar_dd tmp2 =
    s_compute_assignment_relation (rs, 1, nb_assignments, assignments, cache);
  ar_dd result = ar_dd_compute_and (rs->ddm, tmp1, tmp2);
  AR_DD_FREE (tmp1);
  AR_DD_FREE (tmp2);
  
  return result;
}

static ar_sgs_set *
s_rs_assignment (ar_sgs *sgs, ar_ca_expr **assign,  int nb_assign)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ccl_hash *cache;
  ar_dd Rdd;
  ar_sgs_set *R;

  ar_ca_expr_build_dd_caches (&cache, NULL);
  Rdd = s_compute_assignment_relation (rs, 0, nb_assign, assign, cache);
  R = s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);
  ar_ca_expr_clean_up_dd_caches (cache, NULL);
	
  for (i = 0; i < rs->nb_events; i++)
    DD (R, i) = AR_DD_DUP (Rdd);
  AR_DD_FREE (Rdd);
  
  return R;
}


static ar_dd
s_compute_relation_for_transition (ar_relsem *rs, ar_ca_trans *t, 
				   ccl_hash *cache)
{  
  ar_ca_expr **assignments = ar_ca_trans_get_assignments(t);
  int nb_assignments = ar_ca_trans_get_number_of_assignments(t);
  ar_ca_expr *g = ar_ca_trans_get_guard(t);
  ar_dd result = 
    s_compute_transition_relation (rs, g, nb_assignments, assignments, cache);
  ar_ca_expr_del_reference(g);

  return result;
}

ar_dd 
ar_relsem_lazy_post_by_ith_transition (ar_relsem *rs, int i, ar_dd S)
{
  ar_ca_trans **trans = ar_ca_get_transitions (rs->super.ca);
  ar_dd result = ar_relsem_lazy_post_by_transition (rs, trans[i], S);

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_lazy_pre_by_ith_transition (ar_relsem *rs, int i, ar_dd S)
{
  ar_ca_trans **trans = ar_ca_get_transitions (rs->super.ca);
  ar_dd result = ar_relsem_lazy_pre_by_transition (rs, trans[i], S);

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_lazy_post (ar_relsem *rs, ar_dd S)
{
  int i;
  ar_dd R = ar_dd_dup_zero (rs->ddm);

  for (i = 0; i < rs->nb_transitions; i++)
    {
      ar_dd tmp0 = ar_relsem_lazy_post_by_ith_transition (rs, i, S);
      ar_dd tmp1 = ar_dd_compute_or (rs->ddm, R, tmp0);
      AR_DD_FREE (tmp0);
      AR_DD_FREE (R);
      R = tmp1;
    }

  return R;
}

			/* --------------- */

ar_dd 
ar_relsem_lazy_pre (ar_relsem *rs, ar_dd S)
{
  int i;
  ar_dd R = ar_dd_dup_zero (rs->ddm);

  for (i = 0; i < rs->nb_transitions; i++)
    {
      ar_dd tmp0 = ar_relsem_lazy_pre_by_ith_transition (rs, i, S);
      ar_dd tmp1 = ar_dd_compute_or (rs->ddm, R, tmp0);
      AR_DD_FREE (tmp0);
      AR_DD_FREE (R);
      R = tmp1;
    }

  return R;
}

			/* --------------- */

static int
s_transition_assign_constants (ar_ca_trans *t)
{
  int i;
  ar_ca_expr **assignments = ar_ca_trans_get_assignments(t);
  int nb_assignments = ar_ca_trans_get_number_of_assignments(t);

  for (i = 0; i < nb_assignments; i++)
    {
      if (ar_ca_expr_get_kind (assignments[2 * i + 1]) != AR_CA_CST)
	return 0;
    }

  return 1;
}

			/* --------------- */

static ar_dd
s_lazy_post (ar_relsem *rs, ar_ca_expr *g, int nb_assignments,
	     ar_ca_expr **assignments, ar_dd S, ccl_hash *cache)
{
  ar_dd R;
  int i;
  ar_dd tmp[3];

  tmp[0] = ar_ca_expr_compute_dd (g, rs->ddm, rs->order, cache);
  if (! ar_dd_intersect (rs->ddm, S, tmp[0]))
    {
      AR_DD_FREE (tmp[0]);
      return ar_dd_dup_zero (rs->ddm);
    }
  R = ar_dd_compute_and (rs->ddm, tmp[0], S);
  AR_DD_FREE (tmp[0]);    
#if 0
  tmp[2] = ar_dd_dup_one (rs->ddm);

  for (i = 0; i < nb_assignments; i++)
    {
      ar_ca_expr *v = assignments[2 * i];
      ar_ca_expr *cst = assignments[2 * i + 1];
      ar_ca_expr *v_eq_cst = ar_ca_expr_crt_eq (v, cst);
      int dd_index = varorder_get_dd_index (rs->order, v);
      ccl_assert (dd_index >= 0);
      tmp[0] = ar_dd_project_variable (rs->ddm, R, dd_index);
      AR_DD_FREE (R);
      R = tmp[0];
	  
      tmp[0] = ar_ca_expr_compute_dd (v_eq_cst, rs->ddm, rs->order, cache);
      tmp[1] = ar_dd_compute_and (rs->ddm, tmp[0], tmp[2]);
      ar_ca_expr_del_reference (v_eq_cst);
      AR_DD_FREE (tmp[2]);
      AR_DD_FREE (tmp[0]);
      tmp[2] = tmp[1];
    }
  tmp[0] = ar_dd_compute_and (rs->ddm, R, tmp[2]);
  AR_DD_FREE (tmp[2]);
  AR_DD_FREE (R);
  R = tmp[0];
#else
  for (i = 0; i < nb_assignments; i++)
    {
      ar_ca_expr *v = assignments[2 * i];
      ar_ca_expr *cst = assignments[2 * i + 1];
      ar_ca_expr *v_eq_cst = ar_ca_expr_crt_eq (v, cst);
      int dd_index = varorder_get_dd_index (rs->order, v);
      ccl_assert (dd_index >= 0);

      tmp[0] = ar_ca_expr_compute_dd (v_eq_cst, rs->ddm, rs->order, cache);
      tmp[1] = ar_dd_project_variable2 (rs->ddm, R, tmp[0]);
      AR_DD_FREE (R);
      AR_DD_FREE (tmp[0]);
      R = tmp[1];
      ar_ca_expr_del_reference (v_eq_cst);
    }
#endif
  s_assign_and (rs->ddm, &R, rs->nb_assertions, rs->assertions);

  return R;
}

			/* --------------- */

static ar_dd
s_lazy_pre (ar_relsem *rs, ar_ca_expr *g, int nb_assignments,
	    ar_ca_expr **assignments, ar_dd S, ccl_hash *cache)
{
  ar_dd R;
  int i;
  ar_dd tmp[3];

  tmp[0] = ar_dd_dup_one (rs->ddm);
  for (i = 0; i < nb_assignments; i++)
    {
      ar_ca_expr *v = assignments[2 * i];
      ar_ca_expr *cst = assignments[2 * i + 1];
      ar_ca_expr *v_eq_cst = ar_ca_expr_crt_eq (v, cst);
      int dd_index = varorder_get_dd_index (rs->order, v);
      ccl_assert (dd_index >= 0);

      tmp[1] = ar_ca_expr_compute_dd (v_eq_cst, rs->ddm, rs->order, cache);
      tmp[2] = ar_dd_compute_and (rs->ddm, tmp[0], tmp[1]);
      AR_DD_FREE (tmp[0]);
      AR_DD_FREE (tmp[1]);
      ar_ca_expr_del_reference (v_eq_cst);
      tmp[0] = tmp[2];
    }
  tmp[1] = ar_dd_project_on_variables (rs->ddm, S, rs->state_variables);
  tmp[2] = ar_dd_project_variable3 (rs->ddm, tmp[1], tmp[0]);
  AR_DD_FREE (tmp[1]);
  AR_DD_FREE (tmp[0]);

  tmp[0] = ar_ca_expr_compute_dd (g, rs->ddm, rs->order, cache);
  R = ar_dd_compute_and (rs->ddm, tmp[0], tmp[2]);
  AR_DD_FREE (tmp[0]);
  AR_DD_FREE (tmp[2]);

  s_assign_and (rs->ddm, &R, rs->nb_assertions, rs->assertions);

  return R;
}

			/* --------------- */

static void
s_collect_or (ar_ca_expr *e, ccl_list *result)
{
  if (ar_ca_expr_get_kind (e) == AR_CA_OR)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      s_collect_or (args[0], result);
      s_collect_or (args[1], result);
    }
  else if (ar_ca_expr_get_kind (e) == AR_CA_ITE)
    {
      ar_ca_expr **args = ar_ca_expr_get_args (e);
      ar_ca_expr *tmp = ar_ca_expr_crt_not (args[0]);

      ccl_list_add (result, ar_ca_expr_crt_and (args[0], args[1]));
      ccl_list_add (result, ar_ca_expr_crt_and (tmp, args[2]));
      ar_ca_expr_del_reference (tmp);
    }
  else if (! ccl_list_has (result, e))    
    {
      ccl_list_add (result, ar_ca_expr_add_reference (e));
    }
}

			/* --------------- */


ar_dd 
ar_relsem_lazy_post_by_transition (ar_relsem *rs, ar_ca_trans *t, ar_dd S)
{
  ar_dd result;

  if (s_transition_assign_constants (t))
    {
      ar_dd tmp[3];
      ccl_hash *cache = NULL;
      ar_ca_expr *g = ar_ca_trans_get_guard (t);
      ccl_list *args = ccl_list_create ();
      ar_ca_expr **assignments = ar_ca_trans_get_assignments (t);
      int nb_assignments = ar_ca_trans_get_number_of_assignments (t);

      s_compute_assertions (rs);      
      ar_ca_expr_build_dd_caches (&cache, NULL);
      s_collect_or (g, args);
      result = ar_dd_dup_zero (rs->ddm);
      while (!ccl_list_is_empty (args))
	{
	  ar_ca_expr *arg = ccl_list_take_first (args);
	  tmp[0] = s_lazy_post (rs, arg, nb_assignments, assignments, S, cache);
	  tmp[1] = ar_dd_compute_or (rs->ddm, result, tmp[0]);
	  AR_DD_FREE (tmp[0]);
	  AR_DD_FREE (result);
	  result = tmp[1];
	  ar_ca_expr_del_reference (arg);
	}
      ar_ca_expr_del_reference (g);
      ccl_list_delete (args);

      tmp[0] = ar_dd_project_on_variables (rs->ddm, result, 
      					   rs->state_variables);
      AR_DD_FREE (result);
      result = tmp[0];
      ar_ca_expr_clean_up_dd_caches (cache, NULL);
    }
  else
    {
      ar_dd tmp = ar_relsem_get_transition_relation (rs, 0, 0, t);
      result = ar_relsem_compute_post_by_T (rs, S, tmp, rs->nb_assertions,
					    rs->assertions);
      AR_DD_FREE (tmp);
    }

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_lazy_pre_by_transition (ar_relsem *rs, ar_ca_trans *t, ar_dd S)
{
  ar_dd result;

  if (s_transition_assign_constants (t))
    {
      ar_dd tmp[3];
      ccl_hash *cache = NULL;
      ar_ca_expr *g = ar_ca_trans_get_guard (t);
      ccl_list *args = ccl_list_create ();
      ar_ca_expr **assignments = ar_ca_trans_get_assignments (t);
      int nb_assignments = ar_ca_trans_get_number_of_assignments (t);

      s_compute_assertions (rs);      
      ar_ca_expr_build_dd_caches (&cache, NULL);
      s_collect_or (g, args);
      result = ar_dd_dup_zero (rs->ddm);
      while (!ccl_list_is_empty (args))
	{
	  ar_ca_expr *arg = ccl_list_take_first (args);
	  tmp[0] = s_lazy_pre (rs, arg, nb_assignments, assignments, S, cache);
	  tmp[1] = ar_dd_compute_or (rs->ddm, result, tmp[0]);
	  AR_DD_FREE (tmp[0]);
	  AR_DD_FREE (result);
	  result = tmp[1];
	  ar_ca_expr_del_reference (arg);
	}
      ar_ca_expr_del_reference (g);
      ccl_list_delete (args);
      ar_ca_expr_clean_up_dd_caches (cache, NULL);
    }
  else
    {
      ar_dd tmp = ar_relsem_get_transition_relation (rs, 1, 1, t);
      result = ar_relsem_compute_pre_by_T (rs, S, tmp, rs->nb_assertions,
					   rs->assertions);
      AR_DD_FREE (tmp);
    }

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_lazy_reachables (ar_relsem *rs)
{
  int fp; 
  ar_dd R;

  if (rs->reachables != NULL)
    return AR_DD_DUP (rs->reachables);

  R = ar_relsem_get_initial (rs);
  fp = 0;

  while (! fp)
    {
      if (DBG_IS_ON)
	ccl_debug ("new turn with %d nodes\n", 
		   ar_dd_get_number_of_nodes (R));
      {
	ar_dd X = ar_relsem_lazy_post (rs, R);
	/* we assume there exists an epsilon loop */
	fp = (R == X);
	AR_DD_FREE (R);
	R = X;
      }
    }

  return R;
}

			/* --------------- */

static ar_dd
s_apply (ar_relsem *rs, ar_dd S, ar_dd T, int nb_I, ar_dd *I, int inv_apply)
{
  ar_dd tmp = ar_dd_compute_and (rs->ddm, T, S);
  ar_dd plist = inv_apply ? rs->non_prime : rs->to_non_prime;
  ar_dd result = ar_dd_project_on_variables (rs->ddm, tmp, plist);
  
  AR_DD_FREE (tmp);

  s_assign_and (rs->ddm, &result, nb_I, I);
  
  return result;
}

			/* --------------- */

static void
s_compute_event_relations (ar_relsem *rs)
{
  int i;
  ar_ca_event **events;
  ar_ca_trans **trans;
  ccl_hash *indexes;

  if (rs->event_relations != NULL)
    return;
  s_compute_transition_relations (rs);

  DBG_START_TIMER (("compute relation for events"));
  trans = ar_ca_get_transitions (rs->super.ca);
  indexes = ccl_hash_create (NULL, NULL, NULL, NULL);
  events = ar_ca_get_events (rs->super.ca);
  rs->event_relations = ccl_new_array (ar_dd, rs->nb_events);

  for (i = 0; i < rs->nb_events; i++)
    {
      ccl_hash_find (indexes, events[i]);
      ccl_hash_insert (indexes, (void *) (intptr_t) i);
      rs->event_relations[i] = ar_dd_dup_zero (rs->ddm);
    }

  for (i = 0; i < rs->nb_transitions; i++)
    {
      int index;
      ar_dd tmp;
      ar_ca_event *te = ar_ca_trans_get_event (trans[i]);

      ccl_assert (ccl_hash_find (indexes, te));
      ccl_hash_find (indexes, te);
      index = (intptr_t) ccl_hash_get (indexes);
      tmp = ar_dd_compute_or (rs->ddm, rs->event_relations[index], 
			      rs->transition_relations[i]);
      AR_DD_FREE (rs->event_relations[index]);
      rs->event_relations[index] = tmp;
      ar_ca_event_del_reference (te);
    }

  ccl_hash_delete (indexes);
  DBG_END_TIMER ();
}

			/* --------------- */

ar_dd 
ar_relsem_compute_post (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I)
{
  return s_post (rs, S, nb_I, I);
}

			/* --------------- */

ar_dd 
ar_relsem_compute_pre (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I)
{
  return s_pre (rs, S, nb_I, I);
}

			/* --------------- */

ar_dd 
ar_relsem_compute_post_by_T (ar_relsem *rs, ar_dd S, ar_dd T, int nb_I, 
			     ar_dd *I)
{
  ar_dd R = s_apply (rs, S, T, nb_I, I, 0);
  return R;
}

			/* --------------- */

ar_dd 
ar_relsem_compute_pre_by_T (ar_relsem *rs, ar_dd S, ar_dd T, int nb_I, ar_dd *I)
{
  ar_dd tmp = MAKE_IT_PRIME (rs, S);
  ar_dd result = s_apply (rs, tmp, T, nb_I, I, 1);
  AR_DD_FREE (tmp);

  return result;
}

			/* --------------- */

ar_sgs_set *
ar_relsem_create_state_set (ar_relsem *rs, ar_dd S)
{
  ar_sgs_set *result = 
    s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  ccl_pre (rs != NULL);
  ccl_pre (S != NULL);

  DD(result, 0) = AR_DD_DUP (S);

  return result;
}

			/* --------------- */

ar_dd 
ar_relsem_compute_simple_set (ar_relsem *rs, ar_ca_expr *e)
{
  return ar_ca_expr_compute_dd (e, rs->ddm, rs->order, NULL);
}

			/* --------------- */

static void
s_join_projection_lists (ar_relsem *rs) 
{
  int i;
  int nb_vars;
  ar_ca_expr **pvars;
  ccl_hash *allvars;
  ccl_pointer_iterator *pi;

  if (rs->prime_to_second != NULL)
    return;

  nb_vars = ar_ca_get_number_of_variables (rs->super.ca);
  pvars = rs->prime_variables;
  allvars = ar_ca_get_variables (rs->super.ca, AR_CA_ALL_VARS);
  pi = ccl_hash_get_keys (allvars);

  rs->prime_to_second = ar_dd_create_projection_list (rs->ddm);
  rs->non_prime_to_prime = ar_dd_create_projection_list (rs->ddm);
  rs->prime = ar_dd_create_projection_list (rs->ddm);


  while (ccl_iterator_has_more_elements (pi))
    {
      ar_ca_expr *var = ccl_iterator_next_element (pi);
      ar_ca_expr *rep = varorder_get_representative (rs->order, var);

      if (rep == var)
	{
	  int dd_i = varorder_get_dd_index (rs->order, var);
	  ar_dd_projection_list_add (rs->ddm, &rs->prime_to_second, 
				     dd_i, dd_i);   
	}
      
    }
  ccl_iterator_delete (pi);
  ccl_hash_delete (allvars);

  rs->second_to_prime = AR_DD_DUP (rs->prime_to_second);
  for (i = 0; i <  nb_vars; i++)
    {
      ar_ca_expr *var = pvars[i];
      ar_ca_expr *rep = varorder_get_representative (rs->order, var);

      if (rep == var)
	{
	  int dd_i_prime = varorder_get_dd_index (rs->order, var);
#if PRIME_BEFORE_NON_PRIME
	  int dd_i_second = dd_i_prime + 3;
#else
	  int dd_i_second = dd_i_prime + 1;
#endif

	  ar_dd_projection_list_add (rs->ddm, &rs->prime_to_second, 
				     dd_i_prime, dd_i_second);   
	  ar_dd_projection_list_add (rs->ddm, &rs->non_prime_to_prime, 
				     dd_i_second, dd_i_second);
	  ar_dd_projection_list_add (rs->ddm, &rs->prime, 
				     dd_i_prime, dd_i_prime);
	  ar_dd_projection_list_add (rs->ddm, &rs->second_to_prime, 
				     dd_i_second, dd_i_prime);   
	}
    }

  ar_dd_assign_bin_op (rs->ddm, ar_dd_compute_and, &rs->non_prime_to_prime, 
		       rs->to_prime);
}

#if CCL_ENABLE_ASSERTIONS
static int
s_check_join (ar_relsem *rs, ar_dd T1, ar_dd T2, ar_dd R, int nbI, ar_dd *I)
{
  ar_dd tmp1 = ar_dd_dup_one (rs->ddm);
  ar_dd tmp2 = ar_relsem_compute_post_by_T (rs, tmp1, T1, nbI, I);
  ar_dd tmp3;
  
  
  AR_DD_FREE (tmp1);
  tmp1 = ar_relsem_compute_post_by_T (rs, tmp2, T2, nbI, I);
  AR_DD_FREE (tmp2);

  tmp2 = ar_dd_dup_one (rs->ddm);
  tmp3 = ar_relsem_compute_post_by_T (rs, tmp2, R, nbI, I);
  AR_DD_FREE (tmp2);
  AR_DD_FREE (tmp1);
  AR_DD_FREE (tmp3);
  ccl_assert (tmp1 == tmp3);

  return (tmp1 == tmp3);
}
#endif

ar_dd 
ar_relsem_join (ar_relsem *rs, ar_dd T1, ar_dd T2, int nbI, ar_dd *I)
{
  int i;
  ar_dd T1a = AR_DD_DUP (T1);
  ar_dd R = AR_DD_DUP (T2); /* R(V,V') = T2(V,V') */

  s_join_projection_lists (rs);
  /* R(V,V") = T2[V->V; V'-> V"] = T2(V,V") */
  ar_dd_assign_bin_op (rs->ddm, 
		       ar_dd_project_on_variables, &R, rs->prime_to_second);
  /* R(V',V")= T2[V->V'; V"-> V"]  = T2(V',V") */
  ar_dd_assign_bin_op (rs->ddm, 
		       ar_dd_project_on_variables, &R, rs->non_prime_to_prime);

  /* T1a(V,V') = add invariants as post-condition of T1(V,V') */
  for (i = 0; i < nbI; i++)
    {
      ar_dd pi = MAKE_IT_PRIME (rs, I[i]);
      ar_dd_assign_bin_op (rs->ddm, ar_dd_compute_and, &T1a, I[i]);
      ar_dd_assign_bin_op (rs->ddm, ar_dd_compute_and, &T1a, pi);
      AR_DD_FREE (pi);
    }

  /* R(V,V',V") = T1a(V,V') & T2(V', V") */
  ar_dd_assign_bin_op (rs->ddm, ar_dd_compute_and, &R, T1a);
  AR_DD_FREE (T1a);

  /* R(V,V") = exists V' (T1a(V,V') & T2(V', V")) */
  ar_dd_assign_bin_op (rs->ddm, ar_dd_project_variables, &R, rs->prime);
  /* R(V,V') = exists W (T1a(V,W) & T2(W, V')) */
  ar_dd_assign_bin_op (rs->ddm, 
		       ar_dd_project_on_variables, &R, rs->second_to_prime);
  /* R(V,V') = add invariants as post-condition */
  for (i = 0; i < nbI; i++)
    {
      ar_dd pi = MAKE_IT_PRIME (rs, I[i]);
      ar_dd_assign_bin_op (rs->ddm, ar_dd_compute_and, &R, pi);
      AR_DD_FREE (pi);
    }

  ccl_assert (s_check_join (rs, T1, T2, R, nbI, I));

  return R;
}

#if POSTPRE_USE_GLOBAL_TRANSITION_RELATION
static ar_dd
s_postpre (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I, int pre)
{
  ar_dd result;

  s_compute_global_transition_relation (rs);

  DBG_F_START_TIMER (comp_post, ("compute post/E"));
  result = s_apply (rs, S, rs->global_transition_relation, nb_I, I, pre);
  DBG_F_END_TIMER (comp_post);

  return result;
}
#else
static ar_dd
s_postpre (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I, int pre)
{
  int i;
  ar_dd tmp;
  ar_dd *rels;
  ar_dd zero = ar_dd_zero (rs->ddm);
  ar_dd one = ar_dd_one (rs->ddm);
  ar_dd result = AR_DD_DUP (zero);
  int max = rs->nb_events;
  ar_dd *as = ccl_new_array (ar_dd, max);

  s_compute_event_relations (rs);
  rels = rs->event_relations;
  
  DBG_START_TIMER (("compute post/E"));
  for(i = 0; i < max; i++)
    as[i] = s_apply (rs, S, rels[i], 1, &one, pre);
  DBG_END_TIMER ();

  DBG_START_TIMER (("compute union of post/E"));
  for(i = 0; i < max; i++)
    {
      tmp = ar_dd_compute_or (rs->ddm, as[i], result);
      AR_DD_FREE (as[i]);
      AR_DD_FREE (result);
      result = tmp;
    }
  DBG_END_TIMER ();

  if (nb_I)
    {
      DBG_START_TIMER (("applying assertions"));
      s_assign_and (rs->ddm, &result, nb_I, I);
      DBG_END_TIMER ();
    }
  ccl_delete (as);

  return result;
}
#endif
			/* --------------- */

static ar_dd
s_post (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I)
{
  return s_postpre (rs, S, nb_I, I, 0);
}

			/* --------------- */

static ar_dd
s_pre (ar_relsem *rs, ar_dd S, int nb_I, ar_dd *I)
{
  ar_dd tmp = MAKE_IT_PRIME (rs, S);
  ar_dd result = s_postpre (rs, tmp, nb_I, I, 1);

  AR_DD_FREE (tmp);

  return result;
}

			/* --------------- */

struct display_stack_data
{
  ccl_log_type log;
  const char *sep;
  ar_sgs_display_format df;
  int is_first;
};

struct assignment_stack
{
  struct assignment_stack *next;
  ar_ca_expr *var;
  ar_ca_expr *othervar;
  int min, max;
};

			/* --------------- */

static void
s_display_stack_rec (ar_relsem *rs, ccl_log_type log, 
		     struct assignment_stack *dds)
{  
  const ar_ca_domain *dom;
  if (dds == NULL)
    return;

  if (dds->next != NULL)
    {
      s_display_stack_rec (rs, log, dds->next);
      ccl_log (log, ", ");
    }
  
  ar_ca_expr_log (log, dds->var);

  if (0 && dds->othervar != NULL)
    {
      ccl_log (log, " = ");
      ar_ca_expr_log (log, dds->othervar);
      return;
    }

  dom = ar_ca_expr_variable_get_domain (dds->var);
  if (dds->min == dds->max)
    {
      int val = ar_ca_domain_get_ith_value (dom, dds->min);
      ccl_log (log, " = ");
      ar_ca_domain_log_value (log, dom, val, rs->eman);
    }
  else if (ar_ca_domain_is_integer (dom))
    {
      int val = ar_ca_domain_get_ith_value (dom, dds->min);
      ccl_log (log, " in [");
      ar_ca_domain_log_value (log, dom, val, rs->eman);
      ccl_log (log, ", ");
      val = ar_ca_domain_get_ith_value (dom, dds->max);
      ar_ca_domain_log_value (log, dom, val, rs->eman);
      ccl_log (log, "]");
    }
  else if (ar_ca_domain_is_enum (dom))
    {
      int i;

      ccl_log (log, " in {");
      for (i = dds->min; i <= dds->max; i++)
	{
	  int val = ar_ca_domain_get_ith_value (dom, i);
	  ar_ca_domain_log_value (log, dom, val, rs->eman);
	  if (i < dds->max)
	    ccl_log (log, ", ");
	}
      ccl_log (log, "}");
    }
  else
    {
      ccl_log (log, " in bool");
    }
}

			/* --------------- */

static void
s_display_stack (ar_relsem *rs, struct assignment_stack *dds,
		 void *data)
{
  struct display_stack_data *dsd = data;

  if (dsd->is_first)
    dsd->is_first = 0;
  else 
    ccl_log (dsd->log, dsd->sep);
  s_display_stack_rec (rs, dsd->log, dds);
}

			/* --------------- */

static void
s_enumerate_paths (ar_relsem *rs, ccl_log_type log, ar_dd pvar,
		   struct assignment_stack *dds, ar_dd X, 
		   void (*accepted)(ar_relsem *rs,
				    struct assignment_stack *dds,
				    void *data),
		   void *acc_data)
{
  if (X == ar_dd_zero (rs->ddm))
    return;

  if (AR_DD_ADDR (pvar) == ar_dd_one (rs->ddm))
    {
      ccl_assert (X == ar_dd_one (rs->ddm));
      accepted (rs, dds, acc_data);
    }
  else 
    {
      struct assignment_stack data;

      data.next = dds;      
      data.var = varorder_get_variable_from_dd_index (rs->order, 
						      AR_DD_INDEX (pvar));

      if (! ar_dd_is_one (rs->ddm, X) && AR_DD_INDEX (X) == AR_DD_INDEX (pvar))
	{
	  int arity = AR_DD_ARITY (X);

	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      int i;
	      
	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = (AR_DD_POLARITY (X)
			       ? AR_DD_NOT(AR_DD_ENTHSON (X,i))
			       : AR_DD_ENTHSON (X,i));
		  
		  data.min = data.max = i;
		  s_enumerate_paths (rs, log, ar_dd_card_list_get_tail (pvar), 
				     &data, son, accepted, acc_data);
		}
	    }
	  else
	    {
	      int i;
	      
	      for (i = 0; i < arity; i++)
		{
		  ar_dd son = (AR_DD_POLARITY (X)
			       ? AR_DD_NOT(AR_DD_CNTHSON (X,i))
			       : AR_DD_CNTHSON (X,i));
		  
		  data.min = AR_DD_MIN_OFFSET_NTHSON (X, i);
		  data.max = AR_DD_MAX_OFFSET_NTHSON (X, i);
		  s_enumerate_paths (rs, log, ar_dd_card_list_get_tail (pvar), 
				     &data, son, accepted, acc_data);
		}
	    }
	}
      else
	{
	  const ar_ca_domain *dom = ar_ca_expr_variable_get_domain (data.var);
	  int max = ar_ca_domain_get_cardinality (dom) - 1;
	  
	  ccl_assert (max + 1 ==  ar_dd_card_list_get_card (pvar));

	  if (! IS_TRIVIAL (rs, X))
	    ccl_assert (AR_DD_INDEX (X) > varorder_get_dd_index (rs->order, 
								 data.var));

	  data.min = 0;
	  data.max = max;

	  s_enumerate_paths (rs, log, ar_dd_card_list_get_tail (pvar),
			     &data, X, accepted, acc_data);
	}
    }
}

			/* --------------- */

static void 
s_rs_post_add_set (ar_sgs *sgs, ar_identifier *setid, ar_sgs_set *set)
{
  char *setid_str = ar_identifier_to_string (setid);

  if (! ar_bang_str_to_bang_t (setid_str, NULL))
    {
      ar_node *node = ar_sgs_get_node (sgs);
      ar_identifier *id = ar_node_get_name (node);
      char *id_str = ar_identifier_to_string (id);
    
      char *relid_str = ccl_string_format_new ("%s!%s", id_str, setid_str);
      ar_identifier *relid = ar_identifier_create (relid_str);

      if (ar_sgs_set_is_state_set (set))
	{
	  ccl_assert (WIDTH (set) == 1);

	  ar_mec5_add_configuration_relation (RS (sgs), relid, 
					      AR_BANG_SUPER_CONFIGURATIONS,
					      1, &(DD(set,0)));

	}
      else
	{
	  ar_mec5_add_transition_relation (RS (sgs), relid, &SETS(set));
	}

      if (ccl_debug_is_on)
	{
	  ccl_debug ("register relation '");
	  ar_identifier_log (CCL_LOG_DEBUG, relid);
	  ccl_debug ("'.\n");
	}
      ccl_string_delete (id_str);

      ccl_string_delete (relid_str);
      ar_identifier_del_reference (relid);
      ar_identifier_del_reference (id);
      ar_node_del_reference (node);
    }
  ccl_string_delete (setid_str);
}

			/* --------------- */

static void 
s_rs_pre_remove_set (ar_sgs *sgs, ar_identifier *id)
{
  ccl_pre (id != NULL);

  if (ccl_debug_is_on)
    {
      ccl_debug ("unregister relation '");
      ar_identifier_log (CCL_LOG_DEBUG, id);
      ccl_debug ("'.\n");
    }

  if (ar_mec5_has_relation (id))
    ar_mec5_remove_relation (id);
  else
    {
      ar_node *node = ar_sgs_get_node (sgs);
      ar_identifier *nodeid = ar_node_get_name (node);
      char *id_str = ar_identifier_to_string (nodeid);
      char *setid_str = ar_identifier_to_string (id);
      char *relid_str = ccl_string_format_new ("%s!%s", id_str, setid_str);
      ar_identifier *relid = ar_identifier_create (relid_str);
      if (ar_mec5_has_relation (relid))
	ar_mec5_remove_relation (relid);
      ccl_string_delete (id_str);
      ccl_string_delete (setid_str);
      ccl_string_delete (relid_str);
      ar_identifier_del_reference (relid);
      ar_identifier_del_reference (nodeid);
      ar_node_del_reference (node);
    }
}

			/* --------------- */

static void 
s_rs_display_set (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *set,
		  ar_sgs_display_format df)
{
  ar_relsem *rs = RS (sgs);
  int is_state_set = ar_sgs_set_is_state_set (set);
  struct display_stack_data data = { log, "\n", df, 1 };

  if (df == AR_SGS_DISPLAY_DOT)
    data.sep = "\\n";

  if (is_state_set)
    s_enumerate_paths (rs, log, rs->not_primed_variables, NULL, DD (set, 0), 
		       s_display_stack, &data);
  else
    {
      int i;
      ar_ca_event **events = ar_ca_get_events (rs->super.ca);

      for (i = 0; i < rs->nb_events; i++)
	{
	  if (DD (set, i) == ar_dd_zero (rs->ddm))
	    continue;
	  ccl_log (log, "e = ");
	  ar_ca_event_log (log, events[i]);
	  ccl_log (log, " and {\n");
	  s_enumerate_paths (rs, log, rs->all_variables, NULL, DD (set, i),
			     s_display_stack, &data);
	  ccl_log (log, "}\n");
	}      
    }
}

			/* --------------- */

static ar_expr *
s_variable_in_range (ar_relsem *rs, ar_expr *var, int min, int max)
{
  ar_expr *tmp[3];
  ar_expr *result;
  ar_type *type = ar_expr_get_type (var);
  ar_type_kind kind = ar_type_get_kind (type);

  if (min == max)
    {
      if (kind == AR_TYPE_BOOLEANS)
	{
	  if (min)	    
	    result = ar_expr_add_reference (var);	  
	  else
	    result = ar_expr_crt_unary (AR_EXPR_NOT, var);
	}
      else 
	{
	  ar_constant *c = ar_type_get_ith_element (type, min);
	  ar_expr *cst = ar_expr_crt_constant (c);
	  result = ar_expr_crt_binary (AR_EXPR_EQ, var, cst);
	  ar_constant_del_reference (c);
	  ar_expr_del_reference (cst);
	}
    }
  else if (kind == AR_TYPE_RANGE)
    {
      ar_constant *c = ar_type_get_ith_element (type, min);
      ar_expr *cst = ar_expr_crt_constant (c);
      tmp[0] = ar_expr_crt_binary (AR_EXPR_LEQ, cst, var);
      ar_constant_del_reference (c);
      ar_expr_del_reference (cst);
      c = ar_type_get_ith_element (type, max);
      cst = ar_expr_crt_constant (c);
      tmp[1] = ar_expr_crt_binary (AR_EXPR_LEQ, var, cst);
      ar_constant_del_reference (c);
      ar_expr_del_reference (cst);
      result = ar_expr_crt_binary (AR_EXPR_AND, tmp[0], tmp[1]);
      ar_expr_del_reference (tmp[0]);
      ar_expr_del_reference (tmp[1]);
    }
  else if (kind == AR_TYPE_SYMBOL_SET || kind == AR_TYPE_ENUMERATION)
    {
      int i = min;
      ar_constant *c = ar_type_get_ith_element (type, i);
      ar_expr *cst = ar_expr_crt_constant (c);

      result = ar_expr_crt_binary (AR_EXPR_EQ, var, cst);
      ar_constant_del_reference (c);
      ar_expr_del_reference (cst);
      for (i++; i <= max; i++)
	{
	  c = ar_type_get_ith_element (type, i);
	  cst = ar_expr_crt_constant (c);
	  tmp[0] = ar_expr_crt_binary (AR_EXPR_EQ, var, cst);
	  tmp[1] = ar_expr_crt_binary (AR_EXPR_OR, result, tmp[0]);
	  ar_expr_del_reference (tmp[0]);
	  ar_expr_del_reference (result);
	  result = tmp[1];
	  ar_constant_del_reference (c);
	  ar_expr_del_reference (cst);
	}
    }
  else
    {
      ccl_unreachable ();
    }
  ar_type_del_reference (type);

  return result;
}

			/* --------------- */

static int
s_set_are_equal (relsem_set *S1, relsem_set *S2)
{
  int i;

  if (WIDTH (S1) != WIDTH (S2))
    return 0;

  for (i = 0; i < WIDTH (S1); i++)
    if (DD (S1, i) != DD (S2, i))
      return 0;
  return 1;
}

			/* --------------- */

static int 
s_rs_sets_are_equal (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  return s_set_are_equal (RS_SET (S1), RS_SET (S2));
}

			/* --------------- */

static ccl_hash *
s_rs_get_events (ar_sgs *sgs, ar_sgs_set *T)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ccl_hash *result = 
    ccl_hash_create (NULL, NULL, NULL, 
		     (ccl_delete_proc *) ar_ca_event_del_reference);
  ar_ca *ca = ar_sgs_get_constraint_automaton (sgs);
  ar_ca_event **events = ar_ca_get_events (ca);

  for (i = 0; i < rs->nb_events; i++)
    {
      if (! ar_dd_is_zero (rs->ddm, DD (T,i)))
	{
	  ccl_assert (! ccl_hash_find (result, events[i]));
	  ccl_hash_find (result, events[i]);
	  ccl_hash_insert (result, events[i]);
	  ar_ca_event_add_reference (events[i]);
	}
    }
  ar_ca_del_reference (ca);

  return result;
}

static ar_sgs_set * 
s_rs_get_rel_for_event (ar_sgs *sgs, ar_sgs_set *T, ar_ca_event *e)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_ca_event **events = ar_ca_get_events (rs->super.ca);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);
  ar_dd zero = ar_dd_zero (rs->ddm);
  
  for (i = 0; i < rs->nb_events; i++)
    {
      ar_dd set;

      if (events[i] == e)
	set = DD (T, i);
      else
	set = zero;
      DD (R, i) = AR_DD_DUP (set);
    }

  return R;
}

			/* --------------- */

static ar_expr *
s_rs_states_to_formula_rec (ar_relsem *rs, ar_dd X, ccl_hash *index2vars)
{
  int index = AR_DD_INDEX (X);
  ar_expr *result = NULL;

  if (X == ar_dd_zero (rs->ddm))
    result = ar_expr_crt_false ();
  else if (X == ar_dd_one (rs->ddm))
    result = ar_expr_crt_true ();
  else
    {
      int i;
      int arity = AR_DD_ARITY (X);
      ar_expr *tmp[3];
      ar_expr *var;

      ccl_assert (ccl_hash_find (index2vars, (void *) (intptr_t) index));

      ccl_hash_find (index2vars, (void *) (intptr_t) index);
      var = ccl_hash_get (index2vars);

      for (i = 0; i < arity; i++)
	{
	  int min, max;
	  ar_dd son;
		  
	  if (AR_DD_CODE (X) == EXHAUSTIVE)
	    {
	      son = AR_DD_EPNTHSON (X,i);
	      min = max = i;
	    }
	  else
	    {
	      son = AR_DD_CPNTHSON (X,i);
	      min = AR_DD_MIN_OFFSET_NTHSON (X, i);
	      max = AR_DD_MAX_OFFSET_NTHSON (X, i);
	    }

	  tmp[0] = s_rs_states_to_formula_rec (rs, son, index2vars);
	  tmp[1] = s_variable_in_range (rs, var, min, max);
	  tmp[2] = ar_expr_crt_binary (AR_EXPR_AND, tmp[0], tmp[1]);
	  ar_expr_del_reference (tmp[0]);
	  ar_expr_del_reference (tmp[1]);

	  if (result == NULL)
	    result = tmp[2];
	  else
	    {
	      tmp[0] = ar_expr_crt_binary (AR_EXPR_OR, result, tmp[2]);
	      ar_expr_del_reference (result);
	      ar_expr_del_reference (tmp[2]);
	      result = tmp[0];
	    }
	}
    }

  return result;
}
			/* --------------- */


static ar_expr *
s_rs_states_to_formula (ar_sgs *sgs, ar_sgs_set *S, ccl_list *pvars,
			ccl_list *vars, int exist)
{
  ar_expr *result;
  ccl_pair *p[2];
  ccl_hash *ddindex2vars = ccl_hash_create (NULL, NULL, NULL, NULL);
  ar_relsem *rs = RS (sgs);
  ar_dd varlist = ar_dd_create_projection_list (rs->ddm);
  ar_dd tmp, Rdd;

  ccl_assert (ar_sgs_set_is_state_set (S));

  for (p[0] = FIRST (pvars), p[1] = FIRST (vars); p[0]; 
       p[0] = CDR (p[0]), p[1] = CDR (p[1]))
    {
      ar_expr *var = CAR (p[1]);
      int ddindex = varorder_get_dd_index (sgs->order, CAR (p[0]));
      
      if (! ccl_hash_find (ddindex2vars, (void *) (intptr_t) ddindex))
	ccl_hash_insert (ddindex2vars, var);
      ar_dd_projection_list_add (rs->ddm, &varlist, ddindex, ddindex);
    }

  if (exist) 
    tmp = DD (S,0);
  else
    tmp = AR_DD_NOT (DD (S,0));

  Rdd = ar_dd_project_on_variables (rs->ddm, tmp, varlist);
  if (! exist)
    Rdd = AR_DD_NOT (Rdd);

  result = s_rs_states_to_formula_rec (rs, Rdd, ddindex2vars);
  
  ccl_hash_delete (ddindex2vars);
  AR_DD_FREE (varlist);
  AR_DD_FREE (Rdd);

  return result;
}

			/* --------------- */

static ar_ca_expr *
s_rs_states_to_ca_expr (ar_sgs *sgs, ar_sgs_set *S, ccl_hash *vars)
{
  ar_ca_expr *result;
  ar_relsem *rs = RS (sgs);
  ar_dd Rdd;

  ccl_assert (ar_sgs_set_is_state_set (S));
  
  if (vars != NULL)
    {
      ar_dd varlist = ar_dd_create_projection_list (rs->ddm);
      ccl_pointer_iterator *pi = ccl_hash_get_keys (vars);
      while (ccl_iterator_has_more_elements (pi))
	{
	  ar_expr *var = ccl_iterator_next_element (pi);
	  int ddindex = varorder_get_dd_index (sgs->order, var);
	  
	  ar_dd_projection_list_add (rs->ddm, &varlist, ddindex, ddindex);
	}
      ccl_iterator_delete (pi);
      Rdd = ar_dd_project_on_variables (rs->ddm, DD (S,0), varlist);
      AR_DD_FREE (varlist);
    }
  else
    {
      Rdd = AR_DD_DUP (DD (S,0));
    }
  result = 
    ar_ca_expr_compute_expr_from_dd (rs->ddm, rs->eman, rs->order, Rdd, NULL);
  
  AR_DD_FREE (Rdd);

  return result;
}

			/* --------------- */

static void 
s_rs_destroy (ar_sgs *sgs)
{
  int i;
  ar_relsem *rs = RS (sgs);

  ccl_hash_delete (rs->is_state_variable);

  ccl_zdelete (ccl_list_delete, rs->ordered_state_variables);

  ccl_zdelete (AR_DD_FREE, rs->prime_to_second);
  ccl_zdelete (AR_DD_FREE, rs->non_prime_to_prime);
  ccl_zdelete (AR_DD_FREE, rs->prime);
  ccl_zdelete (AR_DD_FREE, rs->second_to_prime);

  ccl_zdelete (AR_DD_FREE, rs->all_variables);
  ccl_zdelete (AR_DD_FREE, rs->not_primed_variables);
  ccl_zdelete (AR_DD_FREE, rs->to_prime);
  ccl_zdelete (AR_DD_FREE, rs->to_non_prime);
  ccl_zdelete (AR_DD_FREE, rs->non_prime);
  ccl_zdelete (AR_DD_FREE, rs->state_variables);
  ccl_zdelete (AR_DD_FREE, rs->flow_variables);

  ccl_zdelete (AR_DD_FREE, rs->assertion);
  ccl_zdelete (AR_DD_FREE, rs->post_reachables);
  ccl_zdelete (AR_DD_FREE, rs->reachables);
  ccl_zdelete (AR_DD_FREE, rs->initial);

  if (rs->assertions)
    {
      for (i = 0; i < rs->nb_assertions; i++)
	AR_DD_FREE (rs->assertions[i]);
      ccl_delete (rs->assertions);
    }
  if (rs->post_assertions)
    {
      for (i = 0; i < rs->nb_assertions; i++)
	AR_DD_FREE (rs->post_assertions[i]);
      ccl_delete (rs->post_assertions);
    }

  if (rs->transition_relations)
    {
      ccl_zdelete (AR_DD_FREE, rs->global_transition_relation);
      for (i = 0; i < rs->nb_transitions; i++)
	{
	  ccl_zdelete (AR_DD_FREE, rs->transition_relations[i]);
	}
      ccl_delete (rs->transition_relations);
    }

  if (rs->event_relations != NULL)
    {
      for (i = 0; i < rs->nb_events; i++)
	ccl_zdelete (AR_DD_FREE, rs->event_relations[i]);

      ccl_delete (rs->event_relations);
    }

  if (rs->event_dds != NULL)
    {
      for (i = 0; i < rs->nb_events; i++)
	ccl_zdelete (AR_DD_FREE, rs->event_dds[i]);

      ccl_delete (rs->event_dds);
    }
  ccl_zdelete (AR_DD_FREE, rs->event_pl);
  
  for (i = ar_ca_get_number_of_variables (rs->super.ca) - 1; i >= 0; i--)
    ccl_zdelete (ar_ca_expr_del_reference, rs->prime_variables[i]);
  ccl_delete (rs->prime_variables);

  varorder_del_reference (rs->order);

  ccl_zdelete (ar_idtable_del_reference, rs->event_indexes);
  ccl_zdelete (ar_idtable_del_reference, rs->event_types);

  ar_ca_exprman_del_reference (rs->eman);
}

			/* --------------- */

static double 
s_rs_card (ar_sgs *sgs, ar_sgs_set *S)
{
  int is_state = ar_sgs_set_is_state_set (S);
  double result = ar_relsem_get_cardinality (RS(sgs), DD (S, 0), ! is_state);

  if (! is_state)
    {
      int i;
      int max = WIDTH (S);
      for (i = 1; i < max; i++)
	result += ar_relsem_get_cardinality (RS(sgs), DD (S, i), 1);
    }
  return result;
}

			/* --------------- */

static int
s_rs_is_empty (ar_sgs *sgs, ar_sgs_set *S)
{
  int i;
  int max = WIDTH (S);
  ar_dd zero = ar_dd_zero (RS (sgs)->ddm);
  int result = (DD (S, 0) == zero);

  for (i = 1; i < max && result; i++)
    result = (DD (S, i) == zero);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_post (ar_sgs *sgs, ar_sgs_set *S)
{
  ar_sgs_set *R = s_relsem_set_create (1, 0);
  DD (R, 0) = s_post (RS (sgs), DD (S, 0), RS(sgs)->nb_assertions, 
		      RS(sgs)->assertions);

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_pre (ar_sgs *sgs, ar_sgs_set *S)
{
  ar_sgs_set *R = s_relsem_set_create (1, 0);
  DD (R, 0) = s_pre (RS (sgs), DD (S, 0), RS(sgs)->nb_assertions,
		     RS(sgs)->assertions);

  return R;
}

			/* --------------- */

static void 
s_rs_to_mec4 (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	      ccl_config_table *conf)
{
  ccl_log (log, "symbolic graph to mec4 translator not yet implemented\n");
}

			/* --------------- */

static void
s_log_event_name (ccl_log_type log, ar_relsem *rs, int eventindex)
{
  ar_ca *ca = ar_sgs_get_constraint_automaton (SGS (rs));
  ar_ca_event **events = ar_ca_get_events (ca);
  ar_ca_event_log (log, events[eventindex]);
  ar_ca_del_reference (ca);
}

			/* --------------- */

static ar_dd 
s_compute_useful_variables (ar_ddm ddm, ar_dd N, ar_dd s)
{
  ar_dd result = 
    ar_dd_entry_for_operation (ddm, (void *) s_compute_useful_variables, N, s);

  if (result != NULL)
    return AR_DD_DUP (result);

  if (ar_dd_is_zero (ddm, N) || ar_dd_is_zero (ddm, s))
    result = ar_dd_create_projection_list (ddm);
  else if (ar_dd_is_one (ddm, N) && ar_dd_is_one (ddm, s))
    result = ar_dd_get_variables (ddm, s);
  else if (ar_dd_is_one (ddm, N) || AR_DD_INDEX (s) < AR_DD_INDEX (N))
    {
      int i;
      int arity = AR_DD_ARITY (s);
      int index = AR_DD_INDEX (s);
      ar_dd son;

      for (i = 0; i < arity; i++)
	{
	  son = AR_DD_PNTHSON (s, i);
	  if (! ar_dd_is_zero (ddm, son))
	    break;
	}
      ccl_assert (i < arity);
      result = s_compute_useful_variables (ddm, N, son);
      ar_dd_projection_list_add (ddm, &result, index, index);
    }
  else
    {
      int i, is, min_is;
      int arity = AR_DD_ARITY (s);
      int index = AR_DD_INDEX (s);
      ar_ddcode encoding = AR_DD_CODE (N);
      ar_dd son;
      int addit = 0;

      ccl_assert (AR_DD_INDEX (s) == AR_DD_INDEX (N));

      for (is = 0; is < arity; is++)
	{
	  son = AR_DD_PNTHSON (s, is);
	  if (! ar_dd_is_zero (ddm, son))
	    break;
	}

      ccl_assert (is < arity);
      if (encoding == COMPACTED)
	{
	  min_is = AR_DD_MIN_OFFSET_NTHSON (s, is);
	  ccl_assert (min_is == AR_DD_MAX_OFFSET_NTHSON (s, is));
	}
	      
      arity = AR_DD_ARITY (N);
      result = ar_dd_create_projection_list (ddm);
      for (i = 0; i < arity; i++)
	{
	  ar_dd sonN = AR_DD_PNTHSON (N, i);	      
	  if (! ar_dd_is_zero (ddm, sonN))
	    {
	      ar_dd tmp0 = s_compute_useful_variables (ddm, sonN, son);
	      ar_dd tmp1 = ar_dd_compute_and (ddm, result, tmp0);
	      AR_DD_FREE (tmp0);
	      AR_DD_FREE (result);
	      result = tmp1;
	      if (encoding == EXHAUSTIVE && !addit)
		addit = (i != is);
	      else if (encoding == COMPACTED && !addit)
		addit = 
		  (min_is < AR_DD_MIN_OFFSET_NTHSON(N,i) || 
		   AR_DD_MAX_OFFSET_NTHSON(N,i) < min_is ||
		   AR_DD_MIN_OFFSET_NTHSON(N,i) < AR_DD_MAX_OFFSET_NTHSON(N,i));
	    }
	}
      if (addit)
	ar_dd_projection_list_add (ddm, &result, index, index);
    }
  ar_dd_memoize_operation (ddm, (void *) s_compute_useful_variables, N, s, 
			   result);

  return result;
}

			/* --------------- */

static ar_dd
s_compute_neightbours (ar_relsem *rs, ar_dd s, ar_sgs_set *T, ar_dd inv,
		       int mode)
{
  int i;
  ar_dd result = ar_dd_dup_zero (rs->ddm);

  for (i = 0; i < rs->nb_events; i++)
    {
      ar_dd tmp0, tmp1;

      if ((mode & DOT_DIFF_STATE_MODE_PRE) != 0)
	{
	  tmp0 = ar_relsem_compute_pre_by_T (rs, s, DD (T, i), 1, &inv);
	  tmp1 = ar_dd_compute_or (rs->ddm, result, tmp0);
	  AR_DD_FREE (tmp0);
	  AR_DD_FREE (result);
	  result = tmp1;
	}

      if ((mode & DOT_DIFF_STATE_MODE_POST) != 0)
	{
	  tmp0 = ar_relsem_compute_post_by_T (rs, s, DD (T, i), 1, &inv);
	  tmp1 = ar_dd_compute_or (rs->ddm, result, tmp0);
	  AR_DD_FREE (tmp0);
	  AR_DD_FREE (result);
	  result = tmp1;
	}
    }
  return result;
}

			/* --------------- */

static void
s_display_dot_state (ar_sgs *sgs, ccl_log_type log, ar_dd s, ar_sgs_set *T,
		     ar_dd inv, int mode)
{  
  ar_relsem *rs = RS (sgs);
  struct display_stack_data dsd = { log, "" };
  ar_dd pl;
  
  if (mode == DOT_DIFF_STATE_MODE_NONE)
    {
      s = AR_DD_DUP (s);
      pl = AR_DD_DUP (rs->not_primed_variables);
    }
  else
    {
      ar_dd N = s_compute_neightbours (rs, s, T, inv, mode);
      pl = s_compute_useful_variables (rs->ddm, N, s);
      AR_DD_FREE (N);
      s = ar_dd_project_on_variables (rs->ddm, s, pl);
    }
  s_enumerate_paths (rs, log, pl, NULL, s, s_display_stack, &dsd);
  AR_DD_FREE (s);
  AR_DD_FREE (pl);
}
			/* --------------- */

static void
s_rs_to_dot (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf)
{
  ar_relsem *rs = RS (sgs);
  ar_dd done = ar_dd_dup_zero (rs->ddm);
  ar_dd todo;
  ar_identifier *name = ar_sgs_get_name (sgs);
  ar_dd inv = AR_DD_DUP (DD (S, 0));
  int gc_status = ar_dd_get_garbage_collection_status (rs->ddm);
  int mode = DOT_DIFF_STATE_MODE_DEFAULT;
  const char *strmode = 
    ccl_config_table_get (conf, ACHECK_PREF_DOT_DIFF_STATE_MODE_STR);

  if (strmode != NULL)
    {
      if (strcmp (strmode, "none") == 0)
	mode = DOT_DIFF_STATE_MODE_NONE;
      else if (strcmp (strmode, "post") == 0)
	mode = DOT_DIFF_STATE_MODE_POST;
      else if (strcmp (strmode, "pre") == 0)
	mode = DOT_DIFF_STATE_MODE_PRE;
      else if (strcmp (strmode, "both") == 0)
	mode = DOT_DIFF_STATE_MODE_BOTH;
      else
	{
	  ccl_warning ("unknown dot-diff-mode value '%s'.\n", strmode);
	}
    }
  s_compute_reachables (RS (sgs)); 
  s_apply_assertions (rs, &inv, 0);
  
  ar_dd_set_garbage_collection_status (rs->ddm, 0);

  ccl_log (log, "digraph StateGraph {\n");
  ccl_log (log, "  label=\"");
  ar_identifier_log (log, name);
  ar_identifier_del_reference (name);
  ccl_log (log, "\";\n");

  todo = ar_dd_compute_and (rs->ddm, rs->reachables, DD (S, 0)); 

  while (! ar_dd_is_zero (rs->ddm, todo))
    {
      int i;
      ar_dd tmp[2];
      ar_dd s = ar_dd_pick (rs->ddm, todo, rs->not_primed_variables);
      ccl_list *loops = ccl_list_create ();
      
      ccl_assert (ar_relsem_get_cardinality (rs, s, 0) == 1);
      ccl_log (log, "  N%p [label=\"", s);
      s_display_dot_state (sgs, log, s, T, inv, mode);
      ccl_log (log, "\"];\n");

      tmp[0] = ar_dd_compute_diff (rs->ddm, todo, s);
      AR_DD_FREE (todo);
      todo = tmp[0];

      tmp[0] = ar_dd_compute_or (rs->ddm, done, s);
      AR_DD_FREE (done);
      done = tmp[0];
      
      for (i = 0; i < rs->nb_events; i++)
	{
	  ar_dd post_s = s_apply (rs, s, DD (T, i), 1, &inv, 0);

	  tmp[0] = ar_dd_compute_diff (rs->ddm, post_s, done);
	  tmp[1] = ar_dd_compute_or (rs->ddm, todo, tmp[0]);
	  AR_DD_FREE (todo);
	  AR_DD_FREE (tmp[0]);
	  todo = tmp[1];

	  while (! ar_dd_is_zero (rs->ddm, post_s))
	    {
	      ar_dd sp = ar_dd_pick (rs->ddm, post_s, rs->not_primed_variables);
	      tmp[0] = ar_dd_compute_diff (rs->ddm, post_s, sp);
	      AR_DD_FREE (post_s);
	      post_s = tmp[0];
	      if (sp == s)
		ccl_list_add (loops, (void *) (uintptr_t) i);
	      else
		{
		  ccl_log (log, "  N%p -> N%p [label=\"", s, sp);
		  s_log_event_name (log, rs, i);
		  ccl_log (log, "\"];\n");
		}
	      AR_DD_FREE (sp);
	    }
	  AR_DD_FREE (post_s);
	}
      
      if (! ccl_list_is_empty (loops)) {
	uintptr_t i = (uintptr_t) ccl_list_take_first (loops);
	ccl_log (log, "  N%p -> N%p [label=\"", s, s);
	s_log_event_name (log, rs, i);
	while (! ccl_list_is_empty (loops))
	  {
	    i = (uintptr_t) ccl_list_take_first (loops);
	    ccl_log (log, "\\n");
	    s_log_event_name (log, rs, i);
	  }
	ccl_log (log, "\"];\n");
      }
      ccl_list_delete (loops);
      AR_DD_FREE (s);
    }

  AR_DD_FREE (todo);
  AR_DD_FREE (done);
  AR_DD_FREE (inv);

  ccl_log (log, "}\n");
  ar_dd_set_garbage_collection_status (rs->ddm, gc_status);
}

			/* --------------- */

static void 
s_rs_to_gml (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf)
{
  ccl_log (log, "symbolic graph to gml translator not yet implemented\n");
}

			/* --------------- */

static ar_sgs_set * 
s_rs_src_or_tgt (ar_relsem *rs, ar_sgs_set *T, ar_dd varlist)
{
  int i;
  int max  = rs->nb_events;
  ar_dd Rdd = ar_dd_project_on_variables (rs->ddm, DD (T, 0), varlist);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  for (i = 1; i < max; i++)
    {
      ar_dd tmp0 = ar_dd_project_on_variables (rs->ddm, DD (T, i), varlist);
      ar_dd tmp1 = ar_dd_compute_or (rs->ddm, Rdd, tmp0);
      AR_DD_FREE (Rdd);
      AR_DD_FREE (tmp0);
      Rdd = tmp1;
    }
  DD (R, 0) = Rdd;

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_proj (ar_sgs *sgs, ar_sgs_set *X, int on_state)
{   
  int i;
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *result = 
    s_relsem_set_create (ar_sgs_set_is_state_set (X), WIDTH (X));
  ar_dd varlist = on_state ? rs->state_variables : rs->flow_variables;

  DD (result, 0) = ar_dd_project_on_variables (rs->ddm, DD (X, 0), varlist);
  for (i = 1; i < WIDTH (X); i++)
    DD (result, i) = ar_dd_project_on_variables (rs->ddm, DD (X, i), varlist);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_pick (ar_sgs *sgs, ar_sgs_set *X)
{   
  int i = 0;
  ar_relsem *rs = RS (sgs);
  ar_dd zero = ar_dd_zero (rs->ddm);
  int width = WIDTH (X);
  int is_state_set = ar_sgs_set_is_state_set (X);
  ar_sgs_set *result = s_relsem_set_create (is_state_set, width);
  ar_dd varlist = is_state_set ? rs->not_primed_variables : rs->all_variables;

  DD (result, 0) = ar_dd_pick (rs->ddm,  DD (X, 0), varlist);
  if (DD (result, 0) == zero)
    {
      for (i++; i < width; i++)
	{
	  DD (result, i) = ar_dd_pick (rs->ddm, DD (X, i), varlist);
	  if (DD (result, i) != zero)
	    break;
	}
    }
  for (i++; i < width; i++)
    DD (result, i) = AR_DD_DUP (zero);

  return result;
}
			/* --------------- */

static ar_sgs_set * 
s_rs_assert_config (ar_sgs *sgs, ar_sgs_set *S)
{   
  ar_sgs_set *result = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  DD (result, 0) = AR_DD_DUP (DD (S, 0));
  s_apply_assertions (RS (sgs), &DD (result, 0), 0);

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_src (ar_sgs *sgs, ar_sgs_set *T)
{   
  return s_rs_src_or_tgt (RS (sgs), T, RS(sgs)->non_prime);
}
			/* --------------- */

static ar_sgs_set * 
s_rs_tgt (ar_sgs *sgs, ar_sgs_set *T)
{
  return s_rs_src_or_tgt (RS (sgs), T, RS(sgs)->to_non_prime);
}

			/* --------------- */

static ar_sgs_set * 
s_compute_rsrc_or_rtgt (ar_relsem *rs, ar_dd set, int rsrc)
{
  int i;
  ar_dd T;
  int width = rs->nb_events;
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_TRANS, width);

  s_compute_event_relations (rs);
  if (rsrc)
    T = AR_DD_DUP (set);
  else
    T = MAKE_IT_PRIME (rs, set);

  for (i = 0; i < width; i++)
    DD (R, i) = ar_dd_compute_and (rs->ddm, T, rs->event_relations[i]);

  AR_DD_FREE (T);

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_rsrc (ar_sgs *sgs, ar_sgs_set *S)
{
  return s_compute_rsrc_or_rtgt (RS (sgs), DD (S, 0), 1);
}

			/* --------------- */

static ar_sgs_set * 
s_rs_rtgt (ar_sgs *sgs, ar_sgs_set *S)
{
  return s_compute_rsrc_or_rtgt (RS (sgs), DD (S, 0), 0);
}

			/* --------------- */

static ar_sgs_set * 
s_rs_loop (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2)
{
  int i;
  ar_dd zero = ar_dd_zero (RS (sgs)->ddm);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);
  
  ccl_warning ("unable to compute loop () symbolically; "
	       "return an empty set.\n");

  for (i = 0; i < WIDTH (R); i++)
    DD (R, i) = AR_DD_DUP (zero);

  return R;
}

			/* --------------- */

/*!
 * Compute a trace from S1 to S2 using transitions in T.
 * R is the set of transitions that belong to the computed trace.
 * Z is the current set of reachable configurations
 *
 * The function returns an element of S1 that belong to the trace (if it 
 * exists).
 */
static ar_dd 
s_build_trace_rec (ar_relsem *rs, ar_dd S1, ar_sgs_set *T, ar_dd S2,
		   ar_sgs_set *R, ar_dd Z)
{
  ar_dd tmp[2];
  ar_dd result;

  /* we check if the goal S2 is reached. */
  result = ar_dd_compute_and (rs->ddm, S1, S2);

  if (ar_dd_is_zero (rs->ddm, result))
    {
      int i;
      ar_dd X;

      /* Now we check if we have reached the fixed point of reachable 
	 configurations */
      /* Z = Z U S1 */
      tmp[0] = ar_dd_compute_or (rs->ddm, Z, S1);
      if (Z == tmp[0])
	{
	  /* fix point is reached */
	  AR_DD_FREE (tmp[0]);
	  goto end;
	}
      else
	{
	  Z = tmp[0];
	}

      /* X = post (S1,T) \ Z */
      tmp[0] = s_apply (rs, S1, DD (T, 0), rs->nb_assertions, rs->assertions, 0);
      X = ar_dd_compute_diff (rs->ddm, tmp[0], Z);
      AR_DD_FREE (tmp[0]);

      for (i = 1; i < rs->nb_events; i++)
	{
	  tmp[0] = s_apply (rs, S1, DD (T, i), rs->nb_assertions, 
			    rs->assertions, 0);
	  tmp[1] = ar_dd_compute_diff (rs->ddm, tmp[0], Z);
	  AR_DD_FREE (tmp[0]);
	  tmp[0] = ar_dd_compute_or (rs->ddm, tmp[1], X);
	  AR_DD_FREE (tmp[1]);
	  AR_DD_FREE (X);
	  X = tmp[0];
	}

      /* Now we compute a trace from X to S2. */
      tmp[0] = s_build_trace_rec (rs, X, T, S2, R, Z);
      if (ar_dd_is_zero (rs->ddm, tmp[0]))
	AR_DD_FREE (tmp[0]);
      else
	{
	  ar_dd aux;

	  /* tmp[0] contains a element 's' of X that belongs to the trace */
	  /* Now we look for a predessor of tmp[0] in S1. */
	  
	  /* aux = S1 x { s } */
	  tmp[1] = MAKE_IT_PRIME (rs, tmp[0]);
	  AR_DD_FREE (tmp[0]);     
	  aux = ar_dd_compute_and (rs->ddm, S1, tmp[1]);
	  AR_DD_FREE (tmp[1]);     
	  
	  for (i = 0; i < rs->nb_events; i++)
	    {
	      tmp[0] = ar_dd_compute_and (rs->ddm, aux, DD (T, i));
	      if (! ar_dd_is_zero (rs->ddm, tmp[0]))
		{
		  tmp[1] = ar_dd_project_on_variables (rs->ddm, tmp[0], 
						       rs->non_prime);
		  AR_DD_FREE (tmp[0]);
		  tmp[0] = ar_dd_pick (rs->ddm, tmp[1], 
				       rs->not_primed_variables);
		  AR_DD_FREE (result);
		  result = tmp[0];

		  AR_DD_FREE (tmp[1]);
		  tmp[1] = ar_dd_compute_and (rs->ddm, aux, tmp[0]);
		  AR_DD_FREE (aux);
		  aux = ar_dd_compute_or (rs->ddm, DD (R, i), tmp[1]);
		  AR_DD_FREE (DD (R, i));
		  AR_DD_FREE (tmp[1]);
		  DD (R, i) = AR_DD_DUP (aux);
		  break;
		}
	      AR_DD_FREE (tmp[0]);
	    }
	  AR_DD_FREE (aux);
	}
      AR_DD_FREE (X);
      AR_DD_FREE (Z);
    }
  else
    {
      /* The goal S2 is reached. We return an element of S1 /\ S2. */
      tmp[0] = ar_dd_pick (rs->ddm, result, rs->not_primed_variables);
      AR_DD_FREE (result);
      result = tmp[0];
    }
 end:

  return result;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_trace (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, ar_sgs_set *S2)
{
  int i;
  ar_dd aux;
  ar_dd zero = ar_dd_zero (RS (sgs)->ddm);
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_TRANS, rs->nb_events);

  for (i = 0; i < rs->nb_events; i++)
    DD (R, i) = AR_DD_DUP (zero);

  aux = s_build_trace_rec (rs, DD (S1, 0), T, DD (S2, 0), R, zero)  ;
  AR_DD_FREE (aux);

  return R;  
}

			/* --------------- */

static ar_sgs_set * 
s_rs_unav (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S)
{
  int cont = 1;
  ar_sgs_set *tmp[2];
  ar_sgs_set *any_s = s_predef_ANY_S (sgs, NULL);
  ar_sgs_set *R = ar_sgs_set_add_reference (S);

  while (cont)
    {
      tmp[0] = ar_sgs_compute_difference (sgs, any_s, R);
      tmp[1] = ar_sgs_compute_rtgt (sgs, tmp[0]);
      ar_sgs_set_del_reference (tmp[0]);
      tmp[0] = ar_sgs_compute_intersection (sgs, tmp[1], T);
      ar_sgs_set_del_reference (tmp[1]);
      tmp[1] = ar_sgs_compute_src (sgs, tmp[0]);
      ar_sgs_set_del_reference (tmp[0]);
      tmp[0] = ar_sgs_compute_difference (sgs, any_s, tmp[1]);
      ar_sgs_set_del_reference (tmp[1]);
      tmp[1] = ar_sgs_compute_union (sgs, S, tmp[0]);
      ar_sgs_set_del_reference (tmp[0]);
      cont = (DD (R, 0) != DD (tmp[1], 0));
      ar_sgs_set_del_reference (R);
      R = tmp[1];
    }
  ar_sgs_set_del_reference (any_s);

  return R;
}

			/* --------------- */

static ar_dd
s_apply_trans_set (ar_relsem *rs, ar_dd X, relsem_set *T, int nb_I, ar_dd *I, 
		   int invapply)
{
  int i;
  ar_dd R = s_apply (rs, X, DD (T, 0), nb_I, I, invapply);
  
  for (i = 1; i < WIDTH (T); i++)
    {
      ar_dd tmp0 = s_apply (rs, X, DD (T, i), nb_I, I, invapply);
      ar_dd tmp1 = ar_dd_compute_or (rs->ddm, R, tmp0);
      AR_DD_FREE (R);
      AR_DD_FREE (tmp0);
      R = tmp1;
    }

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_reach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T)
{
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);
  int stop = 0;
  ar_dd Y;
  ar_dd X = AR_DD_DUP (DD (S, 0));

  s_apply_assertions (rs, &X, 0);
  while (!stop) 
    {
      ar_dd tmp = s_apply_trans_set (rs, X, RS_SET (T), rs->nb_assertions,
				     rs->assertions, 0);

      Y = ar_dd_compute_or (rs->ddm, X, tmp);
      AR_DD_FREE (tmp);
      stop = (X == Y);

      if (!stop)
	{
	  AR_DD_FREE (X);
	  X = Y;
	}
    }
  AR_DD_FREE (Y);
  DD (R, 0) = X;

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_coreach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T)
{
  int stop = 0;
  ar_dd Y;
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);
  ar_dd X = AR_DD_DUP (DD (S, 0));

  s_apply_assertions (rs, &X, 0);

  while (!stop)
    {
      ar_dd tmp0 = MAKE_IT_PRIME (rs, X);
      ar_dd tmp1 = 
	s_apply_trans_set (rs, tmp0, RS_SET (T), rs->nb_assertions,
			   rs->assertions, 1);
      AR_DD_FREE (tmp0);
      Y = ar_dd_compute_or (rs->ddm, X, tmp1);
      AR_DD_FREE (tmp1);
      stop = (X == Y);
      if (!stop)
	{
	  AR_DD_FREE (X);
	  X = Y;
	}
    }
  AR_DD_FREE (Y);
  DD (R, 0) = X;

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_state_set (ar_sgs *sgs, ar_ca_expr *cond)
{
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  DD (R, 0) = ar_ca_expr_compute_dd (cond, RS (sgs)->ddm, RS (sgs)->order, 
				     NULL);

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_label (ar_sgs *sgs, ar_identifier *label)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_ca_event **events = ar_ca_get_events (rs->super.ca);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);
  ar_dd zero = ar_dd_zero (rs->ddm);
  
  s_compute_event_relations (rs);
  for (i = 0; i < rs->nb_events; i++)
    {
      ar_dd set;

      if (ar_ca_event_contains (events[i], label))
	set = rs->event_relations[i];
      else
	set = zero;
      DD (R, i) = AR_DD_DUP (set);
    }

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_event_attribute (ar_sgs *sgs, ar_identifier *attribute)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_ca_event **events = ar_ca_get_events (rs->super.ca);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);
  ar_dd zero = ar_dd_zero (rs->ddm);

  s_compute_event_relations (rs);
  for (i = 0; i < rs->nb_events; i++)
    {
      ar_dd set;

      if (ar_ca_event_has_attribute (events[i], attribute))
	set = rs->event_relations[i];
      else
	set = zero;
      DD (R, i) = AR_DD_DUP (set);
    }

  return R;
}

			/* --------------- */

static ar_dd 
s_dd_for_number (ar_relsem *rs, int value, int bit_index, int start_index)
{
  ar_dd R;
  const int encoding = COUNTER_ENCODING;
  const int bitindex = (value & (1 << bit_index)) ? 1 : 0 ;
  const int otherindex = (bitindex + 1) & 0x1;
  ar_ddtable sons = ar_dd_allocate_dd_table (rs->ddm, 2, encoding);
  
  if (encoding == EXHAUSTIVE)
    {
      AR_DD_ENTHDD (sons, otherindex) = ar_dd_dup_zero (rs->ddm);
      if (bit_index == 0)
	AR_DD_ENTHDD (sons, bitindex) = ar_dd_dup_one (rs->ddm);
      else
	AR_DD_ENTHDD (sons, bitindex) = 
	  s_dd_for_number (rs, value, bit_index - 1, start_index + 1);
    }
  else
    {
      AR_DD_MIN_OFFSET_NTHDD (sons, otherindex) = otherindex;
      AR_DD_MAX_OFFSET_NTHDD (sons, otherindex) = otherindex;
      AR_DD_CNTHDD (sons, otherindex) = ar_dd_dup_zero (rs->ddm);

      AR_DD_MIN_OFFSET_NTHDD (sons, bitindex) = bitindex;
      AR_DD_MAX_OFFSET_NTHDD (sons, bitindex) = bitindex;

      if (bit_index == 0)
	AR_DD_CNTHDD (sons, bitindex) = ar_dd_dup_one (rs->ddm);
      else
	AR_DD_CNTHDD (sons, bitindex) = 
	  s_dd_for_number (rs, value, bit_index - 1, start_index + 1);
    }

  R = ar_dd_find_or_add (rs->ddm, start_index, 2, encoding, sons);
  ar_dd_free_dd_table (rs->ddm, 2, encoding, sons);

  return R;
}

			/* --------------- */

static ar_dd 
s_new_block_number (ar_relsem *rs, int *pcurrent, int nb_bits, int start_index)
{
  ar_dd result = s_dd_for_number (rs, *pcurrent, nb_bits - 1, start_index);
  (*pcurrent)++;

  return result;
}

			/* --------------- */

static void
s_refine_partition_with_set (ar_relsem *rs, ccl_list *P, ccl_list *tmp, 
			     ar_dd S)
{
  while (! ccl_list_is_empty (P))
    {
      ar_dd B = ccl_list_take_first (P);
      ar_dd B_and_S = ar_dd_compute_and (rs->ddm, S, B);

      if (ar_dd_is_zero (rs->ddm, B_and_S))
	{
	  ccl_list_add (tmp, AR_DD_DUP (B));
	  AR_DD_FREE (B_and_S); 
	}
      else
	{
	  ccl_list_add (tmp, B_and_S);
	  if (B_and_S != B)
	    {
	      ar_dd B_minus_S = 
		ar_dd_compute_and (rs->ddm, B, AR_DD_NOT (B_and_S));
	      ccl_list_add (tmp, B_minus_S);
	    }
	}
      AR_DD_FREE (B);
    }
}

			/* --------------- */

static ar_dd 
s_initial_partition (ar_relsem *rs, int nb_bits_for_cnt, int counter_index,
		     ccl_list *initial_sets)
{
  int nb_blocks;
  ar_dd result = ar_dd_dup_zero (rs->ddm);
  ccl_list *aux;
  ccl_list *tmp = ccl_list_create ();
  ccl_list *P = ccl_list_create ();
  ar_identifier_iterator *ii = ar_sgs_get_state_sets (SGS (rs));
  
  s_compute_reachables (rs);  
  ccl_list_add (P, AR_DD_DUP (rs->reachables));

  while (ccl_iterator_has_more_elements (ii))
    {
      ar_identifier *id = ccl_iterator_next_element (ii);
      ar_sgs_set *S = ar_sgs_get_state_set (SGS (rs), id);
      s_refine_partition_with_set (rs, P, tmp, DD (S, 0));
      aux = P;
      P = tmp;
      tmp = aux;

      ar_sgs_set_del_reference (S);
      ar_identifier_del_reference (id);
    }
  ccl_iterator_delete (ii);
  ccl_list_delete (tmp);
  
  nb_blocks = 0;
  while (! ccl_list_is_empty (P))
    {
      ar_dd tmp0 = ccl_list_take_first (P);
      ar_dd tmp1 = 
	s_new_block_number (rs, &nb_blocks, nb_bits_for_cnt, counter_index);
      ar_dd B_and_cnt = ar_dd_compute_and (rs->ddm, tmp0, tmp1);
      ccl_list_add (initial_sets, tmp0);
      AR_DD_FREE (tmp1);
      tmp0 = ar_dd_compute_or (rs->ddm, result, B_and_cnt);
      AR_DD_FREE (result);
      AR_DD_FREE (B_and_cnt);
      result = tmp0;
    }
  ccl_list_delete (P);


  if (ccl_debug_is_on)
    ccl_debug ("[QUOT] INITIAL PARTITION SIZE=%d\n", nb_blocks);

  return result;
}

			/* --------------- */

static int
s_nb_bits (int n)
{
  int i = (sizeof (n) * 8 - 2);
  int f = 1 << i;

  ccl_assert (n > 0);

  while (f != 0 && (f&n) == 0)
    {
      f >>= 1;
      i++;
    }
  ccl_assert (f != 0);
  return i;
}

static ar_dd *
s_make_dd_for_events (ar_relsem *rs, int eindex, int *p_nb_bits)
{
  int i;
  ar_dd *result = ccl_new_array (ar_dd, rs->nb_events);
  int nb_bits = s_nb_bits (rs->nb_events);

  for (i = 0; i < rs->nb_events; i++)
    result[i] = s_dd_for_number (rs, i, nb_bits - 1, eindex);

  *p_nb_bits = nb_bits;

  return result;
}

			/* --------------- */

static ar_dd
s_compute_strong_bisim_signature (ar_relsem *rs, ar_dd P, ar_dd *events,
				  ar_dd to_prime, ar_dd non_prime)
{
  ar_dd result = ar_dd_dup_zero (rs->ddm);

  s_compute_event_relations (rs);

  DBG_START_TIMER (("compute signature"));
  {
    int i;
    ar_dd aux;
    ar_dd Pprime = ar_dd_project_on_variables (rs->ddm, P, to_prime);
    
    for (i = rs->nb_events - 1; i >= 0; i--)
      {
	ar_dd B;
	ar_dd a_rel = 
	  ar_dd_compute_and (rs->ddm, rs->event_relations[i], Pprime);
	ar_dd tmp = ar_dd_project_on_variables (rs->ddm, a_rel, non_prime);

	AR_DD_FREE (a_rel);

	B = ar_dd_compute_and (rs->ddm, tmp, events[i]);
	AR_DD_FREE (tmp);

	tmp = ar_dd_compute_or (rs->ddm, B, result);
	AR_DD_FREE (result);
	AR_DD_FREE (B);
	result = tmp;
      }
    AR_DD_FREE (Pprime);

    aux = ar_dd_compute_and (rs->ddm, result, rs->reachables);
    AR_DD_FREE (result);
    result = aux;

    if (ccl_debug_is_on)
      ccl_debug ("size (P) = %ld nodes\n", 
		 ar_dd_get_number_of_nodes (result));

  }
  DBG_END_TIMER ();

  return result;
}

			/* --------------- */

static ar_dd
s_refine_partition_rec (ar_relsem *rs, ar_dd Sig, 
			int *p_nb_blocks, int nb_bits, int icounter, 
			ccl_hash *cache)
{
  int index;
  ar_dd result;

  ccl_pre (! ar_dd_is_one (rs->ddm, Sig));

  if (ar_dd_is_zero (rs->ddm, Sig))
    return AR_DD_DUP (Sig);

  index = AR_DD_INDEX (Sig);
  if (index > rs->max_dd_index)
    {
      if (ccl_hash_find (cache, Sig))
	result = ccl_hash_get (cache);
      else
	{
	  result = s_new_block_number (rs, p_nb_blocks, nb_bits, icounter);
	  AR_DD_DUP (Sig);
	  ccl_hash_insert (cache, result);
	}
      result = AR_DD_DUP (result);
    }
  else if ((result = 
	    ar_dd_entry_for_operation (rs->ddm, 
				       (void *) s_refine_partition_rec, 
				       Sig, NULL)) != NULL)
    return AR_DD_DUP (result);
  else
    {
      int i;
      int arity = AR_DD_ARITY (Sig);
      ar_ddcode encoding = AR_DD_CODE (Sig);
      ar_ddtable sons = ar_dd_allocate_dd_table (rs->ddm, arity, encoding);

      if (encoding == EXHAUSTIVE)
	{
	  for (i = 0; i < arity; i++)
	    AR_DD_ENTHDD (sons, i) = 
	      s_refine_partition_rec (rs, AR_DD_EPNTHSON (Sig, i), p_nb_blocks, 
				      nb_bits, icounter, cache);
	}
      else
	{
	  for (i = 0; i < arity; i++)
	    {
	      AR_DD_MIN_OFFSET_NTHDD (sons, i) = 
		AR_DD_MIN_OFFSET_NTHSON (Sig, i);
	      AR_DD_MAX_OFFSET_NTHDD (sons, i) = 
		AR_DD_MAX_OFFSET_NTHSON (Sig, i);
	      AR_DD_CNTHDD (sons, i) = 
	      s_refine_partition_rec (rs, AR_DD_CPNTHSON (Sig, i), p_nb_blocks, 
				      nb_bits, icounter, cache);
	    }
	}
      result = ar_dd_find_or_add (rs->ddm, index, arity, encoding, sons);
      ar_dd_free_dd_table (rs->ddm, arity, encoding, sons);
    }


  return result;
}

			/* --------------- */

static void
s_delete_dd (void *F)
{
  AR_DD_FREE (F);
}

			/* --------------- */

static ar_dd
s_refine_partition (ar_relsem *rs, ar_dd Sig, int nb_bits, int icounter, 
		    int *p_nb_blocks, ccl_list *isets)
{
  ar_dd result = ar_dd_dup_zero (rs->ddm);

  DBG_START_TIMER (("refine partition"));
  {
    ccl_pair *p;

    *p_nb_blocks = 0;

    for (p = FIRST (isets); p; p = CDR (p))
      {
	ccl_hash *cache = 
	  ccl_hash_create (NULL, NULL, s_delete_dd, s_delete_dd);
	ar_dd tmp0 = ar_dd_compute_and (rs->ddm, CAR (p), Sig);
	ar_dd tmp1 = s_refine_partition_rec (rs, tmp0, p_nb_blocks, nb_bits, 
					     icounter, cache);
	AR_DD_FREE (tmp0);
	tmp0 = ar_dd_compute_or (rs->ddm, result, tmp1);
	AR_DD_FREE (tmp1);
	AR_DD_FREE (result);
	result = tmp0;
	ccl_hash_delete (cache);
      }

    ccl_assert (*p_nb_blocks > 0);

    if (ccl_debug_is_on)
      ccl_debug ("[QUOT] NEW PARTITION SIZE=%d\n", *p_nb_blocks);
  }
  ar_dd_remove_foreign_memoization_records (rs->ddm);
  DBG_END_TIMER ();

  return result;
}

			/* --------------- */

static ar_dd
s_remove_counter_dd (ar_relsem *rs, ar_dd P)
{
  return ar_dd_project_on_variables (rs->ddm, P, rs->non_prime);
}

			/* --------------- */

static ccl_list *
s_rs_classes (ar_sgs *sgs)
{
  ccl_list *result = ccl_list_create ();

  DBG_START_TIMER (("compute bisimulation contraction"));
  {
    int i;
    int nb_blocks;
    ar_relsem *rs = RS (sgs);
    int ievent = rs->max_dd_index + 1;
    int nb_bits_for_events = 0;
    ar_dd to_prime = AR_DD_DUP (rs->to_prime);
    ar_dd non_prime = AR_DD_DUP (rs->non_prime);
    ar_dd *events = s_make_dd_for_events (rs, ievent, &nb_bits_for_events);
    int icounter = ievent + nb_bits_for_events;
    int nb_bits = COUNTER_BITS;
    ccl_list *isets = ccl_list_create ();
    ar_dd P = s_initial_partition (rs, nb_bits, icounter, isets);  
    int stop = ar_dd_is_zero (rs->ddm, P);

    for (i = ievent; i < icounter + nb_bits; i++)
      {
	ar_dd_projection_list_add (rs->ddm, &to_prime, i, i);
	ar_dd_projection_list_add (rs->ddm, &non_prime, i, i);
      }

    nb_bits <<= 1;
    while (!stop)
      {
	ar_dd Sig = s_compute_strong_bisim_signature (rs, P, events, to_prime,
						      non_prime);
	ar_dd Pp = s_refine_partition (rs, Sig, nb_bits, icounter, &nb_blocks,
				       isets); 

	AR_DD_FREE (Sig);
	AR_DD_FREE (P);
	stop = (Pp == P);
	P = Pp;
      }
    
    ccl_list_clear_and_delete (isets, s_delete_dd);
    for (i = 0; i < rs->nb_events; i++)
      AR_DD_FREE (events[i]);
    ccl_delete (events);

    DBG_START_TIMER (("separation of classes"));
    for (i = 0; i < nb_blocks; i++)
      {
	ar_dd n = s_dd_for_number (rs, i, nb_bits - 1, icounter);
	ar_dd tmp = ar_dd_compute_and (rs->ddm, P, n);
	ar_dd Cdd = s_remove_counter_dd (rs, tmp);
	ar_sgs_set *C = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);
	DD (C, 0) = Cdd;
	ccl_list_add (result, C);
	AR_DD_FREE (n);
	AR_DD_FREE (tmp);
      }
    DBG_END_TIMER ();
    AR_DD_FREE (P);
    AR_DD_FREE (to_prime);
    AR_DD_FREE (non_prime);
  }
  DBG_END_TIMER ();


  return result;  
}

			/* --------------- */

static ar_sgs_set *
s_create_X_set (ar_relsem *rs, int is_state, int is_full, int width)
{
  int i;
  ar_sgs_set *R = s_relsem_set_create (is_state, width);
  
  DD (R, 0) = is_full ? ar_dd_one (rs->ddm) : ar_dd_zero (rs->ddm);
  DD (R, 0) = AR_DD_DUP (DD (R, 0));

  for (i = 1; i < width; i++)
    DD (R, i) = AR_DD_DUP (DD (R,0));

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_create_empty_set (ar_relsem *rs, int is_state)
{
  return s_create_X_set (rs, is_state, 0, is_state ? 1 : rs->nb_events);
}

			/* --------------- */

static ar_sgs_set *
s_create_full_set (ar_relsem *rs, int is_state)
{
  return s_create_X_set (rs, is_state, 1, is_state ? 1 : rs->nb_events);
}

			/* --------------- */

static ar_sgs_set *
s_compute_fixpoint (ar_sgs *sgs, ar_identifier *var, int lfp, 
		    ar_sgs_formula *F)
{
  int is_state = ar_sgs_formula_is_state_formula (F);
  int stop = 0;
  ar_sgs_set *S;
  ar_sgs_set *S0;

  if (lfp)
    S0 = s_create_empty_set (RS (sgs), is_state);
  else 
    S0 = s_create_full_set (RS (sgs), is_state);
  
  while (!stop)
    {
      if (is_state) 
	ar_sgs_add_state_set (sgs, var, S0);
      else          
	ar_sgs_add_trans_set (sgs, var, S0);

      S = ar_sgs_formula_compute_constant_formula (F, sgs);
      stop = s_set_are_equal (RS_SET (S0), RS_SET (S));
      if (! stop)
	{
	  ar_sgs_set_del_reference (S0);
	  S0 = S;
	}
    }
  ar_sgs_set_del_reference (S);

  return S0;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_lfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F)
{
  return s_compute_fixpoint (sgs, var, 1, F);
}

			/* --------------- */

static ar_sgs_set * 
s_rs_gfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F)
{
  return s_compute_fixpoint (sgs, var, 0, F);
}

			/* --------------- */

static ar_sgs_set * 
s_rs_set_complement (ar_sgs *sgs, ar_sgs_set *S)
{ 
  int i;
  int width = WIDTH (S);
  ar_sgs_set *R = s_relsem_set_create (ar_sgs_set_is_state_set (S), width);

  DD (R, 0) = AR_DD_DUP_NOT (DD (S, 0));
  for (i = 1; i < width; i++)
    DD (R, i) = AR_DD_DUP_NOT (DD (S, i));

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_set_union (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  int i;
  int width = WIDTH (S1);
  ar_sgs_set *R = s_relsem_set_create (ar_sgs_set_is_state_set (S1), width);

  DD (R, 0) = ar_dd_compute_or (RS (sgs)->ddm, DD (S1, 0), DD (S2, 0));
  for (i = 1; i < width; i++)
    DD (R, i) = ar_dd_compute_or (RS (sgs)->ddm, DD (S1, i), DD (S2, i));

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_set_intersection (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  int i;
  int width = WIDTH (S1);
  ar_sgs_set *R = s_relsem_set_create (ar_sgs_set_is_state_set (S1), width);

  DD (R, 0) = ar_dd_compute_and (RS (sgs)->ddm, DD (S1, 0), DD (S2, 0));
  for (i = 1; i < width; i++)
    DD (R, i) = ar_dd_compute_and (RS (sgs)->ddm, DD (S1, i), DD (S2, i));

  return R;
}

			/* --------------- */

static ar_sgs_set * 
s_rs_set_difference (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  ar_sgs_set *nS2 = s_rs_set_complement (sgs, S2);
  ar_sgs_set *R = s_rs_set_intersection (sgs, S1, nS2);
  ar_sgs_set_del_reference (nS2);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_relsem_set_create (int is_state, int width)
{
  size_t size = sizeof (relsem_set);
  relsem_set *result = (relsem_set *) 
    ar_sgs_set_create (is_state, size, &RS_SET_METHODS);

  ccl_assert (! is_state || width == 1);
  ccl_array_init_with_size (result->sets, width);

  return SGS_SET (result);
}

			/* --------------- */

static void
s_rs_set_destroy (ar_sgs_set *set)
{
  AR_DD_DELETE_ARRAY (SETS (set));
}

			/* --------------- */

static ar_sgs_set *
s_predef_ANY_C (ar_sgs *sgs, const ar_identifier *id)
{
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  s_compute_assertion (rs);
  DD (R, 0) = AR_DD_DUP (rs->assertion);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_ANY_S (ar_sgs *sgs, const ar_identifier *id)
{
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  s_compute_reachables (rs);

  DD (R, 0) = AR_DD_DUP (rs->reachables);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_EMPTY_S (ar_sgs *sgs, const ar_identifier *id)
{
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  DD (R, 0) = ar_dd_dup_zero (rs->ddm);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_INITIAL_STATES (ar_sgs *sgs, const ar_identifier *id)
{
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  s_compute_initial (rs);

  DD (R, 0) = AR_DD_DUP (rs->initial);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_VALID_STATE_ASSIGNMENTS (ar_sgs *sgs, const ar_identifier *id)
{
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = s_relsem_set_create (AR_SGS_SET_IS_STATE, 1);

  s_compute_assertion (rs);
  DD (R, 0) = ar_dd_project_on_variables (rs->ddm, rs->assertion, 
					  rs->state_variables);

  return R;
}

			/* --------------- */

static void
s_compute_post_reachables (ar_relsem *rs)
{
  if (rs->post_reachables != NULL)
    return;

  s_compute_reachables (rs);
  rs->post_reachables = MAKE_IT_PRIME (rs, rs->reachables);
}

			/* --------------- */

static ar_sgs_set *
s_predef_ANY_T (ar_sgs *sgs, const ar_identifier *id)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);
  ar_ca_event **events = ar_ca_get_events (rs->super.ca);

  s_compute_reachables (rs);
  s_compute_post_assertions (rs);
  s_compute_event_relations (rs);

  for (i = 0; i < rs->nb_events; i++)
    {
      if (ccl_debug_is_on)
	{
	  ccl_debug ("compute any_t/");
	  ar_ca_event_log (CCL_LOG_DEBUG, events[i]);
	  ccl_debug ("\n");
	}

      DD (R, i) = ar_dd_compute_and (rs->ddm, rs->reachables, 
				     rs->event_relations[i]);

      s_apply_assertions (rs, &DD (R, i), 1);
    }

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_EMPTY_T (ar_sgs *sgs, const ar_identifier *id)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);

  for (i = 0; i < rs->nb_events; i++)
    DD (R, i) = ar_dd_dup_zero (rs->ddm);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_ANY_TRANS (ar_sgs *sgs, const ar_identifier *id)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);

  s_compute_event_relations (rs);
  for (i = 0; i < rs->nb_events; i++)
    DD (R, i) = AR_DD_DUP (rs->event_relations[i]);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_EPSILON (ar_sgs *sgs, const ar_identifier *id)
{
  int i;  
  ar_relsem *rs = RS (sgs);
  ar_dd zero = ar_dd_zero (rs->ddm);
  ar_ca_event **events = ar_ca_get_events (rs->super.ca);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);

  s_compute_event_relations (rs);
  s_compute_post_reachables (rs);
  for (i = 0; i < rs->nb_events; i++)
    {
      if (ar_ca_event_is_epsilon(events[i]))
	{
	  DD (R, i) = 
	    ar_dd_compute_and (rs->ddm, rs->reachables, rs->event_relations[i]);
	  s_apply_assertions (rs, &DD (R, i), 1);
	}
      else
	DD (R, i) = AR_DD_DUP (zero);
    }

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_predef_SELF (ar_sgs *sgs, const ar_identifier *id)
{
  int i;
  ar_dd loop;
  ar_relsem *rs = RS (sgs);
  ar_ca_expr *c = ar_ca_expr_crt_boolean_constant(rs->eman, 1);
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_state_variables(rs->super.ca);
  ar_sgs_set *R = 
    s_relsem_set_create (AR_SGS_SET_IS_TRANS, RS (sgs)->nb_events);

  s_compute_event_relations (rs);

  for (i = 0; i < 2; i++)
    {
      CCL_LIST_FOREACH (p, vars)
	{
	  ar_ca_expr *tmp0 = s_get_prime_variable (rs, CAR (p));
	  ar_ca_expr *tmp1 = ar_ca_expr_crt_eq (CAR (p), tmp0);
	  ar_ca_expr_del_reference (tmp0);
	  tmp0 = ar_ca_expr_crt_and (c, tmp1);
	  ar_ca_expr_del_reference (c);
	  ar_ca_expr_del_reference (tmp1);
	  c = tmp0;
	}
      vars = ar_ca_get_flow_variables(rs->super.ca);      
    }

  s_compute_post_reachables (rs);
  loop = ar_ca_expr_compute_dd (c, rs->ddm, rs->order, NULL);
  ar_ca_expr_del_reference (c);

  for (i = 0; i < WIDTH (R); i++)
    {
      ar_dd tmp0 = ar_dd_compute_and (rs->ddm, loop, rs->event_relations[i]);
      ar_dd tmp1 = ar_dd_compute_and (rs->ddm, rs->reachables, tmp0);
      AR_DD_FREE (tmp0);
      DD (R, i) = ar_dd_compute_and (rs->ddm, rs->post_reachables, tmp1);
      AR_DD_FREE (tmp1);
    }
  AR_DD_FREE (loop);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_get_set (ar_sgs *sgs, enum ar_acheck_builtin id,
	   ar_sgs_set * (*get_it) (ar_sgs *, ar_identifier *))
{
  const char *sid = AR_ACHECK_BUILTIN_NAMES[id];
  ar_identifier *ident = ar_identifier_create (sid);
  ar_sgs_set *R = get_it (sgs, ident);
  ar_identifier_del_reference (ident);

  return R;
}

			/* --------------- */

static ar_sgs_set *
s_get_trans_set (ar_sgs *sgs, enum ar_acheck_builtin id)
{
  return s_get_set (sgs, id, ar_sgs_get_trans_set);
}

			/* --------------- */

static ar_sgs_set *
s_get_state_set (ar_sgs *sgs, enum ar_acheck_builtin id)
{
  return s_get_set (sgs, id, ar_sgs_get_state_set);
}

			/* --------------- */

static ar_sgs_set *
s_predef_SELF_EPSILON (ar_sgs *sgs, const ar_identifier *id)
{
  ar_sgs_set *self = s_get_trans_set (sgs, AR_ACHECK_SELF);
  ar_sgs_set *epsilon = s_get_trans_set (sgs, AR_ACHECK_EPSILON);
  ar_sgs_set *result = s_rs_set_intersection (sgs, self, epsilon);
  ar_sgs_set_del_reference (self);
  ar_sgs_set_del_reference (epsilon);

  return result;
}

			/* --------------- */

static ar_sgs_set *
s_predef_VALID_STATE_CHANGES (ar_sgs *sgs, const ar_identifier *id)
{
  int i;
  ar_relsem *rs = RS (sgs);
  ar_sgs_set *T = s_predef_ANY_TRANS (sgs, NULL);
  ar_sgs_set *VS = s_get_state_set (sgs, AR_ACHECK_VALID_STATE_ASSIGNMENTS);
  ar_dd VSP = MAKE_IT_PRIME (rs, DD (VS, 0));
  
  for (i = 0; i < rs->nb_events; i++)
    {
      ar_dd tmp;

      s_apply_assertions (rs, &DD (T, i), 0);
      tmp = ar_dd_project_on_variables (rs->ddm, DD (T, i), 
					rs->state_variables);
      AR_DD_FREE (DD (T, i));
      DD (T, i) = ar_dd_compute_and (rs->ddm, tmp, VSP);
      AR_DD_FREE (tmp);
    }
  AR_DD_FREE (VSP);
  ar_sgs_set_del_reference (VS);

  return T;
}

void
ar_relsem_log_variables_of_dd (ccl_log_type log, ar_relsem *rs, ar_dd R)
{
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  const varorder *order = ar_relsem_get_order (rs);
  ar_dd list = ar_dd_get_variables (ddm, R);

  if (! ar_dd_is_zero (ddm, list))
    {
      while (! ar_dd_is_one (ddm, list))
	{
	  int index = AR_DD_INDEX (list);
	  ar_dd tmp = ar_dd_projection_list_get_tail (list);
	  tmp = AR_DD_DUP (tmp);
	  AR_DD_FREE (list);
	  list = tmp;
	  ar_ca_expr_log (log, 
			  varorder_get_variable_from_dd_index (order, index));
	  ccl_log (log, "\n");
	}
    }
  AR_DD_FREE (list);  
}

ccl_hash *
ar_relsem_variables_of_dd (ar_relsem *rs, ar_dd R)
{
  ar_ddm ddm = ar_relsem_get_decision_diagrams_manager (rs);
  const varorder *order = ar_relsem_get_order (rs);
  ccl_hash *result = ccl_hash_create (NULL, NULL, NULL, NULL);
  ar_dd vars = ar_dd_get_variables (ddm, R);

  if (! ar_dd_is_zero (ddm, vars))
    {
      while (! ar_dd_is_one (ddm, vars))
	{
	  int index = AR_DD_INDEX (vars);
	  ar_ca_expr *var = varorder_get_variable_from_dd_index (order, index);
	  ar_dd tmp = ar_dd_projection_list_get_tail (vars);
	  ccl_hash_find (result, var);
	  ccl_hash_insert (result, var);
	  AR_DD_FREE (vars);
	  vars = AR_DD_DUP (tmp);
	}
    }

  AR_DD_FREE (vars);

  return result;
}

ar_dd 
ar_relsem_make_it_prime (ar_relsem *rs, ar_dd R)
{
  return MAKE_IT_PRIME(rs, R);
}

ar_dd 
ar_relsem_pick (ar_relsem *rs, ar_dd S)
{
  return ar_dd_pick (rs->ddm, S, rs->not_primed_variables);
}

static ar_dd
s_mec5_dd_to_relsem_dd (ar_ddm ddm, ar_dd F, uintptr_t rl, 
			ccl_hash *relabelling)
{
  ar_dd R;

  if (ar_dd_is_zero (ddm, F) || ar_dd_is_one (ddm, F))
    return AR_DD_DUP (F);

  R = ar_dd_entry_for_operation (ddm, (void *) s_mec5_dd_to_relsem_dd, 
				 F, (void *) rl);
  if (R != NULL)
    R = AR_DD_DUP (R);
  else    
    {
      int i;
      int arity = AR_DD_ARITY (F);
      int index = AR_DD_INDEX (F);
      ar_ddcode encoding = AR_DD_CODE (F);
      int card = (encoding == EXHAUSTIVE 
		  ? arity 
		  : (AR_DD_MAX_OFFSET_NTHSON (F, arity - 1) + 1));
      ccl_list *labels;
      ar_dd tmp[3];
      ar_dd one = ar_dd_one (ddm);
      ar_dd zero = ar_dd_zero (ddm);

      ccl_pre (ccl_hash_find (relabelling, (void *) (uintptr_t) index));

      ccl_hash_find (relabelling, (void *) (uintptr_t) index);
      labels = ccl_hash_get (relabelling);

      R = AR_DD_DUP (zero);

      for (i = 0; i < arity && R != one; i++)
	{
	  int j;
	  ccl_pair *p;
	  ar_dd relabel = AR_DD_DUP (zero);
	  ar_dd son;
	  int min, max;
	  
	  if (encoding == EXHAUSTIVE)
	    {
	      min = max = i;
	      son = AR_DD_EPNTHSON (F, i);
	    }
	  else
	    {
	      min = AR_DD_MIN_OFFSET_NTHSON (F, i);
	      max = AR_DD_MAX_OFFSET_NTHSON (F, i);
	      son = AR_DD_CPNTHSON (F, i);
	    }

	  for (j = min; j <= max; j++)
	    {
	      tmp[2] = AR_DD_DUP (one);
	      for (p = FIRST (labels); p && tmp[2] != zero; p = CDR (p))
		{
		  int l = (uintptr_t) CAR (p);
		  
		  tmp[0] = ar_dd_var_in_range (ddm, l, card, encoding, j, j);
		  tmp[1] = ar_dd_compute_and (ddm, tmp[2], tmp[0]);
		  AR_DD_FREE (tmp[0]);
		  AR_DD_FREE (tmp[2]);
		  tmp[2] = tmp[1];
		}
	      tmp[0] = ar_dd_compute_or (ddm, relabel, tmp[2]);
	      AR_DD_FREE (relabel);
	      AR_DD_FREE (tmp[2]);
	      relabel = tmp[0];
	    }

	  if (relabel != zero)
	    {
	      tmp[0] = s_mec5_dd_to_relsem_dd (ddm, son, rl, relabelling);
	      tmp[1] = ar_dd_compute_and (ddm, relabel, tmp[0]);
	      AR_DD_FREE (tmp[0]);
	      AR_DD_FREE (relabel);
	      relabel = tmp[1];
	      tmp[0] = ar_dd_compute_or (ddm, R, relabel);
	      AR_DD_FREE (R);
	      R = tmp[0];
	    }
	  AR_DD_FREE (relabel);
	}

      ar_dd_memoize_operation (ddm, (void *) s_mec5_dd_to_relsem_dd, F, 
			       (void *) rl, R);
    }

  return R;
}

ar_sgs_set *
ar_relsem_translate_configuration_relation (ar_relsem *rs,
					    ar_mec5_relation *mrel)
{
  int i;
  ar_dd R;
  int arity = ar_mec5_relation_get_arity (mrel);
  int *colindexes = ccl_new_array (int, arity);
  ar_sgs *sgs = SGS (rs);
  ar_sgs_set *result;
  ar_dd set = ar_mec5_relation_get_dd (mrel);
  ccl_hash *relabelling = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *) ccl_list_delete);
  static uintptr_t rl = 0;
  
  ar_mec5_relation_get_ordered_columns (mrel, colindexes);
  rl++;
  for (i = 0; i < arity; i++)
    {
      int col = colindexes[i];
      ar_identifier *varname = ar_mec5_relation_get_ith_arg_name (mrel, col);
      ar_identifier *cname = NULL;
      ar_identifier *tmp = ar_identifier_get_first (varname, &cname);
      intptr_t index = ar_mec5_relation_get_ith_dd_index (mrel, col);
      int newindex = ar_relsem_get_var_dd_index ((ar_relsem *) sgs, cname);
      ccl_list *newlabels;

      if (ccl_debug_is_on)
	{
	  ccl_debug ("%d -> ", i);
	  ar_identifier_log (CCL_LOG_DEBUG, varname);
	  ccl_debug (" -> ");
	  ar_identifier_log (CCL_LOG_DEBUG, cname);
	  ccl_debug (" -> %d -> %d\n", index, newindex);
	}

      ar_identifier_del_reference (cname);
      ar_identifier_del_reference (varname);
      ar_identifier_del_reference (tmp);

      if (ccl_hash_find (relabelling, (void *) index))
	newlabels = ccl_hash_get (relabelling);
      else
	{
	  newlabels = ccl_list_create ();
	  ccl_hash_insert (relabelling, newlabels);
	}

      if (! ccl_list_has (newlabels, (void *) (uintptr_t) newindex))
	ccl_list_add (newlabels, (void *) (uintptr_t) newindex);
    }

  ccl_delete (colindexes);
  R = s_mec5_dd_to_relsem_dd (MEC5_DDM, set, rl, relabelling);
  
  
  AR_DD_FREE (set);
  ccl_hash_delete (relabelling);

  result = ar_relsem_create_state_set ((ar_relsem *) sgs, R);
  AR_DD_FREE (R);


  return result;
}

static uintptr_t rl = 0;

static ccl_hash *
s_build_relabelling_for_trans (ar_relsem *rs, ar_mec5_relation *mrel)
{
  int i;
  int nb_event_vars = ar_idtable_get_size (rs->event_indexes);
  int nb_vars = (ar_ca_get_number_of_state_variables (rs->super.ca) +
		 ar_ca_get_number_of_flow_variables (rs->super.ca));
  int arity = ar_mec5_relation_get_arity (mrel);
  ccl_hash *relabelling = 
    ccl_hash_create (NULL, NULL, NULL, (ccl_delete_proc *) ccl_list_delete);

  for (i = 0; i < arity; i++)
    {      
      int newindex;
      ccl_list *newlabels;
      intptr_t index = ar_mec5_relation_get_ith_dd_index (mrel, i);
      ar_identifier *varname = ar_mec5_relation_get_ith_arg_name (mrel, i);
      ar_identifier *cname = NULL;
      ar_identifier *tmp = ar_identifier_get_first (varname, &cname);     

      ccl_assert (index >= 0);
      
      if (0 <= i && i < nb_vars)
	newindex = ar_relsem_get_var_dd_index (rs, cname);
      else if (nb_vars <= i && i < nb_vars + nb_event_vars)
	newindex = ar_relsem_get_event_var_dd_index (rs, cname);
      else 
	{
	  ccl_assert (i < 2 * nb_vars + nb_event_vars);
	  newindex = ar_relsem_get_var_dd_index (rs, cname);
	  newindex = ar_relsem_get_primed_dd_index (rs, newindex);
	}
      
      ccl_assert (newindex >= 0);
      
      if (ccl_debug_is_on)
	{
	  ccl_debug ("%d -> ", i);
	  ar_identifier_log (CCL_LOG_DEBUG, varname);
	  ccl_debug (" -> ");
	  ar_identifier_log (CCL_LOG_DEBUG, cname);
	  ccl_debug (" -> %d -> %d\n", index, newindex);
	}

      ar_identifier_del_reference (cname);
      ar_identifier_del_reference (varname);
      ar_identifier_del_reference (tmp);

      if (ccl_hash_find (relabelling, (void *) index))
	newlabels = ccl_hash_get (relabelling);
      else
	{
	  newlabels = ccl_list_create ();
	  ccl_hash_insert (relabelling, newlabels);
	}

      if (! ccl_list_has (newlabels, (void *) (uintptr_t) newindex))
	ccl_list_add (newlabels, (void *) (uintptr_t) newindex);

    }
  
  return relabelling;
}

ar_sgs_set *
ar_relsem_translate_transition_relation (ar_relsem *rs, ar_mec5_relation *mrel)
{
  int i;
  ar_dd rel, aux;
  ar_sgs_set *result = s_relsem_set_create (AR_SGS_SET_IS_TRANS, rs->nb_events);
  ccl_hash *relabelling;
  
  ccl_pre (rs != NULL);
  ccl_pre (mrel != NULL);
  
  s_compute_event_dds (rs);
  aux = ar_mec5_relation_get_dd (mrel);
  relabelling = s_build_relabelling_for_trans (rs, mrel);
  rel = s_mec5_dd_to_relsem_dd (MEC5_DDM, aux, rl++, relabelling);
  AR_DD_FREE (aux);
  ccl_hash_delete (relabelling);
  
  for (i = 0; i < rs->nb_events; i++)
    {
      ar_dd tmp = ar_dd_compute_and (rs->ddm, rs->event_dds[i], rel);
      DD (result, i) = ar_dd_project_variables (rs->ddm, tmp, rs->event_pl);
      AR_DD_FREE (tmp);
    }
  AR_DD_FREE (rel);
  
  return result;
}

