/*
 * ts-atoms.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ts-p.h"
#include "ts-algorithms.h"

			/* --------------- */

ar_ts_mark *
ar_ts_compute_src(ar_ts *ts, ar_ts_mark *mark)
{
  int i;
  int s; 
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( mark != NULL );

  result = ar_ts_mark_create(AR_SGS_SET_IS_STATE, ts->nb_states);

  i = ar_ts_mark_get_first(mark);
  while( i >= 0 )
    {
      s = ts->trans_table.data[i].src->index;
      ar_ts_mark_add(result,s);
      i = ar_ts_mark_get_next(mark,i);
    }

  return result;
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_rsrc(ar_ts *ts, ar_ts_mark *mark)
{
  int i; 
  int t; 
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( mark != NULL );

  result = ar_ts_mark_create(AR_SGS_SET_IS_TRANS, ts->nb_trans);

  i = ar_ts_mark_get_first(mark);
  while( i >= 0 )
    {
      t = ts->state_table.data[i]->first_out;
      while( t >= 0 )
	{
	  ar_ts_mark_add(result,t);
	  t = ts->trans_table.data[t].next_out;
	}
      i = ar_ts_mark_get_next(mark,i);
    }

  return result;
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_tgt(ar_ts *ts, ar_ts_mark *mark)
{
  int          i;
  int          s; 
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( mark != NULL );
  result = ar_ts_mark_create(AR_SGS_SET_IS_STATE, ts->nb_states);

  i = ar_ts_mark_get_first(mark);
  while( i >= 0 )
    {
      s = ts->trans_table.data[i].tgt->index;
      ar_ts_mark_add(result,s);
      i = ar_ts_mark_get_next(mark,i);
    }

  return result;
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_rtgt(ar_ts *ts, ar_ts_mark *mark)
{
  int          i; 
  int          t; 
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( mark != NULL );
  result = ar_ts_mark_create(AR_SGS_SET_IS_TRANS, ts->nb_trans);

  i = ar_ts_mark_get_first(mark);
  while( i >= 0 )
    {
      t = ts->state_table.data[i]->first_in;
      while( t >= 0 )
	{
	  ar_ts_mark_add(result,t);
	  t = ts->trans_table.data[t].next_in;
	}
      i = ar_ts_mark_get_next(mark,i);
    }

  return result;
}

			/* --------------- */

int
ar_ts_eval_expr_in_state (ar_ts *ts, ar_ca_expr *expr, const ts_state *st)
{
  int j;

  ccl_pre (ts != NULL);
  ccl_pre (expr != NULL);
  ccl_pre (st != NULL);

  for (j = 0; j < ts->variables.size; j++)
    {
      ar_bounds *b = ar_ca_expr_get_bounds_address (ts->variables.data[j]);
      int v = st->body[j] + ts->min_values[j];
      ar_bounds_set (b, v, v);
    }

  ar_ca_expr_evaluate (expr, 1);
  ccl_assert (ar_ca_expr_get_min (expr) == ar_ca_expr_get_max (expr));
  
  return ar_ca_expr_get_min (expr);
}

ar_ts_mark *
ar_ts_compute_state_set(ar_ts *ts, ar_ca_expr *cond)
{
  int i;
  ar_ts_mark *result;
  ts_state *S;

  ccl_pre( ts != NULL ); ccl_pre( cond != NULL );

  result = ar_ts_mark_create(AR_SGS_SET_IS_STATE, ts->nb_states);
  for(i = 0; i < ts->nb_states; i++)
    {
      S = ts->state_table.data[i];
      if (ar_ts_eval_expr_in_state (ts, cond, S))
	ar_ts_mark_add(result, i);
    }

  return result;
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_label(ar_ts *ts, ar_identifier *label)
{
  int i;
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( label != NULL );

  result = ar_ts_empty_trans_mark(ts);
  for(i=0; i<ts->nb_trans; i++)
    {
      if( ar_ca_event_contains(ts->trans_table.data[i].label,label) )
	ar_ts_mark_add(result,i);
    }

  return result;
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_event_attribute (ar_ts *ts, ar_identifier *label)
{
  int i;
  ar_ts_mark *result;

  ccl_pre (ts != NULL); 
  ccl_pre (label != NULL);

  result = ar_ts_empty_trans_mark (ts);
  for (i = 0; i < ts->nb_trans; i++)
    {
      if (ar_ca_event_has_attribute (ts->trans_table.data[i].label, label))
	ar_ts_mark_add (result, i);
    }

  return result;
}

