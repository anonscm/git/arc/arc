/*
 * ts-predef.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ts-algorithms.h"
#include "ts-p.h"

void
ar_ts_compute_any_state(ar_ts *ts, ar_ts_mark *M, int s)
{  
  ccl_pre( 0 <= s && s < ar_ts_mark_get_width(M) ); 

  ar_ts_mark_add(M,s);
}

			/* --------------- */

void
ar_ts_compute_empty_state(ar_ts *ts, ar_ts_mark *M, int s)
{  
  ccl_pre( 0 <= s && s < ar_ts_mark_get_width(M) ); 
}

			/* --------------- */

void
ar_ts_compute_any_trans(ar_ts *ts, ar_ts_mark *M, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_mark_get_width(M) ); 

  ar_ts_mark_add(M,t);
}

			/* --------------- */

void
ar_ts_compute_empty_trans(ar_ts *ts, ar_ts_mark *M, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_mark_get_width(M) ); 
}

			/* --------------- */

void
ar_ts_compute_epsilon(ar_ts *ts, ar_ts_mark *M, int t)
{
  ar_ts_event *ev;

  ccl_pre( 0 <= t && t < ar_ts_mark_get_width(M) );

  ev = ar_ts_trans_get_event(ts,t);

  if( ar_ts_event_is_epsilon(ev) )
    ar_ts_mark_add(M,t);
  ar_ts_event_del_reference(ev);
}

			/* --------------- */

void
ar_ts_compute_self(ar_ts *ts, ar_ts_mark *M, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) ); ccl_pre( M != NULL ); 

  if( ar_ts_trans_get_src(ts,t) == ar_ts_trans_get_tgt(ts,t) )
    ar_ts_mark_add(M,t);
}

			/* --------------- */

void
ar_ts_compute_self_epsilon(ar_ts *ts, ar_ts_mark *M, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_mark_get_width(M) ); 

  if( ar_ts_trans_get_src(ts,t) == ar_ts_trans_get_tgt(ts,t) )
    {
      ar_ts_event *  ev = ar_ts_trans_get_event(ts,t);

      if( ar_ts_event_is_epsilon(ev) )
	ar_ts_mark_add(M,t);
      ar_ts_event_del_reference(ev);
    }
}

			/* --------------- */

void
ar_ts_compute_not_deterministic(ar_ts *ts, ar_ts_mark *M, int t)
{
  int to;

  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) ); ccl_pre( M != NULL ); 

  for(to = ts->trans_table.data[t].src->first_out; to >= 0; 
      to = ts->trans_table.data[to].next_out)
    {
      if( to == t )
	continue;
      if( ts->trans_table.data[to].tgt == ts->trans_table.data[t].tgt )
	continue;

      if( ar_ts_event_equals(ts->trans_table.data[t].label,
			     ts->trans_table.data[to].label) )
	{
	  ar_ts_mark_add(M,t);
	  ar_ts_mark_add(M,to);
	}
    }
}

