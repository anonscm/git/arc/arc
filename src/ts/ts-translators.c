/*
 * ts-translators.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ts-algorithms.h"
#include "ts-translators.h"

			/* --------------- */

#define DOT_NODES_RANKS_PROP       "translators.ts.dot.nodes.ranks"      
#define DOT_NODES_LABELS_PROP 	   "translators.ts.dot.nodes.tooltips"
#define DOT_NODES_TOOLTIPS_PROP    "translators.ts.dot.nodes.labels"    
#define DOT_NODES_ATTRIBUTES_PROP  "translators.ts.dot.nodes.attributes"

#define DOT_EDGES_LABELS_PROP 	   "translators.ts.dot.edges.labels"    
#define DOT_EDGES_ATTRIBUTES_PROP  "translators.ts.dot.edges.attributes"
#define DOT_EDGES_TOOLTIPS_PROP    "translators.ts.dot.edges.tooltips"
#define DOT_EDGES_EPSILON_PROP 	   "translators.ts.dot.edges.epsilon"    
				  					 
#define QUOT_NODES_LABELS_PROP 	   "translators.ts.quot.nodes.tooltips"
#define QUOT_NODES_TOOLTIPS_PROP   "translators.ts.quot.nodes.labels"   
#define QUOT_NODES_ATTRIBUTES_PROP "translators.ts.quot.nodes.attributes"
#define QUOT_EDGES_LABELS_PROP 	   "translators.ts.quot.edges.attributes"
#define QUOT_EDGES_ATTRIBUTES_PROP "translators.ts.quot.edges.labels"  
#define QUOT_EDGES_EPSILON_PROP    "translators.ts.quot.edges.epsilon"
				  					 
#define WTS_EDGES_EPSILON_PROP 	   "translators.ts.wts.edges.epsilon"    

			/* --------------- */

void
ar_ts_translators_init(ccl_config_table *conf)
{
}

			/* --------------- */

void
ar_ts_translators_terminate(void)
{
}

			/* --------------- */

void
ar_ts_to_mec4(ccl_log_type log, ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T,
	      ccl_config_table *conf)
{
  int s,t;
  int show_epsilon = 
    ccl_config_table_get_boolean(conf,WTS_EDGES_EPSILON_PROP);

  ccl_pre( ts != NULL ); ccl_pre( S != NULL ); ccl_pre( T != NULL );

  {
    ar_identifier *id = ar_ts_get_name(ts);
    ccl_log(log,"transition_system ");
    ar_identifier_log_global_quote(log,id);
    ccl_log(log," < width = 0 >;\n");
    ar_identifier_del_reference(id);
  }

  for(s = ar_ts_mark_get_first(S); s >= 0; s = ar_ts_mark_get_next(S,s))
    {
      ccl_log(log,"'");
      ar_ts_display_state (log, ts, s, AR_SGS_DISPLAY_MEC4);
      ccl_log(log,"' |- ");

      for(t = ar_ts_state_get_first_out(ts,s); t >= 0; 
	  t = ar_ts_trans_get_next_out(ts,t))
	{
	  ccl_list *props;
	  ar_ts_event *label;
	  int tgt;

	  if( ! ar_ts_mark_has(T,t) )
	    continue;

	  label = ar_ts_trans_get_event(ts,t);
	  tgt = ar_ts_trans_get_tgt(ts,t);	  
	  props = ar_ts_trans_get_properties(ts,t);

	  ar_ts_event_log(log,label,show_epsilon, 1);
	  ar_ts_event_del_reference(label);


	  ccl_log(log," -> '");
	  ar_ts_display_state (log, ts, s, AR_SGS_DISPLAY_MEC4);
	  ccl_log(log,"' ");

	  if( tgt == s ) 
	    ccl_log(log,"/* loop */ ");
	  
	  if( ccl_list_get_size(props) > 0 )
	    {
	      ccl_log(log,"<property=(");
	      while( ! ccl_list_is_empty(props) )
		{
		  ar_identifier *id = 
		    (ar_identifier *)ccl_list_take_first(props);
		  ar_identifier_log(log,id);
		  if( ! ccl_list_is_empty(props) )
		    ccl_log(log,", ");
		  ar_identifier_del_reference(id);
		}
	      ccl_log(log,")>");
	    }
	  ccl_list_delete(props);
	  if( ar_ts_trans_get_next_out(ts,t) >= 0 )
	    ccl_log(log,",");
	  ccl_log(log,"\n");
	}
      ccl_log(log,";\n\n");
    }
  ccl_log(log,"<");

  {
    ar_identifier_iterator *ii = ar_ts_get_state_marks(ts);
    
    while( ccl_iterator_has_more_elements(ii) )
      {
	ar_identifier *id = ccl_iterator_next_element(ii);
	ar_ts_mark *M = ar_ts_get_state_mark(ts,id);
	ar_ts_mark *M_cap_S = ar_ts_mark_intersection(M,S);
	int i = ar_ts_mark_get_first(M_cap_S);

	if( i >= 0 )
	  {
	    ar_identifier_log(log,id);
	    ccl_log(log," = { ");
	    while( i >= 0 )
	      {
		ccl_log(log,"'");
		ar_ts_display_state (log, ts, i, AR_SGS_DISPLAY_MEC4);
		ccl_log(log,"'");
		if( (i = ar_ts_mark_get_next(M_cap_S,i)) >= 0 )
		  ccl_log(log,", ");
	      }
	    ccl_log(log," }");
	    if( ccl_iterator_has_more_elements(ii) )
	      ccl_log(log,";");
	  }
	ar_ts_mark_del_reference(M_cap_S);
	ar_ts_mark_del_reference(M);
	ar_identifier_del_reference(id);
      }
    ccl_iterator_delete(ii);
  }

  ccl_log(log,">.\n");
}

			/* --------------- */
static void
s_dot_state(ccl_log_type log, ar_ts *ts, int s, int rank, 
	    ccl_config_table *conf)
{
  int one = 
    ccl_config_table_get_boolean(conf,DOT_NODES_RANKS_PROP);

  ccl_log(log,"\ts%d[",s);

  if( one )
    ccl_log(log,"rank=%d",rank);

  if( ccl_config_table_get_boolean(conf,DOT_NODES_LABELS_PROP) )
    {
      if( one ) ccl_log(log,",");
      ccl_log(log,"label=\"");
      ar_ts_display_state (log, ts, s, AR_SGS_DISPLAY_DOT);
      ccl_log(log,"\"");
      one = 1;
    }

  if( ccl_config_table_get_boolean(conf,DOT_NODES_TOOLTIPS_PROP) )
    {
      ccl_list *props = ar_ts_state_get_properties(ts,s);
  
      if( one ) ccl_log(log,",");
      ccl_log(log,"URL=\"#\",tooltip=\"");
      while( ! ccl_list_is_empty(props) )
	{
	  ar_identifier *id = (ar_identifier *)ccl_list_take_first(props);
	  ar_identifier_log(log,id);
	  if( ! ccl_list_is_empty(props) )
	    ccl_log(log,", ");
	  ar_identifier_del_reference(id);
	}
      ccl_list_delete(props);
      ccl_log(log,"\"");
      one = 1;
    }
  ccl_log(log,"]\n");
}

			/* --------------- */

static void
s_dot_trans(ccl_log_type log, ar_ts *ts, int t, ccl_config_table *conf)
{
  int one = 
    ccl_config_table_get_boolean(conf,DOT_EDGES_LABELS_PROP);
  int show_epsilon = 
    ccl_config_table_get_boolean(conf,DOT_EDGES_EPSILON_PROP);

  ccl_log(log,"\ts%d -> s%d[",ar_ts_trans_get_src(ts,t),
	  ar_ts_trans_get_tgt(ts,t));

  if( one )
    {
      ar_ts_event *ev = ar_ts_trans_get_event(ts,t);
      ccl_log(log,"label=\"");
      ar_ts_event_log(log,ev,show_epsilon, 0);
      ccl_log(log,"\"");
      ar_ts_event_del_reference(ev);
    }

  if( ccl_config_table_get_boolean(conf,DOT_EDGES_TOOLTIPS_PROP) )
    {
      ccl_list *props = ar_ts_trans_get_properties(ts,t);
  
      if( one ) ccl_log(log,",");
      ccl_log(log,"URL=\"#\",tooltip=\"");
      while( ! ccl_list_is_empty(props) )
	{
	  ar_identifier *id = (ar_identifier *)ccl_list_take_first(props);
	  ar_identifier_log(log,id);
	  if( ! ccl_list_is_empty(props) )
	    ccl_log(log,", ");
	  ar_identifier_del_reference(id);
	}
      ccl_list_delete(props);
      ccl_log(log,"\"");
      one = 1;
    }
  ccl_log(log,"]\n");
}

			/* --------------- */

void
ar_ts_to_dot(ccl_log_type log, ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T,
	     ccl_config_table *conf)
{
  int s,t,tgt;
  int *queue;
  int *rank;
  int queue_size;
  ar_ts_mark *inQ;

  ccl_pre( ts != NULL ); ccl_pre( S != NULL ); ccl_pre( T != NULL );

  {
    ar_identifier *ts_name = ar_ts_get_name(ts);
    const char *tmp = 
      ccl_config_table_get(conf,QUOT_NODES_ATTRIBUTES_PROP);

    ccl_log(log,"digraph \"");
    ar_identifier_log(log,ts_name);
    ccl_log(log,"\" {\n");
    ar_identifier_del_reference(ts_name);

    if( tmp != NULL && *tmp != 0 )
      ccl_log(log,"\tnode[%s]\n",tmp);

    tmp = ccl_config_table_get(conf,QUOT_EDGES_ATTRIBUTES_PROP);
    if( tmp != NULL && *tmp != 0 )
      ccl_log(log,"\tedge[%s]\n",tmp);
    ccl_log(log,"\n");
  }

  queue_size = ar_ts_mark_get_size(S)+1;
  queue = ccl_new_array(int,queue_size);
  rank = ccl_new_array(int,ar_ts_get_nb_states(ts));
  queue_size = 0;

  s = ar_ts_mark_get_first(S);
  inQ = ar_ts_empty_state_mark(ts);

  for(; s >= 0 ; s = ar_ts_mark_get_next(S,s))
    {
      queue[queue_size++] = s;
      rank[s] = 0;
      ar_ts_mark_add(inQ,s);
    }
  
  while( queue_size != 0 )
    {
      s = queue[--queue_size];
      s_dot_state(log,ts,s,rank[s],conf);

      
      for(t = ar_ts_state_get_first_out(ts,s); t >= 0; 
	  t = ar_ts_trans_get_next_out(ts,t))
	{
	  if( ! ar_ts_mark_has(T,t) )
	    continue;
	  tgt = ar_ts_trans_get_tgt(ts,t);
	  if( ! ar_ts_mark_has(S,tgt) )
	    continue;

	  s_dot_trans(log,ts,t,conf);

	  rank[tgt] = rank[s]+1;
	  if( ! ar_ts_mark_has(inQ,tgt) )
	    {
	      queue[queue_size++] = tgt;
	      ar_ts_mark_add(inQ,tgt);
	    }
	}
    }

  ccl_log(log,"}\n");
  ar_ts_mark_del_reference(inQ);
  ccl_delete(queue);
  ccl_delete(rank);
}

			/* --------------- */

void
ar_ts_to_gml(ccl_log_type log, ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T,
	     ccl_config_table *conf)
{
  int s,t;
  int nb_states;

  ccl_log(log,"graph [ directed 1 version 2\n");
  
  ccl_pre( ts != NULL ); ccl_pre( S != NULL ); ccl_pre( T != NULL );

  nb_states = ar_ts_get_nb_states(ts);

  for(s = 0; s < nb_states; s++)
    {
      ccl_list *props;
      if( ! ar_ts_mark_has(S,s) )
	continue;
      props = ar_ts_state_get_properties(ts,s);
      ccl_log(log,
	      "  node [\n"
	      "    id %d\n"
	      "    label \"",s);
      ar_ts_display_state (log, ts, s, AR_SGS_DISPLAY_GML);
      if( ccl_list_get_size(props) > 0 )
	{
	  ccl_log(log,", ");
	  while( ! ccl_list_is_empty(props) )
	    {
	      ar_identifier *id = ccl_list_take_first(props);
	      ar_identifier_log(log,id);
	      if( ! ccl_list_is_empty(props) )
		ccl_log(log,", ");
	      ar_identifier_del_reference(id);
	    }
	}
      ccl_list_delete(props);
      ccl_log(log,
	      "\"\n"
	      "  ]\n");
    }
  ccl_log(log,"\n");

  for(s = 0; s < nb_states; s++)
    {  
      if( ! ar_ts_mark_has(S,s) )
	continue;

      for(t = ar_ts_state_get_first_out(ts,s); t >= 0; 
	  t = ar_ts_trans_get_next_out(ts,t))
	{
	  ccl_list *props;
	  ar_ts_event *label;

	  if( ! ar_ts_mark_has(T,t) )
	    continue;

	  props = ar_ts_trans_get_properties(ts,t);
	  label = ar_ts_trans_get_event(ts,t);
	  
	  ccl_log(log,
		  "  edge [\n"
		  "    source %d\n"
		  "    target %d\n"
		  "    id %d\n"
		  "    label \"",
		  ar_ts_trans_get_src(ts,t),
		  ar_ts_trans_get_tgt(ts,t),
		  t,
		  label);
	  ar_ts_event_log(log,label,1, 0);
	  if( ccl_list_get_size(props) > 0 )
	    {
	      ccl_log(log,"\\\\n");
	      while( ! ccl_list_is_empty(props) )
		{
		  ar_identifier *id = ccl_list_take_first(props);
		  ar_identifier_log(log,id);
		  if( ! ccl_list_is_empty(props) )
		    ccl_log(log,"\\\\n");
		  ar_identifier_del_reference(id);
		}
	    }

	  ccl_log(log,
		  "\"\n"
		  "  ]\n");

	  ar_ts_event_del_reference(label);
	  ccl_list_delete(props);
	}
    }
  ccl_log(log,"]\n");
}

			/* --------------- */

void
ar_ts_display_state (ccl_log_type log, ar_ts *ts, int s,
		     ar_sgs_display_format df)
{
  ar_ts_state_log (log, ts, s);
}

			/* --------------- */

void
ar_ts_display_trans (ccl_log_type log, ar_ts *ts, int t,
		     ar_sgs_display_format df)
{
  int src = ar_ts_trans_get_src(ts, t);
  int tgt = ar_ts_trans_get_tgt(ts, t);
  ar_ts_event *e = ar_ts_trans_get_event (ts, t);

  ar_ts_display_state (log, ts, src, df);
  ccl_log (log, "|- ");
  ar_ts_event_log (log, e, 0, 0);
  ccl_log (log, " ->");
  ar_ts_display_state (log, ts, tgt, df);
  ar_ts_event_del_reference (e);
}

			/* --------------- */

#if 0
static void
s_dot_class(ccl_log_type log, ar_ts *ts, ar_ts_mark *M, int i, 
	    ccl_config_table *conf)
{
  int one = 
    ccl_config_table_get_boolean(conf,QUOT_NODES_LABELS_PROP);

  ccl_log(log,"\tC%d[",i);

  if( one )
    {
      int s = ar_ts_mark_get_first(M);

      ccl_log(log,"label=\"");
      while( s >= 0 )
	{
	  ar_ts_state_log(log,ts,s);
	  if( (s = ar_ts_mark_get_next(M,s)) >= 0 )
	    ccl_log(log,"\\n");
	}      
      ccl_log(log,"\"");
    }

  if( ccl_config_table_get_boolean(conf,QUOT_NODES_TOOLTIPS_PROP) )
    {    
      int s = ar_ts_mark_get_first(M);
      ccl_list *props = ar_ts_state_get_properties(ts,s);

      if( one ) ccl_log(log,",");
      ccl_log(log,"URL=\"#\",tooltip=\"");
      while( ! ccl_list_is_empty(props) )
	{
	  ar_identifier *id = (ar_identifier *)ccl_list_take_first(props);
	  ar_identifier_log(log,id);
	  if( ! ccl_list_is_empty(props) )
	    ccl_log(log,", ");
	  ar_identifier_del_reference(id);
	}
      ccl_log(log,"\"");
      ccl_list_delete(props);
      one = 1;
    }
  ccl_log(log,"]\n");
}

			/* --------------- */

struct qtrans
{
  int src;
  ar_ts_event *ev;
  int tgt;
};

static unsigned int
s_qtrans_hash (const void *ptr)
{
  const struct qtrans *qtr = ptr;

  return 19 * qtr->src + 13 * qtr->tgt + (uintptr_t) qtr->ev;
}

static int
s_qtrans_compare (const void *p1, const void *p2)
{
  const struct qtrans *qtr1 = p1;
  const struct qtrans *qtr2 = p2;

  if (qtr1->src == qtr2->src && 
      qtr1->tgt == qtr2->tgt && 
      qtr1->ev == qtr1->ev)
    return 0;

  return 1;
}

			/* --------------- */

CCL_DEFINE_DELETE_PROC (s_qtrans_delete);

void
ar_ts_quot(ccl_log_type log, ar_ts *ts, ccl_config_table *conf)
{
  int i;
  ccl_pair *C;
  ccl_list *classes = ar_ts_compute_classes (ts);
  int show_epsilon = 
    ccl_config_table_get_boolean (conf, QUOT_EDGES_EPSILON_PROP);
  int label_enabled = 
    ccl_config_table_get_boolean (conf, QUOT_EDGES_LABELS_PROP);
  ccl_hash *qtransitions = ccl_hash_create (s_qtrans_hash, s_qtrans_compare, 
					    NULL, s_qtrans_delete);
  
  {
    const char *tmp = 
      ccl_config_table_get(conf,QUOT_NODES_ATTRIBUTES_PROP);
    ar_identifier *name = ar_ts_get_name(ts);

    ccl_log(log,"digraph \"");
    ar_identifier_log(log,name);
    ccl_log(log,"/Q\" {\n");
    ar_identifier_del_reference(name);

    if( tmp != NULL && *tmp != 0 )
      ccl_log(log,"\tnode[%s]\n",tmp);

    tmp = ccl_config_table_get(conf,QUOT_EDGES_ATTRIBUTES_PROP);
    if( tmp != NULL && *tmp != 0 ) 
      ccl_log(log,"\tedge[%s]\n",tmp);
    ccl_log(log,"\n");
  }

  for(C = FIRST(classes), i = 0; C; C = CDR(C), i++)
    {
      ar_ts_mark *M = (ar_ts_mark *)CAR(C);
      s_dot_class(log,ts,M,i,conf);
    }

  while (! ccl_list_is_empty (classes))
    {
      int t;
      struct qtrans qtr;
      ar_ts_mark *M = (ar_ts_mark *) ccl_list_take_first (classes);
      int s = ar_ts_mark_get_first (M);

      qtr.src = classes_by_state[s];
      for(t = ar_ts_state_get_first_out (ts, s); t >= 0; 
	  t = ar_ts_trans_get_next_out (ts, t))
	{
	  int tgt = ar_ts_trans_get_tgt (ts, t);
	  qtr.tgt = classes_by_state[tgt];
	  qtr.ev = ar_ts_trans_get_event (ts, t);

	  if (! ccl_hash_find (qtransitions, &qtr))
	    {
	      struct qtrans *pqtr = ccl_new (struct qtrans);

	      *pqtr = qtr;
	      ccl_hash_find (qtransitions, pqtr);
	      ccl_hash_insert (qtransitions, pqtr);

	      ccl_log (log, "\tC%d -> C%d", qtr.src, qtr.tgt);

	      if (label_enabled)
		{
		  ccl_log(log, "[label=\"");
		  ar_ts_event_log (log, qtr.ev, show_epsilon, 0);
		  ccl_log (log,"\"]");
		}
	      ccl_log (log, "\n");
	    }
	  ar_ts_event_del_reference (qtr.ev);
	}
      ar_ts_mark_del_reference (M);
    }

  ccl_log (log, "}\n");  
  ccl_hash_delete (qtransitions);
  ccl_delete (classes_by_state);
  ccl_list_delete (classes);
}
#endif


			/* --------------- */

