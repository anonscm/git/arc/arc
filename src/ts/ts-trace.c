/*
 * ts-trace.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-bittable.h>
#include "ts-p.h"
#include "ts-algorithms.h"

			/* --------------- */

static int
s_dfs_for_trace(ar_ts *ts, int S, ar_ts_mark *S2, ccl_bittable *follow,
		ccl_bittable *visited, ar_ts_mark *R)
{
  int t, tgt, result = 0;

  if( M_HAS(S2,S) ) result = 1;
  else if( ccl_bittable_has(visited,S) ) result = 0;
  else
    {
      ccl_bittable_set(visited,S);
      t = ts->state_table.data[S]->first_out; 
      for(; t >= 0 && ! result; t = ts->trans_table.data[t].next_out)
	{
	  if( ccl_bittable_has(follow,t) )
	    {
	      tgt = ts->trans_table.data[t].tgt->index;
	      result = s_dfs_for_trace(ts,tgt,S2,follow,visited,R);
	      if( result )
		ar_ts_mark_add(R,t);
	    }
	}
    }

  return result;
}

			/* --------------- */


static void
s_bfs_for_trace(ar_ts *ts, ar_ts_mark *S1, ar_ts_mark *T, ar_ts_mark *S2,
		ar_ts_mark *R)
{
  int  i, s, t, src;  
  int *queue = ccl_new_array(int,ts->nb_states);
  int  queue_size = 0;
  ccl_bittable *follow = ccl_bittable_create(ts->nb_trans);
  ccl_bittable *inQ = ccl_bittable_create(ts->nb_states);

  for(i = M_FIRST(S2); i>= 0; i = M_NEXT(S2,i))
    {
      if( M_HAS(S1,i) )
	continue;
      ccl_bittable_set(inQ,i);
      queue[queue_size++] = i;
    }

  while( queue_size > 0 )
    {
      s = queue[0];
      queue_size--;

      ccl_memcpy(queue,queue+1,queue_size*sizeof(int));
      
      t = ts->state_table.data[s]->first_in;
      while( t >= 0 )
	{
	  if( M_HAS(T,t) ) 
	    {
	      src = ts->trans_table.data[t].src->index;

	      if( ! ccl_bittable_has(inQ,src) )
		{
		  ccl_bittable_set(follow,t);
		  ccl_bittable_set(inQ,src);

		  if( M_HAS(S1,src) )
		    {
		      ccl_bittable *dvisited = 
			ccl_bittable_create(ts->nb_states);
		      s_dfs_for_trace(ts,src,S2,follow,dvisited,R);
		      ccl_bittable_delete(dvisited);
		      goto end;
		    }
		  else
		    queue[queue_size++] = src;
		}
	    }

	  t = ts->trans_table.data[t].next_in;
	}
    }

 end:
  ccl_bittable_delete(follow);
  ccl_bittable_delete(inQ);
  ccl_delete(queue);
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_trace(ar_ts *ts, ar_ts_mark *S1, ar_ts_mark *T, ar_ts_mark *S2)
{
  ar_ts_mark *result = ar_ts_mark_create(AR_SGS_SET_IS_TRANS, ts->nb_trans);

  ccl_pre( ts != NULL ); ccl_pre( S1 != NULL ); ccl_pre( T != NULL ); 
  ccl_pre( S2 != NULL );

  s_bfs_for_trace(ts,S1,T,S2,result);

  return result;
}

			/* --------------- */

