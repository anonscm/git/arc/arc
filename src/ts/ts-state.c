/*
 * ts-state.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include "ts-p.h"

static char *
s_state_to_string(ar_ts *ts, ts_state *s);

static void
s_state_log(ccl_log_type log, ar_ts *ts, ts_state *s);

			/* --------------- */

ccl_list *
ar_ts_state_get_properties(ar_ts *ts, int s)
{
  ccl_list *result = ccl_list_create();
  ar_identifier_iterator *i = ar_ts_get_state_marks(ts);

  ccl_pre( 0 <= s && s < ar_ts_get_nb_states(ts) );

  while( ccl_iterator_has_more_elements(i) )
    {
      ar_identifier *id = ccl_iterator_next_element(i);
      ar_ts_mark *mark = ar_ts_get_state_mark(ts,id);
      
      if( ar_ts_mark_has(mark,s) )
	ccl_list_add(result,id);
      else
	ar_identifier_del_reference(id);
      ar_ts_mark_del_reference(mark);
    }
  ccl_iterator_delete(i);

  return result;
}

			/* --------------- */

char *
ar_ts_state_to_string(ar_ts *ts, int s)
{
  ccl_pre( 0 <= s && s < ar_ts_get_nb_states(ts) );

  return s_state_to_string(ts,ts->state_table.data[s]);
}

			/* --------------- */

void
ar_ts_state_log(ccl_log_type log, ar_ts *ts, int s)
{
  ccl_pre( 0 <= s && s < ar_ts_get_nb_states(ts) );

  s_state_log(log,ts,ts->state_table.data[s]);
}

			/* --------------- */

int
ar_ts_state_get_first_out(ar_ts *ts, int s)
{
  ccl_pre( ts != NULL ); ccl_pre( 0 <= s && s < ts->nb_states);

  return ts->state_table.data[s]->first_out;
}

			/* --------------- */

int
ar_ts_state_get_first_in(ar_ts *ts, int s)
{
  ccl_pre( 0 <= s && s < ar_ts_get_nb_states(ts) );

  return ts->state_table.data[s]->first_in;
}

			/* --------------- */

void
ar_ts_state_get_assignments(ar_ts *ts, int s, ccl_list *vars, int *result)
{
  int i;
  ccl_pair *p;
  
  ccl_pre (0 <= s && s < ar_ts_get_nb_states (ts));

  for(i = 0, p = FIRST (vars); p; p = CDR (p), i++)
    {
      int vindex = (int) varorder_get_index (ts->super.order, CAR (p));

      result[i] = 
	ts->min_values[vindex] + ts->state_table.data[s]->body[vindex];
    }
}

			/* --------------- */

int
ar_ts_state_get_in_degree (ar_ts *ts, int s)
{
  int i;
  int result = 0;

  for (i = ar_ts_state_get_first_in (ts, s); i >= 0;
       i = ar_ts_trans_get_next_in (ts, i))
    result++;

  return result;
}

			/* --------------- */

int
ar_ts_state_get_out_degree (ar_ts *ts, int s)
{
  int i;
  int result = 0;

  for (i = ar_ts_state_get_first_out (ts, s); i >= 0;
       i = ar_ts_trans_get_next_out (ts, i))
    result++;

  return result;
}

			/* --------------- */

static char *
s_state_to_string(ar_ts *ts, ts_state *s)
{
  int i;
  const char *fmt = "%s=%s,";
  char *result = NULL;
  ar_ca_exprman *man = ar_ca_get_expression_manager(ts->super.ca);

  for(i = 0; i < ts->variables.size; i++)
    {      
      int val = ts->min_values[i]+s->body[i];
      const ar_ca_domain *dom = 
	ar_ca_expr_variable_get_domain(ts->variables.data[i]);
      ar_identifier *vname = 
	ar_ca_expr_variable_get_name(ts->variables.data[i]);
      char *sval = ar_ca_domain_value_to_string(dom,val,man);
      char *svname = ar_identifier_to_string(vname);

      if( i == ts->variables.size-1 )
	fmt = "%s=%s";
      ccl_string_format_append(&result,fmt,svname,sval);
      ar_identifier_del_reference(vname);
    }
  ar_ca_exprman_del_reference(man);
  
  return result;
}

			/* --------------- */

static void
s_state_log (ccl_log_type log, ar_ts *ts, ts_state *s)
{
  int i;
  ar_ca_exprman *man = ar_ca_get_expression_manager (ts->super.ca);

  for (i = 0; i < ts->variables.size; i++)
    {      
      int val = ts->min_values[i] + s->body[i];
      const ar_ca_domain *dom = 
	ar_ca_expr_variable_get_domain (ts->variables.data[i]);

      ar_ca_expr_log (log, ts->variables.data[i]);
      ccl_log (log, "=");
      ar_ca_domain_log_value (log, dom, val, man);
      if (i < ts->variables.size - 1)
	ccl_log (log, ",");
    }
  ar_ca_exprman_del_reference (man);
}
