/*
 * ts-semantics.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TS_SEMANTICS_H
# define TS_SEMANTICS_H

# include "ar-node.h"
# include <ts/ts.h>

extern ar_ts *
ar_compute_transition_system_semantics(ar_node *node)
  CCL_THROW(altarica_interpretation_exception);

#endif /* ! TS_SEMANTICS_H */
