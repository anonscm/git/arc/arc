/*
 * ts-reach.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-bittable.h>
#include "ts-p.h"
#include "ts-algorithms.h"

static void
s_reach_dfs(ar_ts *ts, int from, ar_ts_mark *by, ccl_bittable *visited, 
	    ar_ts_mark *result)
{
  if( ! ccl_bittable_has(visited,from) )
    {
      int t;

      ccl_bittable_set(visited,from);
      ar_ts_mark_add(result,from);
      t = ts->state_table.data[from]->first_out;
      for(; t>= 0; t = ts->trans_table.data[t].next_out)
	{
	  if( ar_ts_mark_has(by,t) )
	    s_reach_dfs(ts,ts->trans_table.data[t].tgt->index,by,visited,
			result);
	}
    }
}

			/* --------------- */


ar_ts_mark *
ar_ts_compute_reach(ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T)
{
  int i;
  ccl_bittable *visited;
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( S != NULL ); ccl_pre( T != NULL );
  
  i = ar_ts_mark_get_first(S);
  visited = ccl_bittable_create(ts->nb_states);
  result = ar_ts_empty_state_mark(ts);
  for(; i>= 0; i = ar_ts_mark_get_next(S,i))
    {
      if( ! ccl_bittable_has(visited,i) )
	s_reach_dfs(ts,i,T,visited,result);
    }
  ccl_bittable_delete(visited);

  return result;
}
