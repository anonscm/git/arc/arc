/*
 * ts-p.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_TRANSITION_SYSTEM_P_H
# define AR_TRANSITION_SYSTEM_P_H

# include "ar-state-graph-semantics-p.h"
# include "ts.h"

typedef struct ts_state_st {
  int index;
  int first_in;
  int first_out;
  int body[1];
} ts_state;

typedef CCL_ARRAY(ts_state *) ts_state_array;

typedef struct ts_trans_st {
  ts_state *src;
  ar_ts_event *label;
  ts_state *tgt;
  int next_in;
  int next_out;
} ts_trans;

typedef CCL_ARRAY(ts_trans) ts_trans_array;

struct ar_ts_st {
  ar_sgs super;

  int *is_state;
  /* infos about variables */
  ar_ca_expr_array variables;
  int *min_values;

  /* state table */
  ts_state_array state_table;
  int nb_states;
  size_t state_size; 

  /* transition table */
  ts_trans_array trans_table;
  int nb_trans ;

  /* tempory data */
  ts_state *tmp_s;
};

# define M_FIRST(_m) ar_ts_mark_get_first(_m)
# define M_NEXT(_m,_i) ar_ts_mark_get_next(_m,_i)
# define M_HAS(_m,_i) ar_ts_mark_has(_m,_i)

extern ts_state *
ar_ts_new_state (ar_ts *ts);

extern void
ar_ts_delete_state (ts_state *st);

extern ts_state *
ar_ts_dup_state (ar_ts *ts, const ts_state *st);

extern int
ar_ts_eval_expr_in_state (ar_ts *ts, ar_ca_expr *expr, const ts_state *st);

#endif /* ! AR_TRANSITION_SYSTEM_P_H */
