/*
 * ts-semantics.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ar-semantics.h"
#include "ts-semantics.h"

ar_ts *
ar_compute_transition_system_semantics(ar_node *node)
  CCL_THROW ((altarica_interpretation_exception, 
	      domain_cardinality_exception))
{  
  ar_ts *result = NULL;
  ar_ca *ca = ar_semantics_get (node, AR_SEMANTICS_CONSTRAINT_AUTOMATON);

  if (ar_ca_get_max_cardinality (ca) > AR_TS_MAX_STATE_CARD)
    {
      ar_ca_del_reference(ca);
      ccl_throw_no_msg (domain_cardinality_exception);
    }

  
  result = ar_ts_create (ca);

  ar_ca_del_reference(ca);

  return result;
}
