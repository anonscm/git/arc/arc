/*
 * ar-ts-mark.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_TS_MARK_H
# define AR_TS_MARK_H

typedef struct ar_ts_mark_st ar_ts_mark;

extern ar_ts_mark *
ar_ts_mark_create (int is_state_set, int width);

extern ar_ts_mark *
ar_ts_mark_add_reference (ar_ts_mark *mark);

extern void
ar_ts_mark_del_reference (ar_ts_mark *mark);

extern void
ar_ts_mark_add (ar_ts_mark *mark, int i);

extern void
ar_ts_mark_remove (ar_ts_mark *mark, int i);

extern int
ar_ts_mark_has (ar_ts_mark *mark, int i);

# define ar_ts_mark_is_empty(m) (ar_ts_mark_get_size (m) == 0)

extern int
ar_ts_mark_get_size (ar_ts_mark *mark);

extern int
ar_ts_mark_get_width (ar_ts_mark *mark);

extern void
ar_ts_mark_change_width (ar_ts_mark *mark, int width);

extern int
ar_ts_mark_get_first (ar_ts_mark *mark);

extern int
ar_ts_mark_get_next (ar_ts_mark *mark, int from);

extern ar_ts_mark *
ar_ts_mark_union (ar_ts_mark *m1, ar_ts_mark *m2);

extern ar_ts_mark *
ar_ts_mark_complement (ar_ts_mark *m);

extern ar_ts_mark *
ar_ts_mark_intersection (ar_ts_mark *m1, ar_ts_mark *m2);

extern ar_ts_mark *
ar_ts_mark_sub (ar_ts_mark *m1, ar_ts_mark *m2);

extern int
ar_ts_mark_are_equal (ar_ts_mark *m1, ar_ts_mark *m2);

#endif /* ! AR_TS_MARK_H */
