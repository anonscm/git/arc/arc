/*
 * ts-translators.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TS_TRANSLATORS_H
# define TS_TRANSLATORS_H

# include <ccl/ccl-config-table.h>
# include <ts/ts.h>
# include "ar-state-graph-semantics.h"

extern void
ar_ts_translators_init(ccl_config_table *conf);

extern void
ar_ts_translators_terminate(void);

extern void
ar_ts_to_dot(ccl_log_type log, ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T,
	     ccl_config_table *conf);

extern void
ar_ts_to_mec4(ccl_log_type log, ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T,
	      ccl_config_table *conf);

extern void
ar_ts_to_gml(ccl_log_type log, ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T,
	     ccl_config_table *conf);

extern void
ar_ts_quot(ccl_log_type log, ar_ts *ts, ccl_config_table *conf);

extern void
ar_ts_display_state (ccl_log_type log, ar_ts *ts, int s,
		     ar_sgs_display_format df);

extern void
ar_ts_display_trans (ccl_log_type log, ar_ts *ts, int t,
		     ar_sgs_display_format df);

#endif /* ! TS_TRANSLATORS_H */
