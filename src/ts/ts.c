/*
 * ts.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ts-p.h"
#include "ts-algorithms.h"
#include "ts-translators.h"

#define SGS(ts) ((ar_sgs *)(ts))
#define SGS_SET(m) ((ar_sgs_set *)(m))
#define TS(sgs) ((ar_ts *)(sgs))
#define TS_MARK(s) ((ar_ts_mark *)(s))

#define TS_MIN_TRANS_TABLE_SIZE 250000

static void
s_init_variables (ar_ts *ts);

static int
s_ts_sets_are_equal (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
static ccl_hash *
s_ts_get_events (ar_sgs *sgs, ar_sgs_set *T);

#define s_rs_get_rel_for_event NULL

static ar_expr *
s_ts_states_to_formula (ar_sgs *sgs, ar_sgs_set *S, ccl_list *pvars, 
			ccl_list *vars, int exist);

#define s_ts_states_to_ca_expr NULL
#define s_ts_post_add_set NULL
#define s_ts_pre_remove_set NULL

static void 
s_ts_display_set (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *set,
		  ar_sgs_display_format df);
static void 
s_ts_destroy (ar_sgs *sgs);
static double 
s_ts_card (ar_sgs *sgs, ar_sgs_set *S);
static void 
s_ts_to_mec4 (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	      ccl_config_table *conf);
static void
s_ts_to_dot (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf);

static void 
s_ts_to_gml (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf);

static ar_sgs_set *
s_ts_assignment (ar_sgs *sgs, ar_ca_expr **assignments,  int nb_assignments);

static int
s_ts_is_empty (ar_sgs *sgs, ar_sgs_set *S);

#define s_ts_post NULL
#define s_ts_pre NULL
static ar_sgs_set * 
s_ts_proj (ar_sgs *sgs, ar_sgs_set *X, int on_state);
static ar_sgs_set * 
s_ts_pick (ar_sgs *sgs, ar_sgs_set *X);
static ar_sgs_set * 
s_ts_assert_config (ar_sgs *sgs, ar_sgs_set *T);
static ar_sgs_set * 
s_ts_src (ar_sgs *sgs, ar_sgs_set *T);
static ar_sgs_set * 
s_ts_tgt (ar_sgs *sgs, ar_sgs_set *T);
static ar_sgs_set * 
s_ts_rsrc (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_ts_rtgt (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_ts_loop (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2); 
static ar_sgs_set * 
s_ts_trace (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, ar_sgs_set *S2);
static ar_sgs_set * 
s_ts_unav (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S);
static ar_sgs_set * 
s_ts_reach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);
static ar_sgs_set * 
s_ts_coreach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T);
static ar_sgs_set * 
s_ts_state_set (ar_sgs *sgs, ar_ca_expr *cond);
static ar_sgs_set * 
s_ts_label (ar_sgs *sgs, ar_identifier *label);
static ar_sgs_set * 
s_ts_event_attribute (ar_sgs *sgs, ar_identifier *attribute);
static ccl_list *
s_ts_classes (ar_sgs *sgs);
static ar_sgs_set * 
s_ts_lfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);
static ar_sgs_set * 
s_ts_gfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F);
static ar_sgs_set * 
s_ts_set_complement (ar_sgs *sgs, ar_sgs_set *S);
static ar_sgs_set * 
s_ts_set_union (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
static ar_sgs_set * 
s_ts_set_intersection (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);
static ar_sgs_set * 
s_ts_set_difference (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2);

			/* --------------- */

static const ar_sgs_predef_set PREDEFINED_STATE_SETS[] = { { NULL, NULL } };
static const ar_sgs_predef_set PREDEFINED_TRANS_SETS[] = { { NULL, NULL } };

static const struct ar_state_graph_semantics_methods_st TS_METHODS = 
{
  PREDEFINED_STATE_SETS,
  PREDEFINED_TRANS_SETS,
  s_ts_sets_are_equal,
  s_ts_get_events,
  s_rs_get_rel_for_event,
  s_ts_states_to_formula,
  s_ts_states_to_ca_expr, 
  s_ts_post_add_set,
  s_ts_pre_remove_set,
  s_ts_display_set,
  s_ts_destroy,
  s_ts_card,
  s_ts_to_mec4,
  s_ts_to_dot,
  s_ts_to_gml,
  s_ts_assignment,
  s_ts_is_empty,
  s_ts_post,
  s_ts_pre,
  s_ts_proj,
  s_ts_pick,
  s_ts_assert_config,
  s_ts_src,
  s_ts_tgt,
  s_ts_rsrc,
  s_ts_rtgt,
  s_ts_loop, 
  s_ts_trace,
  s_ts_unav,
  s_ts_reach,
  s_ts_coreach,
  s_ts_state_set,
  s_ts_label,
  s_ts_event_attribute,
  s_ts_classes,
  s_ts_lfp,
  s_ts_gfp,
  s_ts_set_complement,
  s_ts_set_union,
  s_ts_set_intersection,
  s_ts_set_difference
};

			/* --------------- */

ar_ts *
ar_ts_create (ar_ca *ca)
{
  ar_ts *result;
  ccl_pre (ar_ca_get_max_cardinality (ca) <= AR_TS_MAX_STATE_CARD);

  result = (ar_ts *) ar_sgs_create (ca, sizeof (ar_ts), &TS_METHODS);
  s_init_variables (result);

  ccl_array_init (result->state_table);
  result->nb_states = 0;
  result->state_size = (sizeof (ts_state) +
			sizeof (int) * (result->variables.size - 1));

  ccl_array_init_with_size (result->trans_table, TS_MIN_TRANS_TABLE_SIZE);
  result->nb_trans = 0;

  result->tmp_s = (ts_state *) 
    ccl_calloc (result->state_size, sizeof (int));
  result->tmp_s->first_in = result->tmp_s->first_out = -1;

  ar_ts_build (result);

  return result;
}

			/* --------------- */

ar_ts *
ar_ts_add_reference (ar_ts *ts)
{
  return (ar_ts *) ar_sgs_add_reference (SGS (ts));
}

			/* --------------- */

void
ar_ts_del_reference (ar_ts *ts)
{
  ar_sgs_del_reference (SGS (ts));
}

			/* --------------- */

ar_ca *
ar_ts_get_constraint_automaton (ar_ts *ts)
{
  return ar_sgs_get_constraint_automaton (SGS (ts));
}

			/* --------------- */

ar_node *
ar_ts_get_node (ar_ts *ts)
{
  return ar_sgs_get_node (SGS (ts));
}

			/* --------------- */

ar_ca_exprman *
ar_ts_get_expr_manager (ar_ts *ts)
{
  return ar_sgs_get_expr_manager (SGS (ts));
}

			/* --------------- */

ar_identifier *
ar_ts_get_name (ar_ts *ts)
{
  return ar_sgs_get_name (SGS (ts));
}

			/* --------------- */

int
ar_ts_get_nb_states (ar_ts *ts)
{
  ccl_pre (ts != NULL);

  return ts->nb_states;
}

			/* --------------- */

int
ar_ts_get_nb_trans (ar_ts *ts)
{
  ccl_pre (ts != NULL);

  return ts->nb_trans;
}

			/* --------------- */

int
ar_ts_has_mark (ar_ts *ts, ar_identifier *name, int *is_state)
{
  return ar_sgs_has_set (SGS (ts), name, is_state);
}

			/* --------------- */

int
ar_ts_has_state_mark (ar_ts *ts, ar_identifier *name)
{
  return ar_sgs_has_state_set (SGS (ts), name);
}

			/* --------------- */

void
ar_ts_remove_state_mark (ar_ts *ts, ar_identifier *name)
{
  ar_sgs_remove_state_set (SGS (ts), name);
}

			/* --------------- */


int
ar_ts_has_trans_mark (ar_ts *ts, ar_identifier *name)
{
  return ar_sgs_has_trans_set (SGS (ts), name);
}

			/* --------------- */

void
ar_ts_remove_trans_mark (ar_ts *ts, ar_identifier *name)
{
  ar_sgs_remove_trans_set (SGS (ts), name);
}

			/* --------------- */

void
ar_ts_add_state_mark(ar_ts *ts, ar_identifier *name, ar_ts_mark *mark)
{
  ar_sgs_add_state_set (SGS (ts), name, SGS_SET (mark));
}

			/* --------------- */

void
ar_ts_add_trans_mark(ar_ts *ts, ar_identifier *name, ar_ts_mark *mark)
{
  ar_sgs_add_trans_set (SGS (ts), name, SGS_SET (mark));
}

			/* --------------- */

ar_ts_mark *
ar_ts_get_state_mark(ar_ts *ts, ar_identifier *name)
{
  return TS_MARK (ar_sgs_get_state_set (SGS (ts), name));
}

			/* --------------- */

ar_ts_mark* 
ar_ts_get_trans_mark(ar_ts *ts, ar_identifier *name)
{
  return TS_MARK (ar_sgs_get_trans_set (SGS (ts), name));
}

			/* --------------- */

ar_identifier_iterator *
ar_ts_get_state_marks(ar_ts *ts)
{
  return ar_sgs_get_state_sets (SGS (ts));
}

			/* --------------- */

ar_identifier_iterator *
ar_ts_get_trans_marks(ar_ts *ts)
{
  return ar_sgs_get_trans_sets (SGS (ts));
}

			/* --------------- */

ar_ts_mark *
ar_ts_crt_mark (ar_ts *ts, int empty, int state)
{
  int max;
  ar_ts_mark *result;

  ccl_pre (ts != NULL);

  max = state ? ts->nb_states : ts->nb_trans;
  result = ar_ts_mark_create (state, max);

  while (! empty && max--)
    ar_ts_mark_add (result, max);

  return result;
}

			/* --------------- */

void
ar_ts_log (ccl_log_type log, ar_ts *ts, ar_identifier *name)
{
  int s;

  ccl_log (log,"transition_system ");

  if (name != NULL)
    ar_identifier_log (log, name);
  ccl_log (log, ";\n");
  ccl_log (log,
	   "/*\n"
	   " * # states = %d\n"
	   " * # trans = %d\n"
	   " */\n", ts->nb_states, ts->nb_trans);

  for (s = 0; s < ts->nb_states; s++)
    {
      int t;
      
      ar_ts_state_log (log, ts, s);

      for (t = ts->state_table.data[s]->first_out; t >= 0; 
	   t = ts->trans_table.data[t].next_out)
	{
	  ts_trans *T = ts->trans_table.data+t;

	  ccl_log (log, "\t|- ");
	  ar_ts_event_log (log, T->label, 1, 1);
	  ccl_log (log, "-> ");
	  ar_ts_state_log (log, ts, T->tgt->index);
	  if (ts->trans_table.data[t].next_out >= 0)
	    ccl_log (log, ";\n");
	}

      ccl_log (log, ";\n");
    }
  
  ccl_log (log, "<");

  {
    ar_identifier_iterator *ii = ar_ts_get_state_marks (ts);
    while (ccl_iterator_has_more_elements (ii))
      {
	int s;
	ar_identifier *name = ccl_iterator_next_element (ii);
	ar_ts_mark *m = ar_ts_get_state_mark (ts, name);

	ar_identifier_log (log, name);
	ccl_log (log, " = { ");
	for (s = ar_ts_mark_get_first (m); s >= 0; )
	  {
	    ar_ts_state_log (log, ts, s);
	    if( (s = ar_ts_mark_get_next (m, s)) >= 0 )
	      ccl_log (log, ", ");
	  }

	if (ccl_iterator_has_more_elements (ii))
	  ccl_log (log, " }, ");
	else
	  ccl_log (log, " }");
	ar_ts_mark_del_reference (m);
	ar_identifier_del_reference (name);
      }
    
    ccl_iterator_delete (ii);
  }

  ccl_log (log, ">.\n");
}

			/* --------------- */
ts_state *
ar_ts_new_state (ar_ts *ts)
{
  ts_state *result = (ts_state *) ccl_calloc (ts->state_size, sizeof (int));
  result->first_in = result->first_out = -1;

  return result;
}

void
ar_ts_delete_state (ts_state *st)
{
  ccl_delete (st);
}

ts_state *
ar_ts_dup_state (ar_ts *ts, const ts_state *st)
{
  ts_state *result = ar_ts_new_state (ts);

  ccl_memcpy (result, st, ts->state_size * sizeof (int));

  return result;
}

			/* --------------- */

static int
s_cmp_vars (const void *pv1, const void *pv2, void *data)
{
  const ar_ca_expr **v1 = (const ar_ca_expr **) pv1;
  const ar_ca_expr **v2 = (const ar_ca_expr **) pv2;

  return (varorder_get_index (data, CCL_BITPTR2PTR (const ar_ca_expr *, v1)) - 
	  varorder_get_index (data, CCL_BITPTR2PTR (const ar_ca_expr *, v2)));
}

			/* --------------- */

static ccl_list * 
s_sorted_variables (ar_ts *ts)
{
  int i;
  const ccl_pair *p;
  const ccl_list *vars = ar_ca_get_flow_variables (ts->super.ca);
  ccl_list *result = ccl_list_create ();
  
  for (i = 0; i < 2; i++)
    {
      for (p = FIRST (vars); p; p = CDR (p))
	{
	  ar_ca_expr *var = ar_ca_expr_add_reference (CAR (p));
	  if (i == 0)
	    var = CCL_BITPTR (ar_ca_expr *, var);
	  ccl_list_add (result, var);
	}
      vars = ar_ca_get_state_variables (ts->super.ca);
    }
  ccl_list_sort_with_data (result, s_cmp_vars, ts->super.order);

  return result;
}

			/* --------------- */

static void
s_init_variables (ar_ts *ts)
{  
  int i;
  ccl_list *vars = s_sorted_variables (ts);

  ccl_array_init_with_size (ts->variables, ccl_list_get_size (vars));
  ts->is_state = ccl_new_array (int, ccl_list_get_size (vars));
  i = 0;
  while (! ccl_list_is_empty (vars))
    {
      ar_ca_expr *var = ccl_list_take_first (vars);
      ts->is_state[i] = CCL_PTRHASBIT (var);
      ts->variables.data[i] = CCL_BITPTR2PTR (ar_ca_expr *, var);
      i++;
    }
  ccl_list_delete (vars);

  ts->min_values = ccl_new_array (int, ts->variables.size);

  for (i = 0; i < ts->variables.size; i++)
    {
      ar_bounds b;
      const ar_ca_domain *dom = 
	ar_ca_expr_variable_get_domain (ts->variables.data[i]);

      ar_ca_domain_get_bounds (dom,&b);

      ccl_assert (b.max - b.min + 1 <= 256);

      ts->min_values[i] = b.min;
    }
}

			/* --------------- */

static ar_expr *
s_state_to_formula (ccl_list *vars, int *assignment, ar_ca_exprman *man)
{
  ccl_pair *p;
  ar_expr *result = NULL;
  
  for(p = FIRST(vars); p; p = CDR(p), assignment++)
    {
      ar_expr *var = (ar_expr *)CAR(p);
      ar_type *dom = ar_expr_get_type(var);
      ar_expr *val = ar_sgs_get_ith_value_for_type (dom, *assignment, man);
      ar_expr *eq = ar_expr_crt_binary(AR_EXPR_EQ,var,val);

      if( result == NULL )
	result = ar_expr_add_reference(eq);
      else
	{
	  ar_expr *tmp = ar_expr_crt_binary(AR_EXPR_AND,eq,result);
	  ar_expr_del_reference(result);
	  result = tmp;
	}
      ar_expr_del_reference(val);
      ar_expr_del_reference(eq);
      ar_type_del_reference(dom);
    }

  return result;
}

			/* --------------- */

static int
s_ts_sets_are_equal (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  ar_ts_mark *m1 = TS_MARK (S1);
  ar_ts_mark *m2 = TS_MARK (S2);

  return ar_ts_mark_are_equal (m1, m2);
}

			/* --------------- */

static ccl_hash *
s_ts_get_events (ar_sgs *sgs, ar_sgs_set *T)
{
  int i;
  ar_ts *ts = TS (sgs);
  ar_ts_mark *m = TS_MARK (T);
  ccl_hash *result = 
    ccl_hash_create (NULL, NULL, NULL, 
		     (ccl_delete_proc *) ar_ca_event_del_reference);

  for (i = ar_ts_mark_get_first (m); i>= 0; i = ar_ts_mark_get_next (m, i))
    {
      ar_ca_event *e = ar_ts_trans_get_event (ts, i);
      if (! ccl_hash_find (result, e))
	ccl_hash_insert (result, e);
      else
	ar_ca_event_del_reference (e);      
    }

  return result;
}

			/* --------------- */

static ar_expr *
s_ts_states_to_formula (ar_sgs *sgs, ar_sgs_set *S, ccl_list *pvars, 
			ccl_list *vars, int exist)
{
  int *values = ccl_new_array (int, ccl_list_get_length (pvars) + 1);
  ar_expr *result = ar_expr_crt_false ();
  ar_ts *ts = TS (sgs);
  ar_ts_mark *m = TS_MARK (S);
  int i = ar_ts_mark_get_first (m);
  ar_ca_exprman *man = ar_sgs_get_expr_manager (sgs);
  ar_expr_op op = exist ? AR_EXPR_OR : AR_EXPR_AND;

  while (i >= 0)
    {
      ar_expr *tmp[3];
      ar_ts_state_get_assignments (ts, i, pvars, values);

      tmp[0] = s_state_to_formula (vars, values, man);
      tmp[1] = ar_expr_crt_binary (op, result, tmp[0]);
      ar_expr_del_reference (result);
      ar_expr_del_reference (tmp[0]);
      result = tmp[1];

      i = ar_ts_mark_get_next (m, i);
    }
  ccl_delete (values);
  ar_ca_exprman_del_reference (man);

  return result;
}

			/* --------------- */

static void 
s_ts_display_set (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *set,
		  ar_sgs_display_format df)
{
  int i;
  ar_ts *ts = TS (sgs);
  ar_ts_mark *M = TS_MARK (set);
  void (*display) (ccl_log_type log, ar_ts *ts, int s, 
		   ar_sgs_display_format df);
  
  if (ar_sgs_set_is_state_set (set))
    display = ar_ts_display_state;
  else 
    display = ar_ts_display_trans;

  for (i = ar_ts_mark_get_first (M); i >= 0; 
       i = ar_ts_mark_get_next (M, i))
    {
      display (log, ts, i, df);
      if (df == AR_SGS_DISPLAY_DOT)
	ccl_log (log, "\\n");
      else 
	ccl_log (log, "\n");
    }
}

			/* --------------- */

static void
s_ts_destroy (ar_sgs *sgs)
{
  int i;
  ar_ts *ts = (ar_ts *) sgs;
  
  ccl_delete (ts->is_state);
  for(i = 0; i < ts->variables.size; i++)
    ar_ca_expr_del_reference (ts->variables.data[i]);
  ccl_array_delete (ts->variables);
  ccl_delete (ts->min_values);
  
  for (i = 0; i < ts->state_table.size; i++)
    ccl_delete (ts->state_table.data[i]);
  ccl_array_delete (ts->state_table);
  ccl_array_delete (ts->trans_table);
  ccl_delete (ts->tmp_s);
}

			/* --------------- */

static double 
s_ts_card (ar_sgs *sgs, ar_sgs_set *S)
{
  return (double) ar_ts_mark_get_size (TS_MARK (S));
}

			/* --------------- */

static void 
s_ts_to_mec4 (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	      ccl_config_table *conf)
{
  ar_ts_to_mec4 (log, TS(sgs), TS_MARK (S), TS_MARK (T), conf);
}

			/* --------------- */

static void
s_ts_to_dot (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf)
{
  ar_ts_to_dot (log, TS(sgs), TS_MARK (S), TS_MARK (T), conf);
}

			/* --------------- */

static void 
s_ts_to_gml (ar_sgs *sgs, ccl_log_type log, ar_sgs_set *S, ar_sgs_set *T,
	     ccl_config_table *conf)
{
  ar_ts_to_gml (log, TS (sgs), TS_MARK (S), TS_MARK (T), conf);
}

			/* --------------- */

static int
s_ts_is_empty (ar_sgs *sgs, ar_sgs_set *S)
{
  ar_ts_mark *s = TS_MARK (S);

  return ar_ts_mark_is_empty (s);
}

			/* --------------- */

static ar_sgs_set * 
s_ts_proj (ar_sgs *sgs, ar_sgs_set *X, int on_state)
{
  return NULL;
}

			/* --------------- */

static ar_sgs_set * 
s_ts_pick (ar_sgs *sgs, ar_sgs_set *X)
{
  ar_ts_mark *x = TS_MARK (X);
  ar_ts_mark *r = 
    ar_ts_mark_create (ar_sgs_set_is_state_set (X), ar_ts_mark_get_width (x));
  int i = ar_ts_mark_get_first (x);

  if (i >= 0)
    ar_ts_mark_add (r, i);

  return SGS_SET (r);
}

			/* --------------- */

static ar_sgs_set * 
s_ts_assert_config (ar_sgs *sgs, ar_sgs_set *S)
{
  return ar_sgs_set_add_reference (S);
}

			/* --------------- */
static ar_sgs_set * 
s_ts_src (ar_sgs *sgs, ar_sgs_set *T)
{
  return SGS_SET (ar_ts_compute_src (TS (sgs), TS_MARK (T)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_tgt (ar_sgs *sgs, ar_sgs_set *T)
{
  return SGS_SET (ar_ts_compute_tgt (TS (sgs), TS_MARK (T)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_rsrc (ar_sgs *sgs, ar_sgs_set *S)
{
  return SGS_SET (ar_ts_compute_rsrc (TS (sgs), TS_MARK (S)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_rtgt (ar_sgs *sgs, ar_sgs_set *S)
{
  return SGS_SET (ar_ts_compute_rtgt (TS (sgs), TS_MARK (S)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_loop (ar_sgs *sgs, ar_sgs_set *R1, ar_sgs_set *R2)
{
  return SGS_SET (ar_ts_compute_loop (TS (sgs), TS_MARK (R1), TS_MARK (R2)));
} 

			/* --------------- */

static ar_sgs_set * 
s_ts_trace (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *T, ar_sgs_set *S2)
{
  return SGS_SET (ar_ts_compute_trace (TS (sgs), TS_MARK (S1), TS_MARK (T), 
				       TS_MARK (S2)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_unav (ar_sgs *sgs, ar_sgs_set *T, ar_sgs_set *S)
{
  return SGS_SET (ar_ts_compute_unav (TS (sgs), TS_MARK (T), TS_MARK (S)));
}


			/* --------------- */

static ar_sgs_set * 
s_ts_reach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T)
{
  return SGS_SET (ar_ts_compute_reach (TS (sgs), TS_MARK (S), TS_MARK (T)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_coreach (ar_sgs *sgs, ar_sgs_set *S, ar_sgs_set *T)
{
  return SGS_SET (ar_ts_compute_coreach (TS (sgs), TS_MARK (S), TS_MARK (T)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_state_set (ar_sgs *sgs, ar_ca_expr *cond)
{
  return SGS_SET (ar_ts_compute_state_set (TS (sgs), cond));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_label (ar_sgs *sgs, ar_identifier *label)
{
  return SGS_SET (ar_ts_compute_label (TS (sgs), label));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_event_attribute (ar_sgs *sgs, ar_identifier *attribute)
{
  return SGS_SET (ar_ts_compute_event_attribute (TS (sgs), attribute));
}

			/* --------------- */

static ccl_list *
s_ts_classes (ar_sgs *sgs)
{
  return ar_ts_compute_classes (TS (sgs));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_lfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F)
{
  return SGS_SET (ar_ts_formula_compute_lfp (var, F, TS (sgs))); 
}

			/* --------------- */

static ar_sgs_set * 
s_ts_gfp (ar_sgs *sgs, ar_identifier *var, ar_sgs_formula *F)
{
  return SGS_SET (ar_ts_formula_compute_gfp (var, F, TS (sgs)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_set_complement (ar_sgs *sgs, ar_sgs_set *S)
{
  return SGS_SET (ar_ts_mark_complement (TS_MARK (S)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_set_union (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  return SGS_SET (ar_ts_mark_union (TS_MARK (S1), TS_MARK (S2)));
}

			/* --------------- */

static ar_sgs_set * 
s_ts_set_intersection (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  return SGS_SET (ar_ts_mark_intersection (TS_MARK (S1), TS_MARK (S2)));
}
			/* --------------- */

static ar_sgs_set * 
s_ts_set_difference (ar_sgs *sgs, ar_sgs_set *S1, ar_sgs_set *S2)
{
  return SGS_SET (ar_ts_mark_sub (TS_MARK (S1), TS_MARK (S2)));
}


			/* --------------- */

static int
s_trans_satisfies_assignments (ar_ts *ts, ts_trans *t, ar_ca_expr **assignments,
			       int nb_assignments)
{
  int i;

  for (i = 0; i < nb_assignments; i++, assignments += 2)
    {
      ar_ca_expr *v = assignments[0];
      ar_ca_expr *val = assignments[1];
      int pre_val = ar_ts_eval_expr_in_state (ts, val, t->src);	
      int post_val = ar_ts_eval_expr_in_state (ts, v, t->tgt);

      if (pre_val != post_val)
	return 0;
    }
  
  return 1;
}

static ar_sgs_set *
s_ts_assignment (ar_sgs *sgs, ar_ca_expr **assignments,  int nb_assignments)
{
  int i;
  ar_ts *ts = TS (sgs);
  ar_ts_mark *result = ar_ts_crt_mark (ts, 1, 0);
  ts_trans *t = ts->trans_table.data;
  
  for (i = 0; i < ts->nb_trans; i++, t++)
    {
      if (s_trans_satisfies_assignments (ts, t, assignments, nb_assignments))
	ar_ts_mark_add (result, i);
    }
  return SGS_SET (result);
}
