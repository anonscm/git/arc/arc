/*
 * ts-mark.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-bittable.h>
#include "ar-state-graph-semantics-p.h"
#include "ts-mark.h"

			/* --------------- */

struct ar_ts_mark_st {
  ar_sgs_set super;
  int width;
  ccl_bittable *elements;
};

			/* --------------- */

static void 
s_ts_mark_destroy (ar_sgs_set *set);

			/* --------------- */

static const struct ar_state_graph_set_methods_st TS_MARK_METHODS = 
{
  s_ts_mark_destroy
};

			/* --------------- */

static ar_ts_mark *
s_crt_mark(int is_state, int width, ccl_bittable *set)
{
  ar_ts_mark *result = (ar_ts_mark *) 
    ar_sgs_set_create (is_state, sizeof (ar_ts_mark), &TS_MARK_METHODS);

  result->width = width;
  result->elements = set;

  return result;
}

			/* --------------- */

ar_ts_mark *
ar_ts_mark_create (int is_state, int width)
{
  ccl_pre (width >= 0);

  return s_crt_mark (is_state, width, ccl_bittable_create (width));
}

			/* --------------- */

ar_ts_mark *
ar_ts_mark_add_reference (ar_ts_mark *mark)
{
  return (ar_ts_mark *) ar_sgs_set_add_reference ((ar_sgs_set *) mark);
}

			/* --------------- */


void
ar_ts_mark_del_reference(ar_ts_mark *mark)
{
  ar_sgs_set_del_reference ((ar_sgs_set *) mark);
}

			/* --------------- */

void
ar_ts_mark_add(ar_ts_mark *mark, int i)
{
  ccl_pre (mark != NULL); 
  ccl_pre (0 <= i && i < ar_ts_mark_get_width (mark));

  ccl_bittable_set (mark->elements, i);
}

			/* --------------- */

void
ar_ts_mark_remove (ar_ts_mark *mark, int i)
{
  ccl_pre (mark != NULL); 
  ccl_pre (0 <= i && i < ar_ts_mark_get_width (mark));

  ccl_bittable_unset (mark->elements, i);
}

			/* --------------- */

int
ar_ts_mark_has (ar_ts_mark *mark, int index)
{
  ccl_pre (mark != NULL); 
  ccl_pre (0 <= index && index < ar_ts_mark_get_width (mark));

  return ccl_bittable_has (mark->elements, index);
}

			/* --------------- */

int
ar_ts_mark_get_size (ar_ts_mark *mark)
{
  ccl_pre (mark != NULL);

  return ccl_bittable_get_nb_one (mark->elements);
}

			/* --------------- */

int
ar_ts_mark_get_width (ar_ts_mark *mark)
{
  ccl_pre (mark != NULL);

  return mark->width;
}

			/* --------------- */

void
ar_ts_mark_change_width (ar_ts_mark *mark, int width)
{
  ccl_bittable *new_table;

  ccl_pre (mark != NULL);

  new_table = ccl_bittable_resize (mark->elements, width);
  ccl_bittable_delete (mark->elements);
  mark->elements = new_table;
  mark->width = width;
}

			/* --------------- */

int
ar_ts_mark_get_first (ar_ts_mark *mark)
{
  ccl_pre (mark != NULL);

  return ccl_bittable_get_first (mark->elements);
}

			/* --------------- */

int
ar_ts_mark_get_next (ar_ts_mark *mark, int from)
{
  ccl_pre (mark != NULL);

  return ccl_bittable_get_next (mark->elements, from);
}

			/* --------------- */

ar_ts_mark *
ar_ts_mark_union (ar_ts_mark *m1, ar_ts_mark *m2)
{
  ccl_pre (m1 != NULL); ccl_pre (m2 != NULL);
  ccl_pre (m1->width == m2->width);

  return s_crt_mark (m1->super.is_state_set, m1->width, 
		     ccl_bittable_union (m1->elements, m2->elements));
}

			/* --------------- */

ar_ts_mark *
ar_ts_mark_complement (ar_ts_mark *m)
{
  ccl_pre (m != NULL); 

  return s_crt_mark (m->super.is_state_set, m->width, 
		     ccl_bittable_complement (m->elements));
}

			/* --------------- */

ar_ts_mark *
ar_ts_mark_intersection (ar_ts_mark *m1, ar_ts_mark *m2)
{
  ccl_pre (m1 != NULL); ccl_pre (m2 != NULL);
  ccl_pre (m1->width == m2->width);

  return s_crt_mark(m1->super.is_state_set, m1->width,
		    ccl_bittable_intersection (m1->elements, m2->elements));
}

			/* --------------- */

ar_ts_mark *
ar_ts_mark_sub (ar_ts_mark *m1, ar_ts_mark *m2)
{
  ccl_bittable *nm2;
  ar_ts_mark *result;
  
  ccl_pre (m1 != NULL); ccl_pre (m2 != NULL);
  ccl_pre (m1->width == m2->width);

  nm2 = ccl_bittable_complement (m2->elements);
  result = s_crt_mark(m1->super.is_state_set, m1->width, 
		      ccl_bittable_intersection (m1->elements, nm2));
  ccl_bittable_delete (nm2);

  return result;
}

			/* --------------- */

int
ar_ts_mark_are_equal (ar_ts_mark *m1, ar_ts_mark *m2)
{
  ccl_pre (m1 != NULL); 
  ccl_pre (m2 != NULL);
  ccl_pre (m1->width == m2->width);

  return ccl_bittable_equals (m1->elements, m2->elements);
}

static void 
s_ts_mark_destroy (ar_sgs_set *set)
{
  ar_ts_mark *mark = (ar_ts_mark *) set;

  ccl_bittable_delete (mark->elements);
}
