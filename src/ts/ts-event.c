/*
 * ts-event.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ts-p.h"

void
ar_ts_event_log (ccl_log_type log, ar_ts_event *event, int show_epsilon, 
		 int quote)
{
  ar_ca_event_log_gen (log, event, show_epsilon, ".", quote ? "''" : NULL);
}

			/* --------------- */

int
ar_ts_event_is_epsilon (ar_ts_event *event)
{
  ccl_pre (event != NULL);

  return ar_ca_event_is_epsilon (event);
}
