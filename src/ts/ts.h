/*
 * ts.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TS_H
# define TS_H

# include "ar-constraint-automaton.h"
# include <ts/ts-mark.h>


# define AR_TS_MAX_STATE_CARD 256

typedef struct ar_ts_st ar_ts;
typedef ar_ca_event ar_ts_event;

			/* --------------- */

extern ar_ts *
ar_ts_create(ar_ca *ca);

extern ar_ts *
ar_ts_add_reference(ar_ts *ts);

extern void
ar_ts_del_reference(ar_ts *ts);

extern ar_ca *
ar_ts_get_constraint_automaton(ar_ts *ts);

extern ar_node *
ar_ts_get_node(ar_ts *ts);

extern ar_ca_exprman *
ar_ts_get_expr_manager(ar_ts *ts);

extern ar_identifier *
ar_ts_get_name(ar_ts *ts);

extern int
ar_ts_get_nb_states(ar_ts *ts);

extern int
ar_ts_get_nb_trans(ar_ts *ts);

extern int
ar_ts_has_mark(ar_ts *ts, ar_identifier *name, int *is_state);

extern int
ar_ts_has_state_mark(ar_ts *ts, ar_identifier *name);

extern void
ar_ts_remove_state_mark(ar_ts *ts, ar_identifier *name);

extern int
ar_ts_has_trans_mark(ar_ts *ts, ar_identifier *name);

extern void
ar_ts_remove_trans_mark(ar_ts *ts, ar_identifier *name);

extern void
ar_ts_add_state_mark(ar_ts *ts, ar_identifier *name, ar_ts_mark *mark);

extern void
ar_ts_add_trans_mark(ar_ts *ts, ar_identifier *name, ar_ts_mark *mark);

extern ar_ts_mark *
ar_ts_get_state_mark(ar_ts *ts, ar_identifier *name);

extern ar_ts_mark *
ar_ts_get_trans_mark(ar_ts *ts, ar_identifier *name);

extern ar_identifier_iterator *
ar_ts_get_state_marks(ar_ts *ts);

extern ar_identifier_iterator *
ar_ts_get_trans_marks(ar_ts *ts);

extern void
ar_ts_log(ccl_log_type log, ar_ts *ts, ar_identifier *name);

			/* --------------- */

extern ar_ts_mark *
ar_ts_crt_mark(ar_ts *ts, int empty, int state);

# define ar_ts_empty_state_mark(ts) ar_ts_crt_mark(ts,1,1)
# define ar_ts_full_state_mark(ts)  ar_ts_crt_mark(ts,0,1)
# define ar_ts_empty_trans_mark(ts) ar_ts_crt_mark(ts,1,0)
# define ar_ts_full_trans_mark(ts)  ar_ts_crt_mark(ts,0,0)


			/* --------------- */

extern ccl_list *
ar_ts_state_get_properties(ar_ts *ts, int s);

extern char *
ar_ts_state_to_string(ar_ts *ts, int s);

extern void
ar_ts_state_log(ccl_log_type log, ar_ts *ts, int s);

extern int
ar_ts_state_get_first_out(ar_ts *ts, int s);

extern int
ar_ts_state_get_first_in(ar_ts *ts, int s);

extern void
ar_ts_state_get_assignments(ar_ts *ts, int s, ccl_list *vars, int *result);

extern int
ar_ts_state_get_in_degree (ar_ts *ts, int s);

extern int
ar_ts_state_get_out_degree (ar_ts *ts, int s);

			/* --------------- */

extern ccl_list *
ar_ts_trans_get_properties(ar_ts *ts, int t);

extern int
ar_ts_trans_get_src(ar_ts *ts, int t);

extern ar_ts_event *
ar_ts_trans_get_event(ar_ts *ts, int t);

extern int
ar_ts_trans_get_tgt(ar_ts *ts, int t);

extern int
ar_ts_trans_get_next_out(ar_ts *ts, int t);

extern int
ar_ts_trans_get_next_in(ar_ts *ts, int t);

extern void
ar_ts_trans_log(ccl_log_type log, ar_ts *ts, int t);

			/* --------------- */

# define ar_ts_event_del_reference ar_ca_event_del_reference
# define ar_ts_event_add_reference ar_ca_event_add_reference
# define ar_ts_event_equals ar_ca_event_equals

extern void
ar_ts_event_log(ccl_log_type log, ar_ts_event *event, int show_epsilon, 
		int quote);

extern int
ar_ts_event_is_epsilon (ar_ts_event *event);

#endif /* ! TS_H */
