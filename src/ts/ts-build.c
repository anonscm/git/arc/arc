/*
 * ts-build.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include "ar-stepper.h"
#include "ts-p.h"
#include "ts-algorithms.h"
#include "ar-acheck-builtins.h"

			/* --------------- */

# define HTABLE_INIT_SIZE   1001
# define HTABLE_FILL_DEGREE 5

			/* --------------- */

typedef struct state_cell_st {
  ts_state *state;
  struct state_cell_st *next;
} state_cell;

			/* --------------- */

typedef struct state_space_st {
  state_cell **table;
  int table_size;
  state_cell **cursor;
  int nb_states;
} state_space;

			/* --------------- */

typedef struct predefined_mark_st predefined_mark;
struct predefined_mark_st {
  ar_ts_mark *mark;
  const char *name;
  void (*compute)(ar_ts *ts, ar_ts_mark *mark, int item);
};

			/* --------------- */

#define AR_ACHECK_BUILTIN(_enumid, _strid) \
  { NULL, _strid, compute_ ## _enumid },

#define compute_ANY_C NULL
#define compute_VALID_STATE_ASSIGNMENTS NULL
#define compute_ANY_S ar_ts_compute_any_state
#define compute_EMPTY_S ar_ts_compute_empty_state
#define compute_INITIAL_STATES NULL

static predefined_mark PREDEF_STATE_MARKS[] = {
#define AR_ACHECK_NO_BUILTIN_TRANS_SET 1
#include "ar-acheck-builtins.def"
#undef AR_ACHECK_NO_BUILTIN_TRANS_SET 
  { NULL, NULL, NULL }
};

#define compute_ANY_TRANS NULL
#define compute_VALID_STATE_CHANGES NULL
#define compute_ANY_T ar_ts_compute_any_trans
#define compute_EMPTY_T ar_ts_compute_empty_trans
#define compute_EPSILON ar_ts_compute_epsilon 
#define compute_SELF ar_ts_compute_self
#define compute_SELF_EPSILON ar_ts_compute_self_epsilon
#define compute_NOT_DETERMINISTIC ar_ts_compute_not_deterministic 

static predefined_mark PREDEF_TRANS_MARKS[] = {
#define AR_ACHECK_NO_BUILTIN_STATE_SET 1
#include "ar-acheck-builtins.def"
#undef  AR_ACHECK_NO_BUILTIN_STATE_SET
  { NULL, NULL, NULL }
};
#undef AR_ACHECK_BUILTIN

#define NB_PREDEF_STATE_MARKS \
  ((sizeof(PREDEF_STATE_MARKS)/sizeof(PREDEF_STATE_MARKS[0]))-1)

#define NB_PREDEF_TRANS_MARKS \
  ((sizeof(PREDEF_TRANS_MARKS)/sizeof(PREDEF_TRANS_MARKS[0]))-1)

			/* --------------- */

static state_space *
s_state_space_create(void);

static void
s_state_space_delete(state_space *sp, ar_ts *ts);

static int
s_state_space_find(ar_ts *ts, state_space *ht, ts_state *s);

# define s_state_space_get(ht) ((*(ht)->cursor)->state)

static void
s_state_space_insert(ar_ts *ts, state_space *sp, ts_state *s);

static uint32_t
s_hash_state(const ts_state *s, int nb_vars);

static int 
s_cmp_states(const ts_state *s1, const ts_state *s2, int nb_vars);

			/* --------------- */

static void
s_init_predefined_marks(ar_ts *ts, int state_marks, predefined_mark *marks, 
			predefined_mark *refs);

static void
s_trim_tables(ar_ts *ts, state_space *sp, predefined_mark *s_marks, 
	      predefined_mark *t_marks);

static void
s_create_initial_mark(ar_ts *ts, ts_state **initial, int nb_initial);

static ts_state *
s_dfs_traversal(state_space *ht, ar_ts *ts, ar_stepper *stepper, 
		ar_stepper_state *S, predefined_mark *s_mark, 
		predefined_mark *t_mark);

static void
s_compute_predef_marks(ar_ts *ts, predefined_mark *predefmark, int newitem);

static void
s_stepper_state_to_ts_state(ar_ts *ts, ts_state *dst, ar_stepper_state *src);

static int
s_add_transition(ar_ts *ts, ts_state *src, ar_ts_event *e, ts_state *tgt);

			/* --------------- */

void
ar_ts_build(ar_ts *ts)
{
  ar_stepper_state_array states;
  state_space *sp = s_state_space_create();
  ts_state **initial = NULL;
  int nb_initial = 0;
  predefined_mark s_marks[NB_PREDEF_STATE_MARKS+1];
  predefined_mark t_marks[NB_PREDEF_TRANS_MARKS+1];
  ar_stepper *stepper = ar_stepper_create(ts->super.ca, ts->super.order);

  s_init_predefined_marks(ts,1,s_marks,PREDEF_STATE_MARKS);
  s_init_predefined_marks(ts,0,t_marks,PREDEF_TRANS_MARKS);

  states = ar_stepper_get_initial_states(stepper,AR_STEPPER_ALL);
  if( states.size != 0 )
    {
      int i;

      nb_initial = states.size;
      initial = ccl_new_array(ts_state *,nb_initial);

      for(i = 0; i < nb_initial; i++)
	{
	  initial[i] = 
	    s_dfs_traversal(sp,ts,stepper,states.data[i],s_marks,t_marks);
	  ar_stepper_state_delete(states.data[i]);
	}
    }
  ccl_array_delete(states);

  s_trim_tables(ts,sp,s_marks,t_marks);
  s_create_initial_mark(ts,initial,nb_initial);

  if( initial != NULL )
    ccl_delete(initial);

  ar_stepper_del_reference(stepper);
}

			/* --------------- */

static state_space *
s_state_space_create(void)
{
  state_space *result = ccl_new(state_space);

  result->table = ccl_new_array(state_cell *,HTABLE_INIT_SIZE);
  result->table_size = HTABLE_INIT_SIZE;
  result->cursor = NULL;
  result->nb_states = 0;

  return result;
}

			/* --------------- */

static void
s_state_space_delete(state_space *sp, ar_ts *ts)
{
  int i;

  for(i = 0; i < sp->table_size; i++)
    {
      if( sp->table[i] != NULL )
	{
	  state_cell *c;
	  state_cell *next;

	  for(c = sp->table[i]; c != NULL; c = next)
	    {
	      ts->state_table.data[c->state->index] = c->state;
	      next = c->next;
	      ccl_delete(c);
	    }
	}
    }

  ccl_delete(sp->table);
  ccl_delete(sp);
}

			/* --------------- */

static int
s_state_space_find(ar_ts *ts, state_space *sp, ts_state *s)
{
  uint32_t H = s_hash_state(s,ts->variables.size);
  state_cell **C;

  for(C = &sp->table[H%sp->table_size]; *C; C = &((*C)->next))
    {
      if( s_cmp_states((*C)->state,s,ts->variables.size) == 0 )
	break;
      
    }
  sp->cursor = C;

  return (*C != NULL);
}

			/* --------------- */

static void
s_state_space_resize(state_space *sp, size_t nb_vars)
{
  int    i,j;
  size_t newsize = (sp->table_size<<1)+1;
  state_cell **newtable = ccl_new_array(state_cell*,newsize);
  state_cell *p, *next;

  for(i=0; i<sp->table_size; i++)
    {
      for(p = sp->table[i]; p; p = next)
	{
	  next = p->next;
	  j = s_hash_state(p->state, nb_vars) % newsize;
	  p->next = newtable[j];
	  newtable[j] = p;
	}
    }

  ccl_delete(sp->table);
  sp->table = newtable;
  sp->table_size = newsize;
}

			/* --------------- */

static void
s_state_space_insert(ar_ts *ts, state_space *sp, ts_state *s)
{
  state_cell *nc;

  ccl_pre( sp->cursor != NULL );

  nc = ccl_new(state_cell);
  nc->state = s;
  nc->next  = *sp->cursor;
  *sp->cursor = nc;
  sp->nb_states++;
  if( sp->nb_states>= HTABLE_FILL_DEGREE*sp->table_size )
    s_state_space_resize(sp,ts->variables.size);
}

			/* --------------- */

static uint32_t
s_hash_state(const ts_state *s, int nb_vars)
{
  static uint32_t primes[] = { 1, 3, 7, 17, 37, 79, 163, 331, 673, 1361,
			       2729, 5471, 10949, 21911, 43853,  87719, 
			       175447, 350899, 701819, 1403641,
			       2807303, 5614657, 11229331 };
  static const int nb_primes = sizeof(primes)/sizeof(primes[0]);
  int i;
  uint32_t result = nb_vars;
  const int *p = s->body;

  for(i = 0; nb_vars--; p++, i = (i+1)%nb_primes)
    result = 19*result+primes[i]*(uint32_t)*p;

  return result;
}

			/* --------------- */

static int 
s_cmp_states(const ts_state *s1, const ts_state *s2, int nb_vars)
{
  if (s1 == s2) 
    return 0;

  return ccl_memcmp (s1->body, s2->body, sizeof (int) * nb_vars);
}

			/* --------------- */

static void
s_init_predefined_marks(ar_ts *ts, int state_marks, predefined_mark *marks, 
			predefined_mark *refs)
{
  for(; refs->name != NULL; refs++, marks++)
    {
      *marks = *refs;
      if (refs->compute != NULL)
	{
	  ar_identifier *id = ar_identifier_create(refs->name);
	  marks->mark = ar_ts_mark_create(state_marks, 1000);
	  if( state_marks ) ar_ts_add_state_mark(ts,id,marks->mark);
	  else ar_ts_add_trans_mark(ts,id,marks->mark);
	  ar_identifier_del_reference(id);
	}
    }

  ccl_memzero(marks, sizeof(predefined_mark));
}

			/* --------------- */

static void
s_trim_tables(ar_ts *ts, state_space *sp, predefined_mark *s_marks, 
	      predefined_mark *t_marks)
{
  size_t i;

  ccl_array_trim(ts->trans_table,ts->nb_trans);
  ccl_array_trim(ts->state_table,sp->nb_states);
  ts->nb_states = sp->nb_states;
  s_state_space_delete(sp,ts);
      
  for(i = 0; i < NB_PREDEF_STATE_MARKS; i++)
    {
      if (s_marks[i].mark != NULL)
	{
	  ar_ts_mark_change_width(s_marks[i].mark,ts->nb_states);
	  ar_ts_mark_del_reference(s_marks[i].mark);
	}
    }

  for(i = 0; i < NB_PREDEF_TRANS_MARKS; i++)
    {
      if (t_marks[i].mark != NULL)
	{
	  ar_ts_mark_change_width(t_marks[i].mark,ts->nb_trans);
	  ar_ts_mark_del_reference(t_marks[i].mark);
	}
    }
}

			/* --------------- */

static void
s_create_initial_mark(ar_ts *ts, ts_state **initial, int nb_initial)
{
  int i;
  const char *sinitial = AR_ACHECK_BUILTIN_NAMES[AR_ACHECK_INITIAL_STATES];
  ar_identifier *iname = ar_identifier_create (sinitial);
  ar_ts_mark *init_mark = 
    ar_ts_mark_create (AR_SGS_SET_IS_STATE, ts->nb_states);

  for(i = 0; i < nb_initial; i++)
    ar_ts_mark_add (init_mark, initial[i]->index);

  ar_ts_add_state_mark (ts, iname, init_mark);
  ar_identifier_del_reference (iname);
  ar_ts_mark_del_reference (init_mark);
}

			/* --------------- */

static ts_state *
s_dfs_traversal(state_space *sp, ar_ts *ts, ar_stepper *stepper, 
		ar_stepper_state *s, predefined_mark *s_marks, 
		predefined_mark *t_marks)
{
  int i;
  ar_stepper_trans_array transitions;
  int tindex;
  ts_state *src;

  ccl_pre( s != NULL );

  s_stepper_state_to_ts_state(ts,ts->tmp_s,s);

  if( s_state_space_find(ts,sp,ts->tmp_s) )
    return s_state_space_get(sp);

  
  src = ar_ts_dup_state (ts, ts->tmp_s);  
  src->index = sp->nb_states;

  ar_stepper_choice_point(stepper);
  ar_stepper_set_state(stepper,s,1);
  s_compute_predef_marks(ts,s_marks,src->index);
  s_state_space_insert(ts,sp,src);

  transitions = ar_stepper_get_valid_transitions_prepost(stepper);
  
  for(tindex = 0; tindex < transitions.size; tindex++)
    {
      ar_ca_trans *T = transitions.data[tindex];
      ar_stepper_state_array succ = 
	ar_stepper_trigger_transition(stepper,T,AR_STEPPER_ALL);

      if( succ.size != 0 )
	{
	  for(i = 0; i < succ.size; i++)
	    {
	      ar_ca_event *e = ar_ca_trans_get_event(T);
	      ts_state *tgt = s_dfs_traversal(sp,ts,stepper,succ.data[i],
					      s_marks,t_marks);
	      int t = s_add_transition(ts,src,e,tgt);
	      s_compute_predef_marks(ts,t_marks,t);

	      ar_ca_event_del_reference(e);
	      ar_stepper_state_delete(succ.data[i]);
	    }
	}
      ar_ca_trans_del_reference(T);
      ccl_array_delete(succ);
    }

  ccl_array_delete(transitions);
  ar_stepper_restore_last_choice_point(stepper);

  return src;
}

			/* --------------- */

static void
s_compute_predef_marks (ar_ts *ts, predefined_mark *predefmark, int newitem)
{
  predefined_mark *pm;

  for(pm = predefmark; pm->name; pm++)
    {
      if (pm->compute == NULL)
	continue;

      if (ar_ts_mark_get_width (pm->mark) <= newitem)
	ar_ts_mark_change_width (pm->mark, newitem + 1000);
      pm->compute (ts, pm->mark, newitem);
    }
}

			/* --------------- */

static void
s_stepper_state_to_ts_state(ar_ts *ts, ts_state *dst, ar_stepper_state *src)
{
  int i, len = ar_stepper_state_get_width(src);
  int min, max;

  for(i = 0; i < len; i++) 
    {
      int index = varorder_get_index (ts->super.order, ts->variables.data[i]);
      ar_stepper_state_get_range(src,index,&min,&max);
      ccl_assert( min == max );
      dst->body[i] = min-ts->min_values[i];
    }
  dst->first_out = -1;
  dst->first_in = -1;
}

			/* --------------- */

static int
s_add_transition(ar_ts *ts, ts_state *src, ar_ts_event *e, ts_state *tgt)
{
  ts_trans *T;

  if( ts->nb_trans == ts->trans_table.size )
    ccl_array_ensure_size(ts->trans_table,2*ts->trans_table.size+1);

  T = ts->trans_table.data+ts->nb_trans;  
  T->src = src;
  T->label = e;
  T->tgt = tgt;

  T->next_out = src->first_out;
  src->first_out = ts->nb_trans;

  T->next_in = tgt->first_in;
  tgt->first_in = ts->nb_trans;

  return ts->nb_trans++;
}



