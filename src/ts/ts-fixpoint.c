/*
 * ts-fixpoint.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-bittable.h>
#include <ccl/ccl-stack.h>
#include <ccl/ccl-list.h>
#include <ccl/ccl-log.h>
#include "ts-mark.h"
#include "ts.h"
#include "ar-state-graph-semantics-p.h"

typedef struct fp_variable_st fp_variable;
typedef struct fp_equation_st fp_equation;

#define FP_FLAG_POSITIVE   (1<<0)
#define FP_FLAG_ONSTACK    (1<<1)
#define FP_FLAG_IS_STATE   (1<<2)

struct fp_variable_st
{
  fp_equation *eq;
  uint32_t flags;  
  ccl_list *dependencies;
  int *counters;
  ar_identifier *id;
  ar_ts_mark *mark;  
  ccl_bittable *todo;
  ccl_bittable *terminated;
  fp_variable *next;  
};

struct fp_equation_st
{
  fp_variable *var;
  ar_sgs_formula_type type;
  fp_variable *vars[2];
  ar_ts_mark *cst;
  fp_equation *next;
};


			/* --------------- */


static ar_ts_mark *
s_compute_fixpoint (int lfp, ar_identifier *var, ar_sgs_formula *F, ar_ts *ts);

static fp_variable *
s_expand_equation_rec (ar_identifier *id, int positive, ar_sgs_formula *F, 
		       fp_variable **p_vars, fp_equation **p_equations);

static void
s_delete_variables_and_equations (fp_variable *vars, fp_equation *eqs);

static void
s_initialize_variables (fp_variable *vars, ar_ts *ts, ccl_stack *todo);

static void
s_set_dependencies (fp_equation *equations);

static void
s_push_on_stack (ccl_stack *todo, int z, fp_variable *Z);

			/* --------------- */

ar_ts_mark *
ar_ts_formula_compute_lfp (ar_identifier *var, ar_sgs_formula *F, ar_ts *ts)
{
  return s_compute_fixpoint (1, var, F, ts);
}

			/* --------------- */

ar_ts_mark *
ar_ts_formula_compute_gfp (ar_identifier *var, ar_sgs_formula *F, ar_ts *ts)
{
  return s_compute_fixpoint (0, var, F, ts);
}

			/* --------------- */

static ar_ts_mark *
s_compute_fixpoint (int lfp, ar_identifier *var, ar_sgs_formula *F, ar_ts *ts)
{
  ar_ts_mark *result;
  fp_variable *variables = NULL;
  fp_equation *equations = NULL;
  fp_variable *topvar = 
    s_expand_equation_rec (var, lfp, F, &variables, &equations);
  ccl_stack *todo = ccl_stack_create ();

  s_initialize_variables (variables, ts, todo);
  s_set_dependencies (equations);

  result = ar_ts_mark_add_reference (topvar->mark);
  
  while (!ccl_stack_is_empty (todo))
    {
      int z;
      fp_variable *v = (fp_variable *) ccl_stack_pop (todo);

      v->flags &= ~FP_FLAG_ONSTACK;
      for (z = ccl_bittable_get_first (v->todo); z >= 0;
	   z = ccl_bittable_get_next(v->todo, z))
	{
	  ccl_pair *p;

	  if (ar_ts_mark_has (v->mark, z))
	    ar_ts_mark_remove (v->mark, z);
	  else
	    ar_ts_mark_add (v->mark, z);

	  ccl_bittable_set (v->terminated, z);

	  for (p = FIRST (v->dependencies); p; p = CDR (p))
	    {
	      fp_equation *e = (fp_equation *) CAR(p);
	      fp_variable *vv = e->var;

	      switch (e->type)
		{
		case AR_SGS_F_OR: case AR_SGS_F_AND: case AR_SGS_F_NOT:
		  if (vv->counters == NULL)
		    {
		      if (! ccl_bittable_has (vv->terminated, z))
			s_push_on_stack (todo, z, vv);
		    }
		  else 
		    {
		      vv->counters[z]--;
		      if (vv->counters[z] == 0)
			s_push_on_stack (todo, z, vv);
		    }
		  break;

		case AR_SGS_F_SRC: case AR_SGS_F_TGT:
		  {
		    int s;

		    if (e->type == AR_SGS_F_SRC)
		      s = ar_ts_trans_get_src (ts, z);
		    else
		      s = ar_ts_trans_get_tgt (ts, z);
		    
		    if ((vv->flags & FP_FLAG_POSITIVE) == 0)
		      {
			ccl_assert (vv->counters != NULL);
			vv->counters[s]--;
			if (vv->counters[s] == 0)
			  s_push_on_stack (todo, s, vv);
		      }
		    else if (! ccl_bittable_has (vv->terminated, s))
		      {
			s_push_on_stack (todo, s, vv);
		      }
		  }
		  break;

		case AR_SGS_F_RSRC: case AR_SGS_F_RTGT:
		  {
		    int t;
		    
		    if (e->type == AR_SGS_F_RSRC)
		      t = ar_ts_state_get_first_out (ts, z);
		    else
		      t = ar_ts_state_get_first_in (ts, z);

		    while (t >= 0)
		      {
			s_push_on_stack (todo, t, vv);
			if (e->type == AR_SGS_F_RSRC)
			  t = ar_ts_trans_get_next_out (ts, t);
			else
			  t = ar_ts_trans_get_next_in (ts, t);
		      }
		  }
		  break;

		default:
		  ccl_throw (internal_error, "bad TS formula type");
		}
	    }
	}
      ccl_bittable_clear (v->todo);
    }

  s_delete_variables_and_equations (variables, equations);
  ccl_stack_delete (todo);

  return result;
}

			/* --------------- */

static fp_variable *
s_expand_equation_rec (ar_identifier *id, int positive, ar_sgs_formula *F, 
		       fp_variable **p_vars, fp_equation **p_equations)
{
  fp_variable *result;

  if (F->type == AR_SGS_F_VAR)
    {
      ccl_assert (id == NULL);

      for (result = *p_vars; result && result->id != F->ident; 
	   result = result->next)
	/* do nothing */ ;
      ccl_assert (result != NULL);
      ccl_assert ( (positive && (result->flags & FP_FLAG_POSITIVE)) ||
		   (!positive && !(result->flags & FP_FLAG_POSITIVE)) );
    }
  else
    {
      result = ccl_new (fp_variable);
      result->eq = ccl_new (fp_equation);
      if (positive)
	result->flags |= FP_FLAG_POSITIVE;
      ccl_assert (F->is_state != AR_SGS_UNDEF_SET_KIND);
      if (F->is_state)
	result->flags |= FP_FLAG_IS_STATE;
      result->dependencies = NULL;
      result->counters = NULL;
      result->id = id;
      result->mark = NULL;
      result->todo = NULL;
      result->next = *p_vars;
      *p_vars = result;
      
      result->eq->var = result;
      result->eq->type = F->type;
      result->eq->next = *p_equations;
      *p_equations = result->eq;

      if (F->type == AR_SGS_F_CST)
	result->eq->cst = ar_ts_mark_add_reference ((ar_ts_mark *) F->set);
      else 
	{      
	  if (F->type == AR_SGS_F_NOT)
	    result->eq->vars[0] = 
	      s_expand_equation_rec (NULL, ! positive, F->args[0], p_vars, 
				     p_equations);

	  else
	    result->eq->vars[0] = 
	      s_expand_equation_rec (NULL, positive, F->args[0], p_vars, 
				     p_equations);
	  

	  if (F->type == AR_SGS_F_OR || F->type == AR_SGS_F_AND)
	    result->eq->vars[1] = 
	      s_expand_equation_rec (NULL, positive, F->args[1], p_vars, 
				     p_equations);
	}
    }

  return result;
}

			/* --------------- */

static void
s_delete_variables_and_equations (fp_variable *vars, fp_equation *eqs)
{
  fp_variable *next_v;
  fp_equation *next_e;
    
  for (; vars; vars = next_v)
    {
      next_v = vars->next;
      ccl_zdelete (ccl_list_delete, vars->dependencies);
      ccl_zdelete (ccl_delete, vars->counters);
      ccl_zdelete (ar_ts_mark_del_reference, vars->mark);
      ccl_zdelete (ccl_bittable_delete, vars->todo);
      ccl_zdelete (ccl_bittable_delete, vars->terminated);
      ccl_delete (vars);
    }

  for (; eqs; eqs = next_e)
    {
      next_e = eqs->next;
      ccl_zdelete (ar_ts_mark_del_reference, eqs->cst);
      ccl_delete (eqs);
    }
}

			/* --------------- */

static void
s_initialize_variables (fp_variable *vars, ar_ts *ts, ccl_stack *todo)
{
  fp_variable *v;
  int nb_states = ar_ts_get_nb_states (ts);
  int nb_trans = ar_ts_get_nb_trans (ts);

  for (v = vars; v; v = v->next)
    {
      int i;
      int is_state = (v->flags & FP_FLAG_IS_STATE) != 0;
      int is_positive = (v->flags & FP_FLAG_POSITIVE) != 0;
      int card = is_state ? nb_states : nb_trans;
      ar_sgs_formula_type type = v->eq->type;

      v->mark = ar_ts_crt_mark (ts, is_positive, is_state);
      v->terminated = ccl_bittable_create (card);
      v->dependencies = ccl_list_create ();

      if ((type == AR_SGS_F_AND && is_positive) ||
	  ((type == AR_SGS_F_SRC || type == AR_SGS_F_TGT || 
	    type == AR_SGS_F_OR) && !is_positive))
	{
	  int i;
	  
	  v->counters = ccl_new_array (int, card);
	  for (i = 0; i < card; i++)
	    {
	      int cval;

	      if (type == AR_SGS_F_OR || type == AR_SGS_F_AND)
		cval = 2;
	      else 
		{
		  if (type == AR_SGS_F_SRC)
		    cval = ar_ts_state_get_out_degree (ts, i);
		  else
		    cval = ar_ts_state_get_in_degree (ts, i);
		  if (cval == 0)
		    ar_ts_mark_remove (v->mark, i);
		}

	      v->counters[i] = cval;
	    }
	}

      v->todo = ccl_bittable_create (card);

      if (type == AR_SGS_F_CST || v->counters != NULL)
	{
	  int at_least_one = 0;

	  if (type == AR_SGS_F_CST)
	    {
	      for (i = 0; i < card; i++)
		{
		  if ((is_positive && ar_ts_mark_has (v->eq->cst, i)) ||
		      (!is_positive && !ar_ts_mark_has (v->eq->cst, i)))
		    {
		      ccl_bittable_set (v->todo, i);
		      at_least_one = 1;
		    }
		}
	    }
	  else if (v->counters != NULL)
	    {
	      for (i = 0; i < card; i++)
		if (v->counters[i] == 0)
		  {
		    ccl_bittable_set (v->todo, i);
		    at_least_one = 1;
		  }
	    }
	  
	  if (at_least_one)
	    {
	      ccl_stack_push (todo, v);
	      v->flags |= FP_FLAG_ONSTACK;
	    }
	}
    }
}

			/* --------------- */

static void
s_set_dependencies (fp_equation *equations)
{
  fp_equation *e;

  for(e = equations; e; e = e->next)
    {
      int i;
      for (i = 0; i < 2; i++)
	{
	  if (e->vars[i] != NULL && 
	      !ccl_list_has (e->vars[i]->dependencies,e))
	    ccl_list_add (e->vars[i]->dependencies,e);
	}
    }
}

			/* --------------- */

static void
s_push_on_stack (ccl_stack *todo, int z, fp_variable *Z)
{
  ccl_bittable_set (Z->todo, z);
  if ((Z->flags & FP_FLAG_ONSTACK) == 0)
    {
      Z->flags |= FP_FLAG_ONSTACK;
      ccl_stack_push (todo, Z);
    }
}

			/* --------------- */
#if 0
static void
s_display_varname (ccl_log_type log, fp_variable *var)
{
  if (var->id == NULL)
    ccl_log(log, "%p", var);
  else
    ar_identifier_log (log, var->id);
}

			/* --------------- */

static void
s_display_variables (ccl_log_type log, fp_variable *vars)
{
  for (; vars; vars = vars->next)
    {
      s_display_varname (log, vars);

      if ( (vars->flags & FP_FLAG_POSITIVE) )
	ccl_log (log, " += ");
      else
	ccl_log (log, " -= ");
      
      if (vars->eq->type == AR_SGS_F_CST)
	{
	  int i;

	  if (ar_ts_mark_get_width (vars->eq->cst) == 
	      ar_ts_mark_get_size (vars->eq->cst))
	    ccl_log (log, "any");
	  else
	    {
	      ccl_log (log, "{ ");
	      for (i = ar_ts_mark_get_first (vars->eq->cst); i >= 0;
		   i = ar_ts_mark_get_next (vars->eq->cst, i))
		{
		  ccl_log (log, "%d", i);
		  if (ar_ts_mark_get_next (vars->eq->cst, i) >=0)
		    ccl_log (log, ", ");
		}
	      ccl_log (log, " }");
	    }
	}
      else 
	{
	  switch (vars->eq->type)
	    {
	    case AR_SGS_F_SRC: ccl_log (log, "src ("); break;
	    case AR_SGS_F_TGT: ccl_log (log, "tgt ("); break;
	    case AR_SGS_F_RSRC: ccl_log (log, "rsrc ("); break;
	    case AR_SGS_F_RTGT: ccl_log (log, "rtgt ("); break;
	    case AR_SGS_F_NOT: ccl_log (log, "not ("); break;
	    default: ccl_log (log, "("); break;
	    }

	  s_display_varname (log, vars->eq->vars[0]);

	  if (vars->eq->vars[1] != NULL)
	    {
	      if (vars->eq->type == AR_SGS_F_OR) ccl_log (log, " || ");
	      else ccl_log (log, " && ");
	      s_display_varname (log, vars->eq->vars[1]);
	    }
	  ccl_log (log, ")");
	}
      ccl_log (log, "\t");
	{
	  int i;

	  if (ar_ts_mark_get_width (vars->mark) == 
	      ar_ts_mark_get_size (vars->mark))
	    ccl_log (log, "any\n");
	  else
	    {
	      ccl_log (log, "{ ");
	      for (i = ar_ts_mark_get_first (vars->mark); i >= 0;
		   i = ar_ts_mark_get_next (vars->mark, i))
		{
		  ccl_log (log, "%d", i);
		  if (ar_ts_mark_get_next (vars->mark, i) >=0)
		    ccl_log (log, ", ");
		}
	      ccl_log (log, " }\n");
	    }
	}
    }
}
#endif

