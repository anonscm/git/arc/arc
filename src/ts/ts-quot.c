/*
 * ts-quot.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-stack.h>
#include "ts-mark.h"
#include "ts-algorithms.h"

			/* --------------- */

static int
s_property_list_equals(ccl_list *l1, ccl_list *l2)
{
  ccl_pair *p;

  for(p = FIRST(l1); p; p = CDR(p))
    if( ! ccl_list_has(l2,CAR(p)) )
      return 0;

  for(p= FIRST(l2); p; p = CDR(p))
    if( ! ccl_list_has(l1,CAR(p)) )
      return 0;
  return 1;
}

			/* --------------- */

static int
s_state_has_eqtrans_out(int s, ar_ts_event *e, int tgt, ar_ts *ts, 
			void **classes)
{
  int t;
  int result = 0;

  for(t = ar_ts_state_get_first_out(ts,s); t >= 0 && ! result;
      t = ar_ts_trans_get_next_out(ts,t))
    {
      ar_ts_event *e2 = ar_ts_trans_get_event(ts,t);
      int tgt2 = ar_ts_trans_get_tgt(ts,t);
      
      result = (ar_ts_event_equals(e,e2) && classes[tgt] == classes[tgt2]);
      ar_ts_event_del_reference(e2);
    }

  return result;
}

			/* --------------- */

static int
s_state_are_equivalents(int s1, int s2, ar_ts *ts, void **classes)
{
  int t1, t2;
  ccl_list *p1 = ar_ts_state_get_properties(ts,s1);
  ccl_list *p2 = ar_ts_state_get_properties(ts,s2);
  int result = s_property_list_equals(p1,p2);
  ccl_list_clear_and_delete(p1,(ccl_delete_proc *)ar_identifier_del_reference);
  ccl_list_clear_and_delete(p2,(ccl_delete_proc *)ar_identifier_del_reference);

  for(t1 = ar_ts_state_get_first_out(ts,s1); t1 >= 0 && result;
      t1 = ar_ts_trans_get_next_out(ts,t1))
    {
      ar_ts_event *e = ar_ts_trans_get_event(ts,t1);
      int tgt = ar_ts_trans_get_tgt(ts,t1);

      result = s_state_has_eqtrans_out(s2,e,tgt,ts,classes);
      ar_ts_event_del_reference(e);
    }

  for(t2 = ar_ts_state_get_first_out(ts,s2); t2 >= 0 && result;
      t2 = ar_ts_trans_get_next_out(ts,t2))
    {
      ar_ts_event *e = ar_ts_trans_get_event(ts,t2);
      int tgt = ar_ts_trans_get_tgt(ts,t2);

      result = s_state_has_eqtrans_out(s1,e,tgt,ts,classes);
      ar_ts_event_del_reference(e);
    }

  return result;
}

			/* --------------- */

static int
s_split_class(ar_ts_mark *M, ar_ts *ts, ccl_stack *stack, void **classes)
{
  int first, next, result;
  ar_ts_mark *split;

  ccl_pre( ar_ts_mark_get_size(M) > 0 );

  next = first = ar_ts_mark_get_first(M);
  result = 0;
  split = NULL;

  while( (next = ar_ts_mark_get_next(M,next)) >= 0 )
    {
      ccl_assert( classes[first] == classes[next] );

      if( s_state_are_equivalents(first,next,ts,classes) )
	continue;

      if( split == NULL )
	{
	  split = ar_ts_empty_state_mark(ts);
	  ccl_stack_push(stack,split);
	}
      ar_ts_mark_add(split,next);
    }

  if( split != NULL )
    {
      int            i;
      ar_ts_mark *tmp = ar_ts_mark_sub(M,split);

      ccl_stack_push(stack,tmp);

      for(i = ar_ts_mark_get_first(tmp); i >= 0;
	  i = ar_ts_mark_get_next(tmp,i))
	classes[i] = tmp;

      for(i = ar_ts_mark_get_first(split); i >= 0;
	  i = ar_ts_mark_get_next(split,i))
	classes[i] = split;

      result = 1;
    }

  return result;
}

			/* --------------- */

ccl_list *
ar_ts_compute_classes(ar_ts *ts)
{
  int nb_classes = 1;
  int i;
  void **classes_by_state;
  ccl_stack *class_stack = ccl_stack_create();
  ccl_stack *class_stack2 = ccl_stack_create();
  ar_ts_mark *current = ar_ts_full_state_mark(ts);
  ccl_list *classes = ccl_list_create();
  
  ccl_stack_push(class_stack2,ar_ts_mark_add_reference(current));
  classes_by_state = ccl_new_array(void *,ar_ts_get_nb_states(ts));
  for(i=0; i<ar_ts_get_nb_states(ts); i++)
    classes_by_state[i] = current;
  ar_ts_mark_del_reference(current);

  for(;;)
    {
      nb_classes = ccl_stack_get_size (class_stack2);
      while( ! ccl_stack_is_empty(class_stack2) )
	{
	  void *c = ccl_stack_pop(class_stack2);
	  ccl_stack_push(class_stack,c);
	}

      while( ! ccl_stack_is_empty(class_stack) )
	{
	  current = (ar_ts_mark *)ccl_stack_pop(class_stack);
	  
	  if( ! s_split_class(current,ts,class_stack,classes_by_state) )
	    {
	      ccl_stack_push(class_stack2,ar_ts_mark_add_reference(current));
	    }
	  ar_ts_mark_del_reference(current);
	}

      if( ccl_stack_get_size(class_stack2) == nb_classes )
	{
	  while( ! ccl_stack_is_empty(class_stack2) )
	    {
	      void *o = ccl_stack_pop(class_stack2);
	      ccl_list_add(classes,o);
	    }
	  break;
	}
    }

  ccl_stack_delete(class_stack);
  ccl_stack_delete(class_stack2);

  ccl_delete(classes_by_state);

  return classes;
}
