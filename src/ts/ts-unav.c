/*
 * ts-unav.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include "ts-p.h"
#include "ts-algorithms.h"

ar_ts_mark *
ar_ts_compute_unav(ar_ts *ts, ar_ts_mark *t, ar_ts_mark *s)
{
  int cont = 1;
  ar_ts_mark *z1;
  ar_ts_mark *z2;
  ar_ts_mark *t1;
  ar_ts_mark *t2;
  ar_ts_mark *Z;
  ar_ts_mark *T;
  int sZ, sT;

  ccl_pre( ts != NULL ); ccl_pre( t != NULL ); ccl_pre( s != NULL );

  Z = ar_ts_empty_state_mark (ts);
  sZ = ar_ts_mark_get_size (Z);
  T = ar_ts_full_trans_mark (ts);
  sT = ar_ts_mark_get_size (T);

  while (cont)
    {
      z1 = ar_ts_compute_src(ts,T);
      z2 = ar_ts_mark_complement(z1);
      ar_ts_mark_del_reference(z1);
      z1 = ar_ts_mark_union(s,z2);
      ar_ts_mark_del_reference(z2);

      t1 = ar_ts_mark_complement(Z);
      t2 = ar_ts_compute_rtgt(ts,t1);
      ar_ts_mark_del_reference(t1);
      t1 = ar_ts_mark_intersection(t,t2);
      ar_ts_mark_del_reference(t2);    

      ar_ts_mark_del_reference(Z);
      ar_ts_mark_del_reference(T);
      Z = z1;
      T = t1;
      cont = (sZ != ar_ts_mark_get_size(Z) || 
	      sT != ar_ts_mark_get_size(T));
      sZ = ar_ts_mark_get_size(Z);
      sT = ar_ts_mark_get_size(T);
    } 

  ar_ts_mark_del_reference(T);

  return Z;
}
