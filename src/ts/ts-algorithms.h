/*
 * ts-algorithms.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef TS_ALGORITHMS_H
# define TS_ALGORITHMS_H

# include <ts/ts.h>
# include "ar-state-graph-semantics.h"


			/* --------------- */

extern void
ar_ts_build(ar_ts *ts);

extern ar_ts_mark *
ar_ts_compute_src(ar_ts *ts, ar_ts_mark *mark);

extern ar_ts_mark *
ar_ts_compute_rsrc(ar_ts *ts, ar_ts_mark *mark);

extern ar_ts_mark *
ar_ts_compute_tgt(ar_ts *ts, ar_ts_mark *mark);

extern ar_ts_mark *
ar_ts_compute_rtgt(ar_ts *ts, ar_ts_mark *mark);

extern ar_ts_mark *
ar_ts_compute_loop(ar_ts *ts, ar_ts_mark *R1, ar_ts_mark *R2);

extern ar_ts_mark *
ar_ts_compute_trace(ar_ts *ts, ar_ts_mark *S1, ar_ts_mark *T, ar_ts_mark *S2);

extern ar_ts_mark *
ar_ts_compute_unav(ar_ts *ts, ar_ts_mark *T, ar_ts_mark *S);

extern ar_ts_mark *
ar_ts_compute_reach(ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T);

extern ar_ts_mark *
ar_ts_compute_coreach(ar_ts *ts, ar_ts_mark *S, ar_ts_mark *T);

extern ar_ts_mark *
ar_ts_compute_state_set(ar_ts *ts, ar_ca_expr *cond);

extern ar_ts_mark *
ar_ts_compute_label(ar_ts *ts, ar_identifier *label);

extern ar_ts_mark *
ar_ts_compute_event_attribute (ar_ts *ts, ar_identifier *attribute);

extern ccl_list *
ar_ts_compute_classes(ar_ts *ts);

extern void
ar_ts_compute_project(ccl_log_type log, ar_ts *ts, ar_identifier *subid,
		      ar_node *subnode, ar_identifier *pname, ar_ts_mark *S, 
		      ar_ts_mark *T, int simplify);

			/* --------------- */

extern void
ar_ts_compute_any_state(ar_ts *ts, ar_ts_mark *M, int state);

extern void
ar_ts_compute_empty_state(ar_ts *ts, ar_ts_mark *M, int state);

extern void
ar_ts_compute_any_trans(ar_ts *ts, ar_ts_mark *M, int trans);

extern void
ar_ts_compute_empty_trans(ar_ts *ts, ar_ts_mark *M, int trans);

extern void
ar_ts_compute_epsilon(ar_ts *ts, ar_ts_mark *M, int trans);

extern void
ar_ts_compute_self(ar_ts *ts, ar_ts_mark *M, int trans);

extern void
ar_ts_compute_self_epsilon(ar_ts *ts, ar_ts_mark *M, int trans);

extern void
ar_ts_compute_not_deterministic(ar_ts *ts, ar_ts_mark *M, int trans);

			/* --------------- */

extern ar_ts_mark *
ar_ts_formula_compute_lfp (ar_identifier *var, ar_sgs_formula *F, ar_ts *ts);

extern ar_ts_mark *
ar_ts_formula_compute_gfp (ar_identifier *var, ar_sgs_formula *F, ar_ts *ts);

#endif /* ! TS_ALGORITHMS_H */
