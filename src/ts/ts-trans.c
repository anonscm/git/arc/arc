/*
 * ts-trans.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-string.h>
#include "ts-p.h"

			/* --------------- */

ccl_list *
ar_ts_trans_get_properties(ar_ts *ts, int t)
{
  ccl_list *result = ccl_list_create();
  ar_identifier_iterator *i = ar_ts_get_trans_marks(ts);

  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) );

  while( ccl_iterator_has_more_elements(i) )
    {
      ar_identifier *id = ccl_iterator_next_element(i);
      ar_ts_mark *mark = ar_ts_get_trans_mark(ts,id);
      
      if( ar_ts_mark_has(mark,t) )
	ccl_list_add(result,id);
      else
	ar_identifier_del_reference(id);
      ar_ts_mark_del_reference(mark);
    }
  ccl_iterator_delete(i);

  return result;
}

			/* --------------- */


int
ar_ts_trans_get_src(ar_ts *ts, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) );

  return ts->trans_table.data[t].src->index;
}

			/* --------------- */
ar_ts_event *
ar_ts_trans_get_event(ar_ts *ts, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) );

  return ar_ts_event_add_reference(ts->trans_table.data[t].label);
}

			/* --------------- */
int
ar_ts_trans_get_tgt(ar_ts *ts, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) );

  return ts->trans_table.data[t].tgt->index;
}

			/* --------------- */

int
ar_ts_trans_get_next_out(ar_ts *ts, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) );

  return ts->trans_table.data[t].next_out;
}

			/* --------------- */

int
ar_ts_trans_get_next_in(ar_ts *ts, int t)
{
  ccl_pre( 0 <= t && t < ar_ts_get_nb_trans(ts) );

  return ts->trans_table.data[t].next_in;
}

			/* --------------- */

void
ar_ts_trans_log(ccl_log_type log, ar_ts *ts, int t)
{
  ts_trans *T = ts->trans_table.data+t;
  ar_ts_state_log(log,ts,T->src->index);
  ccl_log(log," |- ");
  ar_ts_event_log(log,T->label,1, 1);
  ccl_log(log," -> ");
  ar_ts_state_log(log,ts,T->tgt->index);
}
