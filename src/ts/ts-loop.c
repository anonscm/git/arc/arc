/*
 * ts-loop.c -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#include <ccl/ccl-assert.h>
#include <ccl/ccl-bittable.h>
#include "ts-p.h"
#include "ts-algorithms.h"

typedef struct loop_data_st {
  ar_ts *ts;
  ccl_bittable *visited;
  ccl_bittable *stacked;
  ccl_bittable *valid;
  ccl_bittable *loop;
  int *number;
  int *link;
  int n;
  int *stack;
  int top;
  ar_ts_mark *R1;
  ar_ts_mark *R2;
} loop_data; 

			/* --------------- */

static void
s_compute_loop(loop_data *data, int s)
{
  int itgt, t,sp;
  ts_state *tgt;
  ts_trans *T;

  if( ccl_bittable_has(data->visited,s) )
    return;

  ccl_bittable_set(data->visited,s);

  data->n++;
  data->number[s] = data->n;
  data->link[s]   = data->n;
  data->stack[data->top++] = s;
  ccl_bittable_set(data->stacked,s);

  t = data->ts->state_table.data[s]->first_out; 
  for( ; t >= 0; t = data->ts->trans_table.data[t].next_out)
    {
      if( ! ar_ts_mark_has(data->R2,t) )
	continue;

      T = data->ts->trans_table.data+t;
      tgt = T->tgt;
      itgt = tgt->index;
      s_compute_loop(data,itgt);

      if( ccl_bittable_has(data->stacked,itgt) )
	{
	  if( data->link[itgt] < data->link[s] )
	    data->link[s] = data->link[itgt];
	  ccl_bittable_set(data->loop,t);
	  if( ar_ts_mark_has(data->R1,t) || 
	      ccl_bittable_has(data->valid,itgt) )
	    ccl_bittable_set(data->valid,s);
	}
    }

  if( data->link[s] == data->number[s] )
    {
      if( ccl_bittable_has(data->valid,s) )
	{
	  while( data->stack[data->top-1] != s ) 
	    {
	      sp = data->stack[data->top-1];
	      ccl_bittable_unset(data->stacked,sp);
	      data->top--;
	    }
	  ccl_bittable_unset(data->stacked,s);
	  data->top--;
	}
      else
	{
	  while( data->stack[data->top-1] != s )
	    {
	      sp = data->stack[data->top-1];
	      t = data->ts->state_table.data[sp]->first_out;

	      while( t >= 0 )
		{
		  ccl_bittable_unset(data->loop,t);
		  t = data->ts->trans_table.data[t].next_out;
		}
	      ccl_bittable_unset(data->stacked,sp);
	      data->top--;
	    }
	  t = data->ts->state_table.data[s]->first_out;
	  
	  while( t >= 0 )
	    {
	      ccl_bittable_unset(data->loop,t);
	      t = data->ts->trans_table.data[t].next_out;
	    }
	  ccl_bittable_unset(data->stacked,s);
	  data->top--;
	}
    }
}

			/* --------------- */

ar_ts_mark *
ar_ts_compute_loop(ar_ts *ts, ar_ts_mark *R1, ar_ts_mark *R2)
{
  int i;
  loop_data data;
  ar_ts_mark *result;

  ccl_pre( ts != NULL ); ccl_pre( R1 != NULL ); ccl_pre( R2 != NULL );

  result = ar_ts_mark_create(AR_SGS_SET_IS_TRANS, ts->nb_trans);
  data.ts = ts;
  data.visited = ccl_bittable_create(ts->nb_states);
  data.stacked = ccl_bittable_create(ts->nb_states);
  data.valid = ccl_bittable_create(ts->nb_states);
  data.loop = ccl_bittable_create(ts->nb_trans);
  data.number = ccl_new_array(int,ts->nb_states);
  data.link = ccl_new_array(int,ts->nb_states);
  data.n = 0;
  data.stack = ccl_new_array(int,ts->nb_states);
  data.top = 0;
  data.R1 = R1;
  data.R2 = R2;
  
  for(i=0; i<ts->nb_states; i++)
    {
      if( ccl_bittable_has(data.visited,i) )
	continue;
      s_compute_loop(&data,i);
    }

  i = ccl_bittable_get_first(data.loop);
  while( i >= 0 ) 
    {
      ar_ts_mark_add(result,i);
      i = ccl_bittable_get_next(data.loop,i);
    }
  ccl_bittable_delete(data.visited);
  ccl_bittable_delete(data.stacked);
  ccl_bittable_delete(data.valid);
  ccl_bittable_delete(data.loop);
  ccl_delete(data.number);
  ccl_delete(data.link);
  ccl_delete(data.stack);

  return result;
}

			/* --------------- */
