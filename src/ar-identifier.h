/*
 * ar-identifier.h -- 
 * 
 * This file is a part of the AltaRica Checker (ARC) project. 
 * 
 * See file AUTHORS and COPYING for copyright details.
 *
 */

#ifndef AR_IDENTIFIER_H
# define AR_IDENTIFIER_H

# include <ccl/ccl-array.h>
# include <ccl/ccl-iterator.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-string.h>
# include <ccl/ccl-list.h>
# include <ccl/ccl-serializer.h>

typedef ccl_ustring ar_symbol;
typedef struct ar_identifier_st ar_identifier;
typedef struct ar_idtable_st ar_idtable;
typedef CCL_ARRAY(ar_identifier *) ar_identifier_array;
				 
typedef void (*ar_identifier_log_proc)(ccl_log_type, const ar_identifier *);

CCL_ITERATOR_TYPEDEF(ar_identifier_iterator,ar_identifier *);

extern ar_identifier *AR_EMPTY_ID;


extern void
ar_identifier_init(void);

extern void
ar_identifier_terminate(void);

extern ar_identifier *
ar_identifier_create(const char *name);

extern ar_identifier *
ar_identifier_create_from_path (const ccl_list *path);

extern ar_identifier *
ar_identifier_create_simple(ar_symbol name);

extern ar_identifier *
ar_identifier_create_numbered (const char *pref, int n);

extern ar_identifier *
ar_identifier_create_numbered_auto (const char *pref, int *p_n);

extern ar_identifier *
ar_identifier_add_reference(ar_identifier *id);

extern void
ar_identifier_del_reference(ar_identifier *id);

extern ar_identifier *
ar_identifier_add_prefix(const ar_identifier *id, const ar_identifier *prefix);

extern ar_identifier *
ar_identifier_rename_with_prefix(const ar_identifier *id, const char *prefix);

extern ar_identifier *
ar_identifier_rename_with_suffix(const ar_identifier *id, const char *suffix);

extern int
ar_identifier_is_simple(const ar_identifier *id);

extern ar_identifier *
ar_identifier_get_name(const ar_identifier *id);

/*!
 * @pre ar_identifier_get_path_length (name) == 1
 */
extern int
ar_identifier_has_name (const ar_identifier *id, const ar_identifier *name);

extern ar_identifier *
ar_identifier_get_prefix(const ar_identifier *id);

extern ar_identifier *
ar_identifier_get_prefix_of_length (const ar_identifier *id, int length);

extern ar_identifier *
ar_identifier_get_name_and_prefix(const ar_identifier *id, 
				  ar_identifier **ppref);

extern ar_identifier *
ar_identifier_get_first(const ar_identifier *id, ar_identifier **remain);

extern ccl_list *
ar_identifier_get_path(ar_identifier *id);

extern int
ar_identifier_get_path_length(const ar_identifier *id);

extern int 
ar_identifier_is_container_of(const ar_identifier *id, 
			      const ar_identifier *other);

extern int 
ar_identifier_is_prefix_of(const ar_identifier *id, 
			   const ar_identifier *other);

extern int
ar_identifier_compare(const ar_identifier *id1, const ar_identifier *id2);

extern int
ar_identifier_lex_compare(const ar_identifier *id1, const ar_identifier *id2);

extern uint32_t
ar_identifier_hash(const ar_identifier *id);

extern int
ar_need_quotes (const char *s);

extern void
ar_identifier_log(ccl_log_type log, const ar_identifier *id);

extern void
ar_identifier_log_quote(ccl_log_type log, const ar_identifier *id);

extern void
ar_identifier_log_global_quote(ccl_log_type log, const ar_identifier *id);

extern void
ar_identifier_log_quote_gen (ccl_log_type log, const ar_identifier *id,
			     const char *separator,
			     const char *quotes
			     );

extern void
ar_identifier_log_global_quote_gen (ccl_log_type log, const ar_identifier *id,
				    const char *separator,
				    const char *quotes);

extern char *
ar_identifier_to_string(const ar_identifier *id);

extern ar_identifier *
ar_identifier_collapse(const ar_identifier *id);

extern ar_identifier *
ar_identifier_collapse_suffix(const ar_identifier *id);

extern void
ar_identifier_write (ar_identifier *id, FILE *out, 
		     ccl_serializer_status *p_err);

extern void
ar_identifier_read (ar_identifier **p_id, FILE *in, 
		    ccl_serializer_status *p_err);

			/* --------------- */

extern ar_idtable *
ar_idtable_create(int ordered, ccl_duplicate_func *dup, ccl_delete_proc *del);
extern ar_idtable *
ar_idtable_clone(ar_idtable *idt);
extern ar_idtable *
ar_idtable_add_reference(ar_idtable *idt);
extern void
ar_idtable_del_reference(ar_idtable *idt);
extern void
ar_idtable_put(ar_idtable *idt, ar_identifier *id, void *obj);
extern void
ar_idtable_remove(ar_idtable *idt, ar_identifier *id);
extern int
ar_idtable_has(const ar_idtable *idt, const ar_identifier *id);
extern int
ar_idtable_has_value(const ar_idtable *idt, ccl_compare_func cmp, void *val);
extern void *
ar_idtable_get(const ar_idtable *idt, const ar_identifier *id);
extern ar_identifier_iterator *
ar_idtable_get_keys(const ar_idtable *idt);
extern ccl_list *
ar_idtable_get_list_of_keys(const ar_idtable *idt);
extern int
ar_idtable_get_size(const ar_idtable *idt);
extern void
ar_idtable_log(ccl_log_type log, ar_idtable *idt, 
	       void (*logproc)(ccl_log_type, void *obj, void *data), 
	       void *data);

extern ar_idtable *
ar_idtable_dup (ar_idtable *table);

#endif /* ! AR_IDENTIFIER_H */
