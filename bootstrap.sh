#!/bin/bash 

HERE=$PWD
basedir=$(dirname $0)

source ${basedir}/bootstrap.conf

clone_with_ssh() {
    git clone ${SSH_URL}/${1}/${1}.git
}

clone_with_http() {
    git clone ${HTTP_URL}/${1}/${1}.git
}

get_branch() {
    git describe --contains --all HEAD
}

usage() {
    echo "USAGE: bootstrap.sh [--ssh|--http|--no-clone]"    
}

if test $# != 0
then
    case "$1" in
	--ssh) CLONE_CMD=clone_with_ssh ;;
	--http) CLONE_CMD=clone_with_http ;;
	--no-clone) UPDATE_MODULES=no ;;
	--help) usage; exit 0;; 	
	*) echo "$0: error: invalid argument $1" 1>&2; exit 1;;
    esac
fi

if test "${UPDATE_MODULES}" = "yes"
then
    COMMIT_DATE=$(git log -1 --pretty=format:%at)
    
    submodules=$(sed -e "s/@TOOL_BRANCH@/${TOOL_BRANCH}/g" <<< "${SUBMODULES}")
    
    for m in ${submodules}; do
	module_name=`echo $m | awk -F : ' { print $1 }'`
	module_version=`echo $m | awk -F : ' { print $2 }'`
	LOGFILE=${module_name}-git.log

	if test -d ${module_name}; then
	    cd ${module_name}

	    if [ "$(get_branch)" = "${module_version}" ]
	    then
		echo "Updating module ${module_name}"
		if ! git pull 2> ${LOGFILE}
		then
		    echo "Warning: unable to update submodule " \
			 "${module_name}." 1>&2 
		    echo "Look into ${LOGFILE} for SCM messages." 1>&2
		    continue
		fi
	    else
		if ! git checkout ${module_version} 2> ${LOGFILE}
		then
		    echo "Warning: unable to move to branch " \
			 "${module_version}." 1>&2 
		    echo "Look into ${LOGFILE} for SCM messages." 1>&2
		    continue
		fi		
	    fi
	    cd ${HERE}
	else
	    echo "Checking out module ${module_name}"
	    if (${CLONE_CMD} ${module_name} && cd ${module_name} && \
		    git checkout ${module_version}) 2> ${LOGFILE}; then
		:
	    else
		echo "An error occurs when retrieving submodule " \
		     "${module_name}." 1>&2 
		echo "Look into ${LOGFILE} for SCM messages." 1>&2 
		exit 1
	    fi
	fi

	cd ${module_name}
	if test -n "${DETACHED_DATE}"
	then
	    PREVCOMMIT=$(git rev-list --timestamp ${module_version} | \
			     while read d c; do
				 if test $d -le ${DETACHED_DATE} 
				 then
				     echo $c;
				     break;
				 fi;
			     done)
	    git checkout ${PREVCOMMIT}
	fi
	
	if test -x bootstrap.sh; then
	    ./bootstrap.sh
	fi
	cd ..
    done
else
    echo "skipping submodules update.."
fi

echo "running autotools reconfiguration..."
exec autoreconf -i -f 
