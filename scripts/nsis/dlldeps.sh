#!/bin/bash 

if test $# != 3
then
     echo "wrong # args ($#)" 1>&2 
    exit 1 
fi

EXE="$1"
DST="$2"
PATTERN="$3"

ldd ${EXE} | grep -e "${PATTERN}" | while read l
do
    echo $l
    DLL=$(sed -e 's/^ *\([^ ]\+\.dll\) => \([^ ]\+\.dll\) (.*) *$/\1/g' <<< "$l")
    FILE=$(sed -e 's/^ *\([^ ]\+\.dll\) => \([^ ]\+\.dll\) (.*) *$/\2/g' <<< "$l")
    if test -f ${DST}/${DLL}
    then
	n=1
	while test -f ${DST}/${DLL}.~${n}~
	do
	    let n=$n+1
	done   
	cp ${DST}/${DLL} ${DST}/${DLL}.~${n}~
	rm -f ${DST}/${DLL}
    fi
    cp ${FILE} ${DST}/${DLL}
done
