#! @GAWK@ -f 

#
# This script can be used to transform files containing simple labelled 
# transition system (TS) into an AltaRica node. States of the described TS are
# translated as values of a state variable used to control transitions; as a
# consequence, values used to label states must have same type i.e. either 
# booleans, integers or enum value. In the case of an integer state variable,
# the generated domain is a range whose bounds are the minimal and maximal 
# values encountered into the TS. Quoted identifiers are not allowed. 
# 
# Files are readed lines by lines. The syntax of TS files is the following. 
# - Comments start with a sharp character '#' and terminates at the end of 
#   the line.
# - a line starting with the 'name' keyword specifies the name of the generated
#   node. If no name is specified, "NoName" will be used.
# - a line starting with the 'initial' keyword describes initial values of the
#   state variable. Values are separated by spaces. Example:
#  
#      initial 0 3 # the state is an integer variable initialized with 0 or 3
#   or 
#      initial low # the state is an enum variable initialized with 'low'
# - a line of the form V1 |- e -> V2 describes a transition from the value 'V1'
#   to the value 'V2' by the event 'e'
# - a line starting with the 'no-epsilon' keyword indicates that code must be
#   generated to disabled the 'epsilon' event.
#
# Example:
# $ cat m.ts
# name MyTS
# initial 0
# 0 |- a -> 1
# 1 |- d -> 2
# 3 |- f -> 1
#
# $ ts2alt m.ts
# /* node generated from m.ts */ 
# node MyTS 
#  state
#   s : [0, 3]; 
#  init
#   (s = 0);
#  event
#   a, d, f;
#  trans
#   s = 0 |- a -> s := 1; 
#   s = 1 |- d -> s := 2; 
#   s = 3 |- f -> s := 1; 
# edon
#

function usage() {
    printf("USAGE: %s [options] filename\n", ARGV[0]);    
    print "where options are:";
    print "--help : print this help message.";
}

function parse_arguments() {
    for (a in ARGV) {
	if (ARGV[a] == "--help") {
	    usage();		
	    noerror = 0;
	    exit;
	}
    }
}

function remove_spaces_and_comments (s) {
    match (s, /^[[:space:]]*([^#]*)(#.*)*[[:space:]]*$/, r);
    return r[1];
}

function value_kind (value) {
    if (match(value,/^(true)|(false)$/))
	return STATE_KIND_BOOL;
    if (match(value,/^[-+]?(0|([1-9][[:digit:]]*))$/)) 
	return STATE_KIND_INT;
    if (match(value,/^[[:alpha:]_][[:alnum:]_]*$/)) 
	return STATE_KIND_ENUM;

    printf("%s:%d: error: invalid value kind '%s'.\n", 
	   FILENAME, FNR, value) > "/dev/stderr";
    noerror = 0;
    exit 
}

function assign_state_kind (value) {    
    vkind = value_kind(value);
    if (state_kind == STATE_KIND_NONE) {
	state_kind = vkind;
    } else if (vkind != state_kind) {
	printf("%s:%d: error: value '%s' is not of type '%s'.\n", 
	       FILENAME, FNR, value, state_kind) > "/dev/stderr";
	noerror = 0;
	exit 
    }

    if (state_kind == STATE_KIND_INT) {
	value = strtonum(value);
	if (value < state_min)
	    state_min = value;
	if (value > state_max)
	    state_max = value;
    } else if (state_kind == STATE_KIND_ENUM) {
	split (state_enum_values, values, ", ");
	if (! (value in values)) {
	    if (state_enum_values == "") 
		state_enum_values = value;
	    else
		state_enum_values = state_enum_values ", " value;
	}
    }
}

BEGIN {
    STATE_KIND_NONE = "none";
    STATE_KIND_BOOL = "bool";
    STATE_KIND_INT = "int";
    STATE_KIND_ENUM = "enum";
    NO_EPSILON = 0;

    parse_arguments();
    noerror = 1;
    state_kind = STATE_KIND_NONE;
    state_min = 0x7FFFFFFF;
    state_max = -state_min;
    state_enum_values = "";
    initial_values = "";
    transitions = "";
    event = "";
}

/^[[:space:]]*(#.*)?$/ { next; } 

/^[[:space:]]*no-epsilon[[:space:]]*(#.*)?$/ { 
    NO_EPSILON = 1;
    next;
} 

/^[[:space:]]*name[[:space:]].*/ { 
    input = remove_spaces_and_comments($0);
    if (match(input, "^name[[:space:]]+([[:alpha:]_][[:alnum:]_]*)$", r)) {
	if (NODENAME != "") {
	    printf("%s:%d: error: node name already defined.\n", FILENAME, 
		   FNR) > "/dev/stderr";
	    noerror = 0;
	    exit
	} else {
	    NODENAME = r[1];
	}
    } else {
	printf("%s:%d: error: malformed name directive.\n", FILENAME, 
	       FNR) > "/dev/stderr";
	noerror = 0;
	exit
    }
    next;
}

/^[[:space:]]*initial[[:space:]].*/ { 
    input = remove_spaces_and_comments($0);
    match(input, "^initial[[:space:]]+(.*)$", r);
    input = r[1];
    split(input, r, "[[:space:]]+");
    for(i in r) {
	value = r[i];
	assign_state_kind(value);
	if(initial_values == "") {
	    initial_values = sprintf("(s = %s)", value);
	} else {
	    initial_values = sprintf("%s | (s = %s)", initial_values, value);
	}
    }
    next;
}

/^[[:space:]]*(.*)[[:space:]]*[|]-[[:space:]]*(.*)[[:space:]]*->(.*)[[:space:]]*$/ { 
    input = remove_spaces_and_comments($0);    
    if (match(input, "^([^[:space:]]*)[[:space:]]*[|]-[[:space:]]*([[:alpha:]_][[:alnum:]_]*)[[:space:]]*->[[:space:]]*([^[:space:]]*)[[:space:]]*$", r)) {
	src = r[1];
	assign_state_kind(src);	
	event = r[2];	
	if (events == "")
	    events = event;
	else if (! (event in declared_events)) 
	    events = events ", " event;
	if (! (event in declared_events)) {
	    declared_events[event] = 1;
	}
	
	tgt = r[3];
	assign_state_kind(tgt);
	transitions = transitions "  s = " src " |- " event " -> s := " tgt "; \n";
    } else {
	printf ("%s:%d: error: malformed transition '%s'.\n", FILENAME, FNR,
		input) > "/dev/stderr";
	noerror = 0;
	exit;
    }
    next;
}

/.*$/ {
    printf ("%s:%d: error: malformed line.\n", FILENAME, FNR) > "/dev/stderr";
    noerror = 0;
    exit;
}

END {
    if (!noerror)
	exit;
    if (NODENAME == "")
	NODENAME="NoName";

    printf ("/* node generated from %s */ \n", FILENAME);    
    printf ("node %s \n", NODENAME);    
    printf (" state\n  s : ");
    if (state_kind == STATE_KIND_NONE) {
	printf ("{ none }");
    } else if (state_kind == STATE_KIND_BOOL) {
	printf ("bool");
    } else if (state_kind == STATE_KIND_INT) {
	printf ("[%d, %d]", state_min, state_max);
    } else if (state_kind == STATE_KIND_ENUM) {
	printf ("{ %s }", state_enum_values);
    } else {
	printf ("internal error.\n") > "/dev/null";
	exit
    }
    printf ("; \n");  
    
    if (initial_values != "") {
	printf (" init\n  %s;\n", initial_values);
    }

    if (events != "") {
	printf (" event\n  %s;\n", events);
    }

    if (transitions != "") {
	printf (" trans\n%s", transitions);
    }
    
    if (NO_EPSILON == 1) {
	print " // disabling of epsilon has been requested";
	print " event '$';";
	print " trans false |- '$' -> ;";
    }
    printf ("edon\n");
}
