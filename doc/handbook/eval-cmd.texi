@c %page eval
@c %title Interpret given arguments or standard input as it would be by the 'load' command.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{evalcmd}
Interpret given arguments or standard input as it would be by the 
@command{load} command.

@synopsis

@quotation
@noindent @t{eval} @cmdarg{text@math{_1}} @cmdarg{text@math{_2}} @enddots{} :
Interprets each @cmdarg{text@math{_1}} @cmdarg{text@math{_2}} @enddots{}

@t{eval} : Reads and interprets text from the standard input until the word @code{EOF} is read at the begin of the line or an end of file character e.g. @kbd{@key{CTRL}-D}).

@t{eval} @cmdarg{txt}: Reads and interprets text from the standard input until the word @cmdarg{txt} is read at the begin of the line or an end of file character e.g. @kbd{@key{CTRL}-D}).
@end quotation

@description
@quotation
This command interprets strings given as arguments or the characters typed 
on the standard input. Strings might be either an @altarica{} text, @mecv{} 
equations or @acheck{} commands. If no argument is passed to the command then 
the standard input stream is interpreted.

@Ex{1} Definition of an integer constant. ARC rejects redefinition of constants.
@smallexample
@i{arc>@t{eval}
eval>@t{const N : integer = 3;}
eval>@t{EOF}
arc>@t{eval}
eval>@t{const N : integer = 3;}
eval>@t{EOF}
<eval>:1: error: redefinition of constant 'N'.
arc>
arc>@t{show N}
const N : integer = 3;
arc>}
@end smallexample

@Ex{2} Evaluation of a @mecv{} predicate where the constant @code{N} is used (@code{^D} means @kbd{@key{CTRL}-D}).  
@smallexample
@i{arc>@t{eval}
eval> @t{R(x : [0,10]) := <y : [0,10]> (x = N * y);}
eval> @t{^D}
R (x : [0, 10]) : 4 elements
arc>@t{show R}
R contains :
x = 0
x = 3
x = 6
x = 9

arc>}
@end smallexample
@end quotation
