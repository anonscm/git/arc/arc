@c %page ca
@c %title Compute and display constraint automata that are the semantics of nodes given as arguments.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{cacmd}
Compute and display the constraint automaton (i.e. its semantics) of the given node.

@synopsis
@quotation 
@t{ca} [@option{--mec}] @cmdarg{node-id}@ind{1} @dots{} @cmdarg{node-id}@ind{n} : Displays constraint automata of nodes @cmdarg{node-id}@ind{i}
@end quotation

@description
@quotation 
This command computes and displays the constraint automaton that corresponds to the flat semantics of the node identified by @cmdarg{node-id}@ind{i}. The main difference between the commands @command{ca} and @command{flatten} is the removal of compound types  (i.e. structures and arrays) and quantified formulas in the constraint automaton.

If the option @option{--mec} is specified, dot notation (@t{.}) used to separate fields of a structure is replaced by @t{^}.

See also @refflattencmd{}.

@end quotation
