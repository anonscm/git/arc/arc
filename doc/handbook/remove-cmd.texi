@c %page remove 
@c %title Remove resources allocated to specified objects.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
Remove resources allocated to specified objects.

@synopsis
@quotation
@t{remove} @cmdarg{D} @cmdarg{id@ind{1}} @cmdarg{id@ind{2}} @enddots{}

Remove objects identified by @cmdarg{id@ind{1}}, @cmdarg{id@ind{2}}, @dots{} in
dictionary @cmdarg{D}.

@t{remove} @cmdarg{D} @t{all}

Remove all objects in dictionary @cmdarg{D}.

@t{remove} @t{all}

Remove all objects in all dictionaries.
@end quotation

@description
@quotation
For each given identifier, the command releases resources used by the associated object. Identifiers are related to some namespace or dictionary belonging to the following list: @code{constants}, @code{domains}, @code{nodes}, @code{signatures} or @code{relations}. Prefix of these names can be used.

@Ex{@ }
@smallexample
@i{arc>@t{list relations}
defined relations : 
arc>@t{eval}
eval>@t{R(x :[1,4]) += x <= 2;}
eval>@t{EOF}
R (x : [1, 4]) : 2 elements / 2 nodes
arc>@t{list rel}
defined relations : R
arc>@t{remove relation R}
arc>@t{list rel}
defined relations :}
@end smallexample
@end quotation

