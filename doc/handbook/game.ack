with Board do 
 dot(any_s, any_t) > "game.dot";
done 

with Board do 
 quot() > "game_q0.dot";
done

with Board do 
 W := [c[0][0] + c[0][1] + c[1][0] + c[1][1] = 0 or
       c[0][0] + c[0][1] + c[1][0] + c[1][1] = 4];
 O := [c[0][0] + c[0][1] + c[1][0] + c[1][1] = 1 or
       c[0][0] + c[0][1] + c[1][0] + c[1][1] = 3];

 CL := [c[0][0] + c[1][0] = 2 and c[0][1] + c[1][1] = 0 or 
        c[0][0] + c[1][0] = 0 and c[0][1] + c[1][1] = 2 or
        c[0][0] + c[0][1] = 2 and c[1][0] + c[1][1] = 0 or
        c[0][0] + c[0][1] = 0 and c[1][0] + c[1][1] = 2];

 D := [c[0][0] + c[1][1] = 2 and c[1][0] + c[0][1] = 0 or  
       c[0][0] + c[1][1] = 0 and c[1][0] + c[0][1] = 2];
 show (all) > "Board.props";
 quot() > "game_q1.dot";
done
