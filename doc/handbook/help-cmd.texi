@c %page help 
@c %title Display help informations about given topics.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{helpcmd}
Display help about ARC commands or syntax.

@synopsis
@quotation
@t{help} : With no argument the command is equivalent to 'help help'

@t{help} @cmdarg{topic} : Displays informations related to @cmdarg{topic}
@end quotation

@description
@quotation
This command is the companion of @refaproposcmd{} in the internal documentation of @arc{}. This command displays help informations about a given @cmdarg{topic}. Use command @command{apropos} to look for topics related to some keyword.

You can also type @command{help pages} to get a list of all available help 
pages.

For long help messages you should pipe the help command with a shell command 
like @command{more} or @command{less}.
@end quotation

