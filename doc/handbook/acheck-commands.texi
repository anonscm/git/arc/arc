@c %page acheck-commands 
@c %title commands used in Acheck specifications.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{acheck-commands}

@table @command
@item display(@cmdarg{id@ind{1}}, @dots{}, @cmdarg{id@ind{n}})

This command lists all the elements of sets @cmdarg{id@ind{1}}, @dots{}, 
@cmdarg{id@ind{n}}. Each @cmdarg{id@ind{i}} is either a predefined set like
@t{any_s} or a set computed by the user. If the encoding is @DD{} the 
predefined sets are computed on demand. 

Since the command enumerates elements of specified sets, it is quite wasteful to used it on large sets. To get the size of the set, the command @command{show}
@iftex
(see @ref{acheckshow,,@t{show}})
@end iftex
should be preferred and the operator @command{pick}
@iftex
(see @ref{acheckpick,,@t{pick}})
@end iftex
can be used to extract samples from sets.

@b{Encoding:} @TS{}, @DD{}

@item ctl2mu(@cmdarg{F})
@anchor{acheck:ctl2mu}
This command outputs the translation of @ctlstar{} formula @cmdarg{F} into a @mecv{} specification.
@iftex
@xref{Using CTL* logic}.
@end iftex

@b{Encoding:} @DD{}

@item dot(@cmdarg{S}, @cmdarg{T})
@anchor{acheck:dot}
This command outputs, in @Dot{} graph format, the reachability graph 
restricted to the set of configurations @cmdarg{S} and transitions @cmdarg{T}. 
Note that all reachable configurations belonging to @cmdarg{S} are displayed
even if they have no successors or predecessors.

@b{Encoding:} @TS{}, @DD{}

@item dot-trace(@cmdarg{I}, @cmdarg{T}, @cmdarg{F})
This command outputs, in @Dot{} graph format, a shortest trace from a state in @cmdarg{I} to a state in @cmdarg{F} using transitions in @cmdarg{T}. Actually this command is just a shortcut for:
@quotation
@t{tr := trace (@cmdarg{I}, @cmdarg{T}, @cmdarg{F});}

@t{dot (src(tr) or tgt(tr),tr);}
@end quotation

@b{Encoding:} @TS{}, @DD{}

@item events(@cmdarg{T})

This command lists events that labels transitions belonging to the set 
@cmdarg{T}. 

@b{Encoding:} @TS{}, @DD{}

@item modes()

This command display the mode-automaton of the current node. A @i{mode} is an assignment of state variables. Configurations are gathered according to each mode.
@cmdarg{T}. 

@b{Encoding:} @DD{}

@item nrtest('@cmdarg{filename}') 
@itemx nrtest(@cmdarg{X}, @cmdarg{n})
This command is used to realize non-regression tests. In its first form, the user specifies in a file @cmdarg{filename} expected results. Each line of this file is a couple:

@example
@code{set-identifier} @code{cardinality}
@end example

The @command{nrtest} command simply checks the equality of the cardinality of 
the computed set @code{set-identifier} with the one specified into 
@cmdarg{filename}. 

If either the set has not been computed or if the cardinalities
differ the test fail and in this case, according to the value of preference @preference{acheck.nrtest-failure-aborts}, the program might terminate and returns to the caller program (e.g. the shell) with an error code.

The second form simply checks that @cmdarg{n} is the cardinality of set @cmdarg{X}.

See preference @refpref{acheck.nrtest-failure-aborts}.

@b{Encoding:} @TS{}, @DD{}

@item project(@cmdarg{S}, (@cmdarg{TE}, @cmdarg{TA}), @cmdarg{id}, @cmdarg{simplify} [, @cmdarg{subnode}])
@itemx project(@cmdarg{S}, @cmdarg{TE}, @cmdarg{id}, @cmdarg{simplify} [, @cmdarg{subnode}])

This command projects the semantics of the node under study on one of its component if the argument @cmdarg{subnode} is specified; else it is projected on itself. The  displayed result is an @altarica{} node named @cmdarg{id} that is essentially the same than the original one but where transitions are restricted with those actually reached when the semantics is computed. The reachability graph can also be restricted to configurations belonging to @cmdarg{S} and transitions of @cmdarg{TE} and @cmdarg{TA}. If the Boolean argument @cmdarg{simplify} is @command{true} then guards are simplified; the aim of this parameter is to obtain clearest transitions (in particular for trivial guards).

@b{Encoding:} @TS{}, @DD{}

@item quot()

This command displays, in @code{dot} graph format, the greatest 
autobisimulation compatible with already computed properties of configurations; 
in other words the greatest bisimulation included in the relation 

@center @math{R = @{(s,@Prime{s}) | @forall P@in @Props, s@in P @iff @Prime{s}@in P@}}

where @math{@Props} is the set of configuration sets already computed.

@b{Encoding:} @TS{}, @DD{}

@item remove(@cmdarg{id@ind{1}}, @dots{}, @cmdarg{id@ind{n}})

This command removes from @arc{} memory properties specified by identifiers @cmdarg{id@ind{1}}, @dots{}, @cmdarg{id@ind{n}}. If some property @cmdarg{id@ind{i}} does not exists a warning is emitted.

@b{Encoding:} @TS{}, @DD{}

@item show(@cmdarg{id@ind{1}}, @dots{}, @cmdarg{id@ind{n}})
@anchor{acheckshow}

This command displays the cardinality of sets identified by @cmdarg{id@ind{i}}s.
If some @cmdarg{id@ind{i}} is equal to @code{all} then the command displays
all already computed sets. 

In the case of @DD{} encoding, predefined sets (e.g. @t{any_s}) are computed
on demand. Note that the @code{all} keyword does not induce the computation
of predefined sets.

@b{Encoding:} @TS{}, @DD{}

@item test(@cmdarg{X}, @cmdarg{n}, @cmdarg{ce})
@item test(@cmdarg{X}, @cmdarg{n})
@item test(@cmdarg{X}, @cmdarg{w})
@item test(@cmdarg{X})

This command is used to check that the cardinality of the set @cmdarg{X} is 
@cmdarg{n}. The command simply displays the result of the test. If the test
fails the actual size is displayed. If the set is expected to be empty (i.e.
@cmdarg{n} should be 0) and if the Boolean @cmdarg{ce} is @command{true} while the
test @i{fails}, a counter-example is computed and displayed.

If the expected cardinality @cmdarg{n} is not specified the non-emptyness of @cmdarg{X} is checked and if @cmdarg{w} is @command{true} while the
test @i{successes}, a witness is picked from the set and displayed.

@b{Encoding:} @TS{}, @DD{}

@item validate()

This command simply applies @refvalidatecmd{} to the current node.

@b{Encoding:} @DD{}

@item wts(@cmdarg{S}, @cmdarg{T})

This command is inherited from @sc{Mec 4}. It displays the restriction of the
transition system in @sc{Mec 4}-like format.

@b{Encoding:} @TS{} 

@end table


