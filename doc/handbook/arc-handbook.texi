\input texinfo  @c -*-texinfo-latin1-*-

@c ARC Handbook Version 1.5
@c %**start of header 
@setfilename arc-handbook.info
@documentlanguage en
@documentencoding UTF-8
@settitle AltaRica Checker Handbook - Version 0.2
@afourpaper
@c %**end of header

@include macros.texi

@copying
This document is the manual for the ARC tool version 1.6.@*
Permission is granted to copy and/or distribute this document.
@sp 1
LaBRI - CNRS UMR 5800 - Université Bordeaux@*
351, cours de la Libération@*
F-33405 TALENCE CEDEX@*
FRANCE
@end copying

@titlepage
@title AltaRica Checker Handbook
@subtitle user-guide for ARC 1.6
@subtitle @today{}
@flushright
@image{altarica,4cm,,,.png}
@end flushright
@author A.@tie{}Griffault, G.@tie{}Point, A.@tie{}Vincent
@page @c copyright page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@ifnotinfo
@c Table of contents
@contents

@setchapternewpage odd

@c The Top node (useful only for info files)


@c @node Top
@c  @top ARC Handbook

@include introduction.texi
@include session.texi
@include arc-shell.texi
@include acheck.texi
@include mec5.texi
@include sas.texi
@include as.texi
@include references.texi

@c Appendices
@include preferences.texi
@include sas-laws.texi

@c Indices
@c @include indices.texi
@end ifnotinfo
@bye


