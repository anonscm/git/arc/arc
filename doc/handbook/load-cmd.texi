@c %page load 
@c %title Load the specified files.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{loadcmd}
Load the specified files.

@synopsis
@quotation
@t{load} @cmdarg{file@ind{1}} @cmdarg{file@ind{2}} @enddots{}

Open and interpret files @cmdarg{file@ind{1}}, @cmdarg{file@ind{2}}, @enddots{}
All files are processed even if an error occurs in one of them.
@end quotation

@description
@quotation
This command is used to load into memory @altarica{} models, @acheck{} or @mecv{} specifications, @lustre{} programs (some restriction are applied, see @bibcite{gp06} for details), DD encoded relations (see @refstorecmd{}) or @arc{} scripts files. The parser is selected according to the file extension.

@itemize @bullet
@item If the file terminates with @code{.arc} then it is interpreted as an ARC script.
@item If the file terminates with @code{.lus} then it is interpreted as a Lustreprogram and is automatically translated into AltaRica.
@item If the file terminates with @code{.rel} then it is interpreted as a serialized DD encoding a relation. 
@item In other cases the file is passed to the parser used for @altarica{}, @acheck{}, @mecv{} and @arc{} scripts (these languages share the same parser).
@end itemize

For the latter case, the user can specify a preprocessing tool;
@iftex 
@pxref{arc.shell.preprocessor}. 
@end iftex
@ifplaintext
see @preference{arc.shell.preprocessor} in @command{help preferences}.
@end ifplaintext

@end quotation

