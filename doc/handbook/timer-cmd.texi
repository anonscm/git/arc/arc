@c %page timer
@c %title Measurement of execution times for ARC commands
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{timercmd}
Timers to measure CPU time.

@synopsis
@quotation
@t{timer}: Lists existing timers

@t{timer} @t{start} @cmdarg{timer-id}: Starts the timer @cmdarg{timer-id};
if it does not exists it is created. 

@t{timer} @t{reset} @cmdarg{timer-id}: Stop and reset accumulated time for the timer @cmdarg{timer-id}.

@t{timer} @t{stop} @cmdarg{timer-id}: Stops timer @cmdarg{timer-id}. 
Elapsed CPU time since last @t{start} is accumulated.

@t{timer} @t{elapsed} @cmdarg{timer-id}: Displays current elapsed CPU time
 since last @t{start} of timer @cmdarg{timer-id}. 

@t{timer} @t{print} @cmdarg{timer-id}: If timer @cmdarg{timer-id} is stopped, displays accumulated time since creation or last @t{reset}) of the timer. 

@t{timer} @t{remove} @cmdarg{timer-id}: Releases ressources allocated to 
and unregisters timer @cmdarg{timer-id}.
@end quotation

@description
@quotation
This command permits to create timers. Timers measure CPU time and not calendar time. Each one is identified by a name specified at creation and recalled each time a value of the timer is displayed. Two values are associated to each timer:
@enumerate
@item the current elapsed CPU time since the last @t{start} command;
@item the total amount of CPU time accumulated since the last @t{reset} or since the creation of the timer.
@end enumerate

A timer is created and started using the @t{start} command. The identifier specified at creation is reused when values of the timer are displayed; if the name of the timer is not a simple word then it must be enclosed between double-quotes (''). @t{stop} subcommand permits to temporarily suspend the timer and to store time elapsed since @t{start} into the accumulator; the value of this accumulator can be displayed using @t{print}. @t{elapsed} permits to display elapsed time since the last @t{start} without stopping the timer. Finally @t{reset} subcommand stops the timer and reset its accumulator to 0.

@Ex{@ }
@smallexample
@i{arc>@t{timer start "My Timer"}
arc>@t{eval}
eval>@t{R(s : [0,100], t : [0,100]) := s = 2*t;}
eval>R (s : [0, 100], t : [0, 100]) : 51 elements / 53 nodes
arc>@t{timer elapsed "My Timer"}
elapsed (My Timer) = 0.030
arc>@t{timer}
My Timer
arc>timer remove "My Timer"
arc>timer
arc>}
@end smallexample
@end quotation

