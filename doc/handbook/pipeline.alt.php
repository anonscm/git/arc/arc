const N=<?php echo $argv[1] ?>;

node Cell
 flow i, o : bool;
 state s : bool;
 assert o  = (if s then i else false);
 event act;
 trans true |- act -> s := not s;
edon

node Pipeline
  sub c : Cell[N];
  assert
  <?php for($i = 0; $i < $argv[1] - 1; $i++) { ?>
      c[<?php echo $i ?>].o = c[<?php echo $i+1 ?>].i;
  <?php } ?>
edon
