@node Introduction
@chapter Introduction

@arc{} is a toolbox for the @altarica{} language (@bibcite{agp99}). The main purpose of @arc{} is the @i{model-checking} of systems described with @altarica{} but its aim is to gather several tools for the analysis or compilation of @altarica{} models. @arc{} gathers works realized on two suites of tools: @acheck{} and @mecv{} (@bibcite{av03}). The current version offers the following features:
@itemize
@item The original @altarica{} language has been extended with:
      @itemize
      @item compound types (arrays, structures).
      @item the definition of abstract types and signatures of functions (not 
            supported by the model-checking engine).
      @end itemize
@item @acheck{} specifications are supported using explicit or symbolic 
      representation of the state-spaces (however not all the set of commands 
      are supported in both encoding).
@item @ctlstar{} formulae can be used in place of fixed-point equations.
@item @mecv{} (@bibcite{av03}) specifications are also supported and are handled
      using Decision Diagrams (those used within the @toupie{} tool
      @bibcite{cr97}). @arc{} extends the set of predefined sets with, for
      instance, @var{N}@code{!reach} that 
      specifies the set of reachable configurations of the node @var{N}.
@item @arc{} package integrates a small GUI hymbly called @studio{} that
      implements a graphical simulator.
@item Large relations can be serialized in binary files for future use.
@item Preprocessors can be specified using the configuration file of ARC.

@item Translators for the @lustre{} language (see @bibcite{gp06}).

@item Several algorithms have been implemented to generate sets of sequences or fault trees according to an unexpected configuration @bibcite{akpv11}.
@item A simulator for models decorated with stochastic informations; it permits to evaluate some measure such like @sc{mttf} (@i{mean time to failure}).
@end itemize


@section Where to get @arc{} ?

The @altarica{} website, @url{http://altarica.labri.fr}, housed at 
@url{http://www.labri.fr,LaBRI} gathers many informations related to the 
@altarica{} language. In particular you will find how to download the @arc{} tarball or the URL of the repository that contains its source code.

@section Where to send bug reports, comments or requests for new features ?

Bug reports, comments or any new feature requests can be sent to @url{altarica@@groupes.renater.fr}. You can also use the contact form of the @i{Support} section at @altarica{} website.

