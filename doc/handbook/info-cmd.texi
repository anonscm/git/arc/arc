@c %page info 
@c %title Display detailed informations related to specified objects.
@ifclear ARC_HANDBOOK
@include macros.texi
@end ifclear
@anchor{infocmd}
Display detailed informations related to specified objects.

@synopsis
@quotation
@t{info} @cmdarg{D} @cmdarg{id@ind{1}} @cmdarg{id@ind{2}} @enddots{} :

Displays information about objects identified by @cmdarg{id@ind{1}}, 
@cmdarg{id@ind{2}}, @enddots{} in dictionary @cmdarg{D} where @cmdarg{D} is one 
of the following name: @code{constants}, @code{domains}, @code{nodes}, 
@code{signatures}, @code{relations}, @code{dd-assignments}, @code{dd-all-paths}, or @code{order}.
@end quotation

@description
@quotation
For each given identifier, the command displays informative data related to 
the object. @arc{} looks for identifiers into its table @cmdarg{D}; a prefix can be used to specify the table e.g @code{const} for @code{constants}. If no identifier is given, the command displays informations about all known objects.

Available dictionaries/tables are the following:
@table @code
@item constants
The command returns informations related to declared (global) constants of the model.
@item domains
The command returns informations related to declared (global) domains of the model.
@item dd-all-paths
Identifiers are names for @mecv{} relations. The command display the DD encoding the relations in @Dot{} format. All paths from the root to leaves are displayed.
@item dd-assignments
Identifiers are names for @mecv{} relations. The command display the DD encoding the relations in @Dot{} format. Only paths from the root to the positive leaf @math{\top} are displayed.
@item nodes
For each specified @altarica{} node, the command displays its different dimensions (i.e. its number of variables, events, @enddots{}) and its hierarchy. Since they are not instantiated, no information is displayed for templates.
@item order
For specified @mecv{} relations, the command gives the order on variables used to compute its decision diagram.
@item relations
Identifiers are names of relations. For each one, the command displays the signature of the relation, its size (i.e., its number of elements) and the number of nodes of its underlying decision diagram.
@item signatures
The command recalls the prototype of the specified signatures. 
@end table

@Ex{@ } Below we create a new relation @code{R}. The command @command{info} displays the signature of the relation, its cardinality and the size of the data structure (i.e. the number of nodes of the DD) used to store it.
@smallexample
@i{arc>@t{list all}
arc>@t{eval}
eval>@t{R(x :[1,4]) += x <= 2;}
eval>@t{EOF}
R (x : [1, 4]) : 2 elements
arc>@t{list all}
defined relations : R
arc>info rel R
R : [1, 4] -> bool
cardinality         : 2
data structure size : 2
arc>}
@end smallexample
See also @reflistcmd{}.
@end quotation
