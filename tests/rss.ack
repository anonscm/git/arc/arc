with RssMain do symbolically
  S := [(2 * DMin <= 2 * s & 2 * s <= DMin+DMax)];
  Sc := any_c & [(2 * DMin <= 2 * s & 2 * s <= DMin+DMax)];
  R += initial | tgt (valid_state_changes & rsrc (R));
  Rc := R & any_c;
  nrtest('$NODENAME.prop'); 
done
