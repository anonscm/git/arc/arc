load bugfix-035.alt

echo "MINCUTS"
sequences --min --enum --order=2 --visible-tags=visible System "not (E.C01.s = ok)"

echo "CUTS"
sequences --enum --order=3 --visible-tags=visible System "not (E.C01.s = ok)"
