#!/bin/bash 

if test "x$1" = "x"; then
    echo "Usage: $0 test-name" 1>&2
    exit 1
fi

TESTNAME=$(basename $1 .res)
REFNAME="${srcdir}/${TESTNAME}.result"

if test -s $1.err;
then
    (echo "non empty error file:"; cat $1.err) > /dev/stderr 
    exit 1
else 
    exec diff -u "${REFNAME}" "$1"
fi
