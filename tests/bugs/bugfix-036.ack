/*
with Board do 
 dot(any_s, any_t);
done 

with Board do 
 quot();
done
*/

with Board do 
 W := [c[0][0] + c[0][1] + c[1][0] + c[1][1] = 0 or
       c[0][0] + c[0][1] + c[1][0] + c[1][1] = 4];
 O := [c[0][0] + c[0][1] + c[1][0] + c[1][1] = 1 or
       c[0][0] + c[0][1] + c[1][0] + c[1][1] = 3];

 CL := [c[0][0] + c[1][0] = 2 and c[0][1] + c[1][1] = 0 or 
        c[0][0] + c[1][0] = 0 and c[0][1] + c[1][1] = 2 or
        c[0][0] + c[0][1] = 2 and c[1][0] + c[1][1] = 0 or
        c[0][0] + c[0][1] = 0 and c[1][0] + c[1][1] = 2];

 D := [c[0][0] + c[1][1] = 2 and c[1][0] + c[0][1] = 0 or  
       c[0][0] + c[1][1] = 0 and c[1][0] + c[0][1] = 2];
 show (all);
/* quot(); */
done

Win(s : Board!c) := 
   s.c[0][0] + s.c[1][0] + s.c[0][1] + s.c[1][1] = 0 or
   s.c[0][0] + s.c[1][0] + s.c[0][1] + s.c[1][1] = 4;


card Win

WSeq1(s : Board!c, e1 : Board!ev) := 
 Win(s) or 
 [t : Board!c] (Board!t (s,e1,t) => Win (t));

S1 (e1 : Board!ev) := 
 [s : Board!c] WSeq1 (s, e1);

card S1

WSeq2(s : Board!c, e1 : Board!ev, e2 : Board!ev) := 
 Win(s) or 
 [t : Board!c] (Board!t (s, e1, t) => WSeq1 (t, e2));

S2 (e1 : Board!ev, e2 : Board!ev) := 
 [s : Board!c] WSeq2 (s, e1, e2);

card S2

WSeq3(s : Board!c, e1 : Board!ev, e2 : Board!ev, e3 : Board!ev) := 
 Win (s) or 
 [t : Board!c] (Board!t (s, e1, t) => WSeq2 (t, e2, e3));

S3 (e1 : Board!ev, e2 : Board!ev, e3 : Board!ev) := 
 [s : Board!c] WSeq3 (s, e1, e2, e3);

card S3

WSeq4(s : Board!c, e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev) := 
 Win (s) or 
 [t : Board!c] (Board!t (s, e1, t) => WSeq3 (t, e2, e3, e4));

S4 (e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev) := 
 [s : Board!c] WSeq4 (s, e1, e2, e3, e4);

card S4

WSeq5(s : Board!c, e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev, e5 : Board!ev) := 
 Win (s) or 
 [t : Board!c] (Board!t (s, e1, t) => WSeq4 (t, e2, e3, e4, e5));

S5 (e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev, e5 : Board!ev) := 
[s : Board!c] WSeq5 (s, e1, e2, e3, e4, e5);
card S5

WSeq6(s : Board!c, e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev, e5 : Board!ev, e6 : Board!ev) := 
 Win (s) or 
 [t : Board!c] (Board!t (s, e1, t) => WSeq5 (t, e2, e3, e4, e5, e6));

S6 (e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev, e5 : Board!ev, e6 : Board!ev) := 
[s : Board!c] WSeq6 (s, e1, e2, e3, e4, e5, e6);
card S6

WSeq7(s : Board!c, e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev, e5 : Board!ev, e6 : Board!ev, e7 : Board!ev) := 
 Win (s) or 
 [t : Board!c] (Board!t (s, e1, t) => WSeq6 (t, e2, e3, e4, e5, e6, e7));

S7 (e1 : Board!ev, e2 : Board!ev, e3 : Board!ev, e4 : Board!ev, e5 : Board!ev, e6 : Board!ev, e7 : Board!ev) := 
[s : Board!c] WSeq7 (s, e1, e2, e3, e4, e5, e6, e7);
card S7
show S7
