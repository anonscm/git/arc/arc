#define IN2(x,v1,v2)    ((x=v1)or(x=v2))
#define IN3(x,v1,v2,v3) (IN2(x,v1,v2)or(x=v3))
#define IN4(x,v1,v2,v3,v4) (IN3(x,v1,v2,v3)or(x=v4))

with _0 do
  symbolically 
  _80 := label _44; 
  _81 := tgt (rsrc (initial) - epsilon) and tgt (_80) and valid_state_assignments;
  X := [_36 = _14 and _38 = _14 and _30 = _14 and _19 = _14 and _39 = _14
        and _29 = _14 and _20 = _14 and _40 = _14 and _35 = _14 and _18 = _14
	and _33 = _14 and _41 = _14 and _32 = _14 and _13 = _14 and _34 = _14
	and _28 = _14 and _31 = _14 and _37 = _14 and _22 = _14 and _17 = _14
	and _24 = _14 and _25 = _14 and _21 = _15 ];
  nrtest(X-_81, 0);
  nrtest(_81-X, 0);
done
