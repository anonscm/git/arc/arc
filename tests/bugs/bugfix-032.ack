#define IN2(x,v1,v2)    ((x=v1)or(x=v2))
#define IN3(x,v1,v2,v3) (IN2(x,v1,v2)or(x=v3))
#define IN4(x,v1,v2,v3,v4) (IN3(x,v1,v2,v3)or(x=v4))

with _0 do
  symbolically
  X := [IN3(_18,_14, _15, _16) and IN2(_33,_14, _15) and
        IN3(_41, _14, _15, _16) and IN2(_32, _14, _15) and
	IN2(_36, _14, _15) and IN2(_38, _14, _15) and IN2(_30,_14, _15) and
	IN3(_19,_14, _15, _16) and IN2(_39, _14, _15) and IN2(_29, _14, _15)
	and IN3(_20, _14, _15, _16) and IN3(_40, _14, _15, _16) and
	IN2(_35, _14, _15) and IN3(_13, _14, _15, _16) and IN2(_34, _14, _15)
	and IN3(_28, _14, _26, _27) and IN3(_31, _14, _26, _27) and
	IN2(_37, _14, _15) and IN4(_22, _14, _15, _16, _23) and
	IN3(_17, _14, _15, _16) and IN3(_24, _14, _15, _16) and
	IN3(_25, _14, _26, _27) and IN3(_21,_14, _15, _16)];
  nrtest(X-valid_state_assignments, 0);
  nrtest(valid_state_assignments-X, 0);
done
