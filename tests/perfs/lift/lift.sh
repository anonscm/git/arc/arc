#!/bin/bash


N=$(expr $1 + 0)
SUFFIX=$(printf "%02d" $N)

cat > lift_${SUFFIX}.arc <<EOF
set arc.shell.preprocessor.default.command "${PHP}"
set arc.shell.preprocessor.default.args $N
load lift.alt
load lift.mec5
EOF

