node Component
  flow
    i, o : bool;
  state
    s : { ok, nok };
  init s := ok;
  event
    failure : public;
  extern
    law <event failure> = exponential (LAMBDA);
  trans
    s = ok |- failure -> s := nok;
  assert
    o = ((s = ok) & i);
edon
