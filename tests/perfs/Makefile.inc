SHELL=/bin/bash
ARC=${top_builddir}/src/arc
ARCPATH=${srcdir}:${srcdir}/..
PERF_DEFAULT_TIMEOUT = 2400 #2400 # 40 minutes
MAX_TIME_SECONDS = $${DEFAULT_TIMEOUT:-${TIMEOUT}}
MAX_MEMORY_MBYTES = 4096 # 4 GBytes
RUNBENCH_ENV = ARC="${ARC}"; srcdir="${srcdir}"; ARCPATH=${ARCPATH}; \
               MAX_TIME_SECONDS="${MAX_TIME_SECONDS}"; \
               MAX_MEMORY_MBYTES="${MAX_MEMORY_MBYTES}"; \
               PHP="${PHP}"; ARC_TEST_SUITE=1; \
               export PHP ARC srcdir ARCPATH MAX_MEMORY_MBYTES; \
               export MAX_TIME_SECONDS ARC_TEST_SUITE;

RUNBENCH=${RUNBENCH_ENV} ${SHELL} ${srcdir}/../runbench.sh

runbench_verbose  = $(runbench_verbose_@AM_V@)
runbench_verbose_ = $(runbench_verbose_@AM_DEFAULT_V@)
runbench_verbose_0 = @echo "  RUN $@";

build_verbose  = $(build_verbose_@AM_V@)
build_verbose_ = $(build_verbose_@AM_DEFAULT_V@)
build_verbose_0 = @echo "  BLD $@";

TIME_FILES =
CLEANFILES =
EXTRA_DIST =

all : ${TARGETS}

.arc.time:
	${runbench_verbose} ${RUNBENCH} $<

OUTPUT_FILES = ${TIME_FILES:%.time=%.out}
CLEANFILES += ${TIME_FILES} ${TIME_FILES:%.time=%.out} \
              ${TIME_FILES:%.time=%.err} 

save :
	for f in ${OUTPUT_FILES}; do cp $$f ${srcdir}/$${f}-expected; done

