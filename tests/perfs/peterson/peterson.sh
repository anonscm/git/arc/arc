#!/bin/bash


N=$1

cat > peterson_$N.arc <<EOF
set arc.shell.preprocessor.default.command "${PHP}"
set arc.shell.preprocessor.default.args $1
load peterson.alt
load peterson.mec5
EOF

