#!/bin/bash 


N="$(expr $1 + 0)"
SUFFIX=$(printf "%04d" $N)

cat > stress_${SUFFIX}.alt <<EOF
node Component
  flow i, o : [0,1];
  event failure;
  state st : { ok, ko }; init st := ok;
  trans st  = ok |- failure -> st:= ko;
  assert 
    if st = ok then o = 1-i else o = 0;
edon

node Main
 sub 
   c : Component[$N];
 assert

$(let n=0
while [ $n -lt $N ]
do
    if [ $n = $(expr $N - 1) ]
    then
	next=0
    else
	next=$(expr $n + 1)
    fi
    echo "   c[$n].o = c[${next}].i;"
    let n=$n+1
done)
edon
EOF

cat > stress_${SUFFIX}.mec <<EOF
Init(s : Main!c) := Main!init(s);
Err(s : Main!c) := s.c[0].st = ko$(let n=2;
while [ $n -lt $N ]
do
  printf " & s.c[%d].st = ko" $n
  let n=$n+2
done);

EOF

cat > stress_${SUFFIX}.arc <<EOF
load stress_${SUFFIX}.alt stress_${SUFFIX}.mec 
load reach-direct.mec reach-prestar.mec
card Main!reach
EOF
