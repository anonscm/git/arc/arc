#!/bin/bash 

INPUT_FILE="$1"
INPUT_FILE_PREFIX=$(basename ${INPUT_FILE} .arc)
INPUT_FILE_DIRECTORY=$(dirname ${INPUT_FILE})
TIME_FILE=${INPUT_FILE_PREFIX}.time
OUTPUT_FILE=${INPUT_FILE_PREFIX}.out
ERROR_FILE=${INPUT_FILE_PREFIX}.err

if test -n "${MAX_TIME_SECONDS}"
then
    if bash -c "ulimit -t ${MAX_TIME_SECONDS}" 2> /dev/null
    then
	LIMITS="ulimit -t ${MAX_TIME_SECONDS} ; "
    fi
fi

if test -n "${MAX_MEMORY_MBYTES}"
then
    MAXMEM="-v $(expr ${MAX_MEMORY_MBYTES} '*' 1024)"
    if bash -c "ulimit ${MAXMEM}" 2> /dev/null
    then
	LIMITS="${LIMITS} ulimit ${MAXMEM} ; "
    fi
fi

bash -c "${LIMITS} ${ARC} -qb -c 'set arc.shell.rusage true' \"${INPUT_FILE}\"" 2> "${ERROR_FILE}" | awk "/ru_(time|cputime|maxrss)/ { print > \"${TIME_FILE}\"; next } { print > \"${OUTPUT_FILE}\" }"

if test -f ${OUTPUT_FILE}
then
    EXPECTED_OUTPUT="${srcdir}/${OUTPUT_FILE}-expected"
    if test -f ${EXPECTED_OUTPUT} && ! cmp -s ${OUTPUT_FILE} ${EXPECTED_OUTPUT}
    then
	cat >> "${ERROR_FILE}" <<EOF 
${OUTPUT_FILE} differs from expected output. try:
diff ${OUTPUT_FILE} ${srcdir}/${OUTPUT_FILE}-expected
EOF
    fi
else
    echo "test case '$1' produces no output." 1>&2 >> "${ERROR_FILE}"
fi

exit 0

