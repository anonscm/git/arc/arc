#!/bin/bash

set -e
TARGET="$1"

if test "x${TARGET}" != "x"
then
    trap -- "rm ${TARGET}; exit 1" ERR
    exec 1> "${TARGET}"
fi

for f in $(find . -name '*.time' -o -name '*.err' | sed -e 's/.err$//' -e 's/.time$//g' | sort -u)
do
    TESTNAME=$(basename $f)
    TIMEFILE="${f}.time"
    ERRFILE="${f}.err"
    DIR=$(basename $(dirname $f))
    
    if test -s ${ERRFILE}
    then
	ru_time=err
	ru_cputime=err
	ru_maxrss=err
    elif ! source ${TIMEFILE}
    then
	ru_time=none
	ru_cputime=none
	ru_maxrss=none	
    fi
    echo "${DIR};${TESTNAME};${ru_time};${ru_cputime};${ru_maxrss}" 
done
