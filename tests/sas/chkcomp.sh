#! /bin/bash 

INPUT="$1"
OUTPUT="$2"
ERRPUT="$(basename ${INPUT}).err"
EXPECTED="${srcdir}/${OUTPUT}-expected"
TMP1=tmp1.$$
TMP2=tmp2.$$

NODES=$(${ARC} -qbx ${INPUT} -c 'list root-nodes' 2> ${ERRPUT} | sed -e 's/.* : //g' -e 's/,//g')
exitcode=$?

if test ${exitcode} = 0 -a ! -s "${ERRPUT}";
then
    (for n in ${NODES}; do
	 ${ARC} -qb ${INPUT} -c "show $n" -c "flatten $n" -c "ca $n" || exit 1
     done ) > ${OUTPUT} 2>> ${ERRPUT}
else
    exit 1
fi

sort ${OUTPUT} > ${TMP1}
sort ${EXPECTED} > ${TMP2}
cmp -s ${TMP1} ${TMP2}
cmpres=$?
rm -f ${TMP1} ${TMP2}

if test $cmpres = 0; then
    if test -s ${ERRPUT};
    then
	echo "test produces errors in ${ERRPUT}." 1>&2
	exit 1
    fi
else
    (echo "results differ from expected ones. try:"
     echo "diff ${OUTPUT} ${EXPECTED}") 1>&2 
    exit 1
fi

