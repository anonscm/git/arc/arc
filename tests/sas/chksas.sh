#! /bin/bash 

INPUT="$1"
OUTPUT="$2"
ERRPUT="$(basename ${INPUT}).err"
EXPECTED="${srcdir}/${OUTPUT}-expected"
EXPECTEDERR="${srcdir}/${ERRPUT}-expected"


${ARC} -qb -c 'set sas.nb-threads 1' ${INPUT} > "${OUTPUT}" 2> "${ERRPUT}"

if test -s ${ERRPUT}
then
    if test -f ${EXPECTEDERR} 
    then
	if ! cmp -s ${ERRPUT} ${EXPECTEDERR};
	then
	    (echo "test produces unexpected errors in ${ERRPUT}."
	     echo "diff ${ERRPUT} ${EXPECTEDERR}") 1>&2 
	    exit 1
	fi
    else
	echo "test produces unexpected errors in ${ERRPUT}." 1>&2 
	exit 1
    fi
else
    if ! cmp -s ${OUTPUT} ${EXPECTED}
    then
	(echo "results differ from expected ones. try:"
	 echo "diff ${OUTPUT} ${EXPECTED}") 1>&2 
	exit 1
    fi
fi

