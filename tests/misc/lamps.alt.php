<?php 
  $NB_LAMPS = 50; 
  $MAX_ON = 6; 
?>

const MAX_ON = <?php echo $MAX_ON ?>;
const NB_LAMPS = <?php echo $NB_LAMPS ?>;

node Lamp
  flow 
    i, o : [0, MAX_ON];
    last_on_i, last_on_o : [1, NB_LAMPS];

  state 
  id : [1, NB_LAMPS];
  on : [0,1] : public;
  init 
    on := 0;
  event 
    set_on, set_off;
  
  trans
    last_on_i=id-1 & on = 0 |- set_on -> on := 1;
    last_on_i=id-1 & on = 1 |- set_off -> on := 0;

  assert 
      o = (i + on);
      last_on_o = (if on = 1 then id else last_on_i);
edon

node Main
  sub
   l : Lamp[<?php echo $NB_LAMPS ?>];
  assert
    l[0].i = 0;
    l[0].last_on_i = 1;

<?php for ($i = 1; $i < $NB_LAMPS; $i++) { ?>
  l[<?php echo $i ?>].i = l[<?php echo ($i-1) ?>].o;
  l[<?php echo $i ?>].last_on_i = (l[<?php echo ($i-1) ?>].last_on_o);
<?php } ?>
  init
    l[0].on := 1;    
<?php for ($i = 0; $i < $NB_LAMPS; $i++) { ?>
      l[<?php echo $i ?>].id := <?php echo ($i + 1) ?>;
<?php } ?>
edon

begin
  local aux (m : [1, NB_LAMPS]) := 
      [s : Main!sc] (Main!reach (s) => (s.l[NB_LAMPS - 1].last_on_o <= m));
  R (m : [1, NB_LAMPS]) := aux (m) &
     ([mp  : [1, NB_LAMPS]] (aux (mp) => mp >= m));
end
