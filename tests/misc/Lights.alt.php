<?php
  /* Constants.  See comments below. */
  $LAMPS = 50;
  $MAXON = 6;
?>
// Total number of lamps.  Lamps range from 1 to LAMPS.
const LAMPS = <?php echo "$LAMPS"; ?>;
// Maximum number of lamps that are simultaneously on.
const MAXON = <?php echo "$MAXON"; ?>;

node Lights
state
  last : [1, LAMPS];
<?php
    for ($i = 1; $i <= $LAMPS; $i++) {
        printf("  l%02d: [0, 1];\n", $i);
    }
?>
event
<?php
    for ($i = 2; $i <= $LAMPS; $i++) {
        printf("  sw%02d;\n", $i);
    }
?>
trans
<?php
    for ($i = 1; $i < $LAMPS; $i++) {
            printf("  l%02d = 1 |- sw%02d -> l%02d := 1 - l%02d, last := %d;\n", $i, $i+1, $i+1, $i+1, $i+1);
    }
?>
assert
    l01
<?php
    for ($i = 2; $i <= $LAMPS; $i++) {
            printf("  + l%02d\n", $i);
    }
?>
  <= MAXON
init
  last := 1;
  l01 := 1;
<?php
    for ($i = 2; $i <= $LAMPS; $i++) {
        printf("  l%02d := 0;\n", $i);
    }
?>
edon
