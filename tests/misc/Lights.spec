// Reachability Set.
ReachConf(s:Lights!c) += Lights!init(s) |
                <e:Lights!ev><s':Lights!c> (ReachConf(s') & Lights!t(s', e, s));

// Compute reachable lamps and the max of them.
Reach(m : [1, LAMPS]) := <s:Lights!c> (ReachConf(s) & s.last = m);
MaxReach(m : [1, LAMPS]) := Reach(m) & [p : [1, LAMPS]] (Reach(p) => p <= m);

// Show result.
:d MaxReach
