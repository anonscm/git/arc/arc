// Display computation times
set acheck.timers true
set mec5.timers true

// Compute reachable lamps and the max of them.
begin
  Reach(m : [1, LAMPS]) := <s : Lights!c> (Lights!reach(s) & s.last = m);
  MaxReach(m : [1, LAMPS]) := Reach(m) & [p : [1, LAMPS]] (Reach(p) => p <= m);
end

// Show results.
show MaxReach

// Extract trace to switch the max (given, here, explicitly).
/*
const maxlitlamp = 32;
with Lights do
  T := trace (initial, any_t, [last = maxlitlamp]);
  dot (src(T)|tgt(T), T) > 'trace.dot';
done
*/
