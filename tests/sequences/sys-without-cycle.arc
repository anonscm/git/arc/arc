load sys-without-cycle.alt

echo "REDUCTION"
target-reduction --goal-after= System "not E.o1"
echo "CUTS"
cuts --visible-tags=visible System "not E.o1"
echo "OCUTS"
sequences --enum --order=2 --visible-tags=visible System "not E.o1"