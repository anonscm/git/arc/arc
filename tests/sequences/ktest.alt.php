<?php 
 $NB_MAINTENANCE_TEAM  = 2;
 $PRODUCTION_LINE_SIZE = 5;
 $NB_PU = 2 * $PRODUCTION_LINE_SIZE;
?>

const NB_MAINTENANCE_TEAM  = <?php echo $NB_MAINTENANCE_TEAM ?>;
const PRODUCTION_LINE_SIZE = <?php echo $PRODUCTION_LINE_SIZE ?>;
const NB_PU = <?php echo $NB_PU ?>;

domain ProdUnitStates = { off, run, hs };

node ProductionUnit
  flow i, o : bool;
  state pc : ProdUnitStates;
event start, repair;
        fail : visible;
  trans 
    pc = off |- start -> pc := run;
    pc = run |- fail -> pc := hs;
    pc = hs |- repair -> pc := off;
  assert
    o = (i & pc = run);
edon

node MaintenanceTeam
  flow where : [0, NB_PU];
  state comp : [0, NB_PU]; 
        pc : { idle, repair, sleep };
  init comp := NB_PU, pc := idle;
event on_road[NB_PU], repair, wakeup;
      Zzz : visible;
  trans 
    pc = sleep |- wakeup -> pc := idle;
    pc = repair |- repair -> pc := idle, comp := 0;
    pc = idle |- Zzz -> pc := sleep;
<?php for ($i = 0; $i < $NB_PU; $i++) { ?>
    pc = idle |- on_road[<?php echo $i?>] -> pc := repair, comp := <?php echo $i?>;
<?php } ?>
  assert where = comp;
edon

node System
  flow 
   accept : bool;
  sub 
   MT : MaintenanceTeam[NB_MAINTENANCE_TEAM];
   ML, RL : ProductionUnit[PRODUCTION_LINE_SIZE];
<?php for ($i = 0; $i < $PRODUCTION_LINE_SIZE; $i++) { ?>
  init
    ML[<?php echo $i ?>].pc := run,
    RL[<?php echo $i ?>].pc := off;
<?php } ?>
  assert
   // connection between nodes
   ML[0].i = true;
   RL[0].i = true;
<?php for ($i = 1; $i < $PRODUCTION_LINE_SIZE; $i++) { ?>
   ML[<?php echo $i ?>].i = (ML[<?php echo $i-1 ?>].o|RL[<?php echo $i-1 ?>].o);
   RL[<?php echo $i ?>].i = (ML[<?php echo $i-1 ?>].o|RL[<?php echo $i-1 ?>].o);
<?php } ?>
  
   // constraint on maintenance teams movements
<?php
for ($i = 0; $i < $NB_MAINTENANCE_TEAM; $i++) { 
  for ($j = $i + 1; $j < $NB_MAINTENANCE_TEAM; $j++) {
?>
   (MT[<?php echo $i ?>].where = MT[<?php echo $j ?>].where) => ((MT[<?php echo $i ?>].where = NB_PU) and (MT[<?php echo $j ?>].where = NB_PU));
<?php 
      } 
}
?>
  sync
<?php 
  for ($i = 0; $i < $PRODUCTION_LINE_SIZE; $i++) { 
    for ($j = 0; $j < $NB_MAINTENANCE_TEAM; $j++) { 
?>
   <MT[<?php echo $j ?>].on_road[2 * <?php echo $i ?>]?, ML[<?php echo $i ?>].fail, RL[<?php echo $i ?>].start?>;
   <MT[<?php echo $j ?>].on_road[2 * <?php echo $i ?> + 1]?, RL[<?php echo $i ?>].fail, ML[<?php echo $i ?>].start?>;
   <MT[<?php echo $j ?>].repair, RL[<?php echo $i ?>].repair>;
   <MT[<?php echo $j ?>].repair, ML[<?php echo $i ?>].repair>;
<?php 
    }
  }
?>    
  assert 
    accept = (not (ML[PRODUCTION_LINE_SIZE - 1].o|RL[PRODUCTION_LINE_SIZE - 1].o));  
edon

