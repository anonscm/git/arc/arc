load mmr2004.alt
load mmr2004.spec

echo "Original Contraint Automaton"
ca System

echo "Reduced Contraint Automaton"
target-reduction System accept

echo "Minimal Cuts"
timer start "Mincuts"
cuts --enum --min --tag=visible System 
timer stop "Mincuts"
timer print "Mincuts"

echo "Ordered cuts"
timer start "OrderedMincuts"
sequences --enum --order=4 --tag=visible --min System 
timer stop "OrderedMincuts"
timer print "OrderedMincuts"




