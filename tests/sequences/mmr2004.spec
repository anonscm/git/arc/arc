Error(s : System!sc) := s.accept;

Bad(s : System!sc) := System!reach(s) & Error (s);
InitIsBad(s : System !c) := System!init(s) & Error (s);

/*
with System do symbolically
//   Start := any_c & tgt(rsrc(initial) & label B.failure);
//   Start := any_c & tgt(rsrc(initial) & label K3.untimely_open);
//   Start := any_c & tgt(rsrc(initial) & label A.failure);
   Start := any_c & tgt(rsrc(initial) & label K2.untimely_open);
   SA := Start & [accept];
   T1 := trace (initial,any_t,Start);
   T2 := trace (Start,any_t-attribute visible,[accept]);
   T := T1 or T2;
   show(all);
   dot (src(T) or tgt(T),T);
done

*/

card System!reach Bad InitIsBad

