<?php $NB_PROCESSES=4 ?>

const N : integer = <?php echo  $NB_PROCESSES?>;

node Component
  flow out : [0,1];
  event failure : visible;
  state ok : bool; init ok := true;
  trans ok |- failure -> ok := false;
  assert out = (if ok then 0 else 1);
edon

node Main
  flow error : bool;
  sub c : Component[N];
  assert 
    error = (c[0].out<?php for ($i = 1; $i < $NB_PROCESSES; $i++) echo " + c[$i].out";?> >= 2);
edon
