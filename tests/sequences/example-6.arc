load example-6.alt

echo "REDUCTION"
target-reduction Main "c[0].s = ok & c[0].s = ko & c[1].s = ok & c[1].s = ko"
echo "CUTS"
cuts --visible-tags=visible Main "c[0].s = ok & c[0].s = ko & c[1].s = ok & c[1].s = ko "
echo "OCUTS"
sequences --enum --visible-tags=visible --order=5 Main "c[0].s = ok & c[0].s = ko & c[1].s = ok & c[1].s = ko "
