with Train1_V1, Train2_V1, Train3_V1, Train1_V2, Train2_V2, Train3_V2 do
 symbolically
// quot()                   >  '$NODENAME.dot';
 dead                     := any_s - src(any_t - self_epsilon);
 //notCFC                   := any_t - loop(any_t,any_t);
 show(all)                >  '$NODENAME.prop';
 test(dead,0)             >  '$NODENAME.res';
 //test(notCFC,0)           >> '$NODENAME.res';
done
with Systeme_V1, Systeme_V2 do
 symbolically
 // quot()                   >  '$NODENAME.dot';
 dead                     := any_s - src(any_t - self_epsilon);
 //notCFC                   := any_t - loop(any_t,any_t);
 nonControle              := epsilon;
 controle                 := any_t - nonControle;
 ER                       := dead;

/* Les equations classiques
 *  - de strategies gagnantes
 *  - et de synthese de controleurs
 *  coupA represente les actions du joueur A ou du controleur
 *  coupB represente les actions du joueur B ou du systeme
 *  Gagne represente des configurations de A
 *    - les configurations a atteindre lors d'un pppf
 *    - le complementaire des configurations a eviter lors d'un pgpf
 *  Perdu represente des configurations de B
 *    - les configurations a eviter lors d'un ppf
 *    - le complementaire des configurations a eviter lors d'un pgpf
 Gagnant                = Gagne |& \\ | pour pppf et & pour pgpf
                          src(CoupGagnant);
 CoupGagnant            = coupA & rtgt(Perdant);
 Perdant                = Perdu |& \\ | pour pppf et & pour pgpf
                          (src(CoupPerdant) - src(CoupNonPerdant));
 CoupPerdant            = coupB & rtgt(Gagnant);
 CoupNonPerdant         = coupB - rtgt(Gagnant);
*/

 // les actions de controle pour eviter ER : pgpf
 GagneER := src(controle) - ER;
 PerduER := src(nonControle) - ER;
 CtrlER  -= controle &
            rtgt(PerduER &
                 (src(nonControle & rtgt(GagneER & src(CtrlER)))-
                  src(nonControle - rtgt(GagneER & src(CtrlER)))));

 // Le systeme est-il controlable
 ControlableER            := initial & src(CtrlER);
 // les erreurs de commande possibles
 ErreurControleER         := controle - CtrlER;
 // Generation des controleurs
 project(any_s, CtrlER, '$NODENAME_Controleur', true);
 
 // les actions de controle pour eviter ER et optimiser : pgpf
 AttenteT1 := [T1.vitesse = 0 & T1.canton != T1.cc & T1.canton != T1.cd];
 AttenteT2 := [T2.vitesse = 0 & T2.canton != T2.cc & T2.canton != T2.cd];
 AttenteT3 := [T3.vitesse = 0 & T3.canton != T3.cc & T3.canton != T3.cd];
 GagneEROpt := src(controle) - (ER | AttenteT1 | AttenteT2 | AttenteT3);
 PerduEROpt := src(nonControle) - (ER | AttenteT1 | AttenteT2 | AttenteT3);
 CtrlEROpt  -= controle &
            rtgt(PerduEROpt &
                 (src(nonControle & rtgt(GagneEROpt & src(CtrlEROpt)))-
                  src(nonControle - rtgt(GagneEROpt & src(CtrlEROpt)))));

 // Le systeme est-il controlable
 ControlableEROpt            := initial & src(CtrlEROpt);
// ControlableEROpt_CFC        := loop(CtrlEROpt,CtrlEROpt);
 // les erreurs de commande possibles
 ErreurControleEROpt         := controle - CtrlEROpt;
 // Generation des controleurs
 project(any_s, CtrlEROpt, '$NODENAME_Controleur_Opt', true);
 
 show(all)                >  '$NODENAME.prop';
 test(dead,0)             >  '$NODENAME.res';
// test(notCFC,0)           >> '$NODENAME.res';
 test(ControlableER,1)           >> '$NODENAME.res';
 test(ErreurControleER,0)           >> '$NODENAME.res';
 test(ControlableEROpt,1)           >> '$NODENAME.res';
 test(ErreurControleEROpt,0)           >> '$NODENAME.res';
done
