with A, B do symbolically
  toto := any_s-src(any_t);
  show (all, any_s, toto);
done
