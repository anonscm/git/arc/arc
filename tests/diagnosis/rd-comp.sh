#! /bin/bash -x

topdir=$(dirname $0)

if test -x ${topdir}/common.sh;
then
    . ${topdir}/common.sh;
else
    echo 1>&2 "$0: cannot find common.sh"
    exit 1    
fi

case ${OUTPUT_FILE} in
    *.diag-ar)
	ALGO=diag 
	;;
    *.cuts-ar)
	ALGO=cuts
	;;
    *)
	fail "$0: invalid filename extension; it should terminate with diag-ar or cuts-ar"
	;;
esac

${ARCCMD} -q <<EOF 
load ${INPUT_FILE}

echo "set garbage-collector-period 100000000; " > "${OUTPUT_FILE}"
echo "set maximum-page-number 50000; " >> "${OUTPUT_FILE}"
timer start ${ALGO}_root
${ALGO} --visible-tags=failure --prefix=${ALGO}_ Main root >> "${OUTPUT_FILE}"
timer stop ${ALGO}_root
timer print ${ALGO}_root 
EOF

if test $? != 0 -o ! -s "${OUTPUT_FILE}";
then
    fail "$0: ${ARC} fails in computation of '${OUTPUT_FILE}'"
fi

test -s ${OUTPUT_FILE}.mchk && fail "test fail on memory leak; see ${OUTPUT_FILE}.mchk" || exit 0



