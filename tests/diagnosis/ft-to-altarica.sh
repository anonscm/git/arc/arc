#! /bin/bash 

topdir=$(dirname $0)

if test -x ${topdir}/common.sh;
then
    . ${topdir}/common.sh;
else
    echo 1>&2 "$0: cannot find common.sh"
    exit 1    
fi

${ARALIA} "${INPUT_FILE}" 2>&1 < /dev/null > /dev/null || fail "$0: input file '${INPUT_FILE}' is not readable by ${ARALIA}"

ROOT=$(ft_root ${INPUT_FILE})

cat > ${OUTPUT_FILE} <<EOF
#include "${srcdir}/bf-nodes.alt"

node Main
  flow
$(ft_gates ${INPUT_FILE}) : bool;

  sub 
$(ft_nodes ${INPUT_FILE}) : BooleanVariable;

  assert
$(sed -e 's/@(2,\[\([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\)\])/AT_2_AMONG_3(\1, \2, \3)/g' -e 's/@(2,\[\([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\)\])/AT_2_AMONG_4(\1, \2, \3, \4)/g' -e 's/@(3,\[\([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\)\])/AT_3_AMONG_5(\1, \2, \3, \4, \5)/g' -e 's/@(3,\[\([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\), \([a-z][0-9][0-9]*\)\])/AT_3_AMONG_4(\1, \2, \3, \4)/g' -e 's/:=/=/' -e 's/-/~/g' -e 's/\([^ _rfg0-9][0-9][0-9]*\)/\1.ko/g' ${INPUT_FILE}) 

  flow root : bool;
  assert root = $ROOT
edon
EOF

exit $?
