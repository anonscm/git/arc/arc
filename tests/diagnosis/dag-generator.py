#! /usr/bin/env python

import sys
import argparse
import random

G_nb_nodes = 0
G_succ = dict()
G_pred = dict()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed", dest='seed', type=int)
    parser.add_argument("--min-depth", dest='d', type=int, default=1)
    parser.add_argument("--max-depth", dest='D', type=int, default=5)
    parser.add_argument("--min-per-rank", dest='r', type=int, default=1)
    parser.add_argument("--max-per-rank", dest='R', type=int, default=10)
    parser.add_argument("--edge-density", dest='E', type=int, default=10)
    parser.add_argument("-o", "--outfile", type=argparse.FileType('w'),
                        default=sys.stdout)
    parser.add_argument("--only-flow", action='store_true', default=False)
    parser.add_argument("--with-invisibles", action='store_true', default=False)
    parser.add_argument("--dotfile", type=argparse.FileType('w'))
    return parser.parse_args()

def generate_graph(d, D, r, R, E):
    global G_nb_nodes, G_succ, G_pred
    ranks = random.randint(d,D)
    print ("# generating {} layers".format(ranks))

    for r in range (ranks):
        nb_new_nodes = random.randint (r, R)
        for s in range (G_nb_nodes, G_nb_nodes + nb_new_nodes):
            G_succ[s] = []
            G_pred[s] = []

        print ("# rank {}: adding {} nodes".format(r, nb_new_nodes))
        for src in range (G_nb_nodes):
            for tgt in range (G_nb_nodes, G_nb_nodes + nb_new_nodes):
                if random.randint(0,100) < E:
                    G_succ[src] += [tgt]
                    G_pred[tgt] += [src]
        G_nb_nodes += nb_new_nodes

def dot_graph(out):
    global G_nb_nodes, G_succ
    out.write("digraph {\n");
    for n in range(G_nb_nodes):
        out.write ("  {};\n".format(n))
        for s in G_succ[n]:
            out.write ("  {} -> {};\n".format(n, s))
    out.write("}");

def roots():
    global G_nb_nodes, G_pred
    root_nodes = []
    for n in range (G_nb_nodes):
        if not G_pred[n]:
            root_nodes += [ n ]
    return root_nodes

def generate_bcond_node (out, nb_inputs, nb_outputs, op, visible):
    out.write ("// bcond {}\n".format (op));
    out.write ("  state s : { nominal, hs } : public;\n"
               "  init s := nominal;\n")
    out.write ("  event fail {}; \n".format(visible))
    out.write ("  trans s = nominal");
    if nb_inputs > 0:
        out.write (" & (i[0]");
        for i in range(1, nb_inputs):
            out.write (" {} i[{}]".format (op, i))
        out.write (")")
    out.write (" |- fail -> s := hs\n");

    for i in range(nb_outputs):
        out.write ("  assert o[{}] = (s != nominal);\n".format (i))

def generate_flow_node (out, nb_inputs, nb_outputs, op, visible):
    out.write ("// flow {}\n".format (op));
    out.write ("  state s : { nominal, hs } : public;\n"
               "  init s := nominal;\n")
    out.write ("  event fail {}; \n".format(visible))
    out.write ("  trans s = nominal |- fail -> s := hs;\n");
    if nb_inputs == 0:
        cond = "(s = nominal)"
    else:
        cond = "((s = nominal) & (i[0]"
        for i in range(1, nb_inputs):
            cond += " {} i[{}]".format (op, i)
        cond += "))"
    for i in range(nb_outputs):
        out.write ("  assert o[{}] = {};\n".format (i, cond))

def generate_guarded_flow_node (out, nb_inputs, nb_outputs, op, visible):
    if nb_inputs != nb_outputs:
        generate_flow_node (out, nb_inputs, nb_outputs, op, visible)
        return
    out.write ("// guarded flow\n");
    out.write ("  state s : { nominal")
    for m in range(0, nb_outputs):
        out.write (", hs{}".format(m))
    out.write ("} : public;\n"
               "  init s := nominal;\n"
               "  event fail0");
    for m in range(1, nb_outputs):
        out.write (", fail{}".format(m))
    out.write ("{}; \n".format (visible))
    for m in range(nb_outputs):
        out.write ("  trans s = nominal {op} i[{m}] |- fail{m} -> s := hs{m};\n".format (m=m,op=op));
    for m in range(nb_outputs):
        out.write ("  assert o[{m}] = (s=hs{m});\n".format (m=m))

def generate_daisy_node (out, nb_inputs, nb_outputs, op, visible):
    assert (nb_outputs > 0)
    out.write ("// daisy\n");
    out.write ("  state s : { nominal")
    for m in range(0, nb_outputs):
        out.write (", hs{}".format(m))
    out.write ("} : public;\n"
               "  init s := nominal;\n"
               "  event fail0");
    for m in range(1, nb_outputs):
        out.write (", fail{}".format(m))
    out.write ("{}; \n".format (visible))
    for m in range(nb_outputs):
        out.write ("  trans s = nominal |- fail{m} -> s := hs{m};\n".format (m=m));
    for m in range(nb_outputs):
        out.write ("  assert o[{m}] = ((s=hs{m})".format (m=m))
        if nb_inputs > 0:
            out.write (" {} i[{}]".format(op, random.randint(0, nb_inputs-1)))
        out.write (");\n")

def generate_daisy2_node (out, nb_inputs, nb_outputs, op, visible):
    if nb_inputs == 0:
        generate_flow_node (out, nb_inputs, nb_outputs, op, visible)
        return
    out.write ("// daisy2\n");
    out.write ("  state s : { nominal")
    for m in range(0, nb_inputs):
        out.write (", hs{}".format(m))
    out.write ("} : public;\n"
               "  init s := nominal;\n"
               "  event fail0");
    for m in range(1, nb_inputs):
        out.write (", fail{}".format(m))
    out.write ("{}; \n".format(visible))
    for m in range(nb_inputs):
        out.write ("  trans s = nominal |- fail{m} -> s := hs{m};\n".format (m=m));
    for m in range(nb_outputs):
        out.write ("  assert o[{m}] = ((s!=nominal) {op} i[{i}]);\n".format (m=m,
                                                                             op=op,
                                                                             i=random.randint (0, nb_inputs - 1)))
        
def generate_degraded_node (out, nb_inputs, nb_outputs, op, visible):
    assert (nb_outputs > 0)
    out.write ("// degraded\n");
    out.write ("  state s : { nominal")
    for m in range(0, nb_outputs):
        out.write (", hs{}".format(m))
    out.write ("} : public;\n"
               "  init s := nominal;\n"
               "  event fail0");
    for m in range(1, nb_outputs):
        out.write (", fail{}".format(m))
    out.write (" {}; \n".format (visible))
    for m in range(nb_outputs):
        out.write ("  trans s = nominal |- fail{m} -> s := hs{m};\n".format (m=m));
    for m in range(nb_outputs - 1):
        out.write ("  trans s = hs{m} |- fail{max} -> s := hs{max};\n".format (m=m, max=nb_outputs-1));
    for m in range(nb_outputs):
        out.write ("  assert o[{m}] = ((s=hs{m})".format (m=m))
        if nb_inputs > 0:
            out.write (" {} i[{}]".format(op, random.randint(0, nb_inputs-1)))
        out.write (");\n")

def generate_degraded2_node (out, nb_inputs, nb_outputs, op, visible):
    assert (nb_outputs > 0)
    out.write ("// degraded2\n");
    out.write ("  state s : { nominal")
    for m in range(0, nb_outputs):
        out.write (", hs{}".format(m))
    out.write ("} : public;\n"
               "  init s := nominal;\n"
               "  event fail0");
    for m in range(1, nb_outputs):
        out.write (", fail{}".format(m))
    out.write (" {}; \n".format (visible))
    out.write ("  trans s = nominal |- fail{m} -> s := hs{m};\n".format (m=0));
    for m in range(1, nb_outputs):
        out.write ("  trans s = hs{p} |- fail{m} -> s := hs{m};\n".format (p=m-1,m=m));
    for m in range(nb_outputs):
        out.write ("  assert o[{m}] = ((s=hs{m})".format (m=m))
        if nb_inputs > 0:
            out.write (" {} i[{}]".format(op, random.randint(0, nb_inputs-1)))
        out.write (");\n")

        
def generate_nodes(out, nodetypes, behaviours, with_invisibles):
    global G_nb_nodes, G_pred, G_succ

    cache = dict ()
    nodes = []
    
    for n in range (G_nb_nodes):
        indegree = len (G_pred[n])
        outdegree = len (G_succ[n])
        k = indegree * G_nb_nodes + outdegree
        if k not in cache.keys():
            if indegree == 0:
                nodetype = "Topnode"
            else:
                nodetype = "N_{}_{}".format (indegree, outdegree)
            cache[k] = nodetype
            out.write ("node {}\n".format(nodetype))            
            if outdegree > 0:
                out.write ("  flow i : bool[{}];\n".format(outdegree))
            if indegree == 0:
                indegree = 1
            out.write ("  flow o : bool[{}];\n".format (indegree))
            spec = Behaviours[random.randint(0, len (Behaviours)-1)]

            visible = ": failure"
            if with_invisibles and random.randint(0, 1) == 1:
                visible = ""
            spec (out, outdegree, indegree, visible)
            out.write ("edon\n\n")            
        nodes += [ cache[k] ]
        if cache[k] not in nodetypes.keys():
            nodetypes[cache[k]] = []
        nodetypes[cache[k]] += [ n ]
    return nodes
    
def generate_model(out, behaviours, with_invisibles):
    global G_nb_nodes, G_pred, G_succ

    nodetypes = dict ()
    nodes = generate_nodes (out, nodetypes, behaviours, with_invisibles)
    out.write ("node Main\n"
               " sub \n");

    for t in nodetypes.keys():
        first = True
        for n in nodetypes[t]:
            if first:
                out.write ("  n{}".format (n))
                first = False
            else:
                out.write (", n{}".format (n))
        out.write (" : {};\n".format (t))

    out.write (" flow root : bool;\n"
               " assert\n"
               "  root = {} n{}.o[0];\n".format (("", "not")[random.randint(0,1)], G_nb_nodes - 1))
    
    for n in range (G_nb_nodes):
        for i in range(len(G_succ[n])):
            succ = G_succ[n][i]
            index = G_pred[succ].index (n)
            out.write ("  n{}.i[{}] = n{}.o[{}];\n".format (n, i, succ, index))
            G_pred[succ][index] = -n
    out.write ("edon\n");

args = parse_args()
if args.seed:
    print("# changing PRNG seed to {}.".format(args.seed))
    random.seed (args.seed)

if args.d > args.D:
    args.D += args.d
    print("# max-depth is less than min-depth; changing it to {}.".format(args.D))
    
if args.r > args.R:
    args.R += args.r
    print("# max-per-rank is less than min-per-rank; changing it to {}.".format(args.R))  

Behaviours = [lambda x,y,z,v: generate_flow_node(x, y, z, "|",v),
              lambda x,y,z,v: generate_flow_node(x, y, z, "&",v)]
if not args.only_flow:
    Behaviours += [ lambda x,y,z,v: generate_bcond_node(x, y, z, "|",v),
                    lambda x,y,z,v: generate_bcond_node(x, y, z, "&",v),
                    lambda x,y,z,v: generate_bcond_node(x, y, z, "| not",v),
                    lambda x,y,z,v: generate_bcond_node(x, y, z, "& not",v) ]
    Behaviours += [ lambda x,y,z,v: generate_daisy2_node(x, y, z, "|",v),
                    lambda x,y,z,v: generate_daisy2_node(x, y, z, "&",v)]
    Behaviours += [ lambda x,y,z,v: generate_daisy_node(x, y, z, "& not",v),
                    lambda x,y,z,v: generate_daisy_node(x, y, z, "&",v),
                    lambda x,y,z,v: generate_daisy_node(x, y, z, "|",v),
                    lambda x,y,z,v: generate_daisy_node(x, y, z, "| not",v) ]
    Behaviours += [ lambda x,y,z,v: generate_degraded_node(x, y, z, "& not",v),
                    lambda x,y,z,v: generate_degraded_node(x, y, z, "&",v),
                    lambda x,y,z,v: generate_degraded_node(x, y, z, "|",v),
                    lambda x,y,z,v: generate_degraded_node(x, y, z, "| not",v) ]
    Behaviours += [ lambda x,y,z,v: generate_degraded2_node(x, y, z, "& not",v),
                    lambda x,y,z,v: generate_degraded2_node(x, y, z, "&",v),
                    lambda x,y,z,v: generate_degraded2_node(x, y, z, "|",v),
                    lambda x,y,z,v: generate_degraded2_node(x, y, z, "| not",v) ]
    Behaviours += [ lambda x,y,z,v: generate_guarded_flow_node(x, y, z, "& not",v),
                    lambda x,y,z,v: generate_guarded_flow_node(x, y, z, "&",v),
                    lambda x,y,z,v: generate_guarded_flow_node(x, y, z, "|",v),
                    lambda x,y,z,v: generate_guarded_flow_node(x, y, z, "| not",v) ]

generate_graph(args.d, args.D, args.r, args.R, args.E)

G_pred[G_nb_nodes] = []
G_succ[G_nb_nodes] = roots ()
for r in G_succ[G_nb_nodes]:
    G_pred[r] = [G_nb_nodes]
G_nb_nodes += 1

if args.dotfile:
    dot_graph(args.dotfile)

print ("# {} nodes generated.".format (G_nb_nodes))

generate_model(args.outfile, Behaviours, args.with_invisibles)
