#!/bin/bash 

topdir=$(dirname $0)

if test -x ${topdir}/common.sh;
then
    . ${topdir}/common.sh;
else
    echo 1>&2 "$0: cannot find common.sh"
    exit 1    
fi

function cmpres()
{
    local res
    for f in $1 $2
    do
	sed -e 's/diag_\(n_\)*root/cuts_\1root/g' -e 's/BDD BDD.*$//g' ${f} > ${f}.tmp$$
    done
    diff $1.tmp$$ $2.tmp$$ >> ${TESTNAME}.log 2>&1
    res=$?
    rm -f $1.tmp$$  $2.tmp$$
    return $res
}


if test "x${GENCUTS}" = "x";
then
    GENCUTS=false
fi

res=true
TESTNAME=$(basename ${INPUT_FILE} .alt)
DIAG_IN=${TESTNAME}.diag-ar
DIAG_OUT=${TESTNAME}.diag
CUTS_IN=${TESTNAME}.cuts-ar
CUTS_OUT=${TESTNAME}.cuts
    
make -j ${DIAG_IN} ${CUTS_IN} > ${OUTPUT_FILE} 2>&1 || res=false

if $res
then
    echo "### Aralia computation" >> ${OUTPUT_FILE}
    ${ARALIA} > ${OUTPUT_FILE} 2> ${TESTNAME}.ar-err <<EOF
load "${DIAG_IN}";
load "${CUTS_IN}";
compute ZPC roots;

display size ZPC diag_root > "${DIAG_OUT}";
display size ZPC cuts_root > "${CUTS_OUT}";

display #products ZPC diag_root >> "${DIAG_OUT}";
display #products ZPC cuts_root >> "${CUTS_OUT}";

display computations diag_root >> "${DIAG_OUT}";
display computations cuts_root >> "${CUTS_OUT}";

$($GENCUTS && echo "display products ZPC diag_root >> \"${DIAG_OUT}\";")
$($GENCUTS && echo "display products ZPC cuts_root >> \"${CUTS_OUT}\";")
quit;
EOF
    exitcode=$?
    if test -s ${TESTNAME}.ar-err
    then
	(echo "Aralia generates an error in ${TESTNAME}.ar-err:";
	 cat ${TESTNAME}.ar-err) >> ${OUTPUT_FILE}
	res=false
    else
	if test ${exitcode} != 0
	then
	   (echo "Aralia generates an error (${exitcode})."; cat ${TESTNAME}.ar-err) >> ${OUTPUT_FILE}
	   res=false
	else
  	    cmpres ${DIAG_OUT} ${CUTS_OUT} || res=false
	fi
    fi
fi

exec $res


