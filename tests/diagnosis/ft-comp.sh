#! /bin/bash -x

topdir=$(dirname $0)

if test -x ${topdir}/common.sh;
then
    . ${topdir}/common.sh;
else
    echo 1>&2 "$0: cannot find common.sh"
    exit 1    
fi

${ARCCMD} -q <<EOF 
set arc.shell.preprocessor.default.command cpp

load ${INPUT_FILE}

diag --visible-tags=failure Main root > "${OUTPUT_FILE}.tmp"
EOF

if test $? = 0 -a -s "${OUTPUT_FILE}.tmp";
then
    sed -e "s/\`\([A-Za-z0-9]*\)\.fail'/\1/g" "${OUTPUT_FILE}.tmp" > "${OUTPUT_FILE}"
else
    rm -f "${OUTPUT_FILE}.tmp"
    fail "$0: ${ARC} fails in computation of '${OUTPUT_FILE}'"
fi
rm -f "${OUTPUT_FILE}.tmp"

test -s ${OUTPUT_FILE}.mchk && fail "test fail on memory leak; see ${OUTPUT_FILE}.mchk" || exit 0



