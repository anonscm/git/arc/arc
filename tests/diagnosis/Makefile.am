#
# Default parameters used to generate random DAG models
#
TIMEOUT	     = 300
MIN_DEPTH    = 5
MAX_DEPTH    = 10
MIN_PER_RANK = 5
MAX_PER_RANK = 20
EDGE_DENSITY = 20

SUFFIXES  = .dag .dag.alt .dag.ar .dag.res \
            .alt. .diag-ar .cuts-ar .check 

CLEANEXTENSIONS = .dag.alt .dag.ar .dag.mchk .dag.res .mchk \
                  .diag-ar .cuts-ar .cuts .diag .ar-err .check \
                  .log .test

CLEANFILES  = 

TESTERS = fault-trees.test rnddag-00.test 

if WITH_ALL_TESTS
TESTERS += rnddag-01.test rnddag-02.test rnddag-03.test \
           rnddag-04.test rnddag-05.test rnddag-06.test rnddag-07.test \
           rnddag-08.test rnddag-09.test rnddag-10.test rnddag-11.test \
           rnddag-12.test rnddag-13.test rnddag-14.test 
endif

if HAS_ARALIA
TESTS = ${TESTERS}
endif

#
# Setting TAP driver 
#==============================================================================
TEST_LOG_DRIVER = env AM_TAP_AWK='$(AWK)' $(SHELL) $(top_srcdir)/tap-driver.sh

EXTRA_DIST = $(TESTERS) \
             dag-generator.py \
             common.sh \
             ft-comp.sh ft-check.sh ft-to-altarica.sh \
             rd-check.sh rd-comp.sh \
             $(wildcard *.dag) \
             $(wildcard *.alt) 

AM_TESTS_ENVIRONMENT = ulimit -t ${TIMEOUT} ; \
  MAKE="${MAKE}"; \
  MEMCHECK="${MEMCHECK}"; \
  GENCUTS="${GENCUTS}"; \
  ARC="${top_builddir}/src/arc"; \
  ARALIA="${ARALIA}"; \
  srcdir="${srcdir}" ; \
  export MAKE MAKEFLAGS ARC ARALIA MEMCHECK GENCUTS srcdir; 

memcheck :
	make MEMCHECK="${LIBTOOL} exec memcheck" check

clean-local:
	for ext in ${CLEANEXTENSIONS}; \
        do \
           for f in *$${ext}; \
           do \
             rm -f $${f}; \
           done; \
        done
#
# Compilation rules for fault-tree based tests
#============================================================================== 
.dag.dag.alt:
	@ ${AM_TESTS_ENVIRONMENT} ${srcdir}/ft-to-altarica.sh $< $@

.dag.alt.dag.ar:
	@ ${AM_TESTS_ENVIRONMENT} ${srcdir}/ft-comp.sh $< $@

.dag.ar.dag.res:
	@ ${AM_TESTS_ENVIRONMENT} ${srcdir}/ft-check.sh $< $@

#
# Compilation rules for random DAG models
#============================================================================== 

DAGGEN=${PYTHON} ${srcdir}/dag-generator.py 
DAGGEN_FLAGS=--with-invisibles \
             --min-depth $(MIN_DEPTH) \
             --max-depth $(MAX_DEPTH) \
             --min-per-rank $(MIN_PER_RANK) \
             --max-per-rank $(MAX_PER_RANK) \
             --edge-density $(EDGE_DENSITY)

${srcdir}/${RNDDAG_PREFIX}-%.alt : 
	${DAGGEN} ${DAGGEN_FLAGS} --outfile $@ --dotfile $$(basename $@ .alt).dag.dot

.alt.diag-ar:
	@ ${AM_TESTS_ENVIRONMENT} ${srcdir}/rd-comp.sh $< $@

.alt.cuts-ar:
	@ ${AM_TESTS_ENVIRONMENT} ${srcdir}/rd-comp.sh $< $@

.alt.check:
	@ ${AM_TESTS_ENVIRONMENT} ${srcdir}/rd-check.sh $< $@

rnddag-%.test : rnddag.test
	@ ln -s ${srcdir}/rnddag.test $@

