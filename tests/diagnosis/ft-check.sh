#! /bin/bash 

topdir=$(dirname $0)

if test -x ${topdir}/common.sh;
then
    . ${topdir}/common.sh;
else
    echo 1>&2 "$0: cannot find common.sh"
    exit 1    
fi

TEST_FILE=${srcdir}/$(basename ${INPUT_FILE} .ar)

test -f ${TEST_FILE} || fail "$0: missing test file '${TEST_FILE}'"
ROOT=$(ft_root ${TEST_FILE})

${ARALIA} <<EOF
load "${TEST_FILE}";
load "${INPUT_FILE}";

R := (root = $ROOT);

compute BDD R;
display nodes BDD R > "${OUTPUT_FILE}";
EOF

test $? = 0 || fail "$0: ${ARALIA} fails in computation of '${OUTPUT_FILE}'"

test -f "${OUTPUT_FILE}" || fail "$0: internal error; the file '${OUTPUT_FILE}' is missing."

exec cmp -s ${OUTPUT_FILE} ${srcdir}/one.res 
