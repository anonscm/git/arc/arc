#
# Initializaiton script for diagnosis tests
#

fail() {
    echo "$1" 1>&2 
    exit 1
}

ft_aralia_cmd() {
    echo "$1" | ${ARALIA} "$2" | awk 'BEGIN { A=0 }; /^aralia > $/ { if (A == 1) A=0; else A=1; next; }; /^ *$/ { next; } { if (A == 1) print $0 };'
}

ft_root() {
    ft_aralia_cmd "display roots;" "$1"
}

ft_nodes() {
    ft_aralia_cmd "display leaves;" "$1"
}

ft_gates() {
    ft_aralia_cmd "display  ancestors(*)/leaves(*);" "$1"
}

test $# = 2 || fail "$0: wrong #of arguments"
INPUT_FILE="$1"
OUTPUT_FILE="$2"

test "x${INPUT_FILE}" = "x" && fail "$0: missing test input file"
test -f ${INPUT_FILE} || fail "$0: missing file ${INPUT_FILE}"
test "x${ARALIA}" = "x" && fail "$0: variable ARALIA is not set"
test "x${ARC}" = "x" && fail "$0: variable ARC is not set"
test "x${srcdir}" = "x" && fail "$0: variable srcdir is not set"

if test "x${MEMCHECK}" != "x";
then
    ARCCMD="${MEMCHECK} -q --log-file=${OUTPUT_FILE}.mchk --leak-check=full ${ARC}"
else
    ARCCMD=${ARC}
fi

