<?php 
$var = $argv[1]; 
$formula = $var;

if (preg_match ("/^.*\.n_[0-9]*$/", $var)) {
  $formula = "not (" . $formula . ")";
}
?>

cuts --enum --min --visible-tags=_13 _3814 "<?php echo "$formula"; ?>" > "<?php echo "$var"; ?>.mincuts" 
