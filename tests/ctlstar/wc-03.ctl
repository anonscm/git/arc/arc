node A 
 event to_p, to_q, '$';
 state s : [0,4]; init s := 0;
 trans s < 3 |- to_p -> s := s+1;
       s = 3 |- to_p -> s := 0;
       s <= 3 |- to_q -> s := 4;
edon 

with A do 
  p := [s != 4];
  q := [s = 4];
done

chkctl A "EG((p & EX q))"
