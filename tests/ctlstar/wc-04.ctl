load wc-04.ts

with A do 
  p := [s != 2];
  q := [s = 2];
done


chkctl A "E [ not (p U q) ]"

