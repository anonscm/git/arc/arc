load wc-06.ts

with B do 
  p := [s != 2 & s != 3];
  q := [s = 2];
done

chkctl B "initial => AG (p or q)"
