node A 
 event e;
 state s : [0,4]; init s := 0;
 trans s = 0 |- e -> s := 1
             |- e -> s := 2
             |- e -> s := 3;
       s = 2 |- e -> s := 3;
edon 

with A do 
  p := [s = 2];
  q := [s = 3];
done

chkctl A "EX (p & EX q)"

