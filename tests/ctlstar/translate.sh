#!/bin/bash 

if test "x${ARC}" = "x"; then
   echo "'ARC' environment variable is not set" 1>&2
   exit 1
fi

unset DISPLAY
MEMCHECK_FLAGS="-q --num-callers=20 --leak-check=yes"

if test $# = 0; then
    exit 0
fi

if test "x${MEMCHECK}" != "x"; then
    if test -f memcheck.supp; then
	MEMCHECK_FLAGS="$MEMCHECK_FLAGS --suppressions=memcheck.supp"
    fi

    MEMCHECK="${MEMCHECK} ${MEMCHECK_FLAGS}"    
fi

INPUT="$1"
BASENAME=$(basename ${INPUT} .ctlstar)
PREFIX=$(echo ${BASENAME} | sed -e 's/formula/translation/')
OUTPUT=${PREFIX}.out
ERROR=${PREFIX}.err
EXPECTED_OUTPUT=${srcdir}/expected-${PREFIX}.out
EXPECTED_ERROR=${srcdir}/expected-${PREFIX}.err

test -f ${EXPECTED_OUTPUT} || EXPECTED_OUTPUT=/dev/null
test -f ${EXPECTED_ERROR} || EXPECTED_ERROR=/dev/null

if test -n "$MEMCHECK"; then
    MESSAGE="memory checking for translation of ${BASENAME}"
else
    MESSAGE="checking translation for ${BASENAME}"
fi

echo ${MESSAGE}

FORMULA=$(cat ${INPUT})

SPECFILE="${BASENAME}.spec"

cat > ${SPECFILE} <<EOF 
node A 
edon

with A do 
   symbolically  
   p := any_s;
   q := any_s;
   r := any_s;
   s := any_s;
   ctl2mu (${FORMULA});
   R := ctlspec ${FORMULA};
   show (all);
done
EOF

if test -x /usr/bin/setarch; then
    ARCH=`uname -p`
    SETARCH="/usr/bin/setarch ${ARCH} -R"
fi

if eval ${SETARCH} ${MEMCHECK} ${ARC} -qb ${SPECFILE} > ${OUTPUT} 2> ${ERROR}; then
    retval=0
    if cmp -s ${ERROR} ${EXPECTED_ERROR}; then
#	if test -z "${MEMCHECK}"; then
	    if ! cmp -s ${OUTPUT} ${EXPECTED_OUTPUT}; then
		echo "output files differ. see ${BASENAME}.diff." >&2
		diff ${OUTPUT} ${EXPECTED_OUTPUT} > ${BASENAME}.diff
		retval=1
	    fi
#	fi      
    else
	retval=1
	echo "error files differ" >&2
    fi
else
    retval=1
    echo "arc returns with an error code" >&2
fi

if test ${retval} = 0; then
    rm -f ${SPECFILE}
fi

exit ${retval}
