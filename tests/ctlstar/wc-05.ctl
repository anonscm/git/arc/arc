load wc-05.ts

with B do 
  p := [s != 2 & s != 3];
  q := [s = 2];
done

chkctl B "E [ not (p U q) ]"
