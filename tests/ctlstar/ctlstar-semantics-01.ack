const N = 10;

node A
   state s : [0,5]; init s := 0;
   event a;
   trans s = 0 |- a -> s := 1;
         s = 0 |- a -> s := 2;
         s > 0 |- a -> s := s + 2;
edon


with A do
   symbolically
   ODD := [ (s mod 2) = 1 ];
   EVEN := [ (s mod 2) = 0 ];
   P1 := ctlspec AG ODD;
   P2 := ctlspec AG EVEN;
   Q1 := ctlspec EG ODD;
   Q2 := ctlspec EG EVEN;

   TEST(P1,3);
   TEST(P2,2);
   TEST(Q1,3);
   TEST(Q2,3);
done
