with System do
 NOMUTEX := [ (p0.s=CS) & (p1.s=CS) ];
 Z := NOMUTEX & any_s;
 X := coreach (Z, any_t);

 BAD := ctlspec E [ F NOMUTEX ];
 S := any_s & BAD;
 BAD_INIT := BAD & initial;
 show (all);
done
