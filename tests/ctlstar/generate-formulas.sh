#!/bin/bash

function createfiles() {
    FORMULA="$1"
    FILEINDEX="$2"
    for Q in "E" "A"; do
	F=`printf "formula-${Q}-%02d.ctlstar" ${FILEINDEX}`
	echo "${Q} [ ${FORMULA} ]" > ${F}
    done
}

mapfile -t formulas < formulas.ltl

let i=0
while [ $i -lt ${#formulas[@]} ]; do
    createfiles "${formulas[$i]}" "${i}"
    let i=$i+1
done

