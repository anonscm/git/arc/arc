node A 
 flow f : bool;
 state s : bool; init s := false;
 assert f => s; s => f;
edon 

with A do 
  q := any_s; 
  p := any_s; 
  r := any_s;
done

chkctl A "AG EF p"
