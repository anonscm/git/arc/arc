node A
   state s : [0,6]; init s := 0;
   event a;
   trans s = 0 |- a -> s := 1;
         s = 1 |- a -> s := 2;
         s = 1 |- a -> s := 4;
         s = 2 |- a -> s := 3;
         s = 3 |- a -> s := 0;
         s = 3 |- a -> s := 6;
         s = 4 |- a -> s := 5;
edon


with A do
   symbolically

   P := [s = 3 | s = 6];
   can_do_reset := ctlspec A [ F G P ];
//   show (all);
   show(all);
done



