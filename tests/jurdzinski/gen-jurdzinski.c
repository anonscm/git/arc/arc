/*	$Id: gen-jurdzinski.c,v 1.2 2008/10/13 09:32:13 point Exp $ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct node {
    int x;
    int y;
};

/*
 * Dot graph generation for GraphViz
 */

static void
init_dot(int l, int b) {
    printf("digraph h%d_%d {\n", l, b);
}

static void
fini_dot(void) {
    printf("}\n");
}

static void
print_dot(int universal, int parity, struct node cur,
						int nsucc, struct node succ[]) {
    int i;

    printf("\tnode%d_%d [ pos=\"%d,%d\",label=\"%d\",shape=\"%s\" ];\n",
	cur.x, cur.y, cur.x, cur.y, parity, universal? "box" : "diamond");

    for (i = 0; i < nsucc; i++)
	printf("\tnode%d_%d -> node%d_%d;\n",
	    cur.x, cur.y, succ[i].x, succ[i].y);
}

/*
 * Spec file generation for Mec 5
 */

static char *spec_proto;
static char *spec_invoke;

#define SPEC_FORMAT "t%d_%d_%d_%d"
#define SPEC_TYPE ":bool"
#define SPEC_SEPARATOR ", "
#define SPEC_ALL SPEC_FORMAT SPEC_TYPE SPEC_SEPARATOR

static void
init_spec(int l, int b) {
    char *specp;
    char *speci;
    int x;
    int y;

    /* Conservative: some positions have only two successors */
    specp = spec_proto = malloc(3 + b * l * 3 *
	(sizeof SPEC_ALL + 4 * (10 - 2)));
    speci = spec_invoke = malloc(3 + b * l * 3 *
	(sizeof SPEC_FORMAT + sizeof SPEC_SEPARATOR + 4 * (10 - 2)));

    *specp++ = '(';
    *speci++ = '(';

    for (y = 2; y < 2 * l; y += 2) {
	for (x = 1; x < 2 * b; x += 2) {
	    specp += sprintf(specp, SPEC_ALL, x, y, x + 1, y);
	    specp += sprintf(specp, SPEC_ALL, x, y, x - 1, y);
	    specp += sprintf(specp, SPEC_ALL, x, y, x, 0);
	    speci += sprintf(speci, SPEC_FORMAT SPEC_SEPARATOR, x, y, x + 1, y);
	    speci += sprintf(speci, SPEC_FORMAT SPEC_SEPARATOR, x, y, x - 1, y);
	    speci += sprintf(speci, SPEC_FORMAT SPEC_SEPARATOR, x, y, x, 0);
	}
    }

    for (x = 2; x <= 2 * b - 2; x += 2) {
	specp += sprintf(specp, SPEC_ALL, x, 0, x - 1, 0);
	specp += sprintf(specp, x == 2 * b - 2? SPEC_FORMAT SPEC_TYPE :
		SPEC_ALL, x, 0, x + 1, 0);
	speci += sprintf(speci, SPEC_FORMAT SPEC_SEPARATOR, x, 0, x - 1, 0);
	speci += sprintf(speci, x == 2 * b - 2? SPEC_FORMAT :
		SPEC_FORMAT SPEC_SEPARATOR, x, 0, x + 1, 0);
    }

    *specp++ = ')';
    *specp++ = '\0';
    *speci++ = ')';
    *speci++ = '\0';

    printf("begin\n");
}

static void
fini_spec(void) {
    printf("end\n");
}

static void
print_spec(int universal, int parity, struct node cur,
						int nsucc, struct node succ[]) {
    int i;

    printf("Q%d_%d%s %c%d= ", cur.x, cur.y, spec_proto, parity & 1? '+' : '-',
	parity / 2);

    if (nsucc == 1)
	printf("Q%d_%d%s", succ[0].x, succ[0].y, spec_invoke);
    else if (!universal) {
	printf("(");
	for (i = 0; i < nsucc; i++)
	    printf(i == nsucc - 1? SPEC_FORMAT : SPEC_FORMAT "|",
		cur.x, cur.y, succ[i].x, succ[i].y);
	printf(")");

	for (i = 0; i < nsucc; i++)
	    printf(" & (" SPEC_FORMAT " => Q%d_%d%s)",
		cur.x, cur.y, succ[i].x, succ[i].y, succ[i].x, succ[i].y,
		spec_invoke);
    } else {
	for (i = 0; i < nsucc; i++)
	    printf("Q%d_%d%s%s", succ[i].x, succ[i].y, spec_invoke,
		i == nsucc - 1? "" : " & ");
    }

    printf(";\n");
}

static void
do_gen(int l, int b, void (*print)(int, int, struct node, int, struct node *)) {
    int nsucc = l < 2? 3 : l + 1;
    /*int maxpar = 2 * l;*/
    struct node *succ = malloc(nsucc * sizeof *succ);
    struct node cur;
    int s;

    /*
     * The odd line, placed at y = 0
     */

    /* Eva */
    cur.x = 0;
    cur.y = 0;
    succ[0].x = 1;
    succ[0].y = 0;
    print(0, 0, cur, 1, succ);

    for (cur.x = 2; cur.x < 2 * b; cur.x += 2) {
	succ[0].x = cur.x - 1;
	succ[0].y = 0;
	succ[1].x = cur.x + 1;
	succ[1].y = 0;
	print(0, 0, cur, 2, succ);
    }

    succ[0].x = cur.x - 1;
    succ[0].y = 0;
    print(0, 0, cur, 1, succ);

    /* Adam */
    for (cur.x = 1; cur.x < 2 * b; cur.x += 2) {

	for (s = 0; s < l - 1; s++) {
	    succ[s].x = cur.x;
	    succ[s].y = 2 * s + 2;
	}

	succ[s].x = cur.x - 1;
	succ[s].y = 0;
	s++;
	succ[s].x = cur.x + 1;
	succ[s].y = 0;
	s++;
	print(1, 1, cur, s, succ);
    }

    /*
     * Odd lines are the "player doesn't matter" ones
     */

    for (cur.y = 1; cur.y < 2 * l - 2; cur.y += 2) {
	for (cur.x = 0; cur.x < 2 * b; cur.x += 2) {
	    succ[0].x = cur.x + 1;
	    succ[0].y = cur.y + 1;
	    print(0, cur.y + 2, cur, 1, succ);
	}
    }

    /*
     * Even lines
     */

    for (cur.y = 2; cur.y <= 2 * l - 2; cur.y += 2) {
	/* Eva */
	for (cur.x = 1; cur.x < 2 * b; cur.x += 2) {
	    succ[0].x = cur.x - 1;
	    succ[0].y = cur.y;
	    succ[1].x = cur.x + 1;
	    succ[1].y = cur.y;
	    succ[2].x = cur.x;
	    succ[2].y = 0;
	    print(0, cur.y, cur, 3, succ);
	}

	/* Adam */
	cur.x = 0;
	succ[0].x = cur.x;
	succ[0].y = cur.y - 1;
	succ[1].x = cur.x + 1;
	succ[1].y = cur.y;
	print(1, cur.y, cur, 2, succ);

	for (cur.x = 2; cur.x < 2 * b; cur.x += 2) {
	    succ[0].x = cur.x;
	    succ[0].y = cur.y - 1;
	    succ[1].x = cur.x - 1;
	    succ[1].y = cur.y;
	    succ[2].x = cur.x + 1;
	    succ[2].y = cur.y;
	    print(1, cur.y, cur, 3, succ);
	}
	succ[0].x = cur.x - 1;
	succ[0].y = cur.y;
	print(1, cur.y, cur, 1, succ);
    }
}

int
main(int argc, char *argv[]) {
    int l;
    int b;
    /* int i;*/

    if (argc != 4)
	goto usage;

    l = atoi(argv[2]);
    b = atoi(argv[3]);

    if (l < 1 || b < 2) {
	fprintf(stderr, "error: must have l >= 1 and b >= 2\n");
	exit(EXIT_FAILURE);
    }
	

    if (!strcmp(argv[1], "-dot")) {
	init_dot(l, b);
	do_gen(l, b, print_dot);
	fini_dot();
    } else if (!strcmp(argv[1], "-spec")) {
	init_spec(l, b);
	do_gen(l, b, print_spec);
	fini_spec();
    } else
	goto usage;

    exit(EXIT_SUCCESS);
usage:
    printf("usage: %s [-dot|-spec] l b\n", argv[0]);
    exit(EXIT_FAILURE);
}
