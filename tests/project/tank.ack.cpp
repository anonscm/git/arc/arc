#if EXHAUSTIVE
#define MODE exhaustively
#else
#define MODE symbolically
#endif

with MAINNODE do MODE
 dead                     := any_s - src(any_t - self_epsilon);
 // les situations redoutees
 niveau0                  := any_s & [Cu.niveau = 0];
 niveau1                  := any_s & [Cu.niveau = 1];
 niveau2                  := any_s & [Cu.niveau = 2];
 niveau3                  := any_s & [Cu.niveau = 3];
 niveau4                  := any_s & [Cu.niveau = 4];
 ER                       := (niveau0 | niveau4 | dead);
 // les pannes des vannes
 enPanne0                 := any_s &[V1.on & V2.on & V3.on];
enPanne1                 := any_s &([V1.on & V2.on & ~V3.on] | 
                             [V1.on & ~V2.on & V3.on] | 
				    [~V1.on & V2.on & V3.on] );
enPanne2                 := any_s &([V1.on & ~V2.on & ~V3.on] | 
                             [~V1.on & V2.on & ~V3.on] | 
				    [~V1.on & ~V2.on & V3.on] );
 enPanne3                 := any_s & [~V1.on & ~V2.on & ~V3.on];
 panne1                   := any_t & rsrc(enPanne0) & rtgt(enPanne1);
 panne2                   := any_t & rsrc(enPanne1) & rtgt(enPanne2);
 panne3                   := any_t & rsrc(enPanne2) & rtgt(enPanne3);
 // les debits de l'exploitation
 out2                     := any_s & [V3.debit=2];
 out1                     := any_s & [V3.debit=1];
 out0                     := any_s & [V3.debit=0];
 // les sous modeles en fonction du nombre de pannes permises.
 P0_modele                := reach(initial,any_t - panne1);
 // les types d'evenements
controle    := any_t & rsrc([controle]) - epsilon;
nonControle := any_t & rsrc([~controle]) - self_epsilon;
done

/* Les equations classiques 
 *  - de strat�gies gagnantes (| pour pppf et pour atteindre des etats)
 *  - et de synth�se de contr�leurs (& pour pgpf pour eviter des etats)
 Gagnant           = Gagne |& src(CoupGagnant);
 CoupGagnant       = coup & rtgt(Perdant);
 Perdant           = Perdu |& (src(CoupPerdant) - src(CoupNonPerdant));
 CoupPerdant       = coup & rtgt(Gagnant);
 CoupNonPerdant    = coup - rtgt(Gagnant);
*/
/* Les trois controleurs en phase d'exploitation
 - CtrlER pour eviter ER
 - CtrlOut12ER pour eviter ER et avoir un debit non nul
 - CtrlOut2ER  pour eviter ER et avoir un debit maximal
*/
/* Les quatre proprietes definissent quatre sous-modeles
 - P0 : aucune panne
 - P1 : au plus une panne
 - P2 : au plus deux pannes
 - P3 : au plus trois pannes
*/

// Les controleurs pour la propriete P0
with MAINNODE do MODE
 // Restriction au sous-modele sans panne.
 P0_controle    := any_t & rsrc([controle]&P0_modele) & rtgt(P0_modele)-self_epsilon;
 P0_nonControle := any_t & rsrc([~controle]&P0_modele) & rtgt(P0_modele)-self_epsilon;
 // les actions de controle pour eviter ER
 P0_GagneER     := P0_modele - ER;
 P0_PerduER     := P0_modele - ER;
 P0_CtrlER      -= 
   P0_controle & 
   rtgt(P0_PerduER & 
        (src(P0_nonControle & rtgt(P0_GagneER & src(P0_CtrlER))) - 
         src(P0_nonControle - rtgt(P0_GagneER & src(P0_CtrlER)))));

 // Le syst�me est-il controlable
 P0_ControlableER            := any_s & initial & src(P0_CtrlER);

 // les erreurs de commande possibles
 P0_ErreurControleER         := P0_controle - P0_CtrlER;

 // G�n�ration des controleurs 
#if PROJECT
 project(any_s, P0_CtrlER|nonControle, '$NODENAME_Controleur_P0', true)
# if EXHAUSTIVE
	> '$NODENAME_Controleur_P0_e.alt';
# else
	> '$NODENAME_Controleur_P0_s.alt';
# endif 
#endif

 ER_P0_modele := ER & P0_modele;
done

