// validate command for all nodes
validate Place
validate TicTacToe 

// General validation for all nodes
with Place, TicTacToe do
 show(any_s, any_t, initial, self, epsilon, self_epsilon) ;
 deadlock       := any_s - src(any_t - self_epsilon);
 notResetable   := any_s - coreach(initial, any_t);
 test(deadlock,0);
 test(notResetable,0) ;
done
with TicTacToe do
 // At the end of the game, A or B wins.
 // les types d'evenements
 AStep := any_t & rsrc([A]) - epsilon;
 BStep := any_t & rsrc([~A]) - epsilon;
 // Initial loosing positions for players
 ALoss := any_s & [BWin];
 BLoss := any_s & [AWin];
done
/* Classical equations for
 *  - winning strategy (| for lfp and to reach winning states)
 *  - controller synthesis (& for gfp and to avoid loosing states)
 Winning           = Win |& src(WinningStep);
 WinningStep       = Step & rtgt(Loosing);
 Loosing           = Loss |& (src(LoosingStep) - src(NotLoosingStep));
 LoosingStep       = Step & rtgt(Winning);
 NonLoosingStep    = Step - rtgt(Winning);
*/
with TicTacToe do
 AWinningStep +=
   AStep & 
   rtgt(BLoss |
        (src(BStep & rtgt(src(AWinningStep))) -
         src(BStep - rtgt(src(AWinningStep)))));
 BWinningStep +=
   BStep & 
   rtgt(ALoss |
        (src(AStep & rtgt(src(BWinningStep))) -
         src(AStep - rtgt(src(BWinningStep)))));
done
with TicTacToe do
 // Is there a winning strategy for A ?
 AHasNoStrategy := initial & src(AWinningStep);
 BHasStrategy := tgt(AStep & rsrc(initial)) - src(BWinningStep);
 // Is this game a strategy for A or B ?
 NotStrategyForA := AStep - AWinningStep;
 NotStrategyForB := BStep - BWinningStep;
 // Strategies generation
 project(any_s, AWinningStep|BStep, 'AStrategy_$NODENAME', true) > 'ttt-AStrategy_$NODENAME.alt';
 project(any_s, BWinningStep|AStep, 'BStrategy_$NODENAME', true) > 'ttt-BStrategy_$NODENAME.alt';

 // Save results in files
 show(all); 
 test(AHasNoStrategy,0);
 test(BHasStrategy,0);
 test(NotStrategyForA,0);
 test(NotStrategyForB,0);
done
