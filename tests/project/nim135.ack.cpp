#if EXHAUSTIVE
#define MODE exhaustively
#else
#define MODE symbolically
#endif

with MAINNODE do MODE
 coup                  := any_t - epsilon;
 dead                  := any_s - src(any_t - self_epsilon);
/* Le systeme d'equations pour les strategies gagnantes
 Gagnant               = Gagne | src(CoupGagnant);
 CoupGagnant           = coup & rtgt(Perdant);
 Perdant               = Perdu | (src(CoupPerdant) - src(CoupNonPerdant));
 CoupPerdant           = coup & rtgt(Gagnant);
 CoupNonPerdant        = coup - rtgt(Gagnant);
*/
/* 
   PREMIERE REGLE : celui qui joue en dernier gagne.
   - Les etats puits sont des situations perdues
   - et il n'y a pas de situations gagnees
*/
 PerduR1         := dead;
 CoupGagnantR1   += coup & 
                        rtgt(PerduR1 | 
                         (src(coup & rtgt(src(CoupGagnantR1))) - 
                          src(coup - rtgt(src(CoupGagnantR1)))));
 MauvaisCoupsR1        := coup & (rsrc(src(CoupGagnantR1)) - CoupGagnantR1);
 partiesR1             := reach(initial, coup - MauvaisCoupsR1);
 AWinnerR1             := initial & src(CoupGagnantR1);
 BWinnerR1             := tgt(coup & rsrc(initial)) - src(CoupGagnantR1);

#if PROJECT
 project(any_s, coup - MauvaisCoupsR1, '$NODENAME_PartiesR1', true)
# if EXHAUSTIVE
	> '$NODENAME_PartiesR1_e.alt';
# else
	> '$NODENAME_PartiesR1_s.alt';
# endif
#endif

 /*
    DEUXIEME REGLE : celui qui joue en dernier perd.
    - Les etats puits sont donc des situations gagnees
    - et il n'y a pas de situations perdues
*/
 GagneR2         := dead;
 CoupGagnantR2   += coup & 
                    rtgt((src(coup & rtgt(GagneR2 | src(CoupGagnantR2))) - 
                          src(coup - rtgt(GagneR2 | src(CoupGagnantR2)))));
 MauvaisCoupsR2        := coup & (rsrc(src(CoupGagnantR2)) - CoupGagnantR2);
 partiesR2             := reach(initial, coup - MauvaisCoupsR2);
 AWinnerR2             := initial & src(CoupGagnantR2);
 BWinnerR2             := tgt(coup & rsrc(initial)) - src(CoupGagnantR2);
#if PROJECT
 project(any_s, coup - MauvaisCoupsR2, '$NODENAME_PartiesR2', true)
# if EXHAUSTIVE
	> '$NODENAME_PartiesR2_e.alt';
# else
	> '$NODENAME_PartiesR2_s.alt';
# endif
#endif
done
