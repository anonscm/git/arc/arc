set acheck.nrtest-failure-aborts true
load nim135-cst.alt nim135-line.alt nim135-main.alt 

set arc.shell.preprocessor.cpp.command cpp
set arc.shell.preprocessor.cpp.args "-DPROJECT=1 -DMAINNODE=Nim135 -DEXHAUSTIVE=1"
load nim135.ack.cpp

load Nim135_PartiesR1_e.alt
load Nim135_PartiesR2_e.alt

set arc.shell.preprocessor.cpp.args "-DPROJECT=0 -DMAINNODE=Nim135_PartiesR1 -DEXHAUSTIVE=1"
load nim135.ack.cpp
set arc.shell.preprocessor.cpp.args "-DPROJECT=0 -DMAINNODE=Nim135_PartiesR2 -DEXHAUSTIVE=1"
load nim135.ack.cpp

# To produce nrtests uncomment following line and comment next one
# eval "with Nim135, Nim135_PartiesR1, Nim135_PartiesR2 do exhaustively show (all) > 'nim135-$NODENAME.nrtest'; done" 

eval "with Nim135, Nim135_PartiesR1, Nim135_PartiesR2 do exhaustively nrtest('nim135-$NODENAME.nrtest'); done" 

