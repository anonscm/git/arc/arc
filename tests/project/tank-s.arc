set acheck.nrtest-failure-aborts true
load tank-vanne.alt tank-vanne-rep.alt tank-vanne-robuste.alt tank-cuve.alt
load tank-main.alt

set arc.shell.preprocessor.cpp.command cpp
set arc.shell.preprocessor.cpp.args "-DPROJECT=1 -DMAINNODE=System -DEXHAUSTIVE=0"
load tank.ack.cpp

load System_Controleur_P0_s.alt
set arc.shell.preprocessor.cpp.args "-DPROJECT=0 -DMAINNODE=System_Controleur_P0 -DEXHAUSTIVE=0"
load tank.ack.cpp

eval "with System, System_Controleur_P0 do symbolically nrtest('tank-$NODENAME.nrtest'); done" 

