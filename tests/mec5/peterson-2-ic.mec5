/**
 * $Id: peterson-2-ic.mec5,v 1.1 2010/01/20 13:28:31 point Exp $
 *
 * Mec V specification for the Peterson mutual exclusion protocol with
 * 2 processes.
 */

// Initial configurations
_Ini(s : Main!c) := Main!init(s);

// Errors: configurations that violate mutual exclusion
_Err(s : Main!c) :=
   (s.P1.co = D & s.P2.co = D);

// Deadlocks: configurations with no outbound transition except ``all-epsilons''
_Dead(s : Main!c) :=
  ~<e:Main!ev><s1:Main!c>(Main!t(s, e, s1) & ~(
    e.P1. = '$' &
    e.P2. = '$'));

// Bad: configurations that are in error w.r.t. to the above sets
_Bad(s : Main!c) := _Err(s) | _Dead(s);

// Post^*(Ini): configurations that are reachable from the initial configurations
_PostStarOfIni(s : Main!c) += <s1:Main!c>(_PostStarOfIni(s1) & <e:Main!ev>Main!t(s1, e, s)) | _Ini(s);



_BadReachableFromIni(s : Main!c) := _Bad(s) & _PostStarOfIni(s);
//:rel-c _BadReachableFromIni

// Pre^*(Bad): configurations that can reach the bad configurations
_PreStarOfBad(s : Main!c) += <s1:Main!c>(<e:Main!ev>Main!t(s, e, s1) & _PreStarOfBad(s1)) | _Bad(s); 
_IniCoReachableFromBad(s : Main!c) := _Ini(s) & _PreStarOfBad(s);
//:rel-c _IniCoReachableFromBad

