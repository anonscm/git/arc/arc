load test-23.alt test-23.mec5 test-23.ack
check-card R1 0
check-card R2 0
check-card R3 0
check-card A!c 0
check-card A!reach 0
check-card A!R1 0
check-card A!R2 0
check-card A!R3 0
check-card A!any_c 0

store test-23.rel R1 as R1p R2 as R2p R3 as R3p A!c as Ac A!reach as Areach A!R1 as AR1 A!R2 as AR2 A!R3 as AR3 A!any_c as Aany_c

load test-23.rel

eval "R() := [s : A!sc] (R1(s) = R1p(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (R2(s) = R2p(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (R3(s) = R3p(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (A!c(s) = Ac(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (A!reach(s) = Areach(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (A!R1(s) = AR1(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (A!R2(s) = AR2(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (A!R3(s) = AR3(s));"
check-card R 1
remove relations R

eval "R() := [s : A!sc] (A!any_c(s) = Aany_c(s));"
check-card R 1
remove relations R



