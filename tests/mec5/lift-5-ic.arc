load lift-5-ic.alt lift-5-ic.mec5

check-card Version1!Ini 1
check-card Version2!Ini 1
check-card Version3!Ini 1
check-card Version1!Err1 307200
check-card Version2!Err1 307200
check-card Version3!Err1 614400
check-card Version1!Err2 0
check-card Version2!Err2 0
check-card Version3!Err2 0
check-card Version1!Err3 0
check-card Version2!Err3 0
check-card Version3!Err3 0
check-card Version1!Err4 0
check-card Version2!Err4 0
check-card Version3!Err4 0
check-card Version1!Err5 0
check-card Version2!Err5 0
check-card Version3!Err5 0
check-card Version1!Err6 122400
check-card Version2!Err6 0
check-card Version3!Err6 0
check-card Version1!PostStarOfIni 8960
check-card Version2!PostStarOfIni 8960
check-card Version3!PostStarOfIni 14336
check-card Version1!PreStarOfErr1 312320
check-card Version1!PreStarOfErr2 0
check-card Version1!PreStarOfErr3 0
check-card Version1!PreStarOfErr4 0
check-card Version1!PreStarOfErr5 0
check-card Version1!PreStarOfErr6 327680
check-card Version2!PreStarOfErr1 308480
check-card Version2!PreStarOfErr2 0
check-card Version2!PreStarOfErr3 0
check-card Version2!PreStarOfErr4 0
check-card Version2!PreStarOfErr5 0
check-card Version2!PreStarOfErr6 0
check-card Version3!PreStarOfErr1 616960
check-card Version3!PreStarOfErr2 0
check-card Version3!PreStarOfErr3 0
check-card Version3!PreStarOfErr4 0
check-card Version3!PreStarOfErr5 0
check-card Version3!PreStarOfErr6 0

set arc.shell.preprocessor.php.command php
set arc.shell.preprocessor.php.args "5 lift-ic-"
load lift-store.arc.php
