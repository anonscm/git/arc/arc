with A do symbolically
     R1 += initial | tgt(rsrc(R1));
     R2 := R1 & any_c;
     R3 += initial | (any_c&tgt(rsrc(R3)));
done