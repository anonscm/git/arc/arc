
extern parameter LAMBDA = 0.001;
extern parameter MU = 0.01;

const TEAM_SIZE = 1;

node Component
  flow
    i, o : bool;
  state
    s : { ok, nok, maint };
  init s := ok;
  event
    failure, maintenance, repair : public;
  extern
    law <event failure> = exponential (LAMBDA);
    law <event repair> = exponential (MU);
  trans
    s = ok |- failure -> s := nok;
    s = nok |- maintenance -> s := maint; 
    s = maint |- repair -> s := ok;
  assert
    o = ((s = ok) & i);
edon

node Crew
   state nb_free_team : [0,TEAM_SIZE];
   init nb_free_team := TEAM_SIZE;
   event start_repair, end_repair : public;
   trans
     nb_free_team > 0 |- start_repair -> nb_free_team := nb_free_team - 1;
     nb_free_team < TEAM_SIZE |- end_repair ->
     		  nb_free_team := nb_free_team + 1;     
   extern 
     law <event start_repair> = dirac (0);
edon

node ParallelBlock
  flow
    i, o : bool;
  sub
    C : Component[2];
    M : Crew;
  event	start_repair, end_repair : public;
  trans true |- start_repair, end_repair -> ;
  assert
    C[0].i = i;
    C[1].i = i;
    o = (C[0].o or C[1].o);
  sync
    <start_repair, C[0].maintenance, M.start_repair>;
    <end_repair, C[0].repair, M.end_repair>;    
    <start_repair, C[1].maintenance, M.start_repair>;
    <end_repair, C[1].repair, M.end_repair>;    
edon
node SerialBlock
  flow
    i, o : bool;
  sub
    S : ParallelBlock[4];
  assert
    S[0].i = i;
    S[1].i = S[0].o;
    S[2].i = S[1].o;
    S[3].i = S[2].o;
    o = S[3].o;
edon


node Main
 sub
   S : SerialBlock;
 assert
   S.i = true;
 extern
   predicate ER0 = <term (S.o)>;
   predicate ER1 = <term (not S.o)>;
edon


flatten Main 
ca Main 
