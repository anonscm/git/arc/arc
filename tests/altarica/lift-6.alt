/**
 * $Id: lift-6.alt,v 1.1 2009/08/27 14:02:49 point Exp $
 *
 * AltaRica lift model by Alain Griffault.
 *
 * Original model was with 4 floors. This PHP version generates lift models
 * with any positive number of floors.
 *
 * The only change made w.r.t. to the original model is the addition of
 * events close[0-9]* to the main nodes (Version1, Version2 and Version3).
 * These new events just export door closing events to the outside and are
 * useful in property specifications.
 */
const  DernierEtage = 5 ;
domain Etages       = [0,DernierEtage] ;
/* les boutons r�agissent � :
 *   - une action de l'utilisateur
 *   - un signal d'extinction
*/
node Button
  state
    light : bool : public;
  event
    push : public;
    off;
  trans
    true |- push -> light := true;
    true |- off  -> light := false;
  init
    light := false;
edon
/* les portes r�agissent � :
 *   - un signal d'ouverture
 *   - un signal de fermeture
*/
node Door
  state
    closed : bool : public;
  event
    open, close : public;
  trans
    true |- open  -> closed := false;
    true |- close -> closed := true;
  init
    closed := true;
edon
/* les �tages comprennent une porte et un bouton
 * La mod�lisation doit donner un sens � "la requete est satisfaite"
 * Elle doit choisir entre :
 *   - lors de l'ouverture de la porte
 *   - lors de la fermeture de la porte
 * Nous choisissons la seconde solution
*/
node Floor
  sub
    B : Button;
    D : Door;
  event
    close;
  trans
    ~D.closed |- close -> ;
  sync
    <close, D.close, B.off>;
edon
/* la cabine comprend une porte et un bouton par �tage (ici 4)
 * La mod�lisation doit donner un sens � "la requete est satisfaite"
 * Elle doit choisir entre :
 *   - lors de l'ouverture de la porte
 *   - lors de la fermeture de la porte
 * Nous choisissons la seconde solution (comme dans l'�tage)
*/
node Cage
  state
    floor : Etages : parent;
  sub
    D : Door;
    B0, B1, B2, B3, B4, B5 : Button;
  event
    up, down, close0, close1, close2, close3, close4, close5;
  trans
    D.closed |- up   -> floor := floor + 1;
    D.closed |- down -> floor := floor - 1;
    floor = 0 |- close0 -> ;
    floor = 1 |- close1 -> ;
    floor = 2 |- close2 -> ;
    floor = 3 |- close3 -> ;
    floor = 4 |- close4 -> ;
    floor = 5 |- close5 -> ;
  sync
    <close0, D.close, B0.off>;
    <close1, D.close, B1.off>;
    <close2, D.close, B2.off>;
    <close3, D.close, B3.off>;
    <close4, D.close, B4.off>;
    <close5, D.close, B5.off>;
  init
    floor := 0;
edon
/* L'immeuble comprend une cabine et quatre �tages
 *  - l'ouverture et la fermeture des portes sont synchronis�es
 *  - l'ouverture n'est possible que si une requete existe � cet �tage.
 *  - la cabine se d�place si il existe une requete qui le necessite
*/
node Version1
  flow
    mayGoUp, mayGoDown, request0, request1, request2, request3, request4, request5 : bool : private;
  sub
    F0, F1, F2, F3, F4, F5 : Floor;
    C : Cage;
  event
    open0, open1, open2, open3, open4, open5;
    close0, close1, close2, close3, close4, close5;
    up, down;
  trans
    (C.floor = 0) & request0 |- open0 -> ;
    (C.floor = 1) & request1 |- open1 -> ;
    (C.floor = 2) & request2 |- open2 -> ;
    (C.floor = 3) & request3 |- open3 -> ;
    (C.floor = 4) & request4 |- open4 -> ;
    (C.floor = 5) & request5 |- open5 -> ;
    mayGoDown |- down    -> ;
    mayGoUp   |- up      -> ;
    true |- close0 -> ;
    true |- close1 -> ;
    true |- close2 -> ;
    true |- close3 -> ;
    true |- close4 -> ;
    true |- close5 -> ;
  sync
    <up,    C.up>;
    <down,  C.down>;
    <open0, C.D.open, F0.D.open>;
    <open1, C.D.open, F1.D.open>;
    <open2, C.D.open, F2.D.open>;
    <open3, C.D.open, F3.D.open>;
    <open4, C.D.open, F4.D.open>;
    <open5, C.D.open, F5.D.open>;
    <close0, C.close0, F0.close>;
    <close1, C.close1, F1.close>;
    <close2, C.close2, F2.close>;
    <close3, C.close3, F3.close>;
    <close4, C.close4, F4.close>;
    <close5, C.close5, F5.close>;
  assert
    request0 = (C.B0.light | F0.B.light);
    request1 = (C.B1.light | F1.B.light);
    request2 = (C.B2.light | F2.B.light);
    request3 = (C.B3.light | F3.B.light);
    request4 = (C.B4.light | F4.B.light);
    request5 = (C.B5.light | F5.B.light);
    mayGoUp   = (
        (C.floor=0 & (request5 | request4 | request3 | request2 | request1)) |
        (C.floor=1 & (request5 | request4 | request3 | request2)) |
        (C.floor=2 & (request5 | request4 | request3)) |
        (C.floor=3 & (request5 | request4)) |
        (C.floor=4 & (request5)));
    mayGoDown = (
        (C.floor=5 & (request0 | request1 | request2 | request3 | request4)) |
        (C.floor=4 & (request0 | request1 | request2 | request3)) |
        (C.floor=3 & (request0 | request1 | request2)) |
        (C.floor=2 & (request0 | request1)) |
        (C.floor=1 & (request0)));
edon
/* L'immeuble comprend une cabine et quatre �tages
 *  - l'ouverture et la fermeture des portes sont synchronis�es
 *  - l'ouverture n'est possible que si une requete existe � cet �tage.
 *  - la cabine se d�place si il existe une requete qui le necessite
 *  - la cabine se d�place si il n'y a pas de requete � cet �tage.
*/
node Version2
  flow
    mayGoUp, mayGoDown, request0, request1, request2, request3, request4, request5 : bool : private;
  sub
    F0, F1, F2, F3, F4, F5 : Floor;
    C : Cage;
  event
    open0, open1, open2, open3, open4, open5;
    close0, close1, close2, close3, close4, close5;
    up < {open0, open1, open2, open3, open4, open5};
    down < {open0, open1, open2, open3, open4, open5};
  trans
    (C.floor = 0) & request0 |- open0 -> ;
    (C.floor = 1) & request1 |- open1 -> ;
    (C.floor = 2) & request2 |- open2 -> ;
    (C.floor = 3) & request3 |- open3 -> ;
    (C.floor = 4) & request4 |- open4 -> ;
    (C.floor = 5) & request5 |- open5 -> ;
    mayGoDown |- down    -> ;
    mayGoUp   |- up      -> ;
    true |- close0 -> ;
    true |- close1 -> ;
    true |- close2 -> ;
    true |- close3 -> ;
    true |- close4 -> ;
    true |- close5 -> ;
  sync
    <up,    C.up>;
    <down,  C.down>;
    <open0, C.D.open, F0.D.open>;
    <open1, C.D.open, F1.D.open>;
    <open2, C.D.open, F2.D.open>;
    <open3, C.D.open, F3.D.open>;
    <open4, C.D.open, F4.D.open>;
    <open5, C.D.open, F5.D.open>;
    <close0, C.close0, F0.close>;
    <close1, C.close1, F1.close>;
    <close2, C.close2, F2.close>;
    <close3, C.close3, F3.close>;
    <close4, C.close4, F4.close>;
    <close5, C.close5, F5.close>;
  assert
    request0 = (C.B0.light | F0.B.light);
    request1 = (C.B1.light | F1.B.light);
    request2 = (C.B2.light | F2.B.light);
    request3 = (C.B3.light | F3.B.light);
    request4 = (C.B4.light | F4.B.light);
    request5 = (C.B5.light | F5.B.light);
    mayGoUp   = (
        (C.floor=0 & (request5 | request4 | request3 | request2 | request1)) |
        (C.floor=1 & (request5 | request4 | request3 | request2)) |
        (C.floor=2 & (request5 | request4 | request3)) |
        (C.floor=3 & (request5 | request4)) |
        (C.floor=4 & (request5)));
    mayGoDown = (
        (C.floor=5 & (request0 | request1 | request2 | request3 | request4)) |
        (C.floor=4 & (request0 | request1 | request2 | request3)) |
        (C.floor=3 & (request0 | request1 | request2)) |
        (C.floor=2 & (request0 | request1)) |
        (C.floor=1 & (request0)));
edon
/* L'immeuble comprend une cabine et quatre �tages
 *  - l'ouverture et la fermeture des portes sont synchronis�es
 *  - l'ouverture n'est possible que si une requete existe � cet �tage.
 *  - la cabine se d�place si il existe une requete qui le necessite.
 *  - la cabine se d�place si il n'y a pas de requete � cet �tage.
 *  - le dernier mouvement est m�moris� pour pouvoir d�cider du suivant.
 *  - l'initialisation doit etre coh�rente.
*/
node Version3
  flow
    mayGoUp, mayGoDown : bool : private;
    request0, request1, request2, request3, request4, request5 : bool : private;
  state
    climb : bool;
  sub
    F0, F1, F2, F3, F4, F5 : Floor;
    C : Cage;
  event
    open0, open1, open2, open3, open4, open5;
    close0, close1, close2, close3, close4, close5;
    up < {open0, open1, open2, open3, open4, open5};
    down < {open0, open1, open2, open3, open4, open5};
  trans
    (C.floor = 0) & request0 |- open0 -> ;
    (C.floor = 1) & request1 |- open1 -> ;
    (C.floor = 2) & request2 |- open2 -> ;
    (C.floor = 3) & request3 |- open3 -> ;
    (C.floor = 4) & request4 |- open4 -> ;
    (C.floor = 5) & request5 |- open5 -> ;
    climb & mayGoUp               |- up   -> ;
    ~climb & ~mayGoDown & mayGoUp |- up   -> climb := true;
    ~climb & mayGoDown            |- down -> ;
    climb & ~mayGoUp & mayGoDown  |- down -> climb := false;
    true |- close0 -> ;
    true |- close1 -> ;
    true |- close2 -> ;
    true |- close3 -> ;
    true |- close4 -> ;
    true |- close5 -> ;
  sync
    <up,    C.up>;
    <down,  C.down>;
    <open0, C.D.open, F0.D.open>;
    <open1, C.D.open, F1.D.open>;
    <open2, C.D.open, F2.D.open>;
    <open3, C.D.open, F3.D.open>;
    <open4, C.D.open, F4.D.open>;
    <open5, C.D.open, F5.D.open>;
    <close0, C.close0, F0.close>;
    <close1, C.close1, F1.close>;
    <close2, C.close2, F2.close>;
    <close3, C.close3, F3.close>;
    <close4, C.close4, F4.close>;
    <close5, C.close5, F5.close>;
  assert
    request0 = (C.B0.light | F0.B.light);
    request1 = (C.B1.light | F1.B.light);
    request2 = (C.B2.light | F2.B.light);
    request3 = (C.B3.light | F3.B.light);
    request4 = (C.B4.light | F4.B.light);
    request5 = (C.B5.light | F5.B.light);
    mayGoUp   = (
        (C.floor=0 & (request5 | request4 | request3 | request2 | request1)) |
        (C.floor=1 & (request5 | request4 | request3 | request2)) |
        (C.floor=2 & (request5 | request4 | request3)) |
        (C.floor=3 & (request5 | request4)) |
        (C.floor=4 & (request5)));
    mayGoDown = (
        (C.floor=5 & (request0 | request1 | request2 | request3 | request4)) |
        (C.floor=4 & (request0 | request1 | request2 | request3)) |
        (C.floor=3 & (request0 | request1 | request2)) |
        (C.floor=2 & (request0 | request1)) |
        (C.floor=1 & (request0)));
  init
    climb := false;
edon
