#!/bin/sh

stable_packages="arc-1.4"
current_packages="arc-current"
INSTALLER="install-arc.sh"
FTPHOST="altarica.labri.fr"
PKGDIR="pub/tools/packages"

ftp_download_package() {
    pkg="$1"
    oname="$2"
    ftp -n <<EOF
open ${FTPHOST}
user anonymous anonymous
binary
cd ${FTPDIR}
get ${pkg} ${oname}
quit
EOF
}

wget_download_package() {
    pkg="$1"
    oname="$2"
    url="ftp://${FTPHOST}/${FTPDIR}/${pkg}"
    if test "x${oname}" = "x" ; then
	wget -q ${url}
    else
	wget -q ${url} -O ${oname}
    fi
}

check_download_proc() {
   wget=""
   ftp=""
   for p in /bin /usr/bin /usr/local/bin /usr/pkg/bin /opt/bin ; do
       if test -z "${wget}" -a -x ${p}/wget ; then
	   wget="${p}/wget"
	   break
       fi

       if test -z "${ftp}" -a -x ${p}/ftp ; then
	   ftp="${p}/ftp"
	   break;
       fi
   done

   proc="none"
   if test "${proc}" = "none" -a -n "${wget}" -a -x "${wget}" ; then
       proc="wget_download_package"        
   fi

   if test "${proc}" = "none" -a -n "${ftp}" -a -x "${ftp}" ; then
       proc="ftp_download_package"
   fi

   echo "${proc}"
}

build_package() {
    pkg="$1"
    pkglog="$2"
    result=0

    cd ${p}
    test -d build || mkdir build
    cd build
    if ../configure --prefix=${installdir} > ${pkglog} 2>&1 ; then
	if (make && make install) >> ${pkglog} 2>&1 ; then
	    :
	else
	    result=1
	fi
    else
	result=1
    fi
    
    return ${result}
}

usage() {
    echo "USAGE: $0 --[help|stable|current] /where/to/install/arc"
}

installdir=""
packages="stable"

while [ $# != 0 ]; do
    case "$1" in
	"--help") usage ; exit 0;;
	"--stable") packages="stable" ;;
	"--current") packages="current" ;;

	[\\/]* ) installdir="$1" ;;
	*) 
	    echo "the installation directory must be an absolute path." >&2 
	    exit 1
	    ;;
    esac
    shift
done

if test "x${installdir}" = "x" ; then
    echo "no installation directory is specified" >&2 
    usage
    exit 1
fi

case "${packages}" in
    "stable") 
	echo "installing stable version of ARC"
	packages="${stable_packages}"
	FTPDIR="${PKGDIR}/stable"
	;;

    "current") 
	echo "installing developement version of ARC"
	packages="${current_packages}"
	FTPDIR="${PKGDIR}/current"
	;;
    *)
	echo "internal error" >&2
	exit 1;
	;;
esac

download="`check_download_proc`"
if test "${download}" = "none" ; then
    echo "can't find ftp or wget command." >&2 
    exit 1
fi

topdir=`pwd`
topsrcdir=${topdir}/_sources

DOWNLOAD_LOG="${topdir}/download.log"
CPPFLAGS="-I${installdir}/include -I/usr/pkg/include"
LDFLAGS="-L${installdir}/lib -L/usr/pkg/lib"
PATH=${installdir}/bin:$PATH
export CPPFLAGS LDFLAGS PATH

rm -f ${DOWNLOAD_LOG}

test -d ${topdir} || mkdir ${topdir}
test -d ${topsrcdir} || mkdir ${topsrcdir}

cd ${topsrcdir}

echo "downloading packages (progress shown in ${DOWNLOAD_LOG})"
for p in ${packages}; do
    if test -f ${p}.tar.gz ; then
	echo "   ${p} is already here" 
    else
	echo "   downloading ${p}"
	${download} ${p}.tar.gz >> ${DOWNLOAD_LOG} 2>&1  
    fi
done

echo "installing packages"
for p in ${packages} ; do
    if test -f ${p}.tar.gz ; then
	gzip -dc ${p}.tar.gz | tar xf -    
	echo "   installing ${p} (progress shown in ${topdir}/${p}.log)"
	if (build_package ${p} ${topdir}/${p}.log) ; then
	    :
	else
	    echo "    error: an error occurs while installing ${p}. have a look to ${p}.log" >&2
	    exit 1
	fi
    else
	echo "can't find downloaded packages ${p}" >&2
	exit 1
    fi
done

ldconfig -N ${installdir}

exit 0
