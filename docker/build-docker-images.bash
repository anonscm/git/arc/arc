#!/usr/bin/env bash

set -e

BASE_IMAGE=altarica/base:latest
ARC_IMAGE=altarica/arc:latest
STUDIO_IMAGE=altarica/altarica-studio:latest

docker build -t ${BASE_IMAGE} -f altarica-base.docker .
docker build -t ${ARC_IMAGE} -f altarica-arc.docker .
docker build -t ${STUDIO_IMAGE} -f altarica-studio.docker .

docker push ${BASE_IMAGE}
docker push ${ARC_IMAGE}
docker push ${STUDIO_IMAGE}
