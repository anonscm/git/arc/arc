dnl 
dnl Check if the version of the program respects the version constraint
dnl 'cmp' 'version', where 'cmp' is a comparison operator.
dnl
dnl USAGE: ARCFG_CHECK_PROG_VERSION(prog, cmp, version, [action-if-found],
dnl                             [action-if-not-found],[option-to-get-version])
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_CHECK_PROG_VERSION],
[
 AC_REQUIRE([AC_PROG_GREP])
 AC_REQUIRE([AC_PROG_SED])
 AC_MSG_CHECKING([if version of '$1' is '$2 $3'])
 m4_ifblank([$6],
   [AS_VAR_SET([prog_version],[[`eval "$1" --version 2>&1 | ${AWK} '/.* [0-9]+([.0-9]+)+.*$/ { print ; exit; }' | ${SED} -e 's/^.* \([0-9]\+\(\.[0-9.]\+\)\+\).*$/\1/' `]])],
   [AS_VAR_SET([prog_version],[[`eval "$1" "$6" 2>&1 | ${AWK} '/.* [0-9]+([.0-9]+)+.*$/ { print ; exit; }' | ${SED} -e 's/^.* \([0-9]\+\(\.[0-9.]\+\)\+\).*$/\1/' `]])])

 m4_define([ok],[m4_default([$4],[:])
                AC_MSG_RESULT([yes])])
 m4_define([nok],[m4_default([$5],[:])
                AC_MSG_RESULT([no])])
 AS_CASE(["$2"],
   ["="],  [AS_VERSION_COMPARE([${prog_version}],[$3],[nok],[ok],[nok])],
   [">="], [AS_VERSION_COMPARE([${prog_version}],[$3],[nok],[ok],[ok])],
   [">"],  [AS_VERSION_COMPARE([${prog_version}],[$3],[nok],[nok],[ok])],
   ["<="], [AS_VERSION_COMPARE([${prog_version}],[$3],[ok],[ok],[nok])],
   ["<"],  [AS_VERSION_COMPARE([${prog_version}],[$3],[ok],[nok],[nok])],
   [AC_MSG_ERROR([bad version constraint specifier.])])
 ])

dnl
dnl Generate a help string for an --enable or --disable option. The message is
dnl produced according to the default value i.e. if by default the feature is
dnl enabled then a 'disable' option is generated.
dnl 
dnl USAGE: ARCFG_ENABLE_HLP_STRING(option-id,option-desc,default-enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_HLP],
[m4_if([$3],[yes],
       [AS_HELP_STRING([--disable-m4_translit([$1], [_], [-])], [disable $2])],
       [AS_HELP_STRING([--enable-m4_translit([$1], [_], [-])], [enable $2])])
])
