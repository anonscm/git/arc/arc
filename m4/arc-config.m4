dnl
dnl Enable or not ccl-assertion checking.
dnl
dnl USAGE: ARCFG_CCL_ENABLE_ASSERTIONS(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_CCL_ENABLE_ASSERTIONS],[
  AS_VAR_SET([ENABLE_CCL_ASSERTIONS],[$1])
  AC_DEFINE([CCL_ENABLE_ASSERTIONS],
            [m4_if([$1],[yes],[1],[0])], [set to 1 to enable CCL assetions])

  AC_ARG_ENABLE([ccl_assertions],
    [ARCFG_ENABLE_HLP([ccl-assertions],[CCL assertions],[$1])],
    [ENABLE_CCL_ASSERTIONS=$enableval],
    [])
  AS_IF([test "$ENABLE_CCL_ASSERTIONS" = "yes"],
        [AC_DEFINE([CCL_ENABLE_ASSERTIONS],[1])])
])

dnl
dnl Enable or not the debugging mode in ccl-memory.
dnl
dnl USAGE: ARCFG_CCL_DEBUG_MEMORY(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_CCL_DEBUG_MEMORY],[
  AS_VAR_SET([ENABLE_CCL_DEBUG_MEMORY],[$1])
  AC_DEFINE([CCL_DEBUG_MEMORY],
            [m4_if([$1],[yes],[1],[0])],
	    [set to 1 to enable debugging in CCL memory module])
  AC_ARG_ENABLE([ccl_debug_memory],
    [ARCFG_ENABLE_HLP([debug-memory],[CCL memory debugging],[$1])],
    [ENABLE_CCL_DEBUG_MEMORY=$enableval],
    [])
  AS_IF([test "$ENABLE_CCL_DEBUG_MEMORY" = "yes"],
        [AC_DEFINE([CCL_DEBUG_MEMORY],[1])])
])

dnl 
dnl Enable or not the compilation of handbook
dnl
dnl USAGE: ARCFG_ENABLE_HANDBOOK(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_ARC_HANDBOOK],[
  AS_VAR_SET([ENABLE_ARC_HANDBOOK],[$1])
  AC_ARG_ENABLE([arc_handbook],
    [ARCFG_ENABLE_HLP([arc-handbook],[the compilation of ARC handbook],[$1])],
    [ENABLE_ARC_HANDBOOK=$enableval],
    [])
])

dnl
dnl Enable or disable DD structure checking.
dnl
dnl USAGE: ARCFG_ENABLE_DD_CHECKING
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_DD_CHECKING],[
  AS_VAR_SET([ENABLE_DD_CHECKING],[$1])
  AC_DEFINE([DD_CHECKING_ENABLED],
            [m4_if([$1],[yes],[1],[0])],
	    [set to 1 to enable debugging DD structure])
  AC_ARG_ENABLE([dd_checking],
    [ARCFG_ENABLE_HLP([dd-checking],[checking of DD structure],[$1])],
    [ENABLE_DD_CHECKING=$enableval],
    [])
  AS_IF([test "$ENABLE_DD_CHECKING" = "yes"],
        [AC_DEFINE([DD_CHECKING_ENABLED],[1])])
])

dnl
dnl GMake
dnl
dnl USAGE: ARCFG_CHECK_GMAKE
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_CHECK_GMAKE],[
  AC_CHECK_PROGS(GMAKE, [gmake make],[none])
  AS_CASE([$(${GMAKE} -v)], ["GNU Make"*],
          [AS_VAR_SET([MAKE],[${GMAKE}])
	  ],[GMAKE=none])
  AM_CONDITIONAL([USE_GMAKE], [test "$GMAKE" != "none"])
])

dnl
dnl PTHREAD Library
dnl
dnl USAGE: ARCFG_ENABLE_PTHREAD(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_PTHREAD],[
  AS_VAR_SET([ENABLE_PTHREAD],[$1]) 
  dnl add option to enable/disable the use of pthread
  AC_ARG_ENABLE(pthread,
    [ARCFG_ENABLE_HLP([pthread],[pthread library],[$1])],
    [ENABLE_PTHREAD=$enableval],
    [])
  dnl check the presence of pthread only if enabled 
  AS_IF([test "x${ENABLE_PTHREAD}" = "xyes"],
      [AX_PTHREAD
       LIBS="$PTHREAD_LIBS $LIBS"
       CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
       CC="$PTHREAD_CC"
      ],[
      AC_MSG_WARN([pthread is disabled.])
      ])   
  AM_CONDITIONAL([WITH_PTHREAD], [test "${ENABLE_PTHREAD}" = "yes"])
])

dnl
dnl READLINE Library
dnl
dnl USAGE: ARCFG_ENABLE_READLINE(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_READLINE],[
  AS_VAR_SET([ENABLE_READLINE],[$1]) 
  dnl add option to enable/disable the use of readline
  AC_ARG_ENABLE(readlines,
    [ARCFG_ENABLE_HLP([readline],[readline library],[$1])],
    [ENABLE_READLINE=$enableval],
    [])
  dnl check the presence of readline only if enabled 
  AS_IF([test "${ENABLE_READLINE}" = "yes"],
      [
        AC_LIB_READLINE
        AC_CHECK_FUNC([rl_crlf], [has_crlf=yes], [has_crlf=no])
        AS_IF([test "${has_crlf}" = "no"],
	      [AC_DEFINE([HAVE_LIBREADLINE],[0])],[])
      ],
      []) 
])


dnl
dnl Tests related to GIT
dnl
dnl USAGE: ARCFG_REPOSITORY_INFOS
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_GEN_REPINFO],[
  GEN_REPOINFO_SCRIPT="no"
  AS_IF([test "${GIT}" != "none" && test "${IN_REPOSITORY}" = "yes"],
        [AS_VAR_SET([GEN_REPOINFO_SCRIPT],[yes])])
  AM_CONDITIONAL([GEN_REPOINFO], [test "${GEN_REPOINFO_SCRIPT}" = "yes"])
])

AC_DEFUN([ARCFG_GEN_CHANGELOG],[
  GEN_CHANGELOG_SCRIPT="no"
  AS_IF([test "${GIT2CL}" != "none" && test "${IN_REPOSITORY}" = "yes"],
        [AS_VAR_SET([GEN_CHANGELOG_SCRIPT],[yes])])
  AM_CONDITIONAL([GEN_CHANGELOG], [test "${GEN_CHANGELOG_SCRIPT}" = "yes"])
])

AC_DEFUN([ARCFG_REPOSITORY_INFOS],[
 AS_IF([test -d ${srcdir}/.git],
       [AS_VAR_SET([IN_REPOSITORY],[yes])],
       [AS_VAR_SET([IN_REPOSITORY],[no])])
 ARCFG_GEN_REPINFO
 ARCFG_GEN_CHANGELOG

 AS_IF([test -f "${srcdir}/dist.repinfo"],
       [AS_VAR_SET([DIST_REPINFO],["$(sed -e 's//  /g' ${srcdir}/dist.repinfo)"])],
       [AS_VAR_SET([DIST_REPINFO],["  None"])])
])

dnl
dnl Windows Installer Prerequisites
dnl
dnl USAGE: ARCFG_WINDOWS_INSTALLER
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_WINDOWS_INSTALLER],[
  AS_VAR_SET([build_wininstaller],[no])
  AS_IF([test "${WINDOWS}" = "yes"],[
    AC_CHECK_PROGS([MAKENSIS], [makensis.exe makensis], [none])
    AS_VAR_SET([build_wininstaller],[no])
    AS_IF([test "${MAKENSIS}" = "none"],
          [AS_VAR_SET([build_wininstaller],[no])],
	  [ AS_IF([test "${build_handbook}" = "no" &&
  	         test "x${ARC_HANDBOOK_BUILDDIR}" = "x"],
                [AS_VAR_SET([ARC_HANDBOOK_BUILDDIR],
	               ["\${top_srcdir}/doc/handbook"])])
            AS_VAR_SET([build_wininstaller],[yes])
	  ])    
   ])
  AM_CONDITIONAL([MAKE_WININSTALLER],[test "${build_wininstaller}" = "yes"])
])
	     
dnl
dnl Glucose settings
dnl
dnl USAGE: ARCFG_GLUCOSE_SETTINGS
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_GLUCOSE_SETTINGS],[
  AX_CHECK_ZLIB([], [AC_MSG_ERROR([missing zlib library])])
  GLUCOSE_SRCDIR=\${top_srcdir}/glucose
  GLUCOSE_LIBDIR=\${top_builddir}/glucose
  GLUCOSE_LIBNAME=libarcglucose
  GLUCOSE_LIB=${GLUCOSE_LIBDIR}/${GLUCOSE_LIBNAME}.la
  AC_SUBST([GLUCOSE_SRCDIR])
  AC_SUBST([GLUCOSE_LIBDIR])
  AC_SUBST([GLUCOSE_LIBNAME])
  AC_SUBST([GLUCOSE_LIB])
])

dnl
dnl Generation of ARC help pages
dnl
dnl USAGE: ARCFG_ARC_DOC(makeinfoversion,texi2pdfversion)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ARC_DOC],[
  AS_VAR_SET([MAKEINFO_VERSION],[$1])
  AS_VAR_SET([TEXI2PDF_VERSION],[$2])
  
  AC_CHECK_PROGS([MAKEINFO],[makeinfo],[none])
  AC_CHECK_PROGS([TEXI2PDF],[texi2pdf],[none])
  AC_CHECK_PROGS([PHP],[php],[none])

  AS_IF([test "${PHP}" != "none"],
  	[AS_VAR_SET([HAS_PHP],[yes])],
	[AS_VAR_SET([HAS_PHP],[no])])
	
  AS_IF([test "${MAKEINFO}" != "none"],
    [ARCFG_CHECK_PROG_VERSION([${MAKEINFO}],[>=],[${MAKEINFO_VERSION}],
       [AS_VAR_SET([MAKEINFO_VERSION],["${prog_version}"])],
       [AS_VAR_SET([MAKEINFO],[none])
        AS_UNSET([MAKEINFO_VERSION])])])	
  AS_IF([test "${TEXI2PDF}" != "none"],
    [ARCFG_CHECK_PROG_VERSION([${TEXI2PDF}],[>=],[${TEXI2PDF_VERSION}],
       [AS_VAR_SET([TEXI2PDF_VERSION],["${prog_version}"])],
       [AS_VAR_SET([TEXI2PDF],[none])
        AS_UNSET([TEXI2PDF_VERSION])])])
  AS_IF([test "${MAKEINFO}" != "none" && test "${TEXI2PDF}" != "none" &&
         test "${PHP}" != "none" && test "${ENABLE_ARC_HANDBOOK}" = "yes"],
      [AS_VAR_SET([build_handbook],[yes])],
      [AS_VAR_SET([build_handbook],[no])])
  AM_CONDITIONAL([HAS_HANDBOOK_PREREQ],[test "${build_handbook}" = "yes"])  
])

dnl
dnl Enable/disable all tests even longest or non robust tests
dnl
dnl USAGE: ARCFG_ENABLE_VALGRIND_TESTS(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_VALGRIND_TESTS],[
  AS_VAR_SET([ENABLE_VALGRIND_TESTS],[$1])
  dnl add option to enable/disable the use of valgrind
  AC_ARG_ENABLE([valgrind],
    [ARCFG_ENABLE_HLP([valgrind],[Valgrind for in test suite],[$1])],
    [ENABLE_VALGRIND_TESTS=$enableval],
    [])
  dnl check the presence of valgrinf only if enabled 
  AS_IF([test "${ENABLE_VALGRIND_TESTS}" = "yes"],
        [AC_CHECK_PROGS([VALGRIND], [valgrind], [none])
	 AS_IF([test "${VALGRIND}" = "none"],
               [AS_VAR_SET([ENABLE_VALGRIND_TESTS],["valgrind is missing"])])])
	
  AM_CONDITIONAL([WITH_VALGRIND], [test "${ENABLE_VALGRIND_TESTS}" = "yes"])
])

dnl
dnl Enable/disable all tests even longest or non robust tests
dnl
dnl USAGE: ARCFG_ENABLE_ALL_TESTS(default_enabling)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ENABLE_ALL_TESTS],[
  AS_VAR_SET([ENABLE_ALL_TESTS],[$1])
  AC_ARG_ENABLE([all-tests],
    [ARCFG_ENABLE_HLP([all-tests],[all tests suites], [$1])],
    [ENABLE_ALL_TESTS=$enableval],
    [])
  AM_CONDITIONAL([WITH_ALL_TESTS], [test "$ENABLE_ALL_TESTS" = "yes"])
])

dnl
dnl Enabling of Aralia-based tests (e.g. diagnosis)
dnl
dnl USAGE: ARCFG_ARALIA_TESTS
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ARALIA_TESTS],[
 AS_VAR_SET([ENABLE_ARALIA_TESTS],[no])
 AC_CHECK_PROGS([ARALIA],[aralia],[none])
 AM_CONDITIONAL([HAS_ARALIA], [test "${ARALIA}" != "none"])
 AS_IF([test "${ARALIA}" != "none"],
       [AS_VAR_SET([ENABLE_ARALIA_TESTS],[yes])],
       [AS_VAR_SET([ENABLE_ARALIA_TESTS],["aralia is missing"])])
])

dnl
dnl Run tests
dnl
dnl USAGE: ARCFG_RUN_TESTS
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_RUN_TESTS],[
 AS_IF([test "${GMAKE}" != "none"],[
   AS_VAR_SET([RUN_TESTS],[yes])
   ARCFG_ENABLE_VALGRIND_TESTS(default_enable_valgrind)
   ARCFG_ENABLE_ALL_TESTS(default_enable_all_tests)
   ARCFG_ARALIA_TESTS],[
   AS_VAR_SET([RUN_TESTS],[no])
  ])
])

dnl
dnl Generate ts2alt script 
dnl
dnl USAGE: ARCFG_GENERATE_TS2ALT_SCRIPT
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_GENERATE_TS2ALT_SCRIPT],[
   AS_VAR_SET([GENERATE_TS2ALT],[no])   
   dnl ----- GAWK used by ts2alt script
   AS_IF([test "${GAWK}" != "none"],[GENERATE_TS2ALT=yes])
])

dnl
dnl Test for AltaRica Studio pre-requisites 
dnl
dnl USAGE: ARCFG_ALTARICA_STUDIO_PREREQ(python_version,dot_version)
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_ALTARICA_STUDIO_PREREQ],[
  PYTHON_GTK=no
  AS_IF([test "${PYTHON}" != "none"],
     [AS_IF([${PYTHON} -c "import pygtk" 2> /dev/null],
            [AS_VAR_SET([PYTHON_GTK],[yes])],
            [AS_VAR_SET([PYTHON_GTK],[no])])
      ARCFG_CHECK_PROG_VERSION([${PYTHON}],[>=],[$1],
                            [AS_VAR_SET([PYTHON_VERSION],[${prog_version}])],
                            [AS_UNSET([PYTHON_VERSION])])])

  AC_CHECK_PROGS([GRAPHVIZ_DOT],[dot],[none])

  AS_IF([test "${GRAPHVIZ_DOT}" != "none"],
        [ARCFG_CHECK_PROG_VERSION([${GRAPHVIZ_DOT}],[>=],[$2],
          [AS_VAR_SET([GRAPHVIZ_DOT_VERSION],[${prog_version}])],
          [AS_UNSET([GRAPHVIZ_DOT_VERSION]);
	   GRAPHVIZ_DOT=none],[-V])])

   AS_IF([test "${PYTHON}" != "none" && test "${PYTHON_VERSION}" != "" && 
          test "${PYTHON_GTK}" != "no" && test "${GRAPHVIZ_DOT}" != "none"],
         [AS_VAR_SET([build_AS],[yes])],
         [AS_VAR_SET([build_AS],[no])
          AC_MSG_WARN([cannot find AltaRica Studio prerequisites.])])

   AM_CONDITIONAL([HAS_AS_PREREQ], [test "${build_AS}" = "yes"])
])

dnl
dnl Check if sysconf (_SC_PAGESIZE) is available
dnl
dnl USAGE: ARCFG_PAGESIZE
dnl ----------------------------------------------------------------------------
AC_DEFUN([ARCFG_PAGESIZE],[
AC_DEFINE([HAVE_SYSCONF_PAGESIZE],[0],
          [set to 1 if system supports sysconf (_SC_PAGESIZE) ? ])
AC_MSG_CHECKING(if 'sysconf (_SC_PAGESIZE)' is available ?)
AC_LINK_IFELSE([AC_LANG_SOURCE([
#include <unistd.h>

int main (int argc, char **argv)
{
  sysconf (_SC_PAGESIZE);
  return 0;
}
])],
    [AC_DEFINE([HAVE_SYSCONF_PAGESIZE],[1])
     AC_MSG_RESULT(yes)],
    [AC_MSG_RESULT(no)])
])

AC_DEFUN([ARCFG_VERBOSE_NOTICES],[
  AC_MSG_NOTICE([
  ])
])

AC_DEFUN([ARCFG_NOTICES],[
  AC_MSG_NOTICE([
===============================================================================
		    AltaRica Checker ${VERSION}
===============================================================================

Basic informations
  OS   : ${host_os}
  arch : ${host_cpu}

  prefix : ${prefix}
  srcdir : ${srcdir}

Compilation tools
  CC       : ${CC}
  CFLAGS   : ${CFLAGS}
  C++      : ${CXX}
  CXXFLAGS : ${CXXFLAGS}
 
  CPPFLAGS : ${CPPFLAGS}
  LDFLAGS  : ${LDFLAGS}
  LIBS     : ${LIBS}
  YACC     : ${YACC}
  LEX      : ${LEX}

Optional libraries
  use pthread library  : ${ENABLE_PTHREAD}
  use readline library : ${ENABLE_READLINE}

Debugging flags
  CCL assertions   : $ENABLE_CCL_ASSERTIONS
  CCL debug memory : $ENABLE_CCL_DEBUG_MEMORY
  debugging of DDs : $ENABLE_DD_CHECKING

AltaRica Studio (build : ${build_AS})
  python >= python_studio_version	: ${PYTHON} ${PYTHON_VERSION}
  GTK 2 bindings for python : ${PYTHON_GTK}
  graphviz dot >= graphviz_studio_version : ${GRAPHVIZ_DOT} ${GRAPHVIZ_DOT_VERSION}

Test suites (run: ${RUN_TESTS})
  GNU make           : ${GMAKE}
  valgrind           : ${VALGRIND}
  valgrind tests     : ${ENABLE_VALGRIND_TESTS}
  non robust tests   : ${ENABLE_ALL_TESTS}
  aralia-based tests : ${ENABLE_ARALIA_TESTS}

Other tools
  PHP    : ${PHP}
  GIT    : ${GIT}
  GIT2CL : ${GIT2CL}
  GAWK   : ${GAWK}
 
Settings for Glucose solver
  source directory : ${GLUCOSE_SRCDIR}
  library location : ${GLUCOSE_LIBDIR}
  library name     : ${GLUCOSE_LIBNAME}

ARC documentation (build: ${build_handbook})
  compilation enabled : ${ENABLE_ARC_HANDBOOK}
  makeinfo >= makeinfo_default_version: ${MAKEINFO} ${MAKEINFO_VERSION}
  texi2pdf >= texi2pdf_default_version: ${TEXI2PDF} ${TEXI2PDF_VERSION}
  has PHP : ${HAS_PHP}

Repository infos
  repository is present    : ${IN_REPOSITORY}
  generate repository info : ${GEN_REPOINFO_SCRIPT}
  generate ChangeLog file  : ${GEN_CHANGELOG_SCRIPT}

Creation of ts2alt script (${GENERATE_TS2ALT})

Windows installer (build : ${build_wininstaller})
  makensis program      : ${MAKENSIS}
  ARC_HANDBOOK_BUILDDIR : ${ARC_HANDBOOK_BUILDDIR}
  
Content of dist.repinfo
${DIST_REPINFO}

 ])
])

