#!/bin/bash 

CFLAGS="-Wall -ggdb"
CFGOPT="--enable-ccl-assertions"
DOMAKE=yes

usage() {
    cat <<EOF
usage: $0 [options] ...
where options are:
 --no-make : disable the run of 'make' at the end of the configuration process.
 --debug : configure the package for debugging.
 --release : configure the package for in release mode.
 --help : display this message.

other arguments are passed to 'configure' script.
EOF
    exit $1
}


while test $# -gt 0;
do
    case "$1" in
	--debug)
	    CFLAGS="-Wall -ggdb"
	    CFGOPT="--enable-ccl-assertions"
	    shift
	    ;;
	--release)
	    CFLAGS="-O2"
	    CFGOPT=""
	    shift
	    ;;
	--no-make)
	    DOMAKE=no
	    shift
	    ;;
	--help)
	    usage 0
	    ;;
	*)
	    break;
	    ;;
    esac
done

CXXFLAGS="${CXXFLAGS} ${CFLAGS}"

loc=$(dirname $0)
case "$loc" in
    "/"*)
	CONFIGURE=${loc}/configure
	;;
    *)
	CONFIGURE=${PWD}/${loc}/configure
esac


TOLOWER="tr '[A-Z]' '[a-z]'"
cpu=`(uname -p 2>/dev/null || uname -m 2>/dev/null || echo unknown) | \
        ${TOLOWER}`
os=`uname -s | ${TOLOWER}`

case "$os" in
    darwin*) 
        CFLAGS="${CFLAGS} -gdwarf-2 -gstrict-dwarf --no-pie"
        LDFLAGS="${LDFLAGS} -gdwarf-2 -gstrict-dwarf --no-pie"
	;;
esac

case "$cpu" in
    unknown|*" "*)
        cpu=`uname -m | ${TOLOWER}` ;;
esac

BUILDDIR=${os}-${cpu}

[ -d ${BUILDDIR} ] || mkdir ${BUILDDIR}

cd ${BUILDDIR}

test "x${INST}" = "x" && INST=${PWD}/_inst

CXXFLAGS=${CXXFLAGS} CFLAGS="$CFLAGS" ${CONFIGURE} \
	${CFGOPT} \
	--prefix=$INST \
	--enable-silent-rules \
	--disable-doxygen-doc \
	--enable-readline \
	--disable-valgrind \
	"$@"

if test "${DOMAKE}" = "yes"
then
   make 
fi

