import sys
import select
import threading
import subprocess
import shlex
import os
import re

class Error (Exception):    
    pass

class Process:        
    def __init__ (self, logfile = None):
        if 'ARC' in os.environ:
            args = os.environ['ARC']
        else:
            args = "arc"
        args += " -qx"
        if (logfile != None):
            args += " -l '%s'" % logfile;

        self.arc = subprocess.Popen (shlex.split(args),
                                     bufsize = 0,
                                     executable = None,
                                     stdin = subprocess.PIPE,
                                     stdout = subprocess.PIPE, 
                                     stderr = subprocess.PIPE)
        self.fds = [self.arc.stdout.fileno (), self.arc.stderr.fileno ()]

    def __del__ (self):
        self.arc.terminate ()
        del self.arc

    def command (self, cmd):
        if cmd == "\n":
            return None
        outresult = []
        errresult = None
        self.arc.stdin.write (cmd)
        while True:
            resp = self.arc.stdout.readline ()
            if resp == "EOC\n":
                break;
            elif resp == "EOCWE\n":                
                errresult = ""
                while True:
                    fd = select.select (self.fds, [], [], 0)[0]
                    if not fd or fd[0] != self.fds[1]:
                        break
                    errresult += self.arc.stderr.readline ()
                break
            else:       
                outresult += [ re.sub("\n$","", resp) ]
        if errresult != None:
            raise Error (errresult)

        return outresult

class Connector:
    def __init__ (self, logfile = None):
        self.proc = Process (logfile)
        self.lock = threading.Lock ()

    def __del__ (self):
        del self.proc
        del self.lock

    def send_command (self, cmd):
        if cmd == "\n":
            return []
        out = None
        self.lock.acquire ()
        try:
            out = self.proc.command (cmd);
        except Error, e:
            raise
        finally:
            self.lock.release ()
        assert out != None

        return out
        
    def load_file (self, filename):
        self.send_command ("load \"%s\"\n" % filename)

    def _get_nodes (self):
        return self.send_command ("list nodes\n")

    def get_source (self, model):
        return self.send_command ("show %s\n" % model)

    def validate (self, model):
        return self.send_command ("validate %s\n" % model)

    def get_children (self, nodemodel):
        for c in self.send_command ("node-info %s children\n" % nodemodel):
            l = re.split (" : ", c)
            assert len(l) == 2
            yield l

    def get_nodes (self):
        for nodeid in self._get_nodes ():
            yield re.sub("[*]$", "", nodeid)

    def get_root_nodes (self):
        for nodeid in self._get_nodes ():
            m = re.match ("(^.+)[*]$", nodeid)
            if m:
                yield re.sub("[*]$", "", m.group(0))

    def get_max_nb_states (self, nodemodel):
        return self.send_command ("node-info %s max-conf-card\n" % nodemodel)[0]

    def get_relation_card (self, name):
        return long(float(self.send_command ("card %s\n" % name)[0]))

    def get_relations (self):
        for relid in self.send_command ("list relations\n"):
            yield (relid, self.get_relation_card (relid) )

    def get_transitions (self, nodeid):
        return self.send_command ("node-info %s transitions\n" % nodeid)

    def compute_with (self, nodeid, stmt, symbolically = True):
        if symbolically: mode = "symbolically" 
        else: mode = ""
        self.send_command ("eval\n")
        self.send_command ("with %s do %s %s done\n" % (nodeid, mode, stmt))

        return self.send_command ("EOF\n")
