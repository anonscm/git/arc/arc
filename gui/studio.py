import os
import copy
import sys
import gtk
import gtk.gdk
import gtk.keysyms
import inspect
import re
import xdot
import arc
import glib
import simulator
import stepper
import urllib
from optparse import OptionParser

#from urllib.parse import urlparse
from urlparse import urlparse

AS_CARD_WARNING_FOR_SEM = 15
AS_CARD_LIMIT_FOR_SEM = 130

AS_HOME = os.path.dirname (__file__)
AS_CALLBACKS = { }

def register_callback (name, cb):
    AS_CALLBACKS[name] = cb

class AltaricaStudio:
    def __init__ (self, logfilename = None):
        self.builder = gtk.Builder()        
        self.builder.add_from_file (AS_HOME + "/studio.xml")
        self.builder.connect_signals (self._get_callbacks(), self)
        self.output = self.builder.get_object ("as-arc-console-output").get_buffer ()
        self.output_cmd_tag = self.output.create_tag (foreground="blue")
        self.output_result_tag = self.output.create_tag (foreground="black")
        self.output_error_tag = self.output.create_tag (foreground="red")
        self.arc = arc.Connector (logfilename)

        self.semantics = xdot.DotWidget ()
        self.semantics.show ()
        self.assw = self.builder.get_object ("semantics-tab")
        self.assw.add (self.semantics)
        self.seminfo = self.builder.get_object ("as-semantics-info-label")

        self.graphview = xdot.DotWidget ();
        self.graphview.show();
        self.asgv = self.builder.get_object ("graphview-box")
        self.asgv.add (self.graphview)


        self.nodes = self.builder.get_object ("as-nodes-treestore")
        self.relations = self.builder.get_object ("as-relations-liststore")
        self.errwin = self.builder.get_object ("as-error-message")
        self.tabs = self.builder.get_object ("notebook1")
        self.tabs_cb = dict()
        for id, cb in { 'source-tab' : (self._row_selected_command_tab_cb, 
                                        { 'cb' : self.arc.get_source, 
                                          'txtv' : "as-source-textview" } ),
                        'validation-tab' : (self._row_selected_command_tab_cb, 
                                            { 'cb' : self.arc.validate, 
                                              'txtv' : "as-validation-textview"} ),
                        'semantics-tab' : self._row_selected_semantics_tab_cb, 
                        'graphview-tab' : self._row_selected_graphview_tab_cb, 
                        'relations-tab' : self._row_selected_relations_tab_cb, 
                        'console-tab': self._row_selected_console_tab_cb }.items ():
            assert self.builder.get_object (id) != None
            self.tabs_cb[self.builder.get_object (id)] = cb
        
        self.command_history = []
        self.command_history_pos = -1

    def run (self):
        self.get_window ().show ()
        gtk.main ()

    def get_builder (self):
        return self.builder

    def error (self, message):
        self.errwin.format_secondary_text (message)
        self.errwin.run ()
        self.errwin.hide ()

    def load_file (self, filename):
        try:
            self.arc.load_file (filename)
        except arc.Error, e:
            self.error (e.args[0])
        finally:
            self.update_node_infos ()

    def update_node_infos (self):
        self._build_nodes_hierarchy ()
        self._build_relations_list ()

    def get_window (self):
        return self.get_builder().get_object ("as-toplevel-window")

    def send_command (self, cmd):
        return self.arc.send_command (cmd);

    #
    # Callbacks
    #
    def _get_callbacks (self):
        cbs = copy.copy(AS_CALLBACKS)
        for id, obj in inspect.getmembers (self):
            if re.match ("^as_.*_cb$", id) and inspect.ismethod (obj):
                cbs[id] = obj

        return cbs

    def as_node_selected_cb (self, treeview, aspp):        
        nthp = self.tabs.get_nth_page (self.tabs.get_current_page ())
        cb = self.tabs_cb[nthp]
        path = treeview.get_cursor ()[0]
        self._invoke_tab_callback (treeview, path, cb)
        return True

    def as_change_current_tab_cb (self, notebook, page, page_num, asapp):
        cb = self.tabs_cb[notebook.get_nth_page (page_num)]
        if cb:
            tv = self.builder.get_object ("as-nodes-treeview")
            cursor = tv.get_cursor ()
            self._invoke_tab_callback (tv, cursor[0], cb)

        return False

    def as_refresh_cb (self, act, aspp):
        self.update_node_infos ()
        return True

    def as_load_file_cb (self, act, aspp):
        fc = self.builder.get_object ("as-load-file")
        fc.show ()

        return True

    def get_current_model (self):
        tv = self.builder.get_object ("as-nodes-treeview")
        path = tv.get_cursor ()[0]
        if not path:
            return None

        node = self.nodes.get_value (self.nodes.get_iter (path), 0)        
        if len (path) == 1:
            model = node;
        else:
            model = re.split (" : ", node)[1]
        return model

    def as_start_simulator_cb (self, act, aspp):
        model = self.get_current_model ()
        if model != None:
            s = stepper.Stepper (model, self.arc)
            sim = simulator.AltaricaSimulator (s)
            s.start ()

        return True

    def as_file_chooser_ok_cb (self, widget, aspp):
        fc = self.builder.get_object ("as-load-file")
        fc.hide ()
        uri = fc.get_uri ()
        if uri:
            filename = urllib.unquote(urlparse (uri)[2])
            self.load_file (filename)
        return True

    def as_file_chooser_cancel_cb (self, widget, aspp):
        fc = self.builder.get_object ("as-load-file")
        fc.hide ()

        return True

    def as_quit_menu_item_cb (self, menuitem, aspp):
        gtk.main_quit ()
        return True

    def as_close_window_cb (self, widget, event, aspp):
        gtk.main_quit ()
        return True

    def _cmd_history_move (self, entry, dir):
        newpos = self.command_history_pos + dir
        if  not (0 <= newpos <= len (self.command_history)):
            return
        if newpos == len (self.command_history):
            cmd = ""
        else:
            cmd = self.command_history[newpos]
        entry.set_text (cmd)
        self.command_history_pos = newpos

    def as_send_arc_command_cb (self, widget, event, aspp):
        if (event.keyval == gtk.keysyms.Up):
            self._cmd_history_move (widget, -1)
            return True

        if (event.keyval == gtk.keysyms.Down):
            self._cmd_history_move (widget, 1)
            return True

        if (event.keyval != gtk.keysyms.Return):
            return False        

        cmd = widget.get_text ()
        if cmd != "":
            self.command_history.append (cmd)
            self.command_history_pos = len (self.command_history)
        else:
            return
        cmd += "\n"
        widget.set_text ("")
        end = self.output.get_end_iter ()
        self.output.insert_with_tags (end, cmd, self.output_cmd_tag)
        outtag = self.output_result_tag
        try:
            out = self.send_command (cmd)
        except arc.Error, e:
            outtag = self.output_error_tag
            out = [ e.args[0] ]

        for line in out:
            self.output.insert_with_tags (end, "%s\n" % line, outtag)

        tv = self.builder.get_object ("as-arc-console-output")
        tv.scroll_to_mark (self.output.get_insert(), 0)

        return True

    def as_about_open_cb (self, menuitem, asapp):
        about = asapp.get_builder().get_object ("as-about")
        about.run ()
        about.hide ()
        return True

    def as_graphview_command_cb (self, widget, event, aspp):
        if (event.keyval != gtk.keysyms.Return):
            return False        

        model = self.get_current_model ()
        if model == None:
            return
        cmd = widget.get_text ()
        if cmd == "":
            return

        try:
            code = ""
            for line in self.arc.compute_with (model, cmd + ";"):
                code += line + "\n"
            
            self.graphview.set_dotcode (code)
            self.graphview.show ()

        except arc.Error, e:
            self.error (e.args[0])       
        return True

    def _invoke_tab_callback (self, treeview, path, cb):
        if cb and path:
            if isinstance (cb, tuple):
                cb[0] (treeview, path, cb[1])
            else:
                cb (treeview, path)

    def _row_selected_command_tab_cb (self, treeview, path, data):
        if not path:
            return
        node = self.nodes.get_value (self.nodes.get_iter (path), 0)        
        if len (path) == 1:
            model = node;
        else:
            model = re.split (" : ", node)[1]
        tb = self.builder.get_object (data['txtv']).get_buffer ()
        tb.delete (tb.get_start_iter (), tb.get_end_iter ())
        
        try:
            for line in data['cb'] (model):
                tb.insert (tb.get_end_iter (), line + "\n")
        except arc.Error, e:
            self.error (e.args[0])       

    def _row_selected_relations_tab_cb (self, treeview, path):
        self._build_relations_list ()
 
    def has_computed_reachables (self, model):
        id = model + "!reach";
        for r in self.arc.get_relations ():
            if r[0] == id:
                return True;
        return False;

    def _row_selected_semantics_tab_cb (self, treeview, path):
        if not path:
            return
        node = self.nodes.get_value (self.nodes.get_iter (path), 0)        
        if len (path) == 1:
            model = node;
        else:
            model = re.split (" : ", node)[1]
        if self.has_computed_reachables (model):
            card = float (self.arc.get_relation_card (model + "!reach"))
        else:
            card = float (self.arc.get_max_nb_states (model))
        
        if card >= AS_CARD_LIMIT_FOR_SEM:
            buf = gtk.TextBuffer ()
            buf.set_text("Sorry the state grah may have too many "
                         "vertices (%s) to be displayed.\n" % card)
            self.seminfo.set_buffer (buf)
            self.semantics.hide ()
            self.seminfo.show ()
            return
        elif card >= AS_CARD_WARNING_FOR_SEM:
            sci = self.builder.get_object ("as-sem-card-info")
            resp = sci.run ()
            sci.hide ()
            if (resp != int(gtk.RESPONSE_YES)):
                self.semantics.hide ()
                self.seminfo.show ()
                return

        try:
            code = ""
            for line in self.arc.compute_with (model, "dot (any_s, any_t);"):
                code += line + "\n"
            
            self.semantics.set_dotcode (code)
            self.semantics.show ()
            self.seminfo.hide ()

        except arc.Error, e:
            self.error (e.args[0])       

    def _row_selected_graphview_tab_cb (self, treeview, path):
        if not path:
            return
        node = self.nodes.get_value (self.nodes.get_iter (path), 0)        
        if len (path) == 1:
            model = node;
        else:
            model = re.split (" : ", node)[1]
        cmdline = self.builder.get_object ("graphview-command")
        cmd = cmdline.get_text ()
        if cmd == "":
            return
        try:
            code = ""
            for line in self.arc.compute_with (model, cmd + ";"):
                code += line + "\n"
            
            self.graphview.set_dotcode (code)
            self.graphview.show ()

        except arc.Error, e:
            self.error (e.args[0])       



    def _row_selected_console_tab_cb (self, treeview, path):
        glib.idle_add (self.builder.get_object ("arc-command-input").grab_focus)
        ci = self.builder.get_object ("arc-command-input")
        ci.grab_focus ()

    #
    # Internals
    #
    def _build_nodes_hierarchy_rec (self, parent, nodemodel, nodeid = None):
        if nodeid :
            text = nodeid + " : " + nodemodel
        else:
            text = nodemodel
        piter = self.nodes.append (parent, [ text ])
        for node in self.arc.get_children (nodemodel):
            self._build_nodes_hierarchy_rec (piter, node[1], node[0])

    def _build_nodes_hierarchy (self):
        self.nodes.clear ()
        for node in self.arc.get_nodes ():
            self._build_nodes_hierarchy_rec (None, node)

    def _build_relations_list (self):
        self.relations.clear ()
        
        for rel in self.arc.get_relations ():
            piter = self.relations.append ()
            self.relations.set_value (piter, 0, rel[0])
            self.relations.set_value (piter, 1, rel[1])

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-l", "--logfile", dest="logfile",
                      help="specify the logfile for ARC.", 
                      metavar="FILE")

    (options, args) = parser.parse_args()

    asapp = AltaricaStudio (options.logfile)

    for i in range (0, len (args)):
        asapp.load_file (args[i])
    asapp.run ()
