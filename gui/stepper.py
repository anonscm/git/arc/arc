import arc
import re
import gobject

class State:
    def __init__ (self, string):
        self.data = string
        self.formula = None
        self.id = -1
        
    def __str__ (self):
        return self.data

    def __hash__ (self):
        return 19 * hash (self.data)

    def __repr__ (self):
        return str(self)

    def __eq__ (self, other):
        return isinstance (other, State) and self.data == other.data
        
    def __ne__ (self, other):
        return not (self == other)

    def set_ident (self, ident):
        self.id = ident

    def get_ident (self):
        return self.id

    def get_diff (self, other):
        for s,o in zip (re.split (", ", self.data),re.split (", ", other.data)):
            if s != o:
                yield (s,o)

    def get_assignments (self):
        if self.formula == None:
            self.formula = ""
            for s in re.split (", ", self.data):
                self.formula += '\"%s\" ' % s
        return self.formula

    def get_values (self):
        for s in re.split (", ", self.data):
            yield s

class Stepper(gobject.GObject):
    __gsignals__ = {
        "states-changed" : (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
        "enabled-transitions-changed" : (gobject.SIGNAL_RUN_FIRST, 
                                         gobject.TYPE_NONE, ())

        }

    def __init__ (self, nodeid, arc):
        gobject.GObject.__init__ (self)
        self.nodeid = nodeid
        self.arc = arc
        self.state_space = dict ()
        self.states_by_index = dict ()

    def start (self, init = None):
        self.stack = []
        self.stack_index = -1
        self.edges = []
        self.last_state_index = 0

        cmd = "start"
        if init and len (init) > 0:
            cmd += " \"" + init + "\""
        states = []
        for s in self._stepper (cmd):
            states.append (self._find_or_add_state (s))

        transitions = None
        if len (states) == 1:
            transitions = self._get_valid_transitions (states[0])
        self.stack.append ((states, transitions))
        self.stack_index = 0
        self._update_state ()
        
    def get_connector (self):
        return self.arc

    def get_states (self):
        return self.stack[self.stack_index][0]

    def get_number_of_states (self):
        return len (self.stack[self.stack_index][0])

    def get_current_state (self):
        assert len (self.get_states()) == 1
        return self.get_states ()[0]

    def get_valid_transitions (self):
        return self.stack[self.stack_index][1]

    def set_state (self, state):
        assert isinstance (state, State)
        trans = self._get_valid_transitions (state)
        if self.stack_index + 1 == len (self.stack):
            self.stack.append (([state], trans))
        else:
            self.stack[self.stack_index + 1] = ([state], trans)
        self.go_forward ()

    def trigger_transition (self, i):
        assert i in self.get_valid_transitions ()
        src = self.get_current_state ()
        assignment = src.get_assignments ()
        cmd = "trigger %d %s" % (i, assignment)

        new_states = []
        for s in self._stepper (cmd):
            tgt = self._find_or_add_state (s)
            e = (src, i, tgt)
            if e not in self.edges:
                self.edges.append (e)
            new_states.append (tgt)

        transitions = None
        if len (new_states) == 1:
            transitions = self._get_valid_transitions (new_states[0])
            
        self.stack[self.stack_index + 1:] = [(new_states, transitions)]
        self.go_forward ()

    def go_backward (self):
        assert 0 <= self.stack_index < len (self.stack)
        if 0 <= self.stack_index -1:
            self.stack_index -= 1
            self._update_state ()

    def go_forward (self):
        assert 0 <= self.stack_index < len (self.stack)
        if self.stack_index + 1 < len (self.stack):
            self.stack_index += 1
            self._update_state ()

    def get_variables (self):
        return self._stepper ("vars")

    def get_transitions (self):
        return self.arc.get_transitions (self.nodeid)

    def get_edges (self):
        return self.edges

    def get_state_by_index (self, index):
        if 0 <= index < len (self.states_by_index):
            return self.states_by_index[index]
        return None
    #
    # Private methods
    #
    def _stepper (self, cmd):
        return self.arc.send_command ("stepper %s %s\n" % (self.nodeid, cmd))

    def _update_state (self):
        self.states, self.transitions = self.stack[self.stack_index]
        self.emit ("states-changed")
        self.emit ("enabled-transitions-changed")

    def _get_valid_transitions (self, state):
        cmd = 'valid-trans %s' % state.get_assignments ()
        return [ int (t) for t in self._stepper (cmd) ]

    def _find_or_add_state (self, string):
        tmps = State (string)
        if tmps in self.state_space:
            tmps = self.state_space[tmps]
        else:
            self.state_space[tmps] = tmps
            tmps.set_ident (self.last_state_index)
            self.states_by_index[self.last_state_index] = tmps
            self.last_state_index += 1

        return tmps
            

gobject.type_register (Stepper)

if __name__ == "__main__":
    def states_changed_cb (stepper):
        print "states changed to:"
        print stepper.get_states ()

    def trans_changed_cb (stepper):
        print "trans changed to:"
        print stepper.get_valid_transitions ()

    import sys
    nodename = sys.argv[2]
    proc = arc.Connector (logfile = "/dev/pts/2")
    proc.load_file (sys.argv[1])
    transstr = proc.send_command ("node-info %s transitions\n" % nodename)
    s = Stepper (nodename, proc)

    s.connect ("states-changed", states_changed_cb)
    s.connect ("enabled-transitions-changed", trans_changed_cb)

    s.start ()
    while True:
        currentstates = s.get_states()
        if len(currentstates) > 1:
            s.set_state (currentstates[1])
        trans = s.get_valid_transitions ()
        print "trigger ", transstr[trans[0]]
        s.trigger_transition (trans[0])


