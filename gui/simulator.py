import os
import copy
import sys
import gtk
import gobject
import gtk.gdk
import gtk.keysyms
import inspect
import re
import xdot
import arc
import glib
import studio
import stepper
from optparse import OptionParser

class AltaricaSimulator:
    def __init__ (self, stepper):
        # Stepper related initialization
        self.stepper = stepper        
        stepper.connect ("states-changed", self._states_changed_cb)
        stepper.connect ("enabled-transitions-changed", self._trans_changed_cb)
        self.vars = stepper.get_variables ()
        self.visible_vars = dict()

        for v in self.vars:
            self.visible_vars[v] = True

        self.trans = []
        for t in stepper.get_transitions ():
            mo  = re.match (r"^(.*) \|- (.*) -> (.*)$", t)
            g = mo.group(1)
            ev = mo.group(2)
            a = mo.group(3)
            moe = re.match (r"^\((.+), (.+)\)$", ev)
            if moe and moe.group(1) == moe.group(2):
                ev = moe.group(1)
            self.trans.append ((g,ev,a))

        # GUI
        self.builder = gtk.Builder()        
        self.builder.add_from_file (studio.AS_HOME + "/simulator.xml")
        self.builder.connect_signals (self)
        self.dotWidget = xdot.DotWidget ()
        self.dotWidget.connect ('clicked', self.state_selected_cb)
        self.dotWidget.show ()

        self.builder.get_object ("state_graph_viewport").add (self.dotWidget)
        
        self.conftreeview = self.builder.get_object ("configurations-treeview")
        self.confmodel = gtk.TreeStore (gobject.TYPE_STRING)
        self.conftreeview.set_model (self.confmodel)

        column = gtk.TreeViewColumn ("Variable", gtk.CellRendererText (), 
                                     text = 0)
        self.conftreeview.append_column (column)
        self.conftreeview.set_enable_tree_lines (True)

        self.events_treeview = self.builder.get_object ("events-treeview")
        self.enabled_events = self.builder.get_object ("valid_transitions")
        self.event2transindex = dict()

        self.builder.get_object("altarica_simulator").show ()

    def select_variables_cb (self, act):
        vs = VariableSelector (self.visible_vars)
        vs.run ()
        self.confmodel = gtk.TreeStore (str)
        self._states_changed_cb (self.stepper)

        return True

    def state_selected_cb (self, widget, stateid, url, event):
        index = int (stateid)
        s = self.stepper.get_state_by_index (index)
        if s is not None:
            self.stepper.set_state (s)
            return True
        return False

    def go_backward_action_cb (self, act):
        self.stepper.go_backward ()
        return True

    def go_forward_action_cb (self, act):
        self.stepper.go_forward ()
        return True

    def transition_selected_cb (self, treeview, path, view_column):
        if path not in self.event2transindex:
            return
        self.stepper.trigger_transition (self.event2transindex[path])

        return True

    def close_simulator_cb (self, window):
        self.window.destroy ()

        return True

    def _update_graph (self):
        tmpl = '  %d -> %d [label="%s"];\n'
        dotcode = "digraph G {\n"
        for e in self.stepper.get_edges ():            
            dotcode += tmpl % (e[0].get_ident (), e[2].get_ident (), 
                               self.trans[e[1]][1])
        if self.stepper.get_number_of_states () == 1:
            dotcode += ('  %d [style=filled,color=red];\n' % 
                        self.stepper.get_current_state ().get_ident ())
        dotcode += "}\n"
        self.dotWidget.set_dotcode (dotcode)

    def _state_selected_cb (self, tvc, index):
        states = self.stepper.get_states ()
        if len (states) > 1:
            self.stepper.set_state (states[index])
        return True

    def _states_changed_cb (self, stepper):
        states = stepper.get_states ()        
        if len(states) + 1 != self.confmodel.get_n_columns ():
            vartypes = tuple( str for i in range (0, len(states) + 1))
            self.confmodel = gtk.TreeStore (*vartypes)
            self.conftreeview.set_model (self.confmodel)

        self._fill_configuration_treestore (self.confmodel, states)

        renderer = gtk.CellRendererText ()
        
        for c in self.conftreeview.get_columns ()[1:]:
            self.conftreeview.remove_column (c)

        for i in range (1, len(states) + 1):
            column = gtk.TreeViewColumn ("X", renderer, text = i)
            column.set_clickable (True)
            column.connect ("clicked", self._state_selected_cb, i - 1)
            self.conftreeview.append_column (column)
        self._update_graph ()

    def _trans_changed_cb (self, stepper):
        self.enabled_events.clear ()
        trans = stepper.get_valid_transitions ()
        if trans == None:
            return

        del self.event2transindex
        self.event2transindex = dict ()
        for vti in trans:
            e = self.trans[vti][1]
            g = self.trans[vti][0]
            a = self.trans[vti][2]
            p = self.enabled_events.get_iter_first ()
            while p != None:
                if self.enabled_events.get_value (p, 0) == e:
                    break;
                p = self.enabled_events.iter_next (p)

            if p == None:
                p = self.enabled_events.append (p)
                self.enabled_events.set (p, 0, e)
            p = self.enabled_events.append (p)
            self.enabled_events.set (p, 0, g)
            self.enabled_events.set (p, 1, a)
            self.event2transindex[self.enabled_events.get_path(p)] = vti

        p = self.enabled_events.get_iter_first ()
        while p != None:
            if self.enabled_events.iter_n_children (p) == 1:
                path = self.enabled_events.get_path(p)
                self.events_treeview.expand_to_path (path)
            p = self.enabled_events.iter_next (p)

    def _fill_configuration_treestore (self, model, states):
        rows = dict ()
        ordered_vars = []
        for s in states:
            for a in s.get_values ():
                if a == "":
                    continue
                var, val = re.split (" = ", a)
                if not self.visible_vars[var]:
                    continue
                ordered_vars.append (var)

                if not var in rows:
                    rows[var] = re.split ("[.]", var)[-1:]
                rows[var] += [val]

        ordered_vars.sort()
        for var in ordered_vars:
            values = rows[var]
            path = re.split ("[.]", var)
            p = _find_or_add_iter_for_var (model, None, path)
            for i, val in zip(range(0,len(values)),values):
                model.set (p, i, val)

class VariableSelector (gtk.Dialog):
    def __init__ (self, vars):
        gtk.Dialog.__init__ (self, "Visible variables")        
        ok = self.add_button ("Ok", 0)
        ok.set_resize_mode (gtk.RESIZE_IMMEDIATE)
        ok.connect ("clicked", self.close)

        sw = gtk.ScrolledWindow ()             
        self.vars = vars
        self.get_content_area ().add (sw)

        self.model = gtk.TreeStore (str, bool)
        ordered_vars = []
        for var in self.vars.keys ():
            ordered_vars.append (var)
        ordered_vars.sort()

        for var in ordered_vars:
            path = re.split ("[.]", var)
            is_visible = self.vars[var]
            p = _find_or_add_iter_for_var (self.model, None, path)
            self.model.set (p, 0, path[-1])
            self.model.set (p, 1, is_visible)
            self.toggled_rec_up (p)

        self.treeview = gtk.TreeView (self.model)

        variablesColumn = gtk.TreeViewColumn ("Variable")
        toggleButtons = gtk.TreeViewColumn (None)
        self.treeview.append_column (variablesColumn)
        self.treeview.append_column (toggleButtons)

        crtxt = gtk.CellRendererText()
        variablesColumn.pack_start (crtxt, True)
        variablesColumn.set_expand (True)
        variablesColumn.add_attribute (crtxt, "text", 0)

        crtog = gtk.CellRendererToggle ()       
        toggleButtons.pack_start (crtog, False)
        toggleButtons.add_attribute (crtog, "active", 1)
        toggleButtons.set_expand (False)
        crtog.connect ("toggled", self.toggled_cb)

        

        self.treeview.show ()
        sw.add (self.treeview)
        sw.set_policy (gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        sw.show ()

        self.set_size_request (200, 200);
        self.set_modal (True)

    def toggled_rec_down (self, p, value):
        self.model.set (p, 1, value)
        p = self.model.iter_children (p)
        while p != None:
            self.toggled_rec_down (p, value)
            p = self.model.iter_next (p)

    def toggled_rec_up (self, h):
        if not h:
            return
        p = self.model.iter_children (h)
        if p != None:
            value = True
            while p != None:
                value = value and self.model.get_value (p, 1)
                p = self.model.iter_next (p)
            self.model.set (h, 1, value)
        self.toggled_rec_up (self.model.iter_parent (h))
        
    def toggled_cb (self, cell, path):
        value = not self.model[path][1]
        p = self.model.get_iter (path)
        self.toggled_rec_down (p, value)
        self.toggled_rec_up (p)

    def update_vars (self):
        for var in self.vars.keys ():
            path = re.split ("[.]", var)
            p = _find_or_add_iter_for_var (self.model, None, path)
            self.vars[var] = self.model.get_value (p, 1)
        
    def close (self, w):
        self.destroy ()
        self.update_vars ()
        return True

def _find_or_add_iter_for_var (model, parent, path):
    l = path[0]
    p = model.iter_children (parent)
    while p != None:
        if model.get_value (p, 0) == l:
            break
        p = model.iter_next (p)
        
    if p == None:
        p = model.append (parent)
        model.set (p, 0, l)

    if len (path) > 1:
        return _find_or_add_iter_for_var (model, p, path[1:])
    return p


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-l", "--logfile", dest="logfile",
                      help="specify the logfile for ARC.", 
                      metavar="FILE")

    (options, args) = parser.parse_args()

    if (len (args) == 1):
        nodename = "Main";
    else:
        nodename = args[1];
    try:
        proc = arc.Connector (logfile = options.logfile)
        proc.load_file (args[0])
        s = stepper.Stepper (nodename, proc)
        asapp = AltaricaSimulator (s)
        s.start ()
        gtk.main ()
    except arc.Error, e:
        sys.stderr.write (e.message);
        sys.exit (1);

